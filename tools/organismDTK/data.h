#ifndef ORGANISMDTK_DATA_H
#define ORGANISMDTK_DATA_H

#include <vector>
#include <iostream>

typedef std::vector<double> Vector;
typedef std::vector<Vector> Matrix;


// The class Frame is a wrapper to a Matrix, but with an additional
// floating point variable to store the time.
class Frame
{
 public:
	void setMatrix(Matrix matrix);
	Matrix &getMatrix(void);
	void setTime(double time);
	double getTime(void);
	size_t size(void);
	Vector &operator[](size_t index);
 private:
	Matrix matrix_;
	double time_;
};

class Data
{
 public:
	void addFrame(Frame frame);
	size_t frames(void);
	Frame &getFrame(size_t index);
	//	Frame &operator[](size_t index);
 private:
	std::vector<Frame> data_;
};

#endif /* ORGANISMDTK_DATA_H */
