#include <sstream>
#include "inputMethod.h"
#include <cstdlib>

InputMethod::~InputMethod()
{
}

Data *InputMethod::readInput(std::istream &in)
{
	std::cerr << "Internal Error: Do not use base class function InputMethod::readInput(std::istream &in)." << std::endl;
	exit(EXIT_FAILURE);
}

InputMethod *InputMethod::getInputMethod(std::string name)
{
	if (name == "default" || name == "organism") 
		return new OrganismInputMethod();
	else if (name == "raw")
		return new RawInputMethod();
	else if (name == "init")
		return new InitInputMethod();
	else {
		std::cerr << "Error: Input method " << name << " is unsupported." << std::endl;
		exit(EXIT_FAILURE);
	}
}

Data *OrganismInputMethod::readInput(std::istream &in)
{
	Data *data = new Data;
	size_t frames;
	in >> frames;
		for (size_t n = 0; n < frames; ++n) {
		Frame frame;
		Matrix matrix;
		size_t N, M;
		double time;
		in >> N >> M >> time;
		for (size_t i = 0; i < N; ++i) {
			Vector vector;
			for (size_t j = 0; j < M; ++j) {
				double element;
				in >> element;
				vector.push_back(element);
			}
			matrix.push_back(vector);
		}
		frame.setMatrix(matrix);
		frame.setTime(time);
		data->addFrame(frame);
	}
	return data;
}

Data *RawInputMethod::readInput(std::istream &in)
{
	Data *data = new Data;
	Matrix matrix;
	while(true) {
		std::string line;
		getline(in, line);
		if (in.eof())
			break;

		if (line.empty())
			continue;
		
		std::stringstream stream(line);
		Vector vector;
		while (true) {
			std::string tmp;
			stream >> tmp;
			if (tmp.empty())
				break;
			else
				vector.push_back(atof(tmp.c_str()));
		}
		matrix.push_back(vector);
	}
	Frame frame;
	frame.setMatrix(matrix);
	frame.setTime(0);
	data->addFrame(frame);
	return data;
}

Data *InitInputMethod::readInput(std::istream &in)
{
	Data *data = new Data;
	size_t numberOfCompartments;
	size_t numberOfVariables;
	in >> numberOfCompartments >> numberOfVariables;

	Matrix matrix;
	for (size_t i = 0; i < numberOfCompartments; ++i) {
		Vector vector;
		for (size_t j = 0; j < numberOfVariables; ++j) {
			double value;
			in >> value;
			vector.push_back(value);
		}
		matrix.push_back(vector);
	}
	Frame frame;
	frame.setMatrix(matrix);
	frame.setTime(0);
	data->addFrame(frame);
	return data;
}

