#ifndef ORGANISMDTK_INPUTMETHOD_H
#define ORGANISMDTK_INPUTMETHOD_H

#include <istream>
#include "data.h"

class InputMethod
{
 public:
  virtual ~InputMethod();
  virtual Data *readInput(std::istream &in);
  static InputMethod *getInputMethod(std::string name);
};

class OrganismInputMethod : public InputMethod
{
 public:
  Data *readInput(std::istream &in);
};

class RawInputMethod : public InputMethod
{
 public:
  Data *readInput(std::istream &in);
};

class InitInputMethod : public InputMethod
{
 public:
  Data *readInput(std::istream &in);
};

#endif /* ORGANISMDTK_INPUTMETHOD_H */
