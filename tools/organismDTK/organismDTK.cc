#include <fstream>
#include <iostream>
#include "action.h"
#include "data.h"
#include "file.h"
#include "inputMethod.h"
#include "outputMethod.h"
#include "src/myConfig.h"
#include <cstdlib>

void printHelp(void);

int main(int argc, char *argv[])
{
	// Initialise user configuration.
	myConfig::registerOption("input", 1);
	myConfig::registerOption("output", 1);
	myConfig::registerOption("inputFile", 1);
	myConfig::registerOption("outputFile", 1);
	myConfig::registerOption("precision", 1);
	myConfig::initConfig(argc, argv, "");

	// Check the number of command line arguments.
	if (myConfig::argc() == 1) {
		std::cerr << "Type '" << argv[0] << " help' for usage." << std::endl;
		exit(EXIT_FAILURE);
	}
	
	std::string actionName = myConfig::argv(1);
	action::Action *action;
	if (actionName == "help")
		printHelp();
	else
		action = action::Action::getAction(actionName);

	// Check number of parameters.
	action::Range argumentsRange = action->arguments();
	int arguments = myConfig::argc() - 2;
	if (arguments < argumentsRange.first) {
		std::cerr << "Error: Action " << actionName << " requires at least " << argumentsRange.first << " arguments." << std::endl;
		exit(EXIT_FAILURE);
	} else if (arguments != 0 && argumentsRange.first == 0 && argumentsRange.second == 0) {
		std::cerr << "Error: Action " << actionName << " accepts no arguments." << std::endl;
		exit(EXIT_FAILURE);
	} else if (arguments > argumentsRange.second) {
		std::cerr << "Error: Action " << actionName << " accepts at most " << argumentsRange.second << " arguments." << std::endl;
		exit(EXIT_FAILURE);
	}

	// Determine correct input method to use.
	std::string inputMethodName;
	if (myConfig::getBooleanValue("input"))
		inputMethodName = myConfig::getValue("input", 0);
	else
		inputMethodName = "organism";

	// Create an InputMethod object.
	InputMethod *inputMethod = InputMethod::getInputMethod(inputMethodName);

	// Determine correct output method to use.
	std::string outputMethodName;
	if (myConfig::getBooleanValue("output"))
		outputMethodName = myConfig::getValue("output", 0);
	else
		outputMethodName = "organism";

	// Create an OutputMethod object.
	OutputMethod *outputMethod = OutputMethod::getOutputMethod(outputMethodName);

	// Determine source of input and read input.
	Data *dataIn;
	if (myConfig::getBooleanValue("inputFile"))
		file::readFile(myConfig::getValue("inputFile", 0), dataIn, inputMethod);
	else
		dataIn = inputMethod->readInput(std::cin);

	// Process data.
	Data *dataOut = action->process(dataIn, outputMethod);

	if (dataOut != NULL) {
		// Determine target of output and write output.
		if (myConfig::getBooleanValue("outputFile")) {
			file::writeFile(myConfig::getValue("outputFile", 0), dataOut, outputMethod);
		} else {
			if (myConfig::getBooleanValue("precision")) {
				std::cout.precision(atoi(myConfig::getValue("precision", 0).c_str()));
			}
			outputMethod->writeOutput(std::cout, dataOut);
		}
		delete dataOut;
	}

	// Delete all objects created with new.
	delete dataIn;
	delete inputMethod;
	delete outputMethod;
	delete action;

	// Make sure we quit in a clean way.
	exit(EXIT_SUCCESS);
}

void printHelp(void)
{
	std::string actionName;
	action::Action *action;
	switch (myConfig::argc()) {
	case 2:
		std::cout << "organismDTK (organism Data Toolkit) is a utility to extract data and statistics from organism data files." << std::endl;
		std::cout << std::endl;
		std::cout << "Usage: " << myConfig::argv(0) << " action arg1 arg2 arg3 ..." << std::endl;
		std::cout << std::endl;
		std::cout << "Type '" << myConfig::argv(0) << " help action' for information about an action." 
				<< std::endl
				<< "Supported actions are: average, averageShortestDistance, extractPeaksFromNeighborhood, help, identity, split and projectIntoBoundaryBox."
				<< std::endl;
		exit(EXIT_SUCCESS);
	case 3:
		actionName = myConfig::argv(2);
		action = action::Action::getAction(myConfig::argv(2));
		action->printHelp();
		delete action;
		exit(EXIT_SUCCESS);
	default:
		std::cout << "Type '" << myConfig::argv(0) << " help action' for information about an action." 
				<< std::endl;
		exit(EXIT_FAILURE);
	}
}

