#include "outputMethod.h"
#include <cstdlib>

OutputMethod::~OutputMethod()
{

}

void OutputMethod::writeOutput(std::ostream &out, Data *data)
{
	std::cerr << "Internal Error: Do not use base class function OutputMethod::writeOutput(std::ostream &out, const Data &data)." << std::endl;
	exit(EXIT_FAILURE);
}

OutputMethod *OutputMethod::getOutputMethod(std::string name)
{
	if (name == "default" || name == "organism")
		return new OrganismOutputMethod();
	else if (name == "gnuplot")
		return new GnuplotOutputMethod();
	else if (name == "raw")
		return new RawOutputMethod();
	else if (name == "init")
		return new InitOutputMethod();
	else {
		std::cerr << "Error: Output method " << name << " is unsupported." << std::endl;
		exit(EXIT_FAILURE);
	}
}

void OrganismOutputMethod::writeOutput(std::ostream &out, Data *data)
{
	out << data->frames() << std::endl;
	for (size_t n = 0; n < data->frames(); ++n) {
		Frame frame = data->getFrame(n);
		Matrix matrix = frame.getMatrix();
		out << matrix.size() << " " << matrix[0].size() << " " << frame.getTime() << std::endl;
		for (size_t i = 0; i < matrix.size(); ++i) {
			for (size_t j = 0; j < matrix[i].size(); ++j) {
				out << matrix[i][j] << " ";
			}
			out << std::endl;
		}
		out << std::endl;
	}
}

void GnuplotOutputMethod::writeOutput(std::ostream &out, Data *data)
{
	for (size_t n = 0; n < data->frames(); ++n) {
		Matrix matrix = data->getFrame(n).getMatrix();
		for (size_t i = 0; i < matrix.size(); ++i) {
			out << n << " " << data->getFrame(n).getTime() << " " << i << " ";
			for (size_t j = 0; j < matrix[i].size(); ++j) {
				out << matrix[i][j] << " ";
			}
			std::cout << std::endl;
		}
		std::cout << std::endl;
	}
}

void RawOutputMethod::writeOutput(std::ostream &out, Data *data)
{
	for (size_t n = 0; n < data->frames(); ++n) {
		Matrix matrix = data->getFrame(n).getMatrix();
		for (size_t i = 0; i < matrix.size(); ++i) {
			for (size_t j = 0; j < matrix[i].size(); ++j) {
				out << matrix[i][j] << " ";
			}
			std::cout << std::endl;
		}
		std::cout << std::endl;
	}
}

void InitOutputMethod::writeOutput(std::ostream &out, Data *data)
{
	if (data->frames() != 1) {
		std::cerr << "Error: InitOutputMethod::writeOutput(): More than one frame." << std::endl;
		exit(EXIT_FAILURE);
	}

	Matrix matrix = data->getFrame(0).getMatrix();
	out << matrix.size() << " " << matrix[0].size() << std::endl;
	for (size_t i = 0; i < matrix.size(); ++i) {
		for (size_t j = 0; j < matrix[i].size(); ++j) {
			out << matrix[i][j] << " ";
		}
		out << std::endl;
	}
}
