#include "action.h"
#include "actions/average.h"
#include "actions/extractPeaksFromNeighborhood.h"
#include "actions/identity.h"
#include "actions/projectIntoBoundaryBox.h"
#include "actions/shortestDistance.h"
#include "actions/split.h"
#include <cstdlib>

using namespace action;

Action::~Action()
{
}

Data *Action::process(Data *data, OutputMethod *outputMethod)
{
	std::cerr << "Internal Error: Do not use base class function Action::process(Data *data)." << std::endl;
	exit(EXIT_FAILURE);
}

void Action::printHelp(void) 
{
	std::cerr << "Internal Error: Do not use base class function Action::printHelp(void)." << std::endl;
	exit(EXIT_FAILURE);
}

Range Action::arguments(void)
{
	std::cerr << "Internal Error: Do not use base class function Action::arguments(void)." << std::endl;
	exit(EXIT_FAILURE);
}

Action *Action::getAction(std::string name)
{
	if (name == "average")
		return new Average();
 	else if (name == "extractPeaksFromNeighborhood")
 		return new ExtractPeaksFromNeighborhood();
 	else if (name == "identity")
 		return new Identity();
	else if (name == "projectIntoBoundaryBox")
		return new ProjectIntoBoundaryBox();
	else if (name == "split")
		return new Split();
	else if (name == "shortestDistance")
		return new ShortestDistance();
	else {
		std::cerr << "Error: Action " << name << " is unsupported." << std::endl;
		exit(EXIT_FAILURE);
	}
}
