#ifndef ORGANISMDTK_AVERAGE_H
#define ORGANISMDTK_AVERAGE_H

#include "../action.h"

namespace action
{
	class Average : public Action
	{
	public:
		Data *process(Data *data, OutputMethod *outputMethod = NULL);
		void printHelp(void);
		Range arguments(void);
	private:
		std::vector<double> getShortestDistance(const std::vector<size_t> &column, const std::vector<double> &periodicity, const Matrix &data);
		double calculateDistance(size_t i, size_t j, const std::vector<size_t> &column, const std::vector<double> &periodicity, const Matrix &data); 
		std::pair<double, double> calculateStatistics(const std::vector<double> &distances);
	};	
}

#endif /* ORGANISMDTK_AVERAGE_H */
