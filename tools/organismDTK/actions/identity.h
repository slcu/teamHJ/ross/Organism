#ifndef ORGANISMDTK_IDENTITY_H
#define ORGANISMDTK_IDENTITY_H

#include "../action.h"

namespace action
{
	class Identity : public Action
	{
	public:
		Data *process(Data *data, OutputMethod *outputMethod = NULL);
		void printHelp(void);
		Range arguments(void);
	};
}

#endif /* ORGANISMDTK_IDENTITY_H */
