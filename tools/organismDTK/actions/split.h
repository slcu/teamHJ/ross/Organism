#ifndef ORGANISMDTK_SPLIT_H
#define ORGANISMDTK_SPLIT_H

#include "../action.h"

namespace action
{
	class Split : public Action
	{
	public:
		Data *process(Data *data, OutputMethod *outputMethod = NULL);
		void printHelp(void);
		Range arguments(void);
	};
}

#endif /* ORGANISMDTK_SPLIT_H */
