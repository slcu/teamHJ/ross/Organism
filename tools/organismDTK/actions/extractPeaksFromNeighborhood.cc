#include "extractPeaksFromNeighborhood.h"
#include "../../../src/common/myConfig.h"
#include "../../../src/organism.h"

using namespace action;

Data *ExtractPeaksFromNeighborhood::process(Data *data, OutputMethod *outputMethod)
{
	std::vector< std::vector<size_t> > peaks;

	enum { MAX = 0, MIN = 1 };
	Organism organism;
	organism.readModel(myConfig::argv(3));
	
	int column = atoi(myConfig::argv(4).c_str());
	
	unsigned int action;
	if (myConfig::argv(2) == "max")
		action = MAX;
	else if (myConfig::argv(2) == "min")
		action = MIN;
	else {
		std::cerr << "Error: First argument must be 'max' or 'min'." << std::endl;
		exit(EXIT_FAILURE);
	}
	
	if ((int) organism.numVariable() <= column || column < 0) {
		std::cerr << "Error: Given index is out of bonds." << std::endl;
		exit(EXIT_FAILURE);
	} else {
		column = (size_t) column;
	}

	for (size_t n = 0; n < data->frames(); ++n) {
		Matrix matrix = data->getFrame(n).getMatrix();
		
		Matrix init;
		for (size_t i = 0; i < matrix.size(); ++i) {
			Vector vector;
			for (size_t j = 0; j < organism.numVariable(); ++j) {
				vector.push_back(matrix[i][j]);
			}
			init.push_back(vector);
		}

		organism.setInit(init);
		organism.neighborhoodCreate(init);
		
		std::vector<size_t> tmpVector;
		for (size_t i = 0; i < organism.numCompartment(); ++i) {
			Compartment compartment = organism.compartment(i);
			
			size_t ci = compartment.index();

			double maxmin;
			switch (action) {
			case MAX:				
				maxmin = -std::numeric_limits<double>::max();
				break;
			case MIN:
				maxmin = +std::numeric_limits<double>::max();
				break;
			}
			
			for (size_t j = 0; j < compartment.numNeighbor(); ++j) {
				size_t cj = compartment.neighbor(j);
				double tmp = matrix[cj][column];
				switch (action) {
				case MAX:
					if (tmp > maxmin)
						maxmin = tmp;
					break;
				case MIN:
					if (tmp < maxmin)
						maxmin = tmp;
					break;
				}
			}
			switch (action) {
			case MAX:
				if (maxmin < matrix[ci][column])
					tmpVector.push_back(i);
				break;
			case MIN:
				if (maxmin > matrix[ci][column])
					tmpVector.push_back(i);
				break;
			}
		}
		peaks.push_back(tmpVector);
	}	

	Data *output = new Data;
	for (size_t n = 0; n < data->frames(); ++n) {
		Frame outputFrame;
		Frame frame = data->getFrame(n);
		Matrix outputMatrix;
		Matrix matrix = frame.getMatrix();
		for (size_t i = 0; i < peaks[n].size(); ++i) {
			std::vector<double> vector;
 			for (size_t j = 0; j < matrix[peaks[n][i]].size(); ++j) {
				double element = matrix[peaks[n][i]][j];
				vector.push_back(element);
			}
			outputMatrix.push_back(vector);
		}
		outputFrame.setMatrix(outputMatrix);
		outputFrame.setTime(frame.getTime());
		output->addFrame(outputFrame);				
	}

	return output;
}

void ExtractPeaksFromNeighborhood::printHelp(void)
{
	std::cout << "Action: extractPeaksFromNeighborhood\nArguments: <max/min> <model file> <index>" << std::endl
			<< std::endl
			<< "This action finds peaks considering the values of index <index>. It " 
			<< "uses the model file <model file> to get the neighborhood function used "
			<< "to determine next neighbors." << std::endl;
}

Range ExtractPeaksFromNeighborhood::arguments(void)
{
	Range range(3,3);
	return range;
}

