#include "identity.h"

using namespace action;

Data *Identity::process(Data *data, OutputMethod *outputMethod)
{
	// Do absolutely nothing, but create a copy of the data object.
	Data *copy = new Data(*data);
	return copy;
}
 
void Identity::printHelp(void)
{
	std::cout << "Action: identity" << std::endl
			<< "Arguments: none" << std::endl 
			<< std::endl
			<< "This action does nothing to the data. It can be used to convert data "
			<< "between different input/output methods." << std::endl
			<< std::endl;
}

Range Identity::arguments(void)
{
	Range range(0,0);
	return range;
}
