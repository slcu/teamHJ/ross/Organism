#ifndef ORGANISMDTK_ACTION_H
#define ORGANISMDTK_ACTION_H

#include <utility>
#include <limits>
#include "data.h"
#include "outputMethod.h"

namespace action
{
	typedef std::pair<int, int> Range;
	static const int NOLIMIT = std::numeric_limits<int>::max();

	// An action takes a pointer to a Data object and returns a
	// pointer to a new Data object.
	class Action
	{
	public:
		virtual ~Action();
		virtual Data *process(Data *data, OutputMethod *outputMethod = NULL);
		virtual void printHelp(void);
		virtual Range arguments(void);
		static Action *getAction(std::string name);
	};
}

#endif /* ORGANISMDTK_ACTION_H */
