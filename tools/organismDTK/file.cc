#include <fstream>
#include "file.h"
#include "src/myConfig.h"
#include <cstdlib>

namespace file
{
	void readFile(std::string fileName, Data *data, InputMethod *inputMethod)
	{
		std::ifstream file;
		file.open(fileName.c_str());
		if (!file) {
			std::cerr << "Error: Unable to open file " << fileName << " for input."  << std::endl;
			exit(EXIT_FAILURE);
		}
		data = inputMethod->readInput(file);
		file.close();
	}

	void writeFile(std::string fileName, Data *data, OutputMethod *outputMethod)
	{
		std::ofstream file;
		file.open(fileName.c_str());
		if (!file) {
			std::cerr << "Error: Unable to open file " << fileName << " for output."  << std::endl;
			exit(EXIT_FAILURE);
		}
		if (myConfig::getBooleanValue("precision")) {
			file.precision(atoi(myConfig::getValue("precision", 0).c_str()));
		}
		outputMethod->writeOutput(file, data);
		file.close();
	}
}
