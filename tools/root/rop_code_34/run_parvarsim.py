#! /usr/bin/env python
#
# Written by Bettina Greese, CBBP, Uni Lund, Sweden (2012-14).
#

################################################################################
### REQUIRED USER INPUT: ###
############################

# SET LIST OF ALLOWED VARIATIONS:
varTypeList = ['so', 'si', 'pqc', 'pK', 'pHc', 'pHw' \
               , 'pHc', 'pHw', 'Pa', 'Ppin', 'Paux', 'q' \
               , 'Da', 'Dw', 'Du', 'Dv', 'c', 'k1', 'k2', 'k3', 'k4' \
               , 'r', 's', 'b', 'so', 'si', 'facPinAux']
setParName = ''
setFacList = ["1.0"]
#setParName = 'so'
#setFacList = ["1.0", "10.0", "0.1"] # Use this with 'so' to check 3 regimes.
#setParName = 'Dv'
#setFacList = [1.0, 0.01] # Use this with 'Du' or 'Dv'.

# CHOOSE MODEL:
baseName = 'rop'
consSys = 0 # Signal (=1) for conservedSystem, i.e. no prod/degr of ROPs.

# CHOOSE EXTERNAL AUXIN TEMPLATE:
# b and e are indices of the first and last cell in x dir to receive auxin:
# s and p are strengths relative to the source and PIN values (in percent)
appTemplate = ''

# CHOOSE SECTIONS FOR WHICH CONCENTRATION CURVES WILL BE PLOTTED:
plotSecY = ['firstComp'] # firstComp, lastComp, firstWall, lastWall
plotSecX = []

# CHOOSE IF PREVIOUSLY STARTED SIMULATION SHOULD BE CONTINUED OR SKIPPED:
#simStrategy = 'continue'
simStrategy = 'skip'

# CHOOSE IF .data FILE SHOULD BE DELETED TO SAVE DISK SPACE:
doKeepDataFile = 0

# CHOOSE IF CONCENTRATIONS SHOULD BE SAVED AFTER EACH RUN:
doKeepConc = 1

################################################################################
### USER INPUT FROM ARGUMENTS: ###
##################################

# Import necessary modules:
import time, os, pickle, copy, sys
from toolbox import dirnavi, variables
from simfiles import parvalues, simsettings, varsettings
from execsim import auxinsim, extract, ropsim, ropsimpercell

# Get path to folder for simulation output and results:
outPath = simsettings.outpath()

# Construct project name:
projName = os.path.basename(__file__)
projName = projName[4:-6]

# Get default discretization and initial conditons:
[xyzNumCompDef, initCondDef] = simsettings.defaults()

# Get information from input arguments:
argList = sys.argv
[doRop, cellTemplate, auxinTemplate, varType, xyzNumComp, initCond] = variables.inputargs(argList)
if varType not in varTypeList:
    raise Exception('Given varType is not in varTypeList.')
print(varType, setParName)
raw_input()
if varType == setParName:
    raise Exception('Parameter for different situations can not be the varied parameter.')
if xyzNumComp == []:
    xyzNumComp = map(int, xyzNumCompDef)
del(xyzNumCompDef)
if initCond == []:
    initCond = map(float, initCondDef)
del(initCondDef)

################################################################################

# Print user input for checking:
print(baseName, consSys, cellTemplate, auxinTemplate, appTemplate, xyzNumComp, \
      initCond, varType, simStrategy)
raw_input('Check settings and press any key to continue:')

# Start timer:
startTime = time.time()

# Get code version number:
vNrStr = dirnavi.codeversion()

# Collect information about template into a dictionary:
infoDict = variables.templinfo(baseName, vNrStr, cellTemplate, auxinTemplate, \
                               appTemplate, xyzNumComp, initCond, consSys)
del(initCond)

# Get parameter values:
origParDict = parvalues.setallpar(infoDict)

# Set names of directories for different levels:
origLevelList = dirnavi.dirlevel(infoDict, origParDict)

# Change directory:
os.chdir(outPath)
dirnavi.mkchdir(origLevelList[0])
dirnavi.mkchdir(origLevelList[1])
dirnavi.mkchdir(origLevelList[2])

# Check if pickled parDict from template exists (as example):
pickleName = 'template/' + baseName + '_parDict.pkl'
if os.path.exists(pickleName) == False:
    raise Exception('No variable with parameters found, make template first ' \
    + '(use run_simfiles.py).')
del(pickleName)

# Update parDict regarding tolerance for stopping:
simsettings.tolerance(origParDict, infoDict['cellTemplate'])

# Add plot settings to parDict:
origParDict['plotSecX'] = ["sections in x dir for plots", plotSecX]
origParDict['plotSecY'] = ["sections in y dir for plots", plotSecY]
del(plotSecX, plotSecY)

# Loop over settings for which chosen parameter is varied:
for currSetFac in setFacList:
    
    # Preallocate list for path names (for auxin and rop simulations):
    pathList = [[], []]
    
    # Get factor list for current parameter:
    varFacList = varsettings.factors(varType)
    
    # Loop over factors for chosen parameter value:
    for currVarFac in varFacList:
            
        # Copy original parameter values:
        currParDict = copy.deepcopy(origParDict)
        
        # Update current dictionary with parameter values:
        currParDict[varType][1] = currVarFac*currParDict[varType][1]

        if setParName != '':
            currParDict[setParName][1] = currSetFac*currParDict[setParName][1]
            print 
            print('\n' + setParName + ': ' + str(currParDict[setParName][1]) + '\n')
        del(currVarFac)
        print('\n' + varType + ': ' + str(currParDict[varType][1]) + '\n')
        #raw_input()
        
        # Set names of directories for different levels:
        levelList = dirnavi.dirlevel(infoDict, currParDict)
        
        # Check for existing auxin simulation directory (levels 3 and 4):
        doSim = dirnavi.skipdir('auxin', levelList, simStrategy)
        
        # Change directory:
        dirnavi.mkchdir(levelList[3])
        dirnavi.mkchdir(levelList[4])
        
        if doSim == 1:
            # Pickle currParDict (i.e. save as a variable):
            pickleName = baseName + '_parDict.pkl'
            fileObj = open(pickleName, 'w')
            pickle.dump(currParDict, fileObj)
            fileObj.close()
            print("Saved file " + pickleName)
            del(pickleName, fileObj)
            
            # Save list of parameter values as text:
            variables.partext(baseName, currParDict)
            
            # Run simulation for auxin:
            auxinsim.simulate(baseName, currParDict, levelList, doKeepDataFile, doKeepConc)
            
        elif doSim == 0:
            if os.path.exists(baseName + '.final') == True \
                and os.path.exists(baseName + '.sums') == False \
                and ('single' in levelList[1]) == False \
                and ('file' in levelList[1]) == False:
                extract.summass(baseName, currParDict)
        del(doSim)
        
        # Save path to auxin simulation directory:
        currPath = dirnavi.simpath('auxin', levelList)
        pathList[0].append(currPath)
        del(currPath)
        
        if doRop == 1:
            # Change directory:
            dirnavi.mkchdir('../' + levelList[7])
            
            # Check for existing ROP simulation directory (levels 8 and 9):
            doSim = dirnavi.skipdir('rop', levelList, simStrategy)
            
            # Change directory:
            dirnavi.mkchdir(levelList[8])
            dirnavi.mkchdir(levelList[9])
            
            if doSim == 1:
                # Pickle parDict (i.e. save as a variable):
                pickleName = baseName + '_parDict.pkl'
                fileObj = open(pickleName, 'w')
                pickle.dump(currParDict, fileObj)
                fileObj.close()
                print("Saved file " + pickleName)
                del(pickleName, fileObj)

                # Save list of parameter values as text:
                variables.partext(baseName, currParDict)

                # Run ROP simulation per cell:
                ropsim.simulate(baseName, currParDict, levelList, \
                                doKeepDataFile, doKeepConc)
                '''
                # If stopping criteria are not the same for all cells, use the following:
                if 'single' in levelList[1]:
                    # Run ROP simulation for complete template:
                    ropsim.simulate(baseName, currParDict, levelList, \
                                    doKeepDataFile, doKeepConc)
                else:
                    # Run ROP simulation per cell:
                    ropsimpercell.simulate(baseName, currParDict, levelList, \
                                           doKeepDataFile)
                '''
            
            del(doSim)
            
            # Save path to ROP simulation directory:
            currPath = dirnavi.simpath('rop', levelList)
            pathList[1].append(currPath)
            del(currPath)

            # Change directory:
            os.chdir('../../')
        
        del(levelList, currParDict)
        
        # Change directory:
        os.chdir('../..')
    
    del(varFacList)
    
    # Construct project name:
    currProjName = projName + '_' + varType
    
    # Change directory:
    dirnavi.mkchdir('../../../results' + vNrStr + '/')
    dirnavi.mkchdir(currProjName)
    
    # Check number of previous files of given type:
    maxNum = dirnavi.numfiles('.txt', varType, cellTemplate, \
                              auxinTemplate, xyzNumComp)
    
    # Set name for current run of current project:
    countName = cellTemplate + '_' + auxinTemplate + appTemplate + '_' \
        + str(xyzNumComp[0]) + '_' + str(xyzNumComp[1]) + '_' \
        + str(xyzNumComp[2]) + '_' + currProjName + '_' + str(maxNum+1)
    del(currProjName, maxNum)
    
    # Save list with paths to simulation directories as text:
    fileName = countName + '.txt'
    fileObj = open(fileName, 'w')
    for simPhase in [0, 1]:
        for currPath in pathList[simPhase]:
            if currPath != '':
                fileObj.write(currPath + '\n')
    fileObj.close()
    print('Saved file ' + fileName)
    del(countName, fileName, fileObj, pathList)
    
    # Change directory:
    os.chdir('../../' + origLevelList[0] + origLevelList[1] + origLevelList[2])
    if setParName != '':
        raw_input()

del(varType, vNrStr, infoDict, origParDict, projName, origLevelList)
del(cellTemplate, auxinTemplate, appTemplate, xyzNumComp, consSys)
del(setParName, setFacList)

# Calculate run time:
endTime = time.time()
runTime = endTime - startTime
del(startTime, endTime)
print('Run time was ' + str(runTime/3600) + ' hours')
del(runTime)


