#! /usr/bin/env python
# Script that provides the latex template for template of ROP model.
#
# Written by Bettina Greese, CBBP, Uni Lund, Sweden (2013).
#

################################################################################

def latexbeg(commonPath, parDict, cellTemplate, auxinTemplate, appTemplate):
    '''Generates the beginning of the .tex file to display results.
    
    Written by Bettina Greese, CBBP, Uni Lund, Sweden (2012).'''
    
    from toolbox import dirnavi 
    
    # Program control:
    doCartoons = 0 # EDIT if desired...
    
    # Get lists of parameter names:
    parNameDict = dirnavi.getparnames()
    
    text = """\documentclass[11pt, a4paper, landscape]{article}

%%%%%% Graphics %%%%%%%
\usepackage[dvips]{epsfig} % old, should be replaced
\usepackage[font=small, width=\\textwidth]{caption}[2008/04/01]
\usepackage[labelformat=simple]{subcaption}
\\renewcommand{\\thesubfigure}{(\\alph{subfigure})} % put round brackets around subfigure label
\graphicspath{{../../../../../../Documentation/Cartoons/}, {""" + commonPath + """}}

%%%%%% Layout %%%%%%%

% new page layout:
\\renewcommand{\\topfraction}{1} % 100% of page top can be a float
\\renewcommand{\\bottomfraction}{1} % 100% of page bottom...
\\renewcommand{\\textfraction}{0} % only 0% of page must be text
%\\renewcommand{\\baselinestretch}{1.2} % line spacing
\linespread{1.2}
\setlength{\headheight}{14pt}

% page margins:
\usepackage[margin = 1.6 cm]{geometry}
%\usepackage[inner = 3.5 cm, outer = 3 cm, top = 2.5 cm, bottom = 2.5 cm]{geometry}

%fancy header and footer:
\usepackage{fancyhdr} % for pagestyle
\pagestyle{fancy}
\\fancyhead{} % delete header
\lhead{Bettina}
\\rhead{\\today}

\\fancyfoot{} % delete footer
\\rfoot{\\thepage}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\\begin{document}
"""
    
    if doCartoons == 1:
        text = text + """
\section{ROP dynamics in a single cell and auxin dynamics in a cell file}

\\vspace{0.2cm}

\includegraphics[height = 3 cm]{ropDynSingle2c.eps}
\hspace{1cm}
\includegraphics[height = 3 cm]{auxinDynMulti1d.eps}
"""
    
    text = text + '''
\section{Default parameter values for cell template "''' + cellTemplate \
+ '" and auxin template "' + auxinTemplate +'''"}
'''
    
    for listInd in [0, 1, 2, 3]:
        if listInd == 0:
            currList = parNameDict['auxin']
            currName = 'auxin-related parameters'
            textWidth = 1.0
        elif listInd == 1:
            currList = parNameDict['rop']
            currName = 'ROP-related parameters'
            textWidth = 1.0
        elif listInd == 2:
            currList = parNameDict['sim']
            currName = 'simulation parameters'
            textWidth = 1.0
        elif listInd == 3:
            currList = parNameDict['templ']
            currName = 'cell template'
            textWidth = 1.0
        
        text = text + """
\\begin{minipage}{""" + str(textWidth) + """ \\textwidth}

    Default values for """ + currName + """:
    
    \\vspace{0.3 cm}
"""
        del(textWidth)

        if listInd in [0, 1, 2]:
            text = text + """
    \\begin{tabular}{l"""
            for ind in range(len(currList)):
                text = text + "|l"
            del(ind)
            text = text + """}
        par"""
            for parName in currList:
                if parName != 'lengths' and parName != 'widths':
                    text = text + " & $" + parName + "$"
            del(parName)
            text = text + """ \\\\
        \hline
        val"""
            for parName in currList:
                if parName != 'lengths' and parName != 'widths':
                    text = text + " & $" + str(parDict[parName][1]) + "$"
            del(parName)
            text = text + """
    \end{tabular}
    
    \\vspace{0.7 cm}
    """
        elif listInd == 3:
            text = text + """
    \\begin{tabular}{l|l}
        par & val \\\\
        \hline
        """
            for parName in currList:
                if parName in ['pin', 'aux']:
                    numE = 6
                elif parName in ['source', 'sink', 'prod']:
                    numE = 5
                else:
                    numE = 8
                text = text + "$" + parName + "$ & "
                currPar = parDict[parName][1]
                if isinstance(currPar, list) and len(currPar) >= numE:
                    numB = len(parDict[parName][1])/numE
                    for ind1 in range(0, numB):
                        text = text + "$["
                        currParPart = currPar[ind1*numE:(ind1+1)*numE]
                        for ind2 in range(len(currParPart)):
                            text = text + str(currParPart[ind2]) + ", "
                        del(ind2, currParPart)
                        text = text[:-2] + "]$ "
                        if ind1 % 2 == 1 and ind1 != numB - 1:
                            text = text + "\\\\ $ $ & "
                    del(ind1)
                else:
                    text = text + "$" + str(currPar) + "$ "
                del(currPar)
                if parName != currList[-1]:
                    text = text + """\\\\
        """
            del(parName)
            
            text = text + """
    \end{tabular}
    
    \\vspace{0.5 cm}
    
    """
        text = text + """    
\end{minipage}"""
    
    text = text + """

\clearpage

"""
    
    return text

################################################################################

def latexmeanpcps(baseName, filePath, partTitle, substList, plotY, plotX):
    '''Generates section of .tex file for mean and slope of conc per cell.
    
    Written by Bettina Greese, CBBP, Uni Lund, Sweden (2015).'''
    
    # Settings:
    widthStr = '\\textwidth'
    
    text = ""
    
    # Edit partTitle if it contains underscores:
    if '_' in partTitle:
        partTitle = partTitle.replace('_', ' ')
    
    # Loop over different substances:
    for subst in substList:
        text = text + """
\\begin{minipage}[c]{""" + widthStr + """}
    \subsection{Plot of mean """ + subst + " per cell for " \
+ partTitle + """:}
    \includegraphics[width = \\textwidth]{""" + filePath + \
"_meanpc_final_" + subst + """_level.eps}
\end{minipage}
"""
        if subst == 'auxin':
            text = text + """
\\begin{minipage}[c]{""" + widthStr + """}
    \subsection{Plot of slope for """ + subst + " per cell for " \
+ partTitle + """:}
    \includegraphics[width = \\textwidth]{""" + filePath + \
"_meanpc_final_" + subst + """_slope.eps}
\end{minipage}
"""
            text = text + """
\\begin{minipage}[c]{""" + widthStr + """}
    \subsection{Plot of normalized slope for """ + subst + " per cell for " \
+ partTitle + """:}
    \includegraphics[width = \\textwidth]{""" + filePath + \
"_meanpc_final_" + subst + """_normslope.eps}
\end{minipage}
"""
    del(filePath)
    
    return text

################################################################################

def latexpervar(baseName, filePath, partTitle, plotY, zoomList):
    '''Generates one section of the .tex file with curves per variation.
    
    Written by Bettina Greese, CBBP, Uni Lund, Sweden (2012-15).'''
    
    # Settings:
    widthStr = '\\textwidth'
    
    text = ""
    
    for zoom in zoomList:
        for currY in plotY:
            if len(plotY) == 1:
                yStr = ''
            else:
                yStr = str(currY) + 'y_'
            text = text + """
\\begin{minipage}[c]{""" + widthStr + """}
    \subsection{Plot of all substances in root section for """ \
+ partTitle + " (y = " + str(currY) + " dm, zoom = " + str(zoom) + """ dm):}
    \includegraphics[width = \\textwidth]{""" + filePath + baseName \
+ '_curves_final_allsubst_' + yStr + str(zoom) + """dm.eps}
\end{minipage}
\\vspace{0.7cm}
"""
            del(currY)
        del(zoom)
    del(filePath)
    
    return text

################################################################################

def latexpersubst(baseName, filePath, partTitle, substList, zoomList):
    '''Generates one section of the .tex file with curves per substance.
    
    Written by Bettina Greese, CBBP, Uni Lund, Sweden (2012-15).'''
    
    # Settings:
    widthStr = '\\textwidth'
    
    text = ""
    
    for zoom in zoomList:
        for subst in substList:
            text = text + """
\\begin{minipage}[c]{""" + widthStr + """}
    \subsection{Plot of """ + subst + """ in root section for """ \
+ partTitle + " (zoom = " + str(zoom) + """ dm):}
    \includegraphics[width = \\textwidth]{""" + filePath + baseName \
+ '_curves_final_' + subst + '_' + str(zoom) + """dm.eps}
\end{minipage}
\\vspace{0.7cm}
"""
            del(subst)
        del(zoom)
    del(filePath)
    
    return text

################################################################################

def latextime(filePath, partTitle, substList):
    '''Generates one section of the .tex file with time curves.
    
    Written by Bettina Greese, CBBP, Uni Lund, Sweden (2015).'''
    
    # Settings:
    widthStr = '\\textwidth'
    
    text = ""
    
    for subst in substList:
        text = text + """
\\begin{minipage}[c]{""" + widthStr + """}
    \subsection{Plot of """ + subst + " for " + partTitle + """:}
    \includegraphics[width = \\textwidth]{""" + filePath \
+ subst + """_conc_time.eps}
\end{minipage}
\\vspace{0.7cm}
"""
        del(subst)
    del(filePath, substList)
    
    return text

################################################################################

def latexsec(baseName, filePath, partTitle, substList, plotY, plotX, \
             zoomList, doCurves, doMaps):
    '''Generates one section of the .tex file to display results.
    
    Written by Bettina Greese, CBBP, Uni Lund, Sweden (2012-14).'''
    
    # Settings:
    widthStr = '\\textwidth'
    
    text = ""
    
    for zoom in zoomList:
        
        if doMaps == 1 and substList != ['allsubst']:
            # Loop over different substances to be plotted:
            for subst in substList:
                text = text + """
\\begin{minipage}[c]{""" + widthStr + """}
    \subsection{Plot of """ + subst + " for " + partTitle + """:}
    \\vspace{-1.5cm}
    \includegraphics[height = \\textwidth, angle = -90]{""" + filePath + \
    baseName + "_final_map_" + subst + '_' + str(zoom) + """dm.eps}
\end{minipage}
"""
        
        if doCurves == 1:
            subst = 'auxin'
            text = text + """
\\begin{minipage}[c]{""" + widthStr + """}
    \subsection{Plot of """ + subst + """ in root section for """ \
+ partTitle + " (zoom = " + str(zoom) + """ dm):}
    \includegraphics[width = \\textwidth]{""" + filePath + baseName \
+ '_curves_final_' + subst + '_' + str(zoom) + """dm.eps}
\end{minipage}
\\vspace{0.7cm}
"""
            del(subst)
            
            for currY in plotY:
                for subst in substList:
                    text = text + """
\\begin{minipage}[c]{""" + widthStr + """}
    \subsection{Plot of """ + subst + """ in root section for """ \
+ partTitle + " (y = " + str(currY) + " dm, zoom = " + str(zoom) + """ dm):}
    \includegraphics[width = \\textwidth]{""" + filePath + baseName \
+ '_curves_final_' + subst + '_' + str(currY) + 'y_' + str(zoom) + """dm.eps}
\end{minipage}
\\vspace{0.7cm}
"""
                    del(subst)
                del(currY)
            
            # Loop over diff x val for which curves of all subst are plotted:
            for currX in plotX: # Loop over x values for which curves are plotted.
                text = text + """
\\begin{minipage}[c]{""" + widthStr + """}
    \subsection{Plot of root section through x = """ + str(currX) + " for " \
+ partTitle + """:}
    \includegraphics[width = \\textwidth]{""" + filePath + baseName + "_final_curve_" + str(zoom) + 'dm_' + str(currX) + """x.eps}
\end{minipage}
\\vspace{1cm}
"""
    
    del(filePath)
    
    return text

################################################################################

def latexderiv(baseName, filePath, partTitle, substList):
    '''Generates one section of the .tex file to display max absolute derivatives.
    
    Written by Bettina Greese, CBBP, Uni Lund, Sweden (2015).'''
    
    # Settings:
    widthStr = '\\textwidth'
    
    text = ""
    
    # Loop over different substances:
    for subst in substList:
        if subst != 'ropi':
            text = text + """
\\begin{minipage}[c]{""" + widthStr + """}
    \subsection{Plot of maximal absolute derivative of """ + subst + " for " \
+ partTitle + """:}
    \includegraphics[width = \\textwidth]{""" + filePath + \
baseName + """_max_deriv.eps}
\end{minipage}
"""
    del(filePath, partTitle, substList)
    
    return text

################################################################################

def latexmeanit(countName):
    '''Generates section of .tex file with means in whole template.
    
    Written by Bettina Greese, CBBP, Uni Lund, Sweden (2015).'''
    
    from plotfiles import forplots
    
    # Settings:
    widthStr = '\\textwidth'
    
    text = ''
    
    # Include plot with comparison of mean auxin concentrations:
    plotName = countName + '_mean_final_auxin.eps'
    text = text + """
\\begin{minipage}[c]{""" + widthStr + """}
\subsection{Comparison of mean auxin in template:}
\includegraphics[width = \\textwidth]{""" + plotName + """}
\end{minipage}
\\vspace{1cm}
"""
    del(plotName, countName, widthStr)
    
    return text

################################################################################

def latexcomp(baseName, simPhaseList, parDict, projName, countName, doCurves, \
              doSecGrad, doSecGradDiff, doTotal):
    '''Generates final section with comparisons for .tex file to display results.
    
    Written by Bettina Greese, CBBP, Uni Lund, Sweden (2012-15).'''
    
    from plotfiles import forplots
    
    # Settings:
    widthStr = '\\textwidth'
    
    text = ''
    
    if doCurves == 1:
        # Include plots with comparison of curves:
        legendList = forplots.layernames(parDict, 'long')
        
        # Loop over simulation phases:
        plotY = parDict['plotY'][1]
        for simPhase in simPhaseList:
            if simPhase == 'A':
                substList = ['auxin']
            elif simPhase == 'R':
                substList = ['ropa', 'ropi']
            
            # Loop over zoom values:
            for zoom in parDict['zoomList'][1]:
                
                # Loop over diff y values for which comparisons are plotted:
                for yInd in range(0, len(plotY)):
                    currY = plotY[yInd]
                    legEntry = legendList[yInd]
                    if simPhase == 'A' or (simPhase == 'R' and 'wall' not in legEntry):
                        # Plot:
                        for subst in substList:
                            plotName = projName + '_curves_final_' + subst \
                                + '_' + str(currY) + 'y_' + str(zoom) + 'dm.eps'
                            text = text + """
\\begin{minipage}[c]{""" + widthStr + """}
    \subsection{Comparison of """ + subst + " for " + legEntry \
    + " (" + str(currY) + """ dm):}
    \includegraphics[width = \\textwidth]{""" + plotName + """}
\end{minipage}
\\vspace{1cm}
"""
                            del(plotName)
                        del(subst, currY, legEntry)
                del(yInd)
            del(substList, zoom)
        del(simPhase)
    
    if doSecGrad == 1:
        # Include plot with comparison of large-scale gradients:
        plotName = countName + '_secgrad_final_auxin.eps'
        text = text + """
\\begin{minipage}[c]{""" + widthStr + """}
    \subsection{Comparison of auxin gradients:}
    \includegraphics[width = \\textwidth]{""" + plotName + """}
\end{minipage}
\\vspace{1cm}
"""
        del(plotName)
    
    if doSecGradDiff == 1:
        # Include plot with differences in comparison:
        plotName = countName + '_graddiff_final_auxin.eps'
        text = text + """
\\begin{minipage}[c]{""" + widthStr + """}
    \subsection{Differences between simulated and measured auxin:}
    \includegraphics[width = \\textwidth]{""" + plotName + """}
\end{minipage}
\\vspace{1cm}
"""
        del(plotName)

    if doTotal == 1:
        # Include plot with comparison of total auxin masses:
        plotName = countName + '_total_final_auxin.eps'
        text = text + """
\\begin{minipage}[c]{""" + widthStr + """}
    \subsection{Comparison of total auxin:}
    \includegraphics[width = \\textwidth]{""" + plotName + """}
\end{minipage}
\\vspace{1cm}
"""
        del(plotName)
    
    del(countName, widthStr)
    
    return text

################################################################################

def latexend():
    '''Generates end of the .tex file to display results.
    
    Written by Bettina Greese, CBBP, Uni Lund, Sweden (2012-14).'''
    
    text = "\n\end{document}\n\n"
    
    return text

################################################################################

