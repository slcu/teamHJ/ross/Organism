#! /usr/bin/env python
#
# Written by Bettina Greese, CBBP, Uni Lund, Sweden (2014-15).
#

################################################################################
### REQUIRED USER INPUT: ###
############################

# SET LIST OF ALLOWED VARIATIONS:
varTypeList = ['auxin', 'roptotal', 'ropafrac']

# CHOOSE MODEL:
baseName = 'rop'
consSys = 0 # Signal (=1) for conservedSystem, i.e. no prod/degr of ROPs.

# CHOOSE EXTERNAL AUXIN TEMPLATE:
# b and e are indices of the first and last cell in x dir to receive auxin:
# s and p are strengths relative to the source and PIN values (in percent)
appTemplate = ''

# CHOOSE IF PREVIOUSLY STARTED SIMULATION SHOULD BE CONTINUED OR SKIPPED:
#simStrategy = 'continue'
simStrategy = 'skip'

# CHOOSE IF .data FILE SHOULD BE DELETED TO SAVE DISK SPACE:
doKeepDataFile = 0

# CHOOSE IF CONCENTRATIONS SHOULD BE SAVED AFTER EACH RUN:
doKeepConc = 1

################################################################################
### USER INPUT FROM ARGUMENTS: ###
##################################

# Import necessary modules:
import time, os, pickle, copy, sys
from toolbox import dirnavi, variables
from simfiles import parvalues, simsettings, varsettings
from execsim import auxinsim, extract, ropsim

# Get path to folder for simulation output and results:
outPath = simsettings.outpath()

# Construct project name:
projName = os.path.basename(__file__)
projName = projName[4:-6]

# Get default discretization and initial conditons:
[xyzNumCompDef, initCondDef] = simsettings.defaults()
del(initCondDef)

# Get information from input arguments:
argList = sys.argv
[doRop, cellTemplate, auxinTemplate, varType, xyzNumComp, initCond] = variables.inputargs(argList)
del(initCond)
if xyzNumComp == []:
    xyzNumComp = map(int, xyzNumCompDef)
del(xyzNumCompDef)
if varType not in varTypeList:
    raise Exception('Given varType is not in varTypeList.')
del(varTypeList, argList)

# CHOOSE INITIAL CONDITIONS TO BE COMPARED (all possible combinations):
if varType == 'auxin': # Initial value of auxin.
    auxinInitList = [0.0, 0.1, 0.2] # List of floats.
    ropTotalList = [0.0] # One float!
    ropaFracList = [0.0] # One float!
elif varType == 'roptotal': # Initial total amount of ROP.
    auxinInitList = [0.0] # One float!
    #ropTotalList = [0.0, 1.0, 2.0, 3.0, 4.0, 5.0, 10.0] # List of floats.
    ropTotalList = [0.0, 5.0, 10.0] # List of floats.
    ropaFracList = [0.0] # One float!
    #ropaFracList = [1.0] # One float!
elif varType == 'ropafrac': # Initial fraction of ropa in total rop.
    auxinInitList = [0.0] # One float!
    #ropTotalList = [1.0] # One float!
    ropTotalList = [10.0] # One float!
    ropaFracList = [0.0, 0.25, 0.5, 0.75, 1.0] # List of floats.
if len(auxinInitList) + len(ropTotalList) + len(ropaFracList) < 4:
    raise Exception('At least two experiments are needed for comparison.')

################################################################################

# Print user input for checking:
print(baseName, consSys, cellTemplate, auxinTemplate, appTemplate, xyzNumComp, \
      varType, auxinInitList, ropTotalList, ropaFracList, simStrategy)
raw_input('Check settings and press any key to continue:')

# Start timer:
startTime = time.time()

# Get code version number:
vNrStr = dirnavi.codeversion()

# Change directory:
dirnavi.mkchdir(outPath)
del(outPath)

# Loop over initial conditions for auxin:
pathList = [[],[]] # Initialize list for paths to simulation directories.
for auxinInit in auxinInitList:
    for ropTotal in ropTotalList:
        for ropaFrac in ropaFracList:
            
            # Calculate ropaInit and ropiInit:
            ropaInit = ropaFrac*ropTotal
            ropiInit = (1 - ropaFrac)*ropTotal
            if ropaInit + ropiInit != ropTotal:
                raise Exception('Error in calculation of initial ropa and ropi.')
            if consSys == 1 and ropaInit == 0.0 and ropiInit == 0.0:
                continue
            
            # Get initial values:
            initCond = [auxinInit, ropaInit, ropiInit]
            del(ropaFrac, ropaInit, ropiInit)
            
            # Collect information about template into a dictionary:
            infoDict = variables.templinfo(baseName, vNrStr, cellTemplate, \
                       auxinTemplate, appTemplate, xyzNumComp, initCond, consSys)
            
            # Get parameter values:
            parDict = parvalues.setallpar(infoDict)
            
            # Set names of directories for different levels:
            levelList = dirnavi.dirlevel(infoDict, parDict)
            
            # Update parDict regarding tolerance for stopping:
            simsettings.tolerance(parDict, infoDict['cellTemplate'])
            del(infoDict)
            
            # Change directory:
            dirnavi.mkchdir(levelList[0])
            dirnavi.mkchdir(levelList[1])
            dirnavi.mkchdir(levelList[2])
            
            # Check if template directory exists:
            fileName = 'template'
            if os.path.exists(fileName) == False:
                print(initCond)
                raise Exception('No template found, make template first.')
            del(initCond)
            
            # Check for existing auxin simulation directory (levels 3 and 4):
            doSim = dirnavi.skipdir('auxin', levelList, simStrategy)
            
            # Change directory:
            dirnavi.mkchdir(levelList[3])
            dirnavi.mkchdir(levelList[4])
            
            if doSim == 1:
                # Pickle parDict (i.e. save as a variable):
                pickleName = baseName + '_parDict.pkl'
                fileObj = open(pickleName, 'w')
                pickle.dump(parDict, fileObj)
                fileObj.close()
                print("Saved file " + pickleName)
                del(pickleName, fileObj)
                
                # Save list of parameter values as text:
                variables.partext(baseName, parDict)
                
                # Run simulation for auxin:
                auxinsim.simulate(baseName, parDict, levelList, \
                                  doKeepDataFile, doKeepConc)
            
            del(doSim)
            
            # Save path to auxin simulation directory:
            currPath = dirnavi.simpath('auxin', levelList)
            pathList[0].append(currPath)
            del(currPath)
            
            if doRop == 1:
                # Change directory:
                dirnavi.mkchdir('../' + levelList[7])
                
                # Check for existing ROP simulation directory (levels 8 and 9):
                doSim = dirnavi.skipdir('rop', levelList, simStrategy)
                
                # Change directory:
                dirnavi.mkchdir(levelList[8])
                dirnavi.mkchdir(levelList[9])
                
                if doSim == 1:
                    # Pickle parDict (i.e. save as a variable):
                    pickleName = baseName + '_parDict.pkl'
                    fileObj = open(pickleName, 'w')
                    pickle.dump(parDict, fileObj)
                    fileObj.close()
                    print("Saved file " + pickleName)
                    del(pickleName, fileObj)

                    # Save list of parameter values as text:
                    variables.partext(baseName, parDict)
                    
                    # Run simulation for ROP:
                    ropsim.simulate(baseName, parDict, levelList, \
                                    doKeepDataFile, doKeepConc)
                
                del(doSim)

                # Save path to ROP simulation directory:
                currPath = dirnavi.simpath('rop', levelList)
                pathList[1].append(currPath)
                del(currPath)

                # Change directory:
                os.chdir('../../')
            
            del(levelList, parDict)
            
            # Change directory:
            os.chdir('../../../../..')
            
        del(ropTotal)
    del(auxinInit)
del(auxinInitList, ropTotalList, ropaFracList)

# Construct project name:
projName = projName + '_' + varType

# Change directory:
dirnavi.mkchdir('results' + vNrStr + '/')
dirnavi.mkchdir(projName)
del(vNrStr)

# Check number of previous files of given type:
maxNum = dirnavi.numfiles('.txt', projName, cellTemplate, \
                          auxinTemplate, xyzNumComp)

# Set name for current run of current project:
countName = cellTemplate + '_' + auxinTemplate + appTemplate + '_' \
    + str(xyzNumComp[0]) + '_' + str(xyzNumComp[1]) + '_' \
    + str(xyzNumComp[2]) + '_' + projName + '_' + str(maxNum+1)
del(cellTemplate, auxinTemplate, appTemplate, xyzNumComp, consSys)
del(projName, maxNum)

# Save list with paths to simulation directories as text:
fileName = countName + '.txt'
fileObj = open(fileName, 'w')
for simPhase in [0, 1]:
    for currPath in pathList[simPhase]:
        if currPath != '':
            fileObj.write(currPath + '\n')
fileObj.close()
print('Saved file ' + fileName)
del(countName, fileName, fileObj, pathList)

# Calculate run time:
endTime = time.time()
runTime = endTime - startTime
print('Run time was ' + str(runTime/3600) + ' hours')
del(startTime, endTime)
