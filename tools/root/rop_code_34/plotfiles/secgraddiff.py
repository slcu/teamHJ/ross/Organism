#! /usr/bin/env python
#
# Written by Bettina Greese, CBBP, Uni Lund, Sweden (2014).
#

################################################################################

def gnuplot(baseName, origParDict, pathList, projName, countName, fileType):
    '''Make plots of large-scale gradients for comparison of different situations.
    
    Written by Bettina Greese, CBBP, Uni Lund, Sweden (2014).'''
    
    import subprocess, os, numpy, pickle
    from toolbox import dirnavi
    
    # Program control:
    scaleType = 'lin' # Choose 'lin' or 'log'.
    
    # Set weighting factor (between 0 and 1) for gradient in relation to total:
    weighFac = 0.5
    
    # Get code version number:
    vNrStr = dirnavi.codeversion()
    
    # Change directory:
    os.chdir('../../')
    
    # Initialize comparison array for differences:
    numSecs = origParDict['numCuts'][1] - 1
    numVar = len(pathList[0])
    compArray = numpy.zeros(shape = (numVar, 4))
    
    # Set x range:
    xAxisBeg = -0.5
    xAxisEnd = numVar - 0.5
    
    # Obtain experimentally measured mass (gradient means and total):
    gradWt = numpy.array(origParDict['gradWt'][1])
    del(origParDict)
    totalWt = sum(gradWt)
    
    # Loop over paths to auxin simulations:
    for pInd in range(0, numVar):
        currPath = pathList[0][pInd]
        
        # Change directory:
        os.chdir(currPath)
        del(currPath)
        
        # Unpickle parDict:
        pickleName = baseName + '_parDict.pkl'
        fileObj = open(pickleName, 'r')
        parDict = pickle.load(fileObj)
        fileObj.close()
        print("Loaded file " + pickleName)
        del(pickleName, fileObj)
        
        # Get value of current variation and put into comparison array:
        if 'parvar' in projName \
        or 'stopcrit' in projName:
            parName = projName[projName.rfind('_') + 1:]
            compArray[pInd, 0] = parDict[parName][1]
            xAxisLabel = 'value of parameter ' + parName
            del(parName)
        else:
            raise Exception('Not implemented for chosen project.')
        
        # Get .sum data as array:
        auxinSumList = numpy.loadtxt(baseName + '.sums')
        
        # Extract simulated mass (gradient and total):
        gradSim = auxinSumList[0:numSecs]
        totalSim = auxinSumList[numSecs]
        del(auxinSumList)
        
        # Calculate root mean square deviation between simulated and measured:
        gradDiff = numpy.sqrt(numpy.mean((gradWt - gradSim)**2))
        totalDiff = abs(totalWt - totalSim)
        del(gradSim, totalSim)
        
        # Add differences to array:
        compArray[pInd, 1] = gradDiff
        compArray[pInd, 2] = totalDiff
        compArray[pInd, 3] = weighFac*gradDiff + (1-weighFac)*totalDiff
        del(gradDiff, totalDiff)
        
        # Change directory:
        os.chdir('../../../../..')
    
    del(pInd, pathList, numSecs, numVar, weighFac, gradWt, totalWt)
    
    # Change directory:
    os.chdir('results' + vNrStr + '/' + projName)
    del(vNrStr, projName)
    
    # Save values of variations (from comparison array) as x tics:
    xTicList = list(compArray[:, 0])
    xTicStr = '('
    for xtInd in range(0, len(xTicList)):
        xTicStr = xTicStr + '"' + str(xTicList[xtInd]) + '" ' + str(xtInd) + ", "
    xTicStr = xTicStr[:-2] + ')'
    del(xTicList)
    
    # Save array as text in .plot file:
    plotName = countName + '_graddiff_final_auxin.plot'
    numpy.savetxt(plotName, compArray)
    print("Saved file " + plotName)
    del(compArray, countName)
    
    # Settings for plot:
    if fileType == '.eps':
        terminalStr = 'set term post enhanced color ' \
                      'portrait size 26 cm, 8 cm'
    elif fileType == '.png':
        terminalStr = 'set term pngcairo enhanced color ' \
                      'size 26 cm, 8 cm'
    if scaleType == 'lin':
        scaleText = '\n### Plot with linear scale ###\n'
        yAxisBeg = 0
        yAxisEnd = 1
    elif scaleType == 'log':
        scaleText = '\n### Plot with logarithmic scale ###\n\n' \
                    + 'set logscale y\n'
        yAxisBeg = 0.1
        yAxisEnd = 1
    else:
        raise Exception("Set scaleType to 'lin' or 'log'.")
    del(scaleType)

    # Make .gnplt file:
    text = '''# File for plotting gradient differences using gnuplot.
#
# Written by Bettina Greese, CBBP, Uni Lund, Sweden (2014).
#
# Specify input data:
dataFileIn = "''' + plotName + '"\n\n' + terminalStr + '''\n
dataFileOut = "''' + plotName[:-5] + fileType + '''"
set output dataFileOut # Set name of output file.
set key horizontal outside center top # Settings for legend.
set size ratio 0.2 #  Set ratio of plot height to width.
set xtics ''' + xTicStr + '''
set ytics out mirror # Put y tic marks on both sides.

# Plot summed masses over tissue section (i.e. large-scale gradient):
set xl "''' + xAxisLabel + '''"
set yl "root mean square deviation"
'''
    del(fileType, terminalStr, xTicStr, xAxisLabel)
    
    # Set scaling of plot:
    text = text + scaleText
    del(scaleText)
    
    # Set axes ranges and plot concentrations:
    text = text + '''
set xrange [''' + str(xAxisBeg) + ':' + str(xAxisEnd) + '''] 

# Root mean square deviation:
plot \\''' # Important: No space allowed after the backslash!
    
    # Only for gradient differences, only for total and both (weighted equally):
    text = text + '''
dataFileIn u 2 title "RMSD of gradient" w lp lt 2 pt 1 lc 1 ,\\
dataFileIn u 3 title "RMSD of total" w lp lt 2 pt 2 lc 2,\\
dataFileIn u 4 title "weighted RMSD of both" w lp lt 2 pt 3 lc 3
pause 1
'''
    
    del(xAxisBeg, xAxisEnd)
    
    # Save .gnplt file:
    gnpltName = plotName[:-5] + '.gnplt'
    fileObj = open(gnpltName, 'w')
    fileObj.write(text)
    fileObj.close()
    print('Saved file ' + gnpltName)
    del(text, fileObj)
    
    # Execute .gnplt file:
    subprocess.call(['gnuplot', gnpltName])
    print('Saved result of ' + gnpltName)
    
    # Delete .plot and .gnplt files:
    os.remove(plotName)
    os.remove(gnpltName)
    del(plotName, gnpltName)

    return

