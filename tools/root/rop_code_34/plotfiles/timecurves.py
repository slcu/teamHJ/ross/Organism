#! /usr/bin/env python
#
# Written by Bettina Greese, CBBP, Uni Lund, Sweden (2013).
#

################################################################################

def gnuplot(baseName, parDict, subst, fileType):
    '''Plot concentration curves for one substance over time.
    
    Written by Bettina Greese, CBBP, Uni Lund, Sweden (2015).'''
    
    import os, subprocess
    import numpy as np
    from toolbox import variables, dirnavi
    
    # Settings:
    diffTol = 10**-9 # Set tolerance for checking diff in test for equality.
    if subst == 'auxin':
        #compType = 'all'
        compType = 'onlyRealCells'
    elif subst in ['ropa', 'ropi']:
        compType = 'onlyRealCells'
    
    # Load substance concentrations (in columns):
    fileName = subst + '_conc.txt'
    fileObj = open(fileName, 'r')
    substConc = np.loadtxt(fileObj)
    fileObj.close()
    print('Loaded ' + subst + ' concentration from file ' + fileName)
    plotFileName = fileName.replace('.txt', '.plot')
    del(fileName, fileObj)
    numRun = np.shape(substConc)[1]
    
    # Get column indices for .final file:
    [colIndDict, colList] = variables.colind('.final')
    del(colList)
    
    # Load .final data as array and extract relevant columns:
    finalData = np.loadtxt(baseName + '.final', skiprows = 1)
    xCoord = finalData[:, colIndDict['x']]
    yCoord = finalData[:, colIndDict['y']]
    if compType != 'all':
        cellMarker = finalData[:, colIndDict['cellMarker']]
        sourceMarker = finalData[:, colIndDict['auxinIn']]
        sinkMarker = finalData[:, colIndDict['auxinOut']]
    
    # Only keep data for first y value:
    plotY = parDict['plotY'][1][0]
    relInd = np.where(yCoord - plotY < diffTol)[0]
    del(yCoord, plotY, diffTol)
    numRel = len(relInd)
    xCoord = xCoord[relInd]
    substConc = substConc[relInd, :]
    if compType != 'all':
        cellMarker = cellMarker[relInd]
        sourceMarker = sourceMarker[relInd]
        sinkMarker = sinkMarker[relInd]
    del(relInd)
    
    # Only keep data from (real) cell compartments:
    if compType in ['onlyCells', 'onlyRealCells']:
        relInd = np.where(cellMarker == 1)[0]
        del(cellMarker)
        xCoord = xCoord[relInd]
        substConc = substConc[relInd]
        sourceMarker = sourceMarker[relInd]
        sinkMarker = sinkMarker[relInd]
        if compType == 'onlyRealCells':
            relInd = np.where(sourceMarker == 0)[0]
            del(sourceMarker)
            xCoord = xCoord[relInd]
            substConc = substConc[relInd]
            sinkMarker = sinkMarker[relInd]
            relInd = np.where(sinkMarker == 0)[0]
            del(sinkMarker)
            xCoord = xCoord[relInd]
            substConc = substConc[relInd]
        numRel = len(relInd)
        xCoord = xCoord[relInd]
        substConc = substConc[relInd]
    
    # Change directory:
    simPathName = os.getcwd()
    simPathName = simPathName[simPathName.rfind('/')+1:]
    plotPathName = simPathName.replace('sim_', 'plot_')
    dirnavi.mkchdir('../' + plotPathName)
    del(plotPathName)
    
    # Loop over simulation runs:
    for indS in range(0, numRun):
        # Reshape array with x coordinate values:
        xCoord = xCoord.reshape(numRel, 1)
        
        # Make array with time index:
        timeInd = indS*np.ones((numRel, 1))
        
        # Extract current substance concentrations:
        currSubstConc = substConc[:, indS]
        currSubstConc = currSubstConc.reshape(numRel, 1)
        
        # Make array with x coord in first col, time in second, conc in third:
        currData = np.concatenate((xCoord, timeInd, currSubstConc), axis = 1)
        del(timeInd, currSubstConc)
        
        # Save array and empty line to .plot file:
        if indS == 0:
            fileObj = open(plotFileName, 'w') # Open new file for writing.
        else:
            fileObj = open(plotFileName, 'a') # Append to exisiting file.
        
        # Loop over compartments:
        np.savetxt(fileObj, currData, fmt = ['%2.4e', '%4i', '%2.4e']) # Save data.
        fileObj.write('\n') # Append empty line.
        fileObj.write('\n') # Append empty line.
        fileObj.close()
        del(fileObj, currData)
    print('Saved file ' + plotFileName)
    del(xCoord, substConc)
    
    # Set x and z ranges:
    xBeg = 0
    xEnd = numRel
    del(numRel)
    zBeg = 0
    zEnd = numRun
    del(numRun)
    
    # Settings:
    scaleType = 'lin'
    
    # Settings for plot:
    if fileType == '.eps':
        terminalStr = 'set term post enhanced color ' \
                      'portrait size 26 cm, 12 cm'
        terminalStr = 'set term post enhanced color ' \
                      'portrait size 20 cm, 10 cm'
    elif fileType == '.png':
        terminalStr = 'set term pngcairo enhanced color ' \
                      'size 26 cm, 8 cm'
    if scaleType == 'lin':
        scaleText = '\n### Plot with linear scale ###\n'
    elif scaleType == 'log':
        scaleText = text + '\n### Plot with logarithmic scale ###\n\n' \
                    + 'set logscale y\n'
    else:
        raise Exception("Set scaleType to 'lin' or 'log'.")
    del(scaleType)
    
    # Make 3D plot:
    gnpltName = plotFileName.replace('.plot', '.gnplt')
    plotOutName = plotFileName.replace('.plot', '') + '_time' + fileType
    text = '''# File for plotting results for ROP model using gnuplot.
#
# Written by Bettina Greese based on template from Henrik Jonsson, 
# CBBP, Uni Lund, Sweden (2013-14).
#
# Specify input data:
dataFileIn = "''' + plotFileName + '"\n\n' + terminalStr + '''\n
dataFileOut = "''' + plotOutName + '''"
set output dataFileOut # Set name of output file.
#set key horizontal outside top center height 1.3 # Settings for legend.
set size ratio 1.0 # 0.3  Set ratio of plot height to width.
set ytics out nomirror # Put y tic marks only on y1 axis.
set xtics out nomirror rotate
set ztics out
set grid xtics
set grid ytics
set grid ztics
set view 50, 0 # or 50, 5, default was 60, 30

# Plot concentration over cell length (i.e. concentration profile):
set xl "x coordinate [dm]"
set yl "time [min]"
set zl "conc [umol/dm^3]"

# Cell walls:

unset arrow
'''
    del(plotOutName)
    
    # Set scaling of plot:
    text = text + scaleText
    
    # Set ranges of axes:
    text = text + '''
#set xrange [''' + str(xBeg) + ':' + str(xEnd) + ''']

# Concentrations:
splot \\'''
    
    # Loop over simulation runs:
    text = text + '''
dataFileIn w l lt 1 lc 1 ,\\'''
    text = text[:-2] + '''
pause 1
'''
    
    # Save .gnplt file:
    fileObj = open(gnpltName, 'w')
    fileObj.write(text)
    fileObj.close()
    print("Saved file " + gnpltName)
    del(text, fileObj)
    
    # Execute .gnplt file:
    subprocess.call(['gnuplot', gnpltName])
    print('Saved result of ' + gnpltName)
    
    # Delete .gnplt and .plot file:
    os.remove(gnpltName)
    os.remove(plotFileName)
    del(gnpltName, plotFileName)
    del(xBeg, xEnd, zBeg, zEnd)
    del(terminalStr, scaleText)
    
    os.chdir('../' + simPathName)
    del(simPathName)
    
    return

