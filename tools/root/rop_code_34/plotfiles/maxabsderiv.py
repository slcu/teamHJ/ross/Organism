#! /usr/bin/env python
#
# Written by Bettina Greese, CBBP, Uni Lund, Sweden (2014).
#

################################################################################

def gnuplot(baseName, simPhase, parDict, fileType):
    '''Make plots of maximal absolute derivative over time for one simulation.
    
    Written by Bettina Greese, CBBP, Uni Lund, Sweden (2014).'''
    
    import pickle, subprocess, os
    
    # Get values:
    sTol = parDict['sTol' + simPhase][1]
    sCount = parDict['sCount' + simPhase][1]
    
    # Load dictionary with derivative data:
    pickleName = '../' + simPhase + 'sim_' + str(sTol) \
                 + '_' + str(sCount) + '/' + baseName + '_stats.pkl'
    del(sTol, sCount)
    fileObj = open(pickleName, 'r')
    [simCount, tolCount, simTime, maxDerivList, saveTime] = pickle.load(fileObj)
    fileObj.close()
    del(fileObj, pickleName, simCount, tolCount, simTime, saveTime)
    
    # Set x and y ranges:
    xBeg = -0.5
    xEnd = len(maxDerivList) - 0.5
    if simPhase == 'A':
        yBeg = 0.0
        yEnd = 10.0**-3
    elif simPhase == 'R':
        yBeg = 0.0
        yEnd = 10.0**-1
    
    # Make .plot file:
    plotName = baseName + '_max_deriv.plot'
    plotFileObj = open(plotName, 'w')
    
    # Loop over maximal absolute derivatives:
    for ind in range(0, len(maxDerivList)):
        plotFileObj.write(str(maxDerivList[ind]) + '\n')
    del(ind, maxDerivList)
    
    # Close .plot file:
    plotFileObj.close()
    print('Saved file ' + plotName)
    del(plotFileObj)
    
    # Make .gnplt file:
    if fileType == '.eps':
        terminalStr = 'set term post enhanced color ' \
                      'portrait size 26 cm, 8 cm'
        terminalStr = 'set term post enhanced color ' \
                      'portrait size 20 cm, 6 cm'
    elif fileType == '.png':
        terminalStr = 'set term pngcairo enhanced color ' \
                      'size 26 cm, 5 cm'
    text = '''# File for plotting maximal absolute derivative list using gnuplot.
#
# Written by Bettina Greese, CBBP, Uni Lund, Sweden (2014).
#

# Specify input data:
dataFileIn = "''' + plotName + '"\n\n' + terminalStr + '''\n
dataFileOut = "''' + plotName[:-5] + fileType + '''"
set output dataFileOut # Set name of output file.

# Settings:
set key horizontal inside center top # Settings for legend.
set size ratio 0.3 #  Set ratio of plot height to width.
set ytics out mirror # Put y tic marks on both sides.
set xl "time step"
set yl "max abs deriv"
set xrange [''' + str(xBeg) + ':' + str(xEnd) + ''']
set yrange [''' + str(yBeg) + ':' + str(yEnd) + ''']

# Maximal absolute derivative:
plot dataFileIn u 1 title "" w lp lt 1
pause 1
'''
    del(terminalStr, fileType, xBeg, xEnd, yBeg, yEnd)
    
    # Save .gnplt file:
    gnpltName = plotName[:-5] + '.gnplt'
    fileObj = open(gnpltName, 'w')
    fileObj.write(text)
    fileObj.close()
    print('Saved file ' + gnpltName)
    del(text, fileObj)
    
    # Execute .gnplt file:
    subprocess.call(['gnuplot', gnpltName])
    print('Saved result of ' + gnpltName)
    
    # Delete .plot and .gnplt files:
    os.remove(plotName)
    os.remove(gnpltName)
    del(plotName, gnpltName)
    
    return


