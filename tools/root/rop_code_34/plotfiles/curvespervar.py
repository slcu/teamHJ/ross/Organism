#! /usr/bin/env python
#
# Written by Bettina Greese, CBBP, Uni Lund, Sweden (2013).
#

################################################################################

def gnuplot(baseName, pathName, parDict, zoom, timeVal, fileType):
    '''Plot concentration curves per substance for one parameter set.
    
    Written by Bettina Greese, CBBP, Uni Lund, Sweden (2012-14).'''
    
    import os, pickle, subprocess
    import numpy as np
    from plotfiles import forplots
    from toolbox import variables
    
    # Program control:
    doOnlyRealCells = 0
    
    # Settings:
    diffTol = 10**-9 # Set tolerance for checking diff in test for equality.
    if timeVal == 'final':
        commonName = baseName + '_curves_final_allsubst_'
    else:
        commonName = baseName + '_curves_allsubst_'
    scaleType = 'lin'
    if 'A' in pathName:
        substList = ['auxin']
    elif 'R' in pathName:
        substList = ['auxin', 'ropi', 'ropa']
    
    if timeVal == 'final':
        # Get column indices for .final file:
        [colIndDict, colList] = variables.colind('.final')
        
        # Check if simulation has been finished:
        if os.path.exists('../' + pathName + baseName + '.sums') == False:
            raise Exception('Simulations for current path are not finished.')
        
        # Load .final data as array:
        currArray = np.loadtxt('../' + pathName + baseName + '.final', skiprows=1)
    else:
        # Get column indices for .data file:
        [colIndDict, colList] = variables.colind('.data')
        
        # Unpickle array from current time point:
        pickleName = baseName + '_data_array_' + str(timeVal) + '.pkl'
        arrayFileObj = open(pickleName, 'r')
        currArray = pickle.load(arrayFileObj)
        arrayFileObj.close()
        del(pickleName, arrayFileObj)
    del(colList)
    
    # Calculate total number of compartments:
    numTotal = parDict['xTotalComp'][1]\
              *parDict['yTotalComp'][1]\
              *parDict['zTotalComp'][1]
    
    # Set x range and calculate x values of walls:
    xBeg = 0.0
    xEnd = zoom
    xWallList = forplots.walls('x', xEnd, parDict)
    if doOnlyRealCells == 1 and parDict['sosiType'][1] == 'tipshoot':
        xBeg = xWallList[1]
        xEnd = min(xWallList[-1], xEnd)
        xWallList = xWallList[1:-1]
    elif doOnlyRealCells == 1 and parDict['sosiType'][1] == 'bothshoot':
        xBeg = 0.0
        xEnd = min(xWallList[-1], xEnd)
    
    # For single cell, shift x values such that first wall is at zero:
    #print(len(xWallList))
    #raw_input()
    
    wallText = forplots.walltext(xWallList)
    del(xWallList)
    
    # Settings for plot:
    if fileType == '.eps':
        terminalStr = 'set term post enhanced color ' \
                      'portrait size 26 cm, 12 cm'
        terminalStr = 'set term post enhanced color ' \
                      'portrait size 20 cm, 10 cm linewidth 1.3'
    elif fileType == '.png':
        terminalStr = 'set term pngcairo enhanced color ' \
                      'size 26 cm, 8 cm'
    if scaleType == 'lin':
        scaleText = '\n### Plot with linear scale ###\n'
    elif scaleType == 'log':
        scaleText = text + '\n### Plot with logarithmic scale ###\n\n' \
                    + 'set logscale y\n'
    else:
        raise Exception("Set scaleType to 'lin' or 'log'.")
    
    # Specify column indices for .plot file:
    xCol, lCol, sCol = '1', '2', '3'
    
    # Loop over y values chosen for plotting:
    plotY = parDict['plotY'][1]
    if plotY == []:
        raise Exception('List of y values for plotting is empty.')
    elif len(plotY) != 1:
        raise Exception('Check if plotting makes sense for several y values.')
    for yVal in plotY:
        
        # In current data array, find rows for current y value:
        rowCrit = (abs(currArray[:, colIndDict['y']] - yVal) < diffTol)
        if sum(np.shape(rowCrit)) != numTotal:
            print(np.shape(rowCrit), numTotal)
            raise Exception('Wrong length of array criterium.')
        if sum(rowCrit) != parDict['xTotalComp'][1]:
            raise Exception('Wrong length of row indices list.')
        
        # Save values for x and cellLabel:
        relArray = currArray[rowCrit == True, :]
        relArray = relArray[:, [colIndDict['x'], colIndDict['cellLabel']]]
        
        # Loop over substances:
        for subst in substList:
            if subst == 'auxin':
                substCol = colIndDict['auxin']
                if scaleType == 'lin':
                    y1Beg = 0.0
                    #y1End = 0.1
                    y1End = 0.5
                elif scaleType == 'log':
                    y1Beg = 0.001
                    y1End = 0.1
                y2Beg, y2End = y1Beg, y1End
            elif subst in ['ropa', 'ropi']:
                y2Beg = 0.0
                y2End = 20.0
                if subst == 'ropa':
                    substCol = colIndDict['ROPactive']
                elif subst == 'ropi':
                    substCol = colIndDict['ROPinactive']
            
            # Save values for substance in every iteration:
            substArray = currArray[rowCrit == True, substCol]
            substArray.resize(sum(substArray.shape), 1) # Add second dimension.
            relArray = np.concatenate((relArray, substArray), axis = 1)
            del(substArray, subst, substCol)
        del(rowCrit)
        
        # Save array as text in .plot file:
        if len(plotY) == 1:
            yStr = ''
        else:
            yStr = str(yVal) + 'y_'
        if timeVal == 'final':
            plotName = commonName + str(zoom) + 'dm.plot'
        else:
            plotName = commonName + str(zoom) \
                       + 'dm_' + str(timeVal) + '.plot'
        np.savetxt(plotName, relArray)
        print("Saved file " + plotName)
        del(relArray, yVal)
        
        # Make one plot for all substances and current y value:
        gnpltName = plotName[:-4] + 'gnplt'
        text = '''# File for plotting results for ROP model using gnuplot.
#
# Written by Bettina Greese based on template from Henrik Jonsson, 
# CBBP, Uni Lund, Sweden (2013-14).
#
# Specify input data:
dataFileIn = "''' + plotName + '"\n\n' + terminalStr + '''\n
dataFileOut = "''' + plotName[:-5] + fileType + '''"
set output dataFileOut # Set name of output file.
set key horizontal outside top center height 1.3 # Settings for legend.
set size ratio 0.3 #  Set ratio of plot height to width.
set ytics out nomirror # Put y tic marks only on y1 axis.
set y2tics out nomirror # Put y2 tic marks only on y2 axis.
set xtics out nomirror rotate
set grid ytics

# Plot concentration over cell length (i.e. concentration profile):
set xl "x coordinate [dm]"
set yl "auxin conc [umol/dm^3]"
set y2l "ROP conc [umol/dm^3]"

# Cell walls:

unset arrow
'''
        
        # Plotting of cell walls as lines:
        text = text + wallText
        
        # Set scaling of plot:
        text = text + scaleText
        
        # Set ranges of axes:
        text = text + '''
set xrange [''' + str(xBeg) + ':' + str(xEnd) + ''']
#set yrange [''' + str(y1Beg) + ':' + str(y1End) + ''']
#set y2range [''' + str(y2Beg) + ':' + str(y2End) + ''']

# Concentrations:

plot \\'''
        
        # Loop over substances:
        for ind in range(0, len(substList)):
            relCol = str(int(sCol) + ind)
            legEntry = substList[ind]
            if substList[ind] == 'auxin':
                axesStr = 'x1y1'
            elif substList[ind] in ['ropa', 'ropi']:
                axesStr = 'x1y2'
            if parDict['xNumComp'][1] <= 4:
                lineStyle = 'lp'
            else:
                lineStyle = 'l'
            #lineStyle = 'lp'
            if doOnlyRealCells == 0:
                partText = relCol
            elif doOnlyRealCells == 1:
                partText = '($' + lCol + '>0 ? $' + relCol + ': 1/0)'
            if ind < 5:
                lineColor = str(ind + 1)
            else:
                lineColor = str(ind + 2)
            text = text + '''
dataFileIn u ''' + xCol + ':' + partText + ' title "' + legEntry \
+ '" w ' + lineStyle + ' lt 1 lc ' + lineColor + ' lw 1.3 axes ' + axesStr + ',\\'
            del(relCol, legEntry, lineStyle, partText, axesStr, lineColor)
        del(ind)
        text = text[:-2] + '''
pause 1
'''
        
        # Save .gnplt file:
        fileObj = open(gnpltName, 'w')
        fileObj.write(text)
        fileObj.close()
        print("Saved file " + gnpltName)
        del(text, fileObj)
        
        # Execute .gnplt file:
        subprocess.call(['gnuplot', gnpltName])
        print('Saved result of ' + gnpltName)
        
        # Delete .plot and .gnplt files:
        os.remove(plotName)
        os.remove(gnpltName)
        del(plotName, gnpltName)
    del(substList, wallText, xBeg, xEnd, y1Beg, y1End, y2Beg, y2End)
    del(currArray, terminalStr, scaleText, xCol, lCol, sCol, plotY)
    
    return

