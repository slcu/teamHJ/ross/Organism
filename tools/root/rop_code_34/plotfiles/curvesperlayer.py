#! /usr/bin/env python
#
# Written by Bettina Greese, CBBP, Uni Lund, Sweden (2014).
#

################################################################################

def gnuplot(baseName, origParDict, pathList, projName, zoom, fileType):
    '''Make plots of concentrations for each layer separately and for comparison.
    
    Written by Bettina Greese, CBBP, Uni Lund, Sweden (2014-15).'''
    
    import subprocess, os, numpy, pickle
    from toolbox import dirnavi, variables
    from plotfiles import plotsettings, forplots
    
    # Program control:
    doOnlyRealCells = 0
    scaleType = 'lin' # Choose 'lin' or 'log'.
    xScaleFac = 10**5 # Factor for scaling of x tics.
    
    # Settings:
    eTol = 10**-12 # Error tolerance for checks.
    commonName = projName + '_curves_final_auxin_'
    if scaleType == 'lin':
        yBeg = 0.0
        yEnd = 0.1
        yEnd = 0.2
        #yBeg = 0.003
        #yEnd = 0.1
    elif scaleType == 'log':
        yBeg = 0.001
        yEnd = 1.0
        # To match figures in Jones_2009_ATN: 
        yBeg = 0.003
        yEnd = 3.0
    
    # Get code version number:
    vNrStr = dirnavi.codeversion()

    # Get column indices for .final file:
    [colIndDict, colList] = variables.colind('.final')
    del(colList)
    
    # Set x range and calculate x values of walls:
    xBeg = 0.0
    xEnd = zoom
    xWallList = forplots.walls('x', xEnd, origParDict)
    if doOnlyRealCells == 1 and origParDict['sosiType'][1] == 'tipshoot':
        xBeg = xWallList[1]
        xEnd = min(xWallList[-1], xEnd)
        xWallList = xWallList[1:-1]
    elif doOnlyRealCells == 1 and origParDict['sosiType'][1] == 'bothshoot':
        xBeg = 0.0
        xEnd = min(xWallList[-1], xEnd)
    xBeg = xScaleFac*xBeg
    xEnd = xScaleFac*xEnd
    xWallList = [xScaleFac*xWallVal for xWallVal in xWallList]
    del(xWallVal)
    wallText = forplots.walltext(xWallList)
    del(xWallList)
    
    # Change directory:
    os.chdir('../../')
    
    # Initialize array for concentrations and list for legend:
    numComp = origParDict['xTotalComp'][1]\
             *origParDict['yTotalComp'][1]\
             *origParDict['zTotalComp'][1]
    numVar = len(pathList[0])
    numCC = 3 # Number of common columns.
    compArray = numpy.zeros(shape = (numComp, numCC + numVar))
    legStrList = []
    
    # Loop over paths to auxin simulations:
    for pInd in range(0, numVar):
        currPath = pathList[0][pInd]
        
        # Change directory:
        os.chdir(currPath)
        
        # Unpickle parDict:
        pickleName = baseName + '_parDict.pkl'
        fileObj = open(pickleName, 'r')
        parDict = pickle.load(fileObj)
        fileObj.close()
        print("Loaded file " + pickleName)
        del(pickleName, fileObj)
        
        # Make legend entry:
        if 'parvar' in projName \
        or 'stopcrit' in projName:
            # Find name and value of varied parameter:
            parName = projName[projName.rfind('_') + 1:]
            legStrList.append(parName + ' = ' + str(parDict[parName][1]))
        elif 'initcond' in projName:
            # Find value of initial condition:
            parName = projName[projName.rfind('_') + 1:]
            if parName == 'auxin':
                legEntry = 'auxin = ' + str(parDict['initAuxin'][1])
            elif parName in ['roptotal', 'ropafrac']:
                legEntry = 'ropa = ' + str(parDict['initRopa'][1]) \
                          + ', ropi = ' + str(parDict['initRopi'][1])
            legStrList.append(legEntry)
            del(legEntry)
        elif 'vartempl' in projName:
            # Find name of cell and auxin template:
            legEntry = currPath[currPath.find('/')+1:]
            legEntry = legEntry[:legEntry.find('/')]
            indUn = legEntry.find('_')
            legEntry = legEntry[:indUn] + ' - ' + legEntry[indUn + 1:]
            legStrList.append(legEntry)
            del(legEntry, indUn)
        del(currPath)
        
        # Check number of compartments:
        if numComp != parDict['xTotalComp'][1]\
                     *parDict['yTotalComp'][1]\
                     *parDict['zTotalComp'][1]:
            raise Exception('Wrong total number of compartments.')
        
        # Add settings for visualization:
        plotsettings.visual(parDict)
        
        # Get x and y values of sections for which conc curves will be plotted:
        pickleName = '../../template/' + baseName + '_comp.pkl'
        forplots.getsections(baseName, parDict, pickleName, 'auxin')
        del(pickleName)
        
        # Check if simulation has been finished:
        if os.path.exists(baseName + '.sums') == False:
            raise Exception('Simulations for current path are not finished.')
        
        # Load .final data as array:
        finalArray = numpy.loadtxt(baseName + '.final', skiprows=1)
        
        # Extract/check columns with x, y and cellLabel:
        if pInd == 0: # Only once.
            # Extract columns:
            xCoord = finalArray[:, colIndDict['x']]
            compArray[:, 0] = xCoord
            yCoord = finalArray[:, colIndDict['y']]
            compArray[:, 1] = yCoord
            cellLabel = finalArray[:, colIndDict['cellLabel']]
            compArray[:, 2] = cellLabel
        else:
            # Check columns:
            if sum(xCoord - finalArray[:, colIndDict['x']]) > eTol:
                raise Exception('x coordinates are not equal to previous case.')
            if sum(yCoord - finalArray[:, colIndDict['y']]) > eTol:
                raise Exception('y coordinates are not equal to previous case.')
            if sum(cellLabel - finalArray[:, colIndDict['cellLabel']]) > eTol:
                raise Exception('Cell labels are not equal to previous case.')
        
        # Extract auxin concentration column:
        auxinConc = finalArray[:, colIndDict['auxin']]
        if len(auxinConc) == len(xCoord):
            compArray[:, pInd + numCC] = auxinConc
        else:
            raise Exception('Auxin concentration has different length than others.')
        del(finalArray, auxinConc)
        
        # Rescale x values:
        if pInd == 0: # Only once.
            compArray[:, 0] = [xScaleFac*compVal for compVal in compArray[:, 0]]
            del(compVal)
        
        # Change directory:
        os.chdir('../../../../..')
    
    del(xCoord, yCoord, cellLabel, colIndDict, numComp)
    
    # Change directory:
    os.chdir('results' + vNrStr + '/' + projName)
    del(vNrStr)
    
    # Settings for plot:
    if fileType == '.eps':
        #terminalStr = 'set term post enhanced color ' \
        #              'portrait size 26 cm, 15 cm'
        #terminalStr = 'set term post enhanced color ' \
        #              'portrait size 20 cm, 10 cm linewidth 1.3'
        terminalStr = 'set term post enhanced color ' \
                      'portrait size 20 cm, 8 cm linewidth 1.2'
    elif fileType == '.png':
        terminalStr = 'set term pngcairo enhanced color ' \
                      'size 26 cm, 15 cm'
    if scaleType == 'lin':
        scaleText = '\n### Plot with linear scale ###\n'
    elif scaleType == 'log':
        scaleText = '\n### Plot with logarithmic scale ###\n\n' \
                    + 'set logscale y\n'
    else:
        raise Exception("Set scaleType to 'lin' or 'log'.")
    
    # Specify column indices for .plot file:
    xCol, yCol, lCol = '1', '2', '3'
    
    # Loop over relevant y values:
    for yVal in parDict['plotY'][1]:
        
        # Extract data with current y value:
        rowInd = abs(compArray[:, 1] - yVal) < eTol # Relevant rows.
        relArray = compArray[rowInd, :] # Corresponding data.
        del(rowInd)
        if numpy.shape(relArray) != (origParDict['xTotalComp'][1], numCC + numVar):
            raise Exception('Array with relevant compartments has wrong size.')
        
        # Save array as text in .plot file:
        plotName = commonName + str(yVal) + 'y_' + str(zoom) + 'dm.plot'
        numpy.savetxt(plotName, relArray)
        print("Saved file " + plotName)
        del(relArray, yVal)
        
        # Make .gnplt file:
        text = '''# File for plotting results for multicell ROP model using gnuplot.
#
# Written by Bettina Greese based on template from Henrik Jonsson, 
# CBBP, Uni Lund, Sweden (2014).
#
# Specify input data:
dataFileIn = "''' + plotName + '"\n\n' + terminalStr + '''\n
dataFileOut = "''' + plotName[:-5] + fileType + '''"
set output dataFileOut # Set name of output file.
#set key horizontal outside top center height 1.3 # Settings for legend.
set key horizontal outside center top # Settings for legend.
set size ratio 0.25 #  Set ratio of plot height to width.
set xtics out nomirror rotate offset 0,graph 0.1
set ytics out mirror # Put y tic marks on both sides.
set grid ytics

# Plot concentration over cell length (i.e. concentration profile):
set xl "x coordinate [um]"
set yl "auxin conc [umol/dm^3]"

# Cell walls:

unset arrow
'''
        
        # Plotting of cell walls as lines:
        text = text + wallText
        
        # Set scaling of plot:
        text = text + scaleText
        
        # Set axes ranges:
        text = text + '''
set xrange [''' + str(xBeg) + ':' + str(xEnd) + '''] 
#set yrange [''' + str(yBeg) + ':' + str(yEnd) + ''']

# Concentrations:
plot \\''' # Important: No space allowed after the backslash!
        
        # Plot concentrations:
        for ind in range(0, numVar):
            relCol = str(numCC + ind + 1)
            legEntry = legStrList[ind]
            if doOnlyRealCells == 0:
                lineWidth = '0.3'
            elif doOnlyRealCells == 1:
                lineWidth = '1.3'
            if ind < 5:
                lineColor = str(ind + 1)
            else:
                lineColor = str(ind + 2)
            text = text + '''
dataFileIn u ''' + xCol + ':' + relCol + ' title "' + legEntry \
+ '" w l lt 1 lc ' + lineColor + ' lw ' + lineWidth + ',\\'
            #+ '" w lp lt ' + str(numVar - ind) + ' lc ' + str(ind + 1) + ',\\'
            del(lineWidth)
            if doOnlyRealCells == 0:
                partText = '($' + lCol + '>0 ? $' + relCol + ': 1/0)'
                text = text + '''
dataFileIn u ''' + xCol + ':' + partText + ' title "" w l lt 1 lc ' \
+ lineColor + ' lw 1.3,\\'
        #+ '" w lp lt ' + str(numVar - ind) + ' lc ' + str(ind + 1) + ',\\'
                del(partText)
            del(relCol, legEntry, ind, lineColor)
        text = text[:-2] # Remove last ',\\'.
        text = text + '''
pause 1
'''
        
        # Save .gnplt file:
        gnpltName = plotName[:-5] + '.gnplt'
        fileObj = open(gnpltName, 'w')
        fileObj.write(text)
        fileObj.close()
        print('Saved file ' + gnpltName)
        del(text, fileObj)
        
        # Execute .gnplt file:
        subprocess.call(['gnuplot', gnpltName])
        print('Saved result of ' + gnpltName)
        
        # Delete .plot and .gnplt files:
        os.remove(plotName)
        os.remove(gnpltName)
        del(plotName, gnpltName)
    
    del(compArray, wallText, xCol, yCol, lCol, numCC, numVar, legStrList, scaleText)
    del(xBeg, xEnd, yBeg, yEnd)
    
    return


