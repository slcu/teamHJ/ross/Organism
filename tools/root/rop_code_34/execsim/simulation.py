#! /usr/bin/env python
#
# Written by Bettina Greese, CBBP, Uni Lund, Sweden (2012-14).
#

################################################################################

def simuplot(parDict, levelList):
    '''Runs the code for the various simulation of the ROP model in organism.
    
    Written by Bettina Greese, CBBP, Uni Lund, Sweden (2012-13).'''
    
    import os, pickle, shutil, copy, subprocess #, rop_sim
    from toolbox import dirnavi
    #import rop_execute, rop_general_tb, rop_simulation_tb, rop_sim_cell
    
    
            
            # Change to directory for current auxin-related parameters:
            fileName = rop_general_tb.getfoldername('auxin', currParDict)
            rop_general_tb.mkchdir(fileName)
            del(fileName)
            
            # Loop over two phases of simulation:
            for simPhase in simPhaseList:
                print('\nSIMULATION PHASE ' + simPhase + '\n')
                if simPhase == 'A':
                    checkPhase = 'A'
                elif simPhase == 'R' and len(xLen) == 3:
                    checkPhase = 'R'
                elif simPhase == 'R' and len(xLen) > 3:
                    checkPhase = 'Rpercell'
                
                if simPhase == 'A':
                    simStr = 'auxin'
                    sTol = sTolA
                    sCount = sCountA
                    parNamePhase = parNameAuxin
                elif simPhase == 'R':
                    simStr = 'rop'
                    sTol = sTolR
                    sCount = sCountR
                    doMovie = 0
                    parNamePhase = parNameRop
                    
                    # Change to dir for initial conditions for ROP:
                    startNameRop = rop_general_tb.startname('rop', currParDict)
                    rop_general_tb.mkchdir(startNameRop)
                    
                    # Make dummy file for curr rop-related param if needed:
                    rop_simulation_tb.pardummy(parNameRop)
                    
                    # Change to directory for current rop-related parameters:
                    fileName = rop_general_tb.getfoldername('rop', currParDict)
                    rop_general_tb.mkchdir(fileName)
                    del(fileName)
                
                # Change to dir for simu for curr auxin/rop parameter set:
                rop_general_tb.mkchdir(simPhase + 'sim_' \
                                       + str(sTol) + '_' + str(sCount))
                
                        elif simPhase == 'R':
                            fileName = '../../../Asim_' + \
                            str(sTolA) + '_' + str(sCountA) + '/' + baseName
                            shutil.copy2(fileName + '.final', baseName + '.init')
                            shutil.copy2(fileName + '.neigh', baseName + '.neigh')
                            print('Copied .init and .neigh to ropsim dir.')
                            del(fileName)
                            
                            # Set correct initial conditions for inactive ROP:
                            if currParDict['ropa'][1] != 0.0 \
                               or currParDict['ropi'][1] != 0.0:
                                rop_simulation_tb.initrop(baseName, currParDict)
                        
                        # Initiate number of simulations (label for next):
                        simCount = 1 # Counter for curr/next simulations.
                    
                # Execute auxin/rop phase of simulation:
                if simStatus in ['notDone', 'partDone']:
                    if simPhase == 'A':
                        
                        # Simulation of auxin:
                        rop_execute.simulate(baseName, currParDict, \
                                    simPhase, simCount, tolCount, doMovie, \
                                    plotY, plotX, zoomList, movieList)
                        del(tolCount)
                    
                    elif simPhase == 'R' and len(xLen) == 3: # Single cell.
                        
                        # Simulation for ROP for all cells:
                        rop_execute.simulate(baseName, currParDict, \
                                    simPhase, simCount, tolCount, doMovie, \
                                    plotY, plotX, zoomList, movieList)
                        del(tolCount)
                    
                    elif simPhase == 'R' and len(xLen) > 3:
                        
                        # Simulation of ROP per cell:
                        rop_sim_cell.simpercell(baseName, currParDict, \
                                     simPhase, simCount, tolCount, 0, \
                                     plotY, plotX, zoomList, ['curves'])
                
                os.chdir('..') # Back to auxin parameter directory.
                
                if len(plotList) > 0:
                    # Change to dir for plots for curr stopping criteria:
                    rop_general_tb.mkchdir(simPhase + 'plots_' \
                                       + str(sTol) + '_' + str(sCount))
                
                    # Plotting of final state for current parameter set:
                    rop_execute.plot(baseName, currParDict, plotY, plotX, \
                                     plotList, zoomList, simPhase, simStatus)
                    os.chdir('..') # Back to auxin/rop parameter directory.
                
                if doMovie == 1:
                    movieFolder = simPhase + 'movies_' \
                              + str(sTol) + '_' + str(sCount)
                    rop_general_tb.mkchdir(movieFolder)
                    prevList = os.listdir('.')
                    os.chdir('..')
                    
                    # Making of movie for current parameter set:
                    framePath = '../' + simPhase + 'frames_' \
                              + str(sTol) + '_' + str(sCount)
                    rop_simulation_tb.movie(baseName, simPhase, movieFolder, \
                                            framePath, zoomList, movieList)
                    del(framePath)
                    
                    # Combine previous and current movie:
                    if len(prevList) > 0:
                        rop_simulation_tb.combine(movieFolder, prevList)
                    
                if simPhase == 'R':
                    os.chdir('../..') # Back to auxin/rop param directory.
                del(simPhase, simStr, simStatus, sTol, sCount)
            
            os.chdir('..') # Back to auxin directory.
            del(currParDict)
        del(currVarFacList)
    del(currParName, parNameList, varFacList, movieList)
    
    os.chdir('..') # Back to cell template directory.
    
    return

################################################################################

