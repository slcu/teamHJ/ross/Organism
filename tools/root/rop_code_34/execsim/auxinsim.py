#! /usr/bin/env python
# Module with one function for simulation of auxin part in the root model.
#
# Written by Bettina Greese, CBBP, Uni Lund, Sweden (2012-14).
#

################################################################################

def simulate(baseName, parDict, levelList, doKeepDataFile, doKeepConc):
    '''Runs the code for the various simulation of the ROP model in organism.
    
    Written by Bettina Greese, CBBP, Uni Lund, Sweden (2012-15).'''
    
    import os, pickle, shutil, copy, subprocess
    import numpy as np
    from toolbox import dirnavi
    from simfiles import shellbinfiles, modelfile
    from execsim import checksim, extract, merge
    
    # Parameter values:
    timeA = parDict['timeA'][1]
    sTolA = parDict['sTolA'][1]
    sCountA = parDict['sCountA'][1]
    saveTime = parDict['saveTime'][1]
    
    # Get lists of parameter names:
    parNameDict = dirnavi.getparnames()
    parNameAuxin = parNameDict['auxin']
    
    # Make dummy file for curr auxin-related param if needed:
    os.chdir('../../')
    dirnavi.pardummy(parNameAuxin)
    os.chdir(levelList[3] + levelList[4])
    
    # Get list of files in simulation directory:
    fileList = os.listdir(os.getcwd())
    
    # Check if simulations are needed:
    if doKeepDataFile == 1:
        if baseName + '.sums' in fileList and baseName + '.data' in fileList:
            print('Auxin simulations have been done and completed, .data exists.')
            simNeeded = False
        else:
            print('Auxin .data file does not exist.')
            simNeeded = True
            
            # Initialize counters for simulations and results below threshold:
            simLabel = 0
            tolCount = 0
            
            # Copy simulation files from template folder:
            shutil.copy2('../../template/osimA.sh', 'osimA.sh')
            print('Copied osimA.sh to auxin simulation directory.')
            
            # Copy simulation files from template folder:
            fileName = '../../template/' + baseName
            shutil.copy2(fileName + '.init', baseName + '.init')
            shutil.copy2(fileName + '.neigh', baseName + '.neigh')
            print('Copied .init and .neigh to auxin simulation directory.')
            shutil.copy2(fileName + '_comp.pkl', baseName + '_comp.pkl')
            print('Copied _comp.pkl file to auxin simulation directory.')
            del(fileName)
            
            # Make .model file:
            modelfile.model(baseName, parDict, 'A')
    
    elif doKeepDataFile == 0:
        if baseName + '.sums' in fileList:
            print('Auxin simulations have been done and completed.')
            simNeeded = False
        
        elif baseName + '.final' in fileList \
        and baseName + '_stats.pkl' in fileList:
            print('Some auxin simulation run has been done for current stopping criteria.')
            
            # Check if simulations have reached stopping criteria:
            [check, simNeeded, simLabel, tolCount] = checksim.stopcrit(baseName, \
                                                                     parDict, '')
        
        elif baseName + '.final' not in fileList \
        and baseName + '_stats.pkl' not in fileList:
            print('No auxin simulation has been done for current stopping criteria.')
            simNeeded = True
            
            # Check which simulations with other stopping criteria are best:
            bestName = checksim.checkprev(baseName, parDict, 'A')
            
            if bestName != '':
                print('Best simulation with other stopping criteria is: ' + bestName)
                
                # Check if best simulations have reached stopping criteria:
                [check, simNeeded, simLabel, tolCount] = checksim.stopcrit(baseName, \
                                                    parDict, '../' + bestName + '/')
                
                # Copy results from best simulation:
                if check in ['reached', 'toofew']:
                    checksim.copyprev(baseName, '../' + bestName + '/', doKeepDataFile)
                elif check == 'toomany':
                    # Do not use best simulation:
                    bestName = ''
            
            if bestName == '': # Not elif!
                print('No simulation with other stopping criteria has been done.')
                simNeeded = True
                
                # Initialize counters for simulations and results below threshold:
                simLabel = 0
                tolCount = 0
                
                # Copy simulation files from template folder:
                shutil.copy2('../../template/osimA.sh', 'osimA.sh')
                print('Copied osimA.sh to auxin simulation directory.')
                
                # Copy simulation files from template folder:
                fileName = '../../template/' + baseName
                shutil.copy2(fileName + '.init', baseName + '.init')
                shutil.copy2(fileName + '.neigh', baseName + '.neigh')
                print('Copied .init and .neigh to auxin simulation directory.')
                shutil.copy2(fileName + '_comp.pkl', baseName + '_comp.pkl')
                print('Copied _comp.pkl file to auxin simulation directory.')
                del(fileName)
                
                # Make .model file:
                modelfile.model(baseName, parDict, 'A')
        
        else:
            print(os.getcwd())
            raise Exception('Unexpected situation, check simulation folder.')
    
    # Run auxin simulation:
    if doKeepConc == 1:
        collAuxinConc = []
        collRopaConc = []
        collRopiConc = []
    while simNeeded == True:
        print('Run next auxin simulation')
        
        if simLabel != 0:
            # Copy previous .final into .init file:
            shutil.copy2(baseName + '.final', baseName + '.init')
            print('Copied .final into .init file.')
        
        # Calculate start and end time:
        startTime = simLabel*timeA
        endTime = (simLabel+1)*timeA
        
        # Make solver file:
        shellbinfiles.obin(baseName, startTime, endTime, saveTime)
        
        # Run simulation:
        subprocess.call(['chmod', '+x', 'osimA.sh'])
        subprocess.call(['./osimA.sh', baseName])
        print('Finished simulation ' + str(simLabel) + ' for auxin phase')
        
        if doKeepConc == 1:
            # Extract substance concentrations from current simulation:
            auxinConc = checksim.conc('auxin', baseName)
            ropaConc = checksim.conc('ROPactive', baseName)
            ropiConc = checksim.conc('ROPinactive', baseName)
            auxinConc = auxinConc.reshape(np.shape(auxinConc)[0], 1)
            ropaConc = ropaConc.reshape(np.shape(ropaConc)[0], 1)
            ropiConc = ropiConc.reshape(np.shape(ropiConc)[0], 1)
            if collAuxinConc == [] or collRopaConc == [] or collRopiConc == []:
                collAuxinConc = auxinConc
                collRopaConc = ropaConc
                collRopiConc = ropiConc
            else:
                collAuxinConc = np.concatenate((collAuxinConc, auxinConc), axis = 1)
                collRopaConc = np.concatenate((collRopaConc, ropaConc), axis = 1)
                collRopiConc = np.concatenate((collRopiConc, ropiConc), axis = 1)
            del(auxinConc, ropaConc, ropiConc)
            
            # Save/update substance concentrations:
            fileName = 'auxin_conc.txt'
            fileObj = open(fileName, 'w')
            np.savetxt(fileObj, collAuxinConc)
            fileObj.close()
            print('Saved auxin concentration in file ' + fileName)
            del(fileName, fileObj)
            fileName = 'ropa_conc.txt'
            fileObj = open(fileName, 'w')
            np.savetxt(fileObj, collRopaConc)
            fileObj.close()
            print('Saved ropa concentration in file ' + fileName)
            del(fileName, fileObj)
            fileName = 'ropi_conc.txt'
            fileObj = open(fileName, 'w')
            np.savetxt(fileObj, collRopiConc)
            fileObj.close()
            print('Saved ropi concentration in file ' + fileName)
            del(fileName, fileObj)
        
        # Save maximal absolute derivative for current simulation:
        maxDeriv = checksim.maxderiv('auxin', baseName)
        if np.isnan(maxDeriv):
            raise Exception('Maximal absolute derivative is NaN.') 
        
        # Count consecutive max differences below error tolerance:
        if maxDeriv < sTolA:
            tolCount += 1
        else:
            tolCount = 0
        
        # Remove .init file and rename or delete .data file:
        os.remove(baseName + '.init')
        if doKeepDataFile == 1:
            os.rename(baseName + '.data', baseName + '_' + str(simLabel) + '.data')
        elif doKeepDataFile == 0:
            os.remove(baseName + '.data')    
        
        # Update list with maximal absolute derivative:
        if simLabel == 0:
            maxDerivList = []
        else:
            # Unpickle previous simulation statistics:
            fileObj = open(baseName + '_stats.pkl', 'r')
            [prevSimLabel, prevTolCount, prevTimeA, maxDerivList, \
                prevSaveTime] = pickle.load(fileObj)
            fileObj.close()
            del(fileObj, prevTolCount)
            if prevTimeA != timeA or prevSaveTime != saveTime \
                or len(maxDerivList) != prevSimLabel + 1:
                raise Exception('Error in parameter values.')
            if prevSimLabel + 1 != simLabel:
                raise Exception('Wrong count of simulations.')
            del(prevSimLabel, prevTimeA, prevSaveTime)
        
        # Save maximal absolute derivative for current simulation:
        maxDerivList.append(maxDeriv)
        
        # Pickle simulation statistics:
        pickleName = baseName + '_stats.pkl'
        fileObj = open(pickleName, 'w')
        pickle.dump([simLabel, tolCount, timeA, maxDerivList, saveTime], fileObj)
        fileObj.close()
        print("Saved file " + pickleName)
        del(pickleName, fileObj)
        
        # Save simulation statistics as text:
        extract.stattext(baseName, simLabel, timeA, maxDerivList)
        del(maxDerivList)

        if doKeepDataFile == 1:
            if simLabel == 0:
                # Start merged file with current .data file:
                merge.mergedata(baseName, ['_' + str(simLabel)], parDict)
            else:
                if os.path.exists(baseName + '_all.data'):
                    # Merge previous results with current .data file:
                    merge.mergedata(baseName, ['_all', '_' + str(simLabel)], parDict)
                else:
                    # Start merged file with current .data file:
                    merge.mergedata(baseName, ['_' + str(simLabel)], parDict)
        
        # Update label for simulation runs:
        simLabel += 1
        if simLabel >= parDict['maxCount'][1]:
            raise Exception('Maximal number of simulations reached.')
        
        # Check if required number of simulations is below threshold for deriative:
        if tolCount >= sCountA:
        
            if doKeepDataFile == 1:
                # Rename final merged .data file:
                os.rename(baseName + '_all.data', baseName + '.data')
            
            if 'single' in levelList[1] or 'Two' in levelList[1]:
                # Save empty .sums file:
                fileName = baseName + '.sums'
                fileObj = open(fileName, 'w')
                fileObj.write('')
                fileObj.close()
                print('Saved empty file ' + fileName)
                del(fileName, fileObj)
            else: 
                # Extract section data from simulation result:
                extract.summass(baseName, parDict)
            
            # Stop simulation loop:
            break
    
    del(collAuxinConc, collRopaConc, collRopiConc)
    
    # Check if .sum file exists:
    if baseName + '.sums' not in fileList:
        if 'single' in levelList[1] or 'Two' in levelList[1]:
            # Save empty .sums file:
            fileName = baseName + '.sums'
            fileObj = open(fileName, 'w')
            fileObj.write('')
            fileObj.close()
            print('Saved empty file ' + fileName)
            del(fileName, fileObj)
        else:
            # Extract section data from simulation result:
            extract.summass(baseName, parDict)
    del(parDict)        
    
    return

################################################################################

