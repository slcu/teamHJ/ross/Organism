#! /usr/bin/env python
# Module with one function for simulation of auxin part in the root model.
#
# Written by Bettina Greese, CBBP, Uni Lund, Sweden (2012-14).
#

################################################################################

def simulate(baseName, parDict, levelList, doKeepDataFile, doKeepConc):
    '''Runs the code for the various simulation of the ROP model in organism.
    
    Written by Bettina Greese, CBBP, Uni Lund, Sweden (2012-14).'''
    
    import os, pickle, shutil, copy, subprocess
    import numpy as np
    from toolbox import dirnavi, variables
    from simfiles import shellbinfiles, modelfile
    from execsim import checksim, merge, ropsimloop
    
    # Parameter values:
    timeR = parDict['timeR'][1]
    sTolR = parDict['sTolR'][1]
    sCountR = parDict['sCountR'][1]
    saveTime = parDict['saveTime'][1]
    
    # Get lists of parameter names:
    parNameDict = dirnavi.getparnames()
    parNameAuxin = parNameDict['auxin']
    parNameRop = parNameDict['rop']
    
    # Make dummy file for curr rop-related param if needed:
    os.chdir('../../')
    dirnavi.pardummy(parNameRop)
    os.chdir(levelList[8] + levelList[9])
    
    # Get list of files in simulation directory:
    fileList = os.listdir(os.getcwd())
    
    # Check if simulations are needed:
    if doKeepDataFile == 1:
        if baseName + '.sums' in fileList and baseName + '.data' in fileList:
            print('ROP simulations have been done and completed, .data exists.')
            simNeeded = False
        else:
            print('ROP .data file does not exist.')
            simNeeded = True
    
    elif doKeepDataFile == 0:
        if baseName + '.sums' in fileList:
            print('ROP simulations have been done and completed.')
            simNeeded = False
        elif baseName + '.final' in fileList \
        and baseName + '_stats.pkl' in fileList:
            print('Some ROP simulation had been started, will be done again.')
            simNeeded = True
        elif baseName + '.final' not in fileList \
        and baseName + '_stats.pkl' not in fileList:
            print('No ROP simulation has been done, will be started.')
            simNeeded = True
        else:
            print(os.getcwd())
            raise Exception('Unexpected situation, check simulation folder.')
    
    if simNeeded == True:    
        # Initialize counters for simulations and results below threshold:
        simLabel = 0
        tolCount = 0
        
        # Copy simulation files from template folder:
        shutil.copy2('../../../../template/osimR.sh', 'osimR.sh')
        print('Copied osimR.sh to rop simulation directory.')
        
        # Copy simulation files from auxin simulation folder:
        fileName = '../../../' + levelList[4] + baseName
        shutil.copy2(fileName + '.final', baseName + '.init')
        shutil.copy2(fileName + '.neigh', baseName + '.neigh')
        print('Copied .final/.init and .neigh to rop simulation directory.')
        shutil.copy2(fileName + '_comp.pkl', baseName + '_comp.pkl')
        print('Copied _comp.pkl file to rop simulation directory.')
        del(fileName)
        
        # Get column indices for .init file:
        [colIndDict, colList] = variables.colind('.init')
        del(colList)
        
        # Open old .init file:
        initFileObj = open(baseName + '.init', 'r')
        
        # Make new .init file:
        newFileObj = open(baseName + '_new.init', 'w')
        
        # Get information about cell template from first line:
        line = initFileObj.readline()
        lineElem = line.split(' ')
        numComp = int(lineElem[0])
        del(lineElem)
        
        # Write first line to new .init file:
        newFileObj.write(line)
        del(line)
        
        # Loop over lines in old .init file:
        initRopa = str(parDict['initRopa'][1])
        initRopi = str(parDict['initRopi'][1])
        for currComp in range(0, numComp):
            line = initFileObj.readline()
            lineElem = line.split(' ')
            
            # For real cell compartments, set ROP initial values:
            if lineElem[colIndDict['cellMarker']] == '1' \
            and lineElem[colIndDict['cellLabel']] != '-1':
                lineElem[colIndDict['ROPactive']] = initRopa
                lineElem[colIndDict['ROPinactive']] = initRopi
            
            # Write line to new .init file:
            line = ' '.join(lineElem)
            newFileObj.write(line)
            del(line)
        del(numComp, initRopa, initRopi, colIndDict)
        
        # Close files:
        initFileObj.close()
        newFileObj.close()
        del(initFileObj, newFileObj)
        print('Saved new .init file.')
        
        # Delete old .init file and rename (new) .init file:
        os.remove(baseName + '.init')
        os.rename(baseName + '_new.init', baseName + '.init')
        
        # Make .model file:
        modelfile.model(baseName, parDict, 'R')
    
    # Run rop simulation:
    if doKeepConc == 1:
        collAuxinConc = []
        collRopaConc = []
        collRopiConc = []
    while simNeeded == True:
        [simLabel, tolCount, simNeeded] = ropsimloop.onerun(baseName, simLabel, \
            tolCount, parDict, levelList, doKeepDataFile)
        
        if doKeepConc == 1:
            # Save ropa and ropi concentrations from current simulation:
            ropaConc = checksim.conc('ropa', baseName)
            ropaSize = np.shape(ropaConc)
            ropaConc = ropaConc.reshape(ropaSize[0], 1)
            del(ropaSize)
            if collRopaConc == []:
                collRopaConc = ropaConc
            else:
                collRopaConc = np.concatenate((collRopaConc, ropaConc), axis = 1)
            del(ropaConc)
            ropiConc = checksim.conc('ropi', baseName)
            ropiSize = np.shape(ropiConc)
            ropiConc = ropiConc.reshape(ropiSize[0], 1)
            del(ropiSize)
            if collRopiConc == []:
                collRopiConc = ropiConc
            else:
                collRopiConc = np.concatenate((collRopiConc, ropiConc), axis = 1)
            del(ropiConc)
            
            # Save/update ropa and ropi concentrations for current simulation:
            fileName = 'ropa_conc.txt'
            fileObj = open(fileName, 'w')
            np.savetxt(fileObj, collRopaConc)
            fileObj.close()
            print('Saved active ROP concentrations in file ' + fileName)
            del(fileName, fileObj)
            fileName = 'ropi_conc.txt'
            fileObj = open(fileName, 'w')
            np.savetxt(fileObj, collRopiConc)
            fileObj.close()
            print('Saved inactive ROP concentrations in file ' + fileName)
            del(fileName, fileObj)
    
    del(collRopaConc, collRopiConc)
    
    return

################################################################################

