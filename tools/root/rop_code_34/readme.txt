### AUTHOR, CONTACT, VERSION, DATE ###

The Python scripts in the folder "root" all provided by Bettina Greese.
For questions and comments please email me (bettina.greese@thep.lu.se).
The current code version is number 34 (seen in "v34" in the main folder name).
Last edited 2015-02-27.

### GETTING STARTED ###

Setting paths:

Before doing anything else, edit the paths to the Organism simulator folder and to the folder where the output of these scripts should be saved. They are found in the functions <orgpath> and <outpath> in the package <simfiles> and then in the module <simsettings.py>.

### STRUCTURE OF THE CODE ###

Top level folder:

The main scripts to work with are in the top level folder called <rop_code_vxx> (xx is the current version number), and start with "run_". There is one script to make all files that are needed for simulations (<run_simfiles.py>). Then, several scripts are provided that run simulations, either just for one parameter set (<run_singlesim.py>) or for several sets that represent some sort of variation (e.g. <run_parvarsim.py>). Furthermore, several other scripts are used to make plots from simulation results and present these in pdf summaries (<run_singleplots.py>, <run_multiplots.py>, <run_multicomp.py>). 

Packages:

The subfolders are all packages for the Python code, and they contain modules grouped by their function. Of particular interest for the user is the package <inputdata> that contains modules where the properties of the template (i.e. cell geometries and neighbourhood relationships) and the parameters (for reactions and also the simulations) are set (see below for details).


### FOLDER STRUCTURE GENERATED BY THE CODE ###

The scripts produce two main folders, one for the simulation data (called <simu_vxx>) and one for the summarized results (called <results_vxx>). Due to my personal arrangement, these two folder appear on the same level as the folder <root>. [It is planned for the future to allow individual setting of the path.] In the following, the subfolder structure is eplained, what the values appearing in them actually mean is explained later.

Results folder:

The folder <results_vxx> will contain one subfolder for each project, e.g. <default> or <parvar_so>. Each project folder will contain sets of at least three files that belong together and have almost the same name, containing the cell and auxin templates and the discretization and a running number. The first two files contains a list of paths to the simulation result, once in pickled Python form and once as a text file, e.g. <singleX50_single1_10_1_1_parvar_so_1.pkl>. The third and further files are pdfs with a summary of parameters and other settings and the results plots, e.g. <singleX50_single1_10_1_1_parvar_so_1_comp.pdf>. In case of the default project, there is only one simulation included, while in all other projects, several simulations are compared.

Simulations folder:

The folder <simu_vxx> will contain one subfolder for each combination of cell and auxin template, e.g. <singleX50_single1>. This, in turn, will contain one subfolder for each combination of the discretization and the initial concentration of auxin, e.g. <10_1_1_A_0.0>. This will contain one subfolder called <template>, one subfolder for each set of auxin parameters used and one subfolder for each mode of ROP simulations (dynamic or conserved) and the initial concentrations of active and inactive ROP, e.g. <dyna_R_0.0_0.0>. To remember the order in which the auxin parameter values appear in these names, there is an (empty) file with the names of the auxin parameters, i.e. <Da__Dw__Pa__Paux__Ppin__pHc__pHw__pK__pqc__q__si__so>. Then, each parameter folder has one subfolder for each set of stopping criteria for the simulations, e.g. <Asim_1.0_60>. And this - finally - contains the simulation files and the result, e.g. the file <rop.final>. The subfolders of the ROP folder are organized in analogy to those of the corresponding auxin folder. In summary, the hierarchy for the given example is: 
<simu_vxx><singleX50_single1><10_1_1_A_0.0><xxx_xxx_xxx><Asim_1.0_60>.
This may look complicated at first, but it enables the code to check if a simulation for certain settings have been performed before and then reused the result and hence save time.


### SCRIPT FOR MAKING TEMPLATES ###

The script <run_simfiles.py> makes the "template", i.e. it generates the files needed for the simulation, and also a pdf that illustrates some aspects of the cell and auxin templates. These files are stored in:
<simu_vxx><singleX50_single1><10_1_1_A_0.0><template>.

Executable script:

For each script, before it is run for the first time, it needs to be made executable. In the terminal, type the following line, for example:
chmod +x run_simfiles.py

Arguments:

The template script uses up to 10 arguments. The first is an integer (0 or 1) that is there for consistency reasons and does not play a role. The second and third are the names of the cell and auxin templates, e.g. singleX50 and single1. The fourth is a string that does not play a role here. The next three are integers that give the discretization, i.e. number of compartments, in the x, y, z direction, e.g. 10 1 1. The next three are floats that give the initial concentrations of auxin, active and inactive ROP, e.g. 0.0 0.0 0.0.

Here is an example how to run the template script: 
./run_simfiles.py 1 singleX50 single1 none 10 1 1 0.0 0.0 0.0

The last six arguments, i.e. the two sets of three numbers, are optional. When the last three floats are missing, the default initial concentrations 0.0 0.0 0.0 are used. When, in addition, the three integers for the discretization are missing, the default 10 1 1 is used. Note that it is currently not possible to specify the initial concentrations but not the discretization.


### HOW TO MAKE A TEMPLATE WITH SPECIFIC PROPERTIES ###

It is highly recommended to NOT CHANGE any given templates but to copy the sections of the code, give a new template name and then adjust the settings. This makes sure that all simulations with the same template names have actually used the same settings. 

Cell templates:

The cell templates are defined in the function <celltempl> in the module <cells.py> in the package <inputdata>. The following settings are done for each case (also see comments in the file): 
- wall thickness, 
- lengths of cells in x and y direction (along the root and radially outward), 
- number of cells in the z direction (circumference of the root) that are modelled and that are assumed (needed for calculation of cell volume), 
- number of columella and quiescent center (QC) cells, 
- the type of source and sink. 
When more than one cell size is given for the y direction, the template is considered to have layers of different tissues.

Auxin templates:

The auxin templates are defined in the function <auxintempl> in the module <neighs.py> in the package <inputdata>. The following settings are done for each case (also see comments): 
- location of PINs and AUX1 in general and in the QC and the columella, 
- location and relative strength of lateral PINs, 
- areas for weakening of apical/basal PINs, 
- flip of PINs, 
- presence of AUX1 in parts of some layers, 
- number of source and sink cells. 
Most of these settings are given as one key word or index per layer.

So, to make your particular template, add sections to <cells.celltempl> and <neighs.auxintempl> and then execute <run_simfiles.py> as described above. You will find the resulting files in the folder 
<simu_vxx><celltemplname_auxintemplname><10_1_1_A_0.0><template>
if you choose default discretization and initial conditions. Otherwise, the folder above <template> will be called slightly different.


### SCRIPTS FOR SINGLE SIMULATION AND PLOTS ###

to be written...


### PROJECT SIMULATION SCRIPTS ###

to be written...


### SCRIPTS FOR PLOTS AND PRESENTATIONS ###

to be written...



