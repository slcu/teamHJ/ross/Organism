#! /usr/bin/env python
# Module with one function that makes .init or .costtemplate file for Organism.
#
# Written by Bettina Greese, CBBP, Uni Lund, Sweden (2012-14).
#

################################################################################

def init(fileType, infoDict, parDict):
    '''Save .init or .costtemplate file for Organism simulation.
    
    Written by Bettina Greese, CBBP, Uni Lund, Sweden (2012-13).
    Template for .init file provided by Henrik Jonsson.'''
    
    import pickle, math
    from toolbox import variables
    from simfiles import initcomp
    
    # Settings:
    rDig = 8 # Number of digits when rounding.
    fileName = infoDict['baseName'] + '.' + fileType
    if 'singleL' in infoDict['cellTemplate'] or 'fileL' in infoDict['cellTemplate'] \
    or 'fileVarL' in infoDict['cellTemplate']:
        noLatWall = 1
    else:
        noLatWall = 0
    
    # Get parameter values:
    wallThick = parDict['wallThick'][1]
    zNumCells = parDict['zNumCells'][1]
    xTotalComp = parDict['xTotalComp'][1]
    yTotalComp = parDict['yTotalComp'][1]
    zTotalComp = parDict['zTotalComp'][1]
    if fileType == 'costtemplate':
        # Add positions in x direction where root is cut into sections:
        initcomp.cutpositions(parDict)
        xCutList = parDict['xCutList'][1]
        gradData = parDict['gradWt'][1]
    
    # Get column indices for .init file:
    [colIndDict, colList] = variables.colind('.init')
    colKeys = colIndDict.keys()
    ropaCol = colIndDict['ROPactive']
    ropiCol = colIndDict['ROPinactive']
    soCol = colIndDict['auxinIn']
    siCol = colIndDict['auxinOut']
    prodCol = colIndDict['auxinProd']
    cCol = colIndDict['cellMarker']
    wCol = colIndDict['wallMarker']
    lCol = colIndDict['cellLabel']
    if fileType ==  'costtemplate':
        gmCol = colIndDict['sumMass']
        gmaCol = colIndDict['sumMassAll']
    
    # Define "standard" compartment (line in .init file, all markers =0 in default):
    if colKeys[:].sort() != colList[:].sort(): # Use sorted copies, do not sort original!
        raise Exception('Check list of columns.')
    numVar = len(colKeys)
    stdComp = ['unx', 'uny', 'unz', 'unV', '0.0', '0.0', '0.0' \
               , '0', '0', '0', '0', '0', '0', '0.0', '0.0', '0.0']
    stdComp[lCol] = 'unl'
    if fileType == 'costtemplate':
        stdComp[gmCol], stdComp[gmaCol] = 'unk', 'unk'
    stdComp[colIndDict['auxin']] = str(parDict['initAuxin'][1])
    if len(colList) != len(stdComp):
        print(len(colList), len(stdComp))
        raise Exception('Column list and std comp need same number of entries.')
    del(colKeys)
    
    # Preallocation of variables:
    writeStr = '' # Initialize string that will be written to file.
    xCompSizeList = [] # Init list for comp size in x dir for every comp.
    yCompSizeList = [] # Init list for comp size in y dir for every comp.
    zCompSizeList = [] # Init list for comp size in z dir for every comp.
    xPosList = [] # Initialize list for x positions of tissue compartments.
    yPosList = [] # Initialize list for y positions of tissue compartments.
    zPosList = [] # Initialize list for z positions of tissue compartments.
    
    # Loop over cells in x direction (along the root from tip to shoot):
    xPos = -wallThick/2.0 # Initialize consecutive x position of compartment.
    xSecInd = 0 # Initialize index for root sections (for auxin gradient data).
    cellLabel = 0 # Initialize counter for cells that are not source or sink.
    xNumCells = len(parDict['xLengths'][1])
    yNumCells = len(parDict['yLengths'][1])
    for xCellInd in range(0, xNumCells + 1): # xCellInd runs from 0 to xNumCells.
        [currXLen, xCellComp] = initcomp.lenandcomp('x', parDict, xCellInd, rDig)
        
        # Loop over comp in x direction (along the current cell in x dir):
        xAccum = 0 # Variable for accumulated compartment size in x dir.
        for xCompInd in range(0, xCellComp + 1): # From 0 to xCellComp.
            
            # Add compartment size in x direction:
            currXComp = stdComp[:] # Current comp with properties relating to x.
            [xCompSize, xAccum] = initcomp.compsize('x', parDict, xCompInd, \
                currXComp, xCellComp, currXLen, xAccum, wCol, rDig)
            xPos += xCompSize/2 # Calc compartment's x position (center of comp).
            
            # Replace corresponding entry with comp's x position:
            currXComp[colIndDict['x']] = str(xPos)
            xPosList.append(xPos) # Save compartment's x position.
            
            if fileType == 'costtemplate':
                # Update section index and position for cut if needed:
                if xSecInd <= len(gradData) and xPos >= xCutList[xSecInd]:
                    xSecInd += 1
                
                # Add measured auxin mass for optimization:
                initcomp.auxingrad(currXComp, xPos, xSecInd, \
                    xCutList, gradData, colIndDict)
            
            # Loop over cells in y direction (across root from inside to outside):
            yPos = parDict['steleRad'][1] - wallThick/2.0 # Init consec y pos of comp.
            for yCellInd in range(0, yNumCells + 1): # Loop from 0 to yNumCells.
                if yCellInd == yNumCells:
                    if noLatWall == 1:
                        #print('Current "cell" will be skipped in current mode.')
                        continue
                    else:
                        #print('Additional "cell" in y direction, ' \
                        #      + 'will only have one wall compartment.')
                        pass
                else:
                    #print('Normal cell in y direction.')
                    pass
                [currYLen, yCellComp] = initcomp.lenandcomp('y', parDict, yCellInd, rDig)
                
                # Set ROP levels (keep fixed here, set before ROP simulation):
                ropaVal = '0.0'
                ropiVal = '0.0'
                
                # Loop over comp in y dir (along the curr cell in y dir):
                yAccum = 0 # Variable for accumlated comp size in y dir.
                for yCompInd in range(0, yCellComp + 1): # Loop from 0 to yCellComp.
                    if yCompInd == 0:
                        if noLatWall == 1:
                            #print('Current compartment will be skipped in current mode.')
                            continue
                        else:
                            #print('Additional compartment in y direction, will be wall.')
                            pass
                    else:
                        #print('Normal compartment in y direction.')
                        pass
                    #print(yCompInd, yCellComp)
                    #raw_input('yCompInd, yCellComp')
                    
                    # Add compartment size in y direction:
                    currXYComp = currXComp[:] # Curr comp with props relating to x and y.
                    #print(currXYComp)
                    #raw_input('currXYComp')
                    [yCompSize, yAccum] = initcomp.compsize('y', parDict, \
                        yCompInd, currXYComp, yCellComp, currYLen, yAccum, wCol, rDig)
                    #print(yCompSize, yAccum)
                    #raw_input('yCompSize, yAccum')
                    yPos += yCompSize/2 # Calc comp's y pos (center of comp).
                    currXYComp[colIndDict['y']] = str(yPos)
                    #print(currXYComp)
                    #raw_input('check currXYComp')
                    
                    # Add ROP levels:
                    currXYComp[ropaCol] = ropaVal
                    currXYComp[ropiCol] = ropiVal
                    
                    # Loop over cells in z direction (along periphery):
                    zPos = 0.0 # Initialize consecutive z position of comp (float!).
                    if zNumCells == 0: # 2.5D simulation.
                        currXYZComp = currXYComp
                        currXYZComp[colIndDict['z']] = '0.0'
                        zCompSize = yPos*2.0*math.pi/parDict['zNumRoot'][1]
                    else:
                        raise Exception('Not implemented, yet.')
                    
                    if xCellInd == 0 and xCompInd == 0:
                        yPosList.append(yPos) # Save compartment's y pos once.
                        #if yCellInd == 0 and yCompInd == 0:
                        if yCellInd == 0 and yCompInd == 1:
                            zPosList.append(zPos) # Save z pos once.
                    
                    del(currXYComp, zPos)
                    
                    # Save compartment size for use in neigh:
                    xCompSizeList.append(xCompSize)
                    yCompSizeList.append(yCompSize)
                    zCompSizeList.append(zCompSize)
                    
                    # Determine compartment volume:
                    compV = str(xCompSize*yCompSize*zCompSize) # EDIT
                    #print(compV, xCompSize, yCompSize, zCompSize)
                    #raw_input()
                    # Replace corresponding entry with comp volume:
                    currXYZComp[colIndDict['V']] = compV
                    del(compV, zCompSize)
                    #print('currXYZComp')
                    #print(currXYZComp)
                    #raw_input()
                    
                    # Set properties depending on cell/wall:
                    if currXYZComp[wCol] == '0': # Has not been determined as wall.
                        currXYZComp[cCol] = '1' # Set cell marker to 1.
                    elif currXYZComp[wCol] == '1': # Wall.
                        currXYZComp[ropaCol] = '0.0' # Ensure that ropa is 0.0.
                        currXYZComp[ropiCol] = '0.0' # Ensure that ropi is 0.0.
                    checkCell = (currXYZComp[cCol] == '1') # Check for cell.
                    
                    # Source/sink cell property and auxin production:
                    if checkCell: # Only cell comp may be source or sink.
                        [currXYZComp, checkSo] = initcomp.sosiregion(currXYZComp, \
                            soCol, parDict['sourceLoc'][1], xPos, yPos)
                        [currXYZComp, checkSi] = initcomp.sosiregion(currXYZComp, \
                            siCol, parDict['sinkLoc'][1], xPos, yPos)
                        [currXYZComp, checkP] = initcomp.sosiregion(currXYZComp, \
                            prodCol, parDict['prodLoc'][1], xPos, yPos)
                        del(checkP)
                        if checkSo or checkSi: # Set ROPs to zero.
                            currXYZComp[ropaCol] = '0.0'
                            currXYZComp[ropiCol] = '0.0'
                        if checkSo and checkSi:
                            raise Exception('Compartment can not be source and sink.')
                    else: # Wall can never be source or sink.
                        checkSo = False
                        checkSi = False
                    
                    # Set cell label for real cells that are not source/sink:
                    if checkCell and checkSo == False and checkSi == False:
                        if parDict['sosiType'][1] == 'tipshoot':
                            cellLabel = ((xCellInd-1)*yNumCells) + yCellInd + 1
                        elif parDict['sosiType'][1] == 'bothshoot':
                            cellLabel = (xCellInd*yNumCells) + yCellInd + 1
                        currXYZComp[lCol] = cellLabel # Set cell label.
                    else:
                        currXYZComp[lCol] = '-1' # Set (dummy) cell label.
                    del(checkSo, checkSi)
                    #print(currXYZComp)
                    
                    # Check current compartment:
                    if len(currXYZComp) != numVar:
                        raise Exception('Incorrect length of line for .init file.')
                    
                    # Add current compartment to write-string:
                    for entry in currXYZComp:
                        writeStr = writeStr + str(entry) + ' '
                    del(entry, currXYZComp, checkCell)
                    writeStr = writeStr + '\n'
                    
                    yPos += yCompSize/2 # Set y pos to border of curr and next comp.
                    del(yCompSize)
                    
                del(yCompInd, yCellComp, yAccum, currYLen, ropaVal, ropiVal)
            del(currXComp)
            #raw_input()
            
            xPos += xCompSize/2 # Set x pos to border of curr and next comp.
            
            del(yPos, yCellInd, xCompSize)
        del(xCompInd, xCellComp, currXLen, xAccum)
    del(xPos, xCellInd, cellLabel)
    del(xNumCells, yNumCells, zNumCells, wallThick, xSecInd)
    del(ropaCol, ropiCol, cCol, wCol, colIndDict)
    if fileType == 'costtemplate':
        del(gradData, xCutList)
    del(stdComp)
    
    # Add first line to write-string:
    writeStr = str(xTotalComp*yTotalComp*zTotalComp) + ' ' \
             + str(numVar) + '\n' + writeStr
    del(numVar)
    
    # Add last line with comment:
    writeStr = writeStr + '#' # Add comment sign.
    for entry in colList: # Add columns.
        writeStr = writeStr + entry + ' '
    del(entry, colList)
    writeStr = writeStr + '\n'
    
    # Make .init or .costtemplate file:
    initFileObj = open(fileName, 'w')
    initFileObj.write(writeStr)
    initFileObj.close()
    print('Saved file ' + fileName)
    del(fileName, initFileObj, writeStr)
    
    # Check compartment position and size lists:
    #print(len(xPosList), xTotalComp)
    #print(len(yPosList), yTotalComp)
    #print(len(zPosList), zTotalComp)
    check1 = (len(xPosList) == xTotalComp)    
    check2 = (len(yPosList) == yTotalComp)
    check3 = (len(zPosList) == zTotalComp)
    if check1 == False or check2 == False or check3 == False:
        raise Exception('Wrong length of compartment position list.')
    del(check1, check2, check3)
    check1 = (len(xCompSizeList) == len(yCompSizeList))
    check2 = (len(xCompSizeList) == xTotalComp*yTotalComp*zTotalComp)
    del(xTotalComp, yTotalComp, zTotalComp)
    if check1 == False or check2 == False:
        raise Exception('Wrong length of compartment size list.')
    del(check1, check2)
    
    if fileType == 'init':
        # Pickle lists of compartment sizes and positions:
        pickleName = infoDict['baseName'] + '_comp.pkl'
        fileObj = open(pickleName, 'w')
        pickle.dump([xCompSizeList, yCompSizeList, zCompSizeList, \
                     xPosList, yPosList, zPosList], fileObj)
        fileObj.close()
        print('Saved file ' + pickleName)
        del(pickleName, fileObj)
    del(xCompSizeList, yCompSizeList, zCompSizeList, xPosList, yPosList, zPosList)
    
    return

################################################################################

