#! /usr/bin/env python
# Module with functions related to settings for variations in parameters etc.
#
# Written by Bettina Greese, CBBP, Uni Lund, Sweden (2014).
#

################################################################################

def factors(parName):
    '''Returns list with factors for the value of the given parameter.
    
    Written by Bettina Greese, CBBP, Uni Lund, Sweden (2014-15).'''

    # Always include 1.0 to get default value (must be first!):
    varFacList = [1.0]
    
    # CHOOSE FACTORS FOR VARIATION:
    if 'so' == parName:
        #varFacAdd = map(float, range(2, 11))
        #varFacAdd = [0.5, 2.0]
        #varFacAdd = [1.1, 0.9]
        #varFacAdd = [0.1, 0.3, 3.0, 10.0]
        varFacAdd = [0.5, 0.6, 0.7, 0.8, 0.9, 1.1, 1.2, 1.3, 1.4, 1.5]
        #varFacAdd = [0.5, 0.7, 1.5, 2.0]
        #varFacAdd = [float(val) for val in range(2,11)]
        #varFacAdd = [0.1, 0.3, 3.0, 10.0, 100.0, 1000.0]
        #varFacAdd = [0.1, 0.3, 3.0, 10.0, 100.0]
        #varFacAdd = [0.3, 3.0]
        #varFacAdd = [0.1, 3.0, 10.0, 30.0, 300.0] # importance of level in combined single
        #varFacAdd = [0.001, 0.01, 0.1, 3.0] # importance of level in separate single
        #varFacAdd = [1.1, 1.15, 1.2] # fileVarL1
        #varFacAdd = [0.5, 0.7, 1.5, 2.5] # represents different regions of file
        #varFacAdd = map(float, range(2, 18, 2))
        #varFacAdd = [0.1, 10.0]
        #varFacAdd = [0.1, 0.2, 0.5] # test loss of ROP peak for low auxin
        #varFacAdd = [5.0, 50.0, 150.0, 200.0, 300.0] # test loss of ROP peak for high auxin
        #varFacAdd = [10.0, 50.0, 100.0, 500.0, 700.0, 900.0, 1000.0] # test loss of ROP peak for high auxin
        #varFacAdd = [0.1, 300.0] # test loss of ROP peak
    elif 'si' == parName:
        #varFacAdd = [10.0, 10.0**2, 10.0**3, 10.0**4, 10.0**5]
        varFacAdd = [10.0, 0.1]
        #varFacAdd = [0.5, 2.0]
    elif 'q' == parName:
        #varFacAdd = map(float, range(2, 11))
        varFacAdd = [0.5, 0.6, 0.7, 0.8, 0.9, 1.1, 1.2, 1.3, 1.4, 1.5]
        #varFacAdd = [0.2, 0.5, 2.0, 5.0]
        #varFacAdd = [0.3, 0.4, 0.5, 0.6, 0.7]
        #varFacAdd = [0.5, 2.0]
        #varFacAdd = [0.95]
        #varFacAdd = [2.0, 3.0, 4.0, 5.0]
        #varFacAdd = [0.001, 0.01, 0.1, 10.0, 100.0, 1000.0] # for illustration in single
        #varFacAdd = [0.01, 0.1, 10.0, 100.0]
        #varFacAdd = [0.01, 0.03, 0.1, 0.3, 3.0]
        #varFacAdd = [0.01, 0.03, 3.0]
        #varFacAdd = [1.9*10**-2] # compare values for file and single (when set to file)
    elif 'pqc' == parName:
        #varFacAdd = [1.5, 2.0, 5.0, 10.0]
        #varFacAdd = [0.5, 2.0]
        varFacAdd = [2.0, 5.0, 10.0]
    elif 'Tap' == parName:
        varFacAdd = [0.5, 2.0]
        #varFacAdd = [0.5]
        #varFacAdd = [2.0, 1.5, 0.7, 0.5]
        #varFacAdd = [0.6, 0.5, 0.4, 0.3, 0.2]
        #varFacAdd = [0.1, 0.5, 2.0, 10.0]
        #varFacAdd = [0.1, 0.3, 3.0, 10.0, 30.0, 100.0]
    elif 'Taa' == parName:
        varFacAdd = [0.0, 0.1, 10.0, 100.0]
        #varFacAdd = [0.0, 10.0, 1000.0]
    elif 'Da' == parName:
        #varFacAdd = [0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9] # detailed sampling
        #varFacAdd = [0.2, 0.4, 0.6, 0.8] # equal sampling
        #varFacAdd = [0.3, 0.5] # typical values if starting from water value
        #varFacAdd = [0.6, 2.0] # typical values if starting from half water value
        varFacAdd = [2.2, 3.0, 6.6] # typical values if starting from 10**-8
        #varFacAdd = [0.1, 10.0]
        #varFacAdd = [4.0]
    elif 'Dw' == parName:
        varFacAdd = [0.01, 0.1, 10.0, 100.0]
    elif parName in ['Dv', 'Du']:
        #varFacAdd = [0.5, 2.0]
        varFacAdd = [0.01, 0.1, 10.0, 100.0]
        #varFacAdd = [0.1, 10.0]
    elif 'c' == parName:
        #varFacAdd = [0.5, 2.0]
        #varFacAdd = [0.5, 0.1]
        #varFacAdd = [0.01, 0.1, 0.3]
        #varFacAdd = [1.1, 1.2, 1.3, 1.4, 1.5]
        varFacAdd = [2.0, 5.0, 10.0]
        #varFacAdd = [0.1, 0.2, 0.5, 2.0]
        #varFacAdd = [0.01, 0.1, 10.0, 100.0]
    elif 'k1' == parName:
        #varFacAdd = [0.5, 2.0]
        #varFacAdd = [0.2, 0.5, 1.5, 2.0, 5.0]
        #varFacAdd = [0.9, 0.8, 0.7, 0.6, 0.5, 0.4, 0.3]
        varFacAdd = [0.8, 0.6, 0.4, 0.2]
        #varFacAdd = [0.01, 0.05, 0.1, 5.0, 10.0]
        #varFacAdd = [0.1, 0.2, 0.5, 2.0, 5.0, 10.0]
        #varFacAdd = [0.0, 0.01, 0.1, 10.0, 100.0]
        #varFacAdd = [0.01, 0.1, 10.0, 100.0]
        #varFacAdd = [0.0]
        #varFacAdd = [0.03, 0.1, 0.2, 0.3] # for illustration in single
    elif 'k2' == parName:
        varFacAdd = [0.2, 0.3, 0.4]
        #varFacAdd = [0.5, 0.6, 0.7, 0.8, 0.9]
        #varFacAdd = [0.5, 0.6, 0.7, 0.8, 0.9]
        #varFacAdd = [0.2]
        #varFacAdd = [1.01, 1.02, 1.03, 1.04, 1.05]
        #varFacAdd = [5.0, 10.0, 50.0, 100.0]
        #varFacAdd = [0.1, 3.0, 10.0, 100.0, 200.0]
        #varFacAdd = [100.0, 200.0]
        #varFacAdd = [5.0, 6.0, 7.0, 8.0, 9.0, 10.0, 11.0, 12.0]
        #varFacAdd = [0.3, 0.35, 0.4, 2.0, 10.0, 30.0, 300.0] # for illustration in single
        #varFacAdd = [0.9, 0.8, 0.7, 0.6, 0.5, 0.4, 0.3, 0.2, 0.1]
        #varFacAdd = [0.9, 0.8, 0.7, 0.6]
        #varFacAdd = [0.9, 0.8, 0.7]
        #varFacAdd = [1.1, 1.2, 1.2, 1.4, 1.5, 0.9, 0.8, 0.7]
        #varFacAdd = [0.5, 0.55, 0.6, 0.65, 0.7]
    elif 'r' == parName:
        varFacAdd = [0.0, 0.9, 0.8, 0.7, 1.1, 1.2, 1.3]
        #varFacAdd = [0.1, 0.01, 0.001]
        #varFacAdd = [0.5, 2.0]
        #varFacAdd = [0.5, 0.6, 0.7, 0.8, 0.9]
        #varFacAdd = [0.1, 0.5, 1.5, 2.0] # for testing when s is used
        #varFacAdd = [2.8, 2.6, 2.4, 2.2] # 2.8 ok, other not finished (osc?)
        #varFacAdd = [2.2]
    elif 's' == parName:
        varFacAdd = [0.0, 0.5, 2.0, 3.0, 4.0] # for testing with r
    elif 'b' == parName:
        #varFacAdd = [0.5, 3.0, 5.0, 10.0, 30.0] # for illustration in single
        varFacAdd = [1.2, 1.3, 1.5, 2.0, 5.0]
        varFacAdd = [0.9, 0.8, 0.7]
        varFacAdd = [0.5, 2.0]
    elif 'k3' == parName:
        varFacAdd = [0.0, 10.0, 30.0, 100.0, 1000.0] # for no k4
        varFacAdd = [0.0, 0.1, 0.3, 0.5, 2.0]
        #varFacAdd = [1.2, 1.4, 1.6, 1.7, 1.8, 2.0]
        varFacAdd = [0.1]
    elif 'k4' == parName:
        varFacAdd = [0.0, 0.5, 2.0, 5.0, 10.0] # for no k3
        varFacAdd = [0.0, 0.3, 3.0, 10.0, 50.0] # for illustration
        varFacAdd = [0.5, 0.6, 0.7, 0.8, 0.9]
        varFacAdd = [0.0, 0.1, 0.2, 0.3, 0.4]
        varFacAdd = [0.1]
    
    # Combine default and variation list:
    varFacList.extend(varFacAdd)
    del(varFacAdd)
    
    # Sort list in ascending order:
    varFacList.sort()

    return varFacList

################################################################################


