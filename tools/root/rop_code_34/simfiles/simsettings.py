#! /usr/bin/env python
# Module with functions related to settings for simulations.
#
# Written by Bettina Greese, CBBP, Uni Lund, Sweden (2014-15).
#

################################################################################

def orgpath():
    '''Sets the path to the Organism simulator.
    
    Written by Bettina Greese, CBBP, Uni Lund, Sweden (2015).'''
    
    import os
    
    currPath = os.getcwd()
    if 'roth/bettina' in currPath:
        orgPath = '~/organism/bin/simulator'
    elif 'Bettina_Uni_Lund' in currPath:
        orgPath = '/cygdrive/c/organism/bin/simulator'
    else:
        orgPath = '~/software/organism/bin/simulator'
        #raise Exception('Implement path to organism folder (in simsettings.py).')
    
    return orgPath

################################################################################

def outpath():
    '''Sets the path to the folder in which output folders will be created.
    
    Written by Bettina Greese, CBBP, Uni Lund, Sweden (2015).'''

    import os

    currPath = os.getcwd()
    if 'roth' in currPath and 'bettina' in currPath:
        outPath = '/home/roth/bettina/Roots/Simulation/'
    elif 'Bettina_Uni_Lund' in currPath:
        outPath = '/cygdrive/d/Bettina_Uni_Lund/Roots/Simulation/'
    else:
        outPath = '/home/james/andre/projects/root/orgrootsim/Simulation_orig/'
        #raise Exception('Implement path to output folder (in simsettings.py).')
    del(currPath)
    
    return outPath

################################################################################

def defaults():
    '''Sets default discretization and initial conditions.
    
    Written by Bettina Greese, CBBP, Uni Lund, Sweden (2015).'''
    
    # CHOOSE DEFAULT DISCRETIATION:
    xyzNumCompDef = [50, 1, 1]
    
    # CHOOSE DEFAULT INITIAL CONDITIONS:
    initCondDef = [0.0, 0.0, 0.0]
    
    return [xyzNumCompDef, initCondDef]

################################################################################

def time(parDict):
    '''Adds simulation time per run and maximal number of runs to dictionary.
    
    Written by Bettina Greese, CBBP, Uni Lund, Sweden (2014).'''
    
    # Simulation time per run for auxin and ROP:
    parDict['timeA'] = ["auxin simulation time", 60] # 1 minute
    parDict['timeR'] = ["rop simulation time", 60] # 1 minute
    
    # Maximal number of simulation runs:
    parDict['maxCount'] = ["max simulation runs", 60*10]
    
    # Number of simulations saved in results files:
    parDict['saveTime'] = ["save time for results", 6]
    
    return

################################################################################

def tolerance(parDict, cellTemplate):
    '''Adds stopping criteria / tolerance for simulations to dictionary.
    
    Written by Bettina Greese, CBBP, Uni Lund, Sweden (2014).'''
    
    # Set error tolorance for stopping auxin and ROP simulations:
    # Set number of simulation runs below tolerance for stopping:
    parDict['sTolA'] = ["auxin stop tolerance", 1.0]
    parDict['sTolR'] = ["rop stop tolerance", 1.0]
    #parDict['sTolA'] = ["auxin stop tolerance", 100.0] # Dummy.
    #parDict['sTolR'] = ["rop stop tolerance", 100.0] # Dummy.
    if 'single' in cellTemplate:
        parDict['sCountA'] = ["auxin tolerance count", 30]
        #parDict['sCountR'] = ["rop tolerance count", 90]
        #parDict['sCountR'] = ["rop tolerance count", 120]
        parDict['sCountR'] = ["rop tolerance count", 180]
    elif 'epidermis' in cellTemplate or 'file' in cellTemplate \
    or cellTemplate == 'Grieneisen' or cellTemplate == 'Jones':
        parDict['sCountA'] = ["auxin tolerance count", 60]
        #parDict['sCountR'] = ["rop tolerance count", 60]
        #parDict['sCountR'] = ["rop tolerance count", 120]
        parDict['sCountR'] = ["rop tolerance count", 180]
        parDict['sCountR'] = ["rop tolerance count", 60]
    elif '2layers' in cellTemplate:
        parDict['sCountA'] = ["auxin tolerance count", 60]
        parDict['sCountR'] = ["rop tolerance count", 60]
    elif '7layersS' in cellTemplate:
        parDict['sCountA'] = ["auxin tolerance count", 60]
        parDict['sCountR'] = ["rop tolerance count", 0]
    elif '4layers' in cellTemplate or '7layers' in cellTemplate:
        parDict['sCountA'] = ["auxin tolerance count", 60]
        parDict['sCountR'] = ["rop tolerance count", 60]
    else:
        raise Exception('Unknown cell template.')
    
    return

################################################################################

def sosicells(parDict):
    '''Adds size of source and sink cells and external auxin applic to dictionary.
    
    Written by Bettina Greese, CBBP, Uni Lund, Sweden (2014).'''
    
    # Size of source cell in x direction:
    parDict['xSizeSo'] = ["source size in x dir", 10.0]
    
    # Size of sink cell in x direction:
    parDict['xSizeSi'] = ["sink size in x dir", parDict['xSizeSo'][1]]
    
    # Size of external auxin application cell in y direction:
    parDict['ySizeApp'] = ["external applic size in y dir", 10.0]
    
    return

################################################################################

