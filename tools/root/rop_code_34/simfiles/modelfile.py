#! /usr/bin/env python
# Module with one function that makes .model file for Organism simulation.
#
# Written by Bettina Greese, CBBP, Uni Lund, Sweden (2012-14).
#

################################################################################

def model(currName, parDict, simPhase):
    '''Save .model file for ROP model simulations with the specified parameters.
    
    Written by Bettina Greese, CBBP, Uni Lund, Sweden (2012-14).'''
    
    import pickle, os
    from toolbox import variables
    from simfiles import singlereact, multireact
    
    # Settings:
    fileName = currName + simPhase + '.model'
    messageSave = 'Saved file '
    
    if simPhase == 'A':
        # Unpickle lists of compartment sizes and positions:
        pickleName = currName + '_comp.pkl'
        fileObj = open(pickleName, 'r')
        [xCompSizeList, yCompSizeList, zCompSizeList, xPosList, \
                         yPosList, zPosList] = pickle.load(fileObj)
        fileObj.close()
        del(pickleName, fileObj)
        del(xCompSizeList, yCompSizeList, yPosList)
    
    # Get dictionary with species indices:
    [specIndDict, specList] = variables.colind('.model')
    del(specList)
    
    # Calculation of number of multispecies reactions:
    if simPhase == 'A':
        numMult = '3' # Summing of auxin mass for each region, all regions, total.
    elif simPhase == 'R':
        numMult = 5 # Ropa/ropi conversion, max 5 reactions.
        if parDict['k1'][1] == 0.0:
            numMult -= 1
        if parDict['k2'][1] == 0.0:
            numMult -= 1
        if parDict['k3'][1] == 0.0:
            numMult -= 1
        if parDict['k4'][1] == 0.0:
            numMult -= 1
        numMult = str(numMult)
    
    # Beginning of .model file:
    text = """# Model implementation of Payne et al, PLoS ONE (2009)
# into a tissue template
# The auxin gradient is implemented with a source on the left 
# (auxin-producing cell) and a sink to the right (auxin-degrading cell). 
# Parameter names taken from paper.
#
# Edited by Bettina Greese, original provided by Henrik Jonsson,
# CBBP, Uni Lund, Sweden (2012-14).
#
auxinROP 1 12 """ + numMult + " 1 # 1 topology, 12 species, " + numMult + """ multispeciesreaction(s), 1 neighborhood

topologyStatic 4 0 0 # 4 variables (x y z V), 0 reactions, 0 division rules (?), from Pontus' files

"""
    del(numMult)
    
    # Auxin reactions:
    auxinText = singlereact.auxinrct(simPhase, specIndDict, parDict)
    text = text + auxinText
    del(auxinText)
    
    # Active ROP:
    ropaText = singlereact.roparct(simPhase, specIndDict, parDict)
    text = text + ropaText
    del(ropaText)
    
    # Inactive ROP:
    ropiText = singlereact.ropirct(simPhase, specIndDict, parDict)
    text = text + ropiText
    del(ropiText)
    
    # Markers (i.e. species without (single-species) reactions):
    text = text + "auxinSource " + str(specIndDict['auxinIn']) + " 0     " \
         + "# defines (=1) apical or basal most cell(s) (where auxin comes in) \n\n" \
         \
         + "auxinSink " + str(specIndDict['auxinOut']) + " 0       " \
         + "# defines (=1) basal most cell(s) (where auxin is removed) \n\n" \
         \
         + "auxinProd " + str(specIndDict['auxinProd']) + " 0       " \
         + "# defines (=1) compartments with auxin production \n\n" \
         \
         + "cellMarker " + str(specIndDict['cellMarker']) + " 0     " \
         + "# defines (=1) compartments that are within cells \n\n" \
         \
         + "wallMarker " + str(specIndDict['wallMarker']) + " 0     " \
         + "# defines (=1) compartments that are between cells (walls) \n\n" \
         \
         + "cellLabel " + str(specIndDict['cellLabel']) + " 0      " \
         + "# for cell label \n\n" \
         \
         + "sumMass " + str(specIndDict['sumMass']) + " 0        " \
         + "# for summed mass of auxin in each of 3 sections \n\n" \
         \
         + "sumMassAll " + str(specIndDict['sumMassAll']) + " 0     " \
         + "# for summed mass of auxin in all 3 sections \n\n" \
         \
         + "sumMassTempl " + str(specIndDict['sumMassTempl']) + " 0     " \
         + "# for summed mass of auxin in whole template \n\n"
         
    # Multispecies reactions:
    if simPhase == 'A':
        
        # Calculate indices for cutting regions:
        xCutIndList = multireact.getcutind(parDict, xPosList)
        
        # Summed auxin mass in each section, all sections and whole template:
        massText = multireact.summassrct(xCutIndList, specIndDict, parDict)
        text = text + massText
        del(xCutIndList, massText)
    
    if simPhase == 'R':
        
        # Conversion between active and inactive ROP:
        roparopiText = multireact.roparopirct(specIndDict, parDict)
        text = text + roparopiText
        del(roparopiText)
    
    # Neighbourhood:
    text = text + "neighborhoodFromFileInitial 1 # from Pontus' files \n" \
         + "1 \n" \
         + currName + ".neigh \n"
    
    # Save file:
    fileObj = open(fileName, 'w')
    fileObj.write(text)
    fileObj.close()
    print(messageSave + fileName)
    del(text, fileName, fileObj)
    
    return

################################################################################

