#! /usr/bin/env python
# Module with one function that makes .neigh file for Organism simulation.
#
# Written by Bettina Greese, CBBP, Uni Lund, Sweden (2012-14).
#

################################################################################

def neigh(baseName, parDict):
    '''Save .neigh file for Organism simulation.
    
    Written by Bettina Greese, CBBP, Uni Lund, Sweden (2012-14).'''
    
    import pickle
    from toolbox import variables
    from simfiles import neighbors, neighcomp
    
    # Settings:
    eTol = 10**-8 # Error tolerance for checks.
    neighFileName = baseName + '.neigh'
    messageSave = 'Saved file '
    
    # Get column indices for .init file:
    [colIndDict, colList] = variables.colind('.init')
    del(colList)
    cCol = colIndDict['cellMarker']
    wCol = colIndDict['wallMarker']
    del(colIndDict)
    
    # Open .init file:
    initFileObj = open(baseName + '.init', 'r')
    
    # Make .neigh file:
    neighFileObj = open(neighFileName, 'w')
    
    # Get information about cell template from first line in .init file:
    line = initFileObj.readline() # Read the first line.
    lineElem = line.split(' ') # Get elements that were separated by whitespaces.
    numComp = int(lineElem[0]) # Number of compartments, i.e. rows.
    del(line, lineElem)
    neighFileObj.write(str(numComp) + ' 3\n')
    
    # Unpickle lists of compartment sizes and positions:
    fileObj = open(baseName + '_comp.pkl', 'r')
    [xCompSizeList, yCompSizeList, zCompSizeList, \
        xPosList, yPosList, zPosList] = pickle.load(fileObj)
    fileObj.close()
    del(fileObj, xPosList, yPosList, zPosList)
    
    # Check lengths of compartment size lists:
    if len(xCompSizeList) != numComp:
        raise Exception('Wrong length of compartment size list for x.')
    if len(yCompSizeList) != numComp:
        raise Exception('Wrong length of compartment size list for y.')
    if len(zCompSizeList) != numComp:
        raise Exception('Wrong length of compartment size list for z.')
    
    # Put information from .init file into a "matrix":
    initMat = []
    for currComp in range(0, numComp): # Loop over compartments (0 to numComp-1).
        line = initFileObj.readline() # Read next line in .init file.
        lineElem = line.split(' ') # Get elements (were sep by whitesp).
        lineElem.remove('\n') # Remove end-of-line character.
        lineList = [float(elem) for elem in lineElem] # Convert str elem to floats.
        initMat.append(lineList) # Add to matrix.
        del(line, lineElem, lineList)
    initFileObj.close()
    del(initFileObj, currComp)
    
    # For each compartment, define neighbourhood properties:
    num2nb, num3nb = 0, 0 # Init vars for num of comps with 2 or 3 neighb.
    for currComp in range(0, numComp): # Loop over comps (0 to numComp-1).
        currInit = initMat[currComp] # Line from .init for current compartment.
        
        # First entry = compartment index:
        neighFileObj.write(str(currComp) + ' ')
        
        # Find neighbours:
        [neighList, contList] = neighbors.findneigh(numComp, currComp, \
                initMat, xCompSizeList, yCompSizeList, zCompSizeList, eTol)
        
        # Count compartments with only two or three neighbours:
        if len(neighList) == 2:
            num2nb += 1
        elif len(neighList) == 3:
            num3nb += 1
        
        # Second entry = number of neighbours (n):
        neighFileObj.write(str(len(neighList)) + ' ')
        
        # Next n entries = indices of neighbours:
        for currNeigh in neighList: # Loop over neighbours.
            neighFileObj.write(str(currNeigh) + ' ')
        del(currNeigh)
        
        # Next n entries = areas of contact with neighbours:
        for currNeighInd in range(0, len(neighList)): # Loop over neighb.
            neighFileObj.write(str(contList[currNeighInd]) + ' ')
        del(currNeighInd, contList)
        
        # Next 2n entries = PIN and AUX activity at contact areas:
        pinActiv = []
        auxActiv = []
        for neighComp in neighList: # Loop over neighbours.
            neighInit = initMat[neighComp] # Line from .init for curr nb.
            
            # Preallocate variables:
            pinComp, auxComp = 0, 0
            pinStre, auxStre = 0.0, 0.0
            
            # Determine type of interface:
            if currInit[cCol] == 1 and neighInit[wCol] == 1:
                interType = 'cell2wall' # Curr = cell, nb = wall.
            elif currInit[wCol] == 1 and neighInit[cCol] == 1:
                interType = 'wall2cell' # Curr = wall, nb = cell.
            else:
                interType = 'notRelevant'
            
            # If interface is betw cell and wall, PIN and AUX may be needed:
            if interType in ['cell2wall', 'wall2cell']:
                [pinComp, pinStre] = neighcomp.pinauxstrength(pinComp, pinStre, \
                          interType, parDict['pinLoc'][1], currInit, neighInit)
                [auxComp, auxStre] = neighcomp.pinauxstrength(auxComp, auxStre, \
                          interType, parDict['auxLoc'][1], currInit, neighInit)
            del(interType, neighInit)
            
            # Place PIN:
            if pinComp == 0:
                pinActiv.append('0')
            elif pinComp == 1:
                pinActiv.append(str(pinStre))
            else:
                raise Exception('Problem with PIN localization.')
            del(pinComp, pinStre)
            
            # Place AUX:
            if auxComp == 0:
                auxActiv.append('0')
            elif auxComp == 1:
                auxActiv.append(str(auxStre))
            else:
                raise Exception('Problem with AUX localization.')
            del(auxComp, auxStre)
        
        del(neighComp)
        
        # Check PIN and AUX for all neighbours:
        if len(pinActiv) != len(auxActiv) or len(pinActiv) != len(neighList):
            raise Exception('Wrong length of neighbourhood properties.')
        del(neighList)
        
        # Write entries for PIN and AUX activity:
        for entry in pinActiv:
            neighFileObj.write(str(entry) + ' ')
        del(entry, pinActiv)
        for entry in auxActiv:
            neighFileObj.write(str(entry) + ' ')
        del(entry, auxActiv)
        neighFileObj.write('\n')
        del(currInit)
    del(currComp, initMat, eTol, numComp)
    del(xCompSizeList, yCompSizeList, zCompSizeList, cCol, wCol)
    
    # Check number of compartments with exactly 2 or 3 neighbours:
    zNumCells = parDict['zNumCells'][1]
    if num2nb > 0 and num3nb == 0:
        pass # singleL (no walls on sides)
    elif num2nb in [4, 6] and zNumCells == 0:
        pass # 2.5 dimensional case.
    elif num2nb == 0 and  num3nb in [4, 6] and zNumCells > 0:
        pass # 3 dimensional case.
    else:
        raise Exception('Incorrect num of compa with 2 or 3 neighbours.')
    del(num2nb, num3nb, zNumCells)
    
    # Last line in .neigh file with comment:
    neighFileObj.write('#') # Add comment sign.
    for entry in ['index', 'num_neigh_n', 'n_indices', 'n_areas', \
        'n_PIN_values', 'n_AUX_values']: # Add columns.
        neighFileObj.write(entry + ' ')
    del(entry)
    neighFileObj.write('\n')
    
    # Close file:
    neighFileObj.close()
    print(messageSave + neighFileName)
    del(neighFileName, neighFileObj)
    
    return

################################################################################


