#! /usr/bin/env python
# Module with one function for setting and collecting all parameters.
#
# Written by Bettina Greese, CBBP, Uni Lund, Sweden (2012-14).
#

################################################################################

def setallpar(infoDict):
    '''Sets the parameter values for all files.
    
    Written by Bettina Greese, CBBP, Uni Lund, Sweden (2012-13).'''
    
    import simsettings, templregions, pinauxregions
    from inputdata import cells, neighs, measurements, paramvalues
    from toolbox import rescale
    from simfiles import initcomp
    
    # Get variables from dictionary regarding the template:
    cellTemplate = infoDict['cellTemplate'] 
    auxinTemplate = infoDict['auxinTemplate']
    appTemplate = infoDict['appTemplate']
    xyzNumComp = infoDict['xyzNumComp']
    initCond = infoDict['initCond']
    consSys = infoDict['consSys']
    doRop = infoDict['doRop']
    del(infoDict)
    
    # Initialize dictionary for parameter values:
    parDict = {}
    
    # Add information about discretization in different directions:
    parDict['xNumComp'] = ["discret x dir", xyzNumComp[0]]
    parDict['yNumComp'] = ["discret y dir", xyzNumComp[1]]
    parDict['zNumComp'] = ["discret z dir", xyzNumComp[2]]
    del(xyzNumComp)
    
    # Add information about initial conditions for different substances:
    parDict['initAuxin'] = ["initial auxin", initCond[0]]
    parDict['initRopa'] = ["initial active ROP", initCond[1]]
    parDict['initRopi'] = ["initial inactive ROP", initCond[2]]
    del(initCond)
    
    # Add settings for simulation time and tolerance for stopping:
    simsettings.time(parDict)
    simsettings.tolerance(parDict, cellTemplate)
    
    # Add data for cell template (make sure that lengths are floats):
    cells.celltempl(parDict, cellTemplate)
    
    # Add data for auxin template (make sure that conc are floats):
    neighs.auxintempl(parDict, auxinTemplate)
    
    # Add data for measurements:
    measurements.measdata(parDict, cellTemplate)
    
    # Add model parameters:
    paramvalues.parameters(parDict, cellTemplate, doRop)
    if consSys == 1: # No creation or degradation of ropa/ropi for conserved system.
        for consPar in ['a', 'r', 'b', 's']:
            parDict[consPar][1] = 0.0
        del(consPar)
    
    # Add size of source and sink cells and external auxin application cell:
    simsettings.sosicells(parDict)
    
    # Add beginning and end of columella:
    templregions.columella(parDict)
    
    # Calculate quiescent center location in x and y direction:
    templregions.quiescent(parDict)
    
    # Add source/sink cells and lists for source/sink localization:
    templregions.sourcesink(parDict)
    
    # Add total number of compartments in each direction:
    initcomp.numcomp(parDict, cellTemplate)
    
    # Add list for PIN and AUX localization:
    pinauxregions.pinauxloc('pin', parDict)
    pinauxregions.pinauxloc('aux', parDict)
    
    # Rescale all relevant values from micrometer to decimeter:
    rescale.allpar(parDict, 'um2dm')
    
    return parDict

################################################################################

