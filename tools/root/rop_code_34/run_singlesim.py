#! /usr/bin/env python
#
# Written by Bettina Greese, CBBP, Uni Lund, Sweden (2012-14).
#

################################################################################
### REQUIRED USER INPUT: ###
############################

# CHOOSE MODEL:
baseName = 'rop'
consSys = 0 # Signal (=1) for conservedSystem, i.e. no prod/degr of ROPs.

# CHOOSE EXTERNAL AUXIN TEMPLATE:
# b and e are indices of the first and last cell in x dir to receive auxin:
# s and p are strengths relative to the source and PIN values (in percent)
appTemplate = ''

# CHOOSE IF .data FILE SHOULD BE DELETED TO SAVE DISK SPACE:
doKeepDataFile = 0

# CHOOSE IF CONCENTRATIONS SHOULD BE SAVED AFTER EACH RUN:
doKeepConc = 1

# CHOOSE IF SPECIAL TEST SHOULD BE RUN:
doSteadyState = 0

################################################################################
### USER INPUT FROM ARGUMENTS: ###
##################################

# Import necessary modules:
import time, os, pickle, sys
from toolbox import dirnavi, variables
from simfiles import parvalues, simsettings
from execsim import auxinsim, ropsim, ropsimpercell

# Get path to folder for simulation output and results:
outPath = simsettings.outpath()

# Get default discretization and initial conditons:
[xyzNumCompDef, initCondDef] = simsettings.defaults()

# Get information from input arguments:
argList = sys.argv
[doRop, cellTemplate, auxinTemplate, varType, xyzNumComp, initCond] = variables.inputargs(argList)
del(varType)
if xyzNumComp == []:
    xyzNumComp = map(int, xyzNumCompDef)
del(xyzNumCompDef)
if initCond == []:
    initCond = map(float, initCondDef)
del(initCondDef)

################################################################################

# Print user input for checking:
print(baseName, consSys, cellTemplate, auxinTemplate, appTemplate, xyzNumComp, initCond)
raw_input('Check settings and press any key to continue:')

# Start timer:
startTime = time.time()

# Get code version number:
vNrStr = dirnavi.codeversion()

# Collect information about template into a dictionary:
infoDict = variables.templinfo(baseName, vNrStr, cellTemplate, auxinTemplate, \
                               appTemplate, xyzNumComp, initCond, consSys)
del(initCond)

# Get parameter values:
parDict = parvalues.setallpar(infoDict)

# Set names of directories for different levels:
levelList = dirnavi.dirlevel(infoDict, parDict)

# Check if pickled parDict from template exists (as example):
pickleName = outPath + levelList[0] + levelList[1] + levelList[2] \
             + 'template/' + baseName + '_parDict.pkl'
if os.path.exists(pickleName) == False:
    raise Exception('No variable with parameters found, make template first (use run_simfiles.py).')

# Update parDict regarding tolerance for stopping (update of levelList needed!):
simsettings.tolerance(parDict, infoDict['cellTemplate'])
if doSteadyState == 1:
    # Update stopping criteria:
    parDict['sTolA'][1] = 100.0
    parDict['sCountA'][1] = 60
    print(parDict['sTolA'])
    print(parDict['sCountA'])
    print("NOTE: Special stopping criteria for checking steady state!")
    raw_input()
    
    # Update names of directories for different levels:
    levelList = dirnavi.dirlevel(infoDict, parDict)

# Set special values for some parameters (update of levelList needed!):
del(levelList)
#parDict['Du'][1] = 10.0*parDict['Du'][1]
#parDict['Dv'][1] = 10.0*parDict['Dv'][1]
#parDict['q'][1] = 10.0*parDict['q'][1]
#parDict['q'][1] = 0.1*parDict['q'][1]
#parDict['Pa'][1] = 0.0
#parDict['Paux'][1] = 0.0
#parDict['Ppin'][1] = 0.0
#parDict['Da'][1] = 0.0
#parDict['q'][1] = 0.0
#parDict['pqc'][1] = 10.0*parDict['pqc'][1]
#parDict['so'][1] = 0.01*parDict['so'][1]
#print("NOTE: Some parameters have special values!")
#raw_input()

# Update names of directories for different levels:
levelList = dirnavi.dirlevel(infoDict, parDict)

# Change directory:
dirnavi.mkchdir(outPath + levelList[0])
dirnavi.mkchdir(levelList[1])
dirnavi.mkchdir(levelList[2])
dirnavi.mkchdir(levelList[3])
dirnavi.mkchdir(levelList[4])

# Pickle parDict (i.e. save as a variable):
pickleName = baseName + '_parDict.pkl'
fileObj = open(pickleName, 'w')
pickle.dump(parDict, fileObj)
fileObj.close()
print("Saved file " + pickleName)
del(pickleName, fileObj)

# Save list of parameter values as text:
variables.partext(baseName, parDict)

# Run simulation for auxin:
auxinsim.simulate(baseName, parDict, levelList, doKeepDataFile, doKeepConc)

# Save path to auxin simulation directory:
currPath = dirnavi.simpath('auxin', levelList)
pathList = [[currPath], []]
del(currPath)

if doRop == 1:
    # Change directory:
    dirnavi.mkchdir('../' + levelList[7])
    dirnavi.mkchdir(levelList[8])
    dirnavi.mkchdir(levelList[9])
    
    # Pickle parDict (i.e. save as a variable):
    pickleName = baseName + '_parDict.pkl'
    fileObj = open(pickleName, 'w')
    pickle.dump(parDict, fileObj)
    fileObj.close()
    print("Saved file " + pickleName)
    del(pickleName, fileObj)

    # Save list of parameter values as text:
    variables.partext(baseName, parDict)
    
    # Run simulation for ROP:
    ropsim.simulate(baseName, parDict, levelList, \
                    doKeepDataFile, doKeepConc)
    '''
    # If stopping criteria are not the same for all cells, use the following:
    if 'single' in levelList[1]:
        # Run ROP simulation for complete template:
        ropsim.simulate(baseName, parDict, levelList, \
                        doKeepDataFile, doKeepConc)
    else:
        # Run ROP simulation per cell:
        ropsimpercell.simulate(baseName, parDict, levelList, doKeepDataFile)
    '''
    
    # Save path to ROP simulation directory:
    currPath = dirnavi.simpath('rop', levelList)
    pathList[1].append(currPath)
    del(currPath)

    # Change directory:
    os.chdir('../../')

del(levelList)

dirnavi.mkchdir('../../../../../results' + vNrStr)
dirnavi.mkchdir('default')
del(vNrStr)

# Check number of previous files of given type:
maxNum = dirnavi.numfiles('.txt', 'default', cellTemplate, \
                          auxinTemplate, xyzNumComp)

# Set name for current run of current project:
countName = cellTemplate + '_' + auxinTemplate + appTemplate + '_' \
    + str(xyzNumComp[0]) + '_' + str(xyzNumComp[1]) + '_' \
    + str(xyzNumComp[2]) + '_default_' + str(maxNum+1)
del(cellTemplate, auxinTemplate, appTemplate, xyzNumComp, consSys)

# Save list with paths to simulation directories as text:
fileName = countName + '.txt'
fileObj = open(fileName, 'w')
for simPhase in [0, 1]:
    for currPath in pathList[simPhase]:
        if currPath != '':
            fileObj.write(currPath + '\n')
fileObj.close()
print("Saved file " + fileName)
del(fileName, fileObj, pathList)

# Calculate run time:
endTime = time.time()
runTime = endTime - startTime
print('Run time was ' + str(runTime/3600) + ' hours')

