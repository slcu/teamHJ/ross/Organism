#! /usr/bin/env python
#
# Written by Bettina Greese, CBBP, Uni Lund, Sweden (2012-14).
#

################################################################################
### REQUIRED USER INPUT: ###
############################

# CHOOSE WHICH RUN OF CURRENT PROJECT SHOULD BE USED:
resNumDef = 'last'

# SET LIST OF ALLOWED VARIATIONS:
projNameCheckList = ['stopcrit_sTolA', 'stopcrit_sCountA', 'stopcrit_sTolR' \
, 'stopcrit_sCountR', 'vartempl_lateralN' \
, 'parvar_so', 'parvar_si', 'parvar_pqc', 'parvar_pK', 'parvar_pHc' \
, 'parvar_pHw', 'parvar_pHc', 'parvar_pHw', 'parvar_Pa', 'parvar_Ppin' \
, 'parvar_Paux', 'parvar_q', 'parvar_Da', 'parvar_Dw', 'parvar_Du' \
, 'parvar_Dv', 'parvar_c', 'parvar_k1', 'parvar_k2', 'parvar_k3', 'parvar_k4', 'parvar_r', 'parvar_s' \
, 'parvar_b', 'parvar_so', 'parvar_si', 'parvar_Ppin', 'parvar_Paux'\
, 'parvar_Pa', 'vartempl_lateral', 'initcond_auxin', 'initcond_roptotal' \
, 'initcond_ropafrac']

# CHOOSE MODEL:
baseName = 'rop'
consSys = 0 # Signal (=1) for conservedSystem, i.e. no prod/degr of ROPs.

# CHOOSE EXTERNAL AUXIN TEMPLATE:
# b and e are indices of the first and last cell in x dir to receive auxin:
# s and p are strengths relative to the source and PIN values (in percent)
appTemplate = ''

################################################################################
### USER INPUT FROM ARGUMENTS: ###
##################################

# Import necessary modules:
import os, pickle, copy, numpy, sys
from toolbox import dirnavi, variables
from simfiles import parvalues, simsettings
from plotfiles import plotsettings, forplots, curvesperlayer
from plotfiles import meanconc, meanconcpcps
from plotfiles import secgrad, totalsubst, secgraddiff, curvesperlayerrop
from execplot import simplots
from latexfiles import resultstex, forlatex, vareffects

# Get path to folder for simulation output and results:
outPath = simsettings.outpath()

# Get default discretization and initial conditons:
[xyzNumCompDef, initCondDef] = simsettings.defaults()

# Get information from input arguments:
argList = sys.argv
[doRop, cellTemplate, auxinTemplate, projName, xyzNumComp, initCond] = variables.inputargs(argList)
if projName not in projNameCheckList:
    raise Exception('Given project name is not in allowed list.')
del(projNameCheckList)
projNameList = [projName]
del(projName)
if xyzNumComp == []:
    xyzNumComp = map(int, xyzNumCompDef)
del(xyzNumCompDef)
if initCond == []:
    initCond = map(float, initCondDef)
del(initCondDef)

# ASK USER WHICH RUN TO USE:
resNum = raw_input("Which run schould be used? Press enter for 'last' or type number: ")
if resNum == '':
    resNum = resNumDef
else:
    resNum = int(resNum)
del(resNumDef)

################################################################################

# Print user input for checking:
print(baseName, consSys, cellTemplate, auxinTemplate, appTemplate, xyzNumComp, \
      initCond, projNameList, resNum)
raw_input('Check settings and press any key to continue:')

# Set list of plots to be made:
plotList = ['mean_per_cell']
if doRop == 0:
    substList = ['auxin']
elif doRop == 1:
    substList = ['auxin', 'ropa', 'ropi']

# Get code version number:
vNrStr = dirnavi.codeversion()

# Get column indices for .final file:
[colIndDict, colList] = variables.colind('.final')
del(colList)

# Collect information about template into a dictionary:
infoDict = variables.templinfo(baseName, vNrStr, cellTemplate, auxinTemplate, \
                               appTemplate, xyzNumComp, initCond, consSys)
del(initCond)

# Get parameter values:
origParDict = parvalues.setallpar(infoDict)

# Get number of sections for summed masses:
numSecs = origParDict['numCuts'][1] - 1

# Set names of directories for different levels:
origLevelList = dirnavi.dirlevel(infoDict, origParDict)
del(infoDict)

# Add settings for visualization:
plotsettings.visual(origParDict)

# Get x and y values of sections for which conc curves will be plotted:
pickleName = '../../simu' + vNrStr + '/' + origLevelList[1] + origLevelList[2] \
    + 'template/' + baseName + '_comp.pkl'
forplots.getsections(baseName, origParDict, pickleName, 'auxin')
del(pickleName)

# Loop over parameters to be varied:
for currProjName in projNameList:
    
    # Change directory:
    os.chdir(outPath + 'results' + vNrStr + '/')
    os.chdir(currProjName)
    
    # Check how many runs of current project have been done:
    if 'discret' in currProjName:
        maxNum = dirnavi.numfiles('.txt', currProjName, cellTemplate, \
                                  auxinTemplate, [])
    elif 'vartempl_lateral' in currProjName:
        maxNum = dirnavi.numfiles('.txt', currProjName, cellTemplate, \
                                  'varAuxinTempl', xyzNumComp)
    else:
        maxNum = dirnavi.numfiles('.txt', currProjName, cellTemplate, \
                                  auxinTemplate, xyzNumComp)
    
    # Set name for chosen run of current project:
    if resNum == 'last':
        resNum = maxNum
    elif resNum > maxNum:
        resNum = maxNum
        print('Chosen number is too high, will take last result.')
    if 'discret' in currProjName:
        countName = cellTemplate + '_' + auxinTemplate + appTemplate \
            + '_' + currProjName + '_' + str(resNum)
    elif 'parvar' in currProjName \
    or 'stopcrit' in currProjName \
    or 'initcond' in currProjName:
        countName = cellTemplate + '_' + auxinTemplate + appTemplate + '_' \
            + str(xyzNumComp[0]) + '_' + str(xyzNumComp[1]) + '_' \
            + str(xyzNumComp[2]) + '_' + currProjName + '_' + str(resNum)
    elif 'vartempl_lateral' in currProjName:
        countName = cellTemplate + '_varAuxinTempl' + appTemplate + '_' \
            + str(xyzNumComp[0]) + '_' + str(xyzNumComp[1]) + '_' \
            + str(xyzNumComp[2]) + '_'  + currProjName + '_' + str(resNum)
    
    # Make first part of file for presentation:
    simPath = 'simu' + vNrStr
    text = resultstex.latexbeg(simPath, origParDict, cellTemplate, \
                                      auxinTemplate, appTemplate)
    del(simPath)
    
    # Load list with paths to simulation directories:
    fileName = countName + '.txt'
    fileObj = open(fileName, 'r')
    origPathList = fileObj.readlines()
    fileObj.close()
    print("Loaded file " + fileName)
    del(fileName, fileObj)
    
    
    # Format list with paths to simulation directories:
    pathList = [[], []] # Initialize list in correct format.
    for currPath in origPathList:
        currPath = currPath.replace('\n', '')
        if 'Asim' in currPath:
            pathList[0].append(currPath)
        elif 'Rsim' in currPath:
            pathList[1].append(currPath)
        else:
            raise Exception('Path can not be identified as auxin or ROP.')
    del(origPathList)
    
    # Plot mean substance concentrations and slopes:
    if 'mean_per_cell' in plotList:
        if doRop == 0:
            currPathList = pathList[0]
        elif doRop == 1:
            currPathList = pathList[1]
        if currPathList == []:
            raise Exception('Empty path list for current substance.')
        for subst in substList:
            meanconcpcps.gnuplot(baseName, currPathList, \
                         subst, currProjName, countName, '.eps')
        del(currPathList)
    
    # Plot auxin concentrations from different situations for each layer:
    for zoom in origParDict['zoomList'][1]:
        curvesperlayer.gnuplot(baseName, origParDict, pathList, \
                    currProjName, zoom, '.eps')
    
    # Plot mean auxin concentration in template:
    if 'single' not in cellTemplate:
        meanconc.gnuplot(baseName, pathList, \
                         currProjName, countName, '.eps')
    
    if doRop == 1:
        # Plot ropa concentrations from different situations for each layer:
        for zoom in origParDict['zoomList'][1]:
            curvesperlayerrop.gnuplot(baseName, origParDict, pathList, \
                        currProjName, zoom, '.eps', 'ropa')
            curvesperlayerrop.gnuplot(baseName, origParDict, pathList, \
                        currProjName, zoom, '.eps', 'ropi')
    
    if 'single' not in cellTemplate and 'Two' not in cellTemplate \
    and 'file' not in cellTemplate and 'epidermis' not in cellTemplate \
    and cellTemplate not in ['Grieneisen', 'Jones'] \
    and '7layersS' not in cellTemplate:
        # Make table with effects of variations on gradient level and slope:
        vareffects.tabpervar(baseName, origParDict, origLevelList, pathList, \
                             currProjName, countName)
        
        # Plot large scale auxin gradients:
        secgrad.gnuplot(baseName, origParDict, pathList, \
                        currProjName, countName, '.eps')
                        
        # Plot total auxin mass in whole template:
        totalsubst.gnuplot(baseName, pathList, \
                        currProjName, countName, '.eps')
        
        # Plot difference between simulated gradients and measured gradient:
        secgraddiff.gnuplot(baseName, origParDict, pathList, \
                        currProjName, countName, '.eps')
    
    # Make comparison section of presentation file:
    if doRop == 0:
        simPhaseList = ['A']
    elif doRop == 1:
        simPhaseList = ['A', 'R']
    if 'single' in cellTemplate or 'Two' in cellTemplate \
    or 'file' in cellTemplate or 'epidermis' in cellTemplate \
    or cellTemplate in ['Grieneisen', 'Jones'] \
    or '7layersS' in cellTemplate:
        addText = resultstex.latexcomp(baseName, simPhaseList, origParDict, \
                  currProjName, countName, 1, 0, 0, 0)
    else:
        addText = resultstex.latexcomp(baseName, simPhaseList, origParDict, \
                  currProjName, countName, 1, 1, 1, 1)
    text = text + addText
    del(addText, simPhaseList)
    
    # Make section of presentation with mean auxin concentration in template:
    if 'single' not in cellTemplate:
        addText = resultstex.latexmeanit(countName)
    
    # Make section for presentation with means per cell:
    if 'mean_per_cell' in plotList:
        addText = resultstex.latexmeanpcps(baseName, countName, currProjName, \
                  substList, origParDict['plotY'][1], origParDict['plotX'][1])
        text = text + addText
        del(addText)
    
    # Make end of presentation file:
    addText = resultstex.latexend()
    text = text + addText
    del(addText)
    
    # Save latex file and compile it:
    forlatex.latexfile(countName + '_comp', text)
    raw_input('Copy figures if needed!')
    
    # Delete auxiliary files from LaTeX:
    forlatex.dellatex(countName + '_comp')
    
    # Delete plots:
    delName = currProjName + '_curves_final_'
    fileList = os.listdir(os.getcwd())
    for fileName in fileList:
        if delName in fileName:
            os.remove(fileName)
    del(delName, fileName, fileList)
    
    # Delete comparison plots:
    fileList = os.listdir(os.getcwd())
    for fileName in fileList:
        currNamePart = currProjName + '_' + str(resNum) + '_'
        if currNamePart in fileName and '.eps' in fileName:
            os.remove(fileName)
        del(currNamePart)
    del(resNum, currProjName)
    
del(projNameList, origParDict)


