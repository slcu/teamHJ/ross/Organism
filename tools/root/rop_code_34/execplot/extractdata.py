#! /usr/bin/env python
# Module with functions related to extracting data from simulation result.
#
# Written by Bettina Greese, CBBP, Uni Lund, Sweden (2012-14).
#

################################################################################

def blocks(baseName, simPhase, parDict, levelList):
    '''Extracts chosen data blocks from .data file into matrices.
    
    Written by Bettina Greese, CBBP, Uni Lund, Sweden (2014).'''
    
    import pickle, numpy, os
    from toolbox import variables
    
    # Set path to .data file:
    if simPhase == 'A':
        pathName = '../' + levelList[4]
    elif simPhase == 'R':
        pathName = '../' + levelList[9]
    
    # Get column indices:
    [colIndDict, colList] = variables.colind('.data')
    del(colList)
    keyList = colIndDict.keys()
    
    # Get expected size of each block:
    numCols = len(keyList)
    numRows = parDict['xTotalComp'][1]*parDict['yTotalComp'][1]*parDict['zTotalComp'][1]
    
    # Get time step between blocks:
    timeStep = int(parDict['timeA'][1]/parDict['saveTime'][1])
    
    # Check if simulation has been finished:
    if os.path.exists(pathName + baseName + '.sums') == False:
        raise Exception('Simulations for current path are not finished.')
    if os.path.exists(pathName + baseName + '.data') == False:
        raise Exception('There is no .data file for simulations for current path.')
    
    # Open .data file:
    dataFileObj = open(pathName + baseName + '.data', 'r')
    del(pathName)
    
    # Loop over all lines in .data file:
    rowCount = 0 # Initialize counter of rows.
    timeVal = 0 # Initialize time point.
    currArray = numpy.zeros(shape = (numRows, numCols))
    while 1 == 1: # Loop over lines until break is found.
        line = dataFileObj.readline()
        lineElem = line.split(' ') # Get elements that were sep. by whitespaces.
        del(line)
        
        # Check type of line:
        if lineElem == ['']: # End of file.
            break
        elif lineElem == ['\n']: # Empty line (i.e. end of block).
            
            # Check size of array:
            if numpy.shape(currArray) != (numRows, numCols):
                raise Exception('Array has incorrect size/shape.')
            if sum(sum(currArray)) == 0.0:
                raise Exception('Array does not contain data.') 
            
            # Pickle array from previous time point:
            pickleName = baseName + '_data_array_' + str(timeVal) + '.pkl'
            arrayFileObj = open(pickleName, 'w')
            pickle.dump(currArray, arrayFileObj)
            arrayFileObj.close()
            print('Saved file ' + pickleName)
            del(pickleName, arrayFileObj)
            del(currArray)
            
            # Make new array for current time point:
            currArray = numpy.zeros(shape = (numRows, numCols))
            rowCount = 0 # Restart counter for rows.
            timeVal += timeStep # Update time point.
        elif len(lineElem) != numCols + 1: # Line with data.
            raise Exception('Number of entries does not match number of columns.')
        else:
            # Get data from current line:
            lineElem.remove('\n') # Remove end-of-line character.
            lineElem = map(float, lineElem) # Convert strings to floats.
            if lineElem[colIndDict['timeval']] != timeVal:
                print(lineElem[colIndDict['timeval']], timeVal)
                raise Exception('Problem with time point.')
            
            # Put data (as one-row array) into (2D) array for current time point:
            currArray[rowCount, :] = numpy.array(lineElem)
            rowCount += 1 # Update row counter.
        del(lineElem)
    del(colIndDict, timeStep, numRows, numCols)
    
    # Close .data file:
    dataFileObj.close()
    del(dataFileObj)
    
    return timeVal

################################################################################


