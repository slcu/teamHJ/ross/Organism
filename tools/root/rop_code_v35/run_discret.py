#! /usr/bin/env python
#
# Written by Bettina Greese, CBBP, Uni Lund, Sweden (2013-15).
#

################################################################################
### REQUIRED USER INPUT: ###
############################

# CHOOSE MODEL:
baseName = 'rop'
consSys = 0 # Signal (=1) for conservedSystem, i.e. no prod/degr of ROPs.

# CHOOSE EXTERNAL AUXIN TEMPLATE:
# b and e are indices of the first and last cell in x dir to receive auxin:
# s and p are strengths relative to the source and PIN values (in percent)
appTemplate = ''

# CHOOSE DISCRETIZATIONS TO BE COMPARED (each x with each y):
# Num of comp in x dir per cell:
xnDiscretList = [10, 20, 30, 40, 50]
#xnDiscretList = [10, 20, 30]
#xnDiscretList = [5, 10, 20, 30]
#xnDiscretList = [5, 10, 20, 30, 50]
xnDiscretList = range(10, 110, 10)
#xnDiscretList = range(30, 110, 10)
#xnDiscretList = range(30, 210, 10)
#xnDiscretList = range(30, 160, 10)
# Number of compartments in y direction per cell:
ynDiscretList = [1]
# Number of compartments in z direction per cell:
zncomp = 1

# CHOOSE SPECIAL TEST:
ropNothing = 0 # no diff
ropOnlyProd = 0 # no diff
ropOnlyActiv = 0
ropOnlyProdActiv = 0 # no diff
ropOnlyTransfer = 0 # no diff
ropProdDiff = 0
ropProdDiffTransfer = 0 # smooth change
ropProdDiffDegr = 0 # no diff
ropProdDiffActiv = 0 # no diff

# CHOOSE IF .data FILE SHOULD BE DELETED TO SAVE DISK SPACE:
doKeepDataFile = 0

# CHOOSE IF CONCENTRATIONS SHOULD BE SAVED AFTER EACH RUN:
doKeepConc = 1

################################################################################
### USER INPUT FROM ARGUMENTS: ###
##################################

# Import necessary modules:
import time, os, pickle, copy, sys
from toolbox import dirnavi, variables
from simfiles import parvalues, simsettings, varsettings
from execsim import auxinsim, ropsim

# Get path to folder for simulation output and results:
outPath = simsettings.outpath()

# Construct project name:
projName = os.path.basename(__file__)
projName = projName[4:-3]

# Get information from input arguments:
argList = sys.argv
[doRop, cellTemplate, auxinTemplate, varType, xyzNumComp, initCond, \
simStrategy] = variables.arguments(argList)
del(xyzNumComp, varType)

################################################################################

# Print user input for checking:
print(baseName, consSys, cellTemplate, auxinTemplate, appTemplate, xnDiscretList, ynDiscretList, zncomp, initCond, simStrategy)
raw_input('Check settings and press any key to continue:')

# Start timer:
startTime = time.time()

# Get code version number:
vNrStr = dirnavi.codeversion()

# Change directory:
dirnavi.mkchdir(outPath)
del(outPath)

# Loop over discretization in x direction:
pathList = [[],[]] # Initialize list for paths to simulation directories.
for xncomp in xnDiscretList:
    
    # Loop over discretization in y direction:
    for yncomp in ynDiscretList:
        
        # Get discretization:
        xyzNumComp = [xncomp, yncomp, zncomp]
        
        # Collect information about template into a dictionary:
        infoDict = variables.templinfo(baseName, vNrStr, cellTemplate, \
                   auxinTemplate, appTemplate, xyzNumComp, initCond, consSys, doRop)
        
        # Get parameter values:
        parDict = parvalues.setallpar(infoDict)
        
        # Set special parameter values:
        if ropOnlyProd == 1:
            keepParNameList = ['a', 'b']
        elif ropOnlyActiv == 1:
            keepParNameList = ['k1', 'k2', 'k3', 'k4']
        elif ropOnlyProdActiv == 1:
            keepParNameList = ['a', 'b', 'k1', 'k2', 'k3', 'k4']
        elif ropOnlyTransfer == 1:
            keepParNameList = ['k1', 'k2', 'k3', 'k4', 'c']
        elif ropProdDiff == 1:
            keepParNameList = ['a', 'b', 'Du', 'Dv']
        elif ropProdDiffTransfer == 1:
            keepParNameList = ['a', 'b', 'Du', 'Dv', 'k1', 'k2', 'k3', 'k4', 'c']
        elif ropProdDiffDegr == 1:
            keepParNameList = ['a', 'b', 'Du', 'Dv', 'r', 's']
        elif ropProdDiffActiv == 1:
            keepParNameList =['a', 'b', 'Du', 'Dv', 'k1', 'k2', 'k3', 'k4']
        else:
            keepParNameList = []
        if keepParNameList != []:
            parNameDict = dirnavi.getparnames()
            for parName in parNameDict['rop']:
                if parName not in keepParNameList:
                    parDict[parName][1] = 0.0
        if ropNothing == 1:
            parNameDict = dirnavi.getparnames()
            for parName in parNameDict['rop']:
                parDict[parName][1] = 0.0
        
        # Set specific parameter values for testing:
        #parDict['so'][1] = 0.01
        #parDict['sCountA'][1] = 60
        #parDict['c'][1] = 1.0
        #print('Special parameter set!')
        #raw_input()
        
        # Set names of directories for different levels:
        levelList = dirnavi.dirlevel(infoDict, parDict)
        
        # Update parDict regarding tolerance for stopping:
        simsettings.tolerance(parDict, infoDict['cellTemplate'])
        del(infoDict)
        
        # Change directory:
        dirnavi.mkchdir(levelList[0])
        dirnavi.mkchdir(levelList[1])
        dirnavi.mkchdir(levelList[2])
        
        # Check if template directory exists:
        fileName = 'template' 
        if os.path.exists(fileName) == False:
            print(xyzNumComp)
            raise Exception('No template found, make template first.')
        del(xyzNumComp)
        
        # Check for existing auxin simulation directory (levels 3 and 4):
        doSim = dirnavi.skipdir('auxin', levelList, simStrategy)
        
        # Change directory:
        dirnavi.mkchdir(levelList[3])
        dirnavi.mkchdir(levelList[4])
        
        if doSim == 1:
            # Pickle parDict (i.e. save as a variable):
            pickleName = baseName + '_parDict.pkl'
            fileObj = open(pickleName, 'w')
            pickle.dump(parDict, fileObj)
            fileObj.close()
            print("Saved file " + pickleName)
            del(pickleName, fileObj)
            
            # Save list of parameter values as text:
            variables.partext(baseName, parDict)
            
            # Run simulation for auxin:
            auxinsim.simulate(baseName, parDict, levelList, \
                              doKeepDataFile, doKeepConc)
        
        del(doSim)

        # Save path to auxin simulation directory:
        currPath = dirnavi.simpath('auxin', levelList)
        pathList[0].append(currPath)
        del(currPath)
        
        if doRop > 0:
            # Change directory:
            dirnavi.mkchdir('../' + levelList[7])
            
            # Check for existing ROP simulation directory (levels 8 and 9):
            doSim = dirnavi.skipdir('rop', levelList, simStrategy)
            
            # Change directory:
            dirnavi.mkchdir(levelList[8])
            dirnavi.mkchdir(levelList[9])
            
            if doSim == 1:
                # Pickle parDict (i.e. save as a variable):
                pickleName = baseName + '_parDict.pkl'
                fileObj = open(pickleName, 'w')
                pickle.dump(parDict, fileObj)
                fileObj.close()
                print("Saved file " + pickleName)
                del(pickleName, fileObj)

                # Save list of parameter values as text:
                variables.partext(baseName, parDict)
                
                # Run simulation for ROP:
                ropsim.simulate(baseName, parDict, levelList, \
                                doKeepDataFile, doKeepConc)
            
            del(doSim)

            # Save path to ROP simulation directory:
            currPath = dirnavi.simpath('rop', levelList)
            pathList[1].append(currPath)
            del(currPath)

            # Change directory:
            os.chdir('../../')
        
        del(levelList, parDict)
        
        # Change directory:
        os.chdir('../../../../..')
        
        del(yncomp)
    del(xncomp)
del(xnDiscretList, ynDiscretList, initCond)

# Change directory:
dirnavi.mkchdir('results' + vNrStr + '/')
dirnavi.mkchdir(projName)

# Check number of previous files of given type:
maxNum = dirnavi.numfiles('.txt', projName, cellTemplate, \
                          auxinTemplate, [])

# Set name for current run of current project:
countName = cellTemplate + '_' + auxinTemplate + appTemplate \
            + '_' + projName + '_' + str(maxNum+1)
del(cellTemplate, auxinTemplate, appTemplate, consSys)

# Save list with paths to simulation directories as text:
fileName = countName + '.txt'
fileObj = open(fileName, 'w')
for simPhase in [0, 1]:
    for currPath in pathList[simPhase]:
        if currPath != '':
            fileObj.write(currPath + '\n')
fileObj.close()
print('Saved file ' + fileName)
del(fileName, fileObj, pathList)

# Calculate run time:
endTime = time.time()
runTime = endTime - startTime
del(startTime, endTime)
[runHr, runRem] = divmod(runTime, 3600)
[runMin, runRest] = divmod(runRem, 60)
print('Run time: ' + str(int(runHr)) + ' hrs, ' + str(int(runMin)) + ' mins')
del(runTime, runHr, runRem, runMin, runRest)
