#! /usr/bin/env python
#
# Written by Bettina Greese, CBBP, Uni Lund, Sweden (2012-14).
#

################################################################################
### REQUIRED USER INPUT: ###
############################

# CHOOSE MODEL:
baseName = 'rop'
consSys = 0 # Signal (=1) for conservedSystem, i.e. no prod/degr of ROPs.

# CHOOSE EXTERNAL AUXIN TEMPLATE:
# b and e are indices of the first and last cell in x dir to receive auxin:
# s and p are strengths relative to the source and PIN values (in percent)
appTemplate = ''

################################################################################
### USER INPUT FROM ARGUMENTS: ###
##################################

# Import necessary modules:
import os, time, copy, subprocess, pickle, sys
from toolbox import dirnavi, variables
from simfiles import parvalues, simsettings
from simfiles import shellbinfiles, initfile, neighfile, modelfile
from plotfiles import templateplot, plotsettings
from latexfiles import templatetex, forlatex

# Get path to folder for simulation output and results:
outPath = simsettings.outpath()

# Get information from input arguments:
argList = sys.argv
argList.extend(['-v', 'none'])
[doRop, cellTemplate, auxinTemplate, varType, xyzNumComp, initCond, \
simStrategy] = variables.arguments(argList)
del(varType, simStrategy)

################################################################################

# Print user input for checking:
print(baseName, consSys, cellTemplate, auxinTemplate, appTemplate, xyzNumComp, initCond)
#raw_input('Check settings and press any key to continue:')

# Get code version number:
vNrStr = dirnavi.codeversion()

# Collect information about template into a dictionary:
infoDict = variables.templinfo(baseName, vNrStr, cellTemplate, auxinTemplate, \
                               appTemplate, xyzNumComp, initCond, consSys, doRop)
del(vNrStr, auxinTemplate, appTemplate, xyzNumComp, initCond, consSys, doRop)

# Get parameter values:
parDict = parvalues.setallpar(infoDict)

# Add settings for visualization:
plotsettings.visual(parDict)

# Set names of directories for different levels:
levelList = dirnavi.dirlevel(infoDict, parDict)

# Change directory:
dirnavi.mkchdir(outPath + levelList[0])
dirnavi.mkchdir(levelList[1])
dirnavi.mkchdir(levelList[2])
dirnavi.mkchdir('template/')
del(levelList)

# Make shell scripts for Organism simulations:
shellbinfiles.osim('A')
shellbinfiles.osim('R')

# Make .init and .costtemplate files:
initfile.init('init', infoDict, parDict)
if 'single' not in cellTemplate and '7layersS2' not in cellTemplate:
    initfile.init('costtemplate', infoDict, parDict)
del(cellTemplate)

# Make .neigh file:
neighfile.neigh(baseName, parDict)

# Make .model files:
modelfile.model(baseName, parDict, 'A')
modelfile.model(baseName, parDict, 'R')

# Pickle parDict (i.e. save as a variable):
pickleName = baseName + '_parDict.pkl'
fileObj = open(pickleName, 'w')
pickle.dump(parDict, fileObj)
fileObj.close()
print("Saved file " + pickleName)
del(pickleName, fileObj)

# Set template name:
templName = infoDict['cellTemplate'] + '_' + infoDict['auxinTemplate'] \
          + infoDict['appTemplate']

# Make plots of template:
for zoom in parDict['zoomList'][1]:
    templateplot.gnuplot(baseName, templName, parDict, zoom)
    del(zoom)

# Make file for presentation of template:
text = templatetex.latextempl(templName, parDict)
fileName = templName + '_templ'
forlatex.latexfile(fileName, text)

# Delete plots and auxiliary files from LaTeX:
delList = ['ind.eps', 'auxinIn.eps', 'V.eps', 'auxinOut.eps' \
         , 'auxinProd.eps', 'pinMarker.eps', 'auxMarker.eps' \
         , 'cellLabel.eps']
for delFile in delList:
    for zoom in parDict['zoomList'][1]:
        os.remove(templName + '_' + str(zoom) + 'dm_' + delFile)
del(delFile, delList, templName)
del(fileName)

