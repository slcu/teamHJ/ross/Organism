#! /usr/bin/env python
# Module with small functions related to LaTeX documents.
#
# Written by Bettina Greese, CBBP, Uni Lund, Sweden (2014).
#

################################################################################

def latexfile(commonName, text):
    '''Saves the .tex file for results of ROP model and compiles it.
    
    Written by Bettina Greese, CBBP, Uni Lund, Sweden (2014).'''
    
    import subprocess, os
    
    # Save latex file:
    fileName = commonName + '.tex'
    fileObj = open(fileName, 'w')
    fileObj.write(text)
    fileObj.close()
    del(fileObj)
    print('Saved file ' + fileName)
    
    # Compile latex file:
    subprocess.call(['latex', '-interaction=nonstopmode', fileName])
    del(fileName)
    #subprocess.call(['dvipdfm', commonName + '.dvi'])
    subprocess.call(['dvipdf', commonName + '.dvi'])
    del(commonName)
    
    return

################################################################################

def dellatex(commonName):
    '''Deletes auxiliary files from LaTeX.
    
    Written by Bettina Greese, CBBP, Uni Lund, Sweden (2014).'''
    
    import os
    
    # Delete .tex, .aux, .dvi, and .log files:
    fileList = ['.tex', '.aux', '.dvi', '.log']
    for currFile in fileList:
        fileName = commonName + currFile
        os.remove(fileName)
        del(fileName)
    print('Deleted latex auxiliary files for ' + commonName)
    del(currFile, fileList, commonName)
    
    return

################################################################################




