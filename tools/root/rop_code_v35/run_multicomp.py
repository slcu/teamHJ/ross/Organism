#! /usr/bin/env python
#
# Written by Bettina Greese, CBBP, Uni Lund, Sweden (2012-15).
#

################################################################################
### REQUIRED USER INPUT: ###
############################

# CHOOSE WHICH RUN OF CURRENT PROJECT SHOULD BE USED:
resNumDef = 'last'

# SET LIST OF ALLOWED VARIATIONS:
projCheckList = ['stopcrit', 'vartempl', 'parvar', 'parvar1', 'parvar2' \
              , 'initcond', 'manual']

# CHOOSE MODEL:
baseName = 'rop'
consSys = 0 # Signal (=1) for conservedSystem, i.e. no prod/degr of ROPs.

# CHOOSE EXTERNAL AUXIN TEMPLATE:
# b and e are indices of the first and last cell in x dir to receive auxin:
# s and p are strengths relative to the source and PIN values (in percent)
appTemplate = ''

################################################################################
### USER INPUT FROM ARGUMENTS: ###
##################################

# Import necessary modules:
import os, pickle, copy, numpy, sys
from toolbox import dirnavi, variables
from simfiles import parvalues, simsettings
from plotfiles import plotsettings, forplots, curvesperlayer
from plotfiles import meanconc, statspercell
from plotfiles import secgrad, totalsubst, secgraddiff, curvesperlayerrop
from execplot import simplots
from latexfiles import resultstex, forlatex, vareffects

# Get path to folder for simulation output and results:
outPath = simsettings.outpath()

# Get information from input arguments:
argList = sys.argv
[doRop, cellTemplate, auxinTemplate, projName, xyzNumComp, initCond, \
simStrategy] = variables.arguments(argList)
del(simStrategy)
if projName[:projName.find('_')] not in projCheckList:
    raise Exception('Given project name is not in allowed list.')
del(projCheckList)

# ASK USER WHICH RUN TO USE:
print('Which run schould be used?')
resNum = raw_input('Press enter for "last" or type number or type range (with "-"): ')
if resNum == '':
    resNumList = [resNumDef]
elif '-' in resNum:
    begNum = resNum[:resNum.find('-')]
    endNum = resNum[resNum.find('-')+1:]
    resNumList = range(int(begNum), int(endNum)+1)
    del(begNum, endNum)
else:
    resNumList = [int(resNum)]
del(resNumDef)

################################################################################

# Print user input for checking:
print(baseName, consSys, cellTemplate, auxinTemplate, appTemplate, xyzNumComp, \
      initCond, projName, resNumList)
raw_input('Check settings and press any key to continue:')

# Set list of plots to be made:
plotList = ['stats_per_cell']
if doRop == 0:
    substList = ['auxin']
elif doRop > 0:
    substList = ['auxin', 'ropa', 'ropi', 'roptotal', 'ropratio']

# Get code version number:
vNrStr = dirnavi.codeversion()

# Get column indices for .final file:
[colIndDict, colList] = variables.colind('.final')
del(colList)

# Collect information about template into a dictionary:
infoDict = variables.templinfo(baseName, vNrStr, cellTemplate, auxinTemplate, \
                               appTemplate, xyzNumComp, initCond, consSys, doRop)
del(initCond)

# Get parameter values:
origParDict = parvalues.setallpar(infoDict)

# Get number of sections for summed masses:
numSecs = origParDict['numCuts'][1] - 1

# Set names of directories for different levels:
origLevelList = dirnavi.dirlevel(infoDict, origParDict)
del(infoDict)

# Add settings for visualization:
plotsettings.visual(origParDict)

# Get x and y values of sections for which conc curves will be plotted:
pickleName = '../../simu' + vNrStr + '/' + origLevelList[1] + origLevelList[2] \
    + 'template/' + baseName + '_comp.pkl'
forplots.getsections(baseName, origParDict, pickleName, 'auxin')
del(pickleName)

# Loop over result file numbers:
for resNum in resNumList:
    
    # Change directory:
    os.chdir(outPath + 'results' + vNrStr + '/')
    os.chdir(projName)
    
    # Check if setName is given:
    if 'parvar' in projName:
        numUn = projName.count('_')
        if numUn == 1:
            currSetName = ''
            currVarName = projName
        elif numUn == 2:
            indUn1 = projName.find('_')
            indUn2 = projName.rfind('_')
            currSetName = projName[indUn1+1:indUn2]
            currVarName = projName[indUn2+1:]
        else:
            raise Exception('Unknown project name.')
    print(projName, currSetName, currVarName)
    
    # Find value of set parameter:
    #print(levelList)
    #raw_input()
    
    # Copy original parameter values and update value for set:
    #setParDict = copy.deepcopy(origParDict)
    #print(setParDict[currSetName])
    #setParDict[currSetName][1] = 
    #print(
    #raw_input()
    
    # Check how many runs of current project have been done:
    if 'discret' in projName:
        maxNum = dirnavi.numfiles('.txt', projName, cellTemplate, \
                                  auxinTemplate, [])
    elif 'vartempl_lateral' in projName:
        maxNum = dirnavi.numfiles('.txt', projName, cellTemplate, \
                                  'varAuxinTempl', xyzNumComp)
    else:
        maxNum = dirnavi.numfiles('.txt', projName, cellTemplate, \
                                  auxinTemplate, xyzNumComp)
    
    # Set name for chosen run of current project:
    if resNum == 'last':
        resNum = maxNum
    elif resNum > maxNum:
        resNum = maxNum
        print('Chosen number is too high, will take last result.')
    if 'discret' in projName:
        countName = cellTemplate + '_' + auxinTemplate + appTemplate \
            + '_' + projName + '_' + str(resNum)
    elif 'parvar' in projName \
    or 'stopcrit' in projName \
    or 'initcond' in projName:
        countName = cellTemplate + '_' + auxinTemplate + appTemplate + '_' \
            + str(xyzNumComp[0]) + '_' + str(xyzNumComp[1]) + '_' \
            + str(xyzNumComp[2]) + '_' + projName + '_' + str(resNum)
    elif 'vartempl_lateral' in projName:
        countName = cellTemplate + '_varAuxinTempl' + appTemplate + '_' \
            + str(xyzNumComp[0]) + '_' + str(xyzNumComp[1]) + '_' \
            + str(xyzNumComp[2]) + '_'  + projName + '_' + str(resNum)
    
    # Make first part of file for presentation:
    simPath = 'simu' + vNrStr
    text = resultstex.latexbeg(simPath, origParDict, cellTemplate, \
                                      auxinTemplate, appTemplate)
    #text = resultstex.latexbeg(simPath, setParDict, cellTemplate, \
    #                                  auxinTemplate, appTemplate)
    del(simPath)
    
    # Load list with paths to simulation directories:
    fileName = countName + '.txt'
    fileObj = open(fileName, 'r')
    origPathList = fileObj.readlines()
    fileObj.close()
    print("Loaded file " + fileName)
    del(fileName, fileObj)
    
    # Format list with paths to simulation directories:
    pathList = [[], []] # Initialize list in correct format.
    for currPath in origPathList:
        currPath = currPath.replace('\n', '')
        if 'Asim' in currPath:
            pathList[0].append(currPath)
        elif 'Rsim' in currPath:
            pathList[1].append(currPath)
        else:
            raise Exception('Path can not be identified as auxin or ROP.')
    del(origPathList)
    
    # Plot statistics for substance concentrations per cell:
    if 'stats_per_cell' in plotList:
        if doRop == 0:
            currPathList = pathList[0]
        elif doRop > 0:
            currPathList = pathList[1]
        if currPathList == []:
            raise Exception('Empty path list for current substance.')
        for subst in substList:
            statspercell.gnuplot(baseName, currPathList, \
                         subst, projName, countName, '.eps')
        del(currPathList)
    
    # Plot auxin concentrations from different situations for each layer:
    for zoom in origParDict['zoomList'][1]:
        curvesperlayer.gnuplot(baseName, origParDict, pathList, \
                    projName, zoom, '.eps')
        #curvesperlayer.gnuplot(baseName, origParDict, pathList, \
        #            projName, zoom, '.png')

    # EDIT! The following is incorrect!
    # Plot mean auxin concentration in template:
    #if 'single' not in cellTemplate:
    #    meanconc.gnuplot(baseName, pathList, \
    #                     projName, countName, '.eps')
    
    if doRop > 0:
        # Plot ropa concentrations from different situations for each layer:
        for zoom in origParDict['zoomList'][1]:
            curvesperlayerrop.gnuplot(baseName, origParDict, pathList, \
                        projName, zoom, '.eps', 'ropa')
            #curvesperlayerrop.gnuplot(baseName, origParDict, pathList, \
            #            projName, zoom, '.png', 'ropa')
            curvesperlayerrop.gnuplot(baseName, origParDict, pathList, \
                        projName, zoom, '.eps', 'ropi')
    
    if 'single' not in cellTemplate and 'Two' not in cellTemplate \
    and cellTemplate not in ['Grieneisen', 'Jones']:
        # Make table with effects of variations on gradient level and slope:
        #vareffects.tabpervar(baseName, origParDict, origLevelList, pathList, \
        #                     projName, countName)
        
        # Plot large scale auxin gradients:
        secgrad.gnuplot(baseName, origParDict, pathList, \
                        projName, countName, '.eps')
                        
        # Plot total auxin mass in whole template:
        totalsubst.gnuplot(baseName, pathList, \
                        projName, countName, '.eps')
        
        # Plot difference between simulated gradients and measured gradient:
        secgraddiff.gnuplot(baseName, origParDict, pathList, \
                        projName, countName, '.eps')
    
    # Make comparison section of presentation file:
    if doRop == 0:
        simPhaseList = ['A']
    elif doRop > 0:
        simPhaseList = ['A', 'R']
    if 'single' in cellTemplate or 'Two' in cellTemplate \
    or cellTemplate in ['Grieneisen', 'Jones']:
        addText = resultstex.latexcomp(baseName, simPhaseList, origParDict, \
                  projName, countName, 1, 0, 0, 0)
    else:
        addText = resultstex.latexcomp(baseName, simPhaseList, origParDict, \
                  projName, countName, 1, 1, 1, 1)
    text = text + addText
    del(addText, simPhaseList)
    
    # Make section of presentation with mean auxin concentration in template:
    #if 'single' not in cellTemplate:
    #    addText = resultstex.latexmeanit(countName)
    #    text = text + addText
    #    del(addText)
    
    # Make section for presentation with statistics per cell:
    if 'stats_per_cell' in plotList:
        addText = resultstex.statspercell(baseName, countName, projName, \
                  substList, origParDict['plotY'][1], origParDict['plotX'][1])
        text = text + addText
        del(addText)
    
    # Make end of presentation file:
    addText = resultstex.latexend()
    text = text + addText
    del(addText)
    
    # Save latex file and compile it:
    forlatex.latexfile(countName + '_comp', text)
    #raw_input('Copy figures if needed!')
    
    # Delete auxiliary files from LaTeX:
    forlatex.dellatex(countName + '_comp')
    
    # Delete plots:
    delName = projName + '_curves_final_'
    fileList = os.listdir(os.getcwd())
    for fileName in fileList:
        if delName in fileName and '.png' not in fileName:
            os.remove(fileName)
    del(delName, fileName, fileList)
    
    # Delete comparison plots:
    fileList = os.listdir(os.getcwd())
    for fileName in fileList:
        currNamePart = projName + '_' + str(resNum) + '_'
        if currNamePart in fileName and '.eps' in fileName:
            os.remove(fileName)
        del(currNamePart)
    del(resNum)
    
del(resNumList, projName, origParDict)


