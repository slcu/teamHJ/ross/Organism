#! /usr/bin/env python
#
# Written by Bettina Greese, CBBP, Uni Lund, Sweden (2012-14).
#

################################################################################
### REQUIRED USER INPUT: ###
############################

# CHOOSE WHICH RUN OF CURRENT PROJECT SHOULD BE USED:
resNumDef = 'last'

# CHOOSE MODEL:
baseName = 'rop'
consSys = 0 # Signal (=1) for conservedSystem, i.e. no prod/degr of ROPs.

# CHOOSE EXTERNAL AUXIN TEMPLATE:
# b and e are indices of the first and last cell in x dir to receive auxin:
# s and p are strengths relative to the source and PIN values (in percent)
appTemplate = ''

################################################################################
### USER INPUT FROM ARGUMENTS: ###
##################################

# Import necessary modules:
import os, pickle, sys
from toolbox import dirnavi, variables
from simfiles import parvalues, simsettings
from execplot import simplots
from plotfiles import plotsettings, forplots, secgrad, secgraddiff, curvespervar
from plotfiles import maxabsderiv, statspercell, timecurves, totalsubst
from latexfiles import resultstex, forlatex

# Get path to folder for simulation output and results:
outPath = simsettings.outpath()

# Get information from input arguments:
argList = sys.argv
argList.extend(['-v', 'none'])
[doRop, cellTemplate, auxinTemplate, varType, xyzNumComp, initCond, \
simStrategy] = variables.arguments(argList)
del(varType)

# ASK USER WHICH RUN TO USE:
#resNum = raw_input("Which run schould be used? Press enter for 'last' or type number: ")
#if resNum == '':
resNum = resNumDef
#else:
#    resNum = int(resNum)
#del(resNumDef)

################################################################################

# Print user input for checking:
print(baseName, consSys, cellTemplate, auxinTemplate, appTemplate, xyzNumComp, initCond, resNum)
#raw_input('Check settings and press any key to continue:')

# Set list of plots to be made:
#plotList = ['final_curve', 'stats_per_cell', 'max_deriv', 'time_curve'] #, 'final_newman']
#plotList = ['final_curve', 'time_curve', 'stats_per_cell']
plotList = ['final_curve', 'stats_per_cell']
#plotList = ['max_deriv']
if doRop == 0:
    substList = ['auxin']
elif doRop > 0:
    substList = ['auxin', 'ropa', 'ropi', 'roptotal', 'ropratio']

# Get code version number:
vNrStr = dirnavi.codeversion()

# Collect information about template into a dictionary:
infoDict = variables.templinfo(baseName, vNrStr, cellTemplate, auxinTemplate, \
                               appTemplate, xyzNumComp, initCond, consSys, doRop)
del(initCond)

# Change directory:
os.chdir(outPath + 'results' + vNrStr + '/default/')

# Check how many runs of current project have been done:
maxNum = dirnavi.numfiles('.txt', 'default', cellTemplate, auxinTemplate, xyzNumComp)

# Set name for chosen run of current project:
if resNum == 'last':
    resNum = maxNum
elif resNum > maxNum:
    resNum = maxNum
    print('Chosen number is too high, will take last result.')
countName = cellTemplate + '_' + auxinTemplate + appTemplate + '_' \
    + str(xyzNumComp[0]) + '_' + str(xyzNumComp[1]) + '_' \
    + str(xyzNumComp[2]) + '_default_' + str(resNum)
del(xyzNumComp, consSys, maxNum)

# Load list with paths to simulation directories:
fileName = countName + '.txt'
fileObj = open(fileName, 'r')
origPathList = fileObj.readlines()
fileObj.close()
print("Loaded file " + fileName)
del(fileName, fileObj)

# Format list with paths to simulation directories:
pathList = [[], []] # Initialize list in correct format.
for currPath in origPathList:
    currPath = currPath.replace('\n', '')
    if 'Asim' in currPath:
        pathList[0].append(currPath)
    elif 'Rsim' in currPath:
        pathList[1].append(currPath)
    else:
        raise Exception('Path can not be identified as auxin or ROP.')
del(origPathList)

# Set path names for simulation and plot directories:
auxinPath = pathList[0][0]
returnAuxinPath = '../../../../../'
if doRop == 0:
    ropPath = ''
    returnRopPath = ''
    currSimPath = auxinPath
    currReturnPath = returnAuxinPath
elif doRop > 0:
    if pathList[1] == []:
        raise Exception('ROP path list is empty, simulate first with ROP.')
    ropPath = pathList[1][0]
    returnRopPath = '../../../../../../../'
    currSimPath = ropPath
    currReturnPath = returnRopPath

# Set part of title for current variation:
partTitle = currSimPath[:currSimPath.rfind('sim_')-1]
partTitle = partTitle[partTitle.find('/')+1:]
partTitle = partTitle[:partTitle.find('/')]
partTitle = partTitle.replace('_', ' ')

# Plot statistics for substance concentrations per cell:
if 'stats_per_cell' in plotList:
    if doRop == 0:
        substPathList = pathList[0]
    elif doRop > 0:
        substPathList = pathList[1]
    for subst in substList:
        statspercell.gnuplot(baseName, substPathList, \
                             subst, 'default', countName, '.eps')
    del(substPathList)

# Change directory:
os.chdir('../..')

# Plot final simulation results:
simplots.finalplots(baseName, auxinPath, ropPath, plotList)

# Change directory:
os.chdir(auxinPath)

# Get parameter values that were actually used:
pickleName = baseName + '_parDict.pkl'
fileObj = open(pickleName, 'r')
parDict = pickle.load(fileObj)
fileObj.close()
print("Loaded file " + pickleName)
del(pickleName, fileObj)

# Add settings for visualization:
plotsettings.visual(parDict)

# Get x and y values of sections for which conc curves will be plotted:
pickleName = '../../template/' + baseName + '_comp.pkl'
forplots.getsections(baseName, parDict, pickleName, 'auxin')
del(pickleName)

# Plot auxin concentration curves over time:
if 'time_curve' in plotList:
    timecurves.gnuplot(baseName, parDict, 'auxin', '.eps')
    if doRop > 0:
        os.chdir(returnAuxinPath + ropPath)
        timecurves.gnuplot(baseName, parDict, 'ropa', '.eps')
        timecurves.gnuplot(baseName, parDict, 'ropi', '.eps')
        os.chdir(returnRopPath + auxinPath)

# Change directory:
os.chdir(returnAuxinPath)

# Make first part of file for presentation:
simPath = auxinPath[:auxinPath.find('/')]
text = resultstex.latexbeg(simPath, parDict, cellTemplate, \
                           auxinTemplate, appTemplate)
del(auxinTemplate, appTemplate, simPath)

# Edit path names for simulation and plot directories:
auxinPlotPath = '../../' + auxinPath.replace('sim_', 'plot_')
ropPlotPath = '../../' + ropPath.replace('sim_', 'plot_')
currPlotPath = '../../' + currSimPath.replace('sim_', 'plot_')
del(currSimPath)        

# Make section for presentation with curve plots for all substances:
if 'final_curve' in plotList:
    if 'single' in cellTemplate \
    or 'file' in cellTemplate:
        addText = resultstex.latexpervar(baseName, currPlotPath, partTitle, \
                  parDict['plotY'][1], parDict['zoomList'][1])
    else:
        addText = resultstex.latexpersubst(baseName, currPlotPath, partTitle, \
                  substList, parDict['zoomList'][1])
    text = text + addText
    del(addText)

# Make section for presentation with statistics per cell:
if 'stats_per_cell' in plotList:
    addText = resultstex.statspercell(baseName, countName, partTitle, \
              substList, parDict['plotY'][1], parDict['plotX'][1])
    text = text + addText
    del(addText)

# Make section for presentation with derivatives:
if 'max_deriv' in plotList:
    addText = resultstex.latexderiv(baseName, currPlotPath, partTitle, \
              substList)
    text = text + addText
    del(addText)

# Make section for presentation with auxin concentration curves over time:
if 'time_curve' in plotList:
    addText = resultstex.latextime(auxinPlotPath, partTitle, ['auxin'])
    text = text + addText
    del(addText)
    if doRop > 0:
        addText = resultstex.latextime(ropPlotPath, partTitle, ['ropa'])
        text = text + addText
        addText = resultstex.latextime(ropPlotPath, partTitle, ['ropi'])
        text = text + addText
        del(addText)
del(auxinPlotPath, ropPlotPath, currPlotPath)

''' Keep!
if baseName + '.data' in os.listdir(os.getcwd()):
    # Plot intermediate simulation results for movies:
    frameList = ['curve_subst']
    simplots.frameplots(baseName, 'A', frameList, infoDict, parDict)
    
    # Change directory:
    os.chdir('../' + levelList[6])
    
    # Move movies to results directory:
    newPath = '../../../../../results' + vNrStr + '/default/'
    fileList = os.listdir(os.getcwd())
    for fileName in fileList:
        if '.avi' in fileName:
            os.rename(fileName, newPath + countName + '_auxin.avi')
        del(fileName)
    del(fileList)

if doRop > 0:
    # If .data file exists, make movies:
    if baseName + '.data' in os.listdir(os.getcwd()):
        # Plot intermediate simulation results for movies:
        frameList = ['curve_subst']
        simplots.frameplots(baseName, 'R', frameList, infoDict, parDict)
        
        # Change directory:
        os.chdir('../' + levelList[11])
        
        # Move movies to results directory and rename:
        newPath = '../../../../../../../results' + vNrStr + '/default/'
        fileList = os.listdir(os.getcwd())
        for fileName in fileList:
            if '.avi' in fileName:
                os.rename(fileName, newPath + countName + '_rop.avi')
            del(fileName)
        del(fileList)
'''
    
del(plotList, infoDict, partTitle)

# Change directory:
os.chdir('results' + vNrStr + '/default')
del(vNrStr)

if 'single' not in cellTemplate and 'Two' not in cellTemplate \
and 'epidermis' not in cellTemplate \
and cellTemplate not in ['Grieneisen', 'Jones']:# \
#and '7layersS' not in cellTemplate:
    # Plot large scale auxin gradients:
    secgrad.gnuplot(baseName, parDict, pathList, 'default', \
                    countName, '.eps')
    
    # Plot total auxin mass in whole template:
    totalsubst.gnuplot(baseName, pathList, \
                    'default', countName, '.eps')
    
    # Make comparison section of presentation file:
    addText = resultstex.latexcomp(baseName, ['A'], parDict, 'default', \
        countName, 0, 1, 0, 1)
    text = text + addText
    del(addText)
del(parDict, cellTemplate)

# Make end of presentation file:
addText = resultstex.latexend()
text = text + addText
del(addText)

# Save latex file and compile it:
forlatex.latexfile(countName + '_plots', text)
#raw_input('Copy figures if needed!')

# Delete auxiliary files from LaTeX:
forlatex.dellatex(countName + '_plots')

# Delete comparison plots:
fileList = os.listdir(os.getcwd())
for fileName in fileList:
    currNamePart = 'default_' + str(resNum) + '_'
    if currNamePart in fileName and '.eps' in fileName:
        os.remove(fileName)
    del(currNamePart)
    del(fileName)
del(resNum, fileList)


