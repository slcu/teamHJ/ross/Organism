#! /usr/bin/env python
# Module with functions related to regions in template, i.e. PIN or production.
#
# Written by Bettina Greese, CBBP, Uni Lund, Sweden (2014).
#

################################################################################

def pinauxloc(paType, parDict):
    '''Adds list with localization of PIN or AUX to dictionary.
    
    Written by Bettina Greese, CBBP, Uni Lund, Sweden (2014).'''
    
    # Parameters:
    steleRad = parDict['steleRad'][1]
    wallThick = parDict['wallThick'][1]
    xLen = parDict['xLengths'][1]
    yLen = parDict['yLengths'][1]
    prodLoc = parDict['prodLoc'][1]
    paCode = parDict[paType + 'Layers']
    paQcCode = parDict[paType + 'Qc']
    paCoCode = parDict[paType + 'Co']
    xCoBeg = parDict['xCoBeg'][1]
    xCoEnd = parDict['xCoEnd'][1]
    paPartCode = [] # Default.
    paFlipCode = [] # Default.
    if paType == 'pin':
        paFlipCode = parDict[paType + 'Flip']
        paWeakCode = parDict[paType + 'Weak']
        paLatList = parDict[paType + 'Lat'][1]
    elif paType == 'aux':
        paPartCode = parDict[paType + 'Part']
    else:
        raise Exception('Unknown auxin transporter code.')
    sosiType = parDict['sosiType'][1]
    
    # Size of cell template without columella:
    if sosiType == 'tipshoot' and xCoBeg == xCoEnd:
        xBeg = 0.0 # Includes source!
    elif sosiType == 'bothshoot':
        xBeg = xCoEnd
    xEnd = sum(xLen) + len(xLen)*wallThick # Cell lengths + wall lengths.
    yBeg = steleRad
    yEnd = steleRad + sum(yLen) + len(yLen)*wallThick # Cell lengths + wall lengths.
    
    # Loop over layers in y direction:
    paLoc = []
    regEnd = steleRad # Initialize end of (previous) region.
    del(steleRad)
    for yInd in range(0, len(yLen)):
        currCode = paCode[1][yInd]
        if paType == 'pin':
            # Determine lateral transporter strength:
            paLatStre = paLatList[yInd]
        currThick = yLen[yInd] + wallThick # Thickness of layer.
        regBeg = regEnd # End of previous region becomes beg of curr region.
        regEnd = regBeg + currThick # New end of current region.
        if currCode in ['up', 'down', 'in', 'out', 'uniform']:
            paLoc.extend([xBeg, xEnd, regBeg, regEnd, currCode, 1.0])
        elif currCode == 'upweak':
            # Split up layer in two regions between which weakening happens:
            xWeak = splitlayer(xLen, paWeakCode, yInd, wallThick)
            paLoc.extend([xBeg, xWeak, regBeg, regEnd, 'up', 1.0])
        elif currCode == 'upin':
            paLoc.extend([xBeg, xEnd, regBeg, regEnd, 'up', 1.0])
            paLoc.extend([xBeg, xEnd, regBeg, regEnd, 'in', paLatStre])
        elif currCode == 'uplat':
            paLoc.extend([xBeg, xEnd, regBeg, regEnd, 'up', 1.0])
            paLoc.extend([xBeg, xEnd, regBeg, regEnd, 'in', paLatStre])
            paLoc.extend([xBeg, xEnd, regBeg, regEnd, 'out', paLatStre])
        elif currCode == 'downin':
            paLoc.extend([xBeg, xEnd, regBeg, regEnd, 'down', 1.0])
            paLoc.extend([xBeg, xEnd, regBeg, regEnd, 'in', paLatStre])
        elif currCode == 'downlat':
            paLoc.extend([xBeg, xEnd, regBeg, regEnd, 'down', 1.0])
            paLoc.extend([xBeg, xEnd, regBeg, regEnd, 'in', paLatStre])
            paLoc.extend([xBeg, xEnd, regBeg, regEnd, 'out', paLatStre])
        elif currCode == 'upinweak':
            # Split up layer in two regions between which weakening happens:
            xWeak = splitlayer(xLen, paWeakCode, yInd, wallThick)
            paLoc.extend([xBeg, xWeak, regBeg, regEnd, 'up', 1.0])
            #paLoc.extend([xWeak, xEnd, regBeg, regEnd, 'up', 0.0])
            paLoc.extend([xBeg, xEnd, regBeg, regEnd, 'in', paLatStre])
        elif currCode == 'flip':
            # Split up layer in two regions between which flip happens:
            xFlip = splitlayer(xLen, paFlipCode, yInd, wallThick)
            paLoc.extend([xBeg, xFlip, regBeg, regEnd, 'down', 1.0])
            paLoc.extend([xFlip, xEnd, regBeg, regEnd, 'up', 1.0])
            del(xFlip)
        elif currCode == 'flipin':
            # Split up layer in two regions between which flip happens:
            xFlip = splitlayer(xLen, paFlipCode, yInd, wallThick)
            paLoc.extend([xBeg, xFlip, regBeg, regEnd, 'down', 1.0])
            paLoc.extend([xFlip, xEnd, regBeg, regEnd, 'up', 1.0])
            del(xFlip)
            paLoc.extend([xBeg, xEnd, regBeg, regEnd, 'in', paLatStre])
        elif currCode == 'fliplat':
            # Split up layer in two regions between which flip happens:
            xFlip = splitlayer(xLen, paFlipCode, yInd, wallThick)
            paLoc.extend([xBeg, xFlip, regBeg, regEnd, 'down', 1.0])
            paLoc.extend([xFlip, xEnd, regBeg, regEnd, 'up', 1.0])
            del(xFlip)
            paLoc.extend([xBeg, xEnd, regBeg, regEnd, 'in', paLatStre])
            paLoc.extend([xBeg, xEnd, regBeg, regEnd, 'out', paLatStre])
        elif currCode == 'unipart':
            # Split up layer in two regions between which PIN/AUX is lost:
            xPart = splitlayer(xLen, paPartCode, yInd, wallThick)
            paLoc.extend([xBeg, xPart, regBeg, regEnd, 'uniform', 1.0])
            del(xPart)
        elif currCode == 'upunipart':
            paLoc.extend([xBeg, xEnd, regBeg, regEnd, 'up', 1.0])
            # Split up layer in two regions between which PIN/AUX is lost:
            xPart = splitlayer(xLen, paPartCode, yInd, wallThick)
            paLoc.extend([xBeg, xPart, regBeg, regEnd, 'uniform', 1.0])
            del(xPart)
        elif currCode == 'none':
            pass
        else:
            raise Exception('Unknown PIN/AUX code.')
        del(currCode)
        if paType == 'pin':
            del(paLatStre)
    del(regBeg, regEnd, xBeg, yInd, yLen, xEnd, paCode, paFlipCode)
    
    # Add PIN and AUX localization for columella cells:
    if paType in ['pin', 'aux'] and xCoBeg != xCoEnd:
        paLoc.extend([xCoBeg, xCoEnd, yBeg, yEnd, paCoCode[1], 1.0])
    del(xCoBeg, xCoEnd, yBeg, yEnd, paCoCode)
    
    # Add PIN and AUX localization for QC cells:
    if prodLoc[0] != prodLoc[1] or prodLoc[2] != prodLoc[3]:
        paLoc.extend(prodLoc[:4] + [paQcCode[1], 1.0])
    del(prodLoc, paQcCode)
    
    # Add to dictionary:
    if paType == 'pin':
        parDict[paType + 'Loc'] = ["PIN loc", paLoc]
    elif paType == 'aux':
        parDict[paType + 'Loc'] = ["AUX loc", paLoc]
    del(paLoc)
    
    return

################################################################################

def splitlayer(xLen, paSplitCode, yInd, wallThick):
    ''' Split up layer in two regions with different PIN/AUX localization.
    
    Written by Bettina Greese, CBBP, Uni Lund, Sweden.'''

    # Settings:
    eTol = 10**8 # Error tolerance for checks.
    
    # Size of template in x direction:
    xEnd = sum(xLen) + len(xLen)*wallThick # Cell lengths + wall lengths.
    
    # Split template in two parts:
    xLen1 = xLen[:paSplitCode[1][yInd]]
    xLen2 = xLen[paSplitCode[1][yInd]:]
    if xLen != xLen1 + xLen2:
        raise Exception('Problem with splitting of cell length list.')
    del(xLen, paSplitCode, yInd)
    
    # Find location of split and check it:
    xSplit = sum(xLen1) + len(xLen1)*wallThick
    xRest = sum(xLen2) + len(xLen2)*wallThick
    if abs(xEnd - xSplit - xRest) > eTol:
        print(xSplit, xRest, xEnd)
        raise Exception('Problem with splitting of layer.')
    del(xLen1, xLen2, xRest)
    
    return xSplit

################################################################################

