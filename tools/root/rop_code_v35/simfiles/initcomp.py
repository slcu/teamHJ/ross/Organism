#! /usr/bin/env python
#  Module with functions related to compartment properties in .init file.
#
# Written by Bettina Greese, CBBP, Uni Lund, Sweden (2014).
#

################################################################################

def numcomp(parDict, cellTemplate):
    '''Adds the number of compartments in each direction to dictionary.
    
    Written by Bettina Greese, CBBP, Uni Lund, Sweden (2014).'''
    
    # Get parameters:
    xNumComp = parDict['xNumComp'][1]
    yNumComp = parDict['yNumComp'][1]
    zNumComp = parDict['zNumComp'][1]
    
    # Calculate number of cells in all directions:
    xNumCells = len(parDict['xLengths'][1])
    yNumCells = len(parDict['yLengths'][1])
    zNumCells = parDict['zNumCells'][1]
    
    # Total number of compartments in x direction (along root):
    if parDict['sosiType'][1] == 'tipshoot':
        # Count source cell + wall, sink cell + wall, last wall:
        xTotalComp = 5
        # All cells except source and sink have xNumComp + 1 wall comp:
        xTotalComp += (xNumCells-2)*(xNumComp + 1)
    elif parDict['sosiType'][1] == 'bothshoot':
        # Count source/sink cell + wall, last wall:
        xTotalComp = 3 
        # All cells except source/sink have xNumComp + 1 wall comp:
        xTotalComp += (xNumCells-1)*(xNumComp + 1)
    del(xNumCells, xNumComp)
    parDict['xTotalComp'] = ["total num comp x dir", xTotalComp]
    del(xTotalComp)
    
    # Total number of compartments in y direction (radially outwards):
    if 'singleL' in cellTemplate or 'fileL' in cellTemplate \
    or 'fileVarL' in cellTemplate:
        # No wall compartments:
        yTotalComp = yNumCells*yNumComp
    else:
        # 1 compartment = last wall.
        # All cells have yNumComp + 1 wall compartments.
        yTotalComp = 1 + yNumCells*(yNumComp + 1)
    del(yNumCells, yNumComp)
    parDict['yTotalComp'] = ["total num comp y dir", yTotalComp]
    del(yTotalComp)
    
    # Total number of compartments in z direction (along periphery):
    if zNumCells > 0: # 3D simulation.
        zTotalComp = 1 + zNumCells*(zNumComp + 1)
    else: # 2.5D simulation.
        zTotalComp = 1
    del(zNumCells, zNumComp)
    parDict['zTotalComp'] = ["total num comp z dir", zTotalComp]
    del(zTotalComp)
    
    return

################################################################################

def lenandcomp(xyzType, parDict, cellInd, rDig):
    '''Returns length and number of compartments of current cell.
    
    Written by Bettina Greese, CBBP, Uni Lund, Sweden (2014).'''
    
    # Get parameter values:
    lenList = parDict[xyzType + 'Lengths'][1]
    numCells = len(lenList)
    numComp = parDict[xyzType + 'NumComp'][1]
    
    # Check cell location and set length and number of compartments:
    if cellInd == numCells: # Extra "cell" at end of tissue, will be one wall comp.
        currLen = parDict['wallThick'][1] # Dummy to make return work.
        cellComp = 0 # Number of cell compartments in x dir for wall.
    else: # Normal/real cell.
        currLen = lenList[cellInd] # Length of curr cell in curr direction.
        currLen = round(currLen, rDig)
        
        # Set discretization in case of x direction:
        if xyzType == 'x':
            if cellInd == numCells - 1: # Sink cell in both cases.
                cellComp = 1 # Sink consists of only one compartment.
            elif cellInd == 0: # Source cell only in case of 'tipshoot'.
                if parDict['sosiType'][1] == 'tipshoot':
                    cellComp = 1 # Source consists of only one compartment.
                elif parDict['sosiType'][1] == 'bothshoot':
                    cellComp = numComp # Normal num of cell comp in x dir.
            else:
                cellComp = numComp # Normal num of cell comp in x dir.
        elif xyzType in ['y', 'z']:
            cellComp = numComp # Normal number of cell compartments.
    del(xyzType, cellInd, numCells, numComp, lenList, rDig)
    
    return [currLen, cellComp]

################################################################################

def compsize(xyzType, parDict, compInd, currComp, cellComp, currLen, \
    accumSize, wCol, rDig):
    '''Returns size of compartment depending on where in the cell it is.
    
    Written by Bettina Greese, CBBP, Uni Lund, Sweden (2014).'''
    
    # Check compartment location and set size:
    if compInd == 0: # Extra comp at beg of cell (or only comp), will be wall.
        # Set wall comp size to wall thickness:
        compSize = parDict['wallThick'][1]
        # Set wall marker to 1:
        currComp[wCol] = '1'
    elif compInd == cellComp: # Last compartment = cell comp.
        compSize = currLen - accumSize # Must be exact!
        if (accumSize + compSize) != currLen:
            raise Exception('Length of last compartment not correct.')
    else: # Cell compartment.
        # Calculate size of cell compartment:
        compSize = currLen/parDict[xyzType + 'NumComp'][1]
        compSize = round(compSize, rDig)
        accumSize += compSize
    del(wCol, compInd, cellComp, currLen, rDig)
    
    return [compSize, accumSize]

################################################################################

def sosiregion(currComp, sosiCol, sosiLoc, xPos, yPos):
    '''Returns edited compartment if it is in source or sink region.
    
    Written by Bettina Greese, CBBP, Uni Lund, Sweden (2014).'''
    
    from toolbox import variables
    
    # Specifics of source and sink lists:
    numS = 5 # Number of entries per list.
    
    # Loop over source or sink regions:
    numSosiLoc = len(sosiLoc)/numS
    for sInd in range(0, numSosiLoc):
        xSosiBeg = sosiLoc[sInd*numS]
        xSosiEnd = sosiLoc[sInd*numS+1]
        ySosiBeg = sosiLoc[sInd*numS+2]
        ySosiEnd = sosiLoc[sInd*numS+3]
        sosiStre = sosiLoc[sInd*numS+4]
        
        # Check if curr comp is in source or sink region:
        checkSosi = variables.checkregion(xPos, yPos, xSosiBeg, \
                    xSosiEnd, ySosiBeg, ySosiEnd)
        
        # If yes, set source or sink marker to source or sink strength:
        if checkSosi == True:
            currComp[sosiCol] = str(sosiStre)
        
        del(xSosiBeg, xSosiEnd, ySosiBeg, ySosiEnd, sosiStre)
    del(sInd, numSosiLoc, numS)
    
    return [currComp, checkSosi]

################################################################################

def cutpositions(parDict):
    '''Adds the x positions where root is cut into sections to dictionary.
    
    Written by Bettina Greese, CBBP, Uni Lund, Sweden (2014).'''
    
    # Get parameter values:
    xLen = parDict['xLengths'][1]
    wallThick = parDict['wallThick'][1]
    
    # Calculate list of x positions for cuts:
    if len(xLen) > 3: # Not for single cell.
        if parDict['sosiType'][1] == 'tipshoot':
            # End of source as first cutting position:
            xCutList = [wallThick/2.0 + xLen[0]]
        elif parDict['sosiType'][1] == 'bothshoot':
            # Beginning of template as first cutting position:
            xCutList = [-wallThick/2.0]
        for cutInd in range(1, parDict['numCuts'][1]):
            xCutList.append(xCutList[-1] + parDict['xCutLen'][1])
    else:
        xCutList = [0, 0, 0]
    del(xLen, wallThick)
    parDict['xCutList'] = ["list with x pos for cuts",  xCutList]
    del(xCutList)
    
    return

################################################################################

def auxingrad(currXComp, xPos, xSecInd, xCutList, gradData, colIndDict):
    '''Adds measured auxin mass for optimization.
    
    Written by Bettina Greese, CBBP, Uni Lund, Sweden (2014).'''
    
    # Get columns:
    gmCol = colIndDict['sumMass']
    gmaCol = colIndDict['sumMassAll']
    del(colIndDict)
    
    # Check if in region of sections:
    if xSecInd < len(xCutList):
        # Calculate x positions for cutting:
        check1 = (xPos > xCutList[0])
        check2 = (xSecInd <= len(gradData))
        check3 = (xCutList[xSecInd-1] < xPos <= xCutList[xSecInd])
        if check1 and check2 and check3:
            currXComp[gmCol] = str(gradData[xSecInd-1])
            currXComp[gmaCol] = str(sum(gradData))
        else:
            currXComp[gmCol] = '0.0'
            currXComp[gmaCol] = '0.0'
    else:
        currXComp[gmCol] = '0.0'
        currXComp[gmaCol] = '0.0'
    
    del(gmCol, gmaCol, xPos, xCutList, xSecInd, gradData)
    
    return

################################################################################

