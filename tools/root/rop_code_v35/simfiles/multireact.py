#! /usr/bin/env python
# Module with functions relating to the multi-species reactions in the .model file.
#
# Written by Bettina Greese, CBBP, Uni Lund, Sweden (2012-14).
#

################################################################################

def getcutind(parDict, xPosList):
    '''Calculates indices for cutting regions (exclude source and sink). 
    
    Written by Bettina Greese, CBBP, Uni Lund, Sweden (2014).'''
    
    # Settings:
    numCuts = 4 # Number of cuts (one more than number of regions).
    
    # Get parameter values:
    xCutLen = parDict['xCutLen'][1]
    xLen = parDict['xLengths'][1]
    wallThick = parDict['wallThick'][1]
    xTotalComp = parDict['xTotalComp'][1]
    yTotalComp = parDict['yTotalComp'][1]
    zTotalComp = parDict['zTotalComp'][1]
    
    # Count cells and compartments:
    if len(xPosList) != xTotalComp:
        raise Exception('Wrong length of positions in x direction.')
    
    # Initialize list of compartment indices before which cutting is done:
    sosiType = parDict['sosiType'][1]
    if sosiType == 'bothshoot':
        xCutIndList = [0] # Beg of tissue (before first comp).
    elif sosiType == 'tipshoot':
        xCutIndList = [2*yTotalComp*zTotalComp] # Skip source.
    
    # Initialize x position for next cut:
    if sosiType == 'bothshoot':
        # Size of first section as next (i.e. second) cutting position:
        xCutPos = xCutLen
    elif sosiType == 'tipshoot':
        # Size of source plus first section as next cutting position:
        xCutPos = wallThick/2.0 + xLen[0] + xCutLen
    del(sosiType)
    
    # Beginning of sink as last cutting position:
    xSiBeg = wallThick/2.0 + sum(xLen[:-1]) + len(xLen[:-1])*wallThick
    del(xLen, wallThick)
    
    # Make list with x positions for cutting:
    for xCompInd in range(0, len(xPosList)): # Loop over comp in x dir.
        xPos = xPosList[xCompInd] # x position of current comp in x dir.
        
        # If current x comp is above next cut in x direction:
        if xPos >= xCutPos:
            # Account for compartments in y and z direction:
            xCutIndList.append(xCompInd*yTotalComp*zTotalComp)
            
            # Update x position of next cut:
            if xCutPos == xSiBeg: # Reached sink region before.
                break # Exit for loop.
            elif xCutPos + xCutLen >= xSiBeg: # Now reach sink region.
                xCutPos = xSiBeg # Beginning of sink as next cut pos.
            else:
                xCutPos += xCutLen # Update x position for next cut.
        del(xPos, xCompInd)
        
        # Check number of cutting regions:
        if len(xCutIndList) >= numCuts:
            break
    del(xCutPos, xCutLen, xPosList, xSiBeg, numCuts)
    
    # Check list and edit if no sections defined yet
    if len(xCutIndList) == 1:
        # Add cutting position before sink:
        xCutIndList.append((xTotalComp-2)*yTotalComp*zTotalComp)
    
    return xCutIndList

################################################################################

def summassrct(xCutIndList, specIndDict, parDict):
    '''Returns text for multispecies reactions for summed masses of auxin.
    
    Written by Bettina Greese, CBBP, Uni Lund, Sweden (2012-14).'''
    
    # Indices and parameter values:
    ind_V = str(specIndDict['V'])
    ind_auxin = str(specIndDict['auxin'])
    ind_mass = str(specIndDict['sumMass'])
    ind_all = str(specIndDict['sumMassAll'])
    ind_templ = str(specIndDict['sumMassTempl'])
    convFac = parDict['convFac'][1]
    templRatio = parDict['templRatio'][1]
    
    # Summed auxin mass in root sections:
    numCut = len(xCutIndList)
    text = "sumCompartmentMass 1 " + str(numCut) + " 3"
    for ind in range(1, numCut):
        text = text + " 2"
    text = text + "       # 1 K+1 N_k1 [N_k2] ... \n" \
         + str(convFac*templRatio) + "                        " \
         + "# conversion factor p_0 \n" \
         + ind_V + " " + ind_auxin + " " + ind_mass + "                            " \
         + "# V_index conc_index save_index \n"
    endInd = xCutIndList[0] - 1 # Will be first begin index.
    for cutInd in range(1, numCut):
        begInd = endInd + 1 # Current begin index is previous end index.
        endInd = xCutIndList[cutInd] - 1 # Get current end index.
        text = text + str(begInd) + " " + str(endInd) \
            + "                             " \
            + "# compartmentStart_index compartmentEnd_index \n"
        del(cutInd, begInd)
    del(numCut, endInd)
    text = text + "\n" # Empty line after block.
        
    # Summed auxin mass in all sections:
    text = text + "sumCompartmentMass 1 2 3 2      " \
         + "# 1 K+1 N_k1 [N_k2] ... \n" \
         + str(convFac*templRatio) + "                     " \
         + "# conversion factor p_0 \n" \
         + ind_V + " " + ind_auxin + " " + ind_all + "                             " \
         + "# V_index conc_index save_index \n" \
         + str(xCutIndList[0]) + " " + str(xCutIndList[-1] - 1) + "                            " \
         + "# compartmentStart_index compartmentEnd_index \n"
    del(xCutIndList, ind_all)
    text = text + "\n" # Empty line after block.
    
    # Summed auxin mass in whole template:
    xTotalComp = parDict['xTotalComp'][1]
    yTotalComp = parDict['yTotalComp'][1]
    zTotalComp = parDict['zTotalComp'][1]
    text = text + "sumCompartmentMass 1 2 3 2      " \
         + "# 1 K+1 N_k1 [N_k2] ... \n" \
         + str(convFac*templRatio) \
         + "                     " \
         + "# conversion factor p_0 \n" \
         + ind_V + " " + ind_auxin + " " + ind_templ \
         + "                             " \
         + "# V_index conc_index save_index \n" \
         + "0 " + str(xTotalComp*yTotalComp*zTotalComp-1) \
         + "                            " \
         + "# compartmentStart_index compartmentEnd_index \n"
    del(ind_templ, xTotalComp, yTotalComp, zTotalComp)
    text = text + "\n" # Empty line after block.
    
    return text

################################################################################

def roparopirct(specIndDict, parDict):
    '''Returns text for multispecies reactions for (in)activation of ROP.
    
    Written by Bettina Greese, CBBP, Uni Lund, Sweden (2012-14).'''
    
    # Indices:
    ind_auxin = str(specIndDict['auxin'])
    ind_ropa = str(specIndDict['ROPactive'])
    ind_ropi = str(specIndDict['ROPinactive'])
    
    # Inactivation:
    val_c = str(parDict['c'][1])
    text = "massAction 1 2 1 1 \n" \
         + val_c + "                               " \
         + "# c=0.1 \n" \
         + ind_ropa + "                                " \
         + "# from ROPactive \n" \
         + ind_ropi + "                                " \
         + "# to ROPinactive \n"
    text = text + "\n" # Empty line after block.
    del(val_c)
    
    # Linear activation:
    val_k1 = parDict['k1'][1]
    if val_k1 != 0.0:
        val_k1 = str(val_k1)
        text = text + "massAction 1 2 1 1 \n" \
             + val_k1 + "                                  " \
             + "# k_1=0.01 \n"\
             + ind_ropi + "                                " \
             + "# from ROPinactive \n" \
             + ind_ropa + "                                " \
             + "# to ROPactive \n"
        text = text + "\n" # Empty line after block.
    del(val_k1)
    
    # Nonlinear activation including autocatalysis and auxin:
    val_k2 = parDict['k2'][1]
    if val_k2 != 0.0:
        val_k2 = str(val_k2)
        text = text + "massActionEnzymatic 1 3 1 1 3 \n" \
             + val_k2 + "                                  " \
             + "# k_2=0.1 \n" \
             + ind_ropi + "                                " \
             + "# from ROPinactive \n" \
             + ind_ropa + "                                " \
             + "# to ROPactive \n" \
             + ind_ropa + " " + ind_ropa + " " + ind_auxin + "                                 " \
             + "# dependent on u*u*a \n"
        text = text + "\n" # Empty line after block.
    del(val_k2)
    
    # Nonlinear activation by auxin:
    val_k3 = parDict['k3'][1]
    if val_k3 != 0.0:
        val_k3 = str(val_k3)
        text = text + "massActionEnzymatic 1 3 1 1 1 \n" \
             + val_k3 + "                                  " \
             + "# k_3 \n" \
             + ind_ropi + "                                " \
             + "# from ROPinactive \n" \
             + ind_ropa + "                                " \
             + "# to ROPactive \n" \
             + ind_auxin + "                                 " \
             + "# dependent on auxin \n"
        text = text + "\n" # Empty line after block.
    del(val_k3, ind_auxin)
    
    # Nonlinear activation by autocatalysis:
    val_k4 = parDict['k4'][1]
    if val_k4 != 0.0:
        val_k4 = str(val_k4)
        text = text + "massActionEnzymatic 1 3 1 1 2 \n" \
             + val_k4 + "                                  " \
             + "# k_4 \n" \
             + ind_ropi + "                                " \
             + "# from ROPinactive \n" \
             + ind_ropa + "                                " \
             + "# to ROPactive \n" \
             + ind_ropa + " " + ind_ropa + "                                 " \
             + "# dependent on u*u \n"
        text = text + "\n" # Empty line after block.
    del(val_k4, ind_ropa, ind_ropi)
    
    return text

################################################################################

