#! /usr/bin/env python
# Script that provides functions used only in rop_sim_cell_tb.py.
#
# Written by Bettina Greese, CBBP, Uni Lund, Sweden (2014).
#

################################################################################

def cellinit(baseName, cellName, cellLabel, numCpC):
    '''Makes .init file for current cell from original .init file.
    
    Written by Bettina Greese, CBBP, Uni Lund, Sweden (2013-14).'''
    
    from toolbox import variables
    
    # Get column indices for .init file:
    [colIndDict, colList] = variables.colind('.init')
    del(colList)
    lCol = colIndDict['cellLabel']
    del(colIndDict)
    
    # Make new .init file:
    cellInitFileObj = open(cellName + '.init', 'w')
    del(cellName)
    
    # Open original .init file and get number of rows and columns:
    origInitFileObj = open(baseName + '.init', 'r')
    del(baseName)
    line = origInitFileObj.readline() # Read the first line.
    lineElem = line.split(' ') # Get elements of first line.
    del(line)
    numRows = int(lineElem[0]) # Number of compartments, i.e. rows.
    numCols = int(lineElem[1]) # Number of columns.
    del(lineElem)
    cellInitFileObj.write(str(numCpC) + ' ' + str(numCols) + '\n')
    
    # Extract current cell from .init file and get relevant indices:
    relLineIndList = [] # List for row indices in .init (skipping first line).
    # Loop over all compartments:
    for ind in range(0, numRows):
        line = origInitFileObj.readline()
        lineElem = line.split(' ')
        if lineElem[-1] == '\n':
            if len(lineElem[:-1]) != numCols:
                raise Exception('Incorrect length of line from .init file.')
        else:
            raise Exception("Line should end with '\n'.")
        if int(lineElem[lCol]) == int(cellLabel): # Curr comp is in curr cell.
            relLineIndList.append(ind)
            cellInitFileObj.write(line) # Save curr comp data.
        del(line, lineElem)
    del(ind, cellLabel, lCol)
    if len(relLineIndList) != numCpC:
        print(len(relLineIndList), numCpC)
        raise Exception('Wrong length of list with relevant lines.')
    del(numCpC)
    
    # Close original and new .init files:
    origInitFileObj.close()
    cellInitFileObj.close()
    del(origInitFileObj, cellInitFileObj)
    print('Saved .init file for current cell.')
    
    return [numRows, numCols, relLineIndList] 

################################################################################

def cellneigh(baseName, cellName, numRows, relLineIndList, numCpC):
    '''Makes .neigh file for current cell from original .neigh file.
    
    Written by Bettina Greese, CBBP, Uni Lund, Sweden (2013-14).'''
    
    # Make .neigh file for current cell:
    cellNeighFileObj = open(cellName + '.neigh', 'w')
    del(cellName)
    
    # Open original .neigh file:
    origNeighFileObj = open(baseName + '.neigh')
    del(baseName)
    line = origNeighFileObj.readline() # Read the first line.
    lineElem = line.split(' ') # Get elements of first line.
    if int(lineElem[0]) != numRows:
        raise Exception('Different number of rows in .init and .neigh files.')
    numVals = int(lineElem[1])
    cellNeighFileObj.write(str(numCpC) + ' ' + str(numVals) + '\n')
    del(numCpC, numVals)
    
    # Loop over all compartments:
    for rInd in range(0, numRows):
        line = origNeighFileObj.readline()
        
        # Check if relevant line:
        if rInd in relLineIndList:
            lineElem = line.split(' ')
            del(line)
            if int(lineElem[0]) != rInd:
                raise Exception('Error with .neigh file.')
            if int(lineElem[1]) != 4:
                raise Exception('Error with .neigh file.')
            
            # Extract relevant neighbor information:
            neighList = lineElem[2:6]
            areaList = lineElem[6:10]
            pinList = lineElem[10:14]
            auxList = lineElem[14:18]
            del(lineElem)
            
            # Preallocate variables for new neighbor information:
            newNeighList = []
            newAreaList = []
            newPinList = []
            newAuxList = []
            
            # Loop over original neighbors:
            for nInd in range(0, 4):
                neighInd = int(neighList[nInd])
                if neighInd in relLineIndList:
                    newInd = relLineIndList.index(neighInd)
                    newNeighList.append(str(newInd))
                    del(newInd)
                    newAreaList.append(areaList[nInd])
                    newPinList.append(pinList[nInd])
                    newAuxList.append(auxList[nInd])
                del(neighInd)
            del(nInd, neighList, areaList, pinList, auxList)
            numNeigh = len(newNeighList)
            if numNeigh != len(newAreaList):
                raise Exception('Wrong length of list with contact areas.')
            if numNeigh != len(newPinList):
                raise Exception('Wrong length of list with PIN values.')
            if numNeigh != len(newAuxList):
                raise Exception('Wrong length of list with AUX values.')
            
            # Make new line and save:
            compInd = relLineIndList.index(rInd)
            line = str(compInd) + ' ' + str(numNeigh)
            del(compInd, numNeigh)
            for entry in newNeighList:
                line = line + ' ' + entry
            for entry in newAreaList:
                line = line + ' ' + entry
            for entry in newPinList:
                if entry != '0':
                    raise Exception('PIN in single cell must be 0.')
                else:
                    line = line + ' ' + entry
            for entry in newAuxList:
                if entry != '0':
                    raise Exception('AUX in single cell must be 0.')
                else:
                    line = line + ' ' + entry
            del(entry, newNeighList, newAreaList, newPinList, newAuxList)
            cellNeighFileObj.write(line + ' \n')
            del(line)
    del(rInd, numRows)
    
    # Close original and new .neigh files:
    origNeighFileObj.close()
    cellNeighFileObj.close()
    del(origNeighFileObj, cellNeighFileObj)
    print('Saved file .neigh file for current cell.')
    
    return

################################################################################

def cellfinal(baseName, cellName, cellLabel, epidLabel, \
              numRows, numCols, numCpC, relLineIndList):
    '''Merge .final file of current cell with original .final file.
    
    Written by Bettina Greese, CBBP, Uni Lund, Sweden (2013-14).'''
    
    import os
    from toolbox import variables
    
    # Get column indices for .final file:
    [colIndDict, colList] = variables.colind('.final')
    del(colList)
    raCol = colIndDict['ROPactive']
    riCol = colIndDict['ROPinactive']
    del(colIndDict)
    
    # Open .final file from cell simu and move to second line:
    cellFinalFileObj = open(cellName + '.final', 'r')
    cellLine = cellFinalFileObj.readline()
    cellLineElem = cellLine.split()
    if int(cellLineElem[0]) != numCpC:
        raise Exception('Wrong entry in first line of cell .final file.')
    del(cellLine, cellLineElem)
    
    # Open previous common .final file and move to second line:
    prevFinalFileObj = open(baseName + '_all_' \
                            + str(epidLabel) + '.final', 'r')
    line = prevFinalFileObj.readline()
    lineElem = line.split()
    if int(lineElem[0]) != numRows:
        raise Exception('Wrong number of rows.')
    if int(lineElem[1]) != numCols:
        raise Exception('Wrong number of columns.')
    del(lineElem)
    
    # Make new .final file for all results (based on previous .final):
    newFinalFileObj = open(baseName + '_all_' \
                           + str(cellLabel) + '.final', 'w')
    newFinalFileObj.write(line) # Keep first line of orig .init file.
    del(line)
    for ind in range(0, numRows): # Loop over lines in common .final.
        line = prevFinalFileObj.readline()
        if ind in relLineIndList: # Check if relevant line.
            lineElem = line.split(' ')
            del(line)
            
            # Get corresponding line from cell .final file:
            cellLine = cellFinalFileObj.readline()
            cellLineElem = cellLine.split()
            del(cellLine)
            
            # Loop over columns:
            for currCol in range(0, numCols):
                # Make sure cell line is correct and update common line:
                cellVal = cellLineElem[currCol]
                if currCol not in [raCol, riCol]:
                    if cellVal != lineElem[currCol]:
                        raise Exception('Values in .final files do not agree.')
                else:
                    lineElem[currCol] = cellVal # Update ropa/ropi.
                del(cellVal)
            del(currCol, cellLineElem)
            line = ' '.join(lineElem)
            del(lineElem)
        newFinalFileObj.write(line) # Save line in common .final.
        del(line)
    del(numRows, raCol, riCol)
    
    # Close files:
    cellFinalFileObj.close()
    prevFinalFileObj.close()
    newFinalFileObj.close()
    del(cellFinalFileObj, prevFinalFileObj, newFinalFileObj)
    
    # Delete cell .final file and previous common .final file:
    os.remove(cellName + '.final')
    os.remove(baseName + '_all_' + str(epidLabel) + '.final')
    del(cellName, baseName, cellLabel, epidLabel)
    print('Merged .final files and saved updated .final file.')
    
    return

################################################################################

def collstats(baseName, cellName, epidLabelList):
    '''Collects statistics from simulations per cell into one .stats file.
    
    Written by Bettina Greese, CBBP, Uni Lund, Sweden (2013-14).'''
    
    import os
    
    # Make new .stats file:
    newStatsFileObj = open(baseName + '.stats', 'w')
    del(baseName)
    
    # Loop over epidermal cells:
    for cellLabel in epidLabelList:
        newStatsFileObj.write('Cell label: ' + str(cellLabel) + '\n\n')
        
        # Get data from cell .stats file:
        cellStatsFileName = cellName + '_' + str(cellLabel) + '.stats'
        cellStatsFileObj = open(cellStatsFileName, 'r')
        line = cellStatsFileObj.readline()
        while line != '':
            newStatsFileObj.write(line)
            line = cellStatsFileObj.readline()
        del(line)
        cellStatsFileObj.close()
        del(cellStatsFileObj)
        
        # Delete cell .stats file:
        os.remove(cellStatsFileName)
        del(cellStatsFileName)
        
        newStatsFileObj.write('\n')
    del(cellLabel, cellName, epidLabelList)
    
    newStatsFileObj.close()
    del(newStatsFileObj)
    print('Saved file with statistics from all cell simulations.')
    
    return

################################################################################

