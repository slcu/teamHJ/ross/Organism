#! /usr/bin/env python
# Module with function that runs one ROP simulation run.
#
# Written by Bettina Greese, CBBP, Uni Lund, Sweden (2014-15).
#

################################################################################

def onerun(baseName, simLabel, tolCount, parDict, levelList, doKeepDataFile):
    '''Runs one simulation run for ROP.
    
    Written by Bettina Greese, CBBP, Uni Lund, Sweden (2014).'''
    
    import os, shutil, pickle, subprocess
    import numpy as np
    from simfiles import shellbinfiles
    from execsim import checksim, extract, merge
    
    # Parameter values:
    timeR = parDict['timeR'][1]
    sTolR = parDict['sTolR'][1]
    sCountR = parDict['sCountR'][1]
    saveTime = parDict['saveTime'][1]    
    
    print('Run next ROP simulation')

    if simLabel != 0:
        # Copy previous .final into .init file:
        shutil.copy2(baseName + '.final', baseName + '.init')
        print('Copied .final into .init file.')
    
    # Calculate start and end time:
    startTime = simLabel*timeR
    endTime = (simLabel+1)*timeR

    # Make solver file:
    shellbinfiles.obin(baseName, startTime, endTime, saveTime)
    print('Saved solver file.')

    # Run simulation:
    subprocess.call(['chmod', '+x', 'osimR.sh'])
    subprocess.call(['./osimR.sh', baseName])
    print('Finished simulation ' + str(simLabel) + ' for ROP phase')

    # Save maximal absolute derivative for current simulation:
    maxDeriv = checksim.maxderiv('rop', baseName)
    if np.isnan(maxDeriv):
        raise Exception('Maximal absolute derivative is NaN.') 
    
    # Count consecutive max differences below error tolerance:
    if maxDeriv < sTolR:
        tolCount += 1
    else:
        tolCount = 0

    # Remove .init file and rename or delete .data file:
    os.remove(baseName + '.init')
    if doKeepDataFile == 1:
        os.rename(baseName + '.data', baseName + '_' + str(simLabel) + '.data')
        print('Renamed .data file.')
    elif doKeepDataFile == 0:
        os.remove(baseName + '.data')    

    # Update list with maximal absolute derivative:
    if simLabel == 0:
        maxDerivList = []
    else:
        # Unpickle previous simulation statistics:
        fileObj = open(baseName + '_stats.pkl', 'r')
        [prevSimLabel, prevTolCount, prevTimeR, maxDerivList, \
            prevSaveTime] = pickle.load(fileObj)
        fileObj.close()
        del(fileObj)
        if prevTimeR != timeR or prevSaveTime != saveTime \
            or len(maxDerivList) != prevSimLabel + 1:
            raise Exception('Error in parameter values.')
        if prevSimLabel + 1 != simLabel:
            raise Exception('Wrong count of simulations.')
        if tolCount not in [0, prevTolCount + 1]:
            raise Exception('Wrong count of difference below tolerance.')
        del(prevSimLabel, prevTimeR, prevSaveTime)

    # Save maximal absolute derivative for current simulation:
    maxDerivList.append(maxDeriv)
    del(maxDeriv)

    # Pickle simulation statistics:
    pickleName = baseName + '_stats.pkl'
    fileObj = open(pickleName, 'w')
    pickle.dump([simLabel, tolCount, timeR, maxDerivList, saveTime], fileObj)
    fileObj.close()
    print("Saved file " + pickleName)
    del(pickleName, fileObj)

    # Save simulation statistics as text:
    extract.stattext(baseName, simLabel, timeR, maxDerivList)
    del(maxDerivList)

    if doKeepDataFile == 1:
        if simLabel == 0:
            # Start merged file with current .data file:
            merge.mergedata(baseName, ['_' + str(simLabel)], parDict)
        else:
            if os.path.exists(baseName + '_all.data'):
                # Merge previous results with current .data file:
                merge.mergedata(baseName, ['_all', '_' + str(simLabel)], parDict)
            else:
                # Start merged file with current .data file:
                merge.mergedata(baseName, ['_' + str(simLabel)], parDict)

    # Update label for simulation runs:
    simLabel += 1
    if simLabel >= parDict['maxCount'][1]:
        print(simLabel, parDict['maxCount'][1])
        print(os.getcwd())
        raise Exception('Maximal number of simulations reached.')

    # Check if required number of simulations is above threshold:
    if tolCount >= sCountR:

        if doKeepDataFile == 1:
            # Rename final merged .data file:
            os.rename(baseName + '_all.data', baseName + '.data')
            print('Renamed final merged .data file.')
        
        if 'single' in levelList[1] or 'Two' in levelList[1]:
            # Save empty .sums file:
            fileName = baseName + '.sums'
            fileObj = open(fileName, 'w')
            fileObj.write('')
            fileObj.close()
            print('Saved empty file ' + fileName)
            del(fileName, fileObj)
        else: 
            # Extract section data from simulation result:
            extract.summass(baseName, parDict)
        
        # Stop simulation loop:
        simNeeded = False
    else:
        # Continue simulation loop:
        simNeeded = True
    
    return [simLabel, tolCount, simNeeded]

################################################################################

