#! /usr/bin/env python
# Module with functions related to checking which simulations are done.
#
# Written by Bettina Greese, CBBP, Uni Lund, Sweden (2014-15).
#

################################################################################

def maxderiv(simType, baseName):
    '''Returns maximal absolute derivative of auxin, ropa, ropi.
    
    Written by Bettina Greese, CBBP, Uni Lund, Sweden (2014).'''
    
    import pickle, numpy
    from toolbox import variables
    
    # Get column indices for .data file:
    [colIndDict, colList] = variables.colind('.data')
    del(colList)
    
    # Unpickle parDict:
    pickleName = baseName + '_parDict.pkl'
    fileObj = open(pickleName, 'r')
    parDict = pickle.load(fileObj)
    fileObj.close()
    print("Loaded file " + pickleName)
    del(pickleName, fileObj)
    
    # Get .data contents as array:
    dataArray = numpy.loadtxt(baseName + '.data', skiprows=0)
    
    # Extract data from last state:
    lastTimeInd = max(dataArray[:, colIndDict['timeind']])
    rowInd = dataArray[:, colIndDict['timeind']] == lastTimeInd
    del(lastTimeInd)
    lastArray = dataArray[rowInd, :]
    del(dataArray, rowInd)
    
    # Extract derivative lists from last state:
    substDeriv = [] # Initialize list for deriv of all substances.
    if simType == 'auxin':
        keyList = ['auxin_deriv']
    elif simType == 'rop':
        keyList = ['ROPactive_deriv', 'ROPinactive_deriv']
    for currKey in keyList:
        substDeriv.extend(lastArray[:, colIndDict[currKey]])
    del(currKey, keyList, colIndDict, lastArray)
    
    # Find maximal absolute derivative:
    substDeriv = map(abs, substDeriv)
    maxDeriv = max(substDeriv)
    del(substDeriv)
    
    return maxDeriv

################################################################################

def conc(subst, baseName):
    '''Returns list of concentrations of auxin, ropa, ropi.
    
    Written by Bettina Greese, CBBP, Uni Lund, Sweden (2015).'''
    
    import pickle, numpy
    from toolbox import variables
    
    # Get column indices for .final file:
    [colIndDict, colList] = variables.colind('.final')
    if subst == 'ropa':
        substCol = colIndDict['ROPactive']
    elif subst == 'ropi':
        substCol = colIndDict['ROPinactive']
    else:
        substCol = colIndDict[subst]
    del(colList, colIndDict)
    
    # Unpickle parDict:
    pickleName = baseName + '_parDict.pkl'
    fileObj = open(pickleName, 'r')
    parDict = pickle.load(fileObj)
    fileObj.close()
    print("Loaded file " + pickleName)
    del(pickleName, fileObj)
    
    # Load .final data as array:
    finalArray = numpy.loadtxt(baseName + '.final', skiprows=1)
    
    # Extract concentrations and convert to list:
    concArray = finalArray[:, substCol]
    del(finalArray)
    
    return concArray

################################################################################

def stopcrit(baseName, parDict, dirPath):
    '''Returns label of existing simulation, corresp tolerance count and deriv.
    
    Written by Bettina Greese, CBBP, Uni Lund, Sweden (2014-15).'''
    
    import pickle, os
    
    # Unpickle simulation statistics:
    fileObj = open(dirPath + baseName + '_stats.pkl', 'r')
    [prevSimLabel, prevSCount, prevTimeA, prevMaxDerivList, \
        prevSaveTime] = pickle.load(fileObj)
    fileObj.close()
    del(fileObj)
    if prevTimeA != parDict['timeA'][1] \
        or prevSaveTime != parDict['saveTime'][1] \
        or len(prevMaxDerivList) != prevSimLabel + 1:
        raise Exception('Error in parameter values.')
    del(prevSCount, prevTimeA, prevSaveTime)
    
    # Compare previous and current stopping criteria:
    if prevMaxDerivList[-1] < parDict['sTolA'][1]:
        print('Previous simulation was already below stopping tolerance.')
        # Loop over previous maximal absolute derivatives:
        tolCount = 0
        for prevDeriv in prevMaxDerivList:
            if prevDeriv < parDict['sTolA'][1]:
                tolCount += 1
            else:
                tolCount = 0
        if tolCount == parDict['sCountA'][1]:
            check = 'reached'
            print('Previous simulation matches tolerance count.')
        elif tolCount > parDict['sCountA'][1]:
            check = 'toomany'
            print('Previous simulation was already above tolerance count.')
        else:
            check = 'toofew'
            print('Previous simulation was still below tolerance count.')
    else:
        tolCount = 0
        check = 'toofew'
        print('Previous simulation was still below tolerance count.')
    
    del(prevMaxDerivList, parDict)
    
    # Check whether simulation is needed:
    if check == 'reached':
        simNeeded = False
        print('Stopping criteria have been reached.')
    elif check == 'toofew':
        simNeeded = True
        prevSimLabel += 1
        print('Stopping criteria have not been reached.')
    elif check == 'toomany':
        simNeeded = True
        prevSimLabel = 0
        print('Stopping criteria have been exceeded.')
    
    return [check, simNeeded, prevSimLabel, tolCount]

################################################################################

def copyprev(baseName, dirPath, doKeepDataFile):
    '''Copies files from best previous simulation.
    
    Written by Bettina Greese, CBBP, Uni Lund, Sweden (2014-15).'''
    
    import shutil
    
    # Copy simulation files:
    fileTypeList = ['.neigh', '_comp.pkl', '.final', '.stats', '_stats.pkl']
    for fileType in fileTypeList:
        shutil.copy2(dirPath + baseName + fileType, baseName + fileType)
    del(fileType, fileTypeList)
    shutil.copy2(dirPath + 'osimA.sh', 'osimA.sh')
    shutil.copy2(dirPath + 'ropA.model', 'ropA.model')
    if doKeepDataFile == 1 and os.path.exists(baseName + '.data'):
        shutil.copy2(dirPath + baseName + '.data', baseName + '.data')
    print('Copied most files needed for simulation.')
    del(dirPath)
    
    return

################################################################################

def checkprev(baseName, parDict, simPhase):
    '''Checks for previous simulation with lowest maximal absolute derivative.
    
    Written by Bettina Greese, CBBP, Uni Lund, Sweden (2014).'''
    
    import os, pickle
    
    # Check folders with simulations:
    os.chdir('..')
    dirList = os.listdir(os.getcwd())
    
    # Loop over other simulations:
    bestName = ''
    minDeriv = 10000 # Dummy for lowest maximal absolute derivative per sim.
    for dirName in dirList:
        if 'sim_' in dirName:
            # Check if statistics exist for current simulation run:
            os.chdir(dirName)
            if os.path.exists(baseName + '_stats.pkl') == False:
                os.chdir('..')
                continue
            
            # Unpickle statistics of current simulation run:
            fileObj = open(baseName + '_stats.pkl', 'r')
            [prevSimLabel, prevTolCount, prevTimeA, maxDerivList, \
                prevSaveTime] = pickle.load(fileObj)
            fileObj.close()
            del(fileObj)
            del(prevSimLabel, prevTolCount, prevSaveTime)
            if prevTimeA != parDict['timeA'][1]:
                raise Exception('Error in parameter values.')
            del(prevTimeA)
            
            # Check minimum of maximal absolute derivative:
            if min(maxDerivList) < minDeriv:
                minDeriv = min(maxDerivList)
                bestName = dirName
            del(maxDerivList)
            os.chdir('..')
    del(dirName, dirList)
    
    # Change directory:
    os.chdir(simPhase + 'sim_' + str(parDict['sTol' + simPhase][1]) \
                + '_' + str(parDict['sCount' + simPhase][1]))
    
    return bestName

################################################################################

