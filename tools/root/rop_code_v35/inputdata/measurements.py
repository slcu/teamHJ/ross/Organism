#! /usr/bin/env python
# Module with one function for including data from experimental measurements.
#
# Written by Bettina Greese, CBBP, Uni Lund, Sweden (2014).
#

################################################################################

def measdata(parDict, cellTemplate):
    '''Adds measured data, in particular auxin gradients, to dictionary.
    
    Written by Bettina Greese, CBBP, Uni Lund, Sweden (2014).'''
    
    # Settings for cutting root into regions:
    xCutLen = ["cut length x dir", 5.0*10**-3] # Length of regions (in dm).
    numCuts = ["number of cuts", 4] # Num of cuts (one more than number of regions).
    if '7layersS' in cellTemplate:
        numCuts[1] = 3
    
    # Conversion factor from molar concentration to mass concentration:
    convFac = ["conversion factor", 1.75*10**8]
    
    # Auxin mass in three root sections of 0.5 mm length, from tip:
    gradWt = ["meas wt auxin grad", [0.247, 0.154, 0.167], \
             [0.030, 0.008, 0.019]] # Mean and std dev.
    gradBtk = ["meas btk auxin grad", [0.633, 0.381, 0.331], \
              [0.0, 0.0, 0.0]] # Calculate correct standard dev for means.
    
    parDict.update({'xCutLen':xCutLen, 'numCuts':numCuts, 'convFac':convFac \
        , 'gradWt':gradWt, 'gradBtk':gradBtk})
    
    return

################################################################################

