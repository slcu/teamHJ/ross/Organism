#! /usr/bin/env python
# Module with functions related to navigating between directories.
#
# Written by Bettina Greese, CBBP, Uni Lund, Sweden (2014).
#

################################################################################

def codeversion():
    '''Returns code version number for script that calls this function.
    
    Written by Bettina Greese, CBBP, Uni Lund, Sweden (2014).'''
    
    import os
    
    # Get name of current working directory:
    currDir = os.getcwd()
    
    # Find version number in that name:
    vNrEtc = currDir[currDir.find('_v') + 2:]
    del(currDir)
    if '/' in vNrEtc:
        vNr = vNrEtc[:vNrEtc.find('/')]
    else:
        vNr = vNrEtc
    del(vNrEtc)
    
    # Make version string if number exists (local) or leave empty if not (SVN):
    if vNr.isdigit():
        vNrStr = '_v' + vNr
    else:
        vNrStr = ''
    del(vNr)
    
    return vNrStr

################################################################################

def mkchdir(folderName):
    '''Makes new directory if needed and changes to this directory.
    
    Written by Bettina Greese, CBBP, Uni Lund, Sweden (2013).'''
    
    import os
    
    # Make directory if it does not exist already:
    if os.path.isdir(folderName) == False:
        os.mkdir(folderName)
    
    # Change to this directory:
    os.chdir(folderName)
    del(folderName)
    
    return

################################################################################

def dirlevel(infoDict, parDict):
    '''Returns list with names of directories on different levels.
    
    Written by Bettina Greese, CBBP, Uni Lund, Sweden (2014).'''
    
    # Make list with names for various levels:
    levelList = []
    
    # Level 0:
    levelList.append('simu' + infoDict['vNrStr'] + '/')
    
    # Level 1:
    levelList.append(infoDict['cellTemplate'] + '_' \
                   + infoDict['auxinTemplate'] \
                   + infoDict['appTemplate'] + '/')
    
    # Level 2:
    auxinStartName = startname('auxin', parDict, infoDict['consSys'])
    levelList.append(str(infoDict['xyzNumComp'][0]) + '_' \
                   + str(infoDict['xyzNumComp'][1]) + '_' \
                   + str(infoDict['xyzNumComp'][2]) + '_' \
                   + auxinStartName + '/')
    del(auxinStartName)
    
    # Level 3:
    levelList.append(getfoldername('auxin', parDict) + '/')
    
    # Level 4:
    levelList.append('Asim_' + str(parDict['sTolA'][1]) \
                       + '_' + str(parDict['sCountA'][1]) + '/')
    
    # Level 5:
    levelList.append('Aplot_' + str(parDict['sTolA'][1]) \
                       + '_' + str(parDict['sCountA'][1]) + '/')
    
    # Level 6:
    levelList.append('Aframe_' + str(parDict['sTolA'][1]) \
                       + '_' + str(parDict['sCountA'][1]) + '/')
    
    # Level 7:
    ropStartName = startname('rop', parDict, infoDict['consSys'])
    levelList.append(ropStartName + '/')
    
    # Level 8:
    levelList.append(getfoldername('rop', parDict) + '/')
    
    # Level 9:
    levelList.append('Rsim_' + str(parDict['sTolR'][1]) \
                       + '_' + str(parDict['sCountR'][1]) + '/')
    
    # Level 10:
    levelList.append('Rplot_' + str(parDict['sTolR'][1]) \
                       + '_' + str(parDict['sCountR'][1]) + '/')
    
    # Level 11:
    levelList.append('Rframe_' + str(parDict['sTolR'][1]) \
                       + '_' + str(parDict['sCountR'][1]) + '/')
    
    return levelList

################################################################################

def simpath(simType, levelList):
    '''Returns path to auxin simulation directory.
    
    Written by Bettina Greese, CBBP, Uni Lund, Sweden (2015).'''
    
    # Set levels to be added to the path:
    if simType == 'auxin':
        indList = range(0, 5)
    elif simType == 'rop':
        indList = range(0, 4)
        indList.extend(range(7, 10))
    else:
        raise Exception('Unknown simulation type given.')
    
    # Initialize string for current path to auxin simulation directory:
    currPath = ''
    
    # Add desired levels to current path:
    for currInd in indList:
        currPath = currPath + levelList[currInd]
    del(currInd, indList, levelList)
    
    return currPath

################################################################################

def startname(startType, parDict, consSys):
    '''Returns name for initial conditions of auxin or ROP.
    
    Written by Bettina Greese, CBBP, Uni Lund, Sweden (2014).'''
    
    if startType == 'auxin':
        # Set folder name for auxin:
        startName = 'A_' + str(parDict['initAuxin'][1])
    elif startType == 'rop':
        # Test for conserved system:
        if consSys == 1:
            sysType = 'cons'
        elif consSys == 0:
            sysType = 'dyna'
        # Set folder name for rop:
        startName = sysType + '_R_' + str(parDict['initRopa'][1]) \
                    + '_' + str(parDict['initRopi'][1])
        del(sysType)
    
    return startName

################################################################################

def getfoldername(folderType, parDict):
    '''Returns name of directory for current auxin or ROP parameters.
    
    Written by Bettina Greese, CBBP, Uni Lund, Sweden (2013).'''
    
    # Get lists of parameter names:
    parNameDict = getparnames()
    if folderType == 'auxin':
        parNameList = parNameDict['auxin']
    elif folderType == 'rop':
        parNameList = parNameDict['rop']
    else:
        print(folderType)
        raise Exception("Only 'auxin' or 'rop' can be used as folderType.")
    del(parNameDict)
    
    # Make string from current values for auxin-related parameters:
    folderName = ''
    for parName in parNameList: # Loop over parameters.
        folderName = folderName + str(parDict[parName][1]) + '_'
    del(parName)
    folderName = folderName[:-1] # Delete final underscore.
    
    return folderName

################################################################################

def getparnames():
    '''Returns dictionary with parameter names for parameter groups.
    
    Written by Bettina Greese, CBBP, Uni Lund, Sweden (2013).'''

    # Simulation-related:
    parNameSim = ['xNumComp', 'yNumComp', 'sTolA', 'sTolR', 'sCountA', 'sCountR' \
                , 'timeA', 'timeR', 'initAuxin', 'initRopa', 'initRopi']
    parNameSim.sort()
    
    # Auxin-related:
    parNameAuxin = ['pqc', 'q', 'Da', 'so', 'Def', 'Din', 'Tap', 'Taa', 'Dw', 'si']
    parNameAuxin.sort()
    
    # ROP-related:
    parNameRop = ['a', 'r', 'Du', 'b', 's', 'Dv', 'c', 'k1', 'k2', 'k3', 'k4']
    parNameRop.sort()
    
    # Template-related:
    parNameTempl = ['xLengths', 'yLengths', 'zNumCells', 'pinLoc', 'auxLoc' \
                  , 'sourceLoc', 'sinkLoc', 'prodLoc', 'wallThick']
    parNameTempl.sort()
    
    # Set return list:
    parNameDict = {'sim':parNameSim, 'auxin':parNameAuxin \
                 , 'rop':parNameRop, 'templ':parNameTempl}
    
    return parNameDict

################################################################################

def pardummy(parNameList):
    '''Makes empty file with name combined from parameter names.
    
    Written by Bettina Greese, CBBP, Uni Lund, Sweden (2013).'''
    
    import os
    
    # Set name for dummy file for current parameters:
    fileName = ''
    for parName in parNameList: # Loop over parameters.
        fileName = fileName + parName + '__'
        del(parName)
    fileName = fileName[:-2] # Delete final underscores.
    
    # Make dummy file for current parameters if needed:
    if os.path.isfile(fileName) == False:
        fileObj = open(fileName, 'w')
        fileObj.write(fileName)
        fileObj.close()
        del(fileObj, fileName)
    del(parNameList)

################################################################################

def skipdir(simType, levelList, simStrategy):
    '''Checks if directory for current parameter set exists.
    
    Written by Bettina Greese, CBBP, Uni Lund, Sweden (2015).'''
    
    import os
    
    # Get path to be checked:
    if simType == 'auxin':
        checkPath = os.path.isdir(levelList[3] + levelList[4])
    elif simType == 'rop':
        checkPath = os.path.isdir(levelList[8] + levelList[9])
    
    # Set signal for simulation:
    if checkPath == False:
        doSim = 1
        print('Parameter folder does not exist and simulation will be done.')
    elif checkPath == True:
        print('Parameter folder exists already...')
        if simStrategy == 'continue':
            doSim = 1
            print('... and simulation may be continued/restarted.')
        elif simStrategy == 'skip':
            doSim = 0
            print('... and simulation will be skipped.')
    
    return doSim

################################################################################

def numfiles(fileType, varType, cellTemplate, auxinTemplate, xyzNumComp):
    '''Checks number of previous files of given type.
    
    Written by Bettina Greese, CBBP, Uni Lund, Sweden (2014).'''
    
    import os
    
    # Loop over all files in current folder:
    maxNum = 0
    existList = os.listdir(os.getcwd())
    for existFile in existList:
        # Check if current file is of chosen file type:
        if existFile[-len(fileType):] != fileType:
            continue
        
        # Extract cell template, auxin template, variation for current file:
        currExistFile = existFile.split('_')
        currCellTemplate = currExistFile[0]
        currAuxinTemplate = currExistFile[1]
        if xyzNumComp != []:
            currXyzNumComp = [int(currExistFile[2]), \
                              int(currExistFile[3]), \
                              int(currExistFile[4])]
        
        # Check if current file belongs to chosen cell and auxin template:
        if currCellTemplate != cellTemplate:
            continue
        del(currCellTemplate)
        if currAuxinTemplate != auxinTemplate:
            continue
        del(currAuxinTemplate)
        
        # Check if current file belongs to chosen discretization:
        if xyzNumComp != []:
            if currXyzNumComp != xyzNumComp:
                continue
            del(currXyzNumComp)
        
        # Find highest running number:
        currNum = int(existFile[existFile.rfind('_') \
                + 1:existFile.find(fileType)])
        if currNum > maxNum:
            maxNum = currNum
        del(currNum, existFile, currExistFile)
    del(existList, varType, fileType)
    
    return maxNum

################################################################################




