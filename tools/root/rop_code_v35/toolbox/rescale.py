#! /usr/bin/env python
# Module with functions related to rescaling of distances.
#
# Written by Bettina Greese, CBBP, Uni Lund, Sweden (2012-14).
#

################################################################################

def allpar(parDict, rescDir):
    '''Rescales the parameters as needed to go from um to dm or vice versa.
    
    Written by Bettina Greese, CBBP, Uni Lund, Sweden (2014).'''
    
    # Settings:
    if rescDir == 'um2dm':
        rescFac = 10.0**-5 # Factor to go from micrometer to decimeter.
    elif rescDir == 'dm2um':
        rescFac = 10.0**+5 # Factor to go from decimeter to micrometer.
    else:
        raise Exception('Chosen rescaling ' + rescDir + ' is not implemented.')
    
    # Values for .init file:
    parNameList = ['xLengths', 'yLengths', 'steleRad', 'wallThick']
    parDict = scalepar(parDict, parNameList, rescFac)
    del(parNameList)
    parNameList = ['pinLoc', 'auxLoc', 'sourceLoc', 'sinkLoc', 'prodLoc']
    parDict = scaleloc(parDict, parNameList, rescFac)
    del(parNameList, rescFac)
    
    return

################################################################################

def scalepar(parDict, parNameList, scaleFac):
    '''Scales the values for given parameter list by given factor.
    
    Written by Bettina Greese, CBBP, Uni Lund, Sweden (2014).'''
    
    for parName in parNameList:
        if len(parDict[parName]) == 3: # I.e. description, default, range.
            parDict[parName][1] = scaleFac*parDict[parName][1]
            for ind in range(0, len(parDict[parName][2])):
                parDict[parName][2][ind] = scaleFac*parDict[parName][2][ind]
            del(ind)
        elif len(parDict[parName]) == 2: # I.e. description, default.
            if isinstance(parDict[parName][1], float):
                parDict[parName][1] = scaleFac*parDict[parName][1]
            elif isinstance(parDict[parName][1], list):
                for ind in range(0, len(parDict[parName][1])):
                    parDict[parName][1][ind] = scaleFac*parDict[parName][1][ind]
                del(ind)
            else:
                raise Exception('Entry for ' + parName + ' is neither list nor float.')
        else:
            raise Exception('Invalid format in parDict.')
    del(parName, parNameList)
    
    return parDict

################################################################################

def scaleloc(parDict, parNameList, scaleFac):
    '''Scales the values for given location vector by given factor.
    
    Written by Bettina Greese, CBBP, Uni Lund, Sweden (2014).'''
    
    for parName in parNameList:
        # Make list of indices for rescaling:
        if parName in ['pinLoc', 'auxLoc']:
            numE = 6
        elif parName in ['sourceLoc', 'sinkLoc', 'prodLoc']:
            numE = 5
        numB = len(parDict[parName][1])/numE
        
        # Loop over blocks:
        rescInd = []
        for ind in range(0, numB):
            rescInd.extend(range(ind*numE, ind*numE+4))
        del(ind)
        
        # Rescale values:
        for ind in range(0, numB*numE):
            if ind in rescInd:
                parDict[parName][1][ind] = scaleFac*parDict[parName][1][ind]
        del(ind, numB, numE)
    del(parName, parNameList)
    
    return parDict

