#! /usr/bin/env python
#
# Written by Bettina Greese, CBBP, Uni Lund, Sweden (2014).
#

################################################################################

def gnuplot(baseName, origParDict, pathList, projName, countName, fileType):
    '''Make plots of large-scale gradients for comparison of different situations.
    
    Written by Bettina Greese, CBBP, Uni Lund, Sweden (2014).'''
    
    import subprocess, os, numpy, pickle
    from toolbox import dirnavi
    
    # Program control:
    scaleType = 'lin' # Choose 'lin' or 'log'.
    
    # Get code version number:
    vNrStr = dirnavi.codeversion()
    
    # Change directory:
    os.chdir('../../')
    
    # Initialize array for gradient values and list for averages:
    numSecs = origParDict['numCuts'][1] - 1
    numVar = len(pathList[0])
    compArray = numpy.zeros(shape = (numSecs, 4 + numVar))
    avrgAuxinList = []
    
    # Add section labels to array:
    secList = range(1, numSecs + 1)
    compArray[:, 0] = secList
    
    # Add experimentally measured mass (mean and std dev) to beg of array:
    gradWt = origParDict['gradWt']
    gradWt[1] = gradWt[1][:numSecs]
    gradWt[2] = gradWt[2][:numSecs]
    del(origParDict)
    compArray[:, 1] = gradWt[1] # Measured mean.
    meanWt = numpy.array(gradWt[1])
    stdWt = numpy.array(gradWt[2])
    del(gradWt)
    compArray[:, 2] = meanWt + stdWt # Mean plus standard deviation.
    compArray[:, 3] = meanWt - stdWt # Mean minus standard deviation.
    del(stdWt)
    
    # Add experimentally measured average mass to beginning of list:
    avrgAuxinList.append(float(sum(meanWt)/len(meanWt)))
    del(meanWt)
    
    # Entries for legend:
    legStrList = ['wt mean', 'wt sd+', 'wt sd-'] # Measured.
    
    # Loop over paths to auxin simulations:
    for pInd in range(0, numVar):
        currPath = pathList[0][pInd]
        
        # Change directory:
        os.chdir(currPath)
        
        # Unpickle parDict:
        pickleName = baseName + '_parDict.pkl'
        fileObj = open(pickleName, 'r')
        parDict = pickle.load(fileObj)
        fileObj.close()
        print("Loaded file " + pickleName)
        del(pickleName, fileObj)
        
        # Make legend entry:
        if 'default' == projName:
            legStrList.append(projName)
        elif 'parvar' in projName \
        or 'stopcrit' in projName:
            # Find name and value of varied parameter:
            parName = projName[projName.rfind('_') + 1:]
            legStrList.append(parName + ' = ' + str(parDict[parName][1]))
            del(parName)
        elif 'vartempl' in projName:
            # Find name of cell and auxin template:
            legEntry = currPath[currPath.find('/')+1:]
            legEntry = legEntry[:legEntry.find('/')]
            indUn = legEntry.find('_')
            legEntry = legEntry[:indUn] + ' - ' + legEntry[indUn + 1:]
            legStrList.append(legEntry)
            del(legEntry, indUn)
        del(currPath)
        
        # Get .sum data as array:
        auxinSumList = numpy.loadtxt(baseName + '.sums')
        
        # Extract gradient:
        addSumList = auxinSumList[0:numSecs]
        
        # Add summed mass data to array:
        compArray[:, pInd + 4] = addSumList
        
        # Get total summed mass of auxin:
        totalMass = auxinSumList[numSecs]
        del(auxinSumList)
        
        # Add average summed mass to list:
        avrgAuxinList.append(float(totalMass/len(addSumList)))
        del(totalMass, addSumList)
        
        # Change directory:
        os.chdir('../../../../..')

    del(pInd, pathList, numSecs)

    # Check data:
    if len(avrgAuxinList) != numVar + 1:
        raise Exception('List with average auxin has wrong length.')
    if len(legStrList) != numVar + 3:
        raise Exception('Legend list has wrong length.')
    
    # Change directory:
    os.chdir('results' + vNrStr + '/' + projName)
    del(vNrStr, projName)

    # Save array as text in .plot file:
    plotName = countName + '_secgrad_final_auxin.plot'
    numpy.savetxt(plotName, compArray)
    print("Saved file " + plotName)
    del(compArray, countName)
    
    # Settings for plot:
    if fileType == '.eps':
        terminalStr = 'set term post enhanced color ' \
                      'portrait size 26 cm, 15 cm'
        terminalStr = 'set term post enhanced color ' \
                      'portrait size 15 cm, 10 cm linewidth 1.3'
    elif fileType == '.png':
        terminalStr = 'set term pngcairo enhanced color ' \
                      'size 26 cm, 15 cm'
    if scaleType == 'lin':
        scaleText = '\n### Plot with linear scale ###\n'
        yAxisBeg = 0
        yAxisEnd = 1
    elif scaleType == 'log':
        scaleText = '\n### Plot with logarithmic scale ###\n\n' \
                    + 'set logscale y\n'
        yAxisBeg = 0.1
        yAxisEnd = 1
    else:
        raise Exception("Set scaleType to 'lin' or 'log'.")
    del(scaleType)

    # Set x range:
    xAxisBeg = secList[0] - 0.5
    xAxisEnd = secList[-1] + 0.5
    del(secList)
    
    # Make .gnplt file:
    text = '''# File for plotting results for multicell ROP model using gnuplot.
#
# Written by Bettina Greese based on template from Henrik Jonsson, 
# CBBP, Uni Lund, Sweden (2014).
#
# Specify input data:
dataFileIn = "''' + plotName + '"\n\n' + terminalStr + '''\n
dataFileOut = "''' + plotName[:-5] + fileType + '''"
set output dataFileOut # Set name of output file.
#set key horizontal outside top center height 1.3 # Settings for legend.
#set key horizontal outside center top # Settings for legend.
set key vertical outside center right # Settings for legend.
set size ratio 1.0 # 0.4 #  Set ratio of plot height to width.
#set xtics ("0 - 500" 1, "500 - 1000" 2, "1000 - 1500" 3)
set xtics ("0 - 0.5" 1, "0.5 - 1" 2, "1 - 1.5" 3)
set ytics out mirror # Put y tic marks on both sides.

# Plot summed masses over tissue section (i.e. large-scale gradient):
set xl "tissue section [um]"
set yl "(upscaled) auxin mass [pg]"
'''
    del(fileType, terminalStr)
    
    # Plotting of average auxin as lines:
    for lineInd in range(0, numVar + 1): # Plus 1 for measured average.
        meanVal = str(avrgAuxinList[lineInd])
        if lineInd < 6:
            color = str(lineInd)
        else:
            color = str(lineInd + 1)
        text = text + 'set arrow from ' + str(xAxisBeg) + ', ' \
               + meanVal + ' to ' + str(xAxisEnd) + ',' \
               + meanVal + ' nohead lt 3 lc ' + color + '\n'
        del(meanVal, color)
    del(lineInd)

    # Set scaling of plot:
    text = text + scaleText
    del(scaleText)
    
    # Set axes ranges and plot concentrations:
    text = text + '''
set xrange [''' + str(xAxisBeg) + ':' + str(xAxisEnd) + '''] 
#set yrange [''' + str(yAxisBeg) + ':' + str(yAxisEnd) + ''']

# Concentrations:
plot \\''' # Important: No space allowed after the backslash!
    
    for ind in range(0, numVar + 3): # Plus 3 for measured gradient.
        auxinCol = str(2 + ind) # First column has section numbers.
        titleStr = legStrList[ind]
        if ind in [0, 1, 2]:
            color = '0'
        elif ind < 8:
            color = str(ind - 2)
        else:
            color = str(ind - 1)
         
        text = text + '''
dataFileIn u 1:''' + auxinCol + ' title "' + titleStr \
+ '" w lp lt 1 pt ' + color + ' lc ' + color + ' lw 1.3,\\'
        del(auxinCol, titleStr, ind)
    
    text = text[:-2] # Remove last ',\\'.
    
    text = text + '''
pause 1
'''
    
    del(numVar, avrgAuxinList, xAxisBeg, xAxisEnd, yAxisBeg, yAxisEnd, legStrList)
    
    # Save .gnplt file:
    gnpltName = plotName[:-5] + '.gnplt'
    fileObj = open(gnpltName, 'w')
    fileObj.write(text)
    fileObj.close()
    print('Saved file ' + gnpltName)
    del(text, fileObj)
    
    # Execute .gnplt file:
    subprocess.call(['gnuplot', gnpltName])
    print('Saved result of ' + gnpltName)
    
    # Delete .plot and .gnplt files:
    os.remove(plotName)
    os.remove(gnpltName)
    del(plotName, gnpltName)

    return

