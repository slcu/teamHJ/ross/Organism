#! /usr/bin/env python
#
# Written by Bettina Greese, CBBP, Uni Lund, Sweden (2014).
#

################################################################################

def gnuplot(baseName, currPathList, subst, projName, countName, fileType):
    '''Make plots of various statistics (mean etc.) per cell and compare situations.
    
    Written by Bettina Greese, CBBP, Uni Lund, Sweden (2014-15).'''
    
    import subprocess, os
    import numpy as np
    from toolbox import dirnavi, variables
    
    # Program control:
    scaleType = 'lin' # Choose 'lin' or 'log'.
    
    # Settings:
    xFrame = 10.0**-4
    
    # Get code version number:
    vNrStr = dirnavi.codeversion()
    
    # Get column indices for .final file:
    [colIndDict, colList] = variables.colind('.final')
    del(colList)
    
    # Change directory:
    os.chdir('../../')
    
    # Loop over paths to simulations:
    numVar = len(currPathList)
    legStrList = []
    for pInd in range(0, numVar):
        substPath = currPathList[pInd]
        if subst == 'ropa':
            substName = 'ROPactive'
        elif subst == 'ropi':
            substName = 'ROPinactive'
        else:
            substName = subst
        
        # Change directory:
        os.chdir(substPath)
        
        # Make legend entry:
        if 'discret' in projName or 'constcomp' in projName:
            # Find discretization:
            discretStr = substPath[substPath.find('/')+1:]
            discretStr = discretStr[discretStr.find('/')+1:]
            discretStr = discretStr[:discretStr.find('/')]
            indA = discretStr.find('_A_')
            discretStr = discretStr[:indA]
            # Add entry for legend list:
            indU1 = discretStr.find('_')
            indU2 = discretStr.rfind('_')
            legEntry = discretStr[:indU1] + 'x ' \
                     + discretStr[indU1+1:indU2] + 'y ' \
                     + discretStr[indU2+1:] + 'z '
            legStrList.append(legEntry)
            del(indA, indU1, indU2, discretStr, legEntry)
        elif 'length' in projName:
            cellTemplate = substPath[substPath.find('/')+1:]
            cellTemplate = cellTemplate[:cellTemplate.find('_')]
            legStrList.append(cellTemplate)
            del(cellTemplate)
        elif 'parvar' in projName \
        or 'stopcrit' in projName \
        or 'initcond' in projName:
            # Find name of varied parameter:
            parName = projName[projName.find('_') + 1:]
            # Add entry for legend list:
            legStrList.append(parName + ' var ' + str(pInd))
            del(parName)
        elif 'default' in projName:
            legStrList.append(projName)
        else:
            raise Exception('Specify legend entries for current project.')
        
        # Get .final data as array:
        finalArray = np.loadtxt(baseName + '.final', skiprows=1)
        
        # Extract columns with substance concentrations, x coord and markers:
        if substName in ['roptotal', 'ropratio']:
            substConc1 = finalArray[:, colIndDict['ROPactive']]
            substConc2 = finalArray[:, colIndDict['ROPinactive']]
            substConc = []
            for ind in range(0, len(substConc1)):
                if substName == 'roptotal':
                    newConc = substConc1[ind] + substConc2[ind]
                elif substName == 'ropratio':
                    newConc = substConc1[ind]/(substConc1[ind] + substConc2[ind])
                substConc.append(newConc)
            del(ind, newConc, substConc1, substConc2)
        else:
            substConc = finalArray[:, colIndDict[substName]]
        del(substName)
        xCoord = finalArray[:, colIndDict['x']]
        cellMarker = finalArray[:, colIndDict['cellMarker']]
        sourceMarker = finalArray[:, colIndDict['auxinIn']]
        sinkMarker = finalArray[:, colIndDict['auxinOut']]
        cellLabel = finalArray[:, colIndDict['cellLabel']]
        del(finalArray)
        cellMarker = cellMarker.tolist()
        sourceMarker = sourceMarker.tolist()
        sinkMarker = sinkMarker.tolist()
        
        # Extract substance concentrations only from normal cell compartments:
        relSubstConc = []
        relXCoord = []
        relCellLabel = []
        for ind in range(0, len(cellMarker)):
            if cellMarker[ind] == 1 and sourceMarker[ind] == 0 \
            and sinkMarker[ind] == 0:
                relSubstConc.append(substConc[ind])
                relXCoord.append(xCoord[ind])
                relCellLabel.append(cellLabel[ind])
        del(substConc, xCoord, cellLabel, cellMarker, sourceMarker, sinkMarker)
        relSubstConc = np.array(relSubstConc)
        relXCoord = np.array(relXCoord)
        relCellLabel = np.array(relCellLabel)
        
        # Calculate statistics per cell:
        labelList = list(set(relCellLabel))
        numCells = len(labelList)
        substMpc = [] # List for mean substance per cell.
        substSpc = [] # List for slope of substance per cell.
        substNSpc = [] # List for normalized slope of substance per cell.
        substMaxPc = [] # List for maximum of substance per cell.
        xCoordPc = [] # List for x coordinate per cell.
        for label in labelList:
            # Extract concentrations for current cell:
            currSubstConc = relSubstConc[relCellLabel == label]
            currXCoord = relXCoord[relCellLabel == label]
            currMean = currSubstConc.mean()
            substMpc.append(currMean) # Add mean concentration.
            currCellLen = currXCoord[-1] - currXCoord[0]
            currSlope = (currSubstConc[-1] - currSubstConc[0])/currCellLen
            currNormSlope = currSlope/currMean
            substSpc.append(currSlope) # Add slope of concentration.
            substNSpc.append(currNormSlope) # Add normalized slope.
            substMaxPc.append(currSubstConc.max())
            del(currSubstConc, currMean, currCellLen, currSlope, currNormSlope)
            xCoordPc.append(currXCoord.mean()) # Add mean x coordinate.
            if len(substMpc) != len(xCoordPc) or len(substMpc) != len(substSpc) \
            or len(substMpc) != len(substNSpc):
                raise Exception('Incorrect length of lists.')
            del(currXCoord, label)
        del(relSubstConc, relXCoord)
        del(labelList)
        
        # Reshape arrays:
        substMpc = np.reshape(substMpc, (numCells, 1))
        substSpc = np.reshape(substSpc, (numCells, 1))
        substNSpc = np.reshape(substNSpc, (numCells, 1))
        substMaxPc = np.reshape(substMaxPc, (numCells, 1))
        xCoordPc = np.reshape(xCoordPc, (numCells, 1))
        del(numCells)
        
        # Save mean concentrations and x values in collections:
        if pInd == 0:
            collSubstMpc = substMpc
            collSubstSpc = substSpc
            collSubstNSpc = substNSpc
            collSubstMaxPc = substMaxPc
        else:
            collSubstMpc = np.concatenate((collSubstMpc, substMpc), axis = 1)
            collSubstSpc = np.concatenate((collSubstSpc, substSpc), axis = 1)
            collSubstNSpc = np.concatenate((collSubstNSpc, substNSpc), axis = 1)
            collSubstMaxPc = np.concatenate((collSubstMaxPc, substMaxPc), axis = 1)
        del(substMpc, substSpc, substNSpc)
        
        # Change directory:
        if 'Asim' in substPath:
            os.chdir('../../../../..')
        elif 'Rsim' in substPath:
            os.chdir('../../../../../../..')
        del(substPath)
    
    del(colIndDict, pInd, currPathList)
    
    # Change directory:
    os.chdir('results' + vNrStr + '/')
    os.chdir(projName)
    del(vNrStr)
    
    # Save arrays as text in .plot file:
    plotFileName = countName + '_meanpc_final_' + subst + '.plot'
    dataPc = np.concatenate((xCoordPc, collSubstMpc, collSubstSpc, \
                             collSubstNSpc, collSubstMaxPc), axis = 1)
    np.savetxt(plotFileName, dataPc)
    print("Saved file " + plotFileName)
    del(collSubstMpc, collSubstSpc, collSubstNSpc, collSubstMaxPc, dataPc)
    
    # Settings for plot:
    if fileType == '.eps':
        terminalStr = 'set term post enhanced color ' \
                      'portrait size 26 cm, 8 cm'
        terminalStr = 'set term post enhanced color ' \
                      'portrait size 20 cm, 10 cm'
    elif fileType == '.png':
        terminalStr = 'set term pngcairo enhanced color ' \
                      'size 26 cm, 5 cm'
    if scaleType == 'lin':
        scaleText = '\n### Plot with linear scale ###\n'
    elif scaleType == 'log':
        scaleText = '\n### Plot with logarithmic scale ###\n\n' \
                    + 'set logscale y\n'
    else:
        raise Exception("Set scaleType to 'lin' or 'log'.")
    del(scaleType)

    # Set x and y range:
    xAxisBeg = xCoordPc.min() - xFrame
    xAxisEnd = xCoordPc.max() + xFrame
    del(xCoordPc, xFrame)
    yAxisBeg = 0.001
    yAxisEnd = 0.1
    
    # Make .gnplt files:
    for measType in ['level', 'slope', 'normslope', 'max']:
        plotOutName = plotFileName.replace('.plot', '_' + measType + fileType)
        text = '''# File for plotting mean concentrations per cell using gnuplot.
#
# Written by Bettina Greese, CBBP, Uni Lund, Sweden (2015).
#

# Specify input data:
dataFileIn = "''' + plotFileName + '"\n\n' + terminalStr + '''\n
dataFileOut = "''' + plotOutName + '''"
set output dataFileOut # Set name of output file.

# Settings:
set key horizontal outside center top # Settings for legend.
set size ratio 0.3 #  Set ratio of plot height to width.
set ytics out mirror # Put y tic marks on both sides.
set grid ytics
set xl "x coordinate"
set yl "mean ''' + subst + ' ' + measType + '''"
set xrange [''' + str(xAxisBeg) + ':' + str(xAxisEnd) + ''']
#set yrange [''' + str(yAxisBeg) + ':' + str(yAxisEnd) + ''']
'''
        del(plotOutName)
        
        # Set scaling of plot:
        text = text + scaleText
        
        text = text + '''
# Mean concentration per cell:
plot \\''' # Important: No space allowed after the backslash!
        
        # Plot means:
        if measType == 'level':
            offSet = 2
        elif measType == 'slope':
            offSet = 2 + numVar
        elif measType == 'normslope':
            offSet = 2 + 2*numVar
        elif measType == 'max':
            offSet = 2 + 3*numVar
        for ind in range(0, numVar):
            legEntry = legStrList[ind]
            if ind < 5:
                lineColor = str(ind + 1)
            else:
                lineColor = str(ind + 2)
            text = text + '''
dataFileIn u 1:''' + str(ind + offSet) + ' title "' + legEntry \
+ '" w lp lt 1 lc ' + lineColor + ''' ,\\'''
            del(legEntry, lineColor)
        del(offSet)
        text = text[:-2] # Remove last ',\\'.
        text = text + '\npause 1\n'
        
        # Save .gnplt file:
        gnpltName = plotFileName.replace('.plot', '.gnplt')
        fileObj = open(gnpltName, 'w')
        fileObj.write(text)
        fileObj.close()
        print('Saved file ' + gnpltName)
        del(text, fileObj)
        
        # Execute .gnplt file:
        subprocess.call(['gnuplot', gnpltName])
        print('Saved result of ' + gnpltName)
        
        # Delete .plot and .gnplt files:
        os.remove(gnpltName)
        del(gnpltName)
    
    # Delete .plot file:
    os.remove(plotFileName)
    del(plotFileName)
    
    del(terminalStr, fileType, scaleText, countName)
    del(xAxisBeg, xAxisEnd, yAxisBeg, yAxisEnd, numVar)
    
    return
