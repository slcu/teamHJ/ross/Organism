#! /usr/bin/env python
# Module with one function for plotting of template for Organism.
#
# Written by Bettina Greese, CBBP, Uni Lund, Sweden (2014).
#

################################################################################

def gnuplot(baseName, templName, parDict, zoom):
    '''Plot template illustrations for ROP model with the specified input.
    
    Written by Bettina Greese, CBBP, Uni Lund, Sweden (2013-14).'''

    import os, subprocess
    from toolbox import variables
    from plotfiles import forplots
    
    # Program control:
    doPng = 0
    
    # Settings:
    gnpltName = templName + "_" + str(zoom) + "dm.gnplt"
    vFac = 3*10**11 # Factor for volume (for visualisation).
    rDig = 3 # Number of digits for rounding.
    eTol = 10**-10 # Error tolerance in equality check.
    plotRatio = 0.4
    extraHeight = 4.0 # For colorbox and tic marks.
    extraWidth = 1.0 # For tic marks.
    
    messageSave = 'Saved file '
    
    # Get column indices for .init file:
    [initColIndDict, colList] = variables.colind('.init')
    del(colList)
    cColInit = initColIndDict['cellMarker']
    
    # Open .init file:
    initFileObj = open(baseName + '.init', 'r')
    
    # Open .neigh file:
    neighFileObj = open(baseName + '.neigh', 'r')
    
    # Get information about cell template from first lines in .init and .neigh files:
    numComp = forplots.firstlines(initFileObj, neighFileObj)
    
    # Extract indices of relevant columns in .init file:
    initColNameList = ['x', 'y', 'z', 'V', 'auxinIn', 'auxinOut', 'auxinProd' \
                     , 'cellMarker', 'wallMarker', 'cellLabel']
    initRelColList = []
    for col in initColNameList:
        initRelColList.append(initColIndDict[col])
    del(col)
    
    # Make .plot file:
    plotName = gnpltName[:-6] + '.plot'
    plotFileObj = open(plotName, 'w')
    
    for currComp in range(0, numComp): # Loop over compartments (0 to numComp-1).
        
        # Collect relevant data from .init file:
        relElem = [str(currComp)]
        line = initFileObj.readline()
        lineElem = line.split(' ')
        
        if float(lineElem[initColIndDict['x']]) - zoom < eTol:
            cellMarker = lineElem[cColInit]
            for currCol in initRelColList: # Loop over relevant columns.
                currElem = lineElem[currCol]
                if currCol == initColNameList.index('V'):
                    currElem = str(vFac*float(currElem))
                relElem.append(currElem)
                del(currElem)
        del(line, lineElem)
        
        # Collect relevant data from .neigh file:
        line = neighFileObj.readline()
        lineElem = line.split(' ')
        if cellMarker == '1':
            numNeigh = int(lineElem[1])
            pinInd = numNeigh*2+2 # Index of first PIN entry.
            auxInd = numNeigh*3+2 # Index of first AUX entry.
            pinVal = map(float, lineElem[pinInd:pinInd+numNeigh])
            auxVal = map(float, lineElem[auxInd:auxInd+numNeigh])
            del(pinInd, auxInd)
            pinMax = round(max(pinVal), rDig)
            auxMax = round(max(auxVal), rDig)
            del(pinVal, auxVal)
        else:
            pinMax = 0.0
            auxMax = 0.0
        relElem.append(str(pinMax))
        relElem.append(str(auxMax))
        del(pinMax, auxMax, lineElem, line)
        
        # Save relevant data in .plot file:
        rel = ' '.join(relElem)
        plotFileObj.write(rel + '\n')
        
        del(relElem, rel)
    del(numComp, currComp, initRelColList, initColIndDict)
    
    # Close files:
    plotFileObj.close()
    print(messageSave + plotName)
    del(plotFileObj)
    initFileObj.close()
    del(initFileObj)
    neighFileObj.close()
    del(neighFileObj)
    
    # Calculate x, y, z ranges:
    [xBeg, xEnd, yBeg, yEnd] = forplots.ranges(parDict)
    xEnd = min(xEnd, zoom)
    
    # Calculate plot size:
    plotWidth = 27.0 + extraWidth
    plotHeight = plotWidth*plotRatio + extraHeight
    
    # Make list with column names in .plot file:
    extraColNameList = ['ind']
    plotColNameList = extraColNameList + initColNameList \
                    + ['pinMarker', 'auxMarker']
    del(initColNameList)
    
    # Specify column indices for .plot file:
    plotColIndDict = forplots.colind(plotColNameList, 0)
    del(plotColNameList)
    xCol = str(plotColIndDict['x'] + len(extraColNameList))
    yCol = str(plotColIndDict['y'] + len(extraColNameList))
    zCol = str(plotColIndDict['z'] + len(extraColNameList))
    
    # Contents of .gnplt file:
    text = '''# File for plotting results for multicell ROP model using gnuplot.
#
# Written by Bettina Greese, CBBP, Uni Lund, Sweden (2013).
#

# Specify input data:
dataFileIn = "''' + plotName + '"'

    # Loop over different markers to be plotted:
    markerList = ['ind', 'V', 'auxinIn', 'auxinOut', 'auxinProd' \
                 , 'pinMarker', 'auxMarker', 'cellLabel']
    wCol = str(plotColIndDict['wallMarker'] + len(extraColNameList))
    for marker in markerList:
        mCol = str(plotColIndDict[marker] + len(extraColNameList))
        
        text = text + '''
set term post enhanced color landscape \
size ''' + str(plotWidth) + ''' cm, ''' + str(plotHeight) + ''' cm

dataFileOut1 = "''' + templName + "_" + str(zoom) + "dm_" + marker + '''.eps"
set output dataFileOut1 # Set name of output file.
set size ratio ''' + str(plotRatio) + ''' #  Set ratio of plot height to width.
set xtics out rotate
set ytics out
set border back
set pointsize 0.5

# Plot:'''

        text = text + '''
set xrange [''' + str(xBeg) + ':' + str(xEnd) + ''']
set yrange [''' + str(yBeg) + ':' + str(yEnd) + ''']
'''
        
        if marker == 'ind':
            text = text + '''
plot dataFileIn u ''' + xCol + ':' + yCol + ':' + mCol \
+ ''' with labels font ",6" rotate by 45 title ""
pause 1
'''
        elif marker == 'cellLabel':
            text = text + '''
plot \
dataFileIn u ($''' + wCol + ' == "1" ? $' + xCol + ' : 1/0):' \
+ yCol + ''' w d lc 3 lw 2 title "", \
dataFileIn u ($''' + wCol + ' == "0" ? $' + xCol + ' : 1/0):' \
+ yCol + ':' + mCol + ''' w labels font ",6" rotate by 45 title ""
pause 1
'''
        elif marker == 'V':
            text = text + '''
plot \
dataFileIn u ($''' + wCol + ' == "1" ? $' + xCol + ' : 1/0):' \
+ yCol + ':' + mCol + ''' w p pt 4 lc 3 ps variable title "", \
dataFileIn u ($''' + wCol + ' == "0" ? $' + xCol + ' : 1/0):' \
+ yCol + ':' + mCol + ''' w p pt 4 lc 1 ps variable title ""
pause 1
'''
        elif marker in ['cellMarker', 'wallMarker']:
            text = text + '''
plot \
dataFileIn u ($''' + mCol + ' == "0" ? $' + xCol + ' : 1/0):' \
+ yCol + ''' w p lc -1 pt 1 title "", \
dataFileIn u ($''' + mCol + ' == "1" ? $' + xCol + ' : 1/0):' \
+ yCol + ''' w p lc 1 pt 1 ps 0.8 title ""
pause 1
'''
        else:
            text = text + '''
plot \
dataFileIn u ($''' + wCol + ' == "1" ? $' + xCol + ' : 1/0):' \
+ yCol + ''' w d lc 3 lw 2 title "", \
dataFileIn u ($''' + wCol + ' == "0" ? $' + xCol + ' : 1/0):' \
+ yCol + ''' w p lc -1 pt 1 title "", \
dataFileIn u ($''' + mCol + ' > "0" ? $' + xCol + ' : 1/0):' \
+ yCol + ''' w p lc 1 pt 1 ps 0.8 title ""
pause 1
'''
        
        if doPng == 1:
            text = text + '''
set term pngcairo enhanced color size 26 cm, 5 cm

ind = strstrt(dataFileOut1, "eps") # Find start index of file extension.
dataFileOut2 = dataFileOut1[1:ind-1]."png" # Append new extension.
set output dataFileOut2

replot
pause 1
'''

    del(markerList, marker, plotColIndDict, wCol, mCol)
    del(xBeg, xEnd, yBeg, yEnd)
    
    # Save file:
    fileObj = open(gnpltName, 'w')
    fileObj.write(text)
    fileObj.close()
    print(messageSave + gnpltName)
    del(text, fileObj)
    
    # Execute gnuplot:
    subprocess.call(['gnuplot', gnpltName])
    print('Saved result of ' + gnpltName)
    
    # Delete plot files:
    os.remove(plotName)
    os.remove(gnpltName)
    del(plotName, gnpltName)
    
    return

