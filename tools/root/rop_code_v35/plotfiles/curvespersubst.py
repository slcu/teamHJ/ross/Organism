#! /usr/bin/env python
#
# Written by Bettina Greese, CBBP, Uni Lund, Sweden (2013).
#

################################################################################

def gnuplot(baseName, pathName, parDict, zoom, timeVal, fileType):
    '''Plot concentration curves per substance for one parameter set.
    
    Written by Bettina Greese, CBBP, Uni Lund, Sweden (2012-14).'''
    
    import os, pickle, subprocess
    import numpy as np
    from plotfiles import forplots
    from toolbox import variables
    
    # Program control:
    doPlotWalls = 1
    
    # Settings:
    diffTol = 10**-9 # Set tolerance for checking diff in test for equality.
    if 'A' in pathName:
        substList = ['auxin']
        scaleType = 'lin' # Choose 'lin' or 'log'.
        sTol = parDict['sTolA'][1] # Error tolerance for stopping simulations.
        sCount = parDict['sCountA'][1] # Num of simu runs below tol for stopping.
    elif 'R' in pathName:
        substList = ['ropa', 'ropi']
        scaleType = 'lin'
        sTol = parDict['sTolR'][1] # Error tolerance for stopping simulations.
        sCount = parDict['sCountR'][1] # Num of simu runs below tol for stopping.
    
    if timeVal == 'final':
        # Get column indices for .final file:
        [colIndDict, colList] = variables.colind('.final')
        
        # Check if simulation has been finished:
        if os.path.exists('../' + pathName + baseName + '.sums') == False:
            raise Exception('Simulations for current path are not finished.')
        
        # Load .final data as array:
        currArray = np.loadtxt('../' + pathName + baseName + '.final', skiprows=1)
    else:
        # Get column indices for .data file:
        [colIndDict, colList] = variables.colind('.data')
        
        # Unpickle array from current time point:
        pickleName = baseName + '_data_array_' + str(timeVal) + '.pkl'
        arrayFileObj = open(pickleName, 'r')
        currArray = pickle.load(arrayFileObj)
        arrayFileObj.close()
        del(pickleName, arrayFileObj)
    del(colList)
    keyList = colIndDict.keys()
    
    # Get layer names:
    legendList = forplots.layernames(parDict, 'short')
    
    # Calculate total number of compartments:
    numTotal = parDict['xTotalComp'][1]*parDict['yTotalComp'][1]\
              *parDict['zTotalComp'][1]
    
    # Calculate x and y ranges:
    xBeg = 0.0
    xEnd = zoom
    
    # Calculate x values of walls:
    xWallList = forplots.walls('x', xEnd, parDict)
    
    # Specify column indices for .plot file:
    xCol, wCol, sCol = '1', '2', '3'
    
    # Loop over substances:
    plotY = parDict['plotY'][1]
    for subst in substList:
        if subst == 'auxin':
            substCol = colIndDict['auxin']
            if scaleType == 'lin':
                yBeg = 0.0
                yEnd = 0.1
                #yEnd = 0.5
            elif scaleType == 'log':
                yBeg = 0.001
                yEnd = 0.1
                # To match figures in Jones_2009_ATN: 
                yBeg = 0.003
                yEnd = 3.0
        elif subst == 'ropa':
            substCol = colIndDict['ROPactive']
            yBeg = 0.0
            yEnd = 10.0
        elif subst == 'ropi':
            substCol = colIndDict['ROPinactive']
            yBeg = 0.0
            yEnd = 10.0
        elif subst not in keyList:
            raise Exception('Chosen substance not available.')
        else:
            substCol = colIndDict[subst]
        
        # Loop over y values chosen for plotting:
        for yVal in plotY:
            
            # In current data array, find rows for current y value:
            rowCrit = (abs(currArray[:, colIndDict['y']] - yVal) < diffTol)
            if sum(np.shape(rowCrit)) != numTotal:
                raise Exception('Wrong length of array criterium.')
            if sum(rowCrit) != parDict['xTotalComp'][1]:
                raise Exception('Wrong length of row indices list.')
            
            # Save values for x and wallMarker only once:
            if yVal == plotY[0]:
                relArray = currArray[rowCrit == True, :]
                relArray = relArray[:, [colIndDict['x'], colIndDict['wallMarker']]]
            del(yVal)
            
            # Save values for y in every iteration:
            substArray = currArray[rowCrit == True, substCol]
            del(rowCrit)
            substArray.resize(sum(substArray.shape), 1) # Add second dimension.
            relArray = np.concatenate((relArray, substArray), axis = 1)
            del(substArray)
        del(substCol)
        
        # Save array as text in .plot file:
        if timeVal == 'final':
            plotName = 'curves_final_' + subst + '_' + str(zoom) + 'dm.plot'
        else:
            plotName = 'curves_' + subst + '_' + str(zoom) + 'dm_' \
            + str(timeVal) + '.plot'
        np.savetxt(plotName, relArray)
        print("Saved file " + plotName)
        del(relArray)
        
        # Make one plot for all relevant y values for current substance:
        gnpltName = plotName[:-4] + 'gnplt'
        if fileType == '.eps':
            terminalStr = 'set term post enhanced color ' \
                          'portrait size 26 cm, 12 cm'
            terminalStr = 'set term post enhanced color ' \
                          'portrait size 20 cm, 10 cm'
        elif fileType == '.png':
            terminalStr = 'set term pngcairo enhanced color ' \
                          'size 26 cm, 8 cm'
        text = '''# File for plotting results for ROP model using gnuplot.
#
# Written by Bettina Greese based on template from Henrik Jonsson, 
# CBBP, Uni Lund, Sweden (2013-14).
#
# Specify input data:
dataFileIn = "''' + plotName + '"\n\n' + terminalStr + '''\n
dataFileOut = "''' + plotName[:-5] + fileType + '''"
set output dataFileOut # Set name of output file.
set key horizontal outside top center height 1.3 # Settings for legend.
set size ratio 0.3 #  Set ratio of plot height to width.
set ytics out nomirror # Put y tic marks only on y1 axis.
set xtics out nomirror rotate
set grid ytics

# Plot concentration over cell length (i.e. concentration profile):
set xl "x coordinate [dm]"
set yl "''' + subst + ''' conc [umol/dm^3]"

# Cell walls:

unset arrow
'''
        
        # Plotting of cell walls as lines:
        wallText = forplots.walltext(xWallList)
        text = text + wallText
        del(wallText)
        
        # Set ranges of axes:
        if scaleType == 'lin':
            text = text + '''
### Plot all concentrations with linear scale ###'''
        elif scaleType == 'log':
            text = text + '''
### Plot auxin with logarithmic scale and ROPs with linear scale ###

set logscale y'''
        else:
            raise Exception("Set scaleType to 'lin' or 'log'.")
        text = text + '''
set xrange [''' + str(xBeg) + ':' + str(xEnd) + ''']
#set yrange [''' + str(yBeg) + ':' + str(yEnd) + ''']

# Concentrations:
plot \\'''
        
        # Loop over relevant y values:
        for yInd in range(0, len(plotY)):
            relCol = str(int(sCol) + yInd)
            legEntry = legendList[yInd] + ' (' + str(timeVal) + ')'
            if parDict['xNumComp'][1] == 1:
                lineStyle = 'lp'
            else:
                lineStyle = 'l'
            if doPlotWalls == 1:
                partText = relCol
            elif doPlotWalls == 0:
                partText = '($' + wCol + '<0.5 ? $' + relCol + ': 1/0)'
            if yInd < 5:
                lineColor = str(yInd + 1)
            else:
                lineColor = str(yInd + 2)
            text = text + '''
dataFileIn u ''' + xCol + ':' + partText + ' title "' + legEntry \
+ '" w ' + lineStyle + ' lt 1 lc ' + lineColor + ',\\'
            del(relCol, legEntry, lineStyle, partText, lineColor)
        del(yInd)
        text = text[:-2] + '''
pause 1
'''
        
        # Save .gnplt file:
        fileObj = open(gnpltName, 'w')
        fileObj.write(text)
        fileObj.close()
        print("Saved file " + gnpltName)
        del(text, fileObj)
        
        # Execute .gnplt file:
        subprocess.call(['gnuplot', gnpltName])
        print('Saved result of ' + gnpltName)
        
        # Delete .plot and .gnplt files:
        #os.remove(plotName)
        #os.remove(gnpltName)
        del(plotName, gnpltName)
    del(subst, substList, xWallList, legendList, xBeg, xEnd, currArray, yBeg, yEnd)
    
    return

