#! /usr/bin/env python
#
# Written by Bettina Greese, CBBP, Uni Lund, Sweden (2012-15).
#

################################################################################
### REQUIRED USER INPUT: ###
############################

# CHOOSE MODEL:
baseName = 'rop'
consSys = 0 # Signal (=1) for conservedSystem, i.e. no prod/degr of ROPs.

# CHOOSE EXTERNAL AUXIN TEMPLATE:
# b and e are indices of the first and last cell in x dir to receive auxin:
# s and p are strengths relative to the source and PIN values (in percent)
appTemplate = ''

# CHOOSE IF .data FILE SHOULD BE DELETED TO SAVE DISK SPACE:
doKeepDataFile = 0

# CHOOSE IF CONCENTRATIONS SHOULD BE SAVED AFTER EACH RUN:
doKeepConc = 1

# CHOOSE IF SPECIAL TEST SHOULD BE RUN:
doSteadyState = 0

################################################################################
### USER INPUT FROM ARGUMENTS: ###
##################################

# Import necessary modules:
import time, os, pickle, sys
from toolbox import dirnavi, variables
from simfiles import parvalues, simsettings
from execsim import auxinsim, ropsim

# Get path to folder for simulation output and results:
outPath = simsettings.outpath()

# Get information from input arguments:
argList = sys.argv
argList.extend(['-v', 'none'])
[doRop, cellTemplate, auxinTemplate, varType, xyzNumComp, initCond,
simStrategy] = variables.arguments(argList)
del(varType)

################################################################################

# Print user input for checking:
print(baseName, consSys, cellTemplate, auxinTemplate, appTemplate, xyzNumComp, initCond)
#raw_input('Check settings and press any key to continue:')

# Start timer:
startTime = time.time()

# Get code version number:
vNrStr = dirnavi.codeversion()

# Collect information about template into a dictionary:
infoDict = variables.templinfo(baseName, vNrStr, cellTemplate, auxinTemplate, \
                               appTemplate, xyzNumComp, initCond, consSys, doRop)
del(initCond)

# Get parameter values:
parDict = parvalues.setallpar(infoDict)

# Set names of directories for different levels:
levelList = dirnavi.dirlevel(infoDict, parDict)

# Check if pickled parDict from template exists (as example):
pickleName = outPath + levelList[0] + levelList[1] + levelList[2] \
             + 'template/' + baseName + '_parDict.pkl'
if os.path.exists(pickleName) == False:
    raise Exception('No variable with parameters found, make template first (use run_simfiles.py).')

# Update parDict regarding tolerance for stopping (update of levelList needed!):
simsettings.tolerance(parDict, infoDict['cellTemplate'])
if doSteadyState == 1:
    # Update stopping criteria:
    parDict['sTolA'][1] = 100.0
    parDict['sCountA'][1] = 60
    print(parDict['sTolA'])
    print(parDict['sCountA'])
    print("NOTE: Special stopping criteria for checking steady state!")
    raw_input()
    
    # Update names of directories for different levels:
    levelList = dirnavi.dirlevel(infoDict, parDict)

# Set special values for some parameters (update of levelList needed!):
del(levelList)
#parDict['Du'][1] = 10.0*parDict['Du'][1]
#parDict['Dv'][1] = 10.0*parDict['Dv'][1]
#parDict['q'][1] = 10.0*parDict['q'][1]
#parDict['q'][1] = 0.5*parDict['q'][1]
#parDict['Pa'][1] = 0.0
#parDict['Paux'][1] = 0.0
#parDict['Ppin'][1] = 0.0
#parDict['Da'][1] = 0.0
#parDict['Da'][1] = 4.0*parDict['Da'][1]
#parDict['Ppin'][1] = 2.0*parDict['Ppin'][1] 
#parDict['Paux'][1] = 2.0*parDict['Paux'][1] 
#parDict['Pa'][1] = 2.0*parDict['Pa'][1] 
#parDict['pqc'][1] = 10.0*parDict['pqc'][1]
#parDict['k4'][1] = parDict['k2'][1]
#parDict['k2'][1] = 1.5*parDict['k2'][1]
#parDict['Def'][1] = 1.7*10.0**-8
#parDict['Tap'][1] = 10.0*parDict['Tap'][1]
#parDict['Din'][1] = 1.1*10.0**-6
#parDict['Taa'][1] = 1.3*10.0**-5
#parDict['so'][1] = 3.0*parDict['so'][1]
#parDict['q'][1] = 5.0*10.0**-4
#parDict['b'][1] = 0.7*parDict['b'][1] # to avoid peaks in small cells
#parDict['r'][1] = 0.5*parDict['r'][1]
#parDict['c'][1] = 1.7*parDict['c'][1]
#parDict['r'][1] = 2.5*10.0**-3
#parDict['s'][1] = parDict['r'][1]
#print("NOTE: Some parameters have special values!")
#raw_input()

# Values from Andre's email:
#del(levelList)
#parDict['so'][1] = 0.033598
#parDict['si'][1] = 0.1
#parDict['q'][1] = 1.9*10.0**-5
#parDict['Dw'][1] = 4.0*10.0**-9
#parDict['Da'][1] = 10.0**-8
#parDict['a'][1] = 0.0 
#parDict['Du'][1] = 10.0**-11
#parDict['r'][1] = 0.01 
#parDict['b'][1] = 0.01
#parDict['Dv'][1] = 2.06914*10.0**-12
#parDict['s'][1] = 0.0 
#parDict['c'][1] = 0.1
#parDict['k1'][1] = 0.01
#parDict['k2'][1] = 0.1
#parDict['sCountR'][1] = 540
#print("NOTE: Andre's parameter values!")
#raw_input()

# Update names of directories for different levels:
levelList = dirnavi.dirlevel(infoDict, parDict)

# Change directory:
dirnavi.mkchdir(outPath + levelList[0])
dirnavi.mkchdir(levelList[1])
dirnavi.mkchdir(levelList[2])

# Check for existing auxin simulation directory (levels 3 and 4):
doSim = dirnavi.skipdir('auxin', levelList, simStrategy)

# Change directory:
dirnavi.mkchdir(levelList[3])
dirnavi.mkchdir(levelList[4])

if doSim == 1:
    # Pickle parDict (i.e. save as a variable):
    pickleName = baseName + '_parDict.pkl'
    fileObj = open(pickleName, 'w')
    pickle.dump(parDict, fileObj)
    fileObj.close()
    print("Saved file " + pickleName)
    del(pickleName, fileObj)

    # Save list of parameter values as text:
    variables.partext(baseName, parDict)

    # Run simulation for auxin:
    auxinsim.simulate(baseName, parDict, levelList, doKeepDataFile, doKeepConc)
    print('Finished auxin simulation.')
#elif doSim == 0:
#    if os.path.exists(baseName + '.sums') == False:
#        raise Exception('File .sums is missing for auxin.')
del(doSim)

# Save path to auxin simulation directory:
currPath = dirnavi.simpath('auxin', levelList)
pathList = [[currPath], []]
del(currPath)

if doRop > 0:
    # Change directory:
    dirnavi.mkchdir('../' + levelList[7])
    
    # Check for existing ROP simulation directory (levels 8 and 9):
    doSim = dirnavi.skipdir('rop', levelList, simStrategy)
    
    # Change directory:
    dirnavi.mkchdir(levelList[8])
    dirnavi.mkchdir(levelList[9])
    
    if doSim == 1:
        # Pickle parDict (i.e. save as a variable):
        pickleName = baseName + '_parDict.pkl'
        fileObj = open(pickleName, 'w')
        pickle.dump(parDict, fileObj)
        fileObj.close()
        print("Saved file " + pickleName)
        del(pickleName, fileObj)

        # Save list of parameter values as text:
        variables.partext(baseName, parDict)
        
        # Run simulation for ROP:
        ropsim.simulate(baseName, parDict, levelList, \
                        doKeepDataFile, doKeepConc)
        print('Finished ROP simulation.')
        
        '''
        # If stopping criteria are not the same for all cells, use the following:
        if 'single' in levelList[1]:
            # Run ROP simulation for complete template:
            ropsim.simulate(baseName, parDict, levelList, \
                            doKeepDataFile, doKeepConc)
        else:
            # Run ROP simulation per cell:
            ropsimpercell.simulate(baseName, parDict, levelList, doKeepDataFile)
        '''
    #elif doSim == 0:
    #    if os.path.exists(baseName + '.sums') == False:
    #        raise Exception('File .sums is missing for auxin.')
    del(doSim)
        
    # Save path to ROP simulation directory:
    currPath = dirnavi.simpath('rop', levelList)
    pathList[1].append(currPath)
    del(currPath)

    # Change directory:
    os.chdir('../../')

del(levelList)

dirnavi.mkchdir('../../../../../results' + vNrStr)
dirnavi.mkchdir('default')
del(vNrStr)

# Check number of previous files of given type:
maxNum = dirnavi.numfiles('.txt', 'default', cellTemplate, \
                          auxinTemplate, xyzNumComp)

# Set name for current run of current project:
countName = cellTemplate + '_' + auxinTemplate + appTemplate + '_' \
    + str(xyzNumComp[0]) + '_' + str(xyzNumComp[1]) + '_' \
    + str(xyzNumComp[2]) + '_default_' + str(maxNum+1)
del(cellTemplate, auxinTemplate, appTemplate, xyzNumComp, consSys)

# Save list with paths to simulation directories as text:
fileName = countName + '.txt'
fileObj = open(fileName, 'w')
for simPhase in [0, 1]:
    for currPath in pathList[simPhase]:
        if currPath != '':
            fileObj.write(currPath + '\n')
fileObj.close()
print("Saved file " + fileName)
del(fileName, fileObj, pathList)

# Calculate run time:
endTime = time.time()
runTime = endTime - startTime
del(startTime, endTime)
[runHr, runRem] = divmod(runTime, 3600)
[runMin, runRest] = divmod(runRem, 60)
print('Run time: ' + str(int(runHr)) + ' hrs, ' + str(int(runMin)) + ' mins')
del(runTime, runHr, runRem, runMin, runRest)
