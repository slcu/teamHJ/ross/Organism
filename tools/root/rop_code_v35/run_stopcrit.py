#! /usr/bin/env python
#
# Written by Bettina Greese, CBBP, Uni Lund, Sweden (2014).
#

################################################################################
### REQUIRED USER INPUT: ###
############################

# SET LIST OF ALLOWED VARIATIONS:
specList = ['nothing', 'onlydegr', 'onlyprod', 'prodandpass', 'onlypasstrans' \
, 'onlytransport', 'noactivetr', 'nososi']
varTypeList = ['sTolA', 'sCountA', 'sTolR', 'sCountR']
varTypeList.extend(specList)

# CHOOSE MODEL:
baseName = 'rop'
consSys = 0 # Signal (=1) for conservedSystem, i.e. no prod/degr of ROPs.

# CHOOSE EXTERNAL AUXIN TEMPLATE:
# b and e are indices of the first and last cell in x dir to receive auxin:
# s and p are strengths relative to the source and PIN values (in percent)
appTemplate = ''

# CHOOSE IF .data FILE SHOULD BE DELETED TO SAVE DISK SPACE:
doKeepDataFile = 0

# CHOOSE IF CONCENTRATIONS SHOULD BE SAVED AFTER EACH RUN:
doKeepConc = 1

################################################################################
### USER INPUT FROM ARGUMENTS: ###
##################################

# Import necessary modules:
import os, time, pickle, copy, sys
from toolbox import dirnavi, variables
from simfiles import parvalues, simsettings
from execsim import auxinsim, extract, ropsim, ropsimpercell

# Get path to folder for simulation output and results:
outPath = simsettings.outpath()

# Construct project name:
projName = os.path.basename(__file__)
projName = projName[4:-3]

# Get information from input arguments:
argList = sys.argv
argList.extend(['-s', 'continue']) # Do not use 'skip' here!
[doRop, cellTemplate, auxinTemplate, varType, xyzNumComp, initCond, \
simStrategy] = variables.arguments(argList)
if varType not in varTypeList:
    raise Exception('Given varType is not in varTypeList.')

# CHOOSE ERROR TOLERANCE VALUES TO BE COMPARED (all possible combinations):
if varType == 'sTolA':
    raise Exception('Currently not in use.')
elif varType == 'sTolR':
    raise Exception('Currently not in use.')
elif varType == 'sCountA':
    # Number of simulation runs below tolerance for stopping:
    #varList = [15, 30, 60, 90, 120]
    #varList = [15, 30, 60, 90, 120, 150, 180]
    #varList = [1, 2, 3, 5, 10, 30] # for singleL50
    #varList = [4, 5, 6, 7, 8] # for singleL50
    varList = [5, 10, 20, 30, 60, 90, 120] # for fileL50 or fileVarL1 or fileL100
    #varList = [35, 40, 45, 50] # for fileL50
    #varList = [50, 55, 60, 65] # for fileVarL1
    #varList = [20, 30, 40, 50, 60] # for fileVarL1 and initcond 0.2
elif varType == 'sCountR':
    # Number of simulation runs below tolerance for stopping:
    #varList = [15, 30, 60, 90, 120]
    varList = [5, 10, 20, 30, 60, 90, 120, 150, 180, 210, 240]
    #varList = [60, 120, 180, 240, 300, 360, 420]
    #varList = [120, 180, 240, 300, 360]
    #varList = [40, 45, 50, 55] # for singleL50 and fileL50
elif varType in specList:
    raise Exception('Currently not in use.')

################################################################################

# Print user input for checking:
print(baseName, consSys, cellTemplate, auxinTemplate, appTemplate, xyzNumComp, \
      initCond, varType, varList, simStrategy)
raw_input('Check settings and press any key to continue:')

# Start timer:
startTime = time.time()

# Get code version number:
vNrStr = dirnavi.codeversion()

# Collect information about template into a dictionary:
infoDict = variables.templinfo(baseName, vNrStr, cellTemplate, auxinTemplate, \
                               appTemplate, xyzNumComp, initCond, consSys, doRop)
del(initCond)

# Get parameter values:
origParDict = parvalues.setallpar(infoDict)

# Test for different parameter:
#origParDict['so'][1] = 900*origParDict['so'][1]
#raw_input('Special parameter!')

# Set names of directories for different levels:
origLevelList = dirnavi.dirlevel(infoDict, origParDict)

# Change directory:
os.chdir(outPath + origLevelList[0])
dirnavi.mkchdir(origLevelList[1])
dirnavi.mkchdir(origLevelList[2])
del(origLevelList)

# Check if pickled parDict from template exists (as example):
pickleName = 'template/' + baseName + '_parDict.pkl'
if os.path.exists(pickleName) == False:
    raise Exception('No variable with parameters found, make template first ' \
    + '(use run_simfiles.py).')
del(pickleName)

# Loop over stopping criteria:
pathList = [[], []] # Initialize list for paths to simulation directories.
for currVal in varList:
    
    # Copy original parameter values:
    currParDict = copy.deepcopy(origParDict)
    
    # Update parameter values:
    if varType in specList:
        parNameDict = dirnavi.getparnames()
        if varType in ['onlydegr', 'onlyprod', 'prodandpass']:
            for parName in parNameDict['auxin']:
                if varType == 'onlydegr' and parName == 'q':
                    pass
                elif varType in ['onlyprod', 'prodandpass'] and parName == 'pqc':
                    pass
                elif varType == 'prodandpass' and \
                    parName in ['pK', 'pHc', 'pHw', 'Pa']:
                    pass
                else:
                    currParDict[parName][1] = 0.0
            del(parName, parNameDict)
        elif varType in ['onlytransport', 'onlypasstrans']:
            parNameList = ['pqc', 'q', 'si', 'so']
            if varType == 'onlypassivetr':
                parNameList.extend(['Paux', 'Ppin'])
            for parName in parNameList:
                currParDict[parName][1] = 0.0
            del(parName, parNameList)
        elif varType == 'nothing':
            parNameList = parNameDict['auxin']
            parNameList.extend(parNameDict['rop'])
            for parName in parNameList:
                currParDict[parName][1] = 0.0
            del(parNameList)
        elif varType == 'nososi':
            currParDict['so'][1] = 0.0
            #currParDict['si'][1] = 0.0
            currParDict['si'][1] = 0.1
        if currVal%currParDict['timeA'][1] == 0: # currVal is multiple of timeA.
            timeA = currVal/currParDict['timeA'][1]
        else:
            print(currVal, currParDict['timeA'][1])
            raise Exception('Number of simulation sets must be integer.')
        if timeA != int(timeA):
            raise Exception('Number of simulation sets must be integer.')
        currParDict['sCountA'][1] = timeA 
        currParDict['maxcount'][1] = 60*60*10
    
    if varType not in specList:
        currParDict[varType][1] = currVal
    if varType == 'sTolA':
        currParDict['sCountA'][1] = 1
    elif varType == 'sTolR':
        currParDict['sCountR'][1] = 1
    elif varType == 'sCountA':
        currParDict['sTolA'][1] = 1.0
    elif varType == 'sCountR':
        currParDict['sTolR'][1] = 1.0
    elif varType in specList:
        #currParDict['sTolA'][1] = 1
        currParDict['sTolA'][1] = 10
    
    # Set names of directories for different levels:
    levelList = dirnavi.dirlevel(infoDict, currParDict)
    
    # Check for existing auxin simulation directory (levels 3 and 4):
    doSim = dirnavi.skipdir('auxin', levelList, simStrategy)
    
    # Change directory:
    dirnavi.mkchdir(levelList[3])
    dirnavi.mkchdir(levelList[4])

    if doSim == 1:
        # Pickle currParDict (i.e. save as a variable):
        pickleName = baseName + '_parDict.pkl'
        fileObj = open(pickleName, 'w')
        pickle.dump(currParDict, fileObj)
        fileObj.close()
        print("Saved file " + pickleName)
        del(pickleName, fileObj)
        
        # Save list of parameter values as text:
        variables.partext(baseName, currParDict)
        
        # Run simulation for auxin:
        auxinsim.simulate(baseName, currParDict, levelList, doKeepDataFile, doKeepConc)
    
    del(doSim)
    
    # Save path to auxin simulation directory:
    currPath = dirnavi.simpath('auxin', levelList)
    pathList[0].append(currPath)
    del(currPath)
    
    if doRop > 0:
        # Change directory:
        dirnavi.mkchdir('../' + levelList[7])
        
        # Check for existing ROP simulation directory (levels 8 and 9):
        doSim = dirnavi.skipdir('rop', levelList, simStrategy)
        
        # Change directory:
        dirnavi.mkchdir(levelList[8])
        dirnavi.mkchdir(levelList[9])
        
        if doSim == 1:
            # Pickle parDict (i.e. save as a variable):
            pickleName = baseName + '_parDict.pkl'
            fileObj = open(pickleName, 'w')
            pickle.dump(currParDict, fileObj)
            fileObj.close()
            print("Saved file " + pickleName)
            del(pickleName, fileObj)

            # Save list of parameter values as text:
            variables.partext(baseName, currParDict)
            
            # Run simulation for ROP:
            ropsim.simulate(baseName, currParDict, levelList, \
                            doKeepDataFile, doKeepConc)
            
            '''
            # If stopping criteria are not the same for all cells, use the following:
            if 'single' in levelList[1]:
                # Run ROP simulation for complete template:
                ropsim.simulate(baseName, currParDict, levelList, doKeepDataFile, doKeepConc)
            else:
                # Run ROP simulation per cell:
                ropsimpercell.simulate(baseName, currParDict, levelList, doKeepDataFile)
            '''
        
        del(doSim)

        # Save path to ROP simulation directory:
        currPath = dirnavi.simpath('rop', levelList)
        pathList[1].append(currPath)
        del(currPath)

        # Change directory:
        os.chdir('../../')
    
    del(levelList, currParDict)
    
    # Change directory:
    os.chdir('../..')
    
    del(currVal)
del(specList, infoDict, origParDict, varList)

# Construct project name:
projName = projName + '_' + varType

# Change directory:
dirnavi.mkchdir('../../../results' + vNrStr + '/')
dirnavi.mkchdir(projName)
del(vNrStr)

# Check number of previous files of given type:
maxNum = dirnavi.numfiles('.txt', projName, cellTemplate, auxinTemplate, xyzNumComp)

# Set name for current run of current project:
countName = cellTemplate + '_' + auxinTemplate + appTemplate + '_' \
    + str(xyzNumComp[0]) + '_' + str(xyzNumComp[1]) + '_' \
    + str(xyzNumComp[2]) + '_' + projName + '_' + str(maxNum+1)
del(cellTemplate, auxinTemplate, appTemplate, xyzNumComp, consSys)
del(projName, maxNum)

# Save list with paths to simulation directories as text:
fileName = countName + '.txt'
fileObj = open(fileName, 'w')
for simPhase in [0, 1]:
    for currPath in pathList[simPhase]:
        if currPath != '':
            fileObj.write(currPath + '\n')
fileObj.close()
print('Saved file ' + fileName)
del(countName, fileName, fileObj, pathList)

# Calculate run time:
endTime = time.time()
runTime = endTime - startTime
del(startTime, endTime)
[runHr, runRem] = divmod(runTime, 3600)
[runMin, runRest] = divmod(runRem, 60)
print('Run time: ' + str(int(runHr)) + ' hrs, ' + str(int(runMin)) + ' mins')
del(runTime, runHr, runRem, runMin, runRest)

