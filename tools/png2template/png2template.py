import random
import os
import Image
import sys
import math

from optparse import OptionParser
parser = OptionParser()
parser.add_option("-p", "--parameters", dest="parameter_file",
                  help="Parameter file. File containing the RGB color triplet and corresponding columns in template file (b for background color). Allows multiple columns per color.\\nEx:\\n255 255 255 b\\n255 0 0 1 1 1\\n255 255 0 1 0 1\\n")
parser.add_option("-o", "--output", dest="file_out", default = "template",
                  help="Output file. Default = template")
parser.add_option("-d", "--density", dest="density", default = 100, type = "int",
                  help="Amount of cells along the x-axis. Default = 100")
parser.add_option("-u", "--undefined", dest="undefined_color", type = "int", default = 0,
                  help="Method to use when hitting an undefined color. 0 (default): finds the closest defined color using euclidian distance between pixel RGB tuple and defined RGB tuples. 1: matches the pixel with the most represented color in a sample of pixels located on a cell radius around the undefined pixel")

(options, args) = parser.parse_args()
file_in = args[0]
image = Image.open(file_in)
image = image.transpose(1)
image = image.convert("RGB")

def read_parameters(pfile):
    colors_dict={}
    chan = open(pfile,"r")
    lines = chan.readlines()
    chan.close()
    for l in lines:
        if l != "\n":
            s=l[:-1].split(" ")
            while "" in s:
                s.remove("")
            colors_dict[(int(s[0]),int(s[1]),int(s[2]))]=s[3:]
    return colors_dict

# def get_colors(image, parameter_file=options.parameter_file, color_amount=None, background = (255,255,255)):
#     full_color_list = image.getcolors(image.size[0]*image.size[1])
#     full_color_list.sort()
#     full_color_list.reverse()
#     threshold = image.size[0]*image.size[1]/100
#     print threshold
#     colors_dict = {}
#     if parameter_file != None :
#         colors_dict = read_parameters(parameter_file)
#     elif color_amount != None:
#         None
#     else:
#         for c in full_color_list:
#             if c[0] >= threshold: colors_dict[c[1]]=["1"]
#         colors_dict[background]=["b"]
#     return colors_dict     
    

def get_radius(image,density):
    return image.size[0]/float(density)

def get_undefined_color_euclide(pixel,colors_dict):
    colors=colors_dict.keys()
    best_color,best_distance=(-1,-1,-1),9999999999999999999.
    for c in colors:
        distance= math.sqrt((c[0]-pixel[0])**2 + (c[1]-pixel[1])**2 + (c[2]-pixel[2])**2)
        if distance <= best_distance:
            best_distance = distance
            best_color = c
    return best_color

def get_undefined_color_circle(x,y,radius,colors_dict):
    circle_dict={}
    for theta in range(0,360):
        x2 = radius * math.cos((math.pi * theta) / 180) + x
        y2 = radius * math.sin((math.pi * theta) / 180) + y
        if 0<= x2 <= image.size[0] and 0<= y2 <= image.size[1] :
            pixel = image.getpixel((x2,y2))
            try :
                circle_dict[pixel] = circle_dict[pixel] +1
            except KeyError:
                circle_dict[pixel] = 1
    colors = circle_dict.items()
    colors = [(b,a) for (a,b) in colors]
    colors.sort()
    colors.reverse()
    color=None
    i = 0
    ck = colors_dict.keys()
    try:
      while color == None:
	  if colors[i][1] in ck:
	      color = colors[i][1]
	  i+=1
    except IndexError: 
	print "Unable to get color at %i,%i" % (x,y), colors
    return color
        
        
class Cell:
    def __init__(self,x,y,size,pixel,colors_dict):
        self.x=x
        self.y=y
        self.size=size
        self.values=colors_dict[pixel]
    def string_out(self):
        s="%f %f %f " % (self.x,self.y,self.size)
        for e in self.values:
            s+=e+" "
        s+="\n"
        return s

colors_dict = read_parameters(options.parameter_file)
radius = get_radius(image,options.density)
size=radius + 0.1*radius
cell_list=[]

print image.size[0]/int(radius),image.size[1]/int(radius)

for i in range(0,image.size[0]/int(radius)):
    for j in range(0,image.size[1]/int(radius)):
        x = i * (radius+radius)
        if j%2 == 1:
            x+=math.sqrt(3)*radius /2
        y = j * ( math.sqrt(3)*radius)
        x,y = int(x),int(y)
        if x < image.size[0] and y < image.size[1]:
            pixel = image.getpixel((x,y))
            try:
                if colors_dict[pixel] != ["b"]:
                    cell_list.append(Cell(x,y,size,pixel,colors_dict))
            except KeyError:
                if options.undefined_color == 1: 
                    pixel = get_undefined_color_circle(x,y,radius,colors_dict)
                    if colors_dict[pixel] != ["b"]:
                        cell_list.append(Cell(x,y,size,pixel,colors_dict))
                else :
                    pixel = get_undefined_color_euclide(pixel,colors_dict)
                    if colors_dict[pixel] != ["b"]:
                        cell_list.append(Cell(x,y,size,pixel,colors_dict))

variables = len(cell_list[0].values) + 3

chan_newman=open("newman_out","w")
chan=open(options.file_out,"w")
chan_newman.write("1\n%i %i 0\n" % (len(cell_list),variables))
chan.write("%i %i\n" % (len(cell_list),variables))
for c in cell_list:
    chan.write(c.string_out())
    chan_newman.write(c.string_out())
chan.close()
chan_newman.close()
os.popen("../plot/bin/newman newman_out")
