#include "plotToolGL.h"
#include "myGL.h"


PlotToolGL::PlotToolGL(BaseObject *data, const std::string &file,
		       size_t cc, size_t cs, int size, double gLevel,Camera *cam)
  : BasePlotTool(data, file,cc, cs,size,gLevel)
{
  spotCutOff_=5.0f;
  fullScreen_ = false;
  cam_=cam;
  plotIndex_=false;
  timeIndex_=cam_->startTime();
  std::cerr << timeIndex_ << "\n\n";
  myGL::setGL(this);
  quad_ = gluNewQuadric();
  gluQuadricNormals(quad_,GLU_SMOOTH);
  font_=GLUT_BITMAP_HELVETICA_10;
}

void PlotToolGL::initGL()
{	
  glShadeModel(GL_SMOOTH);						// Enable Smooth Shading
  glClearColor(bgLevel(),bgLevel(),bgLevel(),0.0f); 
  if (!light_) {
    GLfloat global_ambient[] = { 1.0f, 1.0f, 1.0f, 1.0f };
    glLightModelfv(GL_LIGHT_MODEL_AMBIENT, global_ambient);
  }
  glEnable(GL_LIGHTING);
  // enable color tracking
  glEnable(GL_COLOR_MATERIAL);
  // set material properties which will be assigned by glColor
  glColorMaterial(GL_FRONT, GL_AMBIENT_AND_DIFFUSE);
  glClearDepth(1.0f);							// Depth Buffer Setup
  glEnable(GL_DEPTH_TEST);						// Enables Depth Testing
  glDepthFunc(GL_LEQUAL);							// The Type Of Depth Testing To Do
  glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);			// Really Nice Perspective Calculations
  setLook();
}

//Directional light source (w = 0)
//The light source is not positioned into a specific place in the space.
//All the ray come from an infinite position and have the direction (x, y, z).
//The ray are all parallel.
void PlotToolGL::setLight0()
{
  GLfloat noAmbient[] = {0.0f, 0.0f, 0.0f, 1.0f};
  GLfloat whiteDiffuse[] = {1.0f, 1.0f, 1.0f, 1.0f};
  GLfloat position[] = {1.0f, 1.0f, 1.0f, 0.0f};
  
  glLightfv(GL_LIGHT0, GL_AMBIENT, noAmbient);
  glLightfv(GL_LIGHT0, GL_DIFFUSE, whiteDiffuse);
  glLightfv(GL_LIGHT0, GL_POSITION, position);
  if (light0_)
    glEnable(GL_LIGHT0);
}

//
//Positional light source (w = 1)
//The light source is positioned at the location (x, y, z).
//The ray come from this particular location (x, y, z) and goes towards all directions.
//
void PlotToolGL::setLight1()
{
  GLfloat diffuse[] = {0.2f, 0.2f, 0.2f, 1.0f};
  //GLfloat ambient[] = {0.4f, 0.4f, 0.4f, 1.0f};
  GLfloat ambient[] = {0.0f, 0.0f, 0.0f, 1.0f};
  
  GLfloat position[] = {1.0f, 1.0f, 1.0f, 1.0f};
  glLightfv(GL_LIGHT1, GL_AMBIENT, diffuse);
  glLightfv(GL_LIGHT1, GL_DIFFUSE, ambient);
  glLightfv(GL_LIGHT1, GL_POSITION, position);
  if (light1_)
    glEnable(GL_LIGHT1);
}
void PlotToolGL::setSpot()
{     
  GLfloat noAmbient[] = {0.2f, 0.2f, 0.2f, 1.0f};       //low ambient light
  GLfloat diffuse[] = {0.4f, 0.4f, 0.4f, 1.0f};
  GLfloat position[] = {0.0f, 0.0f, 0.0f, 1.0f};
  
  //properties of the light
  glLightfv(GL_LIGHT2, GL_AMBIENT, noAmbient);
  glLightfv(GL_LIGHT2, GL_DIFFUSE, diffuse);
  glLightfv(GL_LIGHT2, GL_POSITION, position);
  //Spot properties*/
  float direction[] = {0, 0, -1};
  //spot direction
  glLightfv(GL_LIGHT2, GL_SPOT_DIRECTION, direction);
  updateSpot();
  
  glLightf(GL_LIGHT2, GL_SPOT_EXPONENT, 15.0f);
  
  //light attenuation (default values used here : no attenuation with the distance)
  glLightf(GL_LIGHT2, GL_CONSTANT_ATTENUATION, 1.0f);
  glLightf(GL_LIGHT2, GL_LINEAR_ATTENUATION, 0.0f);
  glLightf(GL_LIGHT2, GL_QUADRATIC_ATTENUATION, 0.0f);
  if (light2_)
    glEnable(GL_LIGHT2);
}

void PlotToolGL::updateSpot()
{
  //angle of the cone light emitted by the spot : value between 0 to 180
  glLightf(GL_LIGHT2, GL_SPOT_CUTOFF, spotCutOff_);   
}

void PlotToolGL::plot()
{
  int argc = 0;; 
  char *argv[1];
  glutInit(&argc,argv);
  glutInitDisplayMode(GLUT_DEPTH | GLUT_DOUBLE | GLUT_RGBA );
  glutInitWindowPosition(0,0);
  glutInitWindowSize(size(),size());
  glutCreateWindow("Organism");
  
  if (light_)
    light0_= light1_=true;
  else 
    light0_= light1_=false;
  light2_=false;
  setLight0();
  setLight1();
  setSpot();
  initGL();
  
  glutKeyboardFunc(myGL::processNormalKeys);
  glutSpecialFunc(myGL::processSpecialKeys);
  glutDisplayFunc(myGL::renderScene);
  glutIdleFunc(myGL::renderScene);
  glutReshapeFunc(myGL::changeSize);
  glEnable(GL_DEPTH_TEST);
  glutMainLoop();
}

void PlotToolGL::plotAxis()
{
  if (object_->dimension()==1)
    return;
  glColor3f(1.0f,1.0f,1.0f);
  Vector3 com=object_->COM();
  
  GLfloat factor = 2/(object_->scale());
  GLfloat shift = object_->maxR()*factor+0.05;
  GLfloat ticLength=0.015f;
  GLfloat xStart = (object_->minX()-com.x())*factor;
  GLfloat yStart = (object_->minY()-com.y())*factor;
  GLfloat zStart = (object_->minZ()-com.z())*factor;
  GLfloat xEnd   = (object_->maxX()-com.x())*factor;
  GLfloat yEnd   = (object_->maxY()-com.y())*factor;
  GLfloat zEnd   = (object_->maxZ()-com.z())*factor;
  GLfloat zOrigin=0.0;
  if (object_->dimension()==3)
    zOrigin= zStart-shift;
  
  //plot values on axis
  for (GLfloat tic=xStart; tic<=xEnd; tic+=0.2*(xEnd-xStart)) 
    plotValue(tic/factor+com.x(),tic,yStart-1.8*shift, zOrigin);
  for (GLfloat tic=yStart; tic<=yEnd; tic+=0.2*(yEnd-yStart)) 
    plotValue(tic/factor+com.y(),xEnd+1.8*shift,tic, zOrigin);
  if (object_->dimension()==3)
    for (GLfloat tic=zStart; tic<=zEnd; tic+=0.2*(zEnd-zStart)) 
      plotValue(tic/factor+com.y(),xEnd+1.8*shift,yStart-1.8*shift-0.1, tic);
  
  glBegin(GL_LINES);
  //x-axis
  glVertex3f(xStart-shift, yStart-shift, zOrigin); // origin of the x-axis
  glVertex3f(xEnd+shift, yStart-shift, zOrigin); // ending point of the x-axis
  glVertex3f(xStart-shift, yEnd+shift, zOrigin); // origin of the x-axis
  glVertex3f(xEnd+shift, yEnd+shift, zOrigin); // ending point of the x-axis
  if (object_->dimension()==3) {
    glVertex3f(xStart-shift, yStart-shift, zEnd+shift); // origin of the x-axis
    glVertex3f(xEnd+shift, yStart-shift, zEnd+shift); // ending point of the x-axis
    glVertex3f(xStart-shift, yEnd+shift, zEnd+shift); // origin of the x-axis
    glVertex3f(xEnd+shift, yEnd+shift, zEnd+shift); // ending point of the x-axis
  }
  //plot tics
  for (GLfloat tic=xStart; tic<=xEnd; tic+=0.2*(xEnd-xStart)) {
    glVertex3f(tic,yStart-shift,zOrigin);
    glVertex3f(tic,yStart-shift+ticLength,zOrigin);
    glVertex3f(tic,yEnd+shift,zOrigin);
    glVertex3f(tic,yEnd+shift-ticLength,zOrigin);
    if (object_->dimension()==3) {
      glVertex3f(tic,yStart-shift,zEnd+shift);
      glVertex3f(tic,yStart-shift+ticLength,zEnd+shift);
      //glVertex3f(tic,yStart-shift,zEnd+shift);
      //glVertex3f(tic,yStart-shift,zEnd+shift-ticLength);
      glVertex3f(tic,yEnd+shift,zEnd+shift);
      glVertex3f(tic,yEnd+shift-ticLength,zEnd+shift);
      //glVertex3f(tic,yEnd+shift,zEnd+shift);
      //glVertex3f(tic,yEnd+shift,zEnd+shift-ticLength);
    }
  }
  //y-axis
  glVertex3f(xStart-shift, yStart-shift, zOrigin); // origin of the y-axis
  glVertex3f(xStart-shift, yEnd+shift, zOrigin); // ending point of the y-axis
  glVertex3f(xEnd+shift, yStart-shift, zOrigin); // origin of the y-axis
  glVertex3f(xEnd+shift, yEnd+shift, zOrigin); // ending point of the y-axis
  if (object_->dimension()==3) {
    glVertex3f(xStart-shift, yStart-shift, zEnd+shift); // origin of the x-axis
    glVertex3f(xStart-shift, yEnd+shift, zEnd+shift); // ending point of the x-axis
    glVertex3f(xEnd+shift, yStart-shift, zEnd+shift); // origin of the x-axis
    glVertex3f(xEnd+shift, yEnd+shift, zEnd+shift); // ending point of the x-axis
  }
  //plot tics
  for (GLfloat tic=yStart; tic<=yEnd; tic+=0.2*(yEnd-yStart)) {
    glVertex3f(xStart-shift,tic,zOrigin);
    glVertex3f(xStart-shift+ticLength,tic,zOrigin);
    glVertex3f(xEnd+shift,tic,zOrigin);
    glVertex3f(xEnd+shift-ticLength,tic,zOrigin);
    if (object_->dimension()==3) {
      glVertex3f(xStart-shift,tic,zEnd+shift);
      glVertex3f(xStart-shift+ticLength,tic,zEnd+shift);
      glVertex3f(xEnd+shift,tic,zEnd+shift);
      glVertex3f(xEnd+shift-ticLength,tic,zEnd+shift);
    }
  }
  //if 3D, z-axis
  if (object_->dimension()==3) {
    glVertex3f(xStart-shift, yStart-shift, zStart-shift); 
    glVertex3f(xStart-shift, yStart-shift, zEnd+shift); 
    glVertex3f(xEnd+shift, yStart-shift, zStart-shift); 
    glVertex3f(xEnd+shift, yStart-shift, zEnd+shift); 
    glVertex3f(xStart-shift, yEnd+shift, zStart-shift); 
    glVertex3f(xStart-shift, yEnd+shift, zEnd+shift); 
    glVertex3f(xEnd+shift, yEnd+shift, zStart-shift); 
    glVertex3f(xEnd+shift, yEnd+shift, zEnd+shift); 
    double sqrt2Inv = 1/std::sqrt(2);
    for (GLfloat tic=zStart; tic<=zEnd; tic+=0.2*(zEnd-zStart)) {
      glVertex3f(xStart-shift, yStart-shift, tic); 
      glVertex3f(xStart-shift+ticLength*sqrt2Inv, yStart-shift+ticLength*sqrt2Inv, tic);
      glVertex3f(xEnd+shift, yStart-shift, tic); 
      glVertex3f(xEnd+shift-ticLength*sqrt2Inv, yStart-shift+ticLength*sqrt2Inv, tic);
      glVertex3f(xStart-shift, yEnd+shift, tic); 
      glVertex3f(xStart-shift+ticLength*sqrt2Inv, yEnd+shift-ticLength*sqrt2Inv, tic);
      glVertex3f(xEnd+shift, yEnd+shift, tic); 
      glVertex3f(xEnd+shift-ticLength*sqrt2Inv, yEnd+shift-ticLength*sqrt2Inv, tic);
      
    }	
  }
  glEnd( );
}

template<typename T>
void PlotToolGL::plotValue(const T value,GLfloat x,GLfloat y,GLfloat z) 
{	
  glDisable(GL_LIGHTING);
  std::stringstream ss;
  //ss << value;
  ss << std::showpos << std::setprecision(4) << std::setw(6) <<  value;
  //ss << std::setprecision(4) << std::setw(6) <<  value;
  std::string tmp = ss.str();
  
  glRasterPos3f(x,y,z);
  for (size_t i = 0; i<tmp.size(); ++i) {	
    glutBitmapCharacter(font_, tmp[i]);
  }		
  glEnable(GL_LIGHTING);
}

void PlotToolGL::plotIndex(size_t index)
{
  glColor3f(1.0f,1.0f,1.0f);
  Vector3 pos = object_->position(timeIndex_,index,0);
  double factor=(2/object_->scale());
  pos -= object_->COM();
  pos *= factor;
  plotValue(index, pos.x(), pos.y(), pos.z());
  glBlendFunc(GL_SRC_COLOR, GL_ONE);	
}

void PlotToolGL::renderScene(void) {
  
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
  glMatrixMode(GL_MODELVIEW);
  if (plotAxis_)
    plotAxis();
  // Draw all cells at time point
  for (size_t cellIndex=0; cellIndex < object_->numCell(timeIndex_); ++cellIndex) {	
    glPushMatrix();
    if (plotIndex_) {
      plotIndex(cellIndex);
    }
    setColor(object_->variable(timeIndex_, cellIndex, colorColumn_));
    glColor3f(red(),green(),blue());
    
    object_->plotGL(cellIndex, timeIndex_);
    glPopMatrix();
  }
  if (outputFlag())
    writeTiff();
  glutSwapBuffers();
  
  if ( cam_->updateCamera())
    setLook();
  timeIndex_+= cam_->speed();
  if (timeIndex_>= object_->TMax()) { 
    timeIndex_=cam_->startTime();
  }  
}

void PlotToolGL::changeSize(int w, int h) {
  
  // Prevent a divide by zero, when window is too short
  // (you cant make a window of zero width).
  if(h == 0)
    h = 1;
  
  float ratio = 1.0f * w / h;
  // Reset the coordinate system before modifying
  glMatrixMode(GL_PROJECTION);
  glLoadIdentity();
  // Set the viewport to be the entire window
  glViewport(0, 0, w, h);
  // Set the clipping volume
  gluPerspective(45,ratio,1,1000);
  glMatrixMode(GL_MODELVIEW);
  setLook();
}

void PlotToolGL::setLook() {
  glLoadIdentity();
  Vector3 a = cam_->position()- cam_->e3();
  gluLookAt(cam_->position().x(),cam_->position().y(),cam_->position().z(),
	    a(0),a(1),a(2),cam_->e2().x(),cam_->e2().y(),cam_->e2().z());
  glRotatef(cam_->angleX(),cam_->e1().x(),cam_->e1().y(),cam_->e1().z());
  glRotatef(cam_->angleY(),cam_->e2().x(),cam_->e2().y(),cam_->e2().z());
  glRotatef(cam_->angleZ(),cam_->e3().x(),cam_->e3().y(),cam_->e3().z());
}

void PlotToolGL::processSpecialKeys(int key, int x, int y) {
  int mod = glutGetModifiers();
  switch (key) {
  case GLUT_KEY_LEFT : 			
    if (mod == GLUT_ACTIVE_SHIFT)   
      cam_->rotateZ(-3.0);
    else if (mod == GLUT_ACTIVE_CTRL) 
      cam_->roll(-3.0);
    else if (mod == GLUT_ACTIVE_ALT)
      cam_->yaw(3.0);
    else
      cam_->moveLeft();
    break;
  case GLUT_KEY_RIGHT :
    if (mod == GLUT_ACTIVE_SHIFT)
      cam_->rotateZ(3.0);	
    else if (mod == GLUT_ACTIVE_ALT)
      cam_->yaw(-3.0);
    else if (mod == GLUT_ACTIVE_CTRL)
      cam_->roll(3.0);
    else
      cam_->moveRight();
    break;
  case GLUT_KEY_UP :
    if (mod == GLUT_ACTIVE_SHIFT)
      cam_->rotateX(3.0);
    else if (mod == GLUT_ACTIVE_ALT)
      cam_-> pitch(-3.0);
    else
      cam_->moveUp();
    break;
  case GLUT_KEY_DOWN : 
    if (mod == GLUT_ACTIVE_SHIFT)
      cam_->rotateX(-3.0);	
    else if (mod == GLUT_ACTIVE_ALT) 
      cam_->pitch(3.0);	
    else
      cam_->moveDown();
    break;			
  case GLUT_KEY_F1 :
    fullScreen_ = !fullScreen_;
    if( fullScreen_ )
      glutFullScreen();
    else {
      glutPositionWindow(1,1);
      glutReshapeWindow(size(),size());
      glutPostRedisplay();
    }
    break;
  }
  setLook();
}

void PlotToolGL::processNormalKeys(unsigned char key, int x, int y) {
  switch(key)
    {
    case 27: 
      exit(0);
    case 'c' :
      if(colorColumn_) {
	colorColumn_--;
	std::cerr << "Color column changed to " << colorColumn_ << "\n";
      } break;
    case 'C':
      if(colorColumn_<object_->numCol()-object_->varIndex()-1) {
	colorColumn_++;    
	std::cerr << "Color column changed to " << colorColumn_ << "\n";
      } break;
    case 't':
      if( cam_->speed() ) 
	cam_->setSpeed(cam_->speed()-1);
      break;
    case 'T':
      cam_->setSpeed(cam_->speed()+1);
      break;
    case 'p':
      if( cam_->speed() ) {
	std::cerr << "System paused at time index " << timeIndex_ << ".\n";
	cam_->setSpeed(0);
      }
      else {
	std::cerr << "System restarted.\n";
	cam_->setSpeed(1);
      } break;
    case '-': 
      if (!cam_->speed()) {
	if( timeIndex_>0 ) {
	  timeIndex_--;
	  std::cerr << "Time index decreased to " << timeIndex_ << ".\n";
	}
      } break;
    case '+' :
      if (!cam_->speed()) {
	if( timeIndex_<object_->TMax()-1 ) {
	  timeIndex_++;
	  std::cerr << "Time index increased to " << timeIndex_ << ".\n";
	}
      } break; 
    case 's':
      if(colorSchema_!=0 ) {
	colorSchema_--;
	std::cerr << "Color schema set to " << colorSchema_ << ".\n";
      } break;
    case 'S':
      if(colorSchema_!=numSchema()-1 ) {
	colorSchema_++;
	std::cerr << "Color schema set to " << colorSchema_ << ".\n";
      } break;
    case 'z':
      cam_->zoomIn();
      setLook();
      break;
    case 'Z':
      cam_->zoomOut();
      setLook();
      break;
    case 'l':
      gLevel_ /= 1.1f;
      if( gLevel_<0.001f ) gLevel_=0.001f; 
      std::cerr << "gLevel=" << gLevel_ << "\n";
      break;
    case 'L' :
      gLevel_ *= 1.1f;
      if( gLevel_>1.0f ) gLevel_=1.0f;
      std::cerr << "gLevel=" << gLevel_ << "\n";
      break;
    case 'b' :
      bgLevel_ /= 1.1f;
      if( bgLevel_<0.001f ) bgLevel_=0.001f; 
      glClearColor(bgLevel_, bgLevel_, bgLevel_, 0.0f);
      std::cerr << "bgLevel=" << bgLevel_ << "\n";
      break;
    case 'B' :
      bgLevel_ *= 1.1f;
      if( bgLevel_>1.0f ) bgLevel_=1.0f;
      glClearColor(bgLevel_, bgLevel_, bgLevel_, 0.0f);
      std::cerr << "bgLevel=" << bgLevel_ << "\n";
      break;
    case '0':
      light0_ =!light0_;
      if (light0_)
	glEnable(GL_LIGHT0);
      else
	glDisable(GL_LIGHT0);
      setLook();
      break;
    case '1':
      light1_ =!light1_;
      if (light1_)
	glEnable(GL_LIGHT1);
      else
	glDisable(GL_LIGHT1);
      setLook();
      break;
    case '2' :
      light2_ =!light2_;
      if (light2_)
	glEnable(GL_LIGHT2);
      else
	glDisable(GL_LIGHT2);
      setLook();
      break;
    case 'f':
      spotCutOff_ -= 1.0f;
      if (spotCutOff_<0.0)
	spotCutOff_ = 0.0f;
      updateSpot();
      break;
    case 'F':
      spotCutOff_ += 1.0f;
      if (spotCutOff_>180.0)
	spotCutOff_ = 180.0f;
      updateSpot();
      break;
    case 'a':
      plotAxis_=!plotAxis_;
      break;
    case 'i':
      plotIndex_=!plotIndex_;
      if (plotIndex_) {
	glEnable(GL_BLEND);		// Turn Blending On
	glDisable(GL_DEPTH_TEST);
      }
      else {
	glDisable(GL_BLEND);	
	glEnable(GL_DEPTH_TEST);
      }
      break;
      
    }
}

int PlotToolGL::writeTiff() const
{
  static size_t frameCount=0;
  //Create outString
  std::ostringstream ost;
  ost << outputFile();
  if( frameCount<10 ) ost << "0";
  if( frameCount<100 ) ost << "0";
  ost << frameCount << ".tif";
  std::string tmpString = ost.str();
  //std::cerr << "Writing file " << tmpString << "\n";
  const char *fileName = tmpString.c_str();
  //Print pixels into tif image
  TIFF *file;
  GLubyte *image, *p;
  int i;
  int samplesPerPixel=3;
  int width = glutGet(GLUT_WINDOW_WIDTH);
  int height =  glutGet(GLUT_WINDOW_HEIGHT);
  int x =0;
  int y=0;
  file = TIFFOpen(fileName, "w");
  if (file == NULL) {
    return 1;
  }
  if( (image = (GLubyte *) malloc(width * height * sizeof(GLubyte) * 
				  samplesPerPixel) ) == NULL ) {
    std::cerr << "writetiff() Could not allocate enough memory.\n";
    exit(-1);
  }
  
  /* OpenGL's default 4 byte pack alignment would leave extra bytes at the
     end of each image row so that each full row contained a number of bytes
     divisible by 4.  Ie, an RGB row with 3 pixels and 8-bit componets would
     be laid out like "RGBRGBRGBxxx" where the last three "xxx" bytes exist
     just to pad the row out to 12 bytes (12 is divisible by 4). To make sure
     the rows are packed as tight as possible (no row padding), set the pack
     alignment to 1. */
  glPixelStorei(GL_PACK_ALIGNMENT, 1);
  
  glReadPixels(x, y, width, height, GL_RGB, GL_UNSIGNED_BYTE, image);
  TIFFSetField(file, TIFFTAG_IMAGEWIDTH, (uint32) width);
  TIFFSetField(file, TIFFTAG_IMAGELENGTH, (uint32) height);
  TIFFSetField(file, TIFFTAG_BITSPERSAMPLE, 8);
  //TIFFSetField(file, TIFFTAG_COMPRESSION, compression);
  TIFFSetField(file, TIFFTAG_PHOTOMETRIC, PHOTOMETRIC_RGB);
  TIFFSetField(file, TIFFTAG_SAMPLESPERPIXEL, samplesPerPixel);
  TIFFSetField(file, TIFFTAG_PLANARCONFIG, PLANARCONFIG_CONTIG);
  TIFFSetField(file, TIFFTAG_ROWSPERSTRIP, 1);
  TIFFSetField(file, TIFFTAG_IMAGEDESCRIPTION, "OpenGL-rendered cells");
  p = image;
  //Old tiff writing
  for (i = height - 1; i >= 0; i--) {
    if (TIFFWriteScanline(file, p, i, 0) < 0) {
      free(image);
      TIFFClose(file);
      //return 1;
      std::cerr << "Could not write image\n";
      exit(-1);
    }
    p += width * sizeof(GLubyte) * 3;
  }
  //New tiff writing
  //   char *pTmp;
  //   if( (pTmp = (char *) malloc(width * height * sizeof(GLubyte) * 
  // 				  samplesPerPixel) ) == NULL ) {
  //     std::cerr << "writetiff() Could not allocate enough pTmp memory.\n";
  //     exit(-1);
  //   }
  //   for( int c=0 ; c<width*height*samplesPerPixel ; c++ )
  //     pTmp[c] = char(image[c]);
  //   if(TIFFWriteEncodedStrip(file, 0, pTmp, 
  // 			   width*height*samplesPerPixel) == 0){
  //     std::cerr << "Could not write image\n";
  //     exit(42);
  //   }
  TIFFClose(file);
  frameCount++;
  if ( cam_->settings() ) {
    if (frameCount >= cam_->TMax()) 
      exit(0);
  }
  else { 
    if (frameCount >= object_->TMax())
      exit(0);
  }
  return 0;
}
