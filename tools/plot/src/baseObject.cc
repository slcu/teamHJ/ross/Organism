//
// Filename     : baseObject.cc
// Description  : Base class for objects to be plotted
// Author(s)    : Pontus Melke (pontus@thep.lu.se)
// Created      : April 2007
// Revision     : $Id:
//
#include <cstdlib>
#include "sphere.h"
#include "cappedCylinder.h"
#include "ellipse.h"
#include "buddingYeast.h"
#include "cylinder.h"
#include "hexagon.h"
#include <cstdlib>

using myVector3::Vector3;
//Only Constructor
BaseObject::BaseObject(Data *data, size_t dimension, std::string min, std::string max) 
{
  if (dimension !=1 && dimension != 2 && dimension!=3) {
    std::cerr << "BaseObject::BaseObject(): " 
	      <<dimension <<"D objects are not supported\n";
    std::exit(EXIT_FAILURE);
  }
  dimension_= dimension;
  data_=data;	
  quad_ = gluNewQuadric();
  gluQuadricNormals(quad_,GLU_SMOOTH);
  if (min.empty())
    truncateMin_=false;
  else {
    truncateMin_=true;
    minValue_=atof(min.c_str());
  }
  if (max.empty())
    truncateMax_=false;
  else {
    truncateMax_=true;
    maxValue_=atof(max.c_str());
  }  
  //neighbor_.resize(0);
}

void BaseObject::plotGL(size_t cellIndex, size_t timeIndex)
{
  std::cerr << "BaseObject::plotGL: This object cannot be plotted in openGL-mode\n";
  std::exit(EXIT_SUCCESS);
}

void BaseObject::plotPS(std::ofstream &OUT,size_t t,size_t cellIndex, 
			float r, float g, float b, int move, double scaleFact)
{
  std::cerr << "BaseObject::plotPS: This object cannot be plotted in PS-mode\n";
  std::exit(0);
}

void BaseObject::setMax(double value) {
  maxValue_=value;
  truncateMax_ = true;
}

void BaseObject::setMin(double value) {
  minValue_=value;
  truncateMin_ = true;
}

//finds max and min value of each column in Data object
void BaseObject::findMaxMin() {
  if ( data_==0) {
    std::cerr << "No Data object has been asigneed\n";
    std::exit(EXIT_FAILURE);
  }
  Matrix tmp = data_->getFrame(0).getMatrix();
  max_=min_= tmp[0];
  for( size_t t=0 ; t<data_->frames() ; ++t ) { 
    tmp = data_->getFrame(t).getMatrix();
    for( size_t i=0 ; i<tmp.size() ; ++i ) {
      for( size_t j=0 ; j<tmp[i].size() ; ++j )
	if( tmp[i][j]>max_[j] )
	  max_[j]=tmp[i][j];
	else if( tmp[i][j]<min_[j] )
	  min_[j]=tmp[i][j];
    }
  }
    
  if (truncateMax_) {
    std::cerr << "truncate max\n";
    for (size_t i=varIndex_; i<max_.size(); ++i) {
      //if (max_[i]>maxValue_)
      max_[i]=maxValue_;
    }
    //truncateMax_=false;
  }
  if (truncateMin_) {
    std::cerr << "truncate min\n";
    for (size_t i=varIndex_; i<max_.size(); ++i) {
      //if (minx_[i]<minValue_)
      min_[i]=minValue_;
    }
    //truncateMin_=false;
  }
  std::cerr << "printing min/max values:\n";
  std::cerr << "min \t max\n";
  for (size_t i=0; i<max_.size(); ++i) 
    std::cerr << min_[i] << "\t" << max_[i] << "\n";
}

//finds max and min of the spatial coordinates and sets member scale to the
//biggest max(maxi - mini), where i = X;Y;Z
void BaseObject::setSpaceScale()
{
  maxX_ = max_[xList_[0]];
  minX_ = min_[xList_[0]];
  minY_=maxY_=minZ_=maxZ_=0.0;
  if (dimension()>1) {
    maxY_ = max_[yList_[0]];
    minY_ = min_[yList_[0]];
    if(dimension()==3) {
      maxZ_=max_[zList_[0]];
      minZ_ = min_[zList_[0]];
    }
  }
  for (size_t i=1; i<numSpatialVar();++i) {
    if (max_[xList_[i]] > maxX_)
      maxX_=max_[xList_[i]];
    if (min_[xList_[i]] < minX_)
      minX_=min_[xList_[i]];
    if (dimension()>1) {
      if (max_[yList_[i]] > maxY_)
	maxY_=max_[yList_[i]];
      if (min_[yList_[i]] < minY_)
	minY_=min_[yList_[i]];
      if (dimension()==3) {
	if (max_[zList_[i]] > maxZ_)
	  maxZ_=max_[zList_[i]];
	if (min_[zList_[i]] < minZ_)
	  minZ_=min_[zList_[i]];
      }
    }
  }
  scale_ = maxX_-minX_;
  if( (maxY_-minY_)>scale_ )
    scale_ = maxY_-minY_;
  if( (maxZ_-minZ_)>scale_ )
    scale_ = maxZ_-minZ_;
  
  if( scale_<=0.0 ) {
    scale_=1.0;  //If no topology do not scale...
    std::cerr << "normalizeSpace() Warning, no extension in any dimension.\n";
  }
}

//returns the "center of mass" of the object
myVector3::Vector3 BaseObject::COM()
{
  myVector3::Vector3 tmp(0.5*(minX_+maxX_),0.5*(minY_+maxY_),
			 0.5*(minZ_+maxZ_));
  return tmp;
}

//Normalize all variables to [0:1]
void BaseObject::normalizeVariables() {
  for (size_t t=0; t<data_->frames(); ++t) {
    Matrix tmp = data_->getFrame(t).getMatrix();
    for( size_t i=0 ; i<tmp.size() ; ++i ) {
      for( size_t j=varIndex(); j<tmp[0].size() ; j++ ) {
	double delta = max_[j]-min_[j];
	if( delta<=0.0 ) 
	  tmp[i][j] = 0.5;
	else
	  tmp[i][j] = (tmp[i][j]-min_[j])/delta;
      }
    }
    data_->getFrame(t).setMatrix(tmp);
  }
}

void BaseObject::truncateData() {
  if (truncateMax_)
    truncateMax();
  if (truncateMin_)
    truncateMin();
}

void BaseObject::truncateMax() {
  
  for (size_t t=0; t<data_->frames(); ++t) {
    Matrix tmp = data_->getFrame(t).getMatrix();
    for( size_t i=0; i<tmp.size() ; ++i ) {
      for( size_t j=varIndex() ; j<tmp[i].size() ; ++j ) {
	if (tmp[i][j] > maxValue_) {
	  tmp[i][j]=maxValue_;
	}
      }
    }
    data_->getFrame(t).setMatrix(tmp);	
  }
}

void BaseObject::truncateMin() {
  for (size_t t=0; t<data_->frames(); ++t) {
    Matrix tmp = data_->getFrame(t).getMatrix();
    for( size_t i=0 ; i<tmp.size() ; ++i ) {
      for( size_t j=varIndex() ; j<tmp[i].size() ; ++j ) {
	if (tmp[i][j] < minValue_)
	  tmp[i][j]=minValue_;
      }
    }
    data_->getFrame(t).setMatrix(tmp);
  }
}

void BaseObject::logVariables(size_t j) {
  for (size_t t=0; t<data_->frames(); ++t) {
    Matrix tmp = data_->getFrame(t).getMatrix();
    for( size_t i=0 ; i<tmp.size() ; ++i ) {
      tmp[i][j] = std::log(tmp[i][j]);
    }
    data_->getFrame(t).setMatrix(tmp);
  }
}

//returns the value of variable 'species' of cell 'cellIndex' at time 't'
double BaseObject::variable(size_t t, size_t cellIndex, size_t species) const 
{
  if (cellIndex>numCell(t)) {
    std::cerr << "cellIndex is out of scope\n";
    std::exit(EXIT_FAILURE);
  }
  if (species+varIndex() >numCol()-1)
    species = numCol()-1-varIndex();
  return data_->getFrame(t).getMatrix()[cellIndex][species+varIndex()];
}
//returns the position of position variable 'posVar' of cell 'cellIndex' at
//time 't'
myVector3::Vector3 BaseObject::position(size_t t, size_t cellIndex,size_t posVar) const
{
  if (posVar> numSpatialVar()) {
    std::cerr << "Object has only " << numSpatialVar() << " spatial variables\n";
    std::exit(EXIT_FAILURE);
  }
  
  myVector3::Vector3 tmp;
  switch (dimension()) {
  case 3:
    tmp.setVector3( data_->getFrame(t).getMatrix()[cellIndex][xList_[posVar]],
		    data_->getFrame(t).getMatrix()[cellIndex][yList_[posVar]],
		    data_->getFrame(t).getMatrix()[cellIndex][zList_[posVar]] );
    
    break;
  case 2:
    tmp.setVector3( data_->getFrame(t).getMatrix()[cellIndex][xList_[posVar]],
		    data_->getFrame(t).getMatrix()[cellIndex][yList_[posVar]],0);
    break;
  case 1:
    tmp.setVector3( data_->getFrame(t).getMatrix()[cellIndex][xList_[posVar]],0,0);
    break;
  default:
    std::cerr << "BaseObject::position() Dimension " << dimension() << "is invalid\n";
    std::exit(EXIT_FAILURE);
  }
  return tmp;
}

double BaseObject::radius(size_t t, size_t cellIndex, size_t posVar) const
{
  if (posVar> numSpatialVar()) {
    std::cerr << "Object has only " << numSpatialVar() << " spatial variables\n";
    std::exit(EXIT_FAILURE);
  }
  return data_->getFrame(t).getMatrix()[cellIndex][rCol(posVar)];
}

size_t BaseObject::rCol(size_t posVar) const
{
  if (posVar> numSpatialVar()) {
    std::cerr << "Object has only " << numSpatialVar() << " spatial variables\n";
    std::exit(EXIT_FAILURE);
  }
  return rCol_[posVar];
}

//returns the number of neíghbors of cell 'cellIndex' at time 'time'
size_t BaseObject::numNeigh(size_t time, size_t cellIndex) const
{
  if (neighbor_.size() && cellIndex < neighbor_[time].size()) 
    return neighbor_[time][cellIndex].size();
  else {
    std::cerr << "BaseObject::numNeigh(): trying to exceed vector index\n";
    std::exit(EXIT_FAILURE);
  }
}
//returns index of neihgbor 'neighbor' of cell 'cellIndex' at time 'time'
size_t BaseObject::neighbor(size_t time, size_t cellIndex, size_t neighbor) const
{
  if (neighbor_.size() && cellIndex < neighbor_[time].size() && 
      neighbor < neighbor_[time][cellIndex].size() ) 
    return neighbor_[time][cellIndex][neighbor];
  else {
    std::cerr << "BaseObject::neighbor(): trying to exceed vector index\n";
    std::exit(EXIT_FAILURE);
  }
}
//returns max(i) where i = X,Y,Z
double BaseObject::maxPosition (size_t i) const
{
  if (i>2) {
    std::cerr << i << " is out of range\n";
    std::exit(EXIT_FAILURE);
  }
  return i ==0 ? maxX() : ( i == 1 ? maxY() :maxZ());
}
//returns min(i) where i = X,Y,Z
double BaseObject::minPosition (size_t i) const
{
  if (i>2) {
    std::cerr << i << " is out of range\n";
    std::exit(EXIT_FAILURE);
  }
  return i ==0 ? minX() : ( i == 1 ? minY() :minZ());
}

//Object factory returns pointer to instance of derived class
BaseObject *objectFactory(std::string id,
			  std::istream &in, size_t dim, std::string min, std::string max)
{
  OrganismInputMethod O;
  Data *data= O.readInput(in);
  if (id == "cappedCylinder")
    return new CappedCylinder(data,dim, min, max);
  else if (id == "sphere")
    return new Sphere(data,dim, min, max);
  else if (id == "ellipse")
    return new Ellipse(data,dim, min, max);
  else if (id == "buddingYeast")
    return new BuddingYeast(data,dim, min, max);
  else if (id == "cylinder")
    return new Cylinder(data,dim, min, max);
  else if (id == "hexagon")
    return new Hexagon(data, dim, min, max);
  else {
    std::cerr << id << " is not a supported object\n";
    std::exit(EXIT_FAILURE);
  }
} 
