//
// Filename     : baseObject.h
// Description  : Base class for objects to be plotted
// Author(s)    : Pontus Melke (pontus@thep.lu.se)
// Created      : April 2007
// Revision     : $Id:
//
#ifndef BASE_OBJECT_H
#define BASE_OBJECT_H
#include "src/vector3.h"
#include "../../organismDTK/inputMethod.h"

//#include <GL/gl.h>
//#include <GL/glu.h>
//#include <GL/glut.h>
#include <OpenGL/gl.h>
#include <OpenGL/glu.h>
#include <GLUT/glut.h>
#include <iostream>
#include <fstream>
#include <string>
#include <vector>

const double Pi = 3.141592653589793238512808959406186204433;

///
///@brief A base class for describing cell populations of some sort 
///
///
/// The BaseObject class is a base class defining an interface for
/// describing different types of cell populations. The class takes a
/// pointer to a Data object and an integer setting the dimension of
/// the object.
///
/// Each derived class must specify which colmumns in the Data object
/// which contains spatial coordinates and which columns contains
/// other variables. The derived class must also define a neighborhood
/// function which list the neighbors of each cell.
///
class BaseObject {
 public:
	/// @brief Main constructor, each derived class must call this
	/// constructor!
	///
	/// @param data object with matrices describing the state of the
	/// population at each time step
	///
	/// @param dimension dimension of the system
	///
	BaseObject(Data *data, size_t dimension, std::string min, std::string max);

	/// @brief virtual destructor, enables polymorphism 
	virtual ~BaseObject() {};
  
  ///
	/// @brief virtual plot function. 
	///
	/// It specifies how the actual objects are plotted, must be implemented in
	/// derived classes.
	///
	virtual void plotGL(size_t cellIndex, size_t time);
  
	///
	/// @brief virtual plot function. 
	///
	/// It specifies how the actual objects are plotted, must be implemented in
	/// derived classes.
	///
	virtual void plotPS(std::ofstream &OUT,size_t t,size_t cellIndex, 
							float r, float g, float b, int move, double scaleFact);

  ///
	/// @brief Sets the neighbors of each cell at each timepoint.
  ///
  /// This function must be implemented in derived classes, it sets the
  /// neighbors of each cell at each timepoint. Is used in plotToolPS in order
  /// to properly 'deform' cells.
  ///
	virtual void setNeighbor(double rFactor=1) {neighbor_.resize(0);}
	///
  /// @brief finds max and min value of each column in Data object
  ///
	void findMaxMin();

	///
	/// @brief Normalize all variables @f$ v @f$ to @f$ v \in [0:1] @f$
	///
	void normalizeVariables();
	void truncateData();
	void truncateMax();
	void truncateMin();

	void logVariables(size_t col);

	///
	/// @brief Finds max and min of the spatial coordinates and sets member scale
	/// to the biggest of them.
	///
	void setSpaceScale();	
	
	///
	/// @brief returns the number of spatial variables that describes the object
	/// (e.g. sphere 1, cappedCylinder 2 etc.)
	///
	inline size_t numSpatialVar() const {return numSpatialVar_;}
	
	///
	/// @brief returns the number of dimensions
	///
	inline size_t dimension() const {return dimension_;}
	
	///
	/// @brief returns the index of the 'radius-column'
	///
	size_t rCol(size_t posVar=0) const; 
	
	///
	/// @brief returns the total number of columns in the data
	///
	inline size_t numCol() const {return data_!=0?data_->getFrame(0).getMatrix()[0].size():0;}
	
	///
	/// @brief returns the number of cells at time \e t
	///
	inline size_t numCell(size_t t) const {return data_->getFrame(t).size();} 
	
  ///
	/// @brief returns the last time position in \e data
	///
	inline size_t TMax() const {return data_->frames();}
	
	///
	/// @brief returns numberof neighbors of cell \e cellIndex at time \e t
	///
	size_t numNeigh(size_t t, size_t cellIndex) const;
	
	///
	/// @brief returns index of neihgbor 'neighbor' of cell \e cellIndex at time
	/// \e t
	///
	size_t neighbor(size_t t, size_t cellIndex, size_t neigh) const;
	
	
	///
	/// @brief returns the value of column 'species' of cell \e cellIndex at time
	/// \e t
	///
	double variable(size_t t, size_t cellIndex, size_t species) const;
	
	///
	/// @brief returns index of the first variable
	///
	inline size_t varIndex() const {return varIndex_;}
	
	///
	/// @brief returns the spatial scale of object
	///
	inline double scale() const {return scale_;}
	
	///
	/// @brief returns maximal value of x-coordinate
	///
	inline double maxX() const {return maxX_;}
	
	///
	/// @brief returns maximal value of y-coordinate
	///
	inline double maxY() const {return maxY_;}
	
	///
	/// @brief returns maximal value of z-coordinate
	///
	inline double maxZ() const {return maxZ_;}

	///
	/// @brief returns minimal value of x-coordinate
	///
	inline double minX() const {return minX_;}

	///
	/// @brief returns minimal value of y-coordinate
	///
	inline double minY() const {return minY_;}

	///
	/// @brief returns minimal value of z-coordinate
	///
	inline double minZ() const {return minZ_;}

	///
	/// @brief returns max(i) where i = 0,1,2 corresponding to X,Y,Z
	///
	double maxPosition (size_t i) const;
	
	///
	/// @brief returns min(i) where i = 0,1,2 corresponding to X,Y,Z
	///
	double minPosition (size_t i) const;
	
	///
	/// @brief returns maximal value of \e radius column
	///
	inline double maxR() const{return max_[rCol()];}
	
	///
	/// @brief returns minimal value of 'radius' column
	///
	inline double minR() const{return min_[rCol()];}
	
	///
	/// @brief returns radius of cell \e cellIndex at time \e t
	///
	virtual double radius(size_t t, size_t cellIndex, size_t posVar=0) const;
	
	///
	/// @brief returns the "center of mass" of the object
	///
	myVector3::Vector3 COM();	
	
	///
  /// @brief returns the position of position variable \e posVar of cell
  /// \e cellIndex at time \e
	///
	myVector3::Vector3 position(size_t t, size_t cellIndex,size_t posVar) const;

	void setMin(double value);
	void setMax(double value);

 
protected:
	BaseObject(const BaseObject &){}
	size_t numCol_,dimension_,numSpatialVar_, varIndex_;
	double maxX_,maxY_, maxZ_, minX_,minY_, minZ_,scale_, minValue_, maxValue_;
	bool truncateMax_, truncateMin_; 
	Data *data_;
  std::vector<double> max_,min_;
	std::vector<size_t> xList_,yList_,zList_, rCol_;
	GLUquadricObj *quad_;
	std::vector< std::vector< std::vector<int> > > neighbor_;
};

///
/// @brief Factory for objects, all creation should be mapped onto this
/// one
///
/// Given the id an object of the defined type is returned (using
/// new Class). 
///
/// @param in contains the file name where the data is stored
///
/// @param dim is a dimension parameter
///
/// @return Returns a pointer to an instance of an object class defined
/// 
///
BaseObject *objectFactory(std::string id,std::istream &in, size_t dim, std::string min, std::string max);

#endif //BASE_OBJECT_H
