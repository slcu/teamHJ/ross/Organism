#include <cstdlib>
#include "buddingYeast.h"
#include <cstdlib>
using myVector3::Vector3;

BuddingYeast::BuddingYeast(Data *data, size_t dim, std::string min, std::string max)
	:BaseObject(data,dim, min, max)
{
	numSpatialVar_=2;
	xList_.resize(numSpatialVar_);
	yList_.resize(numSpatialVar_);
	rCol_.resize(numSpatialVar_);
	if(dimension()==3)
		zList_.resize(numSpatialVar_);
	if (dimension()==2) {
		varIndex_=6;
		xList_[0] = 0;
		yList_[0] = 1;
		rCol_[0]= 4;
		xList_[1] = 2;
		yList_[1] = 3;
		rCol_[1]= 5;
	}
	else if(dimension()==3) {
		varIndex_=8;
		xList_[0] = 0;
		yList_[0] = 1;
		zList_[0] = 2;
		rCol_[0]= 6;
		xList_[1] = 3;
		yList_[1] = 4;
		zList_[1] = 5;
		rCol_[1]= 7;
	}
	else {
		std::cerr << "BuddingYeast::BuddingYeast(): " 
							<<dimension() <<"D objects are not supported\n";
		std::exit(EXIT_FAILURE);
	}
	truncateData();
	findMaxMin();
	normalizeVariables();
	setSpaceScale();
	findMaxMin();
	//setNeighbor();
}

/*void BuddingYeast::setNeighbor(double rFactor)
{ 	
	double d,r;
	Vector3 posi,posj;
	neighbor_.resize(TMax());
	for( size_t t=0 ; t<TMax() ; ++t ) {
		neighbor_[t].resize(numCell(t) );
		for( size_t i=0 ; i<numCell(t) ; i++ )
      for( size_t j=i+1 ; j<numCell(t); j++ ) {
				r = radius(t,i,0)+radius(t,i,1) + radius(t,j)+radius(t,j,1);
				posi = 0.5*(position(t,i,0)+position(t,i,1));
				posj = 0.5*(position(t,j,0)+position(t,j,1));
				d = std::sqrt(dot(posi-posj,posi-posj));
				if( d<= r*rFactor ) {
					//Add neighbor
					neighbor_[t][i].push_back(j);
					neighbor_[t][j].push_back(i);
				}
      }
	}
}*/

void BuddingYeast::plotGL(size_t cellIndex, size_t time)
{
	double factor = 2/(scale()+2*maxR());
	Vector3 position1 = ( position(time,cellIndex,0)-COM() )*factor;
	Vector3 position2 = ( position(time,cellIndex,1)-COM() )*factor;
	double radius1 = ( radius(time, cellIndex,0) )*factor;
	double radius2 = ( radius(time, cellIndex,1) )*factor;
	Vector3 diff= position2-position1;
	glTranslatef(position1(0),position1(1),position1(2));
	gluSphere(quad_,radius1,32,32);
	glTranslatef(diff(0),diff(1),diff(2));
	gluSphere(quad_,radius2,32,32);
}



void BuddingYeast::plotPS(std::ofstream &OUT,size_t t,size_t cellIndex,
													float r, float g, float b, int move, double scaleFact)
{
  Vector3 min(minX(),minY(),minZ()), pos;
  double X1,Y1,R1,X2,Y2,R2;
	pos = ( (position(t,cellIndex,0)- min)+1.2*maxR() )*scaleFact;
	X1 = move + pos.x();
	Y1 = move + pos.y();
	R1 = radius(t,cellIndex,0)*scaleFact;
	pos = scaleFact* 
		( (position(t,cellIndex,1)- min)+1.2*maxR());
	X2 = move + pos.x();
  Y2 = move + pos.y();
  R2 = radius(t,cellIndex,1)*scaleFact;
	OUT << "0 0 0 setrgbcolor\n\n";
	OUT << X1 << " " 
			<< Y1 << " "
			<< R1 << " 0 360 arc stroke\n";
  OUT << X2 << " " 
			<< Y2 << " "
			<< R2 << " 0 360 arc stroke" <<"\n"; 
	OUT << r << " " << g << " " << b << " " << "setrgbcolor\n\n";
	OUT << X1 << " " 
			<< Y1 << " "
			<< R1 << " 0 360 arc fill\n";
  OUT << X2 << " " 
			<< Y2 << " "
			<< R2 << " 0 360 arc fill" <<"\n"; 
}


