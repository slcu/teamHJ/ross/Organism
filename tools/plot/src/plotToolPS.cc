#include "plotToolPS.h"

PlotToolPS::PlotToolPS(BaseObject *obj,const std::string &file, size_t cc, size_t cs, int size,double gLevel) 
	: BasePlotTool(obj,file,cc,cs,size, gLevel)
{
	move_=50;
}

void PlotToolPS::plot()
{
	
  double xSize = 2*(object_->maxX()-object_->minX()+2.4*object_->maxR())*scale();
  double ySize = (object_->maxY()-object_->minY()+2.4*object_->maxR())*scale();
  std::string file = outputFile()+".ps";
  std::ofstream OUT;
  OUT.open(file.c_str());
  if (!OUT) {
    std::cerr << "\nError: Unable to open file " 
	      << file << " for output."  << std::endl;
    exit(EXIT_FAILURE);
  }
  //Print heading
//////////////////////////////////////////////////////////////////////
  OUT << "%!PS-Adobe-2.0\n";
  OUT << "%%DoucumentFonts:\n";
  OUT << "%%Title:\n";
  OUT << "%%Creator: Newman\n";
  OUT << "%%CreationDate:\n";
  OUT << "%%Pages: " << object_->TMax() << " (atend)\n";
  OUT << "%%BoundingBox: " << move() << " " << move() << " " 
	    << int(xSize)+move()<< " " << int(ySize)+move()<< "\n";
  OUT << "%%EndComments\n";
  
  OUT << "/color_res 0.03 def\n/gradient false def\n/border 2.4 def\n\n";
  
  OUT << "/neibclip {\n"
	    << "13 dict begin\n"
	    << "/r exch def\n"
	    << "/y exch def\n"
	    << "/x exch def\n"
	    << "/nr exch def\n"
	    << "/ny exch def\n"
	    << "/nx exch def\n\n"
	    << "/dx nx x sub def\n"
	    << "/dy ny y sub def\n"
	    << "/d dx dup mul dy dup mul add sqrt def\n"
	    << "nr r add d gt {\n"
	    << "nr r sub abs d gt {\n"
	    << "nr r gt {clip} if\n"
	    << "}{\n"
	    << "/f r dup mul nr dup mul sub d dup mul add\n"
	    << "d dup mul 2 mul div\n"
      << "border 0.0 mul d div sub\n" // PSA
	    << "def\n"
	    << "/s 2 r mul d div def\n"
	    << "/sx dx s mul def\n"
	    << "/sy dy s mul def\n"
	    << "x dx f mul add sy sub y dy f mul add sx add moveto\n"
	    << "sx -2 mul sy -2 mul rlineto\n"
	    << "sy 2 mul sx -2 mul rlineto\n"
	    << "sx 2 mul sy 2 mul rlineto\n"
	    << "clip newpath\n"
	    << "} ifelse\n"
	    << "} if\n"
	    << "end\n"
	    << "} bind def\n\n";
  
  OUT << "/drawblob {\n"
			<< "7 dict begin\n"
			<< "/R exch def\n"
	    << "/G exch def\n"
	    << "/B exch def\n"
	    << "/r exch def\n"
	    << "/y exch def\n"
	    << "/x exch def\n"
	    << "gradient {\n"
	    << "0. color_res dup 1. exch 0.5 mul sub {\n"
	    << "/f exch def\n"
	    << "R f mul G f mul B f mul setrgbcolor\n"
	    << "x y r 1. f dup mul sub sqrt mul 0 360 arc fill\n"
	    << "} for\n"
	    << "}{\n"
	    << "R G B setrgbcolor\n"
			<< "x y r 0 360 arc fill\n"
			<< "} ifelse\n"
	    << "end\n"
	    << "} bind def\n\n";
  
  OUT << "/blob {\n"
	    << "6 dict begin\n"
	    << "/R exch def\n"
	    << "/G exch def\n"
	    << "/B exch def\n"
	    << "/r exch def\n"
	    << "/y exch def\n"
	    << "/x exch def\n"
	    << "gsave\n"
	    << "counttomark 3 idiv {x y r neibclip} repeat\n"
	    << "pop\n"
	    << "x y r R G B drawblob\n"
	    << "grestore\n"
	    << "end\n"
	    << "} bind def\n\n";
  
	OUT << "/makepolygon {\n"
			<< "3 dict begin\n"
			<< "/a exch def\n"
			<< "/n a length def\n"
			<< "n 1 gt {\n"
			<< "a 0 get 0 get\n"
			<< "a 0 get 1 get\n"
			<< "moveto\n"
			<< "1 1 n 1 sub {\n"
			<< "/i exch def\n"
			<< "a i get 0 get\n"
			<< "a i get 1 get\n"
			<< "lineto\n"
			<< "} for\n"
			<< "} if\n"
			<< "end\n"
			<< "} def\n\n";

	OUT << "/ellipse {\n"
			<< "6 dict begin\n"    
			<< "/endangle exch def\n"
			<< "/startangle exch def\n"
			<< "/yrad exch def\n"
			<< "/xrad exch def\n"
			<< "/y exch def\n"
			<< "/x exch def\n"
			<< "/savematrix matrix currentmatrix def\n"
			<< "x y translate\n"
			<< "xrad yrad scale\n"
			<< "0 0 1 startangle endangle arc\n"
			<< "savematrix setmatrix\n"
			<< "end\n"
			<< "} def\n\n";

	OUT << "/draw-hexagon { 2 dict begin" << std::endl
	    << "/angle exch def" << std::endl
	    << "/r exch def" << std::endl
	    << "angle cos r mul angle sin r mul moveto" << std::endl
	    << "angle 60 300 angle add {" << std::endl
	    << "/i exch def" << std::endl
	    << "i cos r mul i sin r mul lineto" << std::endl
	    << "} for" << std::endl
	    << "closepath" << std::endl
	    << "end } def" << std::endl
	    << std::endl;

	OUT << "/hexagon { 3 dict begin" << std::endl
	    << "/b exch def" << std::endl
	    << "/g exch def" << std::endl
	    << "/r exch def" << std::endl
	    << "/l exch def" << std::endl
	    << "/y exch def" << std::endl
	    << "/x exch def" << std::endl
	    << "gsave" << std::endl
	    << "l 0.2 mul setlinewidth" << std::endl
	    << "1 setlinejoin" << std::endl
	    << "x y translate" << std::endl
	    << "r g b setrgbcolor" << std::endl
	    << "newpath" << std::endl
	    << "2.0 l mul 3.0 sqrt div 30 draw-hexagon" << std::endl
	    << "fill" << std::endl
	    << "0 0 0 setrgbcolor" << std::endl
	    << "newpath" << std::endl
	    << "2.0 l mul 3.0 sqrt div 30 draw-hexagon" << std::endl
	    << "stroke" << std::endl
	    << "grestore" << std::endl
	    << "end } def" << std::endl
	    << std::endl;


  OUT << "%%EndProlog\n";
  //Print every page
  //////////////////////////////////////////////////////////////////////
  OUT << scale()/12.0 << " setlinewidth\n";
  for( size_t page=0 ; page<object_->TMax() ; ++page ) {
    OUT << "%%Page: p" << page << " " << page << "\nsave\n";
    for( size_t j=0 ; j<object_->numCell(page) ; ++j ) {
			setColor( float( object_->variable(page,j,colorColumn()) ) );
			object_->plotPS(OUT,page,j, red(), green(), blue(), move(),scale());
    }
      OUT << "showpage\n restore\n";
  }
  //Print ending
  //////////////////////////////////////////////////////////////////////
  OUT << "%%Trailer\n%%Pages: " << object_->TMax() << "\n";
  OUT.close();
}				
		 
 
