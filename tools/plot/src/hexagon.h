//
// Filename     : sphere.h
// Description  : Class describing sphere-shaped cell colonies
// Author(s)    : Pontus Melke (pontus@thep.lu.se)
// Created      : April 2007
// Revision     : $Id$
//
#ifndef HEXAGON_H
#define HEXAGON_H

#include "baseObject.h"

class Hexagon : public BaseObject {
 public:
	Hexagon(Data *data, size_t dimension,std::string min, std::string max);
	~Hexagon(){}
	
	void plotPS(std::ofstream &OUT, size_t t, size_t cellIndex, 
		    float r, float g, float b, int move, double scaleFact);
 private:
	Hexagon(const Hexagon &);
	
};

#endif // HEXAGON_H
