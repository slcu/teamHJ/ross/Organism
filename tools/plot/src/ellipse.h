//
// Filename     : ellipse.h
// Description  : Class describing elipse shaped cell colonies
// Author(s)    : Pontus Melke (pontus@thep.lu.se)
// Created      : April 2007
// Revision     : $Id:$
//
#ifndef ELLIPSE_H
#define ELLIPSE_H

#include "baseObject.h"
///
///@brief A class describing ellipse-shaped cells
///
/// An ellipse is described by two spatial coordinates defining the foci. In
/// the data object the two spatial coordinates are located in; 2D: columns
/// 0,1 (x,y) and in 2,3 (x,y) respectively, 3D: columns 0,1,2 (x,y,z) and
/// 3,4,5 (x,y,z). The column following the last spatial variable contains the
/// 'rest length' which is attached between the two coordinates.
///
/// Note that Ellipse overrides the radius function, and
/// returns the constant semiminor axis 'b', by default
/// 0.5. This is not partucularly nice, but it is a problem which is
/// inhertited from the implementation in Organism (Cosmo?) and it is
/// something we have to live with.
///
class Ellipse : public BaseObject {
 public:
	///
	/// @brief Main constructor
	///
	/// Main constructor, sets which indices that describes x,y,z
	/// coordinates and calls contructor of base class.
	///
	/// @param b semiminor axis  
  ///
	/// @see BaseObject::BaseObject(Data *data, size_t dimension);
	///
	Ellipse(Data *data, size_t dimension, std::string min, std::string max, double b=0.5);
	
	///
	/// @brief destructor
	///
	~Ellipse(){}
	
	///
	/// @brief returns b
	///
	inline double radius(size_t time, size_t cellIndex, size_t psoVar=0) const {return b_;}
	
	///
	/// @brief Sets the neighbors of each cell at each timepoint.
  ///
	/// @see BaseObject::setNeighbor(double rFactor=1)
	///
	//void setNeighbor(double rFactor=1);
	
	
  ///
	/// @brief Does the actual plotting
	///
	void plotGL(size_t cellIndex, size_t timeIndex);
  
  ///
	/// @brief Does the actual PS plotting
	///
	void plotPS(std::ofstream &OUT,size_t t,size_t cellIndex, 
							float r, float g, float b, int move, double scaleFact);
	
 private:
	Ellipse(const Ellipse &);
	double b_;
};
#endif //ELLIPSE_H
