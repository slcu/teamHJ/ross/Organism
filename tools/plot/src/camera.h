//
// Filename     : myGL.h
// Description  : Classes Camera and CameraSettings
// Author(s)    : Pontus Melke (pontus@thep.lu.se)
// Created      : April 2007
// Revision     : $Id:
//
#ifndef CAMERA_H
#define CAMERA_H
#include<fstream>
#include<sstream>
#include <vector>

#include "src/vector3.h"
#include "src/myFiles.h"
using myVector3::Vector3;
class CameraSettings;

///
/// @brief Camera class used in PlotToolGL
///
/// Camera implements basic functionalities such as zoom, pitch, yaw and
/// move. The camera can contains a vector of CameraSettings which allows the
/// camera movements to be programmed in advance by sendin a file to
/// readCameraSettings().
///
class Camera {
 public:
  ///
	/// @brief Main constructor, puts the camera at @f$X, Y, Z @f$.
	///
	Camera(double X=0, double Y=0, double Z=3.1);
  
	///
	/// @brief Default destructor
	///
	~Camera(){};

  ///
	/// @brief roll camera with a given angle
	///
	void roll(double angle);
  
	///
	/// @brief pitch camera with a given angle
	///
	void pitch(double angle);

	///
	/// @brief yaw camera with a given angle
	///
  void yaw(double angle);

	///
	/// @brief moves camera upwards with a given step length
	///
  inline void moveUp(double f =0.1) {position_+=f*e2_;}
  
	///
	/// @brief moves camera downwards with a given step length
	///
	inline void moveDown(double f =0.1) {position_-=f*e2_;}
  
  ///
	/// @brief moves camera to the left with a given step length
	///
	inline void moveLeft(double f =0.1) {position_-=f*e1_;}

	///
	/// @brief moves camera to the  right with a given step length
	///
  inline void moveRight(double f =0.1) {position_+=f*e1_;}

	///
	/// @brief moves camera towards the object with a given step length
	///
  inline void zoomIn(double f =0.1) {position_-=f*e3_;}
	
  ///
	/// @brief moves camera away from the object with a given step length
	///
  inline void zoomOut(double f =0.1) {position_+=f*e3_;}

	///
	/// @brief rotates the camera around x-axis with a given angle
	///
  inline void rotateX(double angle) {angleX_ = (angle+angleX_ <360) ? (angle+angleX_) : (angle+angleX_)-360 ;}
  
	///
	/// @brief rotates the camera around y-axis with a given angle
	///
	inline void rotateY(double angle) {angleY_ = (angle+angleY_ <360) ? (angle+angleY_) : (angle+angleY_)-360 ;}
  
  ///
	/// @brief rotates the camera around z-axis with a given angle
	///
	inline void rotateZ(double angle) {angleZ_ = (angle+angleZ_ <360) ? (angle+angleZ_) : (angle+angleZ_)-360 ;}
  
	///
	/// @brief Reads camera settings from a file
	///
	/// Defines one or several sequences where each sequence is a 'program' for
	/// the camera movement. Each sequence must begin with action and end
	/// with cut. Availlable options are: 
	///
	/// <ul>
	///
	/// <li> <tt>startTime @f$t_{init}@f$</tt> - plotting will start at
	/// @f$t_{init}@f$, by default @f$t_{init}=0@f$.
	///
	/// <li> <tt>duration @f$N_{steps}@f$</tt> - The sequence will proceed in @f$N_{steps}@f$ time steps,
	/// @f$N_{steps}=1@f$ by default.
	///
	/// <li> <tt>speed @f$s@f$</tt> - @f$s =1@f$ by default, @f$ s = 0 @f$ is equivalent to pausing
	/// the animation.
	///
	/// <li> <tt>rotateX @f$\alpha_{init}@f$ @f$\alpha_{final}@f$</tt> - Rotate camera around the x-axis
	/// from @f$\alpha_{init}@f$ to @f$\alpha_{final}@f$ in the specified timespan.
	///
	/// <li> <tt>rotateY @f$\alpha_{init}@f$ @f$\alpha_{final}@f$</tt> - Rotate camera around the y-axis
	/// from @f$\alpha_{init}@f$ to @f$\alpha_{final}@f$ in the specified timespan.
	///
	/// <li> <tt>rotateZ @f$\alpha_{init}@f$ @f$\alpha_{final}@f$</tt> - Rotate
	/// camera around the z-axis from @f$\alpha_{init}@f$ to
	/// @f$\alpha_{final}@f$ in the specified timespan.  
	///
	/// <li> <tt>position @f$x_0@f$ @f$y_0@f$ @f$z_0@f$ @f$x_1@f$ @f$y_1@f$
	/// @f$z_1@f$</tt> - Moves the camera from @f$(x_0,y_0,z_0)@f$ to
	/// @f$(x_1,y_1,z_1)@f$ in the specified timespan.
	///
	/// </ul>
	///
	/// Example: The following code will generate a plot where during the first
	/// 50 images the camera is roatated 50 degrees clockwise around the
	/// x-axis. The animation is then paused while the camera rotates around the
	/// z-axis from 0 to 360 degrees in 100 timesteps.
	///
	/// @verbatim
	/// action
	/// duration 50
	/// rotateX  0 -50 
	/// cut
	/// action
	/// speed 0
	/// rotateZ 0 -360
	/// duration 200
	/// cut
	/// @endverbatim
	///
	void readCameraSettings(const std::string &file);
	
	///
	/// @brief update camera according to what was specified in
	/// readCameraSettings()
	///
	bool updateCamera();
  
	///
	/// @brief Reset camera to initial settings in the current sequence
	///
	void resetCamera();
  
	///
	/// @brief Increment the sequence number by one (mod number of sequences)
	///
	inline void incrementSequence() {sequence_= ((sequence_+1) < settings_.size()) ? sequence_+ 1 : 0;}
  
  ///
	/// @brief Sets the camera position
	///
  inline void setPosition(Vector3 v) {position_=v;}
	
	///
	/// @brief Sets the speed of the camera.
	///
	/// Speed here is simply the number of timesteps taken at each iteration.
	///
	inline void setSpeed(size_t s) {speed_=s;}
	
  ///
	/// @brief returns the current angle around the x-axis.
	///
  inline double angleX() const {return angleX_;}
  
	///
	/// @brief returns the current angle around the y-axis.
	///
	inline double angleY() const {return angleY_;}
	
	///
	/// @brief returns the current angle around the z-axis.
	///
  inline double angleZ() const {return angleZ_;}

	///
	/// @brief returns the current sequence-index.
	///
  inline size_t sequence() const {return sequence_;}
  
	///
	/// @brief returns the current camera position.
	///
  inline Vector3 position() const {return position_;}

	///
	/// @brief returns the current camera x-axis.
	///
  inline Vector3 e1() const {return e1_;}
  
	///
	/// @brief returns the current camera y-axis.
	///
	inline Vector3 e2() const {return e2_;}

	///
	/// @brief returns the current camera z-axis.
	///
  inline Vector3 e3() const {return e3_;}

	///
	/// @brief returns the total duration of the sequences define in
	/// readCamerasettings().
	///
  double TMax() const;
	
	///
	/// @brief returns the current camera speed.
	///
	inline unsigned int speed() const {return speed_;}
	
	///
	/// @brief returns the start time as defined in readCameraSettings.
	///
	inline size_t startTime() const {return startTime_;}
	
	///
	/// @brief returns true if settings has been read from file, otherwise false.
	///
	inline bool settings() const {return settings_.size();}
 private:
  Camera(const Camera &);
  std::vector<CameraSettings> settings_;
  Vector3 position_, e1_, e2_, e3_;
  double angleX_,angleY_,angleZ_;
  size_t sequence_,startTime_;
	unsigned int speed_;
}; 

///
/// @brief Settings class which is used for programming the camera
///
class CameraSettings {
 public:

	///
	/// @brief Main constructor
	///
	/// @param numStep sets how many time steps used in the sequence
	///
	CameraSettings(size_t numStep=1);
  
	///
	/// @brief returns increment in position
	///
	Vector3 positionIncrement() const {return (finalPosition_-initialPosition_)*(1/double(numStep_));}
	
	///
	/// @brief returns initial position
	///
	Vector3 initialPosition() const {return initialPosition_;}
	
	///
	/// @brief returns final position
	///
	Vector3 finalPosition() const {return finalPosition_;;}

	///
	/// @brief returns increment in angle around the x-axis
	///
  double angleXIncrement() const {return (angleXFinal_-angleXInit_)*(1/double(numStep_));}
  
	
	///
	/// @brief returns increment in angle around the y-axis
	///
	double angleYIncrement() const {return (angleYFinal_-angleYInit_)*(1/double(numStep_));}

	
	///
	/// @brief returns increment in angle around the z-axis
	///
  double angleZIncrement() const {return (angleZFinal_-angleZInit_)*(1/double(numStep_));}
  
	///
	/// @brief returns initial value of angle around the x-axis
	///
	double angleXInit() const {return angleXInit_;}
	

	///
	/// @brief returns initial value of angle around the y-axis
	///
	double angleYInit() const {return angleYInit_;}

	
	///
	/// @brief returns initial value of angle around the z-axis
	///
	double angleZInit() const {return angleZInit_;}
	
	
	///
	/// @brief returns final value of angle around the x-axis
	///
	double angleXFinal() const {return angleXFinal_;}
	
	
	///
	/// @brief returns final value of angle around the y-axis
	///
	double angleYFinal() const {return angleYFinal_;}
	
	
	///
	/// @brief returns final value of angle around the z-axis
	///
	double angleZFinal() const {return angleZFinal_;}


	///
	/// @brief returns camera speed
	///
	unsigned int speed() const {return speed_;}
	
	
	///
	/// @brief returns number of time steps
	///
	size_t numStep() const {return numStep_;}

	
	///
	/// @brief returns initial time point
	///
	size_t startTime() const {return startTime_;}

	
	///
	/// @brief sets initial position
	///
  void setInitialPosition (Vector3 init) {initialPosition_=init;}
  
  ///
	/// @brief sets final position
	///
	void setFinalPosition(Vector3 final) {finalPosition_=final;}

	///
	/// @brief sets number of time stpes
	///
  void setNumStep(size_t n) {numStep_=n;}
	
	///
	/// @brief sets initial value of the angle around the x-axis
	///
  void setAngleXInit(double a) {angleXInit_=a;}

	///
	/// @brief sets final value for of angle around the x-axis
	///
  void setAngleXFinal(double a) {angleXFinal_=a;}
  
	///
	/// @brief sets initial value of the angle around the y-axis	
  ///
	void setAngleYInit(double a) {angleYInit_=a;}
  
  ///
	/// @brief sets final value of the angle around the y-axis
	///
	void setAngleYFinal(double a) {angleYFinal_=a;}

	///
	/// @brief sets initial value of the angle around the z-axis
	///
  void setAngleZInit(double a) {angleZInit_=a;}

	///
	/// @brief sets final value for the angle around the z-axis
	///
  void setAngleZFinal(double a) {angleZFinal_=a;}

	///
	/// @brief sets camera speed
	///
	void setSpeed(size_t s) {speed_=s;}

	///
	/// @brief sets starting time
	///
	void setStartTime(size_t s) {startTime_=s;}
 private:
  Vector3 initialPosition_, finalPosition_;
  double angleXInit_,angleXFinal_,
    angleYInit_,angleYFinal_,
    angleZInit_,angleZFinal_;
  size_t numStep_,startTime_;
	unsigned int  speed_;
};
#endif //CAMERA_H
