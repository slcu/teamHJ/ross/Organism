//
// Filename     : sphere.h
// Description  : Class describing sphere-shaped cell colonies
// Author(s)    : Pontus Melke (pontus@thep.lu.se)
// Created      : April 2007
// Revision     : $Id:$
//
#ifndef SPHERE_H
#define SPHERE_H

#include "../../../src/common/baseObject.h"
///
///@brief A class describing sphere-shaped cells
///
/// A sphere is described by one spatial varible defining the center
/// of the sphere. In the data object the coordinates variables are
/// located in columns 0,1(2), x,y, (z). The column following the
/// last spatial variable contains the radius of the sphere.
///
class Sphere : public BaseObject {
 public:
	///
	/// @brief Main constructor
	///
	/// Main constructor, sets which indices that describes x,y,z
	/// coordinates and calls contructor of base class.
	///
	/// @see BaseObject::BaseObject(Data *data, size_t dimension);
	///
	Sphere(Data *data, size_t dimension, std::string min, std::string max);
	
	///
	/// @brief destructor
	///
	~Sphere(){}
	
	///
	/// @brief Sets the neighbors of each cell at each timepoint.
  ///
	/// @see BaseObject::setNeighbor(double rFactor=1)
	///
	void setNeighbor(double rFactor=1.0);

  ///
	/// @brief Does the actual GL plotting
	///
	void plotGL(size_t cellIndex, size_t timeIndex);

  ///
	/// @brief Does the actual PS plotting
	///
	void plotPS(std::ofstream &OUT,size_t t,size_t cellIndex, 
							float r, float g, float b, int move, double scaleFact);


 private:
	Sphere(const Sphere &);
	
};
#endif // SPHERE_H
