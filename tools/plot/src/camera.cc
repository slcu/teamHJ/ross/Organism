#include <cstdlib>
#include "camera.h"
#include <cstdlib>
const double Pi = 3.141592653589793238512808959406186204433;

Camera::Camera(double X, double Y, double Z)
{
  sequence_= 0;
	startTime_=0;
	angleX_=angleY_=angleZ_=0.0;
  speed_=1;
	e1_.setVector3(1,0,0);
  e2_.setVector3(0,1,0);
  e3_.setVector3(0,0,1);
  position_.setVector3(X,Y,Z);
}

void Camera::roll(double angle)
{
  angle *= Pi/180.0;
  Vector3 tmp = e1_;
  e1_ = std::cos(angle)*tmp - std::sin(angle)*e2_;
  e2_ = std::sin(angle)*tmp + std::cos(angle)*e2_;
}
void Camera::yaw(double angle)
{
  angle *= Pi/180.0;
  Vector3 tmp = e1_;
  e1_ = std::cos(angle)*tmp - std::sin(angle)*e3_;
  e3_ = std::sin(angle)*tmp + std::cos(angle)*e3_;
}
void Camera::pitch(double angle)
{
  angle *= Pi/180.0;
  Vector3 tmp = e2_;
  e2_ = std::cos(angle)*tmp - std::sin(angle)*e3_;
  e3_ = std::sin(angle)*tmp + std::cos(angle)*e3_;
}

bool Camera::updateCamera()
{
	static size_t time = 0; 
  if (settings()) {
    position_ += settings_[sequence()].positionIncrement();
    angleX_ += settings_[sequence()].angleXIncrement();
    angleY_ += settings_[sequence()].angleYIncrement();
    angleZ_ += settings_[sequence()].angleZIncrement();
    speed_   = settings_[sequence()].speed();
		//startTime_= settings_[sequence()].startTime();		
		time++;
		if (time>=settings_[sequence()].numStep()) {
			incrementSequence();
			resetCamera();
			time =0;
    }
    return true;
  }
  return false;
}

void Camera::resetCamera()
{
  if (settings_.size()) {
    position_=settings_[sequence()].initialPosition();
		speed_=settings_[sequence()].speed();
		startTime_=settings_[sequence()].startTime();
		angleX_=settings_[sequence()].angleXInit();
		angleY_=settings_[sequence()].angleYInit();
		angleZ_=settings_[sequence()].angleZInit();
	}
}

void Camera::readCameraSettings(const std::string &file) 
{  
  std::istream *IN = myFiles::openFile(file);
  settings_.resize(0);
  size_t count =0;
	if (IN != NULL) {
    while (!IN->eof()) {
      // Read line from file.
      std::string input;
      getline(*IN, input);
      
      // Return if EOF is reached.
      if (IN->eof())
				break;
      
      // Make temporary stringstream of line read from file.
      std::stringstream LINE;
      LINE.str(input);
      
      // Read action name.
      std::string action;
      LINE >> action;
      if (action.empty())
				continue;
 
      if (action == "action") {
				CameraSettings cs;
				//set all values to where previous scene ended
				if (count) {
					cs.setInitialPosition(settings_[count-1].finalPosition());
					cs.setFinalPosition(settings_[count-1].finalPosition());
					cs.setAngleXInit(settings_[count-1].angleXFinal());
					cs.setAngleXFinal(settings_[count-1].angleXFinal());
					cs.setAngleYInit(settings_[count-1].angleYFinal());
					cs.setAngleYFinal(settings_[count-1].angleYFinal());
					cs.setAngleZInit(settings_[count-1].angleZFinal());
					cs.setAngleZFinal(settings_[count-1].angleZFinal());
				}
				count++;
				std::vector<std::string> stream;
				while (input!="cut") {	
					getline(*IN, input);
					if (input!="cut")
						stream.push_back(input);
					if (IN->eof()) {
						std::cerr << "cut was not found\n";
						std::exit(EXIT_FAILURE);
					}
				}
				for (size_t i=0; i< stream.size(); ++i) {
					std::stringstream LINE; 
					LINE.str(stream[i]);
					std::string option;
					LINE >> option;
					if (option.empty())
						continue;
					if (option == "position") {
						std::vector <Vector3> pos(2);
						for(size_t n=0; n<2; ++n) {
							for ( size_t i=0; i<3; ++i) {
								std::string tmpValue;
								LINE >> tmpValue;
								if (tmpValue.empty()) {
									std::cerr << "Error: Option position takes 6 arguments, x1,y1,z1, x2,y2,z2."<< std::endl;
									std::exit(EXIT_FAILURE); 
								}
								pos[n].setVector3(i, atof(tmpValue.c_str())); 
							}			 
						}
						cs.setInitialPosition(pos[0]);
						cs.setFinalPosition(pos[1]);
					}  
					else if (option == "startTime") {
						std::string tmpValue;
						LINE >> tmpValue;
						if (tmpValue.empty()) {
							std::cerr << "Error: Option startTime takes one argument, starting time"<< std::endl;
							std::exit(EXIT_FAILURE); 
						}
						cs.setStartTime(atoi(tmpValue.c_str()));
					}
					else if (option == "duration") {
						std::string tmpValue;
						LINE >> tmpValue;
						if (tmpValue.empty()) {
							std::cerr << "Error: Option time takes one argument, number of time steps"
												<< std::endl;
							std::exit(EXIT_FAILURE); 
						}
						cs.setNumStep(atoi(tmpValue.c_str()));
					}
					else if (option == "speed") {
						std::string tmpValue;
						LINE >> tmpValue;
						if (tmpValue.empty()) {
							std::cerr << "Error: Option speed takes one argument, speed"<< std::endl;
							std::exit(EXIT_FAILURE); 
						}
						cs.setSpeed(atoi(tmpValue.c_str()));
					} 
					else if (option == "rotateX") {
						std::vector <double> angle(2);
						for ( size_t i=0; i<2; ++i) {
							std::string tmpValue;
							LINE >> tmpValue;
							if (tmpValue.empty()) {
								std::cerr << "Error: Option rotateX takes two arguments, angleInit and angleFinal"
													<< std::endl;
								std::exit(EXIT_FAILURE); 
							}
							angle[i] = atof(tmpValue.c_str());
						}
						cs.setAngleXInit(angle[0]);
						cs.setAngleXFinal(angle[1]);
					}
					else if (option == "rotateY") {
						std::vector <double> angle(2);
						for ( size_t i=0; i<2; ++i) {
							std::string tmpValue;
							LINE >> tmpValue;
							if (tmpValue.empty()) {
								std::cerr << "Error: Option rotateY takes two arguments, angleInit and angleFinal"
													<< std::endl;
								std::exit(EXIT_FAILURE); 
							}
							angle[i] = atof(tmpValue.c_str());
						}
						cs.setAngleYInit(angle[0]);
						cs.setAngleYFinal(angle[1]);
					}
					else if (option == "rotateZ") {
						std::vector <double> angle(2);
						for ( size_t i=0; i<2; ++i) {
							std::string tmpValue;
							LINE >> tmpValue;
							if (tmpValue.empty()) {
								std::cerr << "Error: Option rotateZ takes two arguments, angleInit and angleFinal"
													<< std::endl;
								std::exit(EXIT_FAILURE); 
							}
							angle[i] = atof(tmpValue.c_str());
						}
						cs.setAngleZInit(angle[0]);
						cs.setAngleZFinal(angle[1]);
					} 
					else {
						std::cerr << "readCameraSettings(): " << option << " is not a valid option\n";
						std::exit(EXIT_FAILURE);
					}
				}	
				settings_.push_back(cs);
      }
      else {
				std::cerr << file << " does not contain \'action\'. " 
									<< "Camera settings cannot be read\n";
				std::exit(EXIT_FAILURE);
      }
    }
  }
  resetCamera();
  delete IN;
}

double Camera::TMax() const 
{
  double tmp=0;
  for (size_t t=0; t<settings_.size(); ++t)
    tmp+= settings_[t].numStep();
  return tmp;
} 

CameraSettings::CameraSettings(size_t ns) : numStep_(ns) 
{
  angleXInit_=angleYInit_=angleZInit_=
    angleXFinal_=angleYFinal_=angleZFinal_=0;
	speed_=1;
	startTime_=0;
	initialPosition_.setVector3(0,0,3);
	finalPosition_.setVector3(0,0,3);
}        


