//
// Filename     : plotToolPS.h
// Description  : Base class for postscript plotting
// Author(s)    : Pontus Melke (pontus@thep.lu.se)
// Created      : April 2007
// Revision     : $Id:
//
#ifndef PLOTTOOLPS_H
#define PLOTTOOLPS_H
#include<fstream>
#include<sstream>

#include "basePlotTool.h"
#include "src/vector3.h"
#include "src/myConfig.h"
using myVector3::Vector3;

///
/// @brief A base class for plot tools using postscript
///
/// The PlotToolPS class defines an interface for plot tools. It does
/// everything except the actual plotting of the objects, this is done in
/// plotObject() which must be implemented in the derived classes.
///
class PlotToolPS : public BasePlotTool {
 public:
  ///
	/// @brief Main constructor, basically just calls the constructor in the
	/// base Class.
	///
	/// @see BasePlotTool::BasePlotTool(BaseObject *obj,const std::string &file,size_t cc, size_t cs, int size);
	///
	PlotToolPS(BaseObject *obj, const std::string &file, size_t cc, size_t cs, int size, double gLevel);
  
	///
	/// @brief Main plot function, contains the prolog to the postscript images
	///
	/// @see BasePlotTool::plot()
	void plot();

	///
	/// @brief returns margin factor.
	///
	int move() const {return move_;}

	///
	/// @brief returns a normalizing factor, used for adjusting the plot to
	/// window
	///
	double scale() const {return (imageSize_+2*object_->maxR())/(object_->scale());}
	
 
 protected:
  PlotToolPS(const PlotToolPS &){}
  int move_;
};
#endif //PLOTTOOLPS_H
