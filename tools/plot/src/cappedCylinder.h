//
// Filename     : cappedCylinder.h
// Description  : Class describing capped cylinder shaped cell colonies
// Author(s)    : Pontus Melke (pontus@thep.lu.se)
// Created      : April 2007
// Revision     : $Id:$
//
#ifndef CAPPEDCYLINDER_H
#define CAPPEDCYLINDER_H

#include "baseObject.h"
///
///@brief A class describing capped cylinder, or E-coli, shaped cells
///
/// A capped cylinder is described by two spatial coordinates defining
/// the centers of the two spheres. In the data object the two spatial
/// coordinates are located in; 2D: columns 0,1 (x,y) and in 2,3 (x,y)
/// respectively, 3D: columns 0,1,2 (x,y,z) and 3,4,5 (x,y,z). The
/// column following the last spatial variable CONTAINS the 'rest length'
/// which is attached between the two coordinates.
///
/// Note that CappedCylinder overrides the radius function, and
/// returns the constant radius of the two spheres, by default
/// 0.5. This is not partucularly nice, but it is a problem which is
/// inhertited from the implementation in Organism (Cosmo?) and it is
/// something we have to live with.
///
class CappedCylinder : public BaseObject {
public:
	///
	/// @brief Main constructor
	///
	/// Main constructor, sets which indices that describes x,y,z
	/// coordinates and calls contructor of base class.
	///
	/// @param b radius of the two spheres  
	///
	/// @see BaseObject::BaseObject(Data *data, size_t dimension);
	///
	CappedCylinder(Data *data, size_t dimension, std::string min, std::string max, double b=0.5);
	
	///
	/// @brief destructor
	///
	~CappedCylinder(){}
	
	
	///
	/// @brief Sets the neighbors of each cell at each timepoint.
	///
	/// @see BaseObject::setNeighbor(double rFactor=1)
	///
	//void setNeighbor(double rFactor=1);
	
	///
	/// @brief Does the actual plotting
	///
	void plotGL(size_t cellIndex, size_t timeIndex);
 
	///
	/// @brief Does the actual PS plotting
	///
	void plotPS(std::ofstream &OUT,size_t t,size_t cellIndex, 
		float r, float g, float b, int move, double scaleFact);
private:
	CappedCylinder(const CappedCylinder &);
	double b_;
};
#endif //CAPPEDCYLINDER_H
