//
// Filename     : basePlotTool.cc
// Description  : Base class for plot tools
// Author(s)    : Pontus Melke (pontus@thep.lu.se)
// Created      : April 2007
// Revision     : $Id:
//

#include "plotToolPS.h"
#include "plotToolGL.h"

BasePlotTool::BasePlotTool(BaseObject *obj, std::string file,size_t cc, size_t cs,int size, 
			   double gLevel)
{
  object_=obj;
  colorSchema_=cs;
  colorColumn_=cc;
  imageSize_ = size;
  if(!file.empty()) {
    outputFile_= file;
  }
  //set dafault values for the color-variables
  gLevel_=gLevel;
  bgLevel_=0.001;
  red_=green_=blue_=1.0;
  light_=true;
}

void BasePlotTool::setColor(float value) 
{ 
  switch (colorSchema()) {
    
  case 1://bw color schema
    {
      red_ = green_ = blue_ = gLevel_ + (1.0f-gLevel_)*value;
      break;
    }
  case 2://green colorschema
    {
      red_ = blue_ = 0.0f; 
      green_ = gLevel_+0.2f + (0.8f-gLevel_)*value;
      break;
    }
  case 3://black-blue-red-yellow
    {
      float frac=1.0f/3.0f;
      if( value<frac ) {//black/>blue
	red_ = green_ = gLevel_;
	blue_ = gLevel_ + (1.0f-gLevel_)*value/frac;
      }
      else if( value<2*frac ) {//blue->red
	green_ = gLevel_;
	blue_ = 2.0-value/frac;
	red_ = gLevel_ + (1.0f-gLevel_)*(value/frac-1.0);
      }
      else {//red->yellow
	green_ = gLevel_ + (1.0f-gLevel_)*(value/frac-2.0);
	blue_ = 0.0f;
	red_ = 1.0f;
      }
      break;
    }
    
  default: //matlab color? black to blue to green to yellow to red
    {
      float frac1 = 0.05;
      float frac = (1.0f-2.0f*frac1)/3.0f;
      if( value<frac1 ) {
	red_ = green_ = 0.0f;
	blue_ = 0.5f*(1.0f+value/frac1);
      }
      else if( value>=(1.0f-frac1) ) {
	green_ = blue_ = 0.0f;
	red_ = 0.5f*( 1.0f+(1.0f-value)/frac1 );
      }
      else if( value>=frac1 && value<(frac+frac1)) {
	red_ = 0.0f;
	green_ = (value-frac1)/(frac);
	blue_ = 1.0f-green_;
      }
      else if( value>=(frac+frac1) && value<(frac1+2.0f*frac) ) {
	blue_ = 0.0f;
	green_ = 1.0f;
	red_ = (value-(frac+frac1))/frac;
      }
      else if( value>=(frac1+2.0f*frac) && value<(1.0f-frac1) ) {
	blue_ = 0.0;
	green_ = (frac1+value-1)/(2.0f*frac1-frac);
	//g = ((frac1+2.0f*frac)-value)/frac;
	red_ = 1.0f;
      }
    }	
  }
}
BasePlotTool *plotFactory(std::string method, BaseObject *obj, 
			  std::string file, size_t cc, size_t cs, int size, double gLevel, Camera *c)
{
  if (method == "GL" || method == "tiff") {
    /*if (shape == "cappedCylinder")
      return new PlotCappedCylinderGL(obj, file,cc, cs, size, c);
      else if (shape == "sphere")
      return new PlotSphereGL(obj, file,cc, cs,size, c);
      else if (shape == "ellipse")
      return new PlotEllipseGL(obj, file,cc, cs,size, c);
      else if (shape == "buddingYeast")
      return new PlotBuddingYeastGL(obj, file,cc, cs,size, c);
      else if (shape == "cylinder")
      return new PlotCylinderGL(obj, file,cc, cs,size, c);*/
    return new PlotToolGL(obj, file,cc, cs, size, gLevel, c);
    
  }
  else if (method == "ps")
    /*if (shape == "sphere")
      return new PlotSpherePS(obj, file,cc, cs, size);
      else if (shape == "cappedCylinder")
      return new PlotCappedCylinderPS(obj, file,cc, cs, size);
      else if (shape == "ellipse")
      return new PlotEllipsePS(obj, file,cc, cs,size);
      else if (shape == "buddingYeast")
      return new PlotBuddingYeastPS(obj, file,cc, cs,size);
      else {
      std::cerr << shape << " cannot be plotted in " << method << "format\n";
      exit(EXIT_SUCCESS);
      }
    */
    return new PlotToolPS(obj, file,cc, cs, size,gLevel);
  else {
    std::cerr << method << "  is not a supported\n";
    exit(-1);
  }
} 
