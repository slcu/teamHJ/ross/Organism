//
// Filename     : plotToolGL.h
// Description  : Base class for openGL plotting
// Author(s)    : Pontus Melke (pontus@thep.lu.se)
// Created      : April 2007
// Revision     : $Id:
//
#ifndef PLOTTOOLGL_H
#define PLOTTOOLGL_H

//#include <GL/gl.h>
//#include <GL/glu.h>
//#include <GL/glut.h>
#include <OpenGL/gl.h>
#include <OpenGL/glu.h>
#include <GLUT/glut.h>
#include<tiffio.h>
#include <cmath>
#include <iomanip>

#include "camera.h"
#include "basePlotTool.h"
#include "src/vector3.h"
#include "src/myConfig.h"
#include "src/myFiles.h"

using myVector3::Vector3;

///
/// @brief A base class for plot tools using openGL
///
/// The PlotToolGL class defines an interface for plot tools. It does
/// everything except plotting the actual objects, i.e. setting up a Camera
/// wich can zoom, move, pitch, yaw, roll etc. Also sets up lightning and
/// enables keyboard control. 
///
/// The derived classes must implement the function plotObject().
///
class PlotToolGL : public BasePlotTool {
 public:
  ///
  /// @brief Main constructor
  ///
  /// Main constructor, sets up all availlable openGL facilities
  ///
  /// @param *cam pointer to a Camera, used for zooming, rotating etc.
  ///
  /// @see BasePlotTool::BasePlotTool(BaseObject *obj,const std::string &file,size_t cc, size_t cs, int size);
  ///
  PlotToolGL(BaseObject *obj, const std::string &file, size_t cc, size_t cs, int size, double gLevel, Camera *cam);
  
  ///
  /// @brief virtual destructor, enables polymorphism 
  ///
  virtual ~PlotToolGL(){}
  
  ///
  /// @brief Main plot function, contains the glutMainLoop which iterates renderScene()
  ///
  /// @see BasePlotTool::plot()
  ///
  void plot();
  
  ///
  /// @brief Plots the coordinate axis
  ///
  void plotAxis();
  
  ///
  /// @brief Plots the cell indices
  ///
  void plotIndex(size_t index);
  
  
  ///
  /// @brief Plots a value to the screen
  ///
  template<typename T>
    void plotValue(const T value,GLfloat x,GLfloat y,GLfloat z);
  
  ///
  /// @brief Used in the main loop, calls plotObject for tha actual plotting
  /// of the objects
  ///
  void renderScene(void);
  
  ///
  /// @brief enables proper resizing of the window
  ///
  void changeSize(int w, int h);
  
  ///
  /// @brief sets the camera in position
  ///
  void setLook();
  
  ///
  /// @brief Enables keyboard input
  ///
  void processSpecialKeys(int key, int x, int y);
	
  ///
  /// @brief Enables keyboard input
  ///
  void processNormalKeys(unsigned char key, int x, int y);
  
  ///
  /// @brief Set up basic openGL settings
  ///
  void initGL();
  
  ///
  /// @brief Set up background directional light
  ///
  void setLight0();
  
  ///
  /// @brief Set up second light source
  ///
  void setLight1();
  
  ///
  /// @brief Set up spotlight
  ///
  void setSpot();
  
  ///
  /// @brief Set axis
  ///
  inline void setAxis(bool flag) {plotAxis_ = flag;}
  
  ///
  /// @brief Updates spotlight, enables user to change the abngle of the light
  /// cone
  ///
  void updateSpot();
  
  ///
  /// @brief Print screen as tif-images
  ///
  int writeTiff () const;
  
 protected:
  PlotToolGL(const PlotToolGL &){}
  Camera *cam_;
  GLfloat spotCutOff_;
  size_t timeIndex_;
  bool fullScreen_, light0_, light1_, light2_;
  GLUquadricObj *quad_;
  void* font_;
  bool plotIndex_;
};
#endif //PLOTTOOLGL_H
