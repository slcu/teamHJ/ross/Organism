//
// Filename     : cylinder.cc
// Description  : Class describing cylinder-shaped cell colonies
// Author(s)    : Pontus Melke (pontus@thep.lu.se)
// Created      : April 2007
// Revision     : $Id:$
//
#include <cstdlib>
#include "cylinder.h"
using myVector3::Vector3;

Cylinder::Cylinder(Data *data, size_t dim, std::string min, std::string max)
	:BaseObject(data,dim, min,max)
{
	numSpatialVar_=1;
	xList_.resize(numSpatialVar_);
	rCol_.resize(numSpatialVar_);
	if (dimension()==1) {
		xList_[0]=0;
		rCol_[0] = 1;
		varIndex_=2;
	}
	else if (dimension() == 2) {
		xList_[0]=0;
		yList_.resize(numSpatialVar_);
		yList_[0] =1;
		rCol_[0] = 2;
		varIndex_=3;
	}
	else {
		std::cerr << "Cylinder is only availlable in 1D and 2D\n";
		std::exit(EXIT_SUCCESS);
	}
	//logVariables(12);
	truncateData();
	findMaxMin();
	
	normalizeVariables();
	//truncateData(0.5, 12);
	
	
	std::cerr << "set space scale ";
	setSpaceScale();
	std::cerr << "done\n";
	findMaxMin();
	max_[rCol_[0]]=14;
}

void Cylinder::plotGL(size_t cellIndex, size_t time)
{
	static bool first = true;
	if (first) {
		glutReshapeWindow(800,360);
		glutPostRedisplay();
		first = false;
	}
	if ( variable(time,cellIndex,0) == 1) {
		Vector3 pos = position(time,cellIndex,0);
		double rad = 2.0;
		double length = data_->getFrame(time).getMatrix()[cellIndex][rCol_[0]]/rad;
		double factor = 2/(scale());
		//double scale = 1.0;
		pos -= COM();
		pos *= factor;
		length *= factor;
		rad *= factor;
		glTranslatef(pos(0)-length/2,pos(1),0);
		glRotatef(pos.theta()*180/Pi,0.0f,1.0f,0.0f);
		gluCylinder(quad_,rad,rad,length,64,64);
	}
}

void Cylinder::plotPS(std::ofstream &OUT,size_t t,size_t cellIndex,
														float r, float g, float b, int move, double scaleFact)
{
	static long unsigned int counter = 0;
	//if ( variable(t,cellIndex,0) == 1) {
		Vector3 min(minX(),minY(),minZ());
		Vector3 max(maxX(),0,0);
		Vector3 pos=position(t,cellIndex,0)- min;
		//pos += max;
		bool isCell = bool(variable(t,cellIndex,0));
		bool isWall = bool(variable(t,cellIndex,1));
		bool isProducer = bool(variable(t,cellIndex,4));
		bool isDegrader = bool(variable(t,cellIndex,3));
		
		//pos += 1.2*maxR();
		pos *= scaleFact;
		double X,Y;
		//std::cerr << scaleFact << "\n";
		
		//move =-100;
		X = (1+scaleFact/2)*move + pos.x()+ maxX()*scaleFact;
		Y = move+ pos.y();
		double h =2.0;
		double l = data_->getFrame(t).getMatrix()[cellIndex][rCol_[0]]/(h*position(t,cellIndex,0).x());
		
		//double length = data_->getFrame(t).getMatrix()[cellIndex][rCol_[0]]/R;
		//double length = R;
		
		
		h*=scaleFact;
		double length=scaleFact*l;
		if (isCell) {
			OUT << r << " " << g << " " << b << " " << "setrgbcolor\n\n";
			//OUT << 1 << " " << 1 << " " << 1 << " " << "setrgbcolor\n\n";
			OUT << "newpath\n"
					<< "[\n"
					<< "\t[" << X-0.5*length << " " << Y-0.5*h << "]\n"
					<< "\t[" << X+0.5*length << " " << Y-0.5*h << "]\n"
					<< "\t[" << X+0.5*length << " " << Y+0.5*h << "]\n"
					<< "\t[" << X-0.5*length << " " << Y+0.5*h << "]\n"
					<< "]\n";		
			OUT << "makepolygon\n"
					<< "closepath\n"
					<< "fill\n\n";
			//mirror image
			X =  (1+scaleFact/2)*move - pos.x() + maxX()*scaleFact;
			
			OUT << "newpath\n"
					<< "[\n"
					<< "\t[" << X-0.5*length << " " << Y-0.5*h << "]\n"
					<< "\t[" << X+0.5*length << " " << Y-0.5*h << "]\n"
					<< "\t[" << X+0.5*length << " " << Y+0.5*h << "]\n"
					<< "\t[" << X-0.5*length << " " << Y+0.5*h << "]\n"
					<< "]\n";		
			OUT << "makepolygon\n"
					<< "closepath\n"
					<< "fill\n\n";

			X = (1+scaleFact/2)*move + pos.x()+ maxX()*scaleFact;
			OUT << "0 0 0 setrgbcolor\n";
			//OUT << "/Helvetica-Bold findfont\n";
			//OUT << "3.0 scalefont\n";
			//OUT << "setfont\n";
			//OUT << X-3 << " " << Y-3 << " moveto\n";
			//OUT << "(" << counter << ") show\n";
			// OUT << "0 0 0 setrgbcolor\n";
// 			OUT << "newpath\n"
// 					<< "[\n"
// 					<< "\t[" << X-0.5*length << " " << Y-0.5*h << "]\n"
// 					<< "\t[" << X+0.5*length << " " << Y-0.5*h << "]\n"
// 					<< "\t[" << X+0.5*length << " " << Y+0.5*h << "]\n"
// 					<< "\t[" << X-0.5*length << " " << Y+0.5*h << "]\n"
// 					<< "]\n";		
// 			OUT << "makepolygon\n"
// 					<< "closepath\n"
// 					<< "stroke\n\n";
// 			X =  (1+scaleFact/2)*move - pos.x() + maxX()*scaleFact;
// 			OUT << "0 0 0 setrgbcolor\n";
// 			OUT << "/Helvetica-Bold findfont\n";
// 			OUT << "3.0 scalefont\n";
// 			OUT << "setfont\n";
// 			OUT << X-3 << " " << Y-3 << " moveto\n";
// 			OUT << "(" << counter << ") show\n";
// 			OUT << "0 0 0 setrgbcolor\n";
// 			OUT << "newpath\n"
// 					<< "[\n"
// 					<< "\t[" << X-0.5*length << " " << Y-0.5*h << "]\n"
// 					<< "\t[" << X+0.5*length << " " << Y-0.5*h << "]\n"
// 					<< "\t[" << X+0.5*length << " " << Y+0.5*h << "]\n"
// 					<< "\t[" << X-0.5*length << " " << Y+0.5*h << "]\n"
// 					<< "]\n";		
// 			OUT << "makepolygon\n"
// 					<< "closepath\n"
// 					<< "stroke\n\n";
		}
		else if (isWall) {
			//OUT << r << " " << g << " " << b << " " << "setrgbcolor\n\n";
			OUT << "0 0 0 setrgbcolor\n";
			OUT << "newpath\n"
					<< "[\n"
					<< "\t[" << X-0.5*length << " " << Y-0.5*h << "]\n"
					<< "\t[" << X+0.5*length << " " << Y-0.5*h << "]\n"
					<< "\t[" << X+0.5*length << " " << Y+0.5*h << "]\n"
					<< "\t[" << X-0.5*length << " " << Y+0.5*h << "]\n"
					<< "]\n";		
			OUT << "makepolygon\n"
					<< "closepath\n"
				//<< "stroke\n";
					<< "fill\n\n";
			//std::cout << X << " " << Y << " " << R << " " << length << "\n";
			if (pos.x() != 0) {
				X =  (1+scaleFact/2)*move - pos.x()+ maxX()*scaleFact;
				OUT << "newpath\n"
						<< "[\n"
						<< "\t[" << X-0.5*length << " " << Y-0.5*h << "]\n"
						<< "\t[" << X+0.5*length << " " << Y-0.5*h << "]\n"
						<< "\t[" << X+0.5*length << " " << Y+0.5*h << "]\n"
						<< "\t[" << X-0.5*length << " " << Y+0.5*h << "]\n"
						<< "]\n";		
				OUT << "makepolygon\n"
						<< "closepath\n"
					//<< "stroke\n";
				<< "fill\n\n";
			}
		//
// 			OUT << "1 1 1 setrgbcolor\n";
// 			OUT << "/Helvetica-Bold findfont\n";
// 			OUT << "3.0 scalefont\n";
// 			OUT << "setfont\n";
// 			OUT << X-3 << " " << Y-3 << " moveto\n";
// 			OUT << "(" << counter << ") show\n";
		}
		counter++;
}



