#include <iostream>
#include "baseObject.h"
#include "basePlotTool.h"
#include "src/myConfig.h"
#include "camera.h"

int main (int argc, char *argv[])
{	
  // Initialise user configuration.
  myConfig::registerOption("d", 1);
  myConfig::registerOption("shape", 1);
  myConfig::registerOption("output", 2); 
  myConfig::registerOption("column", 1);
  myConfig::registerOption("schema", 1);
  myConfig::registerOption("camera", 1);
  myConfig::registerOption("size", 1);
  myConfig::registerOption("axis", 0);
  myConfig::registerOption("ambient", 0);
  myConfig::registerOption("gLevel", 1);
  myConfig::registerOption("min", 1);
  myConfig::registerOption("max", 1);
  myConfig::registerOption("help", 0);
  
  myConfig::initConfig(argc, argv, ".newman");	
  
  if (myConfig::getBooleanValue("help")) {
    std::cerr << std::endl 
	      << "Usage: " << argv[0] << " File" << std::endl 
	      << std::endl;
    std::cerr << "Possible additional flags are:" << std::endl;
    std::cerr << "-d D - Set dimension to D, where D=2"
	      << " is the default option" << std::endl;
    std::cerr << "-shape \"SHAPE\" - Define the shape of "
	      << " object to be plotted. \"SHAPE\" can be chosen as" 
	      << " sphere(default), ellipse, cappedCylinder and buddingYeast" << std::endl;
    std::cerr << "-output \"FORMAT\" \"FILE\" - Set output format (tiff or ps)"
	      << " and file name" << std::endl;
    std::cerr << "-column C - Sets column to be used for color"
	      << " coding the plot (C=0 by default)" << std::endl;
    std::cerr << "-schema S - Sets color schema to be used for color"
      
	      << " coding the plot. S = 0,1,2 or 3 (S=0 by default)" << std::endl;
    std::cerr << "-camera FILE - FILE includes all settings for the camera" << std::endl;
    std::cerr << "-size SIZE - set size of plot (360 default)" << std::endl;
    std::cerr << "-axis - plot with axis" << std::endl;
    std::cerr << "-ambient - use only ambient light" << std::endl;
    std::cerr << "-min VALUE- set the min value" << std::endl;
    std::cerr << "-max VALUE set the maximum value" << std::endl;
    exit(EXIT_SUCCESS);
  }
  //Check the number of command line arguments.
  if (myConfig::argc() != 1 && myConfig::argc() != 2 ) {
    std::cerr << "Type '" << argv[0] << " -help' for usage." << std::endl;
    exit(EXIT_FAILURE);
  }
  
  //check dimensionality (default 2D)
  std::string dimensionString = myConfig::getValue("d",0);
  size_t dimension;
  if (dimensionString.empty()) {
    std::cerr << "No dimension parameter specified, assuming 2D\n";
    dimension =2;
  }
  else
    dimension = atoi(dimensionString.c_str());
  //check size (default 360)
  std::string sizeString = myConfig::getValue("size",0);
  int size;
  if (sizeString.empty()) {
    std::cerr << "using default size\n";
    size =360;
  }
  else
    size = atoi(sizeString.c_str());
  
  //check cell shape (default sphere)
  std::string cellShape =  myConfig::getValue("shape",0);
  if (cellShape.empty()) {
    std::cerr << "No cell shape specified, assuming sphere\n";
    cellShape = "sphere";
  }
  //check output
  std::string outFlag  =  myConfig::getValue("output",0);
  std::string outFile  = myConfig::getValue("output",1);
  if (outFlag.empty()) { 
    std::cerr << "No output specified\n";
    outFlag ="GL";
  }
  else { 
    std::cerr << outFlag << " will be saved in " << outFile << "\n";
  }
  //check color column (default 0)
  size_t cc=0;
  std::string ccString =  myConfig::getValue("column",0);
  if (ccString.empty())
    std::cerr << "No column specified, assuming first column\n";
  else
    cc = atoi(ccString.c_str());
  //check color schema (default 0)
  size_t cs=0;
  std::string csString =  myConfig::getValue("schema",0);
  if (csString.empty())
    std::cerr << "No schema specified, using default\n";
  else
    cs = atoi(csString.c_str());
  //check gLevel-settings
  double gLevel=0.2;
  std::string gLevelString =  myConfig::getValue("gLevel",0);
  if (gLevelString.empty())
    std::cerr << "No gLevel specified, using default\n";
  else
    gLevel = atof(gLevelString.c_str());
  
  //check camera settings
  std::string cameraFile =  myConfig::getValue("camera",0);	
  //Check done, start plotting
  std::istream *input;
  std::ifstream infile;
  if ( myConfig::argc() == 2 ) {
    std::string inputFile = myConfig::argv(1);
    std::cerr << "Try to open file " << inputFile << "...";
    infile.open(inputFile.c_str());
    if (!infile) {
      std::cerr << "\nError: Unable to open file " << inputFile 
		<< " for input."  << std::endl;
      exit(-1);
    }
    input = &infile;
    std::cerr << "Done\n";
  }
  else
    input = &std::cin;
  
  std::string min =myConfig::getValue("min",0);
  std::string max =myConfig::getValue("max",0);
  
  //create object to be plotted
  BaseObject *Obj = objectFactory(cellShape, *input, dimension, min, max);
  
  //Create camera for GL-plotting
  Camera cam;
  if (!cameraFile.empty())
    cam.readCameraSettings(cameraFile);
  
  //create plot tool
  BasePlotTool *Plot = plotFactory(outFlag,Obj,outFile,cc,cs,size,gLevel,&cam);
  Plot->setAxis(myConfig::getBooleanValue("axis"));
  Plot->setLight(!myConfig::getBooleanValue("ambient"));
  //plot
  std::cerr << "Plotting " << cellShape << "s in " << dimension << "D\n";
  Plot->plot();
  
  delete Plot;
  delete Obj;
  
  return 0;
}
