//
// Filename     : ellipse.cc
// Description  : Class describing ellipse shaped cell colonies
// Author(s)    : Pontus Melke (pontus@thep.lu.se)
// Created      : April 2007
// Revision     : $Id:$
//
#include <cstdlib>
#include <cstdlib>
#include "ellipse.h"
using myVector3::Vector3;

Ellipse::Ellipse(Data *data,size_t dim, std::string min, std::string max, double b) 
	:BaseObject(data,dim,min,max), b_(b)
{
		numSpatialVar_ = 2;
	
	
	xList_.resize(numSpatialVar_);
	yList_.resize(numSpatialVar_);
	rCol_.resize(1);
	if(dimension()==3)
		zList_.resize(numSpatialVar_);

	if (dimension()==2) {
		varIndex_= 5;
		rCol_[0]= 4;
		xList_[0] = 0;
		xList_[1] = 2;
		yList_[0] = 1;
		yList_[1] = 3;	
	}
	else if(dimension()==3) {
		varIndex_=7;
		rCol_[0]= 6;
		xList_[0] = 0;
		xList_[1] = 3;
		yList_[0] = 1;
		yList_[1] = 4;
		zList_[0] = 2;
		zList_[1] = 5;
	}
	else {
		std::cerr << "Ellipse::Ellipse(): " 
							<<dimension() <<"D objects are not supported\n";
		std::exit(EXIT_FAILURE);
	}
	truncateData();
	findMaxMin();
	normalizeVariables();
	setSpaceScale();
	findMaxMin();
}

/*void Ellipse::setNeighbor(double rFactor)
{ 	
  double d,r;
	Vector3 posi,posj;
	neighbor_.resize( TMax() );
  for( size_t t=0 ; t<TMax() ; ++t ) {
		neighbor_[t].resize(numCell(t) );
		for( size_t i=0 ; i<numCell(t) ; i++ )
      for( size_t j=i+1 ; j<numCell(t); j++ ) {
				
				r = std::sqrt( dot(position(t,i,0)-position(t,i,1),
													 position(t,i,0)-position(t,i,1))) +
					std::sqrt( dot(position(t,j,0)-position(t,j,1),
												 position(t,j,0)-position(t,j,1)));
				
				posi = 0.5*(position(t,i,0)+position(t,i,1));
				posj = 0.5*(position(t,j,0)+position(t,j,1));
				d = std::sqrt(dot(posi-posj,posi-posj));
				if( d<= r*rFactor ) {
					//Add neighbor
					neighbor_[t][i].push_back(j);
					neighbor_[t][j].push_back(i);
				}
      }
	}	
}*/

void Ellipse::plotGL(size_t cellIndex, size_t time)
{
	//rescales normals after glScale is called, 
	//otherwize shadowing is destroyed
	glEnable(GL_NORMALIZE);
	
	//find foci
	Vector3 position1 = position(time,cellIndex,0);
	Vector3 position2 = position(time,cellIndex,1);

	double factor = 2/(scale());
	//semiminoraxis
	double bCell= b_;
	//rescale vectors
	position1 -= COM();
	position2 -= COM();
	position1 *=factor;
	position2 *=factor;
	bCell *=factor;
	
	Vector3 diff= position2-position1;
	//semimajor axis
	float a = 0.5*std::sqrt(dot(diff,diff));
	//center-of-mass
	Vector3 com = 0.5*(position1+position2);
	//draw ellipse by properly deforming sphere
	glTranslatef(GLfloat(com(0)),float(com(1)),float(com(2)));
	glRotatef(diff.phi()*180/Pi,0.0f,0.0f,1.0f);
	glScalef(a,GLfloat(bCell),GLfloat(bCell));
	gluSphere(quad_,1.0f,16,16);
}
 
void Ellipse::plotPS(std::ofstream &OUT,size_t t,size_t cellIndex,
										 float r, float g, float b, int move, double scaleFact)
{
  Vector3 min(minX(),minY(),minZ()); 
	Vector3 position1=(position(t,cellIndex,0)- min);
	Vector3 position2=(position(t,cellIndex,1)- min);
  position1 += 1.2*maxR();
  position1 *= scaleFact;
	position2 += 1.2*maxR();
  position2 *= scaleFact;
	Vector3 u= position2-position1;
	double X,Y,A,B;
	X = move + 0.5*(position1.x()+position2.x());
	Y = move + 0.5*(position1.y()+position2.y());
	A = 0.5*std::sqrt(dot(u,u));
	B = b_*scaleFact;
	OUT << scaleFact/11.0 << " setlinewidth\n";
	OUT << r << " " << g << " " << b << " " << "setrgbcolor\n\n";
	OUT << "gsave\n";
	OUT << X << " " << Y << " " << "translate\n";
	OUT << u.phi()*180/Pi << " rotate\n";
	OUT << "0 0 " << A << " " << B << " 0 360 ellipse\n";
	OUT << "gsave\n";
	OUT << "fill\n";
	OUT << "grestore\n";
	OUT << "0 0 0 setrgbcolor\n";
	OUT << "stroke\n\n";			
	OUT << "grestore\n";
}



