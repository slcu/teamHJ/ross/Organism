//
// Filename     : basePlotTool.h
// Description  : Base class for plot tools
// Author(s)    : Pontus Melke (pontus@thep.lu.se)
// Created      : April 2007
// Revision     : $Id:
//
#ifndef BASEPLOTTOOL_H
#define BASEPLOTTOOL_H
#include "cappedCylinder.h"
#include <cstdlib>
#include "sphere.h"
#include "camera.h"
#include <cstdlib>

///
/// @brief A base class for plot tools
///
/// The BasePlotTool class defines an interface for plot tools. It
/// sets basic functionalities such as color schemas, back ground
/// intensities etc. The derived classes must implement the function plot()
///
class BasePlotTool {
 public:
	
	BasePlotTool() {std::exit(-1);}
	/// @brief Main constructor each derived class must call this
	/// constructor!
	///
	/// @param obj the object to be plotted
	/// population at each time step
	///
	/// @param file file name for output, if empty nothing is plotted
	///
	/// @param cc color column, i.e. column used for color coding
	///
	/// @param cs color schema
	///
	/// @param size sets size of plot, 360 is default
	///
	BasePlotTool(BaseObject *obj,std::string file,size_t cc, size_t cs, int size=360, double gLevel=0.2);
	
	///
	/// @brief virtual destructor, enables polymorphism 
	///
	virtual ~BasePlotTool(){}

	
	///
	/// @brief pure virtual plot function, must be implemented in
	/// derived classes
	///
	virtual	void plot()=0 ;

	///
	/// @brief sets color schema
	///
	void setColor(float value);
	
	///
	/// @brief sets output file name
	///
	inline void setOutputFile(std::string &s){outputFile_=s;}
	
	/// 
	/// @brief returns color schema
	///
	inline size_t colorSchema()const{return colorSchema_;}
	
	/// 
	/// @brief returns color column
	///
	inline size_t colorColumn()const{return colorColumn_;}
	
	/// 
	/// @brief returns value of the 'red chanel'
	///
	inline float red() const {return red_;}
	
	/// 
	/// @brief returns value of the 'green chanel'
	///
	inline float green() const {return green_;}
	
	/// 
	/// @brief returns value of the 'blue chanel'
	///
	inline float blue() const {return blue_;}
	
	/// 
	/// @brief returns intensity level
	///
	inline float gLevel() const {return gLevel_;}

	/// 
	/// @brief returns background level
	///
	inline float bgLevel() const {return bgLevel_;}
	
	/// 
	/// @brief returns the total number of color schemas
	///
	inline size_t numSchema() const {return 4;}

	/// 
	/// @brief returns file name
	///
	inline std::string outputFile() const {return outputFile_;}

	/// 
	/// @brief returns 'true' if plot is to be saved, otherwise 'false'.
	///
	inline bool outputFlag() {return !outputFile_.empty();}
	
	///
	/// @brief reurns size of plot
	///
	inline int size() {return imageSize_;}

  ///
	/// @brief Set axis
	///
	inline void setAxis(bool flag) {plotAxis_ = flag;}
	
  ///
	/// @brief Set axis
	///
	inline void setLight(bool flag) {light_ = flag;}

 protected:
	BasePlotTool(const BasePlotTool &){}
	BaseObject *object_;
	size_t colorColumn_,colorSchema_;
	float red_, green_, blue_, gLevel_, bgLevel_;
	std::string outputFile_;
	int imageSize_;
	bool plotAxis_, light_;
};

///
/// @brief Factory for plot tools, all creation should be mapped onto this
/// one
///
/// Given the cell shape and the type of plot method, a pointer to an
/// instance of a plot tool object is returned.
///
/// @param shape defines the shape of the object, 'sphere' and
/// 'cappedCylinder' are supported.
///
/// @param method defines type of plotting method, 'GL', 'tiff' and
/// 'ps' are supported.
///
/// For the other parameters see, @see
/// BasePlotTool::BasePlotTool(BaseObject *obj,const std::string
/// &file,size_t cc, size_t cs, int size); 
///
/// @return Returns a pointer to an instance of a plot tool
/// 
///
BasePlotTool *plotFactory(std::string method, BaseObject *obj, 
													std::string file, size_t cc, size_t cs , int size,double gLevel, Camera *c);
#endif //BASEPLOTTOOL_H
