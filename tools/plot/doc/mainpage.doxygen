// This file is used to generate an introductory text in the doxygen
// generated documentation

/*! @mainpage

\section sec1 Introduction

Newman is a C++ software for plotting cell colonies and multicellular
organisms. It is the main plotting tool for the Organism package.

Some of the features of the software are:

<ul>

<li> Follow the time development of multicellular organisms and cell colonies
 in 2D or 3D.

<li> Can deal with several different cell shapes such as spheres, ellipsoids,
capped cylinders etc.

<li> Generate high quality tiff-images that can be used for creating movies.

<li> Generate high quality vector based postscript images

</ul>

The software is mainly developed at the Computational Biology & Biological
Physics group at Lund University. Contact: pontus@thep.lu.se.

\section sec2 Running newman

\e Newman is utilized from the command line. For the simplest applications you
simply type: 
\code 
newman file 
\endcode

where \c file contains the data to be plotted. If nothing else is specified \e
newman assumes a 2D openGL plot with sphere-shaped cells, and no files are
saved to disc. For other settings appropriate keywords can be given at the command line,
where the following command line options are supported in Newman:

<ul> 
<li> <tt>-shape SHAPE:</tt> Sets the cell shape to be used. Possible options are \e
sphere (default), \e ellipse,\e cappedCylinder and \e buddinYeast.

<li> <tt>-d  D :</tt> where \c D is the dimension

<li> <tt>-output FORMAT FILENAME</tt> : Possible formats are \e tiff and  \e
ps. If \e tiff is chosen with no -camera flag, the number of tiff-images
generated will be the same as the number of time points in the data file.

<li> <tt>-column C</tt> : Sets the column to be used for color coding the plot. \e C =
0 by default.

<li> <tt>-schema  S</tt> : Sets the color schema be used for color coding the
plot. Information on the different color schema can be found in
BasePlotTool::setColor(). \e S = 0 by default.

<li> <tt>-size S</tt> : Sets the size of the images.\e S = 360 by default.

<li> <tt>-camera FILE</tt> : The \e FILE defines a series of camera movements which can be
used for creating small movies. Information on the formating the \e camera \e file
can found in the documentation of Camera::readCameraSettings().
</ul>

Instead of giving keywords at the command line it is also possible to save a
settings file. This file must be called <tt>.newman</tt> and must be located in the
directory from where you run Newman. The <tt>.newman</tt> file should be organized as
follows:\n
\code
key1 arg11 arg12 ...
key2 arg21 arg22 ...
 ...  ...
\endcode
Note that the keywords here is given without the dash.
			

\section sec3 Keyboard control

In the openGL/tiff-mode the camera can be controlled with the keyboard as well
as with the camera-flag mentioned above. This allows the user to zoom, rotate,
move etc. The following keyboard options ara available:

\code 

LEFT ARROW - moves the camera to the left 

RIGHT ARROW - moves the camera to the right 

UP ARROW - moves the camera upwards 

DOWN ARROW - moves the camera downwards

SHIFT+LEFT ARROW - Rotate camera anti-clockwise around 
the camera z-axis

SHIFT+RIGHT ARROW - Rotate camera clockwise around 
the camera z-axis

SHIFT+UP ARROW - Rotate camera clockwise around 
the camera x-axis

SHIFT+UP ARROW - Rotate camera anti-clockwise around 
the camera x-axis

CTRL+LEFT ARROW - Roll the camera anti-clockwise

CTRL+RIGHT ARROW - Roll the camera clockwise

ALT+LEFT ARROW - Yaw the camera to the left

ALT+RIGHT ARROW - Yaw the camera to the right

ALT+UP ARROW - Pitch the camera upwards

ALT+DOWN ARROW - Pitch the camera downwards

F1 - Full screen mode

p - pause/restart animation

z/Z - zoom in/ zoom out

s/S - step downwards/upwards in color schema

c/C - step downwards/upwards in color column

0 - on/off background light

1 - on/off directed light

2 - on/off spotlight

f/F - focus/unfocus spotlight

+/- - When paused, forward/rewind

t/T - increase/decrease speed

b/B - decrease/increase background intensity

l/L - decrease/increase intensity of object

\endcode

\section Compilation

The code can be compiled by using \e make or \e scons. Both a \e Makefile and
a \e SConstruct file can be found found in the $ORGANISM/src directory. The
code is ANSI-compatible and has been tested to compile on Linux, Mac OSX and
Windows (using Cygwin) platforms.

*/
