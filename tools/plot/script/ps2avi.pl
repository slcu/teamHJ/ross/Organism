#!/usr/bin/perl -w 
use strict;

#######################################################################
#This perl-script creates an avi-movie using mencoder.  
#Input: a ps-image containing several plots.
#output: avi-movie
########################################################################

if(@ARGV != 2) { 
    die "plot takes two arguments, inputfiles and outputfile\n"; 
} 
my $input = $ARGV[0];
my $output = $ARGV[1];

my $outDir ="PNG";
system("mkdir $outDir");
system("convert $input $outDir/out%03d.png");
my $string = "mencoder -ovc lavc -oac copy -mf fps=2 'mf://PNG/*.png' -of avi -lavcopts vcodec=mjpeg:keyint=1:mbd=1:vqmin=2:vqmax=10:autoaspect -o "; 
#my $string = "mencoder -ovc lavc -oac copy -mf fps=10 'mf://PNG/*.png' -of avi -lavcopts vcodec=mpeg2video:keyint=1:mbd=1:vqmin=2:vqmax=10:autoaspect -o "; 
system($string.$output.".avi");

system("rm -r $outDir");



