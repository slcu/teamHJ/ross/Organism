# -*- coding: utf-8 -*-
import os
import subprocess
import math

# simulates a model file and stores the output in num_out
def integrate (model_file, init_file, rk_param_file, model_param_file, out_file) :
  os.popen("../../bin/simulator %s %s %s -parameter_input %s -verbose 0 > %s" % (model_file,init_file,rk_param_file,model_param_file, out_file))

# generates the rk parameters file for the simulator
def generate_rk_param(file_out,duration,steps):
  chan = open(file_out,"w")
  chan.write("RK5Adaptive\n")
  chan.write("0 %i\n" % duration)
  chan.write("1 %i\n" % steps)
  chan.write("1.0 1e-3\n\n\n\n")
  chan.close()
  
# generates the parameters file for the simulator from a list of floats
def generate_para(file_out,l):
  chan = open(file_out,"w")
  for e in l:
    chan.write(str(e)+" ")
  chan.write("\n")
  chan.close()
  
# uses a stable state from stabilize.py (lines) to output an init_file for the simulator
# cells are the amount of cells
# topology is 4 or 3 for 3D or 2D simulations
# addit_species is the number of additional species in a mutant regarding the wild type
def stable2init(file_out,lines,species,topology,cells,addit_species=0):
  ns = ""
  for i in range(0,addit_species):
    ns += " 0"
  chan=open(file_out,"w")
  chan.write("%i %i\n" % (cells, topology+species+addit_species))
  for l in lines:
    chan.write(l[:-3]+ns+"\n")
  chan.close()
  
#takes an organism model file and return the number of species
def get_nb_species(model_file):
  chan = open(model_file,"r")
  lines = chan.readlines()
  chan.close()
  index,line=0,lines[0]
  while line == "\n" or line[0] == "#":
    index += 1
    line = lines[index]
  s = line.split(" ")
  while "" in s:
    s.remove("")
  return int(s[2])
  
#takes an organism model file and return the amount of variables used for the topology
def get_topology(model_file):
  chan = open(model_file,"r")
  lines = chan.readlines()
  chan.close()
  index,line=0,lines[0]
  while "topology" not in line:
    index += 1
    line = lines[index]
  s = line.split(" ")
  while "" in s:
    s.remove("")
  return int(s[1])

#takes an organism init file and return the amount of cells
def get_nb_cells(init_file):
  chan = open(init_file,"r")
  line = chan.readlines()[0]
  chan.close()
  return int(line.split(" ")[0])

#takes a list of lines ("float"\t"float"\t"float"...) and returns the postion of line having the smallest last float
#lines should come from an optimization file
def get_best_score_pos(lines):
  best_score,best_pos = 99999999999,0
  for i,l in enumerate(lines):
    s=l[:-1].split("\t")
    if float(s[-1]) <= best_score:
      best_score = float(s[-1])
      best_pos = i
  return i
