# -*- coding: utf-8 -*-
import signal
import os
import subprocess
import time
import copy

#returns the square difference of two states (l1,l2) for a set of selected species
def compare_states(l1,l2,species_positions):
  sqr_diff = 0
  for i in range(0,len(l1)-1):
    line1,line2 = l1[i],l2[i]
    s1 = line1[:-1].split(" ")
    s2 = line2[:-1].split(" ")
    for p in species_positions:
      sqr_diff += (float(s1[p]) - float(s2[p]))**2
  #print ("\rSquare diff: "+str(sqr_diff))
  return sqr_diff
  

#launches a simulation and kills it when the simulation reaches its stable state.
#outputs the stable state
#inputs are the files to lauch the simulation, the position of the species to stablilize,
#the square diff threshold below wich the model is considered stabilized, the amount of cells in the simulation
#a maximum running time
def process(model_file,init_file,rk_file,param_file,species_positions, diff_threshold, nb_cells,time_max = 10000, steps_max = 100000):
  t = time.time()
  p = subprocess.Popen(["../../bin/simulator %s %s %s -parameter_input %s" % (model_file,init_file,rk_file,param_file)],shell=True,stdout=subprocess.PIPE,stderr=subprocess.PIPE)
  line = p.stdout.readline()
  test_stable = 1
  old_list,new_list = [],[]
  while (test_stable > diff_threshold):
    line = p.stdout.readline()
    #if line[:4] != str(nb_cells):
    if line.split(" ")[0] != str(nb_cells):
      new_list.append(line)
    else :
      #print line[:-1]
      steps = float(line[:-1].split(" ")[2])
      if len(old_list) > nb_cells:
	test_stable = compare_states(old_list,new_list,species_positions)
      old_list = copy.copy(new_list)
      new_list = []
    if time.time() - t > time_max or steps > steps_max: 
      p.kill()
      #os.kill(p.pid+1,signal.SIGKILL)
      p.stdout.close()
      p.stderr.close()
      return old_list
      break
  p.kill()
  #os.kill(p.pid+1,signal.SIGKILL)
  p.stdout.close()
  p.stderr.close()
  return old_list
  
#process("../../python_sam/model1.model", "../../python_sam/model1.init_3anchors", "../../python_sam/temp2.rk", "../../python_sam/light.para", [5,7,9], 10e-6, 1366)
