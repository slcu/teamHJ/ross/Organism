#! /usr/bin/python
# -*- coding: utf-8 -*-

#changed default step from 0.01 to 0.02

import sys
import routines
import stabilize
import os
import time
import hill_climbing
import random
import copy
import math

# Cell contains 2 lists of floats, the topology information and the concentration of the various species
class Cell:
  def __init__(self,variables):
    self.variables = variables
    
# Type defines the wild and mutant types to apply to the template
# name: string, model_file: organism model file, f_convert: function taking a list of parameters (typically those used to optimize the wild type) and returns the complete list of parameters for organism, cost_species: list of integers giving the position of the species to use for computing the cost function of the optimizer, those species concentration will also be written in the output files
# init_file is used if a type should be run with a specific init file. Typical uses are running the optimization with an init different of the template, or running a mutant with an init different of the stabilized wild type
# sensitivity != None if the type is used to check the sensitivity of the wild model to variation of a parameter. Should be given (int,float): the position of the parameter in organism parameter file, the variation applied to the parameter

class Type:
  def __init__(self,name,model_file,f_convert,cost_species=[],init_file=None, init_template=False,init_wild=False,sensitivity=None, f_theory = None,param_stabilization = [], stabilization_species = []):
    self.name = name
    self.model_file = model_file
    self.f_convert = f_convert
    self.nb_species = routines.get_nb_species(model_file)
    self.init_file = init_file
    self.cost_species = cost_species
    self.f_theory = f_theory
    if f_theory == None:
      self.f_theory = [None for e in cost_species]
    self.init_template= init_template
    self.init_wild= init_wild
    if (init_file==None) and (not init_template) and (not init_wild):
      raise Exception("Types must have an init_file or be initiated with template file or stabilized wild type (init_file > init_wild > init_template)")
    self.sensitivity = sensitivity
    #param_stabilization are the indexes of the parameters that requires the model to be stabilized again if modified. Used in combination with specific cost functions not based on stabilizations.
    self.param_stabilization = param_stabilization
    #if the simulations should not be stabilized based on the cost_species, provide stabilization_species
    self.stabilization_species = stabilization_species
    #see optimization function for scramble option
    self.scramble = 0
    if self.stabilization_species == []:
      self.stabilization_species = self.cost_species

class Pattern:
  def __init__(self, template_file, directory, wild_type, mutants, f_cell_in_cost = (lambda x: True), stable_threshold = 1e-9):
    self.topology = routines.get_topology(wild_type.model_file)
    self.template_file = template_file
    self.directory = directory
    self.wild_type = wild_type
    self.mutants = mutants
    self.nb_cells = routines.get_nb_cells(template_file)
    self.stable_threshold = stable_threshold
    self.stable = None
    self.diff=[]
    self.s2s = False
    self.chan_optimize = None
    # function taking a list of str(float) values corresponding to a cell output of the simulator, if returns false, the cell is not used for computing the cost function
    self.f_cell_in_cost = f_cell_in_cost
    # previous_parameters is the previous set of parameters used in the optimization, the function need_stabilize returns true if one of the parameters from type.param_stabilization has been updated. It then actualises the value of the previous_parameters.
    self.previous_parameters = []
    chan = open(template_file,"r")
    lines = chan.readlines()
    chan.close()
    self.cell_list = []
    for l in lines[1:]:
      if l != "\n" :
        s = l[:-1].split(' ')
        while "" in s:
          s.remove("")
        s = [float(e) for e in s]
        self.cell_list.append(Cell(s[:self.topology+self.wild_type.nb_species]))
        
  def need_stabilize(self,p):
    out = False
    for i in range(0,len(p)):
      if (p[i] != self.previous_parameters[i]) and (i in self.wild_type.param_stabilization): out = True
    self.previous_parameters = copy.copy(p)
    return out
    
    
  def stabilize(self, parameters):
    if self.s2s and self.stable != None: init_file = self.directory+"/temp.init"
    else :
      if self.wild_type.init_template:
	init_file=self.template_file
      else :
	init_file=self.wild_type.init_file
    full_params = self.wild_type.f_convert(parameters)
    routines.generate_rk_param(self.directory+"/temp.rk",1000000,100000)
    routines.generate_para(self.directory+"/parameters",full_params)
    self.stable = stabilize.process(self.wild_type.model_file,init_file,self.directory+"/temp.rk",self.directory+"/parameters",self.wild_type.stabilization_species,self.stable_threshold,self.nb_cells,time_max = 1000, steps_max=1500)
    
  def stabilize_mutant(self,parameters,mutant):
    if mutant.init_template: init_file = self.template_file
    if mutant.init_wild: init_file = self.directory+"/temp.init"
    if mutant.init_file != None: init_file = mutant.init_file
    full_params = mutant.f_convert(parameters)
    routines.generate_rk_param(self.directory+"/temp.rk",1000000,1000000)
    routines.generate_para(self.directory+"/parameters",full_params)
    return stabilize.process(mutant.model_file,init_file,self.directory+"/temp.rk",self.directory+"/parameters",self.wild_type.stabilization_species,self.stable_threshold,self.nb_cells,time_max = 1000, steps_max=1500)

  # computes the square diff between the values stores in the template and the values stored in the stable state for all the cost_species
  # can add a theoretical value for each cost sepcie computed using the variables of a cell in the stable state and the template
  def diff_stable_template(self):
    if self.stable == None :
      self.diff = [10000 for sp in self.wild_type.cost_species]
    else :
      self.diff = [0 for sp in self.wild_type.cost_species]
      for i,s in enumerate(self.stable[:-1]):
        s = s.split(" ")
        s = [float(e) for e in s]
        if self.f_cell_in_cost(s):
	  for j,sp in enumerate(self.wild_type.cost_species):
	    if self.wild_type.f_theory[j] == None:
	      self.diff[j] = self.diff[j] + (s[sp] - self.cell_list[i].variables[sp])**2
	    else :
	      self.diff[j] = self.diff[j] + self.wild_type.f_theory[j](s,self.cell_list[i].variables,self.previous_parameters)
	  
  def randomize_stable(self):
    accu = []
    for l in self.stable[:-1]:
      s = l.split(" ")
      for sp in self.wild_type.stabilization_species:
	if random.random <= 0.5: value = float(s[sp]) * (1 - self.scramble)
	else: value = float(s[sp]) * (1 + self.scramble)
	s[sp] = str(value)
      nl = ""
      for e in s:
	nl += e + " "
      nl = nl[:-1]
      accu.append(nl)
    self.stable = accu + ["\n"]
    
  def f_optimize(self,params):
    if self.previous_parameters == []: self.previous_parameters = [1 for e in params]
    need_stabilize_out = True
    if self.wild_type.param_stabilization != []:
      need_stabilize_out = self.need_stabilize(params)
    if self.s2s and self.stable != None:
      self.randomize_stable()
      routines.stable2init(self.directory+"/temp.init",self.stable,self.wild_type.nb_species,self.topology,self.nb_cells)
    if need_stabilize_out: self.stabilize(params)
    self.diff_stable_template()
    score = sum(self.diff)
    for p in params:
      self.chan_optimize.write(str(p)+"\t")
    for d in self.diff:
      self.chan_optimize.write(str(d)+"\t")
    self.chan_optimize.write(str(score)+"\n")
    self.chan_optimize.flush()
    return score

  #s2s goes from stable state to stable state during the optimization
  # scramble is an s2s option that difines the strength of the randomization to apply a stable state before next optimization step
  def optimize(self,params, iters=10000,step = 0.02, s2s = False, scramble = 0.001 ,limit=0, stop=-1):
    self.scramble = scramble
    self.s2s = s2s
    if self.chan_optimize == None : self.chan_optimize = open(self.directory+"/optimization","a")
    return hill_climbing.run(self.f_optimize,params, step = step, iters=iters, nb_tokens = 100, limit = limit, stop = stop)
  
  #takes a list of type names and a list parameters
  #displays the simulation results
 def test(self,names,parameters,duration=300,steps=300):
    for n in names:
      if n not in [m.name for m in self.mutants+[self.wild_type]]: raise Exception("Undefined mutant: "+n)
    for t in [self.wild_type]+self.mutants:
      if t.name in names:
	init_file = t.init_file
	if init_file == None and t.init_template == True:
	  init_file = self.template_file
	if init_file == None and t.init_wild == True:
	  if self.stable == None:
	    self.stabilize(parameters)
	  routines.stable2init(self.directory+"/temp.init",self.stable,self.wild_type.nb_species,self.topology,self.nb_cells)
	  init_file = self.directory+"/temp.init"
	full_params = t.f_convert(parameters)
	routines.generate_rk_param(self.directory+"/temp.rk",duration,steps)
	routines.generate_para(self.directory+"/parameters",full_params)
	routines.integrate(t.model_file,init_file,self.directory+"/temp.rk",self.directory+"/parameters",self.directory+"/num_out")
	if self.topology == 4 : os.popen("./cheat3d.py "+self.directory+"/num_out")
	#os.popen("../plot/bin/newman -d "+str(self.topology - 1)+" -min 0 -max 2 "+self.directory+"/num_out")
	os.popen("../plot/bin/newman -d "+str(self.topology - 1)+" "+self.directory+"/num_out")
	
  #takes an optimization file and a list of type names
  #default parameters simulates the set of parameters with the lowest cost
  #last_line = True simulates the last line of the file
  def test_file(self,names,file_in,duration=300,steps=300, last_line = False):
    chan = open(file_in,"r")
    lines = chan.readlines()
    chan.close()
    if last_line: pos = len(lines) - 1
    else: pos = routines.get_best_score_pos(lines)
    parameters = [float(x) for x in lines[pos][:-1].split("\t")[:-1]]
    self.test(names,parameters,duration,steps)
    
  #takes an optimization file or a file with parameters + cost
  #for all parameters with cost < cost_limit: computes the varation induced by the mutants(names) for the cost species
  #if sensitivity is None, output is measure variation (mutant concentration / wild concentration)
  #if sensitivity is (p,vp) -parameter value, parameter variation- output is (dm/dp)*(p/m) -m: concentration measure, dm: m variation
  #outputs mutants as they are ordered in the definition of the Pattern
  def compute_mutants(self,names,file_in,cost_limit, sensitivity=None):
    for n in names:
      if n not in [m.name for m in self.mutants]: raise Exception("Undefined mutant: "+n)
    chan = open(file_in,"r")
    lines = chan.readlines()
    chan.close()
    chan = open(self.directory+"/mutants","w")
    for l in lines:
      if l != "\n":
	line_out = ""
	s = l[:-1].split("\t")
	params, cost = [float(e) for e in s[:-1]], float(s[-1])
	if cost <= cost_limit:
	  for p in params: line_out += str(p) + "\t"
	  self.stabilize(params)
	  full_params = m.f_convert(params)
	  routines.stable2init(self.directory+"/temp.init",self.stable,self.wild_type.nb_species,self.topology,self.nb_cells)
	  
	  wild_m = [0 for e in self.wild_type.cost_species]
	  for l in self.stable:
	    if l != "\n":
	      s=l.split(" ")
	      for i,sp in enumerate(self.wild_type.cost_species):
		wild_m[i] = wild_m[i] + float(s[sp])
		
	  for m in self.mutants:
	    if m.name in names:
	      stable_mutant = self.stabilize_mutant(params,m)
	      mutant_m = [0 for e in self.wild_type.cost_species]
	      for l in stable_mutant:
		if l != "\n":
		  s=l.split(" ")
		  for i,sp in enumerate(self.wild_type.cost_species):
		    mutant_m[i] = mutant_m[i] + float(s[sp])
	      for i in range(0,len(self.wild_type.cost_species)):
		if m.sensitivity == None: line_out += str(mutant_m[i] / wild_m[i]) + "\t"
		else: 
		  p = full_params[m.sensitivity[0]]
		  vp = m.sensitivity[1]
		  line_out += str( ((mutant_m[i] - wild_m[i])/(p*vp - p)) * (p / wild_m[i]) ) + "\t"
	chan.write(line_out[:-1] + "\n")
	chan.flush()
    chan.close()
	      
