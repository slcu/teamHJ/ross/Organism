# -*- coding: utf-8 -*-
import random
import copy
random.seed()

fields = []
free_tokens = 0

saved_parameters = []
saved_state = []

def initiate(nb_tokens,nb_params):
  global free_tokens
  global fields
  free_tokens = nb_tokens - 2*nb_params
  fields = [1 for x in range(0,nb_params*2)]
  steps = [0.01 for x in range(0,nb_params)]

def next_move():
  global free_tokens
  global fields
  tokens = sum(fields)
  r = random.random()
  current_field = -1
  while (r > 0) and (current_field < (len(fields) - 1)):
    current_field +=1
    r = r - (float(fields[current_field]) / float(tokens))
  return ((current_field / 2, current_field % 2))

def good_move(last_move):
  #print ("GOOD" + str(last_move))
  global free_tokens
  global fields
  pos = 2*last_move[0] + last_move[1]
  if free_tokens != 0:
    fields[pos] += 1
    free_tokens += -1
  else :
    fields[fields.index(max(fields))] += -1
    fields[pos] += 1

def bad_move(last_move):
 # print ("BAD" + str(last_move))
  global free_tokens
  global fields
  pos = 2*last_move[0] + last_move[1]
  if fields[pos] > 1:
    fields[pos] += -1
    free_tokens += 1

# f(params) returns cost to minimize
# iter: amount of iterations
# step: fixed variation of the parameter at each step
# nb_tokens: size of the discretization of the probabilities
# limit: starts exploration if cost < limit, restarts optimization if cost > limit
# stop: stops the algorithm if cost < stop

def run(f,parameters,step=0.01,iters=5000,nb_tokens=100, limit=0, stop = -1):
  initiate(nb_tokens,len(parameters))
  best_diff = 9999999999
  best_para = parameters
  last_diff = 9999999999
  for i in range(0,iters):
    move = next_move()
    if move[1] == 1:
      parameters[move[0]] = parameters[move[0]] * (1-step)
    else :
      parameters[move[0]] = parameters[move[0]] * (1+step)
    diff = f(parameters)
    if diff > limit :
      if diff < last_diff:
        good_move(move)
      elif diff > last_diff :
        bad_move(move)
    last_diff = diff
    if diff < best_diff:
      best_diff = diff
      best_para = copy.copy(parameters)
    if diff < stop :
      return (diff,parameters)
    #print (i,best_diff)
  #print best_para
  #print best_diff
  return [best_para, best_diff]
