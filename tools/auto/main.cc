#include <cstdlib> 
#include <unistd.h> 
#include <fstream>
#include <iostream>
#include <cmath>
#include <sstream>
#include <string>
#include <vector>

#include <sys/stat.h>
#include <sys/types.h>
#include <errno.h>

typedef struct Variable
{
	std::string name;
	std::vector<double> values;	
	size_t currentIndex;
} Variable;

typedef struct Data
{
	std::vector<std::string> commands;
	std::vector<std::string> files;
	std::vector<std::string> symlinks;
	std::vector<Variable *> variables;
	std::string directory;
	bool override;
} Data;

void changeDirectory(std::string dir);
std::string double2string(double arg);
void makeSymlink(std::string path, std::string file);
void parseFile(std::ifstream &file, Data &data);
void printHelp(std::string exe);
void process(Data &data);
void startClockwork(Data &data);

int main(int argc, char *argv[])
{
	// Check command line arguments.
	if (argc != 2) {
		printHelp(argv[0]);
		exit(EXIT_SUCCESS);
	}

	// Open file.
	std::ifstream file(argv[1]);
	if (!file) {
		std::cout << "Error: Unable to open file " << argv[1] << " for reading." << std::endl;
		exit(EXIT_FAILURE);
	}

	// Create data object and set default values.
	Data data;
	data.override = false;
	data.directory = "autodata";

	// Parse config file.
	parseFile(file, data);


	// Create data directory.
	int err = mkdir(data.directory.c_str(), 0777);
	if (err) {
		if (errno != 17) {
			std::cerr << "Could not create data directory." << std::endl;
			exit(EXIT_FAILURE);
		}
	}

	startClockwork(data);

	// Exit program properly.
	exit(EXIT_SUCCESS);
}

void changeDirectory(std::string dir)
{
  if (chdir(dir.c_str())) {
    std::cerr << "Error: Unable to change directory." << std::endl;
    exit(EXIT_FAILURE);
  }
}

std::string double2string(double arg)
{
	std::stringstream tmpStream;
	std::string tmpString;
	tmpStream << arg;
	return tmpStream.str();
}

void makeSymlink(std::string path, std::string file)
{
	int err;
	std::string pathToSymlink = path;
	pathToSymlink.append("/");
	pathToSymlink.append(file);

	std::string oldPath = "../../";
	oldPath.append(file);

	err = symlink(oldPath.c_str(), pathToSymlink.c_str());
	if (err) {
		std::cerr << "Error: Unable to create symlink." << std::endl;
		exit(EXIT_FAILURE);
	}
}

void findAndReplace(std::string &source, std::string find, std::string replace)
{
	std::string::size_type position;
	while ((position = source.find(find, 0)) != std::string::npos)
		source.replace(position, find.length(), replace);
}

void parseFile(std::ifstream &file, Data &data)
{
	std::string tmp;
	while (!file.eof()) {
		file >> tmp;

		if (tmp == "FILE") {
			file >> tmp;
			data.files.push_back(tmp);
		}
		
		if (tmp == "SYMLINK") {
			file >> tmp;
			data.symlinks.push_back(tmp);
		}

		if (tmp == "DIRECTORY") {
			file >> data.directory;
		}

		if (tmp == "COMMAND") {
			std::string line;
			getline(file, line);
			data.commands.push_back(line);
		}

		if (tmp == "OVERRIDE") {
			data.override = true;
		}

		if (tmp == "VAR") {
			Variable *variable = new Variable;
			variable->currentIndex = 0;
			file >> variable->name;
			file >> tmp;
			if (tmp == "DISCRETE") {
				std::string line;
				getline(file, line);
				std::stringstream stream(line);
				while (!stream.eof()) {
					stream >> tmp;
					if (!tmp.empty()) 
						variable->values.push_back(atof(tmp.c_str()));
				}
			} else if (tmp == "FROM") {
				double from, to, steps;
				file >> from;

				file >> tmp;
				if (tmp != "TO") {
					std::cerr << "Syntax error in configuration file (expected TO)." << std::endl;
					exit(EXIT_FAILURE);
				} else {
					file >> to;
				}

				file >> tmp;
				if (tmp != "STEPS") {
					std::cerr << "Syntax error in configuration file (expected STEPS)." << std::endl;
					exit(EXIT_FAILURE);
				} else {
					file >> steps;
				}
				
				file >> tmp;
				if (tmp == "LINEAR") {
					double stepSize = (to - from) / (steps - 1);
					for (double value = from; value < to  +(stepSize / 2); value += stepSize)
						variable->values.push_back(value);
				} else if (tmp == "LOG10") {
					double stepSize = (log(to) - log(from)) / (steps - 1);
					for (double value = log(from); value < log(to) + (stepSize / 2); value += stepSize)
						variable->values.push_back(exp(value));
				}				
			}
			data.variables.push_back(variable);
		}
	}
	if (data.commands.empty()) {
		std::cerr << "Error: Configure file lack command COMMAND." << std::endl;
		exit(EXIT_FAILURE);
	}
}

void process(Data &data)
{
	// Create prefix
	std::stringstream prefix;
	prefix << data.directory << "/";
	std::vector<Variable *>::iterator v;
	for (v = data.variables.begin(); v != data.variables.end(); ++v) {
		if (v != data.variables.begin()) 
			prefix << "_";
		prefix << (*v)->name << "=" << (*v)->values[(*v)->currentIndex];
	}

	// Create working directory
	std::string tmpPrefix = prefix.str();
	int err = mkdir(tmpPrefix.c_str(), 0777);
	if (err) {
		if (errno == EEXIST) {
			if (data.override == false) 
				return;
		}
		else {
			std::cerr << "Error: Could not open directory " << tmpPrefix.c_str() << std::endl;
			exit(EXIT_FAILURE);
		}
	}

	// Process files.
	std::vector<std::string>::iterator f;		
	for (f = data.files.begin(); f != data.files.end(); f++) {
		std::ifstream ifile;
		std::ofstream ofile;
		std::string targetFileName;
		std::string tmp;
		std::stringstream tmpStream;

		// Open source file for reading.
		ifile.open(f->c_str());
		if (!ifile) {
			std::cerr << "Error: Could not open target file " << f->c_str() << std::endl;
			exit(EXIT_FAILURE);
		}

		// Read source file and process text.
		while (!ifile.eof()) {
			getline(ifile, tmp);

			// Bad design.
			if (tmp == "")
				continue;

			// Loop over variables and "find and replace" with their current values.
			std::vector<Variable *>::iterator v;
			for (v = data.variables.begin(); v != data.variables.end(); ++v) 
				findAndReplace(tmp, (*v)->name, double2string((*v)->values[(*v)->currentIndex]));
			tmpStream << tmp << std::endl;
		}
		ifile.close();

		// Open target file for writing.
		targetFileName = prefix.str();
		targetFileName.append("/");
		targetFileName.append(f->c_str());

		ofile.open(targetFileName.c_str());
		if (!ofile) {
			std::cerr << "Error: Could not open source file for output." << std::endl;
			exit(EXIT_FAILURE);
		}
		ofile << tmpStream.str();
		ofile.close();
	}

	// Make symlinks.
	std::vector<std::string>::iterator s;
	for (s = data.symlinks.begin(); s != data.symlinks.end(); s++) 
		makeSymlink(prefix.str(), *s);

		
	changeDirectory(prefix.str());
	for(std::vector<std::string>::const_iterator it = data.commands.begin(); it!=data.commands.end(); ++it) {
		// 
		// Parse commands.
		//
		std::string tmpCommand;
       		tmpCommand = *it;
		for (v = data.variables.begin(); v != data.variables.end(); v++)
			findAndReplace(tmpCommand, (*v)->name, double2string((*v)->values[(*v)->currentIndex]));

		//
		// Execute commands.
		//
		if (system(tmpCommand.c_str())) {
			std::cerr << "Error: System call returned non-zero value." << std::endl;
			exit(EXIT_FAILURE);
		}
	}
	changeDirectory("../../");
}

void printHelp(std::string exe)
{
	std::cout << "Usage: " << exe << " <config>." << std::endl;
}

void startClockwork(Data &data)
{
	while (true) {
		process(data);
		std::vector<Variable *> variables = data.variables;
		for (size_t l = 0; l < variables.size(); ++l) {
			++variables[l]->currentIndex;
			if (variables[l]->currentIndex == variables[l]->values.size()) {
				if (l != variables.size() - 1)
					variables[l]->currentIndex = 0;
				else
					return;
			} else
				break;					
		}
	}
}



