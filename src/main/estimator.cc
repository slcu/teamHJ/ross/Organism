#include "../optimizers/baseOptimizer.h"
#include "../solvers/baseSolver.h"
#include "../common/myConfig.h"
#include "../common/mySignal.h"
#include "../common/myRandom.h"

int main(int argc,char *argv[]) {

  myConfig::registerOption("verbose", 1);
  myConfig::registerOption("debug_output", 1);
  
  if((argc < 4) && (argc != 2)) {
    std::cerr << "possible usages: 1:" << argv[0] << " modelFile " << "simulatorParaFile analyzerFile\n";
    std::cerr << "                 2:" << argv[0] << " (model + simulator + analyser)File  [i.e. everything consecutively in a big file]\n";
      exit(-1);
  }
  if(argc > 4){
	  std::cerr << argv[0] << "Is normally used with 3 file names. Ignoring the " << 4 - argc << " other files\n";
  }
	
  if(argc == 4){
	char* modelFile=argv[1];
	std::string simPara=argv[2];
	std::string analyzerFile = argv[3];
	
	myRandom::Randomize();
	myTimes::getTime();
	
	//
	// Define the organism (model), simulator and the optimizer
	//	
	Organism O(modelFile);
	std::cerr << "Organism created from model " << modelFile << "." << std::endl;
		
	BaseSolver *S = BaseSolver::getSolver(&O, simPara);
	std::cerr << "Solver created from file " << simPara << "." << std::endl;
		
	O.initiateParameter();
	
	BaseEstimator *Estimator = new BaseEstimator(S, analyzerFile);
	std::cerr << "Estimator created from file " << analyzerFile << "." << std::endl;
	

	//Add optimizer to signal handler.
	//mySignal::addOptimizer(Optimizer);
		
	//Start optimizing  
	std::cerr << "cost = " << Estimator->getCost()<< "\n";
	
	//Delete simulator and optimizer
	delete S;
	delete Estimator;
  }
  if(argc == 2){
	char* totalFile=argv[1];
	  
	myRandom::Randomize();
	myTimes::getTime();	  
	  
	std::istream *IN = myFiles::openFile(totalFile);
	if (!IN) {
		std::cerr << "./estimator - main : "
		<< "Cannot open (model-sim-est)file " << totalFile << std::endl;
		exit(-1);
	}
	
	Organism O((std::ifstream &) *IN);
	std::cerr << "Organism created from a first part of file " << totalFile << "." << std::endl;
		
	BaseSolver *S = BaseSolver::getSolver(&O, (std::ifstream &) *IN);
	std::cerr << "Solver created from a second part of file " << totalFile << "." << std::endl;	
	
	O.initiateParameter();
	
	BaseEstimator *Estimator = new BaseEstimator(S,  (std::ifstream &) *IN);
	std::cerr << "Estimator created from a third and last part of file " << totalFile << "." << std::endl;	
	delete IN;

	std::cerr << "cost = " << Estimator->getCost()<< "\n";
	
	//Delete simulator and optimizer
	delete S;
	delete Estimator;
	  
  }
  return 0;
}
