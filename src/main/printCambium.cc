#include <iostream>
#include <vector>

#include "../common/myConfig.h"
#include "../organism.h"

int main(int argc,char *argv[]) 
{
  //Handle command line arguments
  std::vector<std::string> commandLineArguments;
  myConfig::registerOption("verbose", 1);	
  
  std::string configFile(getenv("HOME"));
  configFile.append("/.organism");
  myConfig::initConfig(argc, argv, configFile);
  
  if (myConfig::argc() != 2) {
    std::cerr << std::endl << "usage:" << argv[0] << " modelFile" 
	      << std::endl; 		
    exit(-1);
  }
  std::string modelFile = myConfig::argv(1);
  
  // Define the organism (model)
  Organism O(modelFile,"");
  
  // Print model in Cambium format
  O.printModelCambium(std::cout);
}
