#include "../optimizers/baseOptimizer.h"
#include "optimizationProblems/problems.h"
#include "../common/myConfig.h"
#include "../common/mySignal.h"
#include "../common/myRandom.h"

int main(int argc,char *argv[]) {
	if((argc != 2)) {
		std::cerr << "Usage " << argv[0] << " otimizer file \n";
		exit(-1);
	}
	std::cerr << "Waza\n";
	std::string optimizerFile = argv[1];
	myRandom::Randomize();
	myTimes::getTime();

	BaseOptimizationProblem *E = new SphereOpt(45);

	BaseOptimizer *Opt = BaseOptimizer::createOptimizer(E, optimizerFile);
	//Add optimizer to signal handler.
	mySignal::addOptimizer(Opt);

	//Start optimizing  
	Opt->optimize();

	//Delete simulator and optimizer
	delete E;
	delete Opt; 
}
