#include "../optimizers/baseOptimizer.h"
#include "../cost/baseEstimator.h"
#include "../solvers/baseSolver.h"
#include "../common/myConfig.h"
#include "../common/mySignal.h"
#include "../common/myRandom.h"

int main(int argc,char *argv[]) {
  
  myConfig::registerOption("verbose", 1);
  myConfig::registerOption("debug_output", 1);
	myConfig::registerOption("all_init_from_previous", 0);
	myConfig::registerOption("first_init_from_previous", 0);
	myConfig::registerOption("help", 0);
	myConfig::registerOption("h", 0);

	std::string configFile(getenv("HOME"));
	configFile.append("/.organism");
	myConfig::initConfig(argc, argv, configFile);
	bool all_init_from_previous = myConfig::getBooleanValue("all_init_from_previous");
	bool first_init_from_previous = myConfig::getBooleanValue("first_init_from_previous");
  
	if( myConfig::getBooleanValue("help") || myConfig::getBooleanValue("h") ) {
		std::cerr << "optimizer modelFile solverFile estimatorFile optimizerFile [options]"
			<<std::endl<<
			"Options:" << std::endl <<
			"-h | -help \t \t Print this help." << std::endl <<
			"-all_init_from_previous \t replace all the init values with the final" << std::endl <<
			"\t \t \t values of the simulation made to evaluate the previous" << std::endl <<
			"\t \t \t parameter update attempt. Beware of this option for" << std::endl <<
		  "\t \t \t potentially multistable models." << std::endl <<
			"-first_init_from_previous \t like the above, but only applied to the" << std::endl <<
			"\t \t \t first init file specified in the estimator file." << std::endl << 
			"\n" <<
			"The result is printed to shell and should preferably be stored as a file" << std::endl <<
			"by redirection using the '>' operator. '>' redirects a list of the attained"<<  std::endl <<
			"parameters, while '2>' redirects a log of the procedure." <<  std::endl <<
			"\n" <<
			"example command:" << std::endl <<
		  "optimizer file.model file.rkT file.est file.opt > result.data 2> result.log" <<  std::endl;
		exit(EXIT_FAILURE);
	}
  
  if((myConfig::argc() !=5) && (myConfig::argc() != 2) && (myConfig::argc() != 4)) {
    std::cerr << "possible usages: 1: " << myConfig::argv(0) << " modelFile " 
	      << "simulatorParaFile analyzerFile optimizerFile\n";
    std::cerr << "                 2: " << myConfig::argv(0) 
	      << " (model + solver + analyzer + optimizer)File "
	      << "[i.e. everything consecutively in the same file}\n";
    std::cerr << "  (old usage )   3: " << myConfig::argv(0) << " modelFile " 
	      << "simulatorParaFile (analyzer+optimizer)File\n";
    exit(-1);
  }

	if ( all_init_from_previous )
		std::cerr << "Replacing all of the specified init values with the final values " 
			<< "from the previous parameter update attempt."
		 	<< std::endl;
	if ( first_init_from_previous )
		std::cerr << "Replacing the first specified init files with the final values " 
			<< "from the previous parameter update attempt."
		 	<< std::endl;

	std::cerr << "PID for optimization: " << ::getpid() << std::endl;
  if(myConfig::argc() == 5 ){
    //std::string modelFile=myConfig::argv(1);
		std::string modelFile=myConfig::argv(1);
    std::string simPara=myConfig::argv(2);
    std::string analyzerFile = myConfig::argv(3);
    std::string optimizerFile = myConfig::argv(4);
   
    myRandom::Randomize();
    myTimes::getTime();
    
    //
    // Define the organism (model), simulator and the optimizer
    //	
    Organism O(modelFile);
    std::cerr << "Organism created from model " << modelFile << "." << std::endl;
    
    BaseSolver *S = BaseSolver::getSolver(&O, simPara);
    std::cerr << "Solver created from file " << simPara << "." << std::endl;
    
    O.initiateParameter();
    
    BaseOptimizationProblem *E = new BaseEstimator(S, analyzerFile, all_init_from_previous, first_init_from_previous );
    
    BaseOptimizer *Opt = BaseOptimizer::createOptimizer(E, optimizerFile);
    //Add optimizer to signal handler.
    mySignal::addOptimizer(Opt);
    
    //Start optimizing  
    Opt->optimize();
    
    //Delete simulator and optimizer
    delete S;
    delete E;
    delete Opt;
  }
  
  if(myConfig::argc() == 4) {
    std::cerr << "!NOTE! *************************************************:" << std::endl
	      << " The syntax of the optimizer part has changed"
	      << ", you should now put :" << std::endl     
	      << "'IdOptimizer TypeOfScaling NbArgumentsForScaling ArgumentsForScaling NbArgumentsForOptimizer ...'"
	      << std::endl << "instead of only 'IdOptimizer NumArgsForOptimizer' ... as before." << std::endl
	      << "Put 'IdOpt 0 0 nbArgsForOpt' ... to stay in non scaling mode as before." << std::endl
	      << "!NOTE! *************************************************:" << std::endl;   
    std::cerr << "Still some implementation lacking on this one yet...Contact P Robert :)." << std::endl;
    std::cerr << "Use instead 4 configuration files where " << std::endl
	      << "'NoScaling 0' is added after the 'Number of initial parameter sets to be read' in the opt file,"
	      << "and the rest of the old style opt file should be stored in an additioanl file (est)." << std::endl
	      << "Old fashioned 'optimizer model rk opt' then becomes 'optimizer model rk est opt." << std::endl
	      << "!NOTE! *************************************************:" << std::endl;
    exit(EXIT_FAILURE);

    std::string modelFile=myConfig::argv(1);
    std::string simPara=myConfig::argv(2);
    std::string estoptFile = myConfig::argv(3);
    
    myRandom::Randomize();
    myTimes::getTime();
    
    //
    // Define the organism (model), simulator and the optimizer
    //	
    Organism O(modelFile);
    std::cerr << "Organism created from model " << modelFile << "." << std::endl;
    
    BaseSolver *S = BaseSolver::getSolver(&O, simPara);
    std::cerr << "Solver created from file " << simPara << "." << std::endl;
    
    O.initiateParameter();
    
    std::istream *IN = myFiles::openFile(estoptFile);
    if (!IN) {
      std::cerr << "./optimizer - main : "
		<< "Cannot open (analyzer-optimizer)file " << estoptFile << std::endl;
      exit(-1);
    }
    
    std::cerr << "Creating estimator..." << std::endl;
    BaseEstimator *E = new BaseEstimator(S, (std::ifstream &) *IN, all_init_from_previous, first_init_from_previous );
    std::cerr << "Estimator created." << std::endl;    

    std::cerr << "Creating optimazor..." << std::endl;
    BaseOptimizer *Opt = BaseOptimizer::createOptimizer(E, (std::ifstream&) *IN);
    //Add optimizer to signal handler.
    mySignal::addOptimizer(Opt);
    std::cerr << "Optimazor created." << std::endl;    
    
    //Start optimizing  
    Opt->optimize();
    
    //Delete simulator and optimizer
    delete S;
    delete E;
    delete Opt;	  
  }
  
  if(myConfig::argc() == 2){
		std::string totalFile=myConfig::argv(1);
    
    std::istream *IN = myFiles::openFile(totalFile);
    if (!IN) {
      std::cerr << "./optimizer - main : "
		<< "Cannot open (analyzer-optimizer)file " << totalFile << std::endl;
      exit(-1);
    } 
    
    myRandom::Randomize();
    myTimes::getTime();
    
    Organism O((std::ifstream&) *IN);
    std::cerr << "Organism created from a first part of file " << totalFile << "." << std::endl;
    
    BaseSolver *S = BaseSolver::getSolver(&O, (std::ifstream&) *IN);
    std::cerr << "Solver created from a second part of file " << totalFile << "." << std::endl;
    
    O.initiateParameter();
    
    BaseEstimator *E = new BaseEstimator(S, (std::ifstream&) *IN, all_init_from_previous, first_init_from_previous );
    std::cerr << "Estimator created from a third part of file " << totalFile << "." << std::endl;
    
    
    BaseOptimizer *Opt = BaseOptimizer::createOptimizer(E, (std::ifstream&) *IN);
    std::cerr << "Estimator created from a fourth and last part of file " 
	      << totalFile << "." << std::endl;
    delete IN;
    //Add optimizer to signal handler.
    mySignal::addOptimizer(Opt);
    
    //Start optimizing  
    Opt->optimize();
    
    //Delete simulator and optimizer
    delete S;
    delete E;
    delete Opt;
  }
  
  return 0;
}
