#include <iostream>
#include <vector>

#include "../common/myConfig.h"
#include "../organism.h"

int main(int argc,char *argv[]) 
{
	//Handle command line arguments
	std::vector<std::string> commandLineArguments;
	myConfig::registerOption("verbose", 1);	

	std::string configFile(getenv("HOME"));
	configFile.append("/.organism");
	myConfig::initConfig(argc, argv, configFile);
	
	if (myConfig::argc() != 2) {
		std::cerr << std::endl << "usage:" << argv[0] << " modelFile" 
							<< std::endl; 		
    exit(-1);
  }
	std::string modelFile = myConfig::argv(1);
	
	int verboseFlag=0;
	std::string verboseString;
	verboseString = myConfig::getValue("verbose", 0);
	if( !verboseString.empty() ) {
		verboseFlag = atoi( verboseString.c_str() );
		if( verboseFlag != 0 && verboseFlag !=1 ) {
			verboseFlag=0;
			std::cerr << "Flag given to -verbose not recognized (0, 1 allowed)."
								<< " Setting it to zero (silent)." << std::endl;
		}
	}
	
  // Define the organism (model)
  Organism O(modelFile,"",verboseFlag);
	
	// Initiate parameter vector
	O.initiateParameter();
	
	// Print the model to standard error if applicable
	if( verboseFlag )
		std::cout << "# Model parameters extracted from file " << modelFile
							<< std::endl << "# Model name " << O.id() << std::endl;
	O.printParameter(std::cout);
}
