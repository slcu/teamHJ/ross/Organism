 
#include "../../cost/baseProblem.h"
#define pi() 3.1415927

class GeneralImplementation : public BaseOptimizationProblem {
	protected:
		vector<double> parameters;
		int dimension;
	public:
		
		GeneralImplementation(int dim) : BaseOptimizationProblem(){
			dimension = dim;
			parameters.resize(dim, 0);
		}
		virtual double getCost(){
			return 0;
		}
		inline void setParameter(size_t i, double value){
			parameters[i] = value;
		}
		inline double parameter(size_t i) const{
			return parameters[i];
		}
		inline size_t numPara() const{
			return dimension;
		}
		void printState(double E, std::ostream &os = std::cout) const{
			os << "E= " << E;
			for(int i = 0; i < dimension; ++i){
				os << "\t" << parameters[i];
			}
			os << "\n";
		}
};

class SphereOpt : public GeneralImplementation {
	public:
		SphereOpt(int n) : GeneralImplementation(n) {};
		double getCost(){
			double res = 0;
			for(int i = 0; i < dimension; ++i){
				res += parameters[i] * parameters[i];
			}
			return res;
		}
};

class Schwefel : public GeneralImplementation {
	public:
	Schwefel(int n) : GeneralImplementation(n) {};
	double getCost(){
		double res = 0;
		for(int i = 0; i < dimension; ++i){
			double resintern = 0;
			for(int j = 0; j <= i; ++j){
				resintern += parameters[j];
			}
			res += resintern * resintern;
		}
		return res;
	}
};

double mysquare(double x){
	return (x * x);
}

class Rosenbrock : public GeneralImplementation {
	public:
	Rosenbrock(int n) : GeneralImplementation(n) {};
	double getCost(){
		double res = 0;
		for(int i = 0; i < dimension-1; ++i){
			res += 100*mysquare(parameters[i+1] - (parameters[i] * parameters[i])) + mysquare(parameters[i] -1);
		}
		return res;
	}
};

class Rastrigin : public GeneralImplementation {
	public:
	Rastrigin(int n) : GeneralImplementation(n) {};
	double getCost(){
		double res = 10*dimension;
		for(int i = 0; i < dimension; ++i){
			res += parameters[i] * parameters[i] - 10*cos(2*pi()*parameters[i]);;
		}
		return res;
	}
};

