//
// Filename     : Vector3.cc
// Description  : Defining a class for standard linear algebra operations
// Author(s)    : Pontus Melke (pontus@thep.lu.se)
// Created      : 
// Revision     : $Id: vector3.cc 474 2010-08-30 15:23:20Z henrik $
//

#include <cstdlib>
#include "vector3.h"
using myVector3::Vector3;

Vector3::Vector3(const Vector3 & p)
	:x_(p.x()), y_(p.y()), z_(p.z()) {}

namespace myVector3 {
	double dot(const Vector3& u, const Vector3& v) 
	{
		return u.x_*v.x_ + u.y_*v.y_ + u.z_*v.z_;
	}
	Vector3 cross(const Vector3& u,const Vector3& v)
	{
		return Vector3(u.y_*v.z_-u.z_*v.y_,u.z_*v.x_-u.x_*v.z_,u.x_*v.y_-u.y_*v.x_);
	}
}

		
void Vector3::setVector3(size_t index, double value)
{
  switch (index) {
  case 0:
    x_=value;
    break;
  case 1:
    y_=value;
    break;
  case 2:
    z_=value;
    break;
  default:
    std::cerr << index << " is out of scope\n";
    exit(-1);
  }
}

double myVector3::Vector3::operator() (size_t i)
{
  switch(i) {
  case 0:
    return x_;
  case 1:
    return y_;
  case 2:
    return z_;
  default:
    std::cerr << i << "is out of scope\n";
    exit(-1);
  }
}

Vector3 myVector3::operator+(const  Vector3& u, const  Vector3& v)
{
	Vector3 w=u;
	return w+=v;
}

Vector3 myVector3::operator+(const Vector3& u, const double a)
{
	Vector3 w=u;
	return w+=a;
}
Vector3 myVector3::operator+(const double a, const Vector3& u)
{
	Vector3 w=u;
	return w+=a;
}
Vector3 myVector3::operator-(const Vector3& u, const Vector3& v) 
{
	Vector3 w=u;
	return w-=v;
}
Vector3 myVector3::operator-(const Vector3& u, const double a)
{
	Vector3 w=u;
	return w-=a;
}
Vector3 myVector3::operator-(const double a, const Vector3& u)
{
	Vector3 w=u;
	return w-=a;
}

Vector3 myVector3::operator*(const Vector3& u, double a)
{
	Vector3 w=u;
	return w*=a;
}
Vector3 myVector3::operator*(double a, const Vector3& u)
{
	Vector3 w=u;
	return w*=a;
}

double  Vector3::phi() const 
{
	return x() == 0.0 && y()==0.0 ?
		0.0 : std::atan2(y(),x());
}

double  Vector3::theta() const 
{
	return x() == 0.0 && y()==0.0 && z() == 0.0 ?
		0.0 : std::atan2(std::sqrt(x()*x()+y()*y()),z());
}

 



