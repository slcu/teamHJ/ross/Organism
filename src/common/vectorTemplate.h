#ifndef VECTORTEMPLATE_H
#define VECTORTEMPLATE_H


template <int T>
class Vector;

template <int T>
double dot(const Vector<T>& u, const Vector<T>& v);

//template <int T>
//Vector<T> cross(const Vector<T>& u, const Vector<T>& v);


template<int T>
class Vector {
 public:
	Vector() {}
	Vector& operator+=(const Vector& v);
	Vector& operator+=(const double a);
	Vector& operator+=(const int i);
	Vector& operator-=(const Vector& v);
	Vector& operator-=(const double a);
	Vector& operator-=(const int i);
	Vector& operator*=(const double a);
	
	const double& operator[] (int i) const {return array_[i];}
	double& operator[] ( int i) {return array_[i];}
	
	friend double dot <>(const Vector<T>& u, const Vector<T>& v);
	//friend Vector<3> cross(const Vector<3>& u, const Vector<3>& v);
	
	void fill(const double value);
	private:
	double array_[T];
};

Vector<3> cross(const Vector<3>& u, const Vector<3>& v);

template <int T> void Vector<T>::fill(const double value) 
{
	for (int i=0; i<T; ++i)
		array_[i]=value;
}

template <int T> Vector<T>& Vector<T>::operator+=(const Vector<T>& v)
{
	for (int i=0; i<T; ++i)
		array_[i] +=v[i];
	return *this;
}

template <int T> Vector<T>& Vector<T>::operator+=(const double d)
{
	for (int i=0; i<T; ++i)
		array_[i] +=d;
	return *this;
}

template <int T> Vector<T>& Vector<T>::operator+=(const int d)
{
	for (int i=0; i<T; ++i)
		array_[i] += d;
	return *this;
}


template <int T> Vector<T>& Vector<T>::operator-=(const Vector<T>& v)
{
	for (int i=0; i<T; ++i)
		array_[i] -=v[i];
	return *this;
}

template <int T> Vector<T>& Vector<T>::operator-=(const double d)
{
 	return *this+=(-d);
}

template <int T> Vector<T>& Vector<T>::operator-=(const int d)
{
	return *this+=(-d);
}

template <int T> Vector<T>& Vector<T>::operator*=(const double d)
{
	for (int i=0; i<T; ++i)
		array_[i] *=d;
	return *this;
}

template <int T> Vector<T> operator+(const  Vector<T>& u, const  Vector<T>& v)
{
	Vector<T> w=u;
	return w+=v;
}

template <int T> Vector<T> operator+(const  Vector<T>& u, const  double a)
{
	Vector<T> w=u;
	return w+=a;
}

template <int T> Vector<T> operator+(const  Vector<T>& u, const int a)
{
	Vector<T> w=u;
	return w+=a;
}

template <int T> Vector<T> operator+(const double a, const  Vector<T>& u)
{
	Vector<T> w=u;
	return w+=a;
}

template <int T> Vector<T> operator+(const int a, const  Vector<T>& u)
{
	Vector<T> w=u;
	return w+=a;
}

template <int T> Vector<T> operator-(const  Vector<T>& u, const  Vector<T>& v)
{
	Vector<T> w=u;
	return w-=v;
}

template <int T> Vector<T> operator-(const  Vector<T>& u, const  double a)
{
	Vector<T> w=u;
	return w-=a;
}

template <int T> Vector<T> operator-(const  Vector<T>& u, const int a)
{
	Vector<T> w=u;
	return w-=a;
}

template <int T> Vector<T> operator-(const double a, const  Vector<T>& u)
{
	Vector<T> w=u;
	return w-=a;
}

template <int T> Vector<T> operator-(const int a, const  Vector<T>& u)
{
	Vector<T> w=u;
	return w-=a;
}

template <int T> Vector<T> operator*(const double a, const  Vector<T>& u)
{
	Vector<T> w=u;
	return w*=a;
}

template <int T> Vector<T> operator*(const  Vector<T>& u,const double a)
{
	Vector<T> w=u;
	return w*=a;
}

template <int T> double dot(const Vector<T>& u, const Vector<T>& v) 
{
	double tmp=0;
	for (int i = 0; i < T; ++i)
		tmp += u.array_[i]*v.array_[i];
	return tmp;
}


/*template <> Vector<3> cross<3>(const Vector<3>& u,const Vector<3>& v)
{
	Vector<3> w;
		w.array_[0] = u.array_[1]*v.array_[2] - u.array_[2]*v.array_[1];
		w.array_[1] = u.array_[2]*v.array_[0] - u.array_[0]*v.array_[2];
		w.array_[2] = u.array_[0]*v.array_[1] - u.array_[1]*v.array_[0];
		return w;
}*/

/*Vector<3> cross(const Vector<3>& u,const Vector<3>& v)
{
	Vector<3> w;
		w[0] = u[1]*v[2] - u[2]*v[1];
		w[1] = u[2]*v[0] - u[0]*v[2];
		w[2] = u[0]*v[1] - u[1]*v[0];
		return w;
}*/

#endif //VECTORTEMPLATE_H
