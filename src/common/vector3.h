//
// Filename     : Vector3.h
// Description  : Defining a class for a three-dimensional vector
// Author(s)    : Pontus Melke (pontus@thep.lu.se)
// Created      : January 2007
// Revision     : $Id: vector3.h 478 2011-02-21 08:34:45Z henrik $
//
#ifndef VECTOR3_H
#define VECTOR3_H
#include <iostream>
#include <cmath>

///
/// @brief myVector3 is a namespace where a three-dimensional vector class,
/// Vector3, is defined.
///
namespace myVector3 {
  ///
  /// This Vector3 class defines all basic vector operations, such as addition,
  /// multiplication, scalar product and vector product.
  ///
  class Vector3 {
  public:
    ///
    /// @brief Constructor, by default all coordinates are set to zero
    ///
  Vector3(double x=0, double y=0, double z=0) :
    x_(x),y_(y), z_(z){}
    
    ///
    /// @brief copy constructor, copies x,y,z of input to the vector
    ///
    Vector3(const Vector3 &);
    
    ///
    /// @brief return x-coordinate
    ///
    double x() const {return x_;}
    
    ///
    /// @brief return x-coordinate
    ///
    double y() const {return y_;}
    
    ///
    /// @brief return x-coordinate
    ///
    double z() const {return z_;}
    
    /// 
    /// @brief returns polar angle
    ///
    double phi() const;
    
    /// 
    /// @brief returns azimuthal angle
    ///
    double theta() const;
        
    ///
    /// @brief set vector to x, y, z
    ///
    inline void setVector3(double x, double y, double z);
    
    ///
    /// @brief set index @f$ index \in [0:2]@f$, corresponding to X, Y, Z, to
    /// @f$ value@f$
    ///
    void setVector3(size_t index, double value);
    
    ///
    /// @brief Defines += operator for vectors
    ///
    inline Vector3& operator+=(const Vector3& v);
    
    ///
    /// @brief Defines += operator for double
    ///
    inline Vector3& operator+=(double a);
    
    ///
    /// @brief Defines -= operator for vectors
    ///
    inline Vector3& operator-=(const Vector3& v);
    
    ///
    /// @brief Defines -= operator for double
    ///
    inline Vector3& operator-=(double a);
    
    ///
    /// @brief Defines *= operator for double
    ///
    inline Vector3& operator*=(double a);
    
    ///
    /// @brief Allow you to write vector(i), for the i:th coordinate,
    /// @f$i\in[0:2]@f$
    ///
    double operator() (const size_t i);
    
    ///
    /// @brief Returns scalar product of two vectors
    ///
    friend double dot(const Vector3& u, const Vector3& v);
    
    ///
    /// @brief Returns vector product of two vectors
    ///
    friend Vector3 cross (const Vector3& u, const Vector3& v);  
    
  private:
    double x_, y_, z_;
  };
  
  ///
  /// @brief Defines + in terms of +=
  ///
  Vector3 operator+(const Vector3& u, const Vector3& v);
  
  ///
  /// @brief Defines + in terms of +=
  ///
  Vector3 operator+(const Vector3& u, double a);
  
  ///
  /// @brief Defines + in terms of +=
  ///
  Vector3 operator+(double a, const Vector3& u);
  
  ///
  /// @brief Defines - in terms of -=
  ///
  Vector3 operator-(const Vector3& u, const Vector3& v);
  
  ///
  /// @brief Defines - in terms of -=
  ///
  Vector3 operator-(const Vector3& u, double a);
  
  ///
  /// @brief Defines - in terms of -=
  ///
  Vector3 operator-(double a, const Vector3& u);
  
  ///
  /// @brief Defines * in terms of '=
  ///
  Vector3 operator*(const Vector3& u, double a);
  
  ///
  /// @brief Defines * in terms of '=
  ///
  Vector3 operator*(double a, const Vector3& u);
  
    
  //inline fucntion definitions
  inline void Vector3::setVector3(double x, double y, double z)
  {
    x_=x;
    y_=y;
    z_=z;
  }
  inline Vector3& Vector3::operator+=(const Vector3& v)
  {
    x_+=v.x_;
    y_+=v.y_;
    z_+=v.z_;
    return *this;
  }
  
  inline Vector3& Vector3::operator+=(double a)
  {
    x_+=a;
    y_+=a;
    z_+=a;
    return *this;
  }
  inline Vector3& Vector3::operator-=(const Vector3& v)
  {
    x_-=v.x_;
    y_-=v.y_;
    z_-=v.z_;
    return *this;
  }
  inline Vector3& Vector3::operator-=(double a)
  {
    x_-=a;
    y_-=a;
    z_-=a;
    return *this;
  }
  inline Vector3& Vector3::operator*=(double a)
  {
    x_*=a;
    y_*=a;
    z_*=a;
    return *this;
  }
}

#endif
