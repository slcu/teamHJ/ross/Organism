// Copyright (C) 2006 Garth N. Wells.
// Licensed under the GNU GPL Version 2.
//
// Modified by Anders Logg 2006.
// Modified by Pawel Krupinski 2007

#include <boost/numeric/ublas/vector.hpp>
#include "uBlasVector.h"
//#include "utils/error_macros.h"

//-----------------------------------------------------------------------------
uBlasVector::uBlasVector()
  : /*GenericVector(),*/
    ublas_vector()
{
  //Do nothing
}
//-----------------------------------------------------------------------------
uBlasVector::uBlasVector(size_t N)
  : /*GenericVector(),*/
    ublas_vector(N)
{
  // Clear matrix (not done by ublas)
  clear();
}
//-----------------------------------------------------------------------------
uBlasVector::~uBlasVector()
{
  //Do nothing
}
//-----------------------------------------------------------------------------
void uBlasVector::init(size_t N)
{
  if( this->size() == N)
  {
    //clear();
    return;
  }

  this->resize(N, false);
  clear();
}
//-----------------------------------------------------------------------------
double uBlasVector::norm(NormType type) const
{
  switch (type) {
  case l1:
    return norm_1(*this);
  case l2:
    return norm_2(*this);
  case linf:
    return norm_inf(*this);
  default:
    std::cerr << "Requested vector norm type for uBlasVector unknown" << std::endl;
    exit(-1);
    //cell_error("Requested vector norm type for uBlasVector unknown");
  }
  return norm_inf(*this);
}
//-----------------------------------------------------------------------------
//double uBlasVector::sum() const
//{
//  //return sum();
//}
//-----------------------------------------------------------------------------
void uBlasVector::apply()
{
  // Do nothing
}
//-----------------------------------------------------------------------------
void uBlasVector::zero()
{
  clear();
}
//-----------------------------------------------------------------------------
void uBlasVector::set(const double block[], const size_t pos[], int n)
{
	set<const double*>(block, pos, n);
// 	for(int i = 0; i < n; ++i)
// 		(*this)(pos[i]) = block[i];
}
//-----------------------------------------------------------------------------
 void uBlasVector::add(const double block[], const size_t pos[], int n)
{
	add<const double*>(block, pos, n);
// 	for(int i = 0; i < n; ++i)
// 		(*this)(pos[i]) += block[i];
}
//-----------------------------------------------------------------------------
void uBlasVector::div(const uBlasVector& y)
{
  uBlasVector& x = *this;
  size_t s = size();

  for(size_t i = 0; i < s; i++)
  {
    x[i] = x[i] / y[i];
  }
}
//-----------------------------------------------------------------------------
const uBlasVector& uBlasVector::operator= (double a) 
{ 
  this->assign(ublas::scalar_vector<double> (this->size(), a));
  return *this;
}
//-----------------------------------------------------------------------------
void uBlasVector::disp(size_t precision) const
{
  std::cout << "[ ";
  for (size_t i = 0; i < size(); i++)
	  std::cout << (*this)(i) << " ";
  std::cout << "]" << std::endl;
}
//-----------------------------------------------------------------------------
void uBlasVector::write(std::ofstream & o) const
{
  if ( !o.good() ) {
    std::cerr << "Unusable output stream." << std::endl;
    exit(-1);
    //cell_error( "Unusable output stream." );
  }
  o << size() << " ";
  for (size_t i = 0; i < size(); i++)
    o << (*this)(i) << " ";
  o << std::endl;
}
//-----------------------------------------------------------------------------
void uBlasVector::read(std::ifstream & in )
{
  if ( !in.good() ) {
    std::cerr << "Unusable input stream." << std::endl;
    exit(-1);
    //cell_error( "Unusable input stream." );
  }
  int size;
  in >> size;
  init(size);
  
  for (int i = 0; i < size; i++)
    in >> (*this)(i);
}
//-----------------------------------------------------------------------------
void uBlasVector::copy(const uBlasVector& y)
{
  *(this) = y;
}
//-----------------------------------------------------------------------------
