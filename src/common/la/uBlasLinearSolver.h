// Copyright (C) 2004-2006 Anders Logg.
// Licensed under the GNU GPL Version 2.
//
// Modified by Garth N. Wells, 2006.
// Modified by Pawel Krupinski 2007

#ifndef __UBLAS_LINEAR_SOLVER_H
#define __UBLAS_LINEAR_SOLVER_H

#include "ublas.h"


  /// Forward declarations
  class uBlasVector;  
  template<class Mat> class uBlasMatrix;

  /// This class defines the interfaces for uBlas-based linear solvers for
  /// systems of the form Ax = b.

  class uBlasLinearSolver
  {
  public:

    /// Constructor
    uBlasLinearSolver() {}

    /// Destructor
    virtual ~uBlasLinearSolver() {}

    /// Solve linear system Ax = b (A is dense)
    virtual size_t solve(const uBlasMatrix<ublas_dense_matrix>& A, uBlasVector& x, 
        const uBlasVector& b) = 0;

    /// Solve linear system Ax = b (A is sparse)
    virtual size_t solve(const uBlasMatrix<ublas_sparse_matrix>& A, uBlasVector& x, 
        const uBlasVector& b) = 0;

  };

#endif
