// Copyright (C) 2006 Anders Logg.
// Licensed under the GNU GPL Version 2.
//
// Modified by Pawel Krupinski 2007

#include "uBlasVector.h"
#include "uBlasDummyPreconditioner.h"


//-----------------------------------------------------------------------------
uBlasDummyPreconditioner::uBlasDummyPreconditioner() : uBlasPreconditioner()
{
  // Do nothing
}
//-----------------------------------------------------------------------------
uBlasDummyPreconditioner::~uBlasDummyPreconditioner()
{
  // Do nothing
}
//-----------------------------------------------------------------------------
void uBlasDummyPreconditioner::solve(uBlasVector& x, const uBlasVector& b) const
{
  x.assign(b);
}
//-----------------------------------------------------------------------------
