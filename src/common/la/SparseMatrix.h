// Copyright (C) 2006 Garth N. Wells
// Licensed under the GNU GPL Version 2.
//
// Modified by Pawel Krupinski 2007

#ifndef __SPARSE_MATRIX_H
#define __SPARSE_MATRIX_H

#include "uBlasSparseMatrix.h"


  typedef uBlasSparseMatrix SparseMatrix;


#endif
