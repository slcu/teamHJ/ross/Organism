// Copyright (C) 2006 Anders Logg.
// Licensed under the GNU GPL Version 2.
//
// Modified by Pawel Krupinski 2007

#include "LU.h"

//-----------------------------------------------------------------------------
void LU::solve(const uBlasMatrix<ublas_dense_matrix>& A, uBlasVector& x,
	       const uBlasVector& b)
{
  uBlasLUSolver solver;
  solver.solve(A, x, b);
}
//-----------------------------------------------------------------------------
void LU::solve(const uBlasMatrix<ublas_sparse_matrix>& A, uBlasVector& x,
	       const uBlasVector& b)
{
  uBlasLUSolver solver;
  solver.solve(A, x, b);
}
//-----------------------------------------------------------------------------
