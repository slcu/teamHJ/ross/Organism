// Copyright (C) 2006 Garth N. Wells
// Licensed under the GNU GPL Version 2.
//
// Modified by Anders Logg 2006.
// Modified by Pawel Krupinski 2007

#ifndef __DENSE_MATRIX_H
#define __DENSE_MATRIX_H

#include "la/uBlasDenseMatrix.h"

typedef uBlasDenseMatrix DenseMatrix;

#endif
