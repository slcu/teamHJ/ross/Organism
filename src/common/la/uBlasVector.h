// Copyright (C) 2006 Garth N. Wells
// Licensed under the GNU GPL Version 2.
//
// Modified by Anders Logg 2006.
// Modified by Pawel Krupinski 2007

#ifndef __UBLAS_VECTOR_H
#define __UBLAS_VECTOR_H

#include "ublas.h"
// #include "la/GenericVector.h"
#include <fstream>

//   namespace ublas = boost::numeric::ublas;
//   typedef ublas::vector<double> ublas_vector;

  /// This class represents a dense vector of dimension N.
  /// It is a simple wrapper for a Boost ublas vector.
  ///
  /// The interface is intentionally simple. For advanced usage,
  /// refer to the documentation for ublas which can be found at
  /// http://www.boost.org/libs/numeric/ublas/doc/index.htm.

  class uBlasVector : /*public GenericVector,*/ public ublas_vector
  {
  public:
    
    /// Constructor
    uBlasVector();
    
    /// Constructor
    uBlasVector(size_t N);
    
    /// Constructor from a uBlas vector_expression
    template <class E>
    uBlasVector(const ublas::vector_expression<E>& x) : ublas_vector(x) {}

    /// Destructor
    ~uBlasVector();

    /// Initialize a vector of length N
    void init(size_t N);

    /// Set all entries to a single scalar value
    const uBlasVector& operator= (double a);

    /// Assignment from a vector_expression
    template <class E>
    uBlasVector& operator=(const ublas::vector_expression<E>& A)
    { 
      ublas::vector<double>::operator=(A); 
      return *this;
    } 
    
    /// Return size
    inline size_t size() const
    { return ublas::vector<double>::size(); }

    /// Access given entry
    inline double& operator() (size_t i)
    { return ublas::vector<double>::operator() (i); };

    /// Access value of given entry
    inline double operator() (size_t i) const
    { return ublas::vector<double>::operator() (i); };

    /// Access element value
    inline double get(size_t i) const { return (*this)(i); }

    /// Set element value
    inline void set(size_t i, double value) { (*this)(i) = value; }
	
	/// Add element value
	inline void add(size_t i, double value) { (*this)(i) += value; }

	/// Set block of values
	void set(const double block[], const size_t pos[], int n);

    /// Add block of values
	void add(const double block[], const size_t pos[], int n);
	
    /// Set block of values
	template<class T>
    void set(const T block, const size_t pos[], int n);

    /// Add block of values
	template<class T>
    void add(const T block, const size_t pos[], int n);

    /// Get block of values
	template<class T>
    void get(T block, const size_t pos[], int n) const;

    /// Compute norm of vector
    enum NormType { l1, l2, linf };
    double norm(NormType type = l2) const;

    /// Compute sum of vector
    double sum() const;

    /// Apply changes to vector (dummy function for compatibility)
    void apply();

    /// Set all entries to zero
    void zero();
    
    /// Element-wise division
    void div(const uBlasVector& x);

    /// Display vector
    void disp(size_t precision = 2) const;

	/// Write vector to the file 
	void write(std::ofstream & o) const;
	
	/// read vector from the file
	void read(std::ifstream & i);
    // Copy values between different vector representations

    void copy(const uBlasVector& y);

  };

//-----------------------------------------------------------------------------
  template<class T>
  void uBlasVector::set(const T block, const size_t pos[], int n)
  {
	  for(int i = 0; i < n; ++i)
		  (*this)(pos[i]) = block[i];
  }
//-----------------------------------------------------------------------------
  template<class T>
  void uBlasVector::add(const T block, const size_t pos[], int n)
  {
	  for(int i = 0; i < n; ++i)
		  (*this)(pos[i]) += block[i];
  }
//-----------------------------------------------------------------------------
  template<class T>
  void uBlasVector::get(T block, const size_t pos[], int n) const
  {
	  for(int i = 0; i < n; ++i)
		  block[i] = (*this)(pos[i]);
  }
//-----------------------------------------------------------------------------
#endif
