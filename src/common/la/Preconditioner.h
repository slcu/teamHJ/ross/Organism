// Copyright (C) 2006 Anders Logg.
// Licensed under the GNU GPL Version 2.
//
// Modified by Pawel Krupinski 2007

#ifndef __PRECONDITIONER_H
#define __PRECONDITIONER_H

  // List of predefined preconditioners.

  enum Preconditioner
  {
    none,      // No preconditioning
    jacobi,    // Jacobi
    sor,       // SOR (successive over relaxation)
    ilu,       // Incomplete LU factorization
    icc,       // Incomplete Cholesky factorization
    amg,       // Algebraic multigrid (through Hypre when available)
    default_pc // Default choice of preconditioner
  };


#endif
