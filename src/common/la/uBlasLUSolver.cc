// Copyright (C) 2006 Garth N. Wells.
// Licensed under the GNU GPL Version 2.
//
// Modified by Anders Logg 2006.
// Modified by Pawel Krupinski 2007


#include "uBlasLUSolver.h"
//#include "la/uBlasKrylovSolver.h"
#include "uBlasSparseMatrix.h"
//#include "la/uBlasKrylovMatrix.h"


// extern "C" 
// {
// // Take care of different default locations
// #ifdef HAVE_UMFPACK_H
//   #include <umfpack.h>
// #elif HAVE_UMFPACK_UMFPACK_H
//   #include <umfpack/umfpack.h>
// #elif HAVE_UFSPARSE_UMFPACK_H
//   #include <ufsparse/umfpack.h>
// #endif
// }


//-----------------------------------------------------------------------------
uBlasLUSolver::uBlasLUSolver() : uBlasLinearSolver(), m_pmatrix(0)
{
  // Do nothing
}
//-----------------------------------------------------------------------------
uBlasLUSolver::~uBlasLUSolver()
{
	delete m_pmatrix;
}
//-----------------------------------------------------------------------------
size_t uBlasLUSolver::solve(const uBlasMatrix<ublas_dense_matrix>& A, uBlasVector& x, const uBlasVector& b)
{    
  // Make copy of matrix and vector
  ublas_dense_matrix Atemp(A);
  x.resize(b.size());
  x.assign(b);

  // Solve
  return solveInPlace(Atemp, x);
}
//-----------------------------------------------------------------------------
#if defined(HAVE_UMFPACK_H)|| defined(HAVE_UMFPACK_UMFPACK_H) || defined(HAVE_UFSPARSE_UMFPACK_H)
size_t uBlasLUSolver::solve(const uBlasMatrix<ublas_sparse_matrix>& A, uBlasVector& x, 
    const uBlasVector& b)
{
  // Check dimensions and get number of non-zeroes
  const size_t M  = A.size(0);
  const size_t N  = A.size(1);
  const size_t nz = A.nnz();

  cell_assert(M == A.size(1));
  cell_assert(nz > 0);

  x.init(N);

  cell_info("Solving linear system of size %d x %d (UMFPACK LU solver).", 
      M, N);

  double* dnull = (double *) NULL;
  int*    inull = (int *) NULL;
  void *Symbolic, *Numeric;
  const size_t* Ap = &(A.index1_data() [0]);
  const size_t* Ai = &(A.index2_data() [0]);
  const double* Ax = &(A.value_data() [0]);
  double* xx = &(x.data() [0]);
  const double* bb = &(b.data() [0]);


  // Solve for transpose since we use compressed row format, and UMFPACK 
  // expects compressed column format

  int* Rp = new int[M+1];
  int* Ri = new int[nz];
  double* Rx = new double[nz];

  // Compute transpose
  umfpack_di_transpose(M, M, (const int*) Ap, (const int*) Ai, Ax, inull, inull, Rp, Ri, Rx);

  // Solve procedure
  umfpack_di_symbolic(M, M, (const int*) Rp, (const int*) Ri, Rx, &Symbolic, dnull, dnull);
  umfpack_di_numeric( (const int*) Rp, (const int*) Ri, Rx, Symbolic, &Numeric, dnull, dnull);
  umfpack_di_free_symbolic(&Symbolic);
  umfpack_di_solve(UMFPACK_A, (const int*) Rp, (const int*) Ri, Rx, xx, bb, Numeric, dnull, dnull);
  umfpack_di_free_numeric(&Numeric);

  // Clean up
  delete [] Rp;
  delete [] Ri;
  delete [] Rx;

  return 1;
}

#else

size_t uBlasLUSolver::solve(const uBlasMatrix<ublas_sparse_matrix>& A, uBlasVector& x, const uBlasVector& b)
{
	ublas_sparse_matrix Atemp(A);
	x.resize(b.size());
	x.assign(b);

  // Solve
	return solveInPlace(Atemp, x);
}
#endif

//-----------------------------------------------------------------------------

