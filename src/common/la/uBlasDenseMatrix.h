// Copyright (C) 2006 Garth N. Wells
// Licensed under the GNU GPL Version 2.
//
// Modified by Pawel Krupinski 2007 

#ifndef __UBLAS_DENSE_MATRIX_H
#define __UBLAS_DENSE_MATRIX_H

#include "uBlasMatrix.h"

typedef uBlasMatrix<ublas_dense_matrix> uBlasDenseMatrix;

#endif
