// Copyright (C) 2006 Anders Logg.
// Licensed under the GNU GPL Version 2.
//
// Modified by Pawel Krupinski 2007

#ifndef __UBLAS_DUMMY_PRECONDITIONER_H
#define __UBLAS_DUMMY_PRECONDITIONER_H

#include "uBlasPreconditioner.h"


  /// This class provides a dummy (do nothing) preconditioner for the
  /// uBlas Krylov solver.

  class uBlasDummyPreconditioner : public uBlasPreconditioner
  {
  public:

    /// Constructor
    uBlasDummyPreconditioner();

    /// Destructor
    ~uBlasDummyPreconditioner();

    /// Solve linear system Ax = b approximately
    void solve(uBlasVector& x, const uBlasVector& b) const;

  };

#endif
