// Copyright (C) 2006 Garth N. Wells.
// Licensed under the GNU GPL Version 2.
//
// Modified by Anders Logg 2006.
// Modified by Pawel Krupinski 2007

#ifndef __UBLAS_LU_SOLVER_H
#define __UBLAS_LU_SOLVER_H

#include "ublas.h"
#include "uBlasVector.h"
#include "uBlasLinearSolver.h"
//#include "parameter/Parametrized.h"
//#include "utils/error_macros.h"

  /// Forward declarations
  class uBlasVector;
  class uBlasKrylovMatrix;
  template<class Mat> class uBlasMatrix;

  /// This class implements the direct solution (LU factorization) of
  /// linear systems of the form Ax = b using uBlas data types. Dense
  /// matrices are solved using uBlas LU factorisation, and sparse matrices
  /// are solved using UMFPACK (http://www.cise.ufl.edu/research/sparse/umfpack/)
  /// is installed. Matrices can also be inverted.
    
  //class uBlasLUSolver : public uBlasLinearSolver, public Parametrized
  class uBlasLUSolver : public uBlasLinearSolver
{

	public:
    
    /// Constructor
		uBlasLUSolver();

    /// Destructor
		~uBlasLUSolver();

    /// Solve linear system Ax = b for a dense matrix
		size_t solve(const uBlasMatrix<ublas_dense_matrix>& A, uBlasVector& x, const uBlasVector& b);

    /// Solve linear system Ax = b for a sparse matrix using UMFPACK if installed
		size_t solve(const uBlasMatrix<ublas_sparse_matrix>& A, uBlasVector& x, const uBlasVector& b);
	
    /// Solve linear system Ax = b in place (A is dense or sparse)
		template<class Mat>
				size_t solveInPlaceUBlas(Mat& A, uBlasVector& x, const uBlasVector& b) const;
	/// LU factorize matrix A overwriting it with factorization matrices (A is dense or sparse)
		template<class Mat>
				size_t lu_factorize(Mat& A) const;
	/// solve linear system  LUx = b by forward and back substitution of provided LU factorized matrix(A is LU factorization)
		template<class Mat>
				size_t lu_substitute(Mat& A, uBlasVector& x, const uBlasVector& b) const;
    /// Compute the inverse of A (A is dense or sparse)
		template<class Mat>
				void invert(Mat& A) const;

	private:
    
    /// General uBlas LU solver which accepts both vector and matrix right-hand sides
		template<class Mat, class B>
				size_t solveInPlace(Mat& A, B& X) const;

    // Temporary data for LU factorization
		mutable ublas::permutation_matrix<std::size_t> *m_pmatrix;
};
//---------------------------------------------------------------------------
// Implementation of template functions
//---------------------------------------------------------------------------
template<class Mat>
		size_t uBlasLUSolver::lu_factorize(Mat& A) const
{
	const size_t M = A.size1();
	//cell_assert( M == A.size2() );
	assert( M == A.size2() );
	
	if(!m_pmatrix)
		m_pmatrix = new ublas::permutation_matrix<std::size_t>(M);
	else
		m_pmatrix->resize(M);
	
	return ublas::lu_factorize(A, *m_pmatrix);
}
//---------------------------------------------------------------------------
template<class Mat>
size_t uBlasLUSolver::lu_substitute(Mat& A, uBlasVector& x, const uBlasVector& b) const
{
  const size_t M = A.size1();
  //cell_assert(M == b.size());
  assert(M == b.size());
  if( x.size() != M )
    x.resize(M);
  // Initialise solution vector
  x.assign(b);
  
  ublas::lu_substitute(A, *m_pmatrix, x);
  return 0;
}
//---------------------------------------------------------------------------
template<class Mat>
		void uBlasLUSolver::invert(Mat& A) const
{
	const size_t M = A.size1();
	//cell_assert(M == A.size2());
	assert(M == A.size2());
  
    // Create indentity matrix
	Mat X(M, M);
	X.assign(ublas::identity_matrix<double>(M));

    // Solve
	solveInPlace(A, X);

	A.assign_temporary(X);
}
  //-----------------------------------------------------------------------------
template<class Mat>
		size_t uBlasLUSolver::solveInPlaceUBlas( Mat& A, uBlasVector& x, const uBlasVector& b) const
{
	const size_t M = A.size1();
	//cell_assert(M == b.size());
	assert(M == b.size());
	if( x.size() != M )
		x.resize(M);
  // Initialise solution vector
	x.assign(b);
  // Solve
	return solveInPlace(A, x);
}
//-----------------------------------------------------------------------------
template<class Mat, class B>
		size_t uBlasLUSolver::solveInPlace(Mat& A, B& X) const
{
	const size_t M = A.size1();
	//cell_assert( M == A.size2() );
	assert( M == A.size2() );
  
    // Create permutation matrix
	ublas::permutation_matrix<std::size_t> pmatrix(M);
    // Factorise (with pivoting)
	size_t singular = ublas::lu_factorize(A, pmatrix);
	if( singular > 0) {
	  std::cerr << "Singularity detected in uBlas matrix factorization on line " << singular-1 
		    << "." << std::endl;
	  exit(-1);
	}

    // Back substitute 
	ublas::lu_substitute(A, pmatrix, X);

	return 1;
}
  //-----------------------------------------------------------------------------


#endif
