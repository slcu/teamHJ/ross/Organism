// Copyright (C) 2006 Garth N. Wells.
// Licensed under the GNU GPL Version 2.
//
// Modified by Pawel Krupinski 2007

#ifndef __LU_SOLVER_H
#define __LU_SOLVER_H

#include "uBlasLUSolver.h"

  typedef uBlasLUSolver LUSolver;

#endif
