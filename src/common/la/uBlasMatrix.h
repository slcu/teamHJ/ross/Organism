// Copyright (C) 2006 Garth N. Wells
// Licensed under the GNU GPL Version 2.
//
// Modified by Anders Logg 2006.
// Modified by Pawel Krupinski 2007

#ifndef __UBLAS_MATRIX_H
#define __UBLAS_MATRIX_H

#include <sstream>
#include <iomanip>
#include <vector>
#include "ublas.h"
#include "uBlasVector.h"
#include "uBlasLUSolver.h"

  /// This class represents a matrix (dense or sparse) of dimension M x N.
  /// It is a wrapper for a Boost uBLAS matrix of type Mat.
  ///
  /// The interface is intended to provide uniformily with respect to other
  /// matrix data types. For advanced usage, refer to the documentation for 
  /// uBLAS which can be found at 
  /// http://www.boost.org/libs/numeric/ublas/doc/index.htm.

  /// Developer note: specialised member functions must be inlined to avoid link errors.

  template< class Mat >
		  class uBlasMatrix : /*public GenericMatrix,*/ public Mat
{
	public:
    
    /// Constructor
		uBlasMatrix();
    
    /// Constructor
		uBlasMatrix(const size_t M, const size_t N);

    /// Destructor
		~uBlasMatrix();

    /// Assignment from a matrix_expression
		template <class E>
				uBlasMatrix<Mat>& operator=(const ublas::matrix_expression<E>& A)
		{ 
			Mat::operator=(A); 
			return *this;
		} 

    /// Return number of rows (dim = 0) or columns (dim = 1) 
		size_t size(size_t dim) const;

    /// Access element value
		double get(size_t i, size_t j) const;

    /// Set element value
		void set(size_t i, size_t j, double value);

    /// Get non-zero values of row i
		void getRow(const size_t i, int& ncols, std::vector<int>& columns, std::vector<double>& values) const;

    /// Lump matrix into vector m
		void lump(uBlasVector& m) const;

    /// Solve Ax = b out-of-place (A is not destroyed)
		void solve(uBlasVector& x, const uBlasVector& b) const;

    /// Compute inverse of matrix
		void invert();

    /// Apply changes to matrix 
// 		void apply();

    /// Set all entries to zero
		void zero();

    /// Set given rows to identity matrix
		void ident(const int rows[], int m);

    /// Compute product y = Ax
		void mult(const uBlasVector& x, uBlasVector& y, bool init = true) const;

    /// Display matrix
		void disp(const size_t precision = 2) const;

    /// The below functions have specialisations for particular matrix types.
    /// In order to link correctly, they must be made inline functions.

    /// Initialize M x N matrix
		void init(size_t M, size_t N);

    /// Initialize M x N matrix with given maximum number of nonzeros in each row
		void init(size_t M, size_t N, size_t nzmax);

    /// Set block of values. The function apply() must be called to commit changes.
		void set(const double block[], const int rows[], int m, 
				 const int cols[], int n);

    /// Add block of values. The function apply() must be called to commit changes.
		void add(const double block[], const size_t rows[], size_t m, 
				 const size_t cols[], size_t n);

    /// Return average number of non-zeros per row
		size_t nzmax() const;

	private:

    /// Matrix used internally for assembly of sparse matrices
// 		ublas_assembly_matrix Assembly_matrix;

    /// Matrix state
// 		bool assembled;

};

  //---------------------------------------------------------------------------
  // Implementation of uBlasMatrix
  //---------------------------------------------------------------------------
  template <class Mat> 
		  uBlasMatrix<Mat>::uBlasMatrix()/* : assembled(true)*/
{ 
    // Do nothing 
}
  //---------------------------------------------------------------------------
  template <class Mat> 
		  uBlasMatrix<Mat>::uBlasMatrix(const size_t M, const size_t N)/* : assembled(true)*/
{ 
	init(M,N); 
}
  //---------------------------------------------------------------------------
  template <class Mat> 
		  uBlasMatrix<Mat>::~uBlasMatrix()
{ 
    // Do nothing 
}
  //---------------------------------------------------------------------------
  template <class Mat> 
		  size_t uBlasMatrix<Mat>::size(const size_t dim) const
{
  //cell_assert( dim < 2 );
  assert( dim < 2 );
  return (dim == 0 ? this->size1() : this->size2());  
}
  //---------------------------------------------------------------------------
  template <class Mat>  
		  inline double uBlasMatrix<Mat>::get(size_t i, size_t j) const
{ 
	return (*this)(i, j);
}
  //---------------------------------------------------------------------------
  template <class Mat>  
		  inline void uBlasMatrix<Mat>::set(size_t i, size_t j, double value) 
{ 
	(*this)(i, j) = value;
}
  //---------------------------------------------------------------------------
  template < class Mat >  
		  void uBlasMatrix< Mat >::getRow(const size_t i, int& ncols, std::vector<int>& columns, 
										  std::vector<double>& values) const
{
// 	if( !assembled )
// 		cell_error("Matrix has not been assembled. Did you forget to call A.apply()?"); 

    // Reference to matrix row (throw away const-ness and trust uBlas)
	ublas::matrix_row< uBlasMatrix<Mat> > row( *(const_cast< uBlasMatrix<Mat>* >(this)) , i);

	typename ublas::matrix_row< uBlasMatrix<Mat> >::const_iterator component;

    // Insert values into Arrays
	columns.clear();
	values.clear();
	for (component=row.begin(); component != row.end(); ++component) 
	{
		columns.push_back( component.index() );
		values.push_back( *component );
	}
	ncols = columns.size();
}
  //-----------------------------------------------------------------------------
  template <class Mat>  
		  void uBlasMatrix<Mat>::lump(uBlasVector& m) const
{
// 	if( !assembled )
// 		cell_error("Matrix has not been assembled. Did you forget to call A.apply()?"); 

	const size_t n = this->size(1);
	m.init( n );
	ublas::scalar_vector<double> one(n, 1.0);
	ublas::axpy_prod(*this, one, m, true);
}
  //-----------------------------------------------------------------------------
  template <class Mat>  
		  void uBlasMatrix<Mat>::solve(uBlasVector& x, const uBlasVector& b) const
{    
// 	if( !assembled )
// 		cell_error("Matrix has not been assembled. Did you forget to call A.apply()?"); 

	uBlasLUSolver solver;
	solver.solve(*this, x, b);
}
  //-----------------------------------------------------------------------------
  template <class Mat>  
		  void uBlasMatrix<Mat>::invert()
{
// 	if( !assembled )
// 		cell_error("Matrix has not been assembled. Did you forget to call A.apply()?"); 

	uBlasLUSolver solver;
	solver.invert(*this);
}
//-----------------------------------------------------------------------------
//   template <class Mat>
// 		  void uBlasMatrix<Mat>::apply()
// {
//     // Assign temporary assembly matrix to the sparse matrix
// 	if( !assembled )
// 	{
//       // Assign temporary assembly matrix to the matrix
// 		this->assign(Assembly_matrix);
// 		assembled = true;
// 
//       // Free memory
// 		Assembly_matrix.resize(0,0, false);
// 	} 
// }
  //---------------------------------------------------------------------------
  template <class Mat>  
		  void uBlasMatrix<Mat>::zero()
{
// 	if( !assembled )
// 		cell_error("Matrix has not been assembled. Did you forget to call A.apply()?"); 

    // Clear destroys non-zero structure of a sparse matrix 
	this->clear();

    // Set all non-zero values to zero without detroying non-zero pattern
  //  (*this) *= 0.0;
}
  //-----------------------------------------------------------------------------
  template <class Mat>  
		  void uBlasMatrix<Mat>::ident(const int rows[], const int m) 
{
// 	if( !assembled )
// 		cell_error("Matrix has not been assembled. Did you forget to call A.apply()?"); 

	const size_t n = this->size(1);
	for(int i = 0; i < m; ++i)
		ublas::row(*this, rows[i]) = ublas::unit_vector<double> (n, rows[i]);
}
  //---------------------------------------------------------------------------
  template <class Mat>  
  void uBlasMatrix<Mat>::mult(const uBlasVector& x, uBlasVector& y, bool init) const
  //HJ (comp prob MACOSX): void uBlasMatrix<Mat>::mult(const uBlasVector& x, uBlasVector& y, bool init = true) const
{
// 	if( !assembled )
// 		cell_error("Matrix has not been assembled. Did you forget to call A.apply()?"); 

	ublas::axpy_prod(*this, x, y, init);
}
  //---------------------------------------------------------------------------
  //template <class Mat>  
  //void uBlasMatrix<Mat>::mult(const uBlasVector& x, uBlasVector& y) const
  //{
  //  if( !assembled )
  //    cell_error("Matrix has not been assembled. Did you forget to call A.apply()?"); 

  //  ublas::axpy_prod(*this, x, y, true);
  //}
  //-----------------------------------------------------------------------------
  template <class Mat>  
		  void uBlasMatrix<Mat>::disp(const size_t precision) const
{
	typename Mat::const_iterator1 it1;  // Iterator over rows
	typename Mat::const_iterator2 it2;  // Iterator over entries

	for (it1 = this->begin1(); it1 != this->end1(); ++it1)
	{
		std::stringstream line;
		line << std::setiosflags(std::ios::scientific);
		line << std::setprecision(precision);
    
		line << "|";
		for (it2 = it1.begin(); it2 != it1.end(); ++it2)
			line << " (" << it2.index1() << ", " << it2.index2() << ", " << *it2 << ")";
		line << " |";

		std::cout << line.str().c_str() << std::endl;
	}  
}
  //-----------------------------------------------------------------------------
  // Specialised member functions (must be inlined to avoid link errors)
  //-----------------------------------------------------------------------------
  template <> 
		  inline void uBlasMatrix< ublas_dense_matrix >::init(const size_t M, const size_t N)
{
    // Resize matrix
	if( size(0) != M || size(1) != N )
		this->resize(M, N, false);  
}
  //---------------------------------------------------------------------------
  template <class Mat> 
		  inline void uBlasMatrix<Mat>::init(const size_t M, const size_t N)
{
    // Resize matrix
	if( size(0) != M || size(1) != N )
		this->resize(M, N, false);  

    // Resize assembly matrix
// 	if(Assembly_matrix.size1() != M && Assembly_matrix.size2() != N )
// 		Assembly_matrix.resize(M, N, false);
}
  //---------------------------------------------------------------------------
  template <> 
		  inline void uBlasMatrix<ublas_dense_matrix>::init(const size_t M, const size_t N, 
		  const size_t nzmax)
{
	init(M, N);
}
  //---------------------------------------------------------------------------
  template <class Mat> 
		  inline void uBlasMatrix<Mat>::init(const size_t M, const size_t N, const size_t nzmax)
{
	init(M, N);

    // Reserve space for non-zeroes
	const size_t total_nz = nzmax*size(0);
	this->reserve(total_nz);
}
  //---------------------------------------------------------------------------
//   template <>  
// 		  inline void uBlasMatrix<ublas_dense_matrix>::set(const double block[], 
// 		  const int rows[], const int m, const int cols[], const int n)
// {
// 	for (int i = 0; i < m; ++i)
// 		for (int j = 0; j < n; ++j)
// 			(*this)(rows[i] , cols[j]) = block[i*n + j];
// }
  //---------------------------------------------------------------------------
//   template <>  
// 		  inline void uBlasMatrix<ublas_dense_matrix>::add(const double block[], 
// 		  const size_t rows[], const size_t m, const size_t cols[], const size_t n)
// {
// 	for (int i = 0; i < m; ++i)
// 		for (int j = 0; j < n; ++j)
// 			(*this)(rows[i] , cols[j]) += block[i*n + j];
// }
  //---------------------------------------------------------------------------
  template <class Mat>  
		  inline void uBlasMatrix<Mat>::add(const double block[], const size_t rows[], 
											const size_t m, const size_t cols[], const size_t n)
{
// 	if( assembled )
// 	{
// 		Assembly_matrix.assign(*this);
// 		assembled = false; 
// 	}
	for (int i = 0; i < m; ++i)
		for (int j = 0; j < n; ++j)
// 			Assembly_matrix(rows[i] , cols[j]) += block[i*n + j];
			(*this)(rows[i] , cols[j]) += block[i*n + j];
}
  //---------------------------------------------------------------------------
  template <class Mat>  
		  inline void uBlasMatrix<Mat>::set(const double block[], const int rows[], 
											const int m, const int cols[], const int n)
{
// 	if( assembled )
// 	{
// 		Assembly_matrix.assign(*this);
// 		assembled = false; 
// 	}
	for (int i = 0; i < m; ++i)
		for (int j = 0; j < n; ++j)
// 			Assembly_matrix(rows[i] , cols[j]) = block[i*n + j];
			(*this)(rows[i] , cols[j]) = block[i*n + j];
}
  //---------------------------------------------------------------------------
  template <>  
		  inline size_t uBlasMatrix<ublas_dense_matrix>::nzmax() const 
{ 
	return 0; 
}
  //---------------------------------------------------------------------------
  template <class Mat>  
		  inline size_t uBlasMatrix<Mat>::nzmax() const 
{ 
	return this->nnz()/size(0); 
}

  //-----------------------------------------------------------------------------

#endif
