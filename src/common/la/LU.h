// Copyright (C) 2006 Anders Logg.
// Licensed under the GNU GPL Version 2.
// 
// Modified by Pawel Krupinski 2007
#ifndef __LU_H
#define __LU_H

#include "uBlasLUSolver.h"

  /// This class provides methods for solving a linear system by
  /// LU factorization.
  
  class LU
  {
  public:

    /// Solve linear system Ax = b
    static void solve(const uBlasMatrix<ublas_dense_matrix>& A, uBlasVector& x, const uBlasVector& b);
    
    /// Solve linear system Ax = b
    static void solve(const uBlasMatrix<ublas_sparse_matrix>& A, uBlasVector& x, const uBlasVector& b);

  };

#endif
