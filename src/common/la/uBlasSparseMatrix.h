// Copyright (C) 2006 Garth N. Wells
// Licensed under the GNU GPL Version 2.
//
// Modified by Pawel Krupinski 2007

#ifndef __UBLAS_SPARSE_MATRIX_H
#define __UBLAS_SPARSE_MATRIX_H

#include "uBlasMatrix.h"


  typedef uBlasMatrix<ublas_sparse_matrix> uBlasSparseMatrix;


#endif
