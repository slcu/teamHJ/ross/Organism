// Copyright (C) 2006 Garth N. Wells.
// Licensed under the GNU GPL Version 2.
//
// Modified by Anders Logg 2006.
// Modified by Pawel Krupinski 2007

#ifndef __UBLAS_ILU_PRECONDITIONER_H
#define __UBLAS_ILU_PRECONDITIONER_H

#include <vector>
#include "ublas.h"
#include "uBlasPreconditioner.h"
#include "uBlasMatrix.h"


  template<class Mat> class uBlasMatrix;
  class uBlasVector;

  /// This class implements an incomplete LU factorization (ILU)
  /// preconditioner for the uBlas Krylov solver.

  class uBlasILUPreconditioner : public uBlasPreconditioner
  {
  public:

    /// Constructor
    uBlasILUPreconditioner();

    /// Constructor
    uBlasILUPreconditioner(const uBlasMatrix<ublas_sparse_matrix>& A);

    /// Destructor
    ~uBlasILUPreconditioner();

    /// Solve linear system Ax = b approximately
    void solve(uBlasVector& x, const uBlasVector& b) const;

  private:

    // Initialize preconditioner
    void init(const uBlasMatrix<ublas_sparse_matrix>& A);

    // Preconditioner matrix
    uBlasMatrix<ublas_sparse_matrix> M;

    // Diagonal
	std::vector<size_t> diagonal;

  };

#endif
