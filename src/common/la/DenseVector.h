// Copyright (C) 2006 Garth N. Wells
// Licensed under the GNU GPL Version 2.
//
// Modified by Anders Logg 2006.
// Modified by Pawel Krupinski 2007

#ifndef __DENSE_VECTOR_H
#define __DENSE_VECTOR_H

#include "uBlasVector.h"


  typedef uBlasVector DenseVector;


#endif
