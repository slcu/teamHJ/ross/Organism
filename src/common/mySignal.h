#ifndef MYSIGNAL_H
#define MYSIGNAL_H

#include <vector>
#include <signal.h>
#include "../solvers/baseSolver.h"
#include "../optimizers/baseOptimizer.h"

namespace mySignal {
  void myExit();
	void addSolver(BaseSolver *S);
	void addOptimizer(BaseOptimizer *Opt);
  void solverSignalHandler(int signal);
  void optimizerSignalHandler(int signal);
}

#endif /* MYSIGNAL_H */
