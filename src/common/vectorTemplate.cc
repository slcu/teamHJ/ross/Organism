#include "vectorTemplate.h"

// Vector<3> cross(const Vector<3>& u,const Vector<3>& v)
//  {
//  	Vector<3> w;
//  		w.array_[0] = u.array_[1]*v.array_[2] - u.array_[2]*v.array_[1];
//  		w.array_[1] = u.array_[2]*v.array_[0] - u.array_[0]*v.array_[2];
//  		w.array_[2] = u.array_[0]*v.array_[1] - u.array_[1]*v.array_[0];
//  		return w;
// }

Vector<3> cross(const Vector<3>& u,const Vector<3>& v)
{
	Vector<3> w;
		w[0] = u[1]*v[2] - u[2]*v[1];
		w[1] = u[2]*v[0] - u[0]*v[2];
		w[2] = u[0]*v[1] - u[1]*v[0];
		return w;
}

