/**
 * Filename     : compartmentRemoval.h
 * Description  : Classes describing compartment removals
 * Author(s)    : Henrik Jonsson (henrik@thep.lu.se)
 * Created      : September 2005
 * Revision     : $Id: compartmentRemoval.h 648 2015-11-26 13:45:20Z andre $
 */
#ifndef COMPARTMENTREMOVAL_H
#define COMPARTMENTREMOVAL_H

#include "baseCompartmentChange.h"
#include "../organism.h"

//!Removes a compartment if threshold in varible reached
class RemovalThreshold : public BaseCompartmentChange {
  
 public:
  
  RemovalThreshold(std::vector<double> &paraValue, 
		   std::vector< std::vector<size_t> > 
		   &indValue );
  
  int flag(Compartment &compartment,
	   std::vector< std::vector<double> > &y,
	   std::vector< std::vector<double> > &dydt );
  void update(Organism &O,Compartment &compartment,
	      std::vector< std::vector<double> > &y,
	      std::vector< std::vector<double> > &dydt, double t=0.0 );  
};

//!Removes a compartment if threshold in distance to origo is reached
class RemovalRadialThreshold : public BaseCompartmentChange {
  
 public:
  
  RemovalRadialThreshold(std::vector<double> &paraValue, 
			 std::vector< std::vector<size_t> > 
			 &indValue );

  int flag(Compartment &compartment,
	   std::vector< std::vector<double> > &y,
	   std::vector< std::vector<double> > &dydt );
  void update(Organism &O,Compartment &compartment,
	     std::vector< std::vector<double> > &y,
	     std::vector< std::vector<double> > &dydt, double t=0.0 );  
};

//!Removes a Cigar compartment if threshold in position varibles reached
class RemovalThresholdBactPos : public BaseCompartmentChange {
  
 public:
  
  RemovalThresholdBactPos(std::vector<double> &paraValue, 
			  std::vector< std::vector<size_t> > 
			  &indValue );
  
  int flag(Compartment &compartment,
	   std::vector< std::vector<double> > &y,
	   std::vector< std::vector<double> > &dydt );
  void update(Organism &O,Compartment &compartment,
	     std::vector< std::vector<double> > &y,
	     std::vector< std::vector<double> > &dydt, double t=0.0 );  
};


//!Removes a compartment if outside a cylinder
// will remove if sqrt(x^2 + y ^2) < R
class RemovalCylinderThreshold : public BaseCompartmentChange {
  
 public:
  
  RemovalCylinderThreshold(std::vector<double> &paraValue, 
			 std::vector< std::vector<size_t> > 
			 &indValue );

  int flag(Compartment &compartment,
	   std::vector< std::vector<double> > &y,
	   std::vector< std::vector<double> > &dydt );
  void update(Organism &O,Compartment &compartment,
	     std::vector< std::vector<double> > &y,
	     std::vector< std::vector<double> > &dydt, double t=0.0 );  
};



#endif


