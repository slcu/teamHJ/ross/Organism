//
// Filename     : compartmentDivision.cc
// Description  : Classes describing compartment divisions
// Author(s)    : Henrik Jonsson (henrik@thep.lu.se)
// Created      : September 2005
// Revision     : $Id: compartmentDivision.cc 631 2015-06-21 23:38:14Z pau $
//

#include<cmath>

#include"baseCompartmentChange.h"
#include"compartmentDivision.h"
#include"../common/myRandom.h"

Sphere::DivisionRandomDirection::
DivisionRandomDirection(std::vector<double> &paraValue, 
			std::vector< std::vector<size_t> > 
			&indValue ) {
  
  //Do some checks on the parameters and variable indeces
  //////////////////////////////////////////////////////////////////////
  if( paraValue.size()!=3 ) {
    std::cerr << "Sphere::DivisionRandomDirection::"
	      << "DivisionRandomDirection() "
	      << "Uses three parameters r_th, V_diff and h.\n";
    exit(0);
  }
  if( indValue.size() ) {
    std::cerr << "Sphere::DivisionRandomDirection::"
	      << "DivisionRandomDirection() "
	      << "No variable index is used.\n";
    exit(0);
  }
  //Set the variable values
  //////////////////////////////////////////////////////////////////////
  setId("Sphere::divisionRandomDirection");
  setParameter(paraValue);  
  setVariableIndex(indValue);
  
  //Set the parameter identities
  //////////////////////////////////////////////////////////////////////
  std::vector<std::string> tmp( numParameter() );
  tmp.resize( numParameter() );
  tmp[0] = "r_th";
  tmp[1] = "V_diff";
  tmp[2] = "h";
  setParameterId( tmp );

  //Set the number of compartment change (+1 for division)
  //////////////////////////////////////////////////////////////////////
  setNumChange(1);
}

//! Flags for division if the radius is larger than a threshold value
int Sphere::DivisionRandomDirection::
flag(Compartment &compartment,
     std::vector< std::vector<double> > &y,
     std::vector< std::vector<double> > &dydt ) {
  
  if( y[compartment.index()][compartment.numTopologyVariable()-1] 
      > parameter(0) ) 
    return 1;
  else 
    return 0;
}

//! Divides a spherical cell in random direction and w preserved volume
void Sphere::DivisionRandomDirection::
update(Organism &O, Compartment &compartment,
       std::vector< std::vector<double> > &y,
       std::vector< std::vector<double> > &dydt, double t ) {
  
  int verbose=0;
  size_t i=compartment.index();
  if (verbose)
    std::cerr << "Sphere::DivisionRandomDirection::update  Cell " << i 
	      << " divides at time " << t << "\n"; 
  size_t dim = compartment.numTopologyVariable()-1;
  double PI=3.14159;  
  double diffFraction=parameter(1);
  double h=parameter(2)*y[i][dim];
  O.addDivision(i,t);
  
  std::vector<double> direction( dim );
  
  //Create a vector in a random direction (uniformly)
  //////////////////////////////////////////////////////////////////////
  if( dim==3 ) {
    double phi = myRandom::Rnd()*2.*PI;
    double theta = myRandom::Rnd()*PI;
    double sinTheta = sin(theta);
    direction[0] = cos(phi)*sinTheta;
    direction[1] = sin(phi)*sinTheta;
    direction[2] = cos(theta);
  }
  else if( dim==2 ) {
    double phi = myRandom::Rnd()*2*PI;
    direction[0] = cos(phi);
    direction[1] = sin(phi);
  }
  else if( dim==1 ) {
    if( myRandom::Rnd()>0.5 )
      direction[0]=1;
    else
      direction[0]=-1;
  }
  else {
    std::cerr << "Sphere::DivisionRandomDirection::update\n"
	      << "Wrong dimension for dividing compartment.\n";
    std::cerr << dim << "\n";
    exit(-1);
  }   
  //Change the direction vector elements to an vector of length h
  double norm=0.;
  for( size_t d=0 ; d<direction.size() ; d++ )
    norm += direction[d]*direction[d];
  if( norm<=0 ) {
    std::cerr << "Sphere::DivisionRandomDirection::update() "
	      << "No length in directional vector!\n";
    exit(-1);
  }
  norm = std::sqrt( norm );
  h = h/norm;//normalizing AND adjusting length at the same time
  for( size_t d=0 ; d<dim ; d++ )
    direction[d] *= h;
  
  //Divide
  //////////////////////////////////////////////////////////////////////
  size_t NN=O.numCompartment();
  size_t N=NN+1;
  size_t M=compartment.numVariable();
  O.addCompartment( compartment );
  O.compartment(NN).setIndex(NN);
  
  y.resize( N );
  dydt.resize( N );
  y[NN].resize( M );
  dydt[NN].resize( M );
  for( size_t j=0 ; j<M ; j++ ) {
    y[NN][j] = y[i][j];
    dydt[NN][j] = dydt[i][j];
  } 
  
  //Move the mother and daughter symmetrically (only in y-matrix)
  for( size_t d=0 ; d<dim ; d++ ) {
    y[i][d] += 0.5*direction[d];
    y[NN][d] -= 0.5*direction[d];
  }
  
  //Divide mass with two and add some diff (only in y-matrix)
  double newMass = std::pow(y[i][dim],(int)dim)/2.;
  double eps = diffFraction*myRandom::Rnd()*newMass;
  if( eps>newMass ) eps = newMass;//Prevent negative mass...
  if( myRandom::Rnd() > 0.5 ) {
    y[i][dim] = std::pow(newMass+eps,(double) (1./double(dim)));
    y[NN][dim] = std::pow(newMass-eps,(double) (1./double(dim)));
  }
  else {
    y[i][dim] = std::pow(newMass-eps,(double) (1./double(dim)));
    y[NN][dim] = std::pow(newMass+eps,(double) (1./double(dim)));
  }
  //Add link relationship between mother/daughter
  //assume this is done directly after divisions!!!
}

Sphere::DivisionRandomAndSCDirection::
DivisionRandomAndSCDirection(std::vector<double> &paraValue, 
			     std::vector< std::vector<size_t> > 
			     &indValue ) {
  
  //Do some checks on the parameters and variable indeces
  //////////////////////////////////////////////////////////////////////
  if( paraValue.size()!=4 ) {
    std::cerr << "Sphere::DivisionRandomAndSCDirection::"
	      << "DivisionRandomAndSCDirection() "
	      << "Uses four parameters r_th, V_diff, h and R.\n";
    exit(0);
  }
  if( indValue.size() ) {
    std::cerr << "Sphere::DivisionRandomAndSCDirection::"
	      << "DivisionRandomAndSCDirection() "
	      << "No variable index is used.\n";
    exit(0);
  }
  //Set the variable values
  //////////////////////////////////////////////////////////////////////
  setId("Sphere::divisionRandomAndSCDirection");
  setParameter(paraValue);  
  setVariableIndex(indValue);
  
  //Set the parameter identities
  //////////////////////////////////////////////////////////////////////
  std::vector<std::string> tmp( numParameter() );
  tmp.resize( numParameter() );
  tmp[0] = "r_th";
  tmp[1] = "V_diff";
  tmp[2] = "h";
  tmp[3] = "R";
  setParameterId( tmp );

  //Set the number of compartment change (+1 for division)
  //////////////////////////////////////////////////////////////////////
  setNumChange(1);
}

//! Flags for division if the radius is larger than a threshold value
int Sphere::DivisionRandomAndSCDirection::
flag(Compartment &compartment,
     std::vector< std::vector<double> > &y,
     std::vector< std::vector<double> > &dydt ) {
  
  if( y[compartment.index()][compartment.numTopologyVariable()-1] 
      > parameter(0) ) 
    return 1;
  else 
    return 0;
}

//! Divides a spherical cell in random direction and w preserved volume
void Sphere::DivisionRandomAndSCDirection::
update(Organism &O, Compartment &compartment,
       std::vector< std::vector<double> > &y,
       std::vector< std::vector<double> > &dydt, double t ) {
  
  size_t i=compartment.index();
  size_t dim = compartment.numTopologyVariable()-1;
  double PI=3.14159;  
  double diffFraction=parameter(1);
  double h=parameter(2)*y[i][dim];
  O.addDivision(i,t);
  
  std::vector<double> direction( dim );
  double Rpos=0.0;
  for( size_t d=0 ; d<dim-1 ; d++ )
    Rpos += y[i][d]*y[i][d];
  if( y[i][dim-1]>0.0 )
    Rpos +=y[i][dim-1]*y[i][dim-1];
  
  Rpos = std::sqrt(Rpos);
  if( Rpos < parameter(3) ) {
    //
    // Create a vector in a random direction (uniformly)
    //
    if( dim==3 ) {
      double phi = myRandom::Rnd()*2.*PI;
      double theta = myRandom::Rnd()*PI;
      double sinTheta = sin(theta);
      direction[0] = cos(phi)*sinTheta;
      direction[1] = sin(phi)*sinTheta;
      direction[2] = cos(theta);
    }
    else if( dim==2 ) {
      double phi = myRandom::Rnd()*2*PI;
      direction[0] = cos(phi);
      direction[1] = sin(phi);
    }
    else if( dim==1 ) {
      if( myRandom::Rnd()>0.5 )
	direction[0]=1;
      else
				direction[0]=-1;
    }
    else {
      std::cerr << "Sphere::DivisionRandomAndSCDirection::update\n"
		<< "Wrong dimension for dividing compartment.\n";
      std::cerr << dim << "\n";
      exit(-1);
    }   
  }
  else {
    //
    // Create an anticlinal vector (perpendic. to a sphere/cylinder surface)
    //
    if( dim==3 ) {
      std::cerr << "Sphere::DivisionRandomAndSCDirection::update\n"
		<< "Not implemented for 3D yet.\n";
      exit(-1);
      //double phi = myRandom::Rnd()*2.*PI;
      //double theta = myRandom::Rnd()*PI;
      //double sinTheta = sin(theta);
      //direction[0] = cos(phi)*sinTheta;
      //direction[1] = sin(phi)*sinTheta;
      //direction[2] = cos(theta);
    }
    else if( dim==2 ) {
      if( y[i][dim-1]<=0.0 ) {
	//On cylinder
	if( myRandom::Rnd()>0.5 )
	  direction[1]=1;
	else
	  direction[1]=-1;
      }
      else {
	//On sphere
	if( myRandom::Rnd()>0.5 ) {
	  direction[0] = y[i][1];
	  direction[1] = -y[i][0];
	}
	else {
	  direction[0] = -y[i][1];
	  direction[1] = y[i][0];
	}
      }
    }
    else if( dim==1 ) {
      if( myRandom::Rnd()>0.5 )
	direction[0]=1;
      else
	direction[0]=-1;
    }
    else {
      std::cerr << "Sphere::DivisionRandomAndSCDirection::update\n"
		<< "Wrong dimension for dividing compartment.\n";
      std::cerr << dim << "\n";
      exit(-1);
    }           
  }
  
  //Change the direction vector elements to an vector of length h
  double norm=0.;
  for( size_t d=0 ; d<direction.size() ; d++ )
    norm += direction[d]*direction[d];
  if( norm<=0 ) {
    std::cerr << "Sphere::DivisionRandomAndSCDirection::update() "
	      << "No length in directional vector!\n";
    exit(-1);
  }
  norm = std::sqrt( norm );
  h = h/norm;//normalizing AND adjusting length at the same time
  for( size_t d=0 ; d<dim ; d++ )
    direction[d] *= h;
  
  //
  // Divide
  //
  size_t NN=O.numCompartment();
  size_t N=NN+1;
  size_t M=compartment.numVariable();
  O.addCompartment( compartment );
  O.compartment(NN).setIndex(NN);
  
  y.resize( N );
  dydt.resize( N );
  y[NN].resize( M );
  dydt[NN].resize( M );
  for( size_t j=0 ; j<M ; j++ ) {
    y[NN][j] = y[i][j];
    dydt[NN][j] = dydt[i][j];
  } 
  
  //Move the mother and daughter symmetrically (only in y-matrix)
  for( size_t d=0 ; d<dim ; d++ ) {
    y[i][d] += 0.5*direction[d];
    y[NN][d] -= 0.5*direction[d];
  }
  
  //Divide mass with two and add some diff (only in y-matrix)
  double newMass = std::pow(y[i][dim],(int)dim)/2.;
  double eps = diffFraction*myRandom::Rnd()*newMass;
  if( eps>newMass ) eps = newMass;//Prevent negative mass...
  if( myRandom::Rnd() > 0.5 ) {
    y[i][dim] = std::pow(newMass+eps,1./double(dim));
    y[NN][dim] = std::pow(newMass-eps,1./double(dim));
  }
  else {
    y[i][dim] = std::pow(newMass-eps,1./double(dim));
    y[NN][dim] = std::pow(newMass+eps,1./double(dim));
  }
  //Add link relationship between mother/daughter
  //assume this is done directly after divisions!!!
}

Sphere::DivisionRandomDirectionOnSCSurface::
DivisionRandomDirectionOnSCSurface(std::vector<double> 
				   &paraValue, 
				   std::vector< 
				   std::vector<size_t> > 
				   &indValue ) 
{  
  // Do some checks on the parameters and variable indeces
  if( paraValue.size()!=4 ) {
    std::cerr << "Sphere::DivisionRandomDirectionOnSCSurface::"
	      << "DivisionRandomDirectionOnSCSurface() "
	      << "Uses three parameters r_th, V_diff, h and R.\n";
    exit(0);
  }
  if( indValue.size() ) {
    std::cerr << "Sphere::DivisionRandomDirectionOnSCSurface::"
	      << "DivisionRandomDirectionOnSCSurface() "
	      << "No variable index is used.\n";
    exit(0);
  }
  
  // Set the variable values
  setId("Sphere::divisionRandomDirectionOnSCSurface");
  setParameter(paraValue);  
  setVariableIndex(indValue);
  
  // Set the parameter identities
  std::vector<std::string> tmp( numParameter() );
  tmp.resize( numParameter() );
  tmp[0] = "r_th";
  tmp[1] = "V_diff";
  tmp[2] = "h";
  tmp[3] = "R";
  setParameterId( tmp );
  
  //Set the number of compartment change (+1 for division)
  setNumChange(1);
}

int Sphere::DivisionRandomDirectionOnSCSurface::
flag(Compartment &compartment,
     std::vector< std::vector<double> > &y,
     std::vector< std::vector<double> > &dydt ) 
{  
  if( y[compartment.index()][compartment.numTopologyVariable()-1] 
      > parameter(0) && 
      y[compartment.index()][compartment.numTopologyVariable()-2]
      > 0.7*parameter(3) ) 
    return 1;
  else 
    return 0;
}

//! Divides a spherical cell in random direction and w preserved volume
void Sphere::DivisionRandomDirectionOnSCSurface::
update(Organism &O, Compartment &compartment,
       std::vector< std::vector<double> > &y,
       std::vector< std::vector<double> > &dydt, double t ) {
  
  size_t verbose=1;
  size_t i=compartment.index();
  std::cerr << "Sphere::DivisionRandomDirectionOnSCSurface::update  Cell " << i 
	    << " divides at time " << t << "\n"; 
  size_t dimension = compartment.numTopologyVariable()-1;
  double diffFraction=parameter(1);
  double h=parameter(2)*y[i][dimension];
  O.addDivision(i,t);
  
  std::vector<double> direction( dimension );
  std::vector<double> A( dimension );//Position of the cell
  double R=0.0;
  int zCol=2;
  for( size_t dim=0 ; dim<dimension ; ++dim )
    A[dim] = y[i][dim];
  if( y[i][zCol]>0.0 )
    R = std::sqrt( A[0]*A[0]+A[1]*A[1]+A[2]*A[2] );
  else
    R = std::sqrt( A[0]*A[0]+A[1]*A[1] );
  if( y[i][2]>0.0 )
    getRandomSphereSurfaceDirection(A,direction);
  else
    getRandomCylinderSurfaceDirection(A,direction);
  
  double norm=0.0;
  for( size_t dim=0 ; dim<dimension ; ++dim )
    norm += direction[dim]*direction[dim];
  norm = 1.0/std::sqrt(norm);
  
  for( size_t d=0 ; d<dimension ; d++ )
    direction[d] *= h*norm;
  
  //std::cerr << "dx dy dz  " << direction[0] << " " << direction[1]
  //	<< " " << direction[2] << " ";
  //
  // Divide
  //
  size_t NN=O.numCompartment();
  size_t N=NN+1;
  size_t M=compartment.numVariable();
  O.addCompartment( compartment );
  O.compartment(NN).setIndex(NN);
  
  y.resize( N );
  dydt.resize( N );
  y[NN].resize( M );
  dydt[NN].resize( M );
  for( size_t j=0 ; j<M ; j++ ) {
    y[NN][j] = y[i][j];
    dydt[NN][j] = dydt[i][j];
  } 
  
  //Move the mother and daughter symmetrically (only in y-matrix)
  for( size_t d=0 ; d<dimension ; d++ ) {
    y[i][d] += 0.5*direction[d];
    y[NN][d] -= 0.5*direction[d];
  }
  
  //Divide mass with two and add some diff (only in y-matrix)
  double newMass = std::pow(y[i][dimension],(int) dimension)/2.;
  double eps = diffFraction*myRandom::Rnd()*newMass;
  if( eps>newMass ) eps = newMass;//Prevent negative mass...
  if( myRandom::Rnd() > 0.5 ) {
    y[i][dimension] = std::pow(newMass+eps,1./double(dimension));
    y[NN][dimension] = std::pow(newMass-eps,1./double(dimension));
  }
  else {
    y[i][dimension] = std::pow(newMass-eps,1./double(dimension));
    y[NN][dimension] = std::pow(newMass+eps,1./double(dimension));
  }
  
  if( verbose ) {
    if( y[i][zCol]>0.0 )
      std::cerr << "R(i)_sphere=" 
		<< std::sqrt( y[i][0]*y[i][0]+y[i][1]*y[i][1]+
			      y[i][2]*y[i][2] ) << " ";
    else
      std::cerr << "R(i)_cylinder=" 
		<< std::sqrt( y[i][0]*y[i][0]+y[i][1]*y[i][1] ) << " ";
    if( y[NN][zCol]>0.0 )
      std::cerr << "R(NN)_sphere=" 
		<< std::sqrt( y[NN][0]*y[NN][0]+y[NN][1]*y[NN][1]+
			      y[NN][2]*y[NN][2] ) << "\n";
    else
      std::cerr << "R(NN)_cylinder=" 
		<< std::sqrt( y[NN][0]*y[NN][0]+y[NN][1]*y[NN][1] ) 
		<< "\n";
  }
	
  //Project the new positions onto the sphere/cylinder surface
  double normI=R,normNN=R;
  if( y[i][zCol]>0.0 )
    normI /= std::sqrt( y[i][0]*y[i][0]+y[i][1]*y[i][1]+y[i][2]*y[i][2] );
  else
    normI /= std::sqrt( y[i][0]*y[i][0]+y[i][1]*y[i][1] );
  if( y[NN][zCol]>0.0 )
    normNN /= std::sqrt( y[NN][0]*y[NN][0]+y[NN][1]*y[NN][1]+
			 y[NN][2]*y[NN][2] );
  else
    normNN /= std::sqrt( y[NN][0]*y[NN][0]+y[NN][1]*y[NN][1] );
  
  for( int dim=0 ; dim<zCol ; dim++ ) {
    y[i][dim] *=normI;
    y[NN][dim] *=normNN;
	}
  if( y[i][zCol]>0.0 )
    y[i][zCol] *=normI;
  if( y[NN][zCol]>0.0 )
    y[NN][zCol] *=normNN;      
  
  //Add link relationship between mother/daughter
  //assume this is done directly after divisions!!!
}

void Sphere::DivisionRandomDirectionOnSCSurface::
getRandomSphereSurfaceDirection(std::vector<double> &A,
				std::vector<double> &d) 
{  
  if( A.size() != 3 ) {
    std::cerr << "void Sphere::DivisionRandomDirectionOnSCSurface"
	      << "::getRandomSphereSurfaceDirection() "
	      << "Assumes three dimensions!\n";
    exit(-1);
  }
  if( d.size() != A.size() )
    d.resize( A.size() );
  
  double eps = 0.001;
  double PI=3.141592654;
  std::vector<double> B(A.size()),r(A.size()),
    W(A.size()),t(A.size());
  double R=0.0;
  for( size_t dim=0 ; dim<A.size() ; dim++ )
    R += A[dim]*A[dim];
  R = std::sqrt(R);
  //Pick a random vector not parallell to A
  double AB;
  do {
    AB=0.0;
    for( size_t dim=0 ; dim<A.size() ; dim++ ) {
      B[dim] = myRandom::Rnd();
      AB += A[dim]*B[dim];
    }
  } while( std::fabs(R*R-AB)<eps );
  //Get a vector orthogonal to A (from B)
  double fac=AB/(R*R);
  double norm=0.0;
  for( size_t dim=0 ; dim<A.size() ; dim++ ) {
    W[dim] = B[dim] - fac*A[dim];
    norm += W[dim]*W[dim];
  }
  norm = 1./std::sqrt(norm);
  double AW=0.0;
  for( size_t dim=0 ; dim<A.size() ; dim++ ) {
    W[dim] *= norm;
    AW += A[dim]*W[dim];
  }
  //Make sure A and W are orthogonal 
  if( AW>eps ) {
    std::cerr << "Sphere::DivisionRandomDirectionOnSCSurface"
	      << "::getRandomSphereSurfaceDirection() A,W vectors "
	      << "not orthogonal\n";
    exit(-1);
  }
  //Define r
  for( size_t dim=0 ; dim<A.size() ; dim++ )
    r[dim] = A[dim]+W[dim];
  //Create t as a random rotation of r (around A)
  // t = r*cos(phi)+A(1-cos(phi)+(rxA/R)sin(phi))
  double phi = 2*PI*myRandom::Rnd();
  t[0] = (r[1]*A[2]-r[2]*A[1])/R;
  t[1] = (r[2]*A[0]-r[0]*A[2])/R;
  t[2] = (r[0]*A[1]-r[1]*A[0])/R;
  for( size_t dim=0 ; dim<A.size() ; dim++ )
    t[dim] = A[dim] + (r[dim]-A[dim])*cos(phi) + t[dim]*sin(phi);
  
  //Define the direction vector
  double Ad=0.0;
  for( size_t dim=0 ; dim<A.size() ; dim++ ) {
    d[dim] = t[dim]-A[dim];
    Ad += A[dim]*d[dim];
  }
  //Make sure A and direction are orthogonal 
  if( Ad>eps ) {
    std::cerr << "Sphere::DivisionRandomDirectionOnSCSurface"
	      << "::cellDivisionSphereSurfaceDirection() "
	      << "A and d vectors not orthogonal\n";
    exit(-1);
  }
}

void Sphere::DivisionRandomDirectionOnSCSurface::
getRandomCylinderSurfaceDirection(std::vector<double> &A,
				  std::vector<double> &d) 
{  
  if( A.size() != 3 ) {
    std::cerr << "void Sphere::DivisionRandomDirectionOnSCSurface"
	      << "::getRandomCylinderSurfaceDirection() "
	      << "Assumes three dimensions!\n";
    exit(-1);
  }
  if( d.size() != A.size() )
    d.resize( A.size() );
  
  double PI=3.141592654;
  double R=std::sqrt( A[0]*A[0] + A[1]*A[1] );
  double phi = std::atan( std::fabs(A[1]/A[0]) );
  if( A[0]>0 && A[1]<0 )
    phi = 2*PI-phi;
  else if( A[0]<0 && A[1]>0 )
    phi = PI-phi;
  else if( A[0]<0 && A[1]<0 )
    phi = PI+phi;
  
  double theta = 2*PI*myRandom::Rnd();
  d[2] = std::sin(theta);
  double dPhi = std::cos(theta)/R;
  //convert dPhi to dx,dy
  d[0] = R*( std::cos(phi+dPhi) - std::cos(phi) );
  d[1] = R*( std::sin(phi+dPhi) - std::sin(phi) );
}

//!Constructor for the DivisionCellDirectionEllipse class
DivisionCellDirectionEllipse::
DivisionCellDirectionEllipse(std::vector<double> &paraValue, 
			     std::vector< std::vector<size_t> > 
			     &indValue ) {
  
  //Do some checks on the parameters and variable indeces
  //////////////////////////////////////////////////////////////////////
  if( paraValue.size()!=3 ) {
    std::cerr << "DivisionCellDirectionEllipse::"
	      << "DivisionCellDirectionEllipse() "
	      << "Uses one parameter d_th d_diff d_random.\n";
    exit(0);
  }
  if( indValue.size() ) {
    std::cerr << "DivisionCellDirectionEllipse::"
	      << "DivisionCellDirectionEllipse() "
	      << "No variable index is used.\n";
    exit(0);
  }
  //Set the variable values
  //////////////////////////////////////////////////////////////////////
  setId("divisionCellDirectionEllipse");
  setParameter(paraValue);  
  setVariableIndex(indValue);
  
  //Set the parameter identities
  //////////////////////////////////////////////////////////////////////
  std::vector<std::string> tmp( numParameter() );
  tmp.resize( numParameter() );
  tmp[0] = "d_th";
  tmp[1] = "d_diff";
  tmp[1] = "d_random";
  
  setParameterId( tmp );
  
  //Set the number of compartment change (+1 for division)
  //////////////////////////////////////////////////////////////////////
  setNumChange(1);
}

//! Flags for division if the radius is larger than a threshold value
int DivisionCellDirectionEllipse::
flag(Compartment &compartment,
     std::vector< std::vector<double> > &y,
     std::vector< std::vector<double> > &dydt ) {
  
  if( y[compartment.index()][compartment.numTopologyVariable()-1] > 
      parameter(0) ) 
    return 1;
  else 
    return 0;
}

//! Divides a spherical cell in random direction and w preserved volume
void DivisionCellDirectionEllipse::
update(Organism &O, Compartment &compartment,
       std::vector< std::vector<double> > &y,
       std::vector< std::vector<double> > &dydt, double t ) {

  if( compartment.numTopologyVariable() != 5 && 
      compartment.numTopologyVariable() != 7 ) {
    std::cerr << "DivisionCellDirectionEllipse::update() "
	      << " Only defined for ellipses of dimension 2,3\n";
    exit(-1);
  }
  size_t i=compartment.index();
  size_t dimension = (compartment.numTopologyVariable()-1)/2;
  
  std::vector<size_t> x1Col(dimension),x2Col(dimension);
  for( size_t d=0 ; d<dimension ; d++ ) {
    x1Col[d] = d;
    x2Col[d] = d+dimension;
  }
  size_t dCol = compartment.numTopologyVariable()-1;
  O.addDivision(i,t);
  std::vector<double> center(dimension), direction(dimension);
  
  //Extract direction from spatial variables
  //////////////////////////////////////////////////////////////////////
  double length=0.0;
  for( size_t d=0 ; d<dimension ; d++ ) {
    center[d] = 0.5*(y[i][x2Col[d]]+y[i][x1Col[d]]);
    direction[d] = (y[i][x2Col[d]]-y[i][x1Col[d]]);
    length += direction[d]*direction[d];
  }
  length = std::sqrt( length );
  for( size_t d=0 ; d<dimension ; d++ )
    direction[d] /= length; 
  
  //Divide
  //////////////////////////////////////////////////////////////////////
  size_t NN=O.numCompartment();
  size_t N=NN+1;
  size_t M=compartment.numVariable();
  O.addCompartment( compartment );
  O.compartment(NN).setIndex(NN);
  
  y.resize( N );
  dydt.resize( N );
  y[NN].resize( M );
  dydt[NN].resize( M );
  for( size_t j=0 ; j<M ; j++ ) {
    y[NN][j] = y[i][j];
    dydt[NN][j] = dydt[i][j];
  } 
  
  //divide d into two almost equal sizes
  double da = 0.5*y[i][dCol]*( 1.0 + y[i][dCol]*parameter(1)*
			       (2.0*myRandom::Rnd()-1.0) );
  double db = y[i][dCol]-da;
  y[i][dCol] = da;
  y[NN][dCol] = db;
  
  //Add center point to first/second point for the new cells
  for( size_t d=0 ; d<dimension ; d++ ) {
    y[i][x2Col[d]]  = y[i][x1Col[d]]  + da*direction[d] +
      parameter(2)*(myRandom::Rnd()-0.5);
    y[NN][x1Col[d]] = y[NN][x2Col[d]] - db*direction[d] +
      parameter(2)*(myRandom::Rnd()-0.5);
  }  
  //Add link relationship between mother/daughter
  //assume this is done directly after divisions!!!
}

Sphere::DivisionDirection::
DivisionDirection(std::vector<double> &paraValue, 
		  std::vector< std::vector<size_t> > 
		  &indValue ) 
{  
  //
  // Do some checks on the parameters and variable indeces
  //
  if( paraValue.size()!=3 ) {
    std::cerr << "Sphere::DivisionDirection::"
	      << "DivisionDirection() "
	      << "Uses three parameters r_th, V_diff and h.\n";
    exit(0);
  }
  if( indValue.size()!=1 || indValue[0].size()!=1 ) {
    std::cerr << "Sphere::DivisionDirection::"
	      << "DivisionDirection() "
	      << "A single variable index is used to specify the direction"
	      << " of division.\n";
    exit(0);
  }
  //
  // Set the variable values
  //
  setId("Sphere::divisionDirection");
  setParameter(paraValue);  
  setVariableIndex(indValue);
  //
  // Set the parameter identities
  //
  std::vector<std::string> tmp( numParameter() );
  tmp.resize( numParameter() );
  tmp[0] = "r_th";
  tmp[1] = "V_diff";
  tmp[2] = "h";
  setParameterId( tmp );
  //
  // Set the number of compartment change (+1 for division)
  //
  setNumChange(1);
}

int Sphere::DivisionDirection::
flag(Compartment &compartment,
     std::vector< std::vector<double> > &y,
     std::vector< std::vector<double> > &dydt ) {
  
  if( y[compartment.index()][compartment.numTopologyVariable()-1] 
      > parameter(0) ) 
    return 1;
  else 
    return 0;
}

void Sphere::DivisionDirection::
update(Organism &O, Compartment &compartment,
       std::vector< std::vector<double> > &y,
       std::vector< std::vector<double> > &dydt, double t ) {
  
  int verbose=0;
  
  size_t i=compartment.index();
  if (verbose)
    std::cerr << "Sphere::DivisionDirection::update  Cell " << i 
	      << " divides at time " << t << "\n"; 
  size_t dim = compartment.numTopologyVariable()-1;
  assert( variableIndex(0,0)<dim);
  double diffFraction=parameter(1);
  double h=parameter(2)*y[i][dim];
  O.addDivision(i,t);
  
  std::vector<double> direction( dim );
  //
  // Create a vector in a specific direction
  //
  if( dim==3 ) {
    //direction[0] = direction[1] = direction[2] = 0.0;
    direction[variableIndex(0,0)] = h;
  }
  else if( dim==2 ) {
    //direction[0] = direction[1] = 0.0;
    direction[variableIndex(0,0)] = h;
  }
  else if( dim==1 ) {
    direction[0]=h;
  }
  else {
    std::cerr << "Sphere::DivisionDirection::update\n"
	      << "Wrong dimension for dividing compartment.\n";
    std::cerr << dim << "\n";
    exit(-1);
  }   
  //
  // Divide
  //
  size_t NN=O.numCompartment();
  size_t N=NN+1;
  size_t M=compartment.numVariable();
  O.addCompartment( compartment );
  O.compartment(NN).setIndex(NN);
  
  y.resize( N );
  dydt.resize( N );
  y[NN].resize( M );
  dydt[NN].resize( M );
  for( size_t j=0 ; j<M ; j++ ) {
    y[NN][j] = y[i][j];
    dydt[NN][j] = dydt[i][j];
  } 
  
  //Move the mother and daughter symmetrically (only in y-matrix)
  for( size_t d=0 ; d<dim ; d++ ) {
    y[i][d] += 0.5*direction[d];
    y[NN][d] -= 0.5*direction[d];
  }
  
  //Divide mass with two and add some diff (only in y-matrix)
  double newMass = std::pow(y[i][dim],(int) dim)/2.;
  double eps = diffFraction*myRandom::Rnd()*newMass;
  if( eps>newMass ) eps = newMass;//Prevent negative mass...
  if( myRandom::Rnd() > 0.5 ) {
    y[i][dim] = std::pow(newMass+eps,1./double(dim));
    y[NN][dim] = std::pow(newMass-eps,1./double(dim));
  }
  else {
    y[i][dim] = std::pow(newMass-eps,1./double(dim));
    y[NN][dim] = std::pow(newMass+eps,1./double(dim));
  }
  //Add link relationship between mother/daughter
  //assume this is done directly after divisions!!!
}

Sphere::DivisionVelocity::
DivisionVelocity(std::vector<double> &paraValue, 
		 std::vector< std::vector<size_t> > 
		 &indValue ) 
{  
  //
  // Do some checks on the parameters and variable indeces
  //
  if( paraValue.size()!=3 ) {
    std::cerr << "Sphere::DivisionVelocity::"
	      << "DivisionVelocity() "
	      << "Uses three parameters r_th, V_diff and h." << std::endl;
    exit(0);
  }
  if( indValue.size()!=0 ) {
    std::cerr << "Sphere::DivisionVelocity::"
	      << "DivisionVelocity() "
	      << "No variable index is used. The start of the direction"
	      << " of division is assumed to be stored in index 0 "
	      << "(of which data will be taken from the derivatives)." << std::endl;
    exit(0);
  }
  //
  // Set the variable values
  //
  setId("Sphere::divisionVelocity");
  setParameter(paraValue);  
  setVariableIndex(indValue);
  //
  // Set the parameter identities
  //
  std::vector<std::string> tmp( numParameter() );
  tmp.resize( numParameter() );
  tmp[0] = "r_th";
  tmp[1] = "V_diff";
  tmp[2] = "h";
  setParameterId( tmp );
  //
  // Set the number of compartment change (+1 for division)
  //
  setNumChange(1);
}

int Sphere::DivisionVelocity::
flag(Compartment &compartment,
     std::vector< std::vector<double> > &y,
     std::vector< std::vector<double> > &dydt ) {
  
  if( y[compartment.index()][compartment.numTopologyVariable()-1] 
      > parameter(0) ) 
    return 1;
  else 
    return 0;
}

void Sphere::DivisionVelocity::
update(Organism &O, Compartment &compartment,
       std::vector< std::vector<double> > &y,
       std::vector< std::vector<double> > &dydt, double t ) {
  
  int verbose=0;
  
  size_t i=compartment.index();
  if (verbose)
    std::cerr << "Sphere::DivisionVelocity::update  Cell " << i 
	      << " divides at time " << t << "\n"; 
  size_t dim = compartment.numTopologyVariable()-1;
  assert( variableIndex(0,0)<dim);
  double diffFraction=parameter(1);
  double h=parameter(2)*y[i][dim];
  O.addDivision(i,t);
  
  std::vector<double> direction( dim );
  //
  // Create a vector in velocity direction of length h
  //
  double length=0.0;
  for (size_t d=0; d<dim; ++d) {
    direction[i] = dydt[i][d];
    length += direction[i]*direction[i];
  }
  length = h/std::sqrt(length);
  for (size_t d=0; d<dim; ++d) {
    direction[i] *= length;
  }
  //
  // Divide
  //
  size_t NN=O.numCompartment();
  size_t N=NN+1;
  size_t M=compartment.numVariable();
  O.addCompartment( compartment );
  O.compartment(NN).setIndex(NN);
  
  y.resize( N );
  dydt.resize( N );
  y[NN].resize( M );
  dydt[NN].resize( M );
  for( size_t j=0 ; j<M ; j++ ) {
    y[NN][j] = y[i][j];
    dydt[NN][j] = dydt[i][j];
  } 
  
  //Move the mother and daughter symmetrically (only in y-matrix)
  for( size_t d=0 ; d<dim ; d++ ) {
    y[i][d] += 0.5*direction[d];
    y[NN][d] -= 0.5*direction[d];
  }
  
  //Divide mass with two and add some diff (only in y-matrix)
  double newMass = std::pow(y[i][dim],(int) dim)/2.;
  double eps = diffFraction*myRandom::Rnd()*newMass;
  if( eps>newMass ) eps = newMass;//Prevent negative mass...
  if( myRandom::Rnd() > 0.5 ) {
    y[i][dim] = std::pow(newMass+eps,1./double(dim));
    y[NN][dim] = std::pow(newMass-eps,1./double(dim));
  }
  else {
    y[i][dim] = std::pow(newMass-eps,1./double(dim));
    y[NN][dim] = std::pow(newMass+eps,1./double(dim));
  }
  //Add link relationship between mother/daughter
  //assume this is done directly after divisions!!!
}

DivisionCellDirectionSphereBud::
DivisionCellDirectionSphereBud(std::vector<double> &paraValue, 
			       std::vector< std::vector<size_t> > 
			       &indValue ) {
  
  //Do some checks on the parameters and variable indeces
  //////////////////////////////////////////////////////////////////////
  if( paraValue.size()!=3 ) {
    std::cerr << "DivisionCellDirectionSphereBud::"
	      << "DivisionCellDirectionSphereBud() "
	      << "Uses three parameters r_th r_bud f_axial.\n";
    exit(0);
  }
  if( indValue.size() ) {
    std::cerr << "DivisionCellDirectionSphereBud::"
	      << "DivisionCellDirectionSphereBud() "
	      << "No variable index is used.\n";
    exit(0);
  }
  if( paraValue[0]<=0.0 ) {
    std::cerr << "DivisionCellDirectionSphereBud::"
	      << "DivisionCellDirectionSphereBud() "
	      << "parameter(0) (r_th) must be larger than zero.\n";
    exit(0);
  }
  if( paraValue[1]<0.0 ) {
    std::cerr << "DivisionCellDirectionSphereBud::"
	      << "DivisionCellDirectionSphereBud() "
	      << "parameter(1) (r_bud) must be larger or equal to "
	      << "zero.\n";
    exit(0);
  }
  if( paraValue[2]<0.0 || paraValue[2]>1.0 ) {
    std::cerr << "DivisionCellDirectionSphereBud::"
	      << "DivisionCellDirectionSphereBud() "
	      << "parameter(2) (f_axial) must be in [0:1].\n";
    exit(0);
  }
  
  //Set the variable values
  //////////////////////////////////////////////////////////////////////
  setId("divisionCellDirectionSphereBud");
  setParameter(paraValue);  
  setVariableIndex(indValue);
  
  //Set the parameter identities
  //////////////////////////////////////////////////////////////////////
  std::vector<std::string> tmp( numParameter() );
  tmp.resize( numParameter() );
  tmp[0] = "r_budThreshold";
  tmp[1] = "r_bud";
  tmp[1] = "f_axial";

  setParameterId( tmp );

  //Set the number of compartment change (+1 for division)
  //////////////////////////////////////////////////////////////////////
  setNumChange(1);
}

//! Flags for division if the radius is larger than a threshold value
int DivisionCellDirectionSphereBud::
flag(Compartment &compartment,
     std::vector< std::vector<double> > &y,
     std::vector< std::vector<double> > &dydt ) {
  
  if( y[compartment.index()][compartment.numTopologyVariable()-1] > 
      parameter(0) ) 
    return 1;
  else 
    return 0;
}

//! Divides a spherical cell in random direction and w preserved volume
void DivisionCellDirectionSphereBud::
update(Organism &O, Compartment &compartment,
       std::vector< std::vector<double> > &y,
       std::vector< std::vector<double> > &dydt, double t ) {
  
  if( compartment.numTopologyVariable() != 4 &&
      compartment.numTopologyVariable() != 6 &&
      compartment.numTopologyVariable() != 8 ) {
    std::cerr << "DivisionCellDirectionSphereBud::update() "
	      << "Wrong number of dimensions (topology variables).\n";
    exit(-1);
  }
  size_t i=compartment.index();
  size_t tCol = compartment.topologyStart();
  size_t dimension = static_cast<size_t>( (compartment.numTopologyVariable()-2)/2 );
  size_t x1Col=tCol;
  size_t x2Col=tCol+dimension;
  size_t r1Col=tCol+2*dimension;
  size_t r2Col=r1Col+1;
  
  //Add division statistics to organism
  O.addDivision(i,t);
  
  //Extract direction from spatial variables
  //////////////////////////////////////////////////////////////////////
  std::vector<double> direction(dimension);
  for( size_t dim=0 ; dim<dimension ; dim++ )
    direction[dim] = (y[i][x2Col+dim]-y[i][x1Col+dim]);
  
  //Divide
  //////////////////////////////////////////////////////////////////////
  size_t NN=O.numCompartment();
  size_t N=NN+1;
  size_t M=compartment.numVariable();
  O.addCompartment( compartment );
  O.compartment(NN).setIndex(NN);
  
  y.resize( N );
  dydt.resize( N );
  y[NN].resize( M );
  dydt[NN].resize( M );
  for( size_t j=0 ; j<M ; j++ ) {
    y[NN][j] = y[i][j];
    dydt[NN][j] = dydt[i][j];
  } 
  
  //divide sphere bud into two new cells with new buds
  y[NN][r1Col] = y[i][r2Col];
  y[i][r2Col]=y[NN][r2Col]=parameter(1);
  for( size_t dim=0 ; dim<dimension ; dim++ )
    y[NN][x1Col+dim]  = y[i][x2Col+dim];
  

  //Add positions for the new buds
  if( myRandom::Rnd()>parameter(2) ) {//polar division
    for( size_t dim=0 ; dim<dimension ; dim++ )
      direction[dim] = -direction[dim];
  }
  
  //For cell i put the bud at its radius (the direction length is ok) 
  for( size_t dim=0 ; dim<dimension ; dim++ )
    y[i][x2Col+dim]  = y[i][x1Col+dim] + direction[dim];
  //For cell NN set it in opposite direction after rescaling length
  for( size_t dim=0 ; dim<dimension ; dim++ )
    direction[dim] *= y[NN][r1Col]/y[i][r1Col];
  for( size_t dim=0 ; dim<dimension ; dim++ )
    y[NN][x2Col+dim]  = y[NN][x1Col+dim] - direction[dim];
  
  //Add link relationship between mother/daughter
  //assume this is done directly after divisions!!!
}


DivisionCellDirectionCigar::
DivisionCellDirectionCigar(std::vector<double> &paraValue, 
			   std::vector< std::vector<size_t> > 
			   &indValue ) {
  
  //Do some checks on the parameters and variable indeces
  //////////////////////////////////////////////////////////////////////
  if( paraValue.size()!=4 ) {
    std::cerr << "DivisionCellDirectionCigar::"
	      << "DivisionCellDirectionCigar() "
	      << "Uses four parameters b d_th d_diff d_random.\n";
    exit(0);
  }
  if( indValue.size() ) {
    std::cerr << "DivisionCellDirectionCigar::"
	      << "DivisionCellDirectionCigar() "
	      << "No variable index is used.\n";
    exit(0);
  }
  //Set the variable values
  //////////////////////////////////////////////////////////////////////
  setId("divisionCellDirectionCigar");
  setParameter(paraValue);  
  setVariableIndex(indValue);
  
  //Set the parameter identities
  //////////////////////////////////////////////////////////////////////
  std::vector<std::string> tmp( numParameter() );
  tmp.resize( numParameter() );
  tmp[0] = "b";
  tmp[1] = "d_th";
  tmp[2] = "d_diff";
  tmp[3] = "d_random";
  
  setParameterId( tmp );

  //Set the number of compartment change (+1 for division)
  //////////////////////////////////////////////////////////////////////
  setNumChange(1);
}

//! Flags for division if the radius is larger than a threshold value
int DivisionCellDirectionCigar::
flag(Compartment &compartment,
     std::vector< std::vector<double> > &y,
     std::vector< std::vector<double> > &dydt ) {
  
  if( y[compartment.index()][compartment.numTopologyVariable()-1] > 
      parameter(1) ) 
    return 1;
  else 
    return 0;
}

//! Divides a cigar-shaped cell
void DivisionCellDirectionCigar::
update(Organism &O, Compartment &compartment,
       std::vector< std::vector<double> > &y,
       std::vector< std::vector<double> > &dydt, double t ) {

  assert( compartment.numTopologyVariable() == 5 || 
	  compartment.numTopologyVariable() == 7  ||
	  compartment.numTopologyVariable() == 9 );
  
  size_t i=compartment.index();
  size_t dimension = static_cast<size_t>(compartment.numTopologyVariable()-1)/2;
  if(compartment.numTopologyVariable() == 9 )
      dimension = 2;
  std::vector<size_t> x1Col(dimension),x2Col(dimension);
  for( size_t d=0 ; d<dimension ; d++ ) {
    x1Col[d] = d;
    x2Col[d] = d+dimension;
  }
  size_t dCol = compartment.numTopologyVariable()-1;
  
  O.addDivision(i,t);
  
  std::vector<double> center(dimension), direction(dimension);
  
  //Extract direction from spatial variables
  //////////////////////////////////////////////////////////////////////
  double length = 0.0;
  for( size_t d=0 ; d<dimension ; d++ ) {
    center[d] = 0.5*(y[i][x2Col[d]]+y[i][x1Col[d]]);
    direction[d] = (y[i][x2Col[d]]-y[i][x1Col[d]]);
    length += direction[d]*direction[d];
  }
  length = std::sqrt( length ); 
  for( size_t d=0 ; d<dimension ; d++ )
    direction[d] /=length; 
  
  //Divide
  //////////////////////////////////////////////////////////////////////
  size_t NN=O.numCompartment();
  size_t N=NN+1;
  size_t M=compartment.numVariable();
  O.addCompartment( compartment );
  O.compartment(NN).setIndex(NN);
  
  y.resize( N );
  dydt.resize( N );
  y[NN].resize( M );
  dydt[NN].resize( M );
  for( size_t j=0 ; j<M ; j++ ) {
    y[NN][j] = y[i][j];
    dydt[NN][j] = dydt[i][j];
  } 
  
  //divide d into two almost equal sizes using length parameter
  //////////////////////////////////////////////////
  //double da = 0.5*y[i][dCol]*( 1.0 + y[i][dCol]*parameter(2)*
  //		       (2.0*myRandom::Rnd()-1.0) );
  //double db = y[i][dCol]-da;
  //divide d into two almost equal sizes using length from positions
  //////////////////////////////////////////////////
	//Old da calculation using an additional length which gives different 
	//asymmetries for different lengths...
  //double da = 0.5*length*( 1.0 + length*parameter(2)*
	//	   (2.0*myRandom::Rnd()-1.0) );
  double da = 0.5*length*( 1.0 + parameter(2)*
			   (2.0*myRandom::Rnd()-1.0) );
  double db = length-da;
  //Minimize forces
  y[i][dCol] = da-parameter(0);
  y[NN][dCol] = db-parameter(0);;
  //Mass "conserved"
  //y[i][dCol] = da;
  //y[NN][dCol] = db;
  assert( y[i][dCol]>0.0 && y[NN][dCol]>0.0 );
  
  //Add center point to first/second point for the new cells
  for( size_t d=0 ; d<dimension ; d++ ) {
    y[i][x2Col[d]] = y[i][x1Col[d]]  + 
      (da-parameter(0))*direction[d]+parameter(3)*(myRandom::Rnd()-0.5);
    y[NN][x1Col[d]] = y[NN][x2Col[d]] - 
      (db-parameter(0))*direction[d]+parameter(3)*(myRandom::Rnd()-0.5);
    //y[i][x2Col[d]] = y[i][x1Col[d]] + da*direction[d];
    //y[NN][x1Col[d]] = y[NN][x2Col[d]]-db*direction[d];
  }
  //Add link relationship between mother/daughter
  //assume this is done directly after divisions!!!
}

DivisionCellDirectionCigarStochastic::
DivisionCellDirectionCigarStochastic(std::vector<double> &paraValue, 
			   std::vector< std::vector<size_t> > 
			   &indValue ) {
  
  //Do some checks on the parameters and variable indeces
  //////////////////////////////////////////////////////////////////////
  if( paraValue.size()!=4 ) {
    std::cerr << "DivisionCellDirectionCigarStochastic::"
	      << "DivisionCellDirectionCigarStochastic() "
	      << "Uses four parameters b d_th d_diff d_random.\n";
    exit(0);
  }
  if( indValue.size() ) {
    std::cerr << "DivisionCellDirectionCigarStochastic::"
	      << "DivisionCellDirectionCigarStochastic() "
	      << "No variable index is used.\n";
    exit(0);
  }
  //Set the variable values
  //////////////////////////////////////////////////////////////////////
  setId("divisionCellDirectionCigarStochastic");
  setParameter(paraValue);  
  setVariableIndex(indValue);
  
  //Set the parameter identities
  //////////////////////////////////////////////////////////////////////
  std::vector<std::string> tmp( numParameter() );
  tmp.resize( numParameter() );
  tmp[0] = "b";
  tmp[1] = "d_th";
  tmp[2] = "d_diff";
  tmp[3] = "d_random";
  
  setParameterId( tmp );

  //Set the number of compartment change (+1 for division)
  //////////////////////////////////////////////////////////////////////
  setNumChange(1);
}

int DivisionCellDirectionCigarStochastic::
flag(Compartment &compartment,
     std::vector< std::vector<double> > &y,
     std::vector< std::vector<double> > &dydt ) {
  
  if( y[compartment.index()][compartment.numTopologyVariable()-1] > 
      parameter(1) ) 
    return 1;
  else 
    return 0;
}

void DivisionCellDirectionCigarStochastic::
update(Organism &O, Compartment &compartment,
       std::vector< std::vector<double> > &y,
       std::vector< std::vector<double> > &dydt, double t ) {

  assert( compartment.numTopologyVariable() == 5 || 
	  compartment.numTopologyVariable() == 7  ||
	  compartment.numTopologyVariable() == 9 );
  
  size_t i=compartment.index();
  size_t dimension = static_cast<size_t>(compartment.numTopologyVariable()-1)/2;
  if(compartment.numTopologyVariable() == 9 )
      dimension = 2;
  std::vector<size_t> x1Col(dimension),x2Col(dimension);
  for( size_t d=0 ; d<dimension ; d++ ) {
    x1Col[d] = d;
    x2Col[d] = d+dimension;
  }
  size_t dCol = compartment.numTopologyVariable()-1;
  
  O.addDivision(i,t);
  
  std::vector<double> center(dimension), direction(dimension);
  
  //Extract direction from spatial variables
  //////////////////////////////////////////////////////////////////////
  double length = 0.0;
  for( size_t d=0 ; d<dimension ; d++ ) {
    center[d] = 0.5*(y[i][x2Col[d]]+y[i][x1Col[d]]);
    direction[d] = (y[i][x2Col[d]]-y[i][x1Col[d]]);
    length += direction[d]*direction[d];
  }
  length = std::sqrt( length ); 
  for( size_t d=0 ; d<dimension ; d++ )
    direction[d] /=length; 
  
  //Divide
  //////////////////////////////////////////////////////////////////////
  size_t NN=O.numCompartment();
  size_t N=NN+1;
  size_t M=compartment.numVariable();
  O.addCompartment( compartment );
  O.compartment(NN).setIndex(NN);
  
  y.resize( N );
  dydt.resize( N );
  y[NN].resize( M );
  dydt[NN].resize( M );
  for( size_t j=0 ; j<M ; j++ ) {
    y[NN][j] = y[i][j]; 
    dydt[NN][j] = dydt[i][j];
  } 
  
  //divide d into two almost equal sizes using length parameter
  //////////////////////////////////////////////////
  //double da = 0.5*y[i][dCol]*( 1.0 + y[i][dCol]*parameter(2)*
  //		       (2.0*myRandom::Rnd()-1.0) );
  //double db = y[i][dCol]-da;
  //divide d into two almost equal sizes using length from positions
  //////////////////////////////////////////////////
	//Old da calculation using an additional length which gives different 
	//asymmetries for different lengths...
  //double da = 0.5*length*( 1.0 + length*parameter(2)*
	//	   (2.0*myRandom::Rnd()-1.0) );
  double da = 0.5*length*( 1.0 + parameter(2)*
			   (2.0*myRandom::Rnd()-1.0) );
  double db = length-da;
  //Minimize forces
  y[i][dCol] = da-parameter(0);
  y[NN][dCol] = db-parameter(0);;
  //Mass "conserved"
  //y[i][dCol] = da;
  //y[NN][dCol] = db;
  assert( y[i][dCol]>0.0 && y[NN][dCol]>0.0 );
  
  //Add center point to first/second point for the new cells
  for( size_t d=0 ; d<dimension ; d++ ) {
    y[i][x2Col[d]] = y[i][x1Col[d]]  + 
      (da-parameter(0))*direction[d]+parameter(3)*(myRandom::Rnd()-0.5);
    y[NN][x1Col[d]] = y[NN][x2Col[d]] - 
      (db-parameter(0))*direction[d]+parameter(3)*(myRandom::Rnd()-0.5);
    //y[i][x2Col[d]] = y[i][x1Col[d]] + da*direction[d];
    //y[NN][x1Col[d]] = y[NN][x2Col[d]]-db*direction[d];
  }
  // Put all molecules in one of the daughter cells to get maximal difference...
  for( size_t j=5 ; j<M ; j++ ) {
    double asymmetryFactor = myRandom::Rnd();
    y[NN][j] = 2.0 * asymmetryFactor * y[i][j]; 
    y[i][j] = 2.0* (1.0 - asymmetryFactor) * y[i][j];
    dydt[NN][j] = 0.0;
  } 
  //Add link relationship between mother/daughter
  //assume this is done directly after divisions!!!
}

DivisionLens::
DivisionLens(std::vector<double> &paraValue, 
						 std::vector< std::vector<size_t> > 
						 &indValue ) 
{  
  //Do some checks on the parameters and variable indeces
  if (paraValue.size()!=1) {
    std::cerr << "DivisionLens::"
							<< "DivisionLens() "
							<< "Uses one parameter r_th.\n";
    exit(0);
  }
  if (indValue.size()!=1 || indValue[0].size()!=2) {
    std::cerr << "DivisionLens::"
							<< "DivisionLens() "
							<< "Variable index for dr and growthIndicator "
							<< "variable is used.\n";
    exit(0);
  }
  //Set the variable values
  //////////////////////////////////////////////////////////////////////
  setId("divisionLens");
  setParameter(paraValue);  
  setVariableIndex(indValue);
  
  //Set the parameter identities
  //////////////////////////////////////////////////////////////////////
  std::vector<std::string> tmp( numParameter() );
  tmp.resize( numParameter() );
  tmp[0] = "r_th";
  setParameterId( tmp );
	
  //Set the number of compartment change (+1 for division)
  //////////////////////////////////////////////////////////////////////
  setNumChange(1);
}

//! Flags for division if the dr is larger than a threshold value
int DivisionLens::
flag(Compartment &compartment,
     std::vector< std::vector<double> > &y,
     std::vector< std::vector<double> > &dydt ) {
  
  if( compartment.index()==y.size()-1 && 
			y[compartment.index()][variableIndex(0,0)] 
      > parameter(0) ) 
    return 1;
  else 
    return 0;
}

//! Divides a spherical cell in random direction and w preserved volume
void DivisionLens::
update(Organism &O, Compartment &compartment,
       std::vector< std::vector<double> > &y,
       std::vector< std::vector<double> > &dydt, double t ) {

  size_t i=compartment.index();
  std::cerr << "DivisionLens::update  Cell " << i 
						<< " divides at time " << t << "\n"; 
  O.addDivision(i,t);

  //Divide
  //////////////////////////////////////////////////////////////////////
  size_t NN=O.numCompartment();
  size_t N=NN+1;
  size_t M=compartment.numVariable();
  O.addCompartment( compartment );
  O.compartment(NN).setIndex(NN);
  
  y.resize( N );
  dydt.resize( N );
  y[NN].resize( M );
  dydt[NN].resize( M );
  for( size_t j=0 ; j<M ; j++ ) {
    y[NN][j] = y[i][j];
    dydt[NN][j] = dydt[i][j];
  } 
	// Divide dr and assume r is beefore dr
	y[i][variableIndex(0,0)] *= 0.5;
	y[NN][variableIndex(0,0)] *= 0.5;
	y[i][variableIndex(0,0)-1] -= 0.5*y[NN][variableIndex(0,0)];
	y[NN][variableIndex(0,0)-1] += 0.5*y[NN][variableIndex(0,0)];	
	y[i][variableIndex(0,1)] =0.0;
}




Sphere::DivisionRandomDirectionFlag::
DivisionRandomDirectionFlag(std::vector<double> &paraValue,
                        std::vector< std::vector<size_t> >
                        &indValue ) {
    
    //Do some checks on the parameters and variable indeces
    //////////////////////////////////////////////////////////////////////
    if( paraValue.size()!=3 ) {
        std::cerr << "Sphere::DivisionRandomDirectionFlag::"
        << "DivisionRandomDirectionFlag() "
        << "Uses three parameters r_th, V_diff and h.\n";
        exit(0);
    }

  if( indValue.size()!=1 || indValue[0].size()!=1 ) {
       	 std::cerr << "Sphere::DivisionRandomDirectionFlag::"
         << "DivisionRandomDirectionFlag() "
	 << "A single variable index is used to specify the variable"
	      << " that will be used as a flag.\n";
    exit(0);

    }
    //Set the variable values
    //////////////////////////////////////////////////////////////////////
    setId("Sphere::DivisionRandomDirectionFlag");
    setParameter(paraValue);
    setVariableIndex(indValue);
    
    //Set the parameter identities
    //////////////////////////////////////////////////////////////////////
    std::vector<std::string> tmp( numParameter() );
    tmp.resize( numParameter() );
    tmp[0] = "r_th";
    tmp[1] = "V_diff";
    tmp[2] = "h";
    setParameterId( tmp );
    
    //Set the number of compartment change (+1 for division)
    //////////////////////////////////////////////////////////////////////
    setNumChange(1);
}

//! Flags for division if the radius is larger than a threshold value
int Sphere::DivisionRandomDirectionFlag::
flag(Compartment &compartment,
     std::vector< std::vector<double> > &y,
     std::vector< std::vector<double> > &dydt ) {
    
    if( y[compartment.index()][compartment.numTopologyVariable()-1]
       > parameter(0)  && 
       y[compartment.index()][variableIndex(0,0)]
       == 0
 )
        return 1;
    else
        return 0;
}

//! Divides a spherical cell in random direction and w preserved volume
void Sphere::DivisionRandomDirectionFlag::
update(Organism &O, Compartment &compartment,
       std::vector< std::vector<double> > &y,
       std::vector< std::vector<double> > &dydt, double t ) {
    
    int verbose=0;
    size_t i=compartment.index();
    if (verbose)
        std::cerr << "Sphere::DivisionRandomDirectionFlag::update  Cell " << i
        << " divides at time " << t << "\n";
    size_t dim = compartment.numTopologyVariable()-1;
    double PI=3.14159;
    double diffFraction=parameter(1);
    double h=parameter(2)*y[i][dim];
    O.addDivision(i,t);
    
    std::vector<double> direction( dim );
    
    //Create a vector in a random direction (uniformly)
    //////////////////////////////////////////////////////////////////////
    if( dim==3 ) {
        double phi = myRandom::Rnd()*2.*PI;
        double theta = myRandom::Rnd()*PI;
        double sinTheta = sin(theta);
        direction[0] = cos(phi)*sinTheta;
        direction[1] = sin(phi)*sinTheta;
        direction[2] = cos(theta);
    }
    else if( dim==2 ) {
        double phi = myRandom::Rnd()*2*PI;
        direction[0] = cos(phi);
        direction[1] = sin(phi);
    }
    else if( dim==1 ) {
        if( myRandom::Rnd()>0.5 )
            direction[0]=1;
        else
            direction[0]=-1;
    }
    else {
        std::cerr << "Sphere::DivisionRandomDirectionFlag::update\n"
        << "Wrong dimension for dividing compartment.\n";
        std::cerr << dim << "\n";
        exit(-1);
    }
    //Change the direction vector elements to an vector of length h
    double norm=0.;
    for( size_t d=0 ; d<direction.size() ; d++ )
        norm += direction[d]*direction[d];
    if( norm<=0 ) {
        std::cerr << "Sphere::DivisionRandomDirectionFlag::update() "
        << "No length in directional vector!\n";
        exit(-1);
    }
    norm = std::sqrt( norm );
    h = h/norm;//normalizing AND adjusting length at the same time
    for( size_t d=0 ; d<dim ; d++ )
        direction[d] *= h;
    
    //Divide
    //////////////////////////////////////////////////////////////////////
    size_t NN=O.numCompartment();
    size_t N=NN+1;
    size_t M=compartment.numVariable();
    O.addCompartment( compartment );
    O.compartment(NN).setIndex(NN);
    
    y.resize( N );
    dydt.resize( N );
    y[NN].resize( M );
    dydt[NN].resize( M );
    for( size_t j=0 ; j<M ; j++ ) {
        y[NN][j] = y[i][j];
        dydt[NN][j] = dydt[i][j];
    }
    
    //Move the mother and daughter symmetrically (only in y-matrix)
    for( size_t d=0 ; d<dim ; d++ ) {
        y[i][d] += 0.5*direction[d];
        y[NN][d] -= 0.5*direction[d];
    }
    
    //Divide mass with two and add some diff (only in y-matrix)
    double newMass = std::pow(y[i][dim],(int)dim)/2.;
    double eps = diffFraction*myRandom::Rnd()*newMass;
    if( eps>newMass ) eps = newMass;//Prevent negative mass...
    if( myRandom::Rnd() > 0.5 ) {
        y[i][dim] = std::pow(newMass+eps,(double) (1./double(dim)));
        y[NN][dim] = std::pow(newMass-eps,(double) (1./double(dim)));
    }
    else {
        y[i][dim] = std::pow(newMass-eps,(double) (1./double(dim)));
        y[NN][dim] = std::pow(newMass+eps,(double) (1./double(dim)));
    }
    //Add link relationship between mother/daughter
    //assume this is done directly after divisions!!!
}



