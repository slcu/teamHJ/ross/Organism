/**
 * Filename     : compartmentRemoval.cc
 * Description  : Classes describing compartment removals
 * Author(s)    : Henrik Jonsson (henrik@thep.lu.se)
 * Created      : September 2005
 * Revision     : $Id: compartmentRemoval.cc 648 2015-11-26 13:45:20Z andre $
 */
#include<cmath>

#include"compartmentRemoval.h"
#include"baseCompartmentChange.h"

//!Constructor for the RemovalThreshold class
RemovalThreshold::RemovalThreshold(std::vector<double> &paraValue, 
				   std::vector< std::vector<size_t> > 
				   &indValue ) {
  
  //Do some checks on the parameters and variable indeces
  //////////////////////////////////////////////////////////////////////
  if( paraValue.size()!=2 ) {
    std::cerr << "RemovalThreshold::RemovalThreshold() "
	      << "Uses two parameters X_th and sign.\n";
    exit(0);
  }
  if( indValue.size()!=1 || indValue[0].size() !=1 ) {
    std::cerr << "RemovalThreshold::RemovalThreshold() "
	      << "One variable index indicating threshold variable "
	      << "is used.\n";
    exit(0);
  }
  //Set the variable values
  //////////////////////////////////////////////////////////////////////
  setId("removalThreshold");
  setParameter(paraValue);  
  setVariableIndex(indValue);
  
  //Set the parameter identities
  //////////////////////////////////////////////////////////////////////
  std::vector<std::string> tmp( numParameter() );
  tmp.resize( numParameter() );
  tmp[0] = "X_th";
  tmp[1] = "sign";
  setParameterId( tmp );

  //Set the number of compartment change (-1 for removal)
  //////////////////////////////////////////////////////////////////////
  setNumChange(-1);
}

//! Flags for removal if a variable is larger/smaller than a threshold value
int RemovalThreshold::
flag(Compartment &compartment,
     std::vector< std::vector<double> > &y,
     std::vector< std::vector<double> > &dydt ) {
  
  if( parameter(1)>0.0 && 
      y[compartment.index()][variableIndex(0,0)] > parameter(0) ) 
    return 1;
  else if( parameter(1)<0.0 && 
	   y[compartment.index()][variableIndex(0,0)] < parameter(0) ) 
    return 1;
  else 
    return 0;
}

//! Removes a spherical cell 
void RemovalThreshold::
update(Organism &O, Compartment &compartment,
       std::vector< std::vector<double> > &y,
       std::vector< std::vector<double> > &dydt, double t ) {

  size_t i=compartment.index();
  //Remove the compartment and update neighborhood in Organism
  O.removeCompartment(i);
  //Remove the row in y and dydt.
  size_t NN=O.numCompartment();//(one compartment already removed)
  y[i] = y[NN];
  dydt[i] = dydt[NN];
  y.pop_back();
  dydt.pop_back();
  //i--;//Since the last one is moved into i and needs to be checked
}    

//!Constructor for the RemovalRadialThreshold class
RemovalRadialThreshold::RemovalRadialThreshold(std::vector<double> &paraValue, 
				   std::vector< std::vector<size_t> > 
				   &indValue ) {
  
  //Do some checks on the parameters and variable indeces
  //////////////////////////////////////////////////////////////////////
  if( paraValue.size()!=1 ) {
    std::cerr << "RemovalRadialThreshold::RemovalRadialThreshold() "
	      << "Uses one parameter R_th.\n";
    exit(0);
  }
  if( indValue.size() ) {
    std::cerr << "RemovalRadialThreshold::RemovalRadialThreshold() "
	      << "No variable index is used.\n";
    exit(0);
  }
  //Set the variable values
  //////////////////////////////////////////////////////////////////////
  setId("removalRadialThreshold");
  setParameter(paraValue);  
  setVariableIndex(indValue);
  
  //Set the parameter identities
  //////////////////////////////////////////////////////////////////////
  std::vector<std::string> tmp( numParameter() );
  tmp.resize( numParameter() );
  tmp[0] = "R_th";
  setParameterId( tmp );

  //Set the number of compartment change (-1 for removal)
  //////////////////////////////////////////////////////////////////////
  setNumChange(-1);
}

//! Flags for removalRadial if a variable is larger/smaller than a threshold value
int RemovalRadialThreshold::
flag(Compartment &compartment,
     std::vector< std::vector<double> > &y,
     std::vector< std::vector<double> > &dydt ) {
  size_t i=compartment.index();
  size_t dim=compartment.numTopologyVariable()-1;
  double R = 0.0;
  for( size_t d=0 ; d<dim ; d++ )
    R += y[i][d]*y[i][d];
  R = std::sqrt( R );
  if( R > parameter(0) ) 
    return 1;
  else 
    return 0;
}

//! Removes a spherical cell 
void RemovalRadialThreshold::
update(Organism &O, Compartment &compartment,
       std::vector< std::vector<double> > &y,
       std::vector< std::vector<double> > &dydt, double t ) {
  
  size_t i=compartment.index();
  //Remove the compartment and update neighborhood in Organism
  O.removeCompartment(i);
  //Remove the row in y and dydt.
  size_t NN=O.numCompartment();//(one compartment already removed)
  y[i] = y[NN];
  dydt[i] = dydt[NN];
  y.pop_back();
  dydt.pop_back();
  //i--;//Since the last one is moved into i and needs to be checked
}    

//!Constructor for the RemovalThreshold class
RemovalThresholdBactPos::
RemovalThresholdBactPos(std::vector<double> &paraValue, 
			std::vector< std::vector<size_t> > 
			&indValue ) {
  
  //Do some checks on the parameters and variable indeces
  //////////////////////////////////////////////////////////////////////
  if( paraValue.size()!=2 ) {
    std::cerr << "RemovalThreshold::RemovalThreshold() "
	      << "Uses two parameters X_th and sign.\n";
    exit(0);
  }
  if( paraValue[1] != 1 && paraValue[1] != -1 ) {
    std::cerr << "RemovalThreshold::RemovalThreshold() "
	      << "Parameter 2 (sign) must be 1 (above) or -1 (below)."
	      << std::endl;
    exit(0);
  }
  if( indValue.size()!=1 || indValue[0].size() !=1 ) {
    std::cerr << "RemovalThreshold::RemovalThreshold() "
	      << "One variable index indicating the positional threshold "
	      << "variable used.\n";
    exit(0);
  }
  //Set the variable values
  //////////////////////////////////////////////////////////////////////
  setId("removalThresholdBactPos");
  setParameter(paraValue);  
  setVariableIndex(indValue);
  
  //Set the parameter identities
  //////////////////////////////////////////////////////////////////////
  std::vector<std::string> tmp( numParameter() );
  tmp.resize( numParameter() );
  tmp[0] = "X_th";
  tmp[1] = "sign";
  setParameterId( tmp );
  
  //Set the number of compartment change (-1 for removal)
  //////////////////////////////////////////////////////////////////////
  setNumChange(-1);
}

//! Flags for removal if a variable is larger/smaller than a threshold value
int RemovalThresholdBactPos::
flag(Compartment &compartment,
     std::vector< std::vector<double> > &y,
     std::vector< std::vector<double> > &dydt ) {
 
  if( compartment.numTopologyVariable() != 5 && 
      compartment.numTopologyVariable() != 7 ) {
    std::cerr << "RemovalThresholdBactPos::flag() "
	      << "Wrong number of topology variables for a bact!\n";
    exit(0);
  }
  size_t dim = static_cast<size_t>( (compartment.numTopologyVariable()-1)/2 ); 
  if( variableIndex(0,0) >= dim ) {
    std::cerr << "RemovalThresholdBactPos::flag() "
	      << "Only positional variable allowed for.\n";
    exit(0);
  }
  if( parameter(1)>0.0 && 
      y[compartment.index()][variableIndex(0,0)] > parameter(0) &&
      y[compartment.index()][variableIndex(0,0)+dim] > parameter(0) ) 
    return 1;
  else if( parameter(1)<0.0 && 
	   y[compartment.index()][variableIndex(0,0)] < parameter(0) &&
	   y[compartment.index()][variableIndex(0,0)+dim] < parameter(0) ) 
    return 1;
  else 
    return 0;
}

//! Removes the cell
void RemovalThresholdBactPos::
update(Organism &O, Compartment &compartment,
       std::vector< std::vector<double> > &y,
       std::vector< std::vector<double> > &dydt, double t ) {
  
  size_t i=compartment.index();
  //Remove the compartment and update neighborhood in Organism
  O.removeCompartment(i);
  //Remove the row in y and dydt.
  size_t NN=O.numCompartment();//(one compartment already removed)
  y[i] = y[NN];
  dydt[i] = dydt[NN];
  y.pop_back();
  dydt.pop_back();
}    

//!Constructor for the RemovalCylinderThreshold class
RemovalCylinderThreshold::RemovalCylinderThreshold(std::vector<double> &paraValue, 
				   std::vector< std::vector<size_t> > 
				   &indValue ) {
  
  //Do some checks on the parameters and variable indeces
  //////////////////////////////////////////////////////////////////////
  if( paraValue.size()!=1 ) {
    std::cerr << "RemovalCylinderThreshold::RemovalCylinderThreshold() "
	      << "Uses one parameter R_c.\n";
    exit(0);
  }
  if( indValue.size() ) {
    std::cerr << "RemovalRadialThreshold::RemovalRadialThreshold() "
	      << "No variable index is used.\n";
    exit(0);
  }
  //Set the variable values
  //////////////////////////////////////////////////////////////////////
  setId("removalCylinderThreshold");
  setParameter(paraValue);  
  setVariableIndex(indValue);
  
  //Set the parameter identities
  //////////////////////////////////////////////////////////////////////
  std::vector<std::string> tmp( numParameter() );
  tmp.resize( numParameter() );
  tmp[0] = "R_c";
  setParameterId( tmp );

  //Set the number of compartment change (-1 for removal)
  //////////////////////////////////////////////////////////////////////
  setNumChange(-1);
}

//! Flags for removalRadial if a variable is larger/smaller than a threshold value
int RemovalCylinderThreshold::
flag(Compartment &compartment,
     std::vector< std::vector<double> > &y,
     std::vector< std::vector<double> > &dydt ) {
  size_t i=compartment.index();
  size_t dim=compartment.numTopologyVariable()-1;
  if(dim > 2) dim = 2; // only use first two coordinates to get cylinder
  double R = 0.0;
  for( size_t d=0 ; d<dim ; d++ )
    R += y[i][d]*y[i][d];
  R = std::sqrt( R );
  if( R > parameter(0) ) 
    return 1;
  else 
    return 0;
}

//! Removes a spherical cell 
void RemovalCylinderThreshold::
update(Organism &O, Compartment &compartment,
       std::vector< std::vector<double> > &y,
       std::vector< std::vector<double> > &dydt, double t ) {
  
  size_t i=compartment.index();
  //Remove the compartment and update neighborhood in Organism
  O.removeCompartment(i);
  //Remove the row in y and dydt.
  size_t NN=O.numCompartment();//(one compartment already removed)
  y[i] = y[NN];
  dydt[i] = dydt[NN];
  y.pop_back();
  dydt.pop_back();
  //i--;//Since the last one is moved into i and needs to be checked
}    
