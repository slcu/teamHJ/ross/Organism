/**
 * Filename     : baseCompartmentChange.h
 * Description  : The common base for classes describing comp updates
 * Author(s)    : Henrik Jonsson (henrik@thep.lu.se)
 * Created      : September 2005
 * Revision     : $Id: baseCompartmentChange.h 643 2015-08-20 08:08:11Z henrik $
 */
#ifndef BASECOMPARTMENTCHANGE_H
#define BASECOMPARTMENTCHANGE_H

#include<vector>
#include<string>
#include<iostream>
#include<fstream>

//#include"organism.h"
#include"../compartment.h"

class Organism;

//! A base class for describing diff equation updates for dynamical variables
/*! The BaseCompartmentChange class is a base class used when defining
  different types of compartmental updates such as divisions and
  removal. Each class uses a vector of parameters and variable indeces
  to ask whether the update is to be made and then also makes an
  update. The variable indeces are devided into multiple layers to
  allow for different types of contributions for different
  variables. The baseCompartmentChange class can be seen as a
  'factory' creating different types of updates.
*/ 
class BaseCompartmentChange {
  
 private:
  
  std::string id_;
  std::vector<double> parameter_;           
  std::vector<std::string> parameterId_;           
  std::vector< std::vector<size_t> > variableIndex_;
  int numChange_;
 public:
  
  ///
  /// @brief Factory constructor, all constructors should be mapped onto this one 
  ///
  /// Given the idValue a compartmentChange of the defined type is returned
  /// (using new Class).
  ///  
  static BaseCompartmentChange* 
    createCompartmentChange(std::vector<double> &paraValue, 
			    std::vector< std::vector<size_t> > 
			    &indValue, const std::string &idValue );
  ///
  /// @brief This constructor reads from an open file and then calls for the main constructor
  ///
  static BaseCompartmentChange* 
    createCompartmentChange( std::istream &IN ); 
  
  //Constructor/destructor not defined!
  //BaseCompartmentChange();
  virtual ~BaseCompartmentChange();
  
  BaseCompartmentChange & operator=( const BaseCompartmentChange & 
				     baseCompartmentChangeCopy );
  
  // Get values
  inline std::string id() const;
  inline size_t numParameter() const;  
  inline size_t numVariableIndexLevel() const;
  inline size_t numVariableIndex(size_t level) const;
  inline int numChange() const;  
  
  inline double parameter(size_t i) const;
  inline double& parameterAddress(size_t i);
  inline std::string parameterId(size_t i) const;
  inline size_t variableIndex(size_t i,size_t j) const;
  
  // Set values
  inline void setId(const std::string &value);
  inline void setParameter(size_t i,double value);
  inline void setParameter(std::vector<double> &value);
  inline void setParameterId(size_t i,const std::string &value);
  inline void setParameterId(std::vector<std::string> &value);
  inline void setVariableIndex(size_t i, size_t j,size_t value);
  inline void setVariableIndex(size_t i, std::vector<size_t> &value);
  inline void setVariableIndex(std::vector< std::vector<size_t> > &value);
  inline void setNumChange(int value);  

  virtual int flag(Compartment &compartment,
		   std::vector< std::vector<double> > &y,
		   std::vector< std::vector<double> > &dydt );
  virtual void update(Organism &O,Compartment &compartment,
		      std::vector< std::vector<double> > &y,
		      std::vector< std::vector<double> > &dydt, 
		      double t=0.0 );  
  ///
  /// @brief Prints the Compartment change reaction in Cambium format
  /// 
  /// @see Organism::printCambium()
  ///
  virtual void printCambium( std::ostream &os) const;

};

//!Returns the id string
inline std::string BaseCompartmentChange::id() const {
  return id_;
}

//!Returns the number of parameters
inline size_t BaseCompartmentChange::numParameter() const {
  return parameter_.size();
}

//!Returns the number of variable-index levels
inline size_t BaseCompartmentChange::numVariableIndexLevel() const {
  return variableIndex_.size();
}

//!Returns the number of variable-index within one level 
inline size_t BaseCompartmentChange::
numVariableIndex(size_t level) const {
  return variableIndex_[level].size();
}

//!Returns a parameter
inline double BaseCompartmentChange::parameter(size_t i) const {
  return parameter_[i];
}

//!Returns a reference of a parameter
inline double& BaseCompartmentChange::parameterAddress(size_t i) {
  return parameter_[i];
}

//!Returns the id for a parameter
inline std::string BaseCompartmentChange::parameterId(size_t i) const {
  return parameterId_[i];
}

//!Returns the index of variable used in the update equation
inline size_t BaseCompartmentChange::
variableIndex(size_t i,size_t j) const {
  return variableIndex_[i][j];
}

//!Returns the net number of compartment change
inline int BaseCompartmentChange::numChange() const {
  return numChange_;
}

//!Sets the id string
inline void BaseCompartmentChange::setId(const std::string &value) {
  id_=value;
}

//!Sets the parameter value with index i
inline void BaseCompartmentChange::setParameter(size_t i,double value) {
  parameter_[i]=value;
}

//!Sets all parameters within a compartmentChange
inline void BaseCompartmentChange::
setParameter(std::vector<double> &value) {
  parameter_ = value;
}

//!Sets the parameter id value with index i
inline void BaseCompartmentChange::
setParameterId(size_t i,const std::string &value) {
  parameterId_[i]=value;
}

//!Sets all parameter id's within a compartmentChange
inline void BaseCompartmentChange::
setParameterId(std::vector<std::string> &value) {
  parameterId_=value;
}

//!Sets the variable index at specific level and place
inline void BaseCompartmentChange::
setVariableIndex(size_t i, size_t j,size_t value) {
  variableIndex_[i][j] = value;
}

//!Sets the variable indeces for a given level
inline void BaseCompartmentChange::
setVariableIndex(size_t i, std::vector<size_t> &value) {
  variableIndex_[i] = value;
}

//!Sets the variable indeces used in a compartmentChange
inline void BaseCompartmentChange::
setVariableIndex(std::vector< std::vector<size_t> > &value) {
  variableIndex_ = value;
}

//!Sets the number of net change in compartments 
inline void BaseCompartmentChange::setNumChange(int value) {
  numChange_=value;
}

#endif


