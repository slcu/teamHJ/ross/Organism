/**
 * Filename     : baseCompartmentChange.cc
 * Description  : A base class describing variable updates
 * Author(s)    : Henrik Jonsson (henrik@thep.lu.se)
 * Created      : October 2003
 * Revision     : $Id: baseCompartmentChange.cc 648 2015-11-26 13:45:20Z andre $
 */
#include<vector>
#include<boost/algorithm/string.hpp>

#include "baseCompartmentChange.h"
#include "compartmentDivision.h"
#include "compartmentRemoval.h"
#include "../reactions/elongatedBacteria/CellDivision.h"
#include "../reactions/sphericalBacteria/CellDivision.h"

BaseCompartmentChange::~BaseCompartmentChange() {
  std::cerr << "BaseCompartmentChange::~BaseCompartmentChange()\n";
}

BaseCompartmentChange *
BaseCompartmentChange::
createCompartmentChange(std::vector<double> &paraValue,
			std::vector< std::vector<size_t> > &indValue, 
			const std::string &idValue ) {
  //Divisions
  //(compartmentDivision.h,compartmentDivision.cc)
  //////////////////////////////////////////////////////////////////////
  if( boost::iequals(idValue,"Sphere::divisionRandomDirection") ) 
    return new Sphere::DivisionRandomDirection(paraValue,indValue);
  if( boost::iequals(idValue,"Sphere::divisionRandomDirectionFlag") )
        return new Sphere::DivisionRandomDirectionFlag(paraValue,indValue);
  else if( boost::iequals(idValue,"Sphere::divisionRandomAndSCDirection") )
    return new Sphere::DivisionRandomAndSCDirection(paraValue,indValue);
  else if( boost::iequals(idValue,"Sphere::divisionRandomDirectionOnSCSurface") ) 
    return new Sphere::DivisionRandomDirectionOnSCSurface(paraValue,indValue);
  else if( boost::iequals(idValue,"Sphere::divisionDirection") ) 
    return new Sphere::DivisionDirection(paraValue,indValue);
  else if( boost::iequals(idValue,"Sphere::divisionVelocity") ) 
    return new Sphere::DivisionVelocity(paraValue,indValue);
  else if( boost::iequals(idValue,"divisionRandomDirectionSphere") ||
	   boost::iequals(idValue,"divisionRandomAndSCDirectionSphere") ||
	   boost::iequals(idValue,"divisionRandomDirectionSphereOnSCSurface") ||
	   boost::iequals(idValue,"divisionDirectionSphere") ) {
    std::cerr << "baseCompartmentChange.cc All divisionXSphere names have been moved into " 
	      << "Sphere::DivisionX.";
    exit(EXIT_FAILURE);
  }
	   
  else if( boost::iequals(idValue,"divisionCellDirectionSphereBud") )
    return new DivisionCellDirectionSphereBud(paraValue,indValue);
  else if( boost::iequals(idValue,"divisionCellDirectionEllipse") )
    return new DivisionCellDirectionEllipse(paraValue,indValue);
  else if( boost::iequals(idValue,"divisionCellDirectionCigar") )
    return new DivisionCellDirectionCigar(paraValue,indValue);
  else if( boost::iequals(idValue,"divisionCellDirectionCigarStochastic") )
    return new DivisionCellDirectionCigarStochastic(paraValue,indValue);
  else if( boost::iequals(idValue,"elongatedBacteria::cellDivision2D") )
    return new elongatedBacteria::CellDivision<2>(paraValue,indValue);
  else if( boost::iequals(idValue,"elongatedBacteria::cellDivision3D") )
    return new elongatedBacteria::CellDivision<3>(paraValue,indValue);
  else if( boost::iequals(idValue,"sphericalBacteria::cellDivision2D") )
    return new sphericalBacteria::CellDivision<2>(paraValue,indValue);
  else if( boost::iequals(idValue,"sphericalBacteria::cellDivision3D") )
    return new sphericalBacteria::CellDivision<3>(paraValue,indValue);
  else if( boost::iequals(idValue,"divisionLens") )
    return new DivisionLens(paraValue,indValue);
  //Removals
  //(compartmentRemoval.h,compartmentRemoval.cc)
  //
  else if( boost::iequals(idValue,"removalThreshold") ) 
    return new RemovalThreshold(paraValue,indValue);
  else if( boost::iequals(idValue,"removalRadialThreshold") ) 
    return new RemovalRadialThreshold(paraValue,indValue);
  else if( boost::iequals(idValue,"removalThresholdBactPos") ) 
    return new RemovalThresholdBactPos(paraValue,indValue);
  else if( boost::iequals(idValue,"removalCylinderThreshold") ) 
    return new RemovalCylinderThreshold(paraValue,indValue);

  //Default, if nothing found
  //
  else {
    std::cerr << std::endl << "BaseCompartmentChange::createCompartmentChange() "
	      << "WARNING: CompartmentChangetype " 
	      << idValue << " not known, no compartmentChange created." << std::endl;
    exit(EXIT_FAILURE);
  }
}


BaseCompartmentChange * 
BaseCompartmentChange::createCompartmentChange(std::istream &IN ) {
  
  std::string idVal;
  size_t pNum,levelNum;
  IN >> idVal;
  IN >> pNum;
  IN >> levelNum;
  std::vector<size_t> varIndexNum( levelNum );
  for( size_t i=0 ; i<levelNum ; i++ )
    IN >> varIndexNum[i];

  std::vector<double> pVal( pNum );
  for( size_t i=0 ; i<pNum ; i++ )
    IN >> pVal[i];

  std::vector< std::vector<size_t> > varIndexVal( levelNum );
  for( size_t i=0 ; i<levelNum ; i++ )
    varIndexVal[i].resize( varIndexNum[i] );

  for( size_t i=0 ; i<levelNum ; i++ )
    for( size_t j=0 ; j<varIndexNum[i] ; j++ )
      IN >> varIndexVal[i][j];

  return createCompartmentChange(pVal,varIndexVal,idVal);
}

int BaseCompartmentChange::
flag(Compartment &compartment,std::vector< std::vector<double> > &y,
     std::vector< std::vector<double> > &dydt ) {
  std::cerr << "BaseCompartmentChange::flag() should not be used. "
	    << "Should always be mapped onto one of the real types.\n";
  exit(EXIT_FAILURE);
}  

void BaseCompartmentChange::
update(Organism &O,Compartment &compartment,
       std::vector< std::vector<double> > &y,
       std::vector< std::vector<double> > &dydt, double t ) {
  std::cerr << "BaseCompartmentChange::update() should not be used. "
	    << "Should always be mapped onto one of the real types.\n";
  exit(EXIT_FAILURE);
}  

void BaseCompartmentChange::
printCambium( std::ostream &os ) const
{
  std::cerr << "BaseCompartmentChange::printCambium(ofstream) should not be used. " << std::endl
	    << "Should always be mapped onto one of the real types." << std::endl
	    << "Creating unresolve reaction in output." << std::endl;
  os << "Arrow[Organism][" << id() << "],{},{},{},Parameters[";
  for (size_t i=0; i<numParameter(); ++i) {
    if (i!=0)
      os << ", ";
    os << "parameter(i)";
  }
  os << "]" << std::endl;
}
