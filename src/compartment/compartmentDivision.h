//
// Filename     : compartmentDivision.h
// Description  : Classes describing compartment divisions
// Author(s)    : Henrik Jonsson (henrik@thep.lu.se)
// Created      : September 2005
// Revision     : $Id: compartmentDivision.h 643 2015-08-20 08:08:11Z henrik $
//
#ifndef COMPARTMENTDIVISION_H
#define COMPARTMENTDIVISION_H

#include "baseCompartmentChange.h"
#include "../organism.h"

///
/// @brief The namespace Sphere is used to group all reactions and rules assuming cells described
/// by spheres.
///
/// These reactions are dealing with division, mechaical interactions and defining neighbors
/// and are based on the assumption that the cells can be described by a position vector and
/// a radius (x,y,z,r), and hence can use this information to update positions and size without
/// users providing indices.
///
/// The namespace might be replaced by a Class inheriting Topology/Compartment features, and
/// right now, if the topology name given in a model file is 'sphere' then
/// Compartment.getVolume() will calculate correct volume given the dimension adn radius.
///
/// @see Compartment::getVolume()
///
namespace Sphere {

  ///
  /// @brief Divides a spherical cell in two using a random division direction
  ///
  /// Divides a spherical cell in a random direction into two when a threshold 
  /// size is reached.
  /// 
  class DivisionRandomDirection : public BaseCompartmentChange {
    
  public:
    
    DivisionRandomDirection(std::vector<double> &paraValue, 
			    std::vector< std::vector<size_t> > 
			    &indValue );
    
    int flag(Compartment &compartment,
	     std::vector< std::vector<double> > &y,
	     std::vector< std::vector<double> > &dydt );
    void update(Organism &O,Compartment &compartment,
		std::vector< std::vector<double> > &y,
		std::vector< std::vector<double> > &dydt, double t=0.0 );  
  };
    
    
    ///
    /// @brief Divides a spherical cell in two using a random division direction
    ///
    /// Divides a spherical cell in a random direction into two when a threshold
    /// size is reached and when a flag is 0. 
    class DivisionRandomDirectionFlag : public BaseCompartmentChange {
        
    public:
        
        DivisionRandomDirectionFlag(std::vector<double> &paraValue,
                                std::vector< std::vector<size_t> >
                                &indValue );
        
        int flag(Compartment &compartment,
                 std::vector< std::vector<double> > &y,
                 std::vector< std::vector<double> > &dydt );
        void update(Organism &O,Compartment &compartment,
                    std::vector< std::vector<double> > &y,
                    std::vector< std::vector<double> > &dydt, double t=0.0 );  
    };
    
    
  
  //!Divides a spherical cell using a random and anticlinal direction
  class DivisionRandomAndSCDirection : public BaseCompartmentChange {
    
  public:
    
    DivisionRandomAndSCDirection(std::vector<double> &paraValue, 
				 std::vector< std::vector<size_t> > 
				 &indValue );
    
    int flag(Compartment &compartment,
	     std::vector< std::vector<double> > &y,
	     std::vector< std::vector<double> > &dydt );
    void update(Organism &O,Compartment &compartment,
		std::vector< std::vector<double> > &y,
		std::vector< std::vector<double> > &dydt, double t=0.0 );  
  };
  
  ///
  /// @brief Divides a spherical cell in two using a random division direction
  /// on a sphere-cylinder surface
  ///
  class DivisionRandomDirectionOnSCSurface : 
  public BaseCompartmentChange {
    
  public:
    
    DivisionRandomDirectionOnSCSurface(std::vector<double> 
				       &paraValue, 
				       std::vector< 
				       std::vector<size_t> > 
				       &indValue );
    
    int flag(Compartment &compartment,
	     std::vector< std::vector<double> > &y,
	     std::vector< std::vector<double> > &dydt );
    void update(Organism &O,Compartment &compartment,
		std::vector< std::vector<double> > &y,
		std::vector< std::vector<double> > &dydt, double t=0.0 );
    
    ///
    /// @brief Defines a random vector in the plane orthogonal to A and puts it 
    /// in d
    ///
    void getRandomSphereSurfaceDirection(std::vector<double> &A,
					 std::vector<double> &d);
    
    ///
    ///@brief Defines a random vector on a cylinder where z is assumed along the 
    ///length
    ///
    ///It unfolds the cylinder, picks a random direction in the z,rPhi,
    ///plane and then converts back to x and y.
    ///
    void getRandomCylinderSurfaceDirection(std::vector<double> &A,
					   std::vector<double> &d);
  };
  
  ///
  /// @brief Divides a spherical cell in a specific direction
  ///
  /// A direction (x,y,z) is given as a parameter and cells divide
  /// along this direction.
  /// 
  class DivisionDirection : public BaseCompartmentChange 
  {
    
  public:
    
    DivisionDirection(std::vector<double> &paraValue, 
		      std::vector< std::vector<size_t> > 
		      &indValue );
    
    int flag(Compartment &compartment,
	     std::vector< std::vector<double> > &y,
	     std::vector< std::vector<double> > &dydt );
    void update(Organism &O,Compartment &compartment,
		std::vector< std::vector<double> > &y,
		std::vector< std::vector<double> > &dydt, double t=0.0 );  
  };

  ///
  /// @brief Divides a spherical cell in the direction it is currently moving
  ///
  /// A direction (x,y,z) is given by the velocity extracted from the
  /// latest calculation of the derivs of the x,y,z component.
  /// 
  class DivisionVelocity : public BaseCompartmentChange 
  {
    
  public:
    
    DivisionVelocity(std::vector<double> &paraValue, 
		     std::vector< std::vector<size_t> > 
		     &indValue );
    
    int flag(Compartment &compartment,
	     std::vector< std::vector<double> > &y,
	     std::vector< std::vector<double> > &dydt );
    void update(Organism &O,Compartment &compartment,
		std::vector< std::vector<double> > &y,
		std::vector< std::vector<double> > &dydt, double t=0.0 );  
  };
}

//!Divides a spherical cell with a bud into two
class DivisionCellDirectionSphereBud : public BaseCompartmentChange {
  
 public:
  
  DivisionCellDirectionSphereBud(std::vector<double> &paraValue, 
				 std::vector< std::vector<size_t> > 
				 &indValue );
  
  int flag(Compartment &compartment,
	   std::vector< std::vector<double> > &y,
	   std::vector< std::vector<double> > &dydt );
  void update(Organism &O,Compartment &compartment,
	      std::vector< std::vector<double> > &y,
	      std::vector< std::vector<double> > &dydt, double t=0.0 );  
};

//!Divides an elliptical cell into two in the major axis direction
class DivisionCellDirectionEllipse : public BaseCompartmentChange {
  
 public:
  
  DivisionCellDirectionEllipse(std::vector<double> &paraValue, 
			       std::vector< std::vector<size_t> > 
			       &indValue );
  
  int flag(Compartment &compartment,
	   std::vector< std::vector<double> > &y,
	   std::vector< std::vector<double> > &dydt );
  void update(Organism &O,Compartment &compartment,
	      std::vector< std::vector<double> > &y,
	      std::vector< std::vector<double> > &dydt, double t=0.0 );  
};

//!Divides a cigar shaped cell into two in the major axis direction
class DivisionCellDirectionCigar : public BaseCompartmentChange {
  
 public:
  
  //!Constructor for the DivisionCellDirectionCigar class
  DivisionCellDirectionCigar(std::vector<double> &paraValue, 
			     std::vector< std::vector<size_t> > 
			     &indValue );
  
  int flag(Compartment &compartment,
	   std::vector< std::vector<double> > &y,
	   std::vector< std::vector<double> > &dydt );
  void update(Organism &O,Compartment &compartment,
	      std::vector< std::vector<double> > &y,
	      std::vector< std::vector<double> > &dydt, double t=0.0 );  
};

///
/// @brief Divides a cigar shaped cell into two in the major axis direction 
/// with random placement of molecules
///
/// 
///
///
class DivisionCellDirectionCigarStochastic : public BaseCompartmentChange 
{
  
 public:
  
  //! Constructor for the DivisionCellDirectionCigar class
  DivisionCellDirectionCigarStochastic(std::vector<double> &paraValue, 
				       std::vector< std::vector<size_t> > 
				       &indValue );
  
  //! Flags for division if the radius is larger than a threshold value
  int flag(Compartment &compartment,
	   std::vector< std::vector<double> > &y,
	   std::vector< std::vector<double> > &dydt );
  //! Divides a cigar-shaped cell with stochastic update of molecules 
  void update(Organism &O,Compartment &compartment,
	      std::vector< std::vector<double> > &y,
	      std::vector< std::vector<double> > &dydt, double t=0.0 );  
};

//! Divides by adding a lens cell outside the outmost
class DivisionLens : public BaseCompartmentChange {
  
 public:
  
  DivisionLens(std::vector<double> &paraValue, 
	       std::vector< std::vector<size_t> > 
	       &indValue );
  
  int flag(Compartment &compartment,
	   std::vector< std::vector<double> > &y,
	   std::vector< std::vector<double> > &dydt );
  void update(Organism &O,Compartment &compartment,
	      std::vector< std::vector<double> > &y,
	      std::vector< std::vector<double> > &dydt, double t=0.0 );  
};

#endif


