#include "baseOptimizer.h"
#include "simulatedAnnealing.h"
#include "GradientDescent.h"
#include "GeneticGeneral.h" 
#include "SRES.h"
#include "Genetic.h"
#include "CMAES.h"
using namespace OrganismLib;

BaseOptimizer::BaseOptimizer(BaseOptimizationProblem* E, const std::string &file){
  E_ = E;
  readOptimizer(file);
  // ADDED (phi)
  randomize(&bestGlobal);
  worstGlobal = 0;
  updateCost(&bestGlobal);
  worstGlobal = bestGlobal.cost();	
  debug_live = 0;
}

BaseOptimizer::BaseOptimizer(BaseOptimizationProblem *E, std::ifstream &IN)
{
  E_ = E;
  readOptimizer(IN);
  // ADDED (phi)
  randomize(&bestGlobal);
  worstGlobal = 0;
  updateCost(&bestGlobal);
  worstGlobal = bestGlobal.cost();
  debug_live = 0;
}

BaseOptimizer* BaseOptimizer::createOptimizer(BaseOptimizationProblem* E, const std::string &file) 
{
  std::istream *IN = myFiles::openFile(file);
  if (!IN) {
    std::cerr << "BaseOptimizer::createOptimizer() - "
	      << "Cannot open file " << file << std::endl;
    exit(-1);
  }
  BaseOptimizer* z = createOptimizer(E, (std::ifstream &) *IN);
  delete IN;
  return z; //OK : returns the value, so, even if the lovation of z is destroyed, 
            // the value points towards a still existing class, ouf !
}

BaseOptimizer* BaseOptimizer::createOptimizer(BaseOptimizationProblem* E, std::ifstream &IN)
{	
  std::string idValue;
  IN >> idValue;
  std::cerr << "Optimization using " << idValue << "\n";
  if 	(idValue == "simulatedAnnealing")
    return new SimulatedAnnealing(E, IN);
  if 	(idValue == "metaSimulatedAnnealing")
    return new MetaSimulatedAnnealing(E, IN);
  if 	(idValue == "simulatedAnnealingWorkers")
    return new SimulatedAnnealingWorkers(E, IN);
  else if (idValue == "simulatedTempering")
    return new SimulatedTempering(E, IN);
  else if (idValue == "geneticAlgorithm")
    return (new GeneticGeneral(E, IN));
  else if (idValue == "SRES")
    return (new SRES(E, IN));
  else if (idValue == "oldGenetic")
    return (new GeneticAlgo(E, IN));
  else if (idValue == "GradientDescent")
    return (new GradientDescent(E, IN));
  else if (idValue == "CMAES")
    return (new CMAES(E, IN));
  else {
    std::cerr << "\nBaseOptimizer::createOptimizer() WARNING: Optimizertype " 
	      << idValue << " not known, no optimizer created.\n\7";
    delete &IN;
    exit(-1);
  }
}

BaseOptimizer::~BaseOptimizer()
{
  
}

void BaseOptimizer::optimize()
{
  std::cerr << "BaseOptimizer::optimize() ->If this appears, it means that the "
	    << "function optimize() has not been found for the type of optimizer you asked.\n";
  exit(-1);
}

void BaseOptimizer::readOptimizer(const std::string &file) 
{
  std::istream *IN = myFiles::openFile(file);
  if (!IN) {
    std::cerr << "BaseOptimizer::readOptimizer() - " << "Cannot open file " << file << std::endl;
    exit(-1);
  }
  // read in the type of analyzer and how many arguments it takes
  //
  std::string idValue;
  *IN >> idValue;
  std::cerr << "Optimization using " << idValue;
  readOptimizer((std::ifstream &) *IN);
}

void BaseOptimizer::readOptimizer(std::ifstream &IN)  
// In the case of the call by the constructor after a new subclass, 
// then idValue has already been read.
{	
  IN >> numArg_;
  // read in the arguments for the analyzer
  // 
  argument_.resize(numArg_);
  if (numArg_) {
    std::cerr << "Optimization with " << numArg_ << " arguments:\n";
    for (size_t i = 0; i < numArg_; ++i) {
      IN >> argument_[i];
      std::cerr << argument_[i] << " ";
    }
  }
  std::cerr << "\n";
  
  
  //Checks the number of parameter indices
  IN >> numIndex_;
  indexVector_.resize(numIndex_);
  paraLowVector_.resize(numIndex_);
  paraHighVector_.resize(numIndex_);
  paraNotScaledLowVector_.resize(numIndex_);
  paraNotScaledHighVector_.resize(numIndex_);
  //read in indices in the indexVector_ and lower and upper bounds into the parLow(High)Vector_
  std::cerr << "Performing analysis with the following " << numIndex_ << " ("
	    << indexVector_.size() 
	    << ") parameter indices (low initial bound, high initial bound):"
	    << std::endl;
  for (size_t i=0; i < numIndex_; ++i) { 
    IN >> indexVector_[i];    
    IN >> paraNotScaledLowVector_[i];
    IN >> paraNotScaledHighVector_[i];
    std::cerr << indexVector_[i] << " (" << paraNotScaledLowVector_[i] << "," 
	      << paraNotScaledHighVector_[i] << "), \n";
  }
  std::cerr << std::endl;
  
  //Checks how many sets of parameters that are to be read from file
  IN >> numParaSet_;
  //read parameters from file and sets parametervalues, if flagged
  if (numParaSet_) {
    std::cerr << "\nReading " << numParaSet_ << " parameter sets from the file...";
    NotScaledParameters_.resize(numParaSet_);
    for (size_t j=0; j<numParaSet_; ++j) {
      NotScaledParameters_[j].resize(numIndex_);
      for (size_t i=0; i<numIndex_; ++i){
	IN >> NotScaledParameters_[j][i];
      }
    }
    std::cerr << "Done\n";
    std::cerr << "Note: by reading parameters, initial parameter boundaries overruled." 
	      << std::endl;
  }
  else {
    std::cerr << "No (" << numParaSet_ << ") initial parameter sets to be read from file."  
	      << std::endl;
  }
  
  //size_t numArg;
  std::string scalingMethod;
  IN >> scalingMethod;
  if(scalingMethod == "NoScaling"){
    scaling_function_intern_to_solver = &Scaling::identity;
    scaling_function_solver_to_intern = &Scaling::identity;
    cerr << "Scaling put to identity\n";
  } else if (scalingMethod == "ExponentialBounded"){
    scaling_function_intern_to_solver = &Scaling::exponential;
    scaling_function_solver_to_intern = &Scaling::logarithmic;
    cerr << "Scaling put to (real solver parameter) -> logarithmic -> (intern exploration space)\n";
  } else if (scalingMethod == "LinearAndInverse"){
    scaling_function_intern_to_solver = &Scaling::linear_symmetric;
    scaling_function_solver_to_intern = &Scaling::linear_symmetric_rev;
    cerr << "Scaling put to (real solver parameter) -> [ x|->1/x if is x < 1,   and x if x > 1 ] -> (intern exploration space)\n";
  } else {
    scaling_function_intern_to_solver = &Scaling::identity;
    scaling_function_solver_to_intern = &Scaling::identity;
    cerr << "Scaling put to identity by default\n";
  }
  
  int num_scaling_parameters;
  IN >> num_scaling_parameters;
  std::cerr << num_scaling_parameters << " Parameters given for the scaling\n";
  if(num_scaling_parameters > 0){
    scaling_parameters = new vector<double>(5,0);
    if(num_scaling_parameters > 5) 
      cerr << "Max 5 arguments for the scaling : xmin xmax, "
	   << "ymin_forced, ymax_forced, coefficient -> Takes only the five first ones\n";
    for(int i = 0; i < num_scaling_parameters; ++i){
      double res;
      IN >> res;
      if(i < 5) (*scaling_parameters)[i] = res;
      if(i == 0) cerr << "xmin = " << res << "\n";
      if(i == 1) cerr << "xmax = " << res << "\n";
      if(i == 2) cerr << "ymin_force = " << res << "\n";
      if(i == 3) cerr << "xmax_force = " << res << "\n";		
      if(i == 4) cerr << "coefficient = " << res << "\n";		
      if(i > 4) cerr << "Ignore\n";
    }
  } else 	
    scaling_parameters = NULL;
  
  for (size_t i=0; i < numIndex_; ++i) { 
    paraLowVector_[i] = scaling_function_solver_to_intern(paraNotScaledLowVector_[i], 
							  scaling_parameters);
    paraHighVector_[i] = scaling_function_solver_to_intern(paraNotScaledHighVector_[i], 
							   scaling_parameters);
  }
  
  if (numParaSet_) {
    parameters_.resize(numParaSet_);
    for (size_t j=0; j<numParaSet_; ++j) {
      parameters_[j].resize(numIndex_);
      for (size_t i=0; i<numIndex_; ++i){
	parameters_[j][i] = scaling_function_solver_to_intern(NotScaledParameters_[j][i], 
							      scaling_parameters);
      }
    }
    setParameter(&NotScaledParameters_[0]);
  }  
}

void BaseOptimizer::resetCostCalls(){
  CostCalls = 0;
}

int  BaseOptimizer::nbCostCalls(){
  return CostCalls;
}

void BaseOptimizer::printOptima(std::ostream &os) //Not scaled (solver)
{
  os << "\n" << "#" << globalOptima() << "\n";
  os << globalOptima() << "  ";
  for (size_t i =0; i<E_->numPara(); i++)
    os << optimalParameter(i) << " ";
  os << "\n";
  os.flush();
}

void BaseOptimizer::setParasToOptimal()
{

  for(size_t i = 0; i < numIndex(); ++i){
    setParameter(i, bestGlobal.equivparam(i));
  }
  return;

}


double BaseOptimizer::globalOptima(){ 
  return bestGlobal.cost();
}

double BaseOptimizer::optimalParameter(int n){ // Not scaled, (solver)
  // If this parameter is one to optimize, then take the value from bstGlobal
  assert(bestGlobal.modified() == false);
  for(size_t i = 0; i < numIndex(); ++i){
   if((int) indexVector(i) == n) return bestGlobal.equivparam(i);
  }
  // Otherwise, take the value in the estimator, which should never change.
  return E_->parameter(n);
}

void BaseOptimizer::getOptimalParameters(vector<double>* save_opt_para){ // Not scaled, (solver)
  assert(bestGlobal.modified() == false);
  if(save_opt_para->size() != numIndex()){
    save_opt_para->resize(numIndex());
  }
  for(size_t i = 0; i < numIndex(); ++i){
    (*save_opt_para)[i] = bestGlobal.equivparam(i);
  }
}

void BaseOptimizer::getAllOptimalParameters(vector<double>* save_opt_para){ // Not Scaled, solver
  assert(bestGlobal.modified() == false);
  if(save_opt_para->size() != E_->numPara()){
    save_opt_para->resize(E_->numPara());
  }
  for(size_t i = 0; i < E_->numPara(); ++i){
    (*save_opt_para)[i] = E_->parameter(i);
  }
  for(size_t i = 0; i < numIndex(); ++i){
    (*save_opt_para)[indexVector(i)] = bestGlobal.equivparam(i);		
  }
}

void BaseOptimizer::setGlobal(double cost, vector<double>* v){
  assert(v->size() == numIndex());
  for (size_t i=0; i<numIndex(); ++i){
    bestGlobal.setGene(i, (*v)[i]);
  }
  bestGlobal.setCost(cost);
}

void BaseOptimizer::setGlobalNotScaled(double cost, vector<double>* v){
  assert(v->size() == numIndex());
  for (size_t i=0; i<numIndex(); ++i){
    bestGlobal.setEquivParam(i, (*v)[i]);
  }
  bestGlobal.ComputeGenesFromEquivParams(scaling_function_intern_to_solver, scaling_parameters);
  bestGlobal.setCost(cost);
}

void BaseOptimizer::improveGlobal(double cost, vector<double>* v){
  if(cost < bestGlobal.cost()){
    setGlobal(cost, v);
  }	
}

void BaseOptimizer::improveGlobalNotScaled(double cost, vector<double>* v){
  if(cost < bestGlobal.cost()){
    setGlobalNotScaled(cost, v);
  }	
}

double BaseOptimizer::computeCost(vector<double>* intern_parameters){
  // double t = nbCostCalls() / 100.0;
  if(debug_live) cerr << ":";
  for(int i = 0; i < (int) numIndex_; ++i){
    E_->setParameter(indexVector_[i], scaling_function_intern_to_solver((*intern_parameters)[i], scaling_parameters));
  }
  double E = E_->getCost();
  // to add a penalty if going outside the boundaries
  // for(int i = 0; i < (int) numIndex(); ++i){
  //if((*intern_parameters)[i] > paraHighVector(i)) E += min(1e5, t*(-1+std::exp((*intern_parameters)[i] - paraHighVector(i))));
  //if((*intern_parameters)[i] < paraLowVector(i)) E += min(1e5, t*(-1+t*std::exp(-(*intern_parameters)[i] + paraLowVector(i))));
  //}	
  CostCalls++;
  if(debug_live) cerr << "|";
  return E;
}

double BaseOptimizer::computeCostNotScaled(vector<double>* parameters){
  if(debug_live) cerr << ":";
	for(int i = 0; i < (int) numIndex_; ++i){
		E_->setParameter(indexVector_[i], (*parameters)[i]);
	}
	double E = E_->getCost();
	CostCalls++;
	if(debug_live) cerr << "|";
	return E;
}

void BaseOptimizer::randomizeSet(vector<double> * toRandomize){ // Intern Scaled Version
	toRandomize->resize(numIndex());
	assert (numIndex()==indexVector_.size() &&
		numIndex()==paraLowVector_.size() &&
		numIndex()==paraHighVector_.size() );
		
	for(size_t i = 0; i < numIndex(); i++) {
		double lMin = (paraLowVector(i));
		double lMax = (paraHighVector(i));
		double lDelta = lMax-lMin;
		double r = lMin+lDelta*myRandom::Rnd();

		/*double lMin = std::log10(paraLowVector(i));
		double lMax = std::log10(paraHighVector(i));
		double lDelta = lMax-lMin;
		double r = std::pow(10.0,lMin+lDelta*myRandom::Rnd());*/
		(*toRandomize)[i] = r;
	}	
}

void BaseOptimizer::randomizeSetNotScaledUniform(vector<double> * toRandomize){ // Intern Scaled Version WARNING ! This is uniform on the non scaled range, which is different from randomized in intern scale plus converted to non scaled solver type
	toRandomize->resize(numIndex());
	assert (numIndex()==indexVector_.size() &&
		numIndex()==paraNotScaledLowVector_.size() &&
		numIndex()==paraNotScaledHighVector_.size() );
		
	for(size_t i = 0; i < numIndex(); i++) {
		double lMin = (paraNotScaledLowVector(i));
		double lMax = (paraNotScaledHighVector(i));
		double lDelta = lMax-lMin;
		double r = lMin+lDelta*myRandom::Rnd();

		/*double lMin = std::log10(paraLowVector(i));
		double lMax = std::log10(paraHighVector(i));
		double lDelta = lMax-lMin;
		double r = std::pow(10.0,lMin+lDelta*myRandom::Rnd());*/
		(*toRandomize)[i] = r;
	}	
}


void BaseOptimizer::setGlobal(double cost){
	// Implicitely say that the current set of parameters in the estimator is the best and has a cost of cost
	bestGlobal.setCost(cost);
	for (size_t i=0; i<numIndex(); ++i){
		bestGlobal.setEquivParam(i, parameter(i));
	}
	bestGlobal.ComputeGenesFromEquivParams(scaling_function_solver_to_intern, scaling_parameters);
}

void BaseOptimizer::improveGlobal(double cost){
	if(cost < bestGlobal.cost()){
		setGlobal(cost);
	}
}

void BaseOptimizer::setParameter(vector<double>* v){
	assert(v->size() == numIndex());
	for(int i = 0; i < (int) numIndex_; ++i){
		E_->setParameter(indexVector_[i], scaling_function_intern_to_solver((*v)[i], scaling_parameters));
	}	
}

void BaseOptimizer::setParameterNotScaled(vector<double>* v){
	assert(v->size() == numIndex());
	for(int i = 0; i < (int) numIndex_; ++i){
		E_->setParameter(indexVector_[i], (*v)[i]);
	}	
}

void BaseOptimizer::setParameter(int i, double value){
	E_->setParameter(indexVector_[i], scaling_function_intern_to_solver(value, scaling_parameters));
}

void BaseOptimizer::setParameterNotScaled(int i, double value){
	E_->setParameter(indexVector_[i], value);
}

void BaseOptimizer::randomizeParameter() // Inside the solver -> not scaled BUT !!! randomized in uniform according to the scaling
{
	assert (numIndex()==indexVector_.size() &&
		numIndex()==paraLowVector_.size() &&
		numIndex()==paraHighVector_.size() );
		
	for(size_t i = 0; i < numIndex(); i++) {
		double lMin = (paraLowVector(i));
		double lMax = (paraHighVector(i));
		double lDelta = lMax-lMin;
		double r = lMin+lDelta*myRandom::Rnd();

		/*double lMin = std::log10(paraLowVector(i));
		double lMax = std::log10(paraHighVector(i));
		double lDelta = lMax-lMin;
		double r = std::pow(10.0,lMin+lDelta*myRandom::Rnd());*/
		E_->setParameter(indexVector(i),scaling_function_intern_to_solver(r, scaling_parameters));
	}
}

void BaseOptimizer::lograndomizeParameter() // Inside the solver -> not scaled BUT !!! log of parameters is randomized in uniform according to the scaling
{
	assert (numIndex()==indexVector_.size() &&
		numIndex()==paraLowVector_.size() &&
		numIndex()==paraHighVector_.size() );
		
	for(size_t i = 0; i < numIndex(); i++) {

		double lMin = std::log10(paraLowVector(i));
		double lMax = std::log10(paraHighVector(i));
		double lDelta = lMax-lMin;
		double r = std::pow(10.0,lMin+lDelta*myRandom::Rnd());
		E_->setParameter(indexVector(i),scaling_function_intern_to_solver(r, scaling_parameters));
	}
}


void BaseOptimizer::randomizeParameterUsingNotScaledUniform() // Inside the solver -> not scaled BUT !!! randomized in uniform according to the scaling
{
	assert (numIndex()==indexVector_.size() &&
		numIndex()==paraNotScaledLowVector_.size() &&
		numIndex()==paraNotScaledHighVector_.size() );
		
	for(size_t i = 0; i < numIndex(); i++) {
		double lMin = (paraNotScaledLowVector(i));
		double lMax = (paraNotScaledHighVector(i));
		double lDelta = lMax-lMin;
		double r = lMin+lDelta*myRandom::Rnd();

		/*double lMin = std::log10(paraLowVector(i));
		double lMax = std::log10(paraHighVector(i));
		double lDelta = lMax-lMin;
		double r = std::pow(10.0,lMin+lDelta*myRandom::Rnd());*/
		setParameterNotScaled(i,r);
	}
}

double BaseOptimizer::computeCost(){
	if(debug_live) cerr << ":";
	double E = E_->getCost();
	CostCalls++;
	if(debug_live) cerr << "|";
	return E;
}

double BaseOptimizer::parameter(int i){
	return scaling_function_solver_to_intern(E_->parameter(indexVector(i)), scaling_parameters);
}


void BaseOptimizer::parameter(std::vector <double> * save_opt_para){
  if(save_opt_para->size() != numIndex()){
    save_opt_para->resize(numIndex());
  }
  for(size_t i = 0; i < numIndex(); ++i){
    (*save_opt_para)[i] = scaling_function_solver_to_intern(E_->parameter(indexVector(i)), scaling_parameters);
  }

}


void BaseOptimizer::parameterNotScaled(std::vector <double> * save_opt_para){
  if(save_opt_para->size() != numIndex()){
    save_opt_para->resize(numIndex());
  }
  for(size_t i = 0; i < numIndex(); ++i){
    (*save_opt_para)[i] = E_->parameter(indexVector(i));
  }

}


double BaseOptimizer::parameterNotScaled(int i){
	return E_->parameter(indexVector(i));
}

void BaseOptimizer::printState(double E, std::ostream &os){
	E_->printState(E, os);
}

void BaseOptimizer::updateCost(individual* ind){
	if(ind->modified()){
		double E = computeCost(ind);
		if(isnan(E) || isinf(E)) {
			E = worstGlobal;
		};
		if(E > 1e50) E = 1e50;
		ind->setCost(E); // This will do modified_ = false inside ind.
		bestGlobal.improve(ind);
		if(ind->cost() > worstGlobal) worstGlobal = ind->cost();
	}
}

individual* BaseOptimizer::newRandomIndividual(){
	vector<double> tempForCopy; 
	tempForCopy.resize(numIndex_);
    if (!numParaSet()){
        randomizeParameter();
    }
	for(int i = 0; i < (int) numIndex_; ++i){
		tempForCopy[i] = E_->parameter(indexVector_[i]); // intern scaled version
	}
	individual* a = new individual(&tempForCopy);
	return a;
}

void BaseOptimizer::randomize(individual* i2){
	individual* i1 = newRandomIndividual();
	i2->copy(i1);
	delete i1;
}

double BaseOptimizer::computeCost(individual* ind){
	//double t = nbCostCalls() / 100.0;
	if(debug_live) cerr << ":";
	ind->ComputeEquivParamsFromGenes(scaling_function_intern_to_solver, scaling_parameters);
	for(int i = 0; i < (int) numIndex_; ++i){
		E_->setParameter(indexVector_[i], (ind->equivparam(i)));
	}
	double E = E_->getCost();
	// to put a penalty if going outside the boundaries
	//for(size_t i = 0; i <numIndex(); ++i){
		//if(ind->gene(i) > paraHighVector(i)) E += min(1e5, t*(-1+std::exp(ind->gene(i) - paraHighVector(i))));
		//if(ind->gene(i) < paraLowVector(i)) E += min(1e5, t*(-1+std::exp(-ind->gene(i) + paraLowVector(i))));
	//}
	CostCalls++;
	if(debug_live) cerr << "|";
	return E;
}




















