//
// Filename     : simulatedAnnealing.cc
// Description  : Classes defining optimization algorithms
// Author(s)    : Pontus Melke (pontus@thep.lu.se)
// Created      : August 2006
// Revision     : $Id: simulatedAnnealing.cc 664 2016-02-26 17:15:54Z andre $
//

#include "simulatedAnnealing.h"
#include "baseOptimizer.h"
#include "../common/myRandom.h"
using namespace OrganismLib;
//
// Simulated Annealing
//
SimulatedAnnealing::SimulatedAnnealing(BaseOptimizationProblem *E, const std::string &file) 
  : BaseOptimizer(E,file)
{
  
  //S_ = S;
  //readOptimizer(file);
  if(numArguments() !=5) {
    std::cerr << "\nSimulated annealing takes four arguments" 
	      << "TInit, TFinal, TUpdate, parameterStepFactor and numMetropolisStep\n";
    exit(-1);
  }
}

SimulatedAnnealing::SimulatedAnnealing(BaseOptimizationProblem *E, std::ifstream & IN) 
  : BaseOptimizer(E,IN)
{
  //S_ = S;
  //readOptimizer(file); already done by the constructor-
  if(numArguments() !=5) {
    std::cerr << "\nSimulated annealing takes four arguments" 
	      << "TInit, TFinal, TUpdate, parameterStepFactor and numMetropolisStep\n";
    exit(-1);
  }
}

void SimulatedAnnealing::optimize() 
{
	double TInit = argument(0);
	double TFinal = argument(1);
	double TStep = argument(2);
	double stepSize = argument(3);
	size_t numMetropolisStep = static_cast<size_t>(argument(4));
    std::cerr << "\nDone\n";
	if (!numParaSet())
		lograndomizeParameter();
	double E = computeCost();
	setGlobal(E);
	std::cerr << "Starting SimulatedAnnealing::optimize()\n"; 
	
	//Starting Simulated Annealing
	int numSuccess = 0,numUphill=0;

	std::cerr << "#T\tnumStep\tnumSucc\tnumUp\tE\tE_opt\tcpu" << std::endl;
	for(double T=TInit; T>TFinal; T *= TStep) {
		numSuccess = metropolis(E, 1/T, stepSize, numMetropolisStep,numUphill);
		std::cerr << T << "\t" << numMetropolisStep << "\t" << numSuccess << "\t" << numUphill << "\t"
			<< E << "\t" << globalOptima() << "\t" << myTimes::getDiffTime() << std::endl; 

		//std::cerr << "Best cost value so far: " << globalOptima() << "\n";
		//std::cerr << "Temporary cost value: " << E << "\n";
		//std::cerr << "T = " << T << "\n";
		//std::cerr << "CPU-time: " << myTimes::getDiffTime() << "\n";
		//std::cerr << "Succeeded in " << numSuccess  
		//<< " cases out of " << numMetropolisStep << "\n";    
		printState(E);
	}

	/*std::cout << "#" << globalOptima() << " ";
	for ( size_t i =0; i<numPara(); i++)
	std::cout << optimalParameter(i) << " ";
	std::cout << "\n";
	std::cout.flush();*/


	printOptima();	    // this (although useful) currently only results in a segmentation fault
}


//
// Custom Simulated Annealing
//
MetaSimulatedAnnealing::MetaSimulatedAnnealing(BaseOptimizationProblem *E, const std::string &file) 
  : BaseOptimizer(E,file)
{
  if(numArguments() !=7) {
    std::cerr << "\nSimulated annealing takes seven arguments" 
	      << "TInit, TFinal, TUpdate, parameterStepFactor, numMetropolisStep, numInitialRuns, numAnnealingRuns\n";
    exit(-1);
  }

  if(argument(5) < 0 or argument(6) < 0){
    std::cerr << "\nnumInitialSearch (para 5) and numAnnealingRuns (para 6) must be larger than zero!\n";
    exit(-1);
  }

}

MetaSimulatedAnnealing::MetaSimulatedAnnealing(BaseOptimizationProblem *E, std::ifstream & IN) 
  : BaseOptimizer(E,IN)
{
  if(numArguments() !=7) {
    std::cerr << "\nSimulated annealing takes seven arguments" 
	      << "TInit, TFinal, TUpdate, parameterStepFactor, numMetropolisStep, numInitialRuns, numAnnealingRuns\n";
    exit(-1);
  }
  if(argument(5) < 0 or argument(6) < 0){
    std::cerr << "\nnumInitialSearch (para 5) and numAnnealingRuns (para 6) must be larger than zero!\n";
    exit(-1);
  }

}

void MetaSimulatedAnnealing::optimize() 
{
	double TInit = argument(0);
	double TFinal = argument(1);
	double TStep = argument(2);
	double stepSize = argument(3);
	size_t numMetropolisStep = static_cast<size_t>(argument(4)); 
	size_t numInitialSearch = argument(5);
	size_t max_sa_runs = argument(6);
	//if (!numParaSet())
	//	lograndomizeParameter();
	std::cerr << "Starting CustomSimulatedAnnealing::optimize()\n"; 

	size_t num_sa_runs = 0;
	double E;
	double metaBestE=1e12;
	std::vector< double> metaBestParas;
	

	do{

	  E = 1e12;
	  setGlobal(E);
	
	  size_t iter=0;
	  do{
	    iter++;
	    lograndomizeParameter();
	    E = computeCost();
	    if (E<globalOptima()){
	      setGlobal(E);
	    }
    
	  }while(iter < numInitialSearch);

	  setParasToOptimal();		  
	
	  //Starting Simulated Annealing
	  int numSuccess = 0,numUphill=0;
     
	  double T = TInit;
	  std::cerr << "#T\tnumStep\tnumSucc\tnumUp\tE\tE_opt\tcpu" << std::endl;
	  while(T>TFinal) {
	    numSuccess = metropolis(E, 1/T, stepSize, numMetropolisStep,numUphill);
	    std::cerr << T << "\t" << numSuccess << "\t" << numUphill << "\t"
		      << E << "\t" << globalOptima() << "\t" << myTimes::getDiffTime() << "\t"
		      << std::endl; 	  
	    printState(E);
	    T *= TStep;
	  }
	  printOptima();
	  std::cerr << "\n\n";

	  if(globalOptima()<metaBestE or num_sa_runs==0){
	    metaBestE = globalOptima();
	    getOptimalParameters(&metaBestParas);
	  }

	  num_sa_runs++;

	  std::cerr << "Simulated annealing run " << num_sa_runs << " of " << max_sa_runs << " done." << std::endl;
	}while(num_sa_runs < max_sa_runs);

	// set the parameters to the meta-best values, recalculate the corresponding cost
	// and reprint this meta-best cost and para values in both cerr and cout

	setParameter(&metaBestParas);
	E = computeCost();
	setGlobal(E);
	printOptima();
	std::cerr << "The (meta-)best energy found was " << metaBestE << std::endl;
}


//
// simulated annealing with workers
//
SimulatedAnnealingWorkers::SimulatedAnnealingWorkers(BaseOptimizationProblem *E, const std::string &file) 
  : BaseOptimizer(E,file)
{
  if(numArguments() !=9) {
    std::cerr << "\nSimulated annealing with workers takes nine arguments:\n" 
	      << "TInit, TFinal, TUpdate,\nparameterStepFactor, numMetropolisStep,\nnumTop numMiddle numBottom, numRestarts\n";
    exit(-1);
  }
  if(argument(6) + argument(7) > 1.0){
    std::cerr << "\nWarning: Fraction of top plus bottom tier larger than one\n";
    exit(-1);
  }
}

SimulatedAnnealingWorkers::SimulatedAnnealingWorkers(BaseOptimizationProblem *E, std::ifstream & IN) 
  : BaseOptimizer(E,IN)
{
  if(numArguments() !=9) {
    std::cerr << "\nSimulated annealing with workers takes nine arguments:\n" 
	      << "TInit, TFinal, TUpdate,\nparameterStepFactor, numMetropolisStep,\nnumTop numMiddle numBottom, numRestarts\n";
    exit(-1);
  }

}

void SimulatedAnnealingWorkers::optimize() 
{
	double TInit = argument(0);
	double TFinal = argument(1);
	double TStep = argument(2);
	double stepSize = argument(3);
	size_t numMetropolisStep = static_cast<size_t>(argument(4)); 

	size_t numTop = argument(5);
	size_t numMiddle = argument(6);
	size_t numBot = argument(7);

	size_t numWorkers = numTop + numMiddle + numBot;

	size_t topThreshold = numTop;
	size_t middleThreshold = numTop + numMiddle;
	size_t restartsThreshold = argument(8);


	size_t resourcesTop = 3; // how many more simulations are done in the lowest parameters regions


	//size_t numWorkers = 8;

	//if (!numParaSet())
	//	lograndomizeParameter();
	std::cerr << "Starting SimulatedAnnealingWorkers::optimize()\n"; 
	std::cerr << "Size of top tier: " << topThreshold << ", size of middle tier "
		  << (middleThreshold-topThreshold) << ", size of bottom tier " << (numWorkers-middleThreshold) << "\n"; 

	double E = 1e12;
	setGlobal(E);

	std::vector< std::vector< double> >  worker_para_list;
	std::vector< size_t> worker_k_list;
	std::vector< double> worker_energy_list;
	std::vector< double> worker_T_list;


	// initialize workers
	for(size_t i = 0; i<numWorkers; i++){
	  lograndomizeParameter();
	  std::vector<double> temp_para;
	  parameter(&temp_para);
	  worker_para_list.push_back(temp_para);
	  E = computeCost();
	  worker_energy_list.push_back(E);
	  if (E<globalOptima()){
	    setGlobal(E);
	  }
	  worker_k_list.push_back(1);  // set to 1 for first run
	  worker_T_list.push_back(TInit);

	}
		
	//Starting Simulated Annealing
	
	double T = TInit;
	size_t num_restarts=0;
	size_t num_Tend=0;
	size_t numSuccess;

	size_t counter = 0;

	// worker_sorted
	// will contain the worker indices where the first element is
	// the worker with the lowest energy, second element is the worker with the second lowest energy, etc..
	std::vector<size_t> worker_sorted(numWorkers);
	std::cerr << "#worker\tT\tnumStep\tnumSuccess\tE\tcpu\tnumTend\tnumRestarts\tE_opt" << std::endl;
	while(num_restarts<restartsThreshold) {

	  //if(counter % 10 == 0){


	  for(size_t i = 0; i<numWorkers; i++){
	    worker_sorted[i] = i;
	  }



	  // bubble sort
	  size_t n = numWorkers -1;
	  for(size_t j = n; j>0; j--){
	    for(size_t i = 0; i<j; i++){
	      size_t index1 = worker_sorted[i];
	      size_t index2 = worker_sorted[i+1];
	      if( worker_energy_list[index1] > worker_energy_list[index2]){
		worker_sorted[i] = index2;
		worker_sorted[i+1] = index1;
	      }
	      n--;
	  }
	  }

	  /*
	    // print worker in sorted according to energy
	    std::cerr << "sorted workers indices (low to high energy): ";
	    for(size_t i = 0; i<numWorkers; i++){
	      size_t index = worker_sorted[i];
	      std::cerr << index << " ";
	    }
	    std::cerr << "\n";

	    // print energies in sorted order
	    std::cerr << "sorted energies: ";
	    for(size_t i = 0; i<numWorkers; i++){
	      size_t index = worker_sorted[i];
	      std::cerr << worker_energy_list[index] << " ";
	    }
	    std::cerr << "\n";
	  */
	  
	    // assign resources
	    for(size_t i = 0; i<numWorkers; i++){
	      size_t wi = worker_sorted[i];
	      if(i<topThreshold) worker_k_list[wi] = resourcesTop; // n minimizations
	      else if(i<middleThreshold) worker_k_list[wi] = 1; // 1 minimizations
	      else worker_k_list[wi] = 0; // 0 minimizations = generate new para set
	    }
	    //}

	  counter++;

	  // do sim annealing minimization

	  for(size_t i = 0; i<numWorkers; i++){
	    size_t k = worker_k_list[i];


	    if(k == 0){   // if energy is in bottom tier: generate new para set
	      lograndomizeParameter();
	      num_restarts++;

	      // get new energy and update
	      // energy/paraset for this worker
	      std::vector<double> temp_para;
	      parameter(&temp_para);
	      worker_para_list[i] = temp_para;
	      E = computeCost();
	      worker_energy_list[i] = E;
	      worker_T_list[i] = TInit;
	      if (E<globalOptima()){
		setGlobal(E);
	      }
	      k = 1; // also run one minimization (with new paraset)
	    }

	    // get paraset and energy for this worker and set
	    // the internal paraset and current energy to these
	    std::vector<double> temp_para;
	    temp_para = worker_para_list[i];
	    setParameter(&temp_para);
	    E = worker_energy_list[i];
	    
	    for(size_t j = 0; j<k;  j++){

	      // metropolis runs for current worker
	      T = worker_T_list[i]; // i is the worker index
	      numSuccess = metropolis(E, 1/T, stepSize, numMetropolisStep);

	      // save paraset for this worker
	      worker_para_list[i] = temp_para;
	      if(TStep*T < TFinal){ // reached final temp !!!
		// generate new para set
		lograndomizeParameter();

		// get new energy from new paraset and update
		// energy/paraset for this worker
		std::vector<double> temp_para;
		parameter(&temp_para);
		worker_para_list[i] = temp_para;
		E = computeCost();
		worker_energy_list[i] = E;
		if (E<globalOptima()){
		  setGlobal(E);
		}

		worker_T_list[i] = TInit; // reset temperature

		num_Tend++;
		num_restarts++;
		
		j=k; // ! this breaks the for-loop
	      }
	      else{ // save latest energy and update temperature for current worker
		worker_energy_list[i] = E;
		worker_T_list[i] = TStep*worker_T_list[i];
	      }
	    }
	    std::cerr << i << "\t" << T << "\t" << k*numMetropolisStep << "\t" << numSuccess << "\t"
		      << E << "\t" << myTimes::getDiffTime()  << "\t" << num_Tend << "\t"
		      << num_restarts << "\t" << globalOptima() <<  std::endl;
	    printState(E);



	  }
	}



 	std::cerr << "\n-------------------------------------------------------------\n";
	std::cerr << "Doing final optimization of best result at lowest temperature\n\n";
	

	size_t loop = 0;

	while(loop < 1){ // resourcesTop <--> 1
	  setParasToOptimal();
	
	  T = TInit;
	  std::cerr << "#T\tnumStep\tnumSuccess\tE\tcpu\tE_opt" << std::endl;
	  while(T>TFinal){
	    numSuccess = metropolis(E, 1/TFinal, stepSize, numMetropolisStep);
	    std::cerr << TFinal << "\t" << numMetropolisStep << "\t"  << numSuccess << "\t"
		      << E << "\t" << myTimes::getDiffTime()  << "\t" << globalOptima() << std::endl;
	    T *= TStep;
	  }

	  std::cerr << "\n";
	  loop++;
	}

 	std::cerr << "-------------------------------------------------------------\n";
	std::cerr << "Optimization done!\n";

	printOptima();
}




//
// Simulated Tempering
//
SimulatedTempering::SimulatedTempering(BaseOptimizationProblem *E, const std::string &file) :BaseOptimizer(E,file)
{
	//S_ = S;
	//readOptimizer(file);
	if(numArguments() !=5) {
		std::cerr << "\nSimulated tempering takes five arguments" 
			<< "TMax, TMin, numT, parameterStepFactor and numMetropolisStep\n";
		exit(-1);
	}
}

SimulatedTempering::SimulatedTempering(BaseOptimizationProblem *E, std::ifstream & IN) :BaseOptimizer(E, IN)
{
	//S_ = S;
	//readOptimizer(file);
	if(numArguments() !=5) {
		std::cerr << "\nSimulated tempering takes five arguments" 
			<< "TMax, TMin, numT, parameterStepFactor and numMetropolisStep\n";
		exit(-1);
	}
}

void SimulatedTempering::optimize() 
{
	double TMax = argument(0);
	double TMin = argument(1);
	unsigned int numT = static_cast<unsigned int>(argument(2));
	double stepSize = argument(3);
	size_t numMetropolisStep = static_cast<size_t>(argument(4));
	std::vector<double> T_r(numT); 
	Gr_.resize(numT);
	randomizeParameter();
	//lograndomizeParameter();
	double E = computeCost();
	setGlobal(E);
	int numUphill;
	metropolis(E, 1/TMin, stepSize,5*numMetropolisStep,numUphill); 
	
	initializeTemp(T_r, TMax, TMin);
	GrInit(T_r, 10*numMetropolisStep, stepSize);
	
	GrAdjust(T_r, 3*numMetropolisStep, numMetropolisStep, stepSize);
	GrAdjust(T_r, 3*numMetropolisStep, numMetropolisStep, stepSize);
	GrAdjust(T_r, 3*numMetropolisStep, numMetropolisStep, stepSize);
	GrAdjust(T_r, 3*numMetropolisStep, numMetropolisStep, stepSize);
	size_t r = static_cast<size_t>(myRandom::Rnd()*numT);
	double T = T_r[r];
	
	std::cerr << "#count\tmax\tE\tTemp\tBestEnergy\n";
	for(unsigned int count=0; count< 15*numMetropolisStep; ++count) {
		std::vector<double> tmpPara(numIndex()), localOptimalPara(numIndex());
		T=T_r[r];
		metropolis(E, 1/T, stepSize,numMetropolisStep,numUphill); 
		int TAccept = metropolisT(E, r, T_r);
		//Quench from lowest temperature
		if (T_r[r] == TMin && TAccept) {
			std::cerr << "#Start quenching...";
			double localOptima = E;
			//save current state
			for (size_t i =0; i<numIndex(); ++i) {
				tmpPara[i] = parameter(i);
				localOptimalPara[i] = parameter(i);
			}
			for (size_t j = 0; j< 40*numMetropolisStep; ++j) { 
				metropolis(E, 1000/TMin, stepSize,1,numUphill);
				if (E< localOptima) {
					localOptima=E;
					for (size_t i =0; i< numIndex(); ++i)
						localOptimalPara[i] = parameter(i);
				}
			}
			for (size_t i =0; i<numIndex(); ++i)
				setParameter(i, tmpPara[i]);
			std::cerr << "Done!\n";
			// print energy value of local optima and then the parameters on the next row
			std::cout << "#" << localOptima << "\n";
			for ( size_t i =0; i<numIndex(); i++)
				std::cout << localOptimalPara[i] << "\t";
			std::cout << "\n";
			std::cout.flush();
		}
		std::cerr << count << "\t" << 15*numMetropolisStep << "\t"
							<<  E << "\t" << T_r[r] << "\t" << globalOptima() 
							<<"\n";
	}
	//std::cout << "\n" << globalOptima() << "\t";
	printOptima();
	std::cout.flush();
}

void SimulatedTempering::initializeTemp(std::vector<double> &T_r, const double TMax, const double TMin) 
{
	if ( TMax<TMin ) 
	{ std::cerr << "initializeTemp() TMax cannot be smaller than TMin\n";exit(-1);}
	
	double RInv = 1/(static_cast<double>(T_r.size()-1));
	double step = exp( (RInv)*log(TMax/TMin) );		     
	double T = TMin;
	for (size_t r=0; r<T_r.size() ; r++ ) {
		T_r[r] = T;
		T *= step;
	}
	std::cerr << "#T\n";
	for (size_t r=0; r<T_r.size() ; r++ )
		std::cerr << T_r[r] <<"\n"; 
}

//Sets initial values gor the Gr.parameters
void SimulatedTempering::GrInit(const std::vector<double> &T_r, const unsigned int SERIE, const double stepSize)
{
	std::vector<double> EMean(T_r.size());
	double E = computeCost();
	double g=0;
	std::cerr << "# T\tE\tGr\n";
	for (int r = int(T_r.size()-1); r>=0; r-- ) {
		double beta = 1/T_r[r];
		int numUphill;
		for (unsigned int i = 0; i < SERIE; i++) {
			metropolis(E, beta, stepSize,1,numUphill); 
			EMean[r] += E;
		}
		EMean[r] /=double(SERIE);

		if ( r != int(T_r.size()-1) )
		g -= EMean[r+1]*( 1/T_r[r] - 1/T_r[r+1]);
		Gr_[r] = g;
		std::cerr << T_r[r] << "\t" << EMean[r] << "\t" << Gr_[r] << "\n";
	}
}

//Adjusts the Gr parameters according to Gr->Gr+ ln(Pr) where Pr is the 
//probabilty of having a certain temperature. This should give a roughly
//uniform temperature distribution
void SimulatedTempering::GrAdjust(const std::vector<double> &T_r, const int SERIE, const int serie, const double stepSize)
{
	std::vector<double> Pr(T_r.size());
	double E = computeCost();
	size_t r = T_r.size()-1;
	int numUphill;
	std::cerr << "#Count\tMax\tTemp\tP\tEnergy\tBestEnergy\n";
	for (int count = 0; count < SERIE; count++) {
		double T = T_r[r];
		metropolis(E, 1/T, stepSize,serie,numUphill); 
		metropolisT(E, r, T_r);
		Pr[r]++;
		std::cerr << count << "\t" << SERIE << "\t" << T_r[r] << "\t" << Pr[r] 
				<< "\t" << E << "\t" << globalOptima() << "\n";
	}
	for(r=0 ; r<Gr_.size() ; r++ )
	Pr[r] /= double(SERIE);

	std::cerr << "#Gr\tP\n";
	for(r=0 ; r<Gr_.size() ; r++ )
		std::cerr << Gr_[r] << "\t" << Pr[r] << "\n";

	double eps = 1./double(SERIE);
	r = Gr_.size()-1;
	double delta = Pr[r]>eps ? Pr[r] : eps;
	double deltaR = log( delta ); 
	for( r=0 ; r<Gr_.size() ; r++ ){
		delta = Pr[r]>eps ? Pr[r] : eps; 
		Gr_[r] += log( delta )- deltaR;
	}
	std::cerr << "#Gr\n";
	for( r=0 ; r<Gr_.size() ; r++ )
	std::cerr << Gr_[r] << "\n";
}

//Takes a montecarlo-step in temperature			  
int SimulatedTempering::metropolisT(const double E,size_t &r,const std::vector<double> &T_r) 
{
	size_t rNew;
	if( r==0 ) 
		rNew = 1;
	else if( r==T_r.size()-1 ) 
		rNew = r-1;
	else if( myRandom::Rnd()>0.5 ) 
		rNew = r+1;
	else rNew = r-1;


	double pArg = Gr_[rNew]-Gr_[r]+E*(1/T_r[rNew] - 1/T_r[r] );
	if( pArg<=0. || exp(-pArg)>myRandom::Rnd() ) {
		r = rNew;
		return 1;
	}
	return 0;
}


int SimulatedAnnealing::metropolis(double &E, const double invT, double stepSize, const size_t numMetropolisStep, int &numUphill)
{
	int numSuccess = 0;
	numUphill=0;
	for (size_t i=0; i<numMetropolisStep; ++i) {
		//Pick index at random
		size_t position = size_t(numIndex() * myRandom::Rnd());
		double p = parameter(position);
		//Change f to 1/f in 50 percent of the cases
		if (myRandom::Rnd() > 0.5)
		stepSize=1/stepSize;
		setParameter(position, stepSize*p);
		double ENew = computeCost();
		double deltaE = ENew - E;
		double r = myRandom::Rnd();
		if (deltaE < 0 || exp(-invT*deltaE) > r ) {
			E=ENew;
			numSuccess++;
			if (deltaE>0.0) {
				numUphill++;
			}
		}
		else {
			setParameter(position, p);
		}
		if (E<globalOptima())
		setGlobal(E);
	}
	return numSuccess;
}

int MetaSimulatedAnnealing::metropolis(double &E, const double invT, double stepSize, const size_t numMetropolisStep, int &numUphill)
{
	int numSuccess = 0;
	numUphill=0;
	for (size_t i=0; i<numMetropolisStep; ++i) {
		//Pick index at random
		size_t position = size_t(numIndex() * myRandom::Rnd());
		double p = parameter(position);
		//Change f to 1/f in 50 percent of the cases
		if (myRandom::Rnd() > 0.5)
		stepSize=1/stepSize;
		setParameter(position, stepSize*p);
		double ENew = computeCost();
		double deltaE = ENew - E;
		double r = myRandom::Rnd();
		if (deltaE < 0 || exp(-invT*deltaE) > r ) {
			E=ENew;
			numSuccess++;
			if (deltaE>0.0) {
				numUphill++;
			}
		}
		else {
			setParameter(position, p);
		}
		if (E<globalOptima())
		setGlobal(E);
	}
	return numSuccess;
}

int SimulatedAnnealingWorkers::metropolis(double &E, const double invT, double stepSize, const size_t numMetropolisStep)
{
  int numSuccess = 0;
  for (size_t i=0; i<numMetropolisStep; ++i) {
    //Pick index at random
    size_t position = size_t(numIndex() * myRandom::Rnd());
    double p = parameter(position);
    //Change f to 1/f in 50 percent of the cases
    if (myRandom::Rnd() > 0.5)
      stepSize=1/stepSize;
    setParameter(position, stepSize*p);
    double ENew = computeCost();
    double deltaE = ENew - E;
    double r = myRandom::Rnd();
    if (deltaE < 0 || exp(-invT*deltaE) > r ) {
      E=ENew;
      numSuccess++;
    }
    else {
      setParameter(position, p); // reset parameter value
    }
    if (E<globalOptima())
      setGlobal(E);
  }
  return numSuccess;
}



int SimulatedTempering::metropolis(double &E, const double invT, double stepSize, const size_t numMetropolisStep, int &numUphill)
{
	int numSuccess = 0;
	numUphill=0;
	for (size_t i=0; i<numMetropolisStep; ++i) {
		//Pick index at random
		size_t position = size_t(numIndex() * myRandom::Rnd());
		double p = parameter(position);
		//Change f to 1/f in 50 percent of the cases
		if (myRandom::Rnd() > 0.5)
		stepSize=1/stepSize;
		setParameter(position, stepSize*p);
		double ENew = computeCost();
		double deltaE = ENew - E;
		double r = myRandom::Rnd();
		if (deltaE < 0 || exp(-invT*deltaE) > r ) {
			E=ENew;
			numSuccess++;
			if (deltaE>0.0) {
				numUphill++;
			}
		}
		else {
			setParameter(position, p);
		}
		if (E<globalOptima())
		setGlobal(E);
	}
	return numSuccess;
}
