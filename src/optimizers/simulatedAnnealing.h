//
// Filename     : simulatedAnnealing.h
// Description  : Classes defining optimization algorithms
// Author(s)    : Pontus Melke (pontus@thep.lu.se), André Larsson (meta and worker methods, andre@thep.lu.se)
// Created      : August 2006
// Revision     : $Id: simulatedAnnealing.h 656 2016-01-21 14:33:38Z andre $
//

#ifndef SIMULATEDANNEALING_H
#define SIMULATEDANNEALING_H

#include "baseOptimizer.h"
using namespace OrganismLib;

/*! 
 *  @brief Class defining the simulated annealing algorithm.
 * 
 *  A temperature is initialized at @f$Tinit@f$, and multiplied with a factor @f$Tstep@f$
 *  until the temperature reaches @f$Tfinal@f$. Between each temperature update,
 *  @f$numStep@f$ "metropolis" steps are taken. In each metropolis step, a parameter
 *  and direction is randomly chosen. The selected parameter is multiplied/divided
 *  with Pstep, and this move is accepted if this results in a lower energy, or if the
 *  following is true:
 *
 *  @f[ exp( - \Delta E/T) > r@f]
 *
 *  In the above expression, @f$ \Delta E =E_{new} - E_{old}@f$ is difference in the value
 *  of the cost function before/after the parameter update, and @f$ r @f$ is a random number
 *  uniformly distributed between zero and one.
 *
 *  In the optimizer file it is defined as
 *
 *  @verbatim
    simulatedAnnealing 5
    Tinit Tfinal Tstep
    Pstep numStep
   
    numParasToOptimize
    index0 minBoundary0 maxBoundary0
    ...
    indexN minBoundaryN maxBoundaryN
    
    @endverbatim
 *  plus settings for possible scaling options / list of initial parameter sets
 * 
 */
class SimulatedAnnealing : public BaseOptimizer {
 public:
	///Main constructor. It calls the constructor in class BaseOptimizer and
	///checks all optimization parameters
	SimulatedAnnealing(BaseOptimizationProblem *E, const std::string &file);
	SimulatedAnnealing(BaseOptimizationProblem *E, std::ifstream &IN);
	int metropolis(double &E, const double invT, double stepSize, const size_t numMetropolisStep, int &numUphill);
	void optimize();
};


/*!  @brief Class defining the meta simulated annealing algorithm. 
 *
 *  Works like the simulatedAnnealing algorithm, with the added option of running
 *  @f$numSA@f$ number of simulated annealing runs succesively. Further, before each
 *  simulated annealing run is started, @f$numInitSearch@f$ parameter sets are sampled.
 *  This is done by drawing the logarithmic of the para values from a uniform
 *  distribution, such that they do not exceed the user-specified limits.
 *  The best (lowest cost) of these parameter sets are then used as an initial
 *  condition for the simulated annealing minimization.
 *
 *  See the documentation of simulatedAnnealing for details of the implementation
 *  of the minimzation.
 *
 *  The final result returned from this optimizer is the all-best energy
 *  from the different simulated annealing runs.\n\n
 *  In the optimizer file, metaSimulatedAnnealing is defined as \n
 *  @verbatim
   metaSimulatedAnnealing 7
   Tinit Tfinal Tstep
   Pstep numStep
   numInitSearch numSA
  
   numParasToOptimize
   index0 minBoundary0 maxBoundary0
   ...
   indexN minBoundaryN maxBoundaryN
   
   @endverbatim
 *  plus list of initial parameter sets / possible scaling options
 * 
 */
class MetaSimulatedAnnealing : public BaseOptimizer {
 public:
	///Main constructor. It calls the constructor in class BaseOptimizer and
	///checks all optimization parameters
	MetaSimulatedAnnealing(BaseOptimizationProblem *E, const std::string &file);
	MetaSimulatedAnnealing(BaseOptimizationProblem *E, std::ifstream &IN);
	int metropolis(double &E, const double invT, double stepSize, const size_t numMetropolisStep, int &numUphill);
	void optimize();
};


/*! @brief Class defining the simulated annealing algorithm with workers
 *
 *  SimulatedAnnealingWorkers should be seen as a meta-optimizer, where each run can include several
 *  complete minimizations. This optimizer utilizes several "workers", each of them performing
 *  a simulated annealing minimization. The workers all keep track of their current
 *  parameters/energy/temperature.
 *
 *  This algorithm pauses after a "work shift" to reassign resources. In each work shift, each worker will
 *  do @f$ k_i @f$ temperature updates (making for a total of @f$ k_i\cdot numStep @f$ metropolis steps per
 *  worker), where the number @f$k_i @f$ is individual to each worker.
 *
 *  For each worker, temperatures are always initialized with @f$ Tinit @f$ and multiplied with @f$ Tfactor @f$ after each batch of
 *  of metropolis steps. The annealings always ends when the temperature reaches @f$ Tfinal @f$. The model parameters
 *  are updated by multiplication/division with @f$ Pstep @f$, and between each temperature update a total number of 
 *  @f$ numStep @f$ parameter updates are tried. See the documentation of simulatedAnnealing for a more
 *  detailed description of the implemented simulated annealing algorithm.
 * 
 *  Between the work shifts (mentioned above) the workers are sorted from lowest to to highest energy of their latest parameter set.
 *  The top @f$ numTop @f$ of these are currently in the best regions and
 *  are assigned more resources by setting their @f$ k_i=3 @f$, while the next @f$numMiddle@f$ 
 *  are in mid-level regions and are assigned @f$ k_i=1 @f$. The remaining  @f$ numBottom @f$
 *  workers gets a new paraset (random numbers are drawn within specified parameter ranges, such that the logarithm of the values
 *  follows a uniform distribution) and restarts their annealing by resetting their temperature ("bottom-tier-restarts").
 * 
 *  Each worker will either reach the final temperature and restart, or reach the bottom tier and restart.
 *  The optimization stops when a total of @f$ maxNumRestarts @f$ restarts have been made
 *  (total restarts = number of times final temperature was reached + number of bottom-tier-restarts).
 *
 *  Some guidelines when using this optimizer: Keep track of how many restarts occur due to reaching the
 *  final temperature and how many that occur due to bottom-tier-restarts (can be read from the
 *  optimizer STDERR output). You might want that at least some of the workers reach their end
 *  temperature. Therefore @f$ maxNumRestarts @f$ should be set such that it is larger than the
 *  number of solutions in the bottom-tier times the number of temperature steps needed to complete an
 *  annealing run.
 *
 *  Note that it possible to remove the favoring of good solutions by setting @f$ numTop @f$ to zero,
 *  and remove the restarts of bad solutions by having @f$ numBottom=0 @f$, or to have a purely random
 *  parameter search by setting @f$ numTop = NumMiddle=0 @f$\n\n
 *  In the optimizer file this optimizer is defined as
 *  @verbatim
    simulatedAnnealingWorkers 9
    Tinit Tfinal Tstep
    Pstep numStep
    numTop numMiddle numBottom
    maxNumRestarts
  
    numParasToOptimize
    index0 minBoundary0 maxBoundary0
    ...
    indexN minBoundaryN maxBoundaryN
   
    @endverbatim
 *  plus list of settings for initial parameter sets / possible scaling options
 */
class SimulatedAnnealingWorkers : public BaseOptimizer {
 public:
	///Main constructor. It calls the constructor in class BaseOptimizer and
	///checks all optimization parameters
	SimulatedAnnealingWorkers(BaseOptimizationProblem *E, const std::string &file);
	SimulatedAnnealingWorkers(BaseOptimizationProblem *E, std::ifstream &IN);
	int metropolis(double &E, const double invT, double stepSize, const size_t numMetropolisStep);
	void optimize();
};


///
///@brief Class defining the simulated tempering algorithm. 
///
class SimulatedTempering : public BaseOptimizer {
 private: 
	std::vector<double> Gr_; 
 public:
	///Main constructor. It calls the constructor in class BaseOptimizer and
	///checks all optimization parameters
	SimulatedTempering(BaseOptimizationProblem *E, const std::string &file);
	SimulatedTempering(BaseOptimizationProblem *E, std::ifstream &IN);
	void optimize();
	void initializeTemp(std::vector<double> &T_r, 
											const double TMax, const double TMin);
	void GrInit(const std::vector<double> &T_r, const unsigned int SERIE, const double stepSize);
	void GrAdjust(const std::vector<double> &T_r, const int SERIE, const int serie, const double stepSize);
	int metropolisT(const double E,size_t &r,const std::vector<double> &beta_r);
	int metropolis(double &E, const double invT, double stepSize, const size_t numMetropolisStep, int &numUphill);
};

#endif
