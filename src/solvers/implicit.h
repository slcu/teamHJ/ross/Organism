#ifndef IMPLICIT_H
#define IMPLICIT_H

#include "baseSolver.h"

///
/// @brief An implicit Euler solver 
///
class ImplicitEuler : public BaseSolver {

private:

	size_t col_;
	double h1_;

public:
	///
	/// @brief Main constructor
	///
	ImplicitEuler(Organism *O,std::ifstream &IN);
	
	///
	/// @brief Reads the parameters used by the Implicit Euler
	///
	/// This function is responsible for reading parameters used by the implicit Euler 
	/// solver. The parameter file sent to the simulator binary looks like:
	///
	/// <pre> 
	/// ImplicitEuler
	/// T_start T_end 
	/// printFlag printNum 
	/// h1
	/// </pre> 
	///
	/// where ImplicitEuler is the identity string used by BaseSolver::getSolver
	/// to identify that the implicit Euler algorithm should be used. T_start
	/// (T_end) is the start (end) time for the simulation, printFlag is an
	/// integer which sets the output format (read by BaseSolver::print()),
	/// printNum is the number of equally spread time points to be printed. h1
	/// is the step size for each step, and col sets
	/// the column with dynamic data. If negative, the first compartment is used.
	///
	/// Comments can be included in the parameter file by starting the line with
	/// an #. Caveat: No check on the validity of the read data is applied.
	///
	/// @see BaseSolver::getSolver()
	/// @see BaseSolver::print()
	///
	void readParameterFile(std::ifstream &IN);
	
	void simulate(void);
	
	double maxDerivative();
};

///
/// @brief A ODE solver using an implicit Euler method with
/// possibility to compare the result to a cost template.
///
/// An implicit Euler algorithm that allows for cost calculations.
///
class ImplicitEulerCost : public BaseSolver {
  
private:
  
  double h1_;

public:

  ImplicitEulerCost(Organism *O,std::ifstream &IN);
  ~ImplicitEulerCost();
	
  ///
  /// @brief Reads the parameters used by the ImplicitEulerCost algorithm
  ///
  /// This function is responsible for reading parameters used by Implicit
  /// Euler algorithm which allows for cost calculations and reading some
  /// of the varables from a file. The parameter file sent to the simulator
  /// binary looks like:
  ///
  /// <pre> 
  /// ImplicitEulerCost
  /// costType
  /// T_start T_end 
  /// printFlag printNum 
  /// h1 col 
  /// numCostVar
  /// v_1 [v_2...]
  /// costTemplateFile
  /// </pre> 
  ///
  /// where ImplicitEulerCost is the identity string used by
  /// BaseSolver::getSolver to identify that the ImplicitEulerCost algorithm
  /// should be used. costType is the functional form (class) used for
  /// calculating the 'cost' (objective function value, energy...) of a
  /// simulation and possible functions for the moment are meanSquare and
  /// meanSquareRelative. T_start (T_end) is the start (end) time for the
  /// simulation, printFlag is an integer which sets the output format (read
  /// by BaseSolver::print()), printNum is the number of equally spread time
  /// points to be printed. h1 is the maximal (and initial) step size for each
  /// IE step, and col sets the column (variable) in the data that should be updated.
  /// numCostVar sets number of variables used in the cost calculation, followed by
  /// the list of variable indices and the name of the cost template file.
  ///
  /// Comments can be included in the parameter file by starting the line with
  /// an #. Caveat: No check on the validity of the read data is applied.
  ///
  /// @see BaseSolver::getSolver()
  /// @see BaseSolver::print()
  /// @see BaseCostFunction::createCostFunction()
  ///
  void readParameterFile(std::ifstream &IN, int verbose=0);
  void simulate(void);
};




///
/// @brief A ODE solver using an implicit Euler method with possibility to compare the result to a cost template.
///
/// An implicit Euler algorithm that allows for cost calculations.
///
class ImplicitEulerCostTemplate : public BaseSolver {
  
private:
  
  double h1_;

public:

  ImplicitEulerCostTemplate(Organism *O,std::ifstream &IN);
  ~ImplicitEulerCostTemplate();
	
  ///
  /// @brief Reads the parameters used by the ImplicitEulerCost algorithm
  ///
  /// This function is responsible for reading parameters used by the implicit Euler 
  /// solver. The parameter file sent to the simulator binary looks like:
  ///
  /// <pre> 
  /// ImplicitEulerCost
  /// costType
  /// T_start T_end 
  /// printFlag printNum 
  /// h1
  /// </pre> 
  ///
  /// where ImplicitEulerCost is the identity string used by
  /// BaseSolver::getSolver to identify that the ImplicitEulerCost algorithm
  /// should be used. costType is the functional form (class) used for
  /// calculating the 'cost' (objective function value, energy...) of a
  /// simulation and possible functions for the moment are meanSquare and
  /// meanSquareRelative. T_start (T_end) is the start (end) time for the
  /// simulation, printFlag is an integer which sets the output format (read
  /// by BaseSolver::print()), printNum is the number of equally spread time
  /// points to be printed. h1 is the maximal (and initial) step size for each
  /// Implicit Euler step
  /// Comments can be included in the parameter file by starting the line with
  /// an #. Caveat: No check on the validity of the read data is applied.
  ///
  /// @see BaseSolver::getSolver()
  /// @see BaseSolver::print()
  /// @see BaseCostFunction::createCostFunction()
  ///
  void readParameterFile(std::ifstream &IN, int verbose=0);
  void simulate(void);
};








#endif /* IMPLICIT_H */

