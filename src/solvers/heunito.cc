//
// Filename     : heunito.cc
// Description  : Heun numerical solver in the Ito interpretation (Carrillo et al. 2003 PRE)
// Author(s)    : Henrik Jonsson (henrik@thep.lu.se)
// Created      : December 2014
// Revision     : $Id:$
//
#include <cmath>
#include "heunito.h"
#include "../common/myRandom.h"

HeunIto::HeunIto(Organism *O,std::ifstream &IN)
  :BaseSolver(O,IN)
{
  readParameterFile(IN);
}

void HeunIto::readParameterFile(std::ifstream &IN)
{
  //Read in the needed parameters
  IN >> startTime_;
  t_=startTime_;
  IN >> endTime_;
  
  IN >> printFlag_; // output format
  IN >> numPrint_;  // number of time points printed to output
  
  IN >> h_;   // time step
  IN >> vol_; // effective volume
  IN >> volFlag_; //setting for knowing how to calculate volume of individual cells
}

void HeunIto::simulate() 
{
  // Check that h1 and endTime-startTime are > 0
  //
  if( !(h_>0. && (endTime_-startTime_)>0.) ) {
    std::cerr << "HeunIto::simulate() Wrong time borders or time step for "
	      << "simulation. No simulation performed.\n";
    return;
  }
  std::cerr << "Simulating using a simple HeunIto solver\n";

  // Initiate reactions for those where it is applicable
  //
  O_->reactionInitiate(startTime_,y_);

  // Introduce the neighborhood
  //
  if( O_->numNeighborhood() )
    O_->neighborhoodCreate(y_,t_);
  if( y_.size() && y_.size() != dydt_.size() ) {
    dydt_.resize( y_.size(),y_[0]);
  }
  assert( y_.size() == O_->numCompartment() && y_.size()==dydt_.size());

  // Create all vectors that will be needed by the solver
  //
  std::vector< std::vector<double> > sdydt( N() ),st( N() ),y1( N() ),dydt2( N() );
  //Resize each vector
  for( size_t i=0 ; i<N() ; i++ ) {
    sdydt[i].resize(M());
    st[i].resize( M() );
    y1[i].resize( M() );
    dydt2[i].resize( M() );
  }
  assert( y_.size()==sdydt.size() && y_.size()==st.size() &&
	  y_.size()==y1.size() && y_.size()==dydt2.size() );

  // Initiate print times
  //
  double tiny = 1e-10;
  double printTime=endTime_+tiny;
  double printDeltaTime=endTime_+2.*tiny;
  if( numPrint_<=0 )//No printing
    printFlag_=0;
  else if( numPrint_==1 ) {//Print last point (default)
  }
  else if( numPrint_==2 ) {//Print first/last point
    printTime=startTime_-tiny;
  } 
  else {//Print first/last points and spread the rest uniformly
    printTime=startTime_-tiny;
    printDeltaTime=(endTime_-startTime_)/double(numPrint_-1);
  }
  
  // Go
  //
  t_=startTime_;
  numOk_ = numBad_ = 0;
  while( t_<endTime_ ) {
    if (debugFlag()) {
      yCopy_[debugCount()] = y_;
    } 
    //Update the derivatives
    O_->derivs(y_,dydt_);
    
    //Print if applicable 
    if( printFlag_ && t_ >= printTime ) {
      printTime += printDeltaTime;
      print();
    }

    //Update
    heunito(sdydt,st,y1,dydt2);
    numOk_++;
    
    // Make updates of rections that requires it
    O_->reactionUpdate(h_, t_, y_);
    
    // Check for cell divisions, compartmental removal etc
    O_->compartmentChange(y_,dydt_,t_);

    // Rescale all temporary vectors as well
    if (y_.size() != sdydt.size()) {
      sdydt.resize(N(), sdydt[0]);
      st.resize(N(), st[0]);
      y1.resize(N(), y1[0]);
      dydt2.resize(N(), dydt2[0]);
    }

    //Update neighborhood
    if( O_->numNeighborhood() )
      O_->neighborhoodUpdate(y_,t_);
    
    //update time variable
    if( (t_+h_)==t_ ) {
      std::cerr << "HeunIto::simulate() Step size too small.";
      exit(-1);
    }
    t_ += h_;
  }
  if( printFlag_ ) {
    //Update the derivatives
    O_->derivs(y_,dydt_);
    print();
  }
  std::cerr << "Simulation done.\n"; 
  return;
}

void HeunIto::heunito(std::vector< std::vector<double> > &sdydt,
		      std::vector< std::vector<double> > &st,
		      std::vector< std::vector<double> > &y1,
		      std::vector< std::vector<double> > &dydt2)
{ 
  double hh=0.5*h_;
  size_t n = N();
  size_t m = M();
  size_t volumeIndex = O_->numTopologyVariable()-1;
  
  O_->derivsWithAbs(y_,dydt_,sdydt);//first step
  DataMatrix rand( N() );
  for(size_t i=0 ; i<N() ; ++i ) {
    // get cell volume
    double volume = 1.0;
    if (volFlag_==1) {
      volume = O_->compartment(i).getVolume(y_[i]);//y_[i][volumeIndex];
    }
    else if (volFlag_ > 1) {
      std::cerr << "HeunIto::heunito() Wrong volume flag given, only 0 "
		<< "(no cell volume dependence"
		<< " or 1 (volume calculated or read from variable) allowed." << std::endl;
      std::cerr << "See Compartment.getVolume()." << std::endl;
      exit(EXIT_FAILURE);
    }
    rand[i].resize( M() );
    for( size_t j=0 ; j<M() ; ++j ) {      
      rand[i][j] = myRandom::Grand();
      st[i][j] = sqrt(sdydt[i][j]*h_/(vol_*volume));
      y1[i][j] = y_[i][j]+h_*dydt_[i][j]+st[i][j]*rand[i][j];
      if(j>=volumeIndex && y1[i][j]<0.0 )// Setting and absortive barrier at 0 (not for positions)
	y1[i][j]=0.0; 
    }
  }
  //double mean=0.0;
  //for(size_t i=0 ; i<N() ; ++i ) {
  //for(size_t j=0 ; j<M() ; ++j ) {
  //  std::cerr << dydt_[i][j] << "," << sdydt[i][j] << " ";
  //}
  //std::cerr << std::endl;
  //}
  //std::cerr << std::endl;
  //std::cerr << "Average: " << mean/(N()*M()) << std::endl;

  O_->derivs(y1,dydt2);//second step
  for(size_t i=0 ; i<n ; ++i ) {
    for( size_t j=0 ; j<m ; ++j ) {
      y_[i][j] = y_[i][j]+hh*(dydt_[i][j]+dydt2[i][j])+st[i][j]*rand[i][j];      
      if(j>=volumeIndex && y_[i][j]<0.0 )// Setting and absortive barrier at 0 (not for positions)
	y_[i][j]=0.0; 
    }
  }
}

