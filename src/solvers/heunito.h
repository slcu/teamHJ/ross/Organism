#ifndef HEUNITO_H
#define HEUNITO_H
//
// Filename     : heunito.h
// Description  : Heun numerical solver in the Ito interpretation (Carrillo et al. 2003 PRE)
// Author(s)    : Henrik Jonsson (henrik@thep.lu.se)
// Created      : December 2014
// Revision     : $Id:$
//

#include "baseSolver.h"

///
/// @brief A simple HeunIto step solver class
///
/// This implementation is developed from adapting the method described in
/// Carrillo et al (2003), Physical Review E and (to be added by Pau)
///
class HeunIto : public BaseSolver {

 private:
  
  double h_;
  double vol_;
  int volFlag_;
  
 public:
  ///
  /// @brief Main constructor
  ///
  HeunIto(Organism *O,std::ifstream &IN);
  
  ///
  /// @brief Reads the parameters used by the HeunIto solver
  ///
  /// This function is responsible for reading parameters used by the
  /// HeunIto algorithm. The parameter file sent to the simulator
  /// binary looks like:
  ///
  /// <pre> 
  /// HeunIto
  /// T_start T_end 
  /// printFlag printNum 
  /// h vol volFlag
  /// </pre> 
  ///
  /// where HeunIto is the identity string used by BaseSolver::getSolver
  /// to identify that the HeunIto algorithm should be used. T_start
  /// (T_end) is the start (end) time for the simulation, printFlag is an
  /// integer which sets the output format (read by BaseSolver::print()),
  /// printNum is the number of equally spread time points to be printed. h
  /// is the step size for each HeunIto step. vol is an effective volume
  /// setting the noise level and volFlag determines how volumes of single cells are
  /// calculated. volFlag=0 assumes all cells same size, volFlag=1 uses a volume
  /// variable (the last topology variable), volFlag=2 will assume cells encoded as
  /// spheres and the radius is read (last topology variable).
  ///
  /// Comments can be included in the parameter file by starting the line with
  /// an #. Caveat: No check on the validity of the read data is applied.
  ///
  /// @see BaseSolver::getSolver()
  /// @see BaseSolver::print()
  ///
  void readParameterFile(std::ifstream &IN);
	
  ///
  /// @brief Runs a simulation of an organism model
  ///
  void simulate(void);
	
  ///
  /// @brief An HeunIto step
  ///
  void heunito(std::vector< std::vector<double> > &sdydt,
	       std::vector< std::vector<double> > &st,
	       std::vector< std::vector<double> > &y1,
	       std::vector< std::vector<double> > &dydt2);
};

///
/// @brief A simple HeunIto step solver class, where negative values are truncated to zero and NaN Inf 
/// truncated to maxVal_ (parameter given)
///
/// WARNING! This solver will give numerical errors, and the results
/// cannot be trusted. It is implemented for use as a quick test of a
/// simulation.
///
/// The algorithm takes HeunIto steps and truncates values at zero and NaN,Inf
/// i.e. negative values will be set to zero and NaN,Inf are set to user provided maxVal.
///
class HeunItoTruncated : public BaseSolver {
  
 private:
  
  double h_;
  double vol_;
  double maxVal_;  
 public:
  ///
  /// @brief Main constructor
  ///
  HeunItoTruncated(Organism *O,std::ifstream &IN);
  
  ///
  /// @brief Reads the parameters used by the HeunIto solver
  ///
  /// This function is responsible for reading parameters used by the
  /// HeunIto algorithm. The parameter file sent to the simulator
  /// binary looks like:
  ///
  /// <pre> 
  /// HeunItoTruncated
  /// T_start T_end 
  /// printFlag printNum 
  /// h maxVal vol
  /// </pre> 
  ///
  /// where HeunItoTruncated is the identity string used by BaseSolver::getSolver
  /// to identify that the HeunIto algorithm should be used. T_start
  /// (T_end) is the start (end) time for the simulation, printFlag is an
  /// integer which sets the output format (read by BaseSolver::print()),
  /// printNum is the number of equally spread time points to be printed. h
  /// is the step size for each HeunIto step. maxVal is the (LARGE) value replacing
  /// NaN and Inf generated in the simulation.
  ///
  /// Comments can be included in the parameter file by starting the line with
  /// an #. Caveat: No check on the validity of the read data is applied.
  ///
  /// @see BaseSolver::getSolver()
  /// @see BaseSolver::print()
  ///
  void readParameterFile(std::ifstream &IN);
	
  ///
  /// @brief Runs a simulation of an organism model
  ///
  void simulate(void);
	
  ///
  /// @brief An HeunIto step with truncation of negative numbers to zero
  ///
  void heunitoTrunc();
};

#endif /* HEUNITO_H */

