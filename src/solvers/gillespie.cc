//
// Filename     : gillespie.cc
// Description  : Simple Gillespie numerical solvers
// Author(s)    : Henrik Jonsson (henrik@thep.lu.se)
// Created      : March 2009
// Revision     : $Id:$
//
#include <cmath>
#include "gillespie.h"
#include "../common/myRandom.h"

Gillespie::Gillespie(Organism *O,std::ifstream &IN)
  :BaseSolver(O,IN)
{
  readParameterFile(IN);
  O->createReactionList(reaction_,species_);
}

void Gillespie::readParameterFile(std::ifstream &IN)
{
  //Read in the needed parameters
  IN >> startTime_;
  t_=startTime_;
  IN >> endTime_;
  
  IN >> printFlag_;
  IN >> numPrint_;
}

void Gillespie::simulate() 
{
  //Check that h1 and endTime-startTime are > 0
  //////////////////////////////////////////////////////////////////////
  if( endTime_<=startTime_ ) {
    std::cerr << "Gillespie::simulate() Wrong time borders for "
	      << "simulation. No simulation performed.\n";
    return;
  }
  std::cerr << "Simulating using a simple Gillespie solver\n";

  // Initiate reactions for those where it is applicable
  //
  O_->reactionInitiate(startTime_,y_);

  //Introduce the neighborhood
  //////////////////////////////////////////////////////////////////////
  if( O_->numNeighborhood() )
    O_->neighborhoodCreate(y_,t_);
  if( y_.size() && y_.size() != dydt_.size() ) {
    dydt_.resize( y_.size(),y_[0]);
  }
  assert( y_.size() == O_->numCompartment() && y_.size()==dydt_.size());
  
  // Initiate print times
  //////////////////////////////////////////////////////////////////////
  double tiny = 1e-10;
  double printTime=endTime_+tiny;
  double printDeltaTime=endTime_+2.*tiny;
  if( numPrint_<=0 )//No printing
    printFlag_=0;
  else if( numPrint_==1 ) {//Print last point (default)
  }
  else if( numPrint_==2 ) {//Print first/last point
    printTime=startTime_-tiny;
  } 
  else {//Print first/last points and spread the rest uniformly
    printTime=startTime_-tiny;
    printDeltaTime=(endTime_-startTime_)/double(numPrint_-1);
  }
  
  // Go
  //////////////////////////////////////////////////////////////////////
  t_=startTime_;
  h_=0.0;
  numOk_ = numBad_ = 0;
  int smallStepCounter=0;

  while( t_+h_<endTime_) {
    if (debugFlag()) {
      yCopy_[debugCount()] = y_;
    } 
    //Update the derivatives
    //O_->derivs(y_,dydt_);
    
    //Print if applicable 
    if( printFlag_ && t_ >= printTime ) {
      printTime += printDeltaTime;
      print();
    }
    // Update
    //
    size_t numReaction = reaction_.size(); 
    size_t numCell = y_.size(); 
    if (numReaction && numCell) {
      a_.resize( numReaction*numCell );
      b_.resize( a_.size() );
      for (size_t cellIndex=0; cellIndex<numCell; ++cellIndex)
	for (size_t r=0; r<numReaction; ++r) {
	  size_t i = cellIndex*numReaction + r;
	  a_[i] = reaction_[r]->
	    propensity(O_->compartment(cellIndex),species_[r],y_);
	  b_[i] = ( i==0 ? a_[i] : b_[i-1]+a_[i] );
	}
    }
    //std::cerr << "a: ";
    //for (size_t ii=0; ii<a_.size(); ++ii)
    //std::cerr << a_[ii] << " ";
    //std::cerr << "\nb: ";
    //for (size_t ii=0; ii<b_.size(); ++ii)
    //std::cerr << b_[ii] << " ";
    //std::cerr << std::endl;

    //Select according to Gillespie
    size_t i=0;
    // Selects reaction i and updates time
    if (gillespie(i)) {
      // Update according to selected reaction (i)
      size_t r = i % numReaction;
      size_t cellIndex = (i-r)/numReaction; //i - r*numCell;
      
      //std::cerr << "tau=" << h_ << " i=" << i << " r=" << r << " cell=" << cellIndex << std::endl;

      reaction_[r]->discreteUpdate(O_->compartment(cellIndex),species_[r],y_);
      //update time variable
      if( (t_-h_)==t_ ) {
	smallStepCounter++;
	std::cerr << "Gillespie::simulate() Step size too small (" << smallStepCounter
		  << ")" << std::endl;
	if (smallStepCounter>100) {
	  exit(-1);
	}
      }
      else {
	smallStepCounter=0;
      }
      numOk_++;
      // For specific gillespie printing
      //std::cout << t_ << "\t" << i << "\t";
      //for (size_t ii=0; ii<y_.size(); ++ii)
      //for (size_t j=0; j<y_[ii].size(); ++j)
      //  std::cout << y_[ii][j] << " ";
      //for (size_t ii=0; ii<a_.size(); ++ii)
      //std::cout << a_[ii] << " ";
      //std::cout << std::endl;
    }

    // Make updates of rections that requires it
    //O_->reactionUpdate(h_, t_, y_);
    
    // Check for cell divisions, compartmental removal etc
    //O_->compartmentChange(y_,dydt_,t_);
    
    //Update neighborhood
    //if( O_->numNeighborhood() )
    //O_->neighborhoodUpdate(y_,t_);
    
  }
  if( printFlag_ ) {
    print();
  }
  std::cerr << "Gillespie Simulation done.\n"; 
  return;
}

size_t Gillespie::gillespie(size_t &i)
{
  // start of the main part of the function this part will generate the
  // mechanics of the system
  // Initiate the two random numbers and do the following
  size_t numReaction = b_.size();
  r1_=myRandom::ran3();
  r2_=myRandom::ran3();
  // converting to timestep, instead of tau use h_
  h_=(1/b_[numReaction-1])*std::log(1/r1_);
  // testing if the new timestep plus the point in time overreaches the time
  // border set by the program
  if (!(t_+h_>endTime_))
    {
      double fraction = r2_*b_[numReaction-1]; 
      // converting to the correct reaction
      if (fraction<=b_[0])
	{
	  i=0;
	  //std::cerr << i << " 0 < " << fraction << " < " << b_[0] << std::endl;
	}
      else
	{
	  for(size_t b=1; b<numReaction; ++b)
	    {
	      if ((fraction<=b_[b]) && (fraction>b_[b-1]))
		{
		  i=b;
		  //std::cerr << i << " " << b_[b] << " > " << fraction << " > " << b_[b-1] << std::endl;
		  break;
		}
	    }
	}
      // Updating the time with the aid of h_
      t_=t_+h_;
      return 1;
    }
  return 0;
}
