#ifndef GILLESPIE_H
#define GILLESPIE_H
//
// Filename     : gillespie.h
// Description  : Simple Gillespie numerical solvers
// Author(s)    : Henrik Jonsson (henrik@thep.lu.se)
// Created      : March 2009
// Revision     : $Id:$
//

#include "baseSolver.h"
#include "../reactions/baseReaction.h"

///
/// @brief A simple Gillespie step solver class
///
class Gillespie : public BaseSolver {

 private:
  
  double h_ , r1_ , r2_;
  int numReaction_;
  std::vector<BaseReaction*> reaction_;
  std::vector<size_t> species_;
  std::vector<double> a_;
  std::vector<double> b_;

 public:
  ///
  /// @brief Main constructor
  ///
  Gillespie(Organism *O,std::ifstream &IN);
  
  ///
  /// @brief Reads the parameters used by the Gillespie solver
  ///
  /// This function is responsible for reading parameters used by the
  /// Gillespie algorithm. The parameter file sent to the simulator
  /// binary looks like:
  ///
  /// <pre> 
  /// Gillespie
  /// T_start T_end 
  /// printFlag printNum 
  /// h 
  /// </pre> 
  ///
  /// where Gillespie is the identity string used by BaseSolver::getSolver
  /// to identify that the Gillespie algorithm should be used. T_start
  /// (T_end) is the start (end) time for the simulation, printFlag is an
  /// integer which sets the output format (read by BaseSolver::print()),
  /// printNum is the number of equally spread time points to be printed. h
  /// is the step size for each Gillespie step.
  ///
  /// Comments can be included in the parameter file by starting the line with
  /// an #. Caveat: No check on the validity of the read data is applied.
  ///
  /// @see BaseSolver::getSolver()
  /// @see BaseSolver::print()
  ///
  void readParameterFile(std::ifstream &IN);
	
  ///
  /// @brief Runs a simulation of an organism model
  ///
  void simulate(void);
	
  ///
  /// @brief An Gillespie step
  ///
  size_t gillespie(size_t &i);
};

#endif /* EULER_H */

