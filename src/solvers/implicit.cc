//
// Filename     : implicit.cc
// Description  : Implicit solvers
// Author(s)    : Henrik Jonsson (henrik@thep.lu.se)
// Created      : May 2009
// Revision     : $Id:$
//
#include <cmath>
#include "implicit.h"
#include "../common/la/SparseMatrix.h"
#include "../common/la/DenseVector.h"
#include "../common/la/LUSolver.h"



ImplicitEuler::ImplicitEuler(Organism *O,std::ifstream &IN)
  :BaseSolver(O,IN)
{
  readParameterFile(IN);
}

void ImplicitEuler::readParameterFile(std::ifstream &IN)
{
  IN >> startTime_;
  t_= startTime_;
  IN >> endTime_;
  
  IN >> printFlag_;
  IN >> numPrint_;
  
  IN >> h1_;
}

void ImplicitEuler::simulate(void)
{ 
  //Check that h1 and endTime - startTime are > 0
  //////////////////////////////////////////////////////////////////////
  if (!(h1_ > 0.0 && (endTime_ - startTime_)) > 0.0)
    {//either h or (endTime-startTime) <=0
      std::cerr << "ImplicitEuler::simulate() - "
		<< "Wrong time borders or time step for simulation. "
		<< "No simulation performed.\n";
      exit(-1);
    }

  // Initiate reactions for those where it is applicable
  //
  O_->reactionInitiate(startTime_,y_);

  //Introduce the neighborhood
  //////////////////////////////////////////////////////////////////////
  if (O_->numNeighborhood())
    O_->neighborhoodCreate(y_, t_);
  if (y_.size() && y_.size() != dydt_.size()) {
    dydt_.resize(y_.size(), y_[0]);
  }
  assert( y_.size() == O_->numCompartment() && y_.size()==dydt_.size());
  //Used here
  //////////////////////////////////////////////////////////////////////


  //y_Width is the number of columns in y_
  size_t y_Width = y_[0].size();
  //y_Height is the number of rows in y_
  size_t y_Height = y_.size();
  //N is the total number of cells in y_
  size_t N = y_Width*y_Height;
  
  SparseMatrix A(N,N);
  
  //DyNew is a one-dimensional vector containing "y_New - y_Old"/h
  DenseVector DyNew(N);

  //f_y is a one-dimensional vector containing dydt
  DenseVector f_y(N);

  LUSolver lu;
  O_->Jacobian(y_,A);
  A = -h1_*A;
  for (size_t i=0; i<N; ++i) {
    A(i,i) = 1+A(i,i);
  }

  lu.lu_factorize(A);

  DenseVector *new_Dy = &DyNew;
  
  // Initiate print times
  //////////////////////////////////////////////////////////////////////
  double tiny = 1e-20;
  double printTime = endTime_ + tiny;
  double printDeltaTime = endTime_ + 2.0 * tiny;
  if (numPrint_ <= 0) //No printing
    printFlag_ = 0;
  else if (numPrint_ == 1) { // Print last point (default)
  }
  else if (numPrint_ == 2) { //Print first/last point
    printTime = startTime_ - tiny;
  } 
  else { //Print first/last points and spread the rest uniformly
    printTime = startTime_ - tiny;
    printDeltaTime = (endTime_ - startTime_) / ((double) (numPrint_ - 1));
  }
  
 // Go
  //////////////////////////////////////////////////////////////////////
  t_ = startTime_;
  numOk_ = numBad_ = 0;
  for (unsigned int nstp = 0;; nstp++) {
    if (debugFlag()) {
      yCopy_[debugCount()] = y_;
    } 
    // Print if applicable 
    if (printFlag_ && t_ >= printTime) {
      printTime += printDeltaTime;
      print();
    }
    
    //Update by solving (1-hA)*(y_old-y_new)/h = f(y_old)
    
    O_->derivs(y_, dydt_);

    //Putting the dydt matrix into the f_y vector.
    for(size_t i=0; i<y_Height;i++){
      for(size_t j=0; j<y_Width;j++){ 
	f_y(y_Width*i + j) = dydt_[i][j];
      }
    }

    //std::cerr << "A: " << nstp << std::endl; 
    //for (size_t n=0; n<N; ++n) {
    // for (size_t nn=0; nn<N; ++nn)
    //	std::cerr << A(n,nn) << " ";
    // std::cerr << std::endl;
    //}

    //std::cerr << "f:" << std::endl;
    //for (size_t n=0; n<N; ++n)
    //std::cerr << f_y(n) << " ";
    //std::cerr << std::endl;
    
  lu.lu_substitute(A,*new_Dy,f_y);

  //std::cerr << "Dy:" << std::endl;
  //for (size_t n=0; n<N; ++n)
  // std::cerr << DyNew(n) << " ";
  //std::cerr << std::endl;

    //New------------------------------
    // Copy new vector into y
    for (size_t i=0; i<y_Height; ++i) {
      for(size_t j=0; j<y_Width; ++j){
	y_[i][j] += h1_*(*new_Dy)(y_Width*i + j);
      }
    }
    //---------------------------------

    //std::cerr << "New y:" << std::endl;
    //for (size_t i=0; i<y_Height; ++i) {
    // for(size_t j=0; j<y_Width; ++j)
    //	std::cerr << y_[i][j]  << " ";
    // std::cerr << std::endl;
    //}
    
    t_ += h1_;
    ++numOk_;
    
    // Make updates of reactions that requires it
    O_->reactionUpdate(h1_, t_, y_);
    
    // Check for cell divisions, compartmental removal etc
    //O_->compartmentChange(y_, dydt_, t_);
    //N = y_.size()*y_[0].size();
    
    // Update neighborhood
    if (O_->numNeighborhood())
      O_->neighborhoodUpdate(y_, t_);
    
    // If the end t is passed return (print if applicable)
    if (t_ >= endTime_) {
      if (printFlag_) {
	// Update the derivatives
	O_->derivs(y_, dydt_);
	print();
      }
      std::cerr << "Simulation done.\n"; 
      return;
    }
    // Update A
    //O_->Jacobian(y_,A);
    //A = -h1_*A;
    //for (size_t i=0; i<N; ++i) {
    //  A(i,i) = A(i,i)+1;
    //}
    //lu.lu_factorize(A);
  }
}

ImplicitEulerCost::ImplicitEulerCost(Organism *O,std::ifstream &IN)
  :BaseSolver(O,IN)
{
  readParameterFile(IN);
}

ImplicitEulerCost::~ImplicitEulerCost()
{
  //std::cerr << "delete costFunction...";
  delete C_;
  //std::cerr << "Done!\n";
}

void ImplicitEulerCost::readParameterFile(std::ifstream &IN, int verbose)
{
  verbose=1;
  if (verbose) {
    std::cerr << "ImplicitEulerCost: Reading parameter file:" << std::endl;
  }
  std::string costType;
  IN >> costType;
  C_ = BaseCostFunction::createCostFunction(costType);
  
  IN >> startTime_;
  t_= startTime_;
  IN >> endTime_;
  
  IN >> printFlag_;
  IN >> numPrint_;
  
  IN >> h1_;
  
  if (verbose) {
    // std::cerr << "Cost function: " << costType << std::endl
    // 	      << "T_start/end: " << startTime_ << " " << endTime_ << std::endl
    // 	      << "Print_flag: " << printFlag_ << " PrintNum: " << numPrint_ << std::endl
    // 	      << "Initial/max step: " << h1_ << " col: " << col_ << std::endl;
  }
  
  //
  //simulation template parameters
  //
  //size_t Mtmp;
  //IN >> Mtmp;
  
  //simulationIndex_.resize( Mtmp );
  
  //if (Mtmp) {
  //if (verbose)
  //  std::cerr << Mtmp << " variables read from template." << std::endl;
  //for (size_t i=0 ; i<Mtmp ; ++i) {
  //  IN >> simulationIndex_[i];
  //  if (verbose)
  //std::cerr << simulationIndex_[i] << " ";
  //}
  //IN >> inputTemplateFile_;
  //if (verbose)
  //  std::cerr << std::endl << "Template file " << inputTemplateFile_ << std::endl; 
  //}
  //else
  //if (verbose)
  //  std::cerr << "No template file used." << std::endl; 
  
  //
  //cost template parameters
  //
  size_t Mtmp;
  IN >> Mtmp;
  std::vector<size_t> costList;
  
  if( Mtmp ) {
    if (verbose)
      std::cerr << Mtmp << " variables used in cost calculation\n";
    costList.resize(Mtmp);
    for (size_t i=0 ; i<Mtmp ; i++ ) {
      IN >> costList[i];
      if (verbose)
	std::cerr << costList[i] << " ";
    }
    if (verbose)
      std::cerr << "\n";
    setCostList(costList);
    std::string costTemplateFile;	
    IN >> costTemplateFile;
    setCostTemplateFile(costTemplateFile);
    if (verbose)
      std::cerr << "Cost template file " << costTemplateFile << "\n"; 
    C_->initiateCostCalculation(startTime_, endTime_);
  }
  else
    if (verbose)
      std::cerr << "No cost template file used\n";
  //IN.close();
  if (verbose) {
    std::cerr << "ImplicitEulerCost: Done reading parameter file." << std::endl << std::endl;
  }
}

void ImplicitEulerCost::simulate(void)
{ 
  size_t verbose=0;
  //
  //Check that h1 and endTime - startTime are > 0
  //
  if (!(h1_ > 0.0 && (endTime_ - startTime_)) > 0.0)
    {//either h or (endTime-startTime) <=0
      std::cerr << "ImplicitEuler::simulate() - "
		<< "Wrong time borders or time step for simulation. "
		<< "No simulation performed.\n";
      exit(-1);
    }
  //
  // Set variables to be simulated, and if template is needed
  //
  //   static bool flag = true;
  //   if(flag)
  //     setSimulationFlag();
  //   flag = false;
  //   size_t simCount=0;
  //   size_t templateFlag=1;
  //   for (size_t i=0 ; i<M() ; i++ ) {
  //     simCount += simulationFlag_[i];
  //   }
  //   if( simCount==M() ) 
  //     templateFlag = 0;

  //
  // Check if the costList is well defined
  //
  for (size_t i=0; i<C_->sizeOfCostList(); ++i) {
    if (C_->costList(i)>=M()) {
      std::cerr << "ImplicitEulerCost::simulate(): "
		<< C_->costList(i) << " is out of scope\n";
      exit(-1);
    }
  }

  // Initiate reactions for those where it is applicable
  //
  O_->reactionInitiate(startTime_,y_);

  //
  //Introduce the neighborhood
  //
  if (O_->numNeighborhood())
    O_->neighborhoodCreate(y_, t_);
  if (y_.size() && y_.size() != dydt_.size()) {
    dydt_.resize(y_.size(), y_[0]);
  }
  assert( y_.size() == O_->numCompartment() && y_.size()==dydt_.size());
  //
  // Introduce simulation variables used here
  //
  size_t N = y_.size();
  SparseMatrix A(N,N);
  DenseVector yOld(N);
  DenseVector yNew(N);
  DenseVector b(N);
  for (size_t i=0; i<N; ++i) {
    //yOld(i) = y_[i][col_];
    //yNew(i) = y_[i][col_];
  }
  O_->Jacobian(y_,A);
  //size_t bUpdate = O_->initiateConstantVector(y_,b,col_);
  size_t bUpdate=0;
  LUSolver lu;
  A = -h1_*A;
  for (size_t i=0; i<N; ++i) {
    A(i,i) = 1+A(i,i);
  }
  lu.lu_factorize(A);
  if (bUpdate)
    b = h1_*b;
  DenseVector *old_y = &yOld;
  DenseVector *new_y = &yNew;
  DenseVector *tmp_y;

  //
  // Initiate cost Calculation if applicable
  //
  double endTimeLocal = endTime_;
  if( C_->sizeOfCostList() ) {
    //C_->initiateCostCalculation(startTime_, endTime_);
    C_->resetCostCalculation();
    if (C_->sizeOfCostTemplateTime() ) {
      endTimeLocal = C_->endTime();// +tiny;  
    }
  }
  
  //
  // Initiate print times
  //
  double tiny = 1e-20;
  double printTime = endTime_ + tiny;
  double printDeltaTime = endTime_ + 2.0 * tiny;
  if (numPrint_ <= 0) //No printing
    printFlag_ = 0;
  else if (numPrint_ == 1) { // Print last point (default)
  }
  else if (numPrint_ == 2) { //Print first/last 
    printTime = startTime_ - tiny;
  } 
  else { //Print first/last points and spread the rest uniformly
    printTime = startTime_ - tiny;
    printDeltaTime = (endTime_ - startTime_) / ((double) (numPrint_ - 1));
  }
  //
  // Initiate variables for simulation 
  //
  t_ = startTime_;
  numOk_ = 0;

  // Initiate template from file if applicable
  //
  //   std::ifstream IN;
  //   if( templateFlag ) {
  //     const char *tmp = inputTemplateFile_.c_str();
  //     IN.open( tmp );
  //     if( !IN ) {
  //       std::cerr << "ImplicitEulerCost::simulate() "
  // 		<< "Cannot open templatefile " << inputTemplateFile_
  // 		<< "\n\n\7";exit(-1);}
  //     //Read previous and future time points
  //     initiateValuesTemplate(IN,divIndex,divTime);
  //   }

  //
  // Start simulation
  //
  for (unsigned int nstp = 0;; nstp++) {
    if (debugFlag()) {
      yCopy_[debugCount()] = y_;
    } 
    // Print if applicable 
    if (printFlag_ && t_ >= printTime) {
      printTime += printDeltaTime;
      print();
    }
    
    // Update by solving yNew (1-hA) = yOld
    lu.lu_substitute(A,*new_y,yOld);
    // Copy new vector into y
    for (size_t i=0; i<N; ++i) {
      //y_[i][col_] = (*new_y)(i);
    }
    tmp_y = new_y; 
    new_y = old_y;
    old_y = tmp_y;

    t_ += h1_;
    ++numOk_;
    
    //     
    // Update other cols of y from template if applicable
    //
    //  if( templateFlag )
    //    allValuesTemplate();//time is already updated

    // Make updates of rections that requires it
    O_->reactionUpdate(h1_, t_, y_);
    
    // Check for cell divisions, compartmental removal etc
    O_->compartmentChange(y_, dydt_, t_);
    N = y_.size();
    //     if( templateFlag ) {
    //       int tmpDivCount=0;
    //       while( division<divIndex.size() && t_>=divTime[division] ) {
    // 				divideTemplate(divIndex[division]);
    // 				division++;
    // 				tmpDivCount++;
    //       }
    //     }

    //
    // Add cost from template comparison if applicable
    //
    if( C_->sizeOfCostList() && C_->costTemplateIndex()< C_->sizeOfCostTemplateTime()
	&& t_>= C_->costTemplateTime(C_->costTemplateIndex() ) ) {
      C_->addCost(y_,t_); 
    }
    
    //
    // Check if new time point data needs to be read from file
    //
    //     if( templateFlag && t_>=futureTime_ && t_<endTimeLocal ) {
    //       updateValuesTemplate(IN,endTimeLocal,divIndex,divTime);
    //       division=0;    
    //     }
    
    // Update neighborhood
    if (O_->numNeighborhood())
      O_->neighborhoodUpdate(y_, t_);
    
    // If the end t is passed return (print if applicable)
    if (t_ >= endTimeLocal) {
      if (printFlag_) {
	// Update the derivatives
	O_->derivs(y_, dydt_);
	print();
      }
      if (verbose)
	std::cerr << "Simulation done.\n"; 
      return;
    }
    // Update A if required
//     if (O_->updateJacobian(y_,A,col_)) {
//       A = -h1_*A;
//       for (size_t i=0; i<N; ++i) {
// 	A(i,i) = A(i,i)+1;
//       }
//       lu.lu_factorize(A);
//     }    
  }
}























ImplicitEulerCostTemplate::ImplicitEulerCostTemplate(Organism *O,std::ifstream &IN) :BaseSolver(O,IN)
{
	readParameterFile(IN);
}

ImplicitEulerCostTemplate::~ImplicitEulerCostTemplate()
{
	//std::cerr << "delete costFunction...";
	delete C_;
	//std::cerr << "Done!\n";
}

void ImplicitEulerCostTemplate::readParameterFile(std::ifstream &IN, int verbose)
{
	verbose=1;
	if (verbose) {
		std::cerr << "ImplicitEulerCostTamplate: Reading parameter file:" << std::endl;
	}
	std::string costType;
	IN >> costType;
	C_ = BaseCostFunction::createCostFunction(costType);

	IN >> startTime_;
	t_= startTime_;
	IN >> endTime_;

	IN >> printFlag_;
	IN >> numPrint_;

	IN >> h1_;
  
	if (verbose) {
		std::cerr << "Cost function: " << costType << std::endl
			      << "T_start/end: " << startTime_ << " " << endTime_ << std::endl
			      << "Print_flag: " << printFlag_ << " PrintNum: " << numPrint_ << std::endl
			      << "Initial/max step: " << h1_ << std::endl;
	}
  //TODO Should be the same as rungeKutta 5 - verify please
	//
	//simulation template parameters
	//

	size_t Mtmp;
	IN >> Mtmp;
  
	simulationIndex_.resize( Mtmp );
  
	if (Mtmp) {
		if (verbose)
			std::cerr << Mtmp << " variables read from template." << std::endl;
		for (size_t i=0 ; i<Mtmp ; ++i) {
			IN >> simulationIndex_[i];
			if (verbose)
				std::cerr << simulationIndex_[i] << " ";
		}
		IN >> inputTemplateFile_;
		if (verbose)
			std::cerr << std::endl << "Template file " << inputTemplateFile_ << std::endl; 
	}
	else
		if (verbose)
			std::cerr << "No template file used." << std::endl; 
  
	//
	//cost template parameters
	//
	IN >> Mtmp;
	std::vector<size_t> costList;

	if( Mtmp ) {
		if (verbose)
			std::cerr << Mtmp << " variables used in cost calculation\n";
		costList.resize(Mtmp);
		for (size_t i=0 ; i<Mtmp ; i++ ) {
			IN >> costList[i];
			if (verbose)
			std::cerr << costList[i] << " ";
		}
		if (verbose)
			std::cerr << "\n";
		setCostList(costList);
		std::string costTemplateFile;	
		IN >> costTemplateFile;
		setCostTemplateFile(costTemplateFile);
		if (verbose)
			std::cerr << "Cost template file " << costTemplateFile << "\n"; 
		C_->initiateCostCalculation(startTime_, endTime_);
	}
	else
		if (verbose)
			std::cerr << "No cost template file used\n";
	//IN.close();
	if (verbose) {
		std::cerr << "ImplicitEulerCost: Done reading parameter file." << std::endl << std::endl;
	}
}

void ImplicitEulerCostTemplate::simulate(void)
{ 
  size_t verbose=0;
  
  //
  // Set variables to be simulated, and if template is needed
  //
  static bool flag = true;
  if(flag)
    setSimulationFlag();
  flag = false;
  
  double tiny = 1e-20; // Caveat! Using new definition //WARNING : What is this ?
  
  //Vectors to store division data from template file
  std::vector<int> divIndex;
  std::vector<double> divTime;
  size_t division=0;
  
  //Check if template reading is needed...
  size_t simCount=0;
  size_t templateFlag=1;
  for (size_t i=0 ; i<M() ; i++ ) {
    simCount += simulationFlag_[i];
  }
  if( simCount==M() ) 
    templateFlag = 0;
  //Check if the costList is well defined
  for (size_t i=0; i<C_->sizeOfCostList(); ++i) {
    if (C_->costList(i)>=M()) {
      std::cerr << "RK5AdaptiveTemplate::simulate(): " << C_->costList(i) << " is out of scope\n";
      exit(-1);
    }
  }
  
  
  //
  //Check that h1 and endTime - startTime are > 0
  //
  double h;
  if ((h1_ > 0.0) && ((endTime_ - startTime_) > 0.0))
    h = h1_; // and h will be set to h1 for each simulation step, and then reduced if necessarym, to reach a template time point
  else {//either h or (endTime-startTime) <=0
    std::cerr << "ImplicitEulerCostTemplate::simulate() - "
	      << "Wrong time borders or time step for simulation. "
	      << "No simulation performed.\n";
    exit(-1);
  }
  
  // Initiate reactions for those where it is applicable
  //
  O_->reactionInitiate(startTime_,y_);
  
  //
  //Introduce the neighborhood
  //
  if (O_->numNeighborhood())
    O_->neighborhoodCreate(y_, t_);
  if (y_.size() && y_.size() != dydt_.size()) {
    dydt_.resize(y_.size(), y_[0]);
  }	
  assert( y_.size() == O_->numCompartment() && y_.size()==dydt_.size());
  
  std::vector< std::vector<double> > yScal( this->N() );
  //
  // Introduce simulation variables used here
  //
  
  
  // number of columns in y (number of variables)
  size_t y_Width = y_[0].size();
  // number of cells
  size_t y_Height = y_.size();
  // Total number of variables that we simulate 
  size_t N = y_Width * y_Height;
  
  SparseMatrix A(N,N);
  
  DenseVector DyNew(N);
  DenseVector f_y(N);
  
  LUSolver lu;
  
  O_->Jacobian(y_,A);
  A = -h*A;
  for (size_t i=0; i<N; ++i) {
    A(i,i) = 1+A(i,i);
  }
  lu.lu_factorize(A);
  
  DenseVector *new_Dy = &DyNew;
  
  //
  // Initiate cost Calculation if applicable
  //
  double endTimeLocal = endTime_;
  if( C_->sizeOfCostList() ) {
    //C_->initiateCostCalculation(startTime_, endTime_);
    C_->resetCostCalculation();
    if (C_->sizeOfCostTemplateTime() ) {
      endTimeLocal = C_->endTime();// +tiny;  //WARNING Why not + tiny ???
    }
  }
  
  //
  // Initiate print times
  //
  double printTime = endTime_ + tiny;
  double printDeltaTime = endTime_ + 2.0 * tiny;
  if (numPrint_ <= 0) //No printing
    printFlag_ = 0;
  else if (numPrint_ == 1) { // Print last point (default)
  }
  else if (numPrint_ == 2) { //Print first/last 
    printTime = startTime_ - tiny;
  } 
  else { //Print first/last points and spread the rest uniformly
    printTime = startTime_ - tiny;
    printDeltaTime = (endTime_ - startTime_) / ((double) (numPrint_ - 1));
  }
  
  // Go
  //////////////////////////////////////////////////////////////////////
  
  //
  // Initiate variables for simulation 
  //
  t_ = startTime_;
  numOk_ = 0; numBad_ = 0;
  
  
  // Initiate template from file if applicable
  //
  
  std::ifstream IN;
  if( templateFlag ) {
    const char *tmp = inputTemplateFile_.c_str();
    IN.open( tmp );
    if( !IN ) {
      std::cerr << "ImplicitEulerCost::simulate() "
		<< "Cannot open templatefile " << inputTemplateFile_
		<< "\n\n\7";exit(-1);}
    //Read previous and future time points
    initiateValuesTemplate(IN,divIndex,divTime);
  }
  
  //
  // Start simulation
  //
  
  for (unsigned int nstp = 0;; nstp++) {
    h = h1_;
    if (debugFlag())
      yCopy_[debugCount()] = y_;
    
    // Updates the derivatives
    O_->derivsTemplate(y_,dydt_,simulationFlag_);
    
    // Calculate 'scaling' for error measure
    //for (size_t i = 0; i < this->N(); i++)
    //	for (size_t j = 0; j < M(); j++) {
    //		yScal[i][j] = fabs(y_[i][j]) + fabs(dydt_[i][j] * h) + tiny;
    // Try this modification to see whether it fasten up things
    // if( yScal[i][j]<1.0 )
    // yScal[i][j] = 1.0;
    //	}
    
    // Print if applicable 
    if (printFlag_ && t_ >= printTime) {
      printTime += printDeltaTime;
      print();
    }

    // Check if step is larger than max allowed
    // max step end is min of endTime_ and printTime
    double tMin = endTimeLocal < printTime ? endTimeLocal : printTime;
    
    int maxStepFlag = 0;//1 if stepsize shortened
    
    if( templateFlag && futureTime()<tMin ) tMin = futureTime(); 
    if( templateFlag && division<divIndex.size() && divTime[division]<tMin ) 
      tMin=divTime[division];
    if( C_->sizeOfCostList() && C_->costTemplateIndex()<C_->sizeOfCostTemplateTime() 
	&& C_->costTemplateTime( C_->costTemplateIndex() )<tMin )
      tMin=C_->costTemplateTime( C_->costTemplateIndex() );
    if( t_+h>tMin ){
      h=tMin-t_;
      maxStepFlag = 1;
    }		
    
    // putting the dydt matrix into the f_y vector
    for(size_t i=0; i < y_Height; i++){
      for(size_t j=0; j < y_Width;j++){
	f_y(y_Width*i + j) = dydt_[i][j];
      }
    }
    
    // Update by solving yNew (1-hA) = yOld
    lu.lu_substitute(A,*new_Dy,f_y);
    
    // copy the vector into y
    for(size_t i =0; i<y_Height; ++i){
      for(size_t j = 0; j < y_Width; ++j){
	y_[i][j] += h1_*(*new_Dy)(y_Width*i + j);
      }
    }

    t_ += h;		
    if (h1_ == h) ++numOk_; else ++numBad_; // ??
    
    //Update other cols of y from template
    if( templateFlag )
      allValuesTemplate();//time is already updated
    
    // Make updates of rections that requires it
    O_->reactionUpdate(h1_, t_, y_);
    
    // Check for cell divisions, compartmental removal etc
    //O_->compartmentChange(y_, dydt_, t_);
    if( templateFlag ) {
      int tmpDivCount=0;
      while( division<divIndex.size() && t_>=divTime[division] ) {
	divideTemplate(divIndex[division]);
	division++;
	tmpDivCount++;
      }
    }		
    
    
    //
    // Add cost from template comparison if applicable
    //
    if( C_->sizeOfCostList() && C_->costTemplateIndex()< C_->sizeOfCostTemplateTime()
	&& t_>= C_->costTemplateTime(C_->costTemplateIndex() ) ) {
      C_->addCost(y_,t_); 
    }	
    
    // Check if new time point data needs to be read from file
    //////////////////////////////////////////////////////////////////////
    if( templateFlag && t_>=futureTime_ && t_<endTimeLocal ) {
      updateValuesTemplate(IN,endTimeLocal,divIndex,divTime);
      division=0;    
    }
    
    // Update neighborhood
    if (O_->numNeighborhood())
      O_->neighborhoodUpdate(y_, t_);
    
    // If the end t is passed return (print if applicable)
    if (t_ >= endTimeLocal) {
      if (printFlag_) {
	// Update the derivatives
	O_->derivs(y_, dydt_);
	print();
      }
      if (verbose)
	std::cerr << "Simulation done.\n"; 
      return;
    }
    
    // Update A if required
    // 
    O_->Jacobian(y_,A) ;  //WARNING updateJacobian is not defined
    A = -h*A;		// WARNING !!! h ou h1 ?
    for (size_t i=0; i<N; ++i) {
      A(i,i) = A(i,i)+1;
    }
    lu.lu_factorize(A);		    
  }
}
