#ifndef SPHERICALBACTERIA_CELLDIVISION_H
#define SPHERICALBACTERIA_CELLDIVISION_H

#include <cmath>
#ifndef M_PI
#define M_PI 3.14159265358979323846
#endif

#include "../../compartment/baseCompartmentChange.h"
#include "../../common/vectorTemplate.h"
#include "../../common/myRandom.h"

namespace sphericalBacteria {
	template <int T>
	class CellDivision : public BaseCompartmentChange {
		
	public:
		CellDivision(std::vector<double> &paraValue, 
			     std::vector< std::vector<size_t> > 
			     &indValue );
		
		int flag(Compartment &compartment,
			 std::vector< std::vector<double> > &y,
			 std::vector< std::vector<double> > &dydt );
		void update(Organism &O,Compartment &compartment,
			    std::vector< std::vector<double> > &y,
			    std::vector< std::vector<double> > &dydt, double t=0.0 );  
		Vector<T> getRandomDirection(); 
	};

	template<int T>
	Vector<T> CellDivision<T>::getRandomDirection()
	{
		Vector<T> direction;
		if( T==3 ) {
			double phi = myRandom::Rnd()*2.*M_PI;
			double theta = myRandom::Rnd()*M_PI;
			double sinTheta = sin(theta);
			direction[0] = cos(phi)*sinTheta;
			direction[1] = sin(phi)*sinTheta;
			direction[2] = cos(theta);
		}
		else if( T==2 ) {
			double phi = myRandom::Rnd()*2*M_PI;
			direction[0] = cos(phi);
			direction[1] = sin(phi);
		}
		else if( T==1 ) {
			if( myRandom::Rnd()>0.5 )
				direction[0]=1;
			else
				direction[0]=-1;
		}
		else {
			std::cerr << "CellDivision::update\n"
				  << "Wrong dimension for dividing a sphere.\n";
			std::cerr << T << "\n";
			exit(-1);
		}
		return direction;
	}
	
	template <int T>
	CellDivision<T>::CellDivision(std::vector<double> &paraValue, 
				      std::vector< std::vector<size_t> > 
				      &indValue )
	{	
		//Do some checks on the parameters and variable indeces
		//////////////////////////////////////////////////////////////////////
		if( paraValue.size()!=3 ) {
			std::cerr << "CellDivison::"
				  << "CellDivision() "
				  << "Uses three parameters R_th V_diff and overlap\n";
			exit(0);
		}
		if( indValue.size() ) {
			std::cerr << "CellDivision::CellDivision() "
				  << "No variable index is used.\n";
			exit(0);
		}
		//Set the variable values
		//////////////////////////////////////////////////////////////////////
		setId("CellDivision");
		setParameter(paraValue);  
		setVariableIndex(indValue);
		
		//Set the parameter identities
		//////////////////////////////////////////////////////////////////////
		std::vector<std::string> tmp( numParameter() );
		tmp.resize( numParameter() );
		tmp[0] = "R_th";
		tmp[1] = "V_diff";
		tmp[2] = "overlap";
				
		setParameterId( tmp );
		
		//Set the number of compartment change (+1 for division)
		//////////////////////////////////////////////////////////////////////
		setNumChange(1);
	}
	
	//! Flags for division if the radius is larger than a threshold value
	template<int T> int CellDivision<T>::
	flag(Compartment &compartment,
	     std::vector< std::vector<double> > &y,
	     std::vector< std::vector<double> > &dydt ) {
		
		if( y[compartment.index()][compartment.numTopologyVariable()-1] > 
		    parameter(0) ) 
			return 1;
		else 
			return 0;
	}
		
		
	template<int T> void CellDivision<T>::
	update(Organism &O, Compartment &compartment,std::vector< std::vector<double> > &y,
	       std::vector< std::vector<double> > &dydt, double t )
	{
		size_t i=compartment.index();
		size_t RCol = compartment.numTopologyVariable()-1;
		double R = y[i][RCol];
		O.addDivision(i,t);
		//Divide
		//////////////////////////////////////////////////////////////////////
		size_t NN=O.numCompartment();
		size_t N=NN+1;
		size_t M=compartment.numVariable();
		O.addCompartment( compartment );
		O.compartment(NN).setIndex(NN);
		
		y.resize( N );
		dydt.resize( N );
		y[NN].resize( M );
		dydt[NN].resize( M );
		for( size_t j=0 ; j<M ; j++ ) {
			y[NN][j] = y[i][j];
			dydt[NN][j] = dydt[i][j];
		} 
		
		double V = std::pow(R,T);
		double V1 = 0.5*V*( 1.0 + parameter(1)*
				    (2.0*myRandom::Rnd()-1.0) );
		double V2 = V-V1;
		double R1=std::pow(V1, 1/static_cast<double>(T));
		double R2=std::pow(V2, 1/static_cast<double>(T));
		//Minimize forces
		y[i][RCol] = R1;
		y[NN][RCol] = R2;
		assert( y[i][RCol]>0.0 && y[NN][RCol]>0.0 );
		
		//Add center point to first/second point for the new cells
		Vector<T> direction = getRandomDirection()*parameter(2)*R;
		//direction*=parameter(2)*R;
		for( size_t d=0; d < T; ++d ) {
			y[i][d] += 0.5*direction[d];
			y[NN][d] -= 0.5*direction[d];
		}
		//Add link relationship between mother/daughter
		//assume this is done directly after divisions!!!
	}
	
}

#endif //SPHERICALBACTERIA_CELLDIVISION_H
