#include "substrateDependentGrowth2D.h"

const double PI=3.141592653589793238512808959406186204433;
sphericalBacteria::SubstrateDependentGrowth2D::
SubstrateDependentGrowth2D(std::vector<double> &paraValue, 
			    std::vector< std::vector<size_t> > &indValue )
{
	
	//Do some checks on the parameters and variable indeces
	//////////////////////////////////////////////////////////////////////
	if ( paraValue.size()!=2 ) {
		std::cerr << "sphericalBacteria::SubstrateDependentGrowth::SubstrateDependentGrowth() "
			  << "Uses two parameters k_max and K\n";
		exit(0);
	}
	if ( indValue.size() !=1 || indValue[0].size() !=1 ) {
		std::cerr << "sphericalBacteria::SubstrateDependentGrowthRestriced::SubstrateDependentGrowth() "
			  << "Needs one index, ligand concentration.\n";
		exit(0);
	}
	//Set the variable values
	//////////////////////////////////////////////////////////////////////
	setId("exponentialGrowth");
	setParameter(paraValue);  
	setVariableIndex(indValue);
  
	//Set the parameter identities
	//////////////////////////////////////////////////////////////////////
	std::vector<std::string> tmp( numParameter() );
	tmp.resize( numParameter() );
	tmp[0] = "k_growth";
	tmp[1] = "K";
	setParameterId( tmp );
}


void sphericalBacteria::SubstrateDependentGrowth2D::
derivs(Compartment &compartment,size_t varIndex,
       std::vector< std::vector<double> > &y,
       std::vector< std::vector<double> > &dydt )
{
	size_t i=compartment.index();
	size_t dCol = compartment.numTopologyVariable()-1+varIndex;
	double S = y[i][variableIndex(0,0)];
	dydt[i][dCol] += 0.5*parameter(0)*y[i][dCol]*S/( parameter(1) + S );
}
