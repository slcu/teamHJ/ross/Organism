#include "gradientSmooth2D.h"

sphericalBacteria::GradientSmooth2D::GradientSmooth2D(std::vector<double> &paraValue, 
			       std::vector< std::vector<size_t> > 
			       &indValue ) 
{
	//Do some checks on the parameters and variable indeces
	//////////////////////////////////////////////////////////////////////
	if( paraValue.size()!=4 ) {
		std::cerr << "GradientSmooth2D::GradientSmooth2D() "
			  << "Uses four parameters k_x, k_y, N_x, N_y \n";
		exit(0);
	}
	if ( indValue.size() !=1 && 
	     indValue[0].size() != 1) { 
		std::cerr << "GradientSmooth2D::GradientSmooth2D() "
			  << "uses one index\n"; 
		exit(0);
	}
	//Set the variable values
	//////////////////////////////////////////////////////////////////////
	setId("gradientSmooth2DCigar");
	setParameter(paraValue);  
	setVariableIndex(indValue);
	
	//Set the parameter identities
	//////////////////////////////////////////////////////////////////////
	std::vector<std::string> tmp( numParameter() );
	tmp[0] = "k_x";
	tmp[1] = "k_y";
	tmp[2] = "N_x";
	tmp[3] = "N_y";
	setParameterId( tmp );
	
}
void sphericalBacteria::GradientSmooth2D::derivs(Compartment &compartment,size_t varIndex,
			    std::vector< std::vector<double> > &y,
			    std::vector< std::vector<double> > &dydt ) 
{
	size_t i=compartment.index();
	double X = y[i][0];
	double Y = y[i][1];
	double grad = 
		parameter(0)*(X-parameter(2)) +
		parameter(1)*(Y-parameter(3));
	y[i][variableIndex(0,0)] = grad > 0.0 ? grad:0.0;
}
