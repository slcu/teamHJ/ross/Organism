#include "NeighborhoodLatticeWalls2D.h"
#include "../../common/vectorTemplate.h"
#include <cmath>
//#include <set>

const int DIMENSION=2;
Sphere::NeighborhoodLatticeWalls2D::
NeighborhoodLatticeWalls2D(std::vector<double> &paraValue, 
			   std::vector< std::vector<size_t> > 
			   &indValue ) 
{
	// Do some checks on the parameters and variable indeces
	if( paraValue.size() < 8 ) {
		std::cerr << "NeighborhoodLatticeWalls2D::"
			  << "NeighborhoodLatticeWalls2D() "
			  << "Must use at least one wall!\n"
			  << "five parameters numCell, cellSize, updateFlag and "
			  << "dt_min followed by wall coordinates.\n";
		exit(0);
	}
	if( indValue.size() ) {
		std::cerr << "NeighborhoodLatticeWalls2D::"
			  << "NeighborhoodLatticeWalls2D() "
			  << "No variable index is used.\n";
		exit(0);
	}

	// Check for consistency in parameter values
	if( paraValue[2]!=0.0 && paraValue[2]!=1.0 ) {
		std::cerr << "NeighborhoodLatticeWalls2D::"
			  << "NeighborhoodLatticeWalls2D() "
			  << "parameter(1) (updateFlag) must be 0 or 1.\n";
		exit(0);
	}

	// Set the variable values
	setId("neighborhoodLatticeWalls2D");
	setParameter(paraValue);  
	setVariableIndex(indValue);

	//Set the parameter identities
	std::vector<std::string> tmp( numParameter() );
	tmp.resize( numParameter() );
	tmp[0] = "numCell";
	tmp[1] = "cellSize";
	tmp[2] = "updateFlag";
	tmp[3] = "dt_min";
	setParameterId( tmp );
	//set member variables
	numWall_ = static_cast<unsigned int>( (numParameter() - 5)/4 );
	numBox_ = static_cast<unsigned int>(parameter(0));
	totalNumberOfBoxes_ = static_cast<unsigned int>(pow(numBox_, 2));
	hasSphereCell_.resize(totalNumberOfBoxes_);
	wallNeighborLattice_.resize(numWall_);
	for(int d = 0; d<DIMENSION; d++)     
		boxCount_[d] =  int(std::pow(static_cast<double>(numBox_),d));
}

unsigned int Sphere::NeighborhoodLatticeWalls2D::
create(Organism &O, std::vector< std::vector<double> > &y, double t ) 
{   
  if( O.numTopologyVariable() != 3) {
    std::cerr << "NeighborhoodLatticeWalls2D::create() "
	      << "Wrong number of topology variables.\n"
	      << " (" << O.numTopologyVariable() << ")\n";
    exit(-1);
  }
  size_t N = O.numCompartment();
  std::vector< std::vector<size_t> > neighbor(N), neighborWall(N);
  double distanceThreshold = 0.5*(std::sqrt(2)+1.0)*parameter(1) + parameter(4);
  Vector<DIMENSION> boxCOM;
  //This is done only in create(), not in update() 
  
  for(unsigned int ly = 0; ly < numBox_; ++ly) {
    for(unsigned int lx = 0; lx < numBox_; ++lx) {
      for(size_t w = 0; w < numWall_; ++w) {
	//Wall properties
	Vector<DIMENSION> n, x1, x2;
	x1[0] = parameter(5+w*4);
	x1[1] = parameter(6+w*4);
	x2[0] = parameter(7+w*4);
	x2[1] = parameter(8+w*4);
	n = x2 - x1;
	double length = std::sqrt(dot(n,n));
	n *= 1/length;
	//lattice cell properties
	size_t boxIndex = lx + ly*numBox_;
	
	boxCOM[0] = parameter(1)*( lx % numBox_ + 0.5);
	boxCOM[1] = parameter(1)*( ly % numBox_ + 0.5);
	//compute closest distance between cell and wall if it's close enough add as neighbor
	double dist = (x1[1] - boxCOM[1])*n[0] - 
	  (x1[0] - boxCOM[0])*n[1];
	double s = std::fabs(n[0])>std::fabs(n[1]) ? 
	  (boxCOM[0]-x1[0]-n[1]*dist)/n[0] : 
	  (boxCOM[1]-x1[1]+n[0]*dist)/n[1] ;
	if( s<0.0 || s>length ) {
	  double dist1= std::sqrt( dot(x1-boxCOM, x1-boxCOM ));
	  double dist2= std::sqrt( dot(x2-boxCOM, x2-boxCOM ));
	  dist = dist1<dist2 ? dist1 : dist2;
	}
	if( std::fabs(dist) < distanceThreshold ) {
	  wallNeighborLattice_[w].push_back(boxIndex);		
	}
      }
    }
  }
  for (size_t i =0; i<wallNeighborLattice_.size(); ++i) {
    std::cerr << "Wall " << i  << " is in boxes:\n\t";
    for (size_t j =0; j<wallNeighborLattice_[i].size(); ++j)
      std::cerr << wallNeighborLattice_[i][j] << " ";
    std::cerr << "\n";
  }
  unsigned numNeigh=setNeighbors(y, neighbor, neighborWall);
  
  //Copy the new neighbours into the cells (Caveat: Not a good solution)
  for(size_t i=0 ; i<N ; i++ ) {
    O.compartment(i).setNeighbor( neighbor[i] );
    O.compartment(i).setNeighborAtLevel( 1, neighborWall[i] );
  }
  
  //Set the previos time variable to current t value
  setPreviousTime(t);
  setNumNeighbor(numNeigh);
  
  return numNeigh;
}

unsigned int Sphere::NeighborhoodLatticeWalls2D::
update(Organism &O, std::vector< std::vector<double> > &y,
       double t ) 
{
  if( parameter(2) == 1.0 && t-previousTime()>parameter(3) ) {
    
    size_t N = O.numCompartment();
    std::vector< std::vector<size_t> > neighbor(N), neighborWall(N);
    for (size_t i=0; i< totalNumberOfBoxes_; ++i)
      hasSphereCell_[i].clear();
    
    unsigned int numNeigh=setNeighbors(y, neighbor, neighborWall);
    
    //Copy the new neighbours into the cells (Caveat: Not a good solution)
    for(size_t i=0 ; i<N ; i++ ) {
      O.compartment(i).setNeighbor( neighbor[i] );
      O.compartment(i).setNeighborAtLevel( 1, neighborWall[i] );
    }
    
    //Set the previous time variable to current t value
    setPreviousTime(t);
    setNumNeighbor(numNeigh);
    return numNeigh;
  }
  
  
  return numNeighbor();
}

unsigned int Sphere::
NeighborhoodLatticeWalls2D::
setNeighbors(const std::vector< std::vector<double> > &y,
	     std::vector< std::vector<size_t> > &neighbor,
	     std::vector< std::vector<size_t> > &neighborWall)
{
  unsigned int numNeigh=0;
  size_t N = neighbor.size();
  
  std::vector< size_t > isInBox(N);//given a compartment index i, gives lattice cell index
  checkCellPosition(N, y, isInBox);
  
  for(size_t i = 0; i<N; ++i){
    hasSphereCell_[ isInBox[i] ].push_back(i);
  }
  //for each cell compartment, add the cells in neighboring lattice cells as neighbors
  
  for (size_t i=0; i < N; ++i ){
    bool flag = true;
    for(int yStep = -1; yStep <= 1; ++yStep) {
      for(int xStep = -1; xStep <= 1; ++xStep){
	int neighborBox = isInBox[i]+ xStep + yStep*numBox_;
	if (neighborBox >= 0 && static_cast<unsigned int>(neighborBox) < totalNumberOfBoxes_) {
	  size_t numCellInBox = hasSphereCell_[neighborBox].size();
	  for(size_t n = 0; n < numCellInBox; ++n){
	    
	    flag = true;
	    size_t potentialNeighbor = hasSphereCell_[neighborBox][n];
	    for(size_t l = 0; l<neighbor[i].size(); l++) 
	      if(potentialNeighbor == neighbor[i][l])//check if neighbor is already added
		flag = false;
	    if (potentialNeighbor > i && flag) {
	      //std::cerr << i << " and " << potentialNeighbor << "\n"; 
	      neighbor[i].push_back(potentialNeighbor);
	      neighbor[potentialNeighbor].push_back(i);
	      numNeigh++;
	    }
	  }
	}
      }
    }
  }
  for(unsigned int w = 0; w < numWall_; ++w) {
    for(size_t m = 0; m < wallNeighborLattice_[w].size(); ++m) {
      for(size_t n = 0 ; n < hasSphereCell_[ wallNeighborLattice_[w][m] ].size() ; ++n) {
	//std::cerr << "wall: " << w << " box: " << wallNeighborLattice_[w][m]
	//					<< " cell: " << hasSphereCell_[ wallNeighborLattice_[w][m] ][n] << "\n";
	neighborWall[hasSphereCell_[ wallNeighborLattice_[w][m] ][n] ].push_back(w); 
      }
    }
  }
  return numNeigh;
}

void  Sphere::
NeighborhoodLatticeWalls2D::
checkCellPosition(size_t N, const std::vector< std::vector<double> > &y,
		  std::vector<size_t> &isInBox) 
{
  
  //set up lattice, and place each sphere-cell point in a lattice box
  double boxSizeInverse = 1/parameter(1);
  for(int d = 0; d<DIMENSION; ++d){     
    for(size_t i = 0; i<N;i++){
      if( (y[i][d] < parameter(1) || y[i][d] > parameter(1)*(numBox_-1)) ){
	std::cerr << "NeighborhoodLatticeWalls2D::setNeighbors(): "
		  << "compartment " << i << " is outside lattice." << std::endl;
	std::cerr << "pos: " << y[i][0] << " " << y[i][1] << std::endl;
	std::cerr << "lattice: " << parameter(1) << " "
		  <<  parameter(1)*(numBox_-1) << std::endl;
	exit(EXIT_FAILURE);
      }
      isInBox[i] +=  boxCount_[d]*static_cast<size_t>( y[i][d]*boxSizeInverse );
    }
  }
}
