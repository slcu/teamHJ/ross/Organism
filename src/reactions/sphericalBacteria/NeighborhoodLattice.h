#ifndef SPHERICALBACTERIA_NEIHBORHOODLATTICE_H
#define SPHERICALBACTERIA_NEIHBORHOODLATTICE_H
#include "../../organism.h"
#include "../../compartment/baseCompartmentNeighborhood.h"
#include "../../common/vectorTemplate.h"
#include <cmath>
#include <set>
///
/// @brief Defines neighborhood by placing each cigar-shaped cell in a lattice
/// box
///
namespace sphericalBacteria {
	template <int T>
	class NeighborhoodLattice: public BaseCompartmentNeighborhood {
		
	public:
  	
		///
		/// @brief Main constructor
		///
		/// This is the main constructor which checks and sets the parameters and
		/// variable indices that defines the neighborhood rule.
		///
		/// @param paraValue Vector with parameters used.
		///
		/// @param indValue Vector of vectors with variable indices used in the
		/// neighborhood calculations.
		///
		/// @see createCompartmentNeighborhood(std::vector<double> &, std::vector<
		/// std::vector<size_t> > &, const std::string &)
		///
		NeighborhoodLattice(std::vector<double> &paraValue, 
															 std::vector< std::vector<size_t> > 
															 &indValue );
		
		///
		/// @brief Creates the (potential) neighborhood before simulation
		///
		/// @see BaseCompartmentNeighborhood::create()
		///
		unsigned int create(Organism &O, std::vector< std::vector<double> > &y,
												double t=0.0 );
		
		///
		/// @brief Updates the (potential) neighborhood during a simulation
		///
		/// @see BaseCompartmentNeighborhood::update()
		///
		unsigned int update(Organism &O, std::vector< std::vector<double> > &y,
												double t=0.0 );
		
		unsigned int setNeighbors(int numBox, double boxSize,
															const std::vector< std::vector<double> > &y,
															std::vector< std::vector<size_t> > &neighbors);
																
	};
	
	template <int T> NeighborhoodLattice<T>::
		NeighborhoodLattice(std::vector<double> &paraValue, 
												std::vector< std::vector<size_t> > 
												&indValue ) 
		{
			// Do some checks on the parameters and variable indeces
			if( paraValue.size() !=3 ) {
				std::cerr << "NeighborhoodLattice::"
									<< "NeighborhoodLattice() "
									<< "Must use at least one wall!\n"
									<< "three parameters numCell, cellSize, updateFlag\n";
					exit(0);
			}
			if( indValue.size() ) {
				std::cerr << "NeighborhoodLattice::"
									<< "NeighborhoodLattice() "
									<< "No variable index is used.\n";
				exit(0);
			}
			
			// Check for consistency in parameter values
			if( paraValue[2]!=0.0 && paraValue[2]!=1.0 ) {
				std::cerr << "NeighborhoodLattice::"
									<< "NeighborhoodLattice() "
									<< "parameter(1) (updateFlag) must be 0 or 1.\n";
				exit(0);
			}
			
			// Set the variable values
			setId("neighborhoodLattice");
			setParameter(paraValue);  
			setVariableIndex(indValue);
			
			//Set the parameter identities
			std::vector<std::string> tmp( numParameter() );
			tmp.resize( numParameter() );
			tmp[0] = "numCell";  
			tmp[1] = "cellSize";
			tmp[2] = "updateFlag";
			setParameterId( tmp );
		}
	
	template<int T> unsigned int NeighborhoodLattice<T>::
		create(Organism &O, std::vector< std::vector<double> > &y, double t ) 
		{   
			if( O.numTopologyVariable() != 3) {
				std::cerr << "NeighborhoodLattice::create() "
									<< "Wrong number of topology variables.\n"
									<< " (" << O.numTopologyVariable() << ")\n";
				exit(-1);
			}
			size_t N = O.numCompartment();
			std::vector< std::vector<size_t> > neighbor(N);
			int numBox = static_cast<int>(parameter(0));
			double boxSize = parameter(1);
			unsigned numNeigh=setNeighbors(numBox, boxSize, y, neighbor);
			
			//Copy the new neighbours into the cells (Caveat: Not a good solution)
			for(size_t i=0 ; i<N ; i++ ) {
				O.compartment(i).setNeighbor( neighbor[i] );
			}
			
			//Set the previos time variable to current t value
			setPreviousTime(t);
			setNumNeighbor(numNeigh);
			
			return numNeigh;
		}
	
	template <int T> unsigned int NeighborhoodLattice<T>::setNeighbors(int numBox,  double boxSize,
																																		 const std::vector< std::vector<double> > &y,
																																		 std::vector< std::vector<size_t> > &neighbor)
		{
			unsigned int numNeigh=0;
			size_t N = neighbor.size();
			double boxSizeInverse = 1/boxSize;
			
			unsigned int totalNumberOfBoxes = static_cast<unsigned int>(std::pow(double(numBox), T));
	
			std::vector< size_t > isInBox( N );//given a compartment index i, gives lattice cell index
			std::vector< std::vector< size_t > > hasBactCell(totalNumberOfBoxes);
			Vector<T> yCOM;
      //set up lattice, and place each bact-cell point in a lattice box
			for(int d = 0; d<T; d++){     
				int powNum =  int(pow(numBox,d));
				for(size_t i = 0; i<N;i++){
					yCOM[d] = y[i][ d ];
					if( (yCOM[d] < boxSize || yCOM[d] > boxSize*(numBox-1)) ){
						std::cerr << "NeighborhoodLatticeWalls2D::create(): "
											<< "compartment is outside lattice\n";
						for (int t=0; t<T; ++t)
							std::cerr << yCOM[t] << " ";
						std::cerr << "\n";
				exit(-1);
					}
					isInBox[i] +=  powNum*static_cast<size_t>( yCOM[d]*boxSizeInverse );
				}
			}
			
			for(size_t i = 0; i<N;i++){
				hasBactCell[ isInBox[i] ].push_back(i);
			}
			//for each cell compartment, add the cells in neighboring lattice cells as neighbors
			for( size_t i=0 ; i<N ; i++ ){
				std::set<size_t> bactCellIndices;
				std::set<size_t>::iterator bactCellIndex;
				bool flag = 1;
				int k[T];
				if (T==2) {
					for(k[1] = -numBox; k[1] <= numBox; k[1]+=numBox) {
						for(k[0] = -1; k[0] <= 1; k[0]++){
							bactCellIndices.insert(isInBox[i]+ k[0] + k[1]);
						}
					}
				}
				else if(T == 3){
					for(k[2] = - numBox*numBox; k[2] <= numBox*numBox; k[2] += numBox*numBox) {
						for(k[1] = -numBox; k[1] <= numBox; k[1]+=numBox) {
							for(k[0] = -1; k[0] <= 1; k[0]++){
								bactCellIndices.insert(isInBox[i]+ k[0] + k[1] + k[2]);
							}
						}
					}
				}
				for(bactCellIndex = bactCellIndices.begin(); bactCellIndex != bactCellIndices.end(); bactCellIndex++) {
					if( (*bactCellIndex >= 0 && *bactCellIndex <= totalNumberOfBoxes) 
							&& hasBactCell[*bactCellIndex].size()) { 
						for(size_t n = 0; n < hasBactCell[*bactCellIndex].size(); n++){
							flag = 1;
							size_t potentialNeighbor = hasBactCell[*bactCellIndex][n];
							if(neighbor[i].size()){
								for(size_t l = 0; l<neighbor[i].size(); l++) {
									if(potentialNeighbor == neighbor[i][l])//check if neighbor is already added
										flag = 0;
								}
							}
							if(potentialNeighbor > i && flag) {
								neighbor[i].push_back(potentialNeighbor);
								neighbor[potentialNeighbor].push_back(i);
								numNeigh++;
							}
						}
					}
				}
			}
			return numNeigh;
		}
	template <int T> unsigned int NeighborhoodLattice<T>::
		update(Organism &O, std::vector< std::vector<double> > &y,
					 double t ) 
		{  
			if( parameter(2) == 1.0 && t-previousTime()>parameter(3) )
				return create(O,y,t);
			
			return numNeighbor();
		}
	
}
#endif //SPHERICALBACTERIA_NEIHBORHOODLATTICE_H
