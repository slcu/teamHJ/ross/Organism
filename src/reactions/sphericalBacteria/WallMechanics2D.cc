#include "WallMechanics2D.h"
#include "../../common/vectorTemplate.h"
#include <cmath>

Sphere::WallMechanics2D::WallMechanics2D(std::vector<double> &paraValue, 
						    std::vector< std::vector<size_t> > 
						    &indValue )
{
  // Do some checks on the parameters and variable indeces
  //
  if( paraValue.size()<5 || (paraValue.size()-1)%4 ) {
    std::cerr << "Sphere::WallMechanics2D::WallMechanics2D()"
	      << "Uses at least six parameters K_force and x_1, y_1, x_2, "
	      << "y_2 [x_1 y_1 x_2 y_2,...]." << std::endl;
    exit(EXIT_FAILURE);
  }	
  if( indValue.size() != 0 && (indValue.size() != 1 && (indValue[0].size() != 1 || indValue[0].size() != 2) ) ) {
    std::cerr << "Sphere::WallMechanics2D::WallMechanics2D()"
	      << "uses up to two indices to store force and overlap, not "
	      << indValue.size() << std::endl;
    exit(EXIT_FAILURE);
  }
  // Set the variable values
  //
  setId("Sphere::WallMechanics2D");
  setParameter(paraValue);  
  setVariableIndex(indValue);
  
  // Set the parameter identities
  //
  std::vector<std::string> tmp( numParameter() );
  tmp[0] = "K_force";
  size_t numWall = (paraValue.size()-2)/4;
  for( size_t i=0 ; i<numWall ; ++i ) {
    size_t add = i*4;
    tmp[1+add] = "x_1";
    tmp[2+add] = "y_1";
    tmp[3+add] = "x_2";
    tmp[4+add] = "y_2";
  }
  setParameterId( tmp );
}

void Sphere::WallMechanics2D::
derivs(Compartment &compartment,size_t varIndex,
       std::vector< std::vector<double> > &y,
       std::vector< std::vector<double> > &dydt ) {
  size_t numNeigh = compartment.numNeighborAtLevel(1);
  //std::cerr << compartment.index() << " " << numNeigh << "\t" << compartment.numNeighbor() << std::endl;
  if( !numNeigh ) return;
  size_t i=compartment.index(); 
  const size_t  dimension=2;
  
  Vector<2> cellPos, wallPos1, wallPos2, u, v;
  for( size_t dim=0 ; dim<dimension ; ++dim )
    cellPos[dim] = y[i][varIndex+dim];
  double R =y[i][compartment.numTopologyVariable() - 1 + varIndex];
  //Loop over neighbors
  for( size_t k=0 ; k<numNeigh ; ++k ) {
    //Wallproperties
    size_t add = 4*compartment.neighborAtLevel(1,k);
    
    wallPos1[0]=parameter(1+add);
    wallPos1[1]=parameter(2+add); 
    wallPos2[0]=parameter(3+add); 
    wallPos2[1]=parameter(4+add);
    v = wallPos2-wallPos1;
    double wallLength = std::sqrt(dot(v,v));
    v *= 1/wallLength;
    u[0] = -v[1];
    u[1] = v[0];
    
    double s = dot(wallPos1-cellPos, u);
    double dist = std::fabs(s);
    if (dist == 0) {
      std::cerr << "Sphere::WallMechanics2D::derivs() complete overlap with wall" << std::endl;
      exit(EXIT_FAILURE);
    }
    double t = dot(cellPos-wallPos1, v);
    if ( double overlap = R-dist > 0  && t >= 0 && t <= wallLength) {
      
      double fac = parameter(0)*overlap*std::sqrt(overlap)/dist; 
      if (s > 0)
	fac *= -1.0;
      double RInv = 1/R;
      std:: cerr << "999 " << i << " " << k << " "; 
      for( size_t dim=0 ; dim<dimension ; ++dim ) {
	std::cerr << y[i][varIndex+dim] << " " << dydt[i][varIndex+dim] << " " << fac*RInv*u[dim] << " ";
	dydt[i][varIndex + dim] += fac*RInv*u[dim]; 
	std::cerr << dydt[i][varIndex+dim] << " ";
      }
      std::cerr << std::endl;
      if (numVariableIndex(0)) {
	double forceSquare = fac*fac*dot(u,u);
	dydt[i][variableIndex(0,0)] += forceSquare;
	if (numVariableIndex(0)>1) {
	  dydt[i][variableIndex(0,1)] += overlap;
	}
      }
    }
  }
}

void Sphere::WallMechanics2D::derivsWithAbs(Compartment &compartment,size_t varIndex,
					    DataMatrix &y,DataMatrix &dydt,DataMatrix &sdydt) {
  derivs(compartment,varIndex,y,dydt);
}
