#include "../../reactions/baseReaction.h"

#ifndef SPHERE_WALLMECHANICS2D_H
#define SPHERE_WALLMECHANICS2D_H

namespace Sphere {
  ///
  /// @brief Mechanical interaction between spherical cells and walls defined within the reaction
  ///
  /// A number of walls can be defined as lines in 2D space, and cells (spherical shape) interact
  /// mechanically with the walls. Can be used to simulate cells in microfluidic chambers.
  ///
  /// A spring force is generated between the cell and the wall and the resting length is the radius
  /// The distance between the cell and the walls are given by the perpendicular distance from the
  /// wall and the center of the sphere.
  ///
  /// In a model file, the reaction is defined as:
  ///
  /// @verbatim
  /// Sphere::WallMechanics2D 1+k*4 1 0/1/2
  /// K_spring
  /// x_1 y_1 x_2 y_2
  /// [x_1 y_1 x_2 y_2]
  /// Force_i Overlap_i
  /// @endverbatim
  ///
  /// where Kspring is the spring constant and the walls are defined by x,y of two end points.
  /// Multiple walls (at least one) can be defined. One layer of variable indices with up to
  /// two values can be defined to store Force and overlap.  
  class WallMechanics2D: public BaseReaction {
    
  public:
		
    ///
    /// @brief Main constructor
    ///
    /// This is the main constructor which sets the parameters and variable
    /// indices that defines the reaction.
    ///
    /// @param paraValue vector with parameters
    ///
    /// @param indValue vector of vectors with variable indices
    ///
    /// @see BaseReaction::createReaction(std::vector<double> &paraValue,...)
    ///
    WallMechanics2D(std::vector<double> &paraValue, 
		    std::vector< std::vector<size_t> > &indValue );
    
    ///
    /// @brief Derivative function for this reaction class
    ///
    /// @see BaseReaction::derivs(Compartment &compartment,size_t species,...)
    ///
    void derivs(Compartment &compartment,size_t varIndex,
		std::vector< std::vector<double> > &y,
		std::vector< std::vector<double> > &dydt );	
    ///
    /// @brief Derivative function for this reaction class calculating the absolute value for noise solvers
    ///
    /// In this reaction it just calls the derivs function allowing it to be used together with stochastic reactions
    ///
    /// @see BaseReaction::derivsWithAbs(Compartment &compartment,size_t species,...)
    ///
    void derivsWithAbs(Compartment &compartment,size_t species,DataMatrix &y,DataMatrix &dydt,DataMatrix &sdydt);    
  };
}
#endif
