#ifndef SPHERICALBACTERIA_CELLMECHANICS_H
#define SPHERICALBACTERIA_CELLMECHANICS_H



namespace sphericalBacteria {
	template<int T>
		class CellMechanics : public BaseReaction {
		
		public:
		
		//
		/// @brief Main constructor
		///
		/// This is the main constructor which sets the parameters and variable
		/// indices that defines the reaction.
		///
		/// @param paraValue vector with parameters
		///
		/// @param indValue vector of vectors with variable indices
		///
		/// @see BaseReaction::createReaction(std::vector<double> &paraValue,...)
		///
		CellMechanics(std::vector<double> &paraValue, 
									std::vector< std::vector<size_t> > &indValue );
		
		///
		/// @brief Derivative function for this reaction class
		///
		/// @see BaseReaction::derivs(Compartment &compartment,size_t species,...)
		///
		void derivs(Compartment &compartment,size_t varIndex,
								std::vector< std::vector<double> > &y,
								std::vector< std::vector<double> > &dydt );
		
	};

	template <int T> CellMechanics<T>::
		CellMechanics(std::vector<double> &paraValue, 
									std::vector< std::vector<size_t> > &indValue ) {
		
		//Do some checks on the parameters and variable indeces
		//////////////////////////////////////////////////////////////////////
		if( paraValue.size() != 1 ) {
			std::cerr << "CellMechanics::CellMechanics() "
								<< "Uses one parameters, k\n";
			exit(0);
		}
		if( indValue.size() != 1 || indValue[0].size() !=2 ) {
			std::cerr << "CellMechanics::CellMechanics() "
								<< "use two index, stores force squared and overlap\n" << indValue.size() << "\n";;
			exit(0);
		}
		
		//Set the variable values
		//////////////////////////////////////////////////////////////////////
		setId("cellMechanics");
		setParameter(paraValue);  
		setVariableIndex(indValue);
		
		//Set the parameter identities
		//////////////////////////////////////////////////////////////////////
		std::vector<std::string> tmp( numParameter() );
		tmp[0] = "k";
		setParameterId( tmp );
	}
	
	template <int T> void CellMechanics<T>::
		derivs(Compartment &compartment,size_t varIndex,
					 std::vector< std::vector<double> > &y,
					 std::vector< std::vector<double> > &dydt ) {
		
		if( compartment.numNeighbor()<1 ) return;

		size_t i=compartment.index(); 
		size_t dimension=(compartment.numTopologyVariable()-1);    
		if ( dimension != T ) {
			std::cerr << "CellMechanics::derivs() "
								<< "Wrong number of topology variables.\n";
			std::cerr << dimension << " " << T << "\n";
			exit(-1);
		}
		//initialize the vectors we need
		Vector<T> u;
    double Ri =y[i][compartment.numTopologyVariable() - 1 + varIndex];
    //Loop over neighbors
		
		for( size_t k=0 ; k<compartment.numNeighbor() ; k++ ) {
			//Set neighbor parameters
		
			size_t j = compartment.neighbor(k);
			if( j > i) {
				for ( size_t dim=0 ; dim<T ; ++dim )
					u[dim]=y[j][varIndex+dim]-y[i][varIndex+dim];
				
				
				double Rj =y[j][compartment.numTopologyVariable() - 1 + varIndex];
				double dist =std::sqrt(dot(u,u));
				if (double overlap = Ri+Rj-dist> 0) {
					double fac = parameter(0)*overlap*std::sqrt(overlap)/dist; 
					double R_iInv=1/Ri;
					double R_jInv=1/Rj;
					for( size_t dim=0 ; dim<T ; ++dim ) {
						dydt[i][varIndex + dim] -= fac*R_iInv*u[dim]; 
						dydt[j][varIndex + dim] += fac*R_jInv*u[dim];   
						
					}
					double forceSquare = fac*fac*dot(u,u);
					dydt[i][variableIndex(0,0)] += forceSquare;
					dydt[j][variableIndex(0,0)] += forceSquare;
					dydt[i][variableIndex(0,1)] += overlap;
					dydt[j][variableIndex(0,1)] += overlap;
				}
			}
		}
	}
}
#endif 
