//
// Filename     : growth.h
// Description  : Classes describing growth updates
// Author(s)    : Henrik Jonsson (henrik@thep.lu.se)
// Created      : October 2003
// Revision     : $Id: growth.h 642 2015-07-31 18:48:00Z pau $
//
#ifndef GROWTH_H
#define GROWTH_H

#include<cmath>

#include"baseReaction.h"
///
/// @brief The class growthConstant describes a constant compartment growth
///
/// A constant growth is applied to the last topology variable, e.g. if a 
/// compartment is described by a sphere x y z r, the update will be
///
/// @f[ \frac{dr}{dt} = p_0 @f]
///
/// where the rate is set by the only used parameter (no variable indices needs 
/// to be supplied). For the bacteria compartments (capped cylinder), it will be
/// applied to the resting length (dL/dt = p0). In the model file the reaction
/// is given by
///
/// @verbatim
/// growthConstant 1 0
/// p_0
/// @endverbatim
///
class GrowthConstant : public BaseReaction {
  
 public:
  
  ///
  /// @brief Main constructor
  ///
  /// This is the main constructor which checks and sets the parameters and
  /// variable indices that defines the neighborhood rule.
  ///
  /// @param paraValue Vector with parameters used.
  ///
  /// @param indValue Vector of vectors with variable indices used by the
  /// reaction.
  ///
  /// @see BaseReaction::createReaction(std::vector<double> &, std::vector<
  /// std::vector<size_t> > &, const std::string &)
  ///
  GrowthConstant(std::vector<double> &paraValue, 
		 std::vector< std::vector<size_t> > &indValue );
  
  ///
  /// @brief Derivative function for this reaction class
  ///
  /// @see BaseReaction::derivs(Compartment &compartment,size_t species,...)
  ///
  void derivs(Compartment &compartment,size_t varIndex,
	      std::vector< std::vector<double> > &y,
	      std::vector< std::vector<double> > &dydt );
  ///
  /// @brief Derivative function for this reaction class calculating the absolute value for noise solvers
  ///
  /// @see BaseReaction::derivsWithAbs(Compartment &compartment,size_t species,...)
  ///
  void derivsWithAbs(Compartment &compartment,size_t varIndex,
		     DataMatrix &y,DataMatrix &dydt,DataMatrix &sdydt);
};

///
// @brief A compartment growth proportional to size
///
/// An exponential growth is applied to the last topology variable, e.g. if a 
/// compartment is described by a sphere x y z r, the update will be
///
/// @f[ \frac{dr}{dt} = p_0 r @f]
///
/// where the rate is set by the only used parameter (no variable indices needs 
/// to be supplied). For the bacteria compartments (capped cylinder), it will be
/// applied to the resting length (dL/dt = p0*L). In the model file the reaction
/// is given by
///
/// @verbatim
/// growthExponential 1 0
/// p_0
/// @endverbatim
///
class GrowthExponential : public BaseReaction {
  
 public:
  
  ///
  /// @brief Main constructor
  ///
  /// This is the main constructor which checks and sets the parameters and
  /// variable indices that defines the neighborhood rule.
  ///
  /// @param paraValue Vector with parameters used.
  ///
  /// @param indValue Vector of vectors with variable indices used by the
  /// reaction.
  ///
  /// @see BaseReaction::createReaction(std::vector<double> &, std::vector<
  /// std::vector<size_t> > &, const std::string &)
  ///
  GrowthExponential(std::vector<double> &paraValue, 
		    std::vector< std::vector<size_t> > &indValue );
  
  ///
  /// @brief Derivative function for this reaction class
  ///
  /// @see BaseReaction::derivs(Compartment &compartment,size_t species,...)
  ///
  void derivs(Compartment &compartment,size_t varIndex,
	      std::vector< std::vector<double> > &y,
	      std::vector< std::vector<double> > &dydt );
  
  ///
  /// @brief Derivative function for this reaction class calculating the absolute value for noise solvers
  ///
  /// @see BaseReaction::derivsWithAbs(Compartment &compartment,size_t species,...)
  ///
  void derivsWithAbs(Compartment &compartment,size_t species,
		     DataMatrix &y,DataMatrix &dydt,DataMatrix &sdydt);
};

///
/// @brief Assumes a compartment growth prop to volume and a molecular content*/
///
///
// @brief A compartment growth proportional to size
///
/// An exponential growth is applied to the last topology variable, e.g. if a 
/// compartment is described by a sphere x y z r, the update will be
///
/// @f[ \frac{dr}{dt} = p_0 r x @f]
///
/// where the rate is set by the only used parameter and multiplied
/// with the molecular value x. (no size variable index needs
/// to be supplied). For the bacteria compartments (capped cylinder), it will be
/// applied to the resting length (dL/dt = p0*L*x). In the model file the reaction
/// is given by
///
/// @verbatim
/// growthExponentialOne 1 1 1
/// p_0
/// xIndex
/// @endverbatim
///
class GrowthExponentialOne : public BaseReaction {
  
 public:
  
  ///
  /// @brief Main constructor
  ///
  /// This is the main constructor which checks and sets the parameters and
  /// variable indices that defines the neighborhood rule.
  ///
  /// @param paraValue Vector with parameters used.
  ///
  /// @param indValue Vector of vectors with variable indices used by the
  /// reaction.
  ///
  /// @see BaseReaction::createReaction(std::vector<double> &, std::vector<
  /// std::vector<size_t> > &, const std::string &)
  ///
  GrowthExponentialOne(std::vector<double> &paraValue, 
		       std::vector< std::vector<size_t> > &indValue );
  
  ///
  /// @brief Derivative function for this reaction class
  ///
  /// @see BaseReaction::derivs(Compartment &compartment,size_t species,...)
  ///
  void derivs(Compartment &compartment,size_t varIndex,
	      std::vector< std::vector<double> > &y,
	      std::vector< std::vector<double> > &dydt );

  ///
  /// @brief Derivative function for this reaction class calculating the absolute value for noise solvers
  ///
  /// @see BaseReaction::derivsWithAbs(Compartment &compartment,size_t species,...)
  ///
  void derivsWithAbs(Compartment &compartment,size_t species,DataMatrix &y,DataMatrix &dydt,DataMatrix &sdydt);
};

///
/// @brief The class GrowthExponentialRestricted assumes exponential growth for cells marked with flag 
///
/// An exponential growth is applied to the last topology variable, e.g. if a 
/// compartment is described by a sphere x y z r, the update will be
///
/// @f[ \frac{dr}{dt} = p_0 r @f]
///
/// if
///
/// @f[ y_{th} > p_1 @f]
/// 
/// where the rate is set by p0 and the growth is only provided if the threshold variable
/// is larger than p1. (no size variable index needs
/// to be supplied). For the bacteria compartments (capped cylinder), it will be
/// applied to the resting length (dL/dt = p0*L). In the model file the reaction
/// is given by
///
/// @verbatim
/// growthExponentialRestricted 2 1 1
/// p_0 p_1
/// thIndex
/// @endverbatim
///
class GrowthExponentialRestricted : public BaseReaction {
  
 public:
  
  ///
  /// @brief Main constructor
  ///
  /// This is the main constructor which checks and sets the parameters and
  /// variable indices that defines the neighborhood rule.
  ///
  /// @param paraValue Vector with parameters used.
  ///
  /// @param indValue Vector of vectors with variable indices used by the
  /// reaction.
  ///
  /// @see BaseReaction::createReaction(std::vector<double> &, std::vector<
  /// std::vector<size_t> > &, const std::string &)
  ///
  GrowthExponentialRestricted(std::vector<double> &paraValue, 
			      std::vector< std::vector<size_t> > &indValue );
  
  ///
  /// @brief Derivative function for this reaction class
  ///
  /// @see BaseReaction::derivs(Compartment &compartment,size_t species,...)
  ///
  void derivs(Compartment &compartment,size_t varIndex,
	      std::vector< std::vector<double> > &y,
	      std::vector< std::vector<double> > &dydt );
};

///
/// @brief Applies an exponential growth smothly truncated at a maximal size
///
/// Assumes a compartment growth prop to size, but decreased when
/// approaching a maximal size (where growth becomes zero). The
/// growth is applied to the last topology variable, e.g. if a 
/// compartment is described by a sphere x y z r, the update will be
///
/// @f[ \frac{dr}{dt} = p_0 r (1 - \frac{r}{p_1} @f]
///
/// where the rate is set by p0 and the growth is smothly truncated when the size approach p1.
/// (no size variable index needs
/// to be supplied). For the bacteria compartments (capped cylinder), it will be
/// applied to the resting length (dL/dt = p0*L (1-L/p1) ). In the model file the reaction
/// is given by
///
/// @verbatim
/// growthExponentialTrunc 2 0
/// p_0 p_1
/// @endverbatim
///
class GrowthExponentialTrunc : public BaseReaction {
  
 public:
  
  ///
  /// @brief Main constructor
  ///
  /// This is the main constructor which checks and sets the parameters and
  /// variable indices that defines the neighborhood rule.
  ///
  /// @param paraValue Vector with parameters used.
  ///
  /// @param indValue Vector of vectors with variable indices used by the
  /// reaction.
  ///
  /// @see BaseReaction::createReaction(std::vector<double> &, std::vector<
  /// std::vector<size_t> > &, const std::string &)
  ///
  GrowthExponentialTrunc(std::vector<double> &paraValue, 
			 std::vector< std::vector<size_t> > &indValue );
  
  ///
  /// @brief Derivative function for this reaction class
  ///
  /// @see BaseReaction::derivs(Compartment &compartment,size_t species,...)
  ///
  void derivs(Compartment &compartment,size_t varIndex,
	      std::vector< std::vector<double> > &y,
	      std::vector< std::vector<double> > &dydt );

  void derivsWithAbs(Compartment &compartment,size_t varIndex,
		DataMatrix &y,DataMatrix &dydt,DataMatrix &sdydt);
};

///
/// @brief Applies an exponential growth smothly truncated at a maximal size if a variable is above threshold
///
/// Assumes a compartment growth prop (p0) to size, but decreased when
/// approaching a maximal size (p2, where growth becomes zero), and only if the value of a threshold
/// variable is above threshold (p1). The
/// growth is applied to the last topology variable, e.g. if a 
/// compartment is described by a sphere x y z r, the update will be
///
/// @f[ \frac{dr}{dt} = p_0 r (1 - \frac{r}{p_2} @f]
///
/// if
///
/// @f[ y_{it} > p_{1} @f]
///
/// where the rate is set by p0 and the growth is smothly truncated when the size approach p2 and
/// only if threshold variable is abobe p1.
/// (no size variable index needs
/// to be supplied). For the bacteria compartments (capped cylinder), it will be
/// applied to the resting length (dL/dt = p0*L (1-L/p1) ). In the model file the reaction
/// is given by
///
/// @verbatim
/// growthExponentialTrunc 3 1 1
/// p_0 p_1 p_2
/// thIndex
/// @endverbatim
///
class GrowthExponentialRestrictedTrunc : public BaseReaction {
  
 public:
  
  ///
  /// @brief Main constructor
  ///
  /// This is the main constructor which checks and sets the parameters and
  /// variable indices that defines the neighborhood rule.
  ///
  /// @param paraValue Vector with parameters used.
  ///
  /// @param indValue Vector of vectors with variable indices used by the
  /// reaction.
  ///
  /// @see BaseReaction::createReaction(std::vector<double> &, std::vector<
  /// std::vector<size_t> > &, const std::string &)
  ///
  GrowthExponentialRestrictedTrunc(std::vector<double> &paraValue, 
			      std::vector< std::vector<size_t> > &indValue );
  
  ///
  /// @brief Derivative function for this reaction class
  ///
  /// @see BaseReaction::derivs(Compartment &compartment,size_t species,...)
  ///
  void derivs(Compartment &compartment,size_t varIndex,
	      std::vector< std::vector<double> > &y,
	      std::vector< std::vector<double> > &dydt );
};

///
/// @brief Dilution corrects for concentration changes due to compartment growth
///
/// Calculates dilution update for a specific molecular species by assuming a
/// radial growth and spherical cells.
///
/// @[f \frac{dc_i}{dt] = -\frac{c_i dV_i}{V_i}
///
/// where volume, V, and dV are calculated depending on dimension (dV/V equals
/// 2 dr/r in 2D and 3 dr/r in 3D).
/// Uses the variable index for the radii (provided from user) to determine
/// dimension.
///
/// In a model file the reaction is given by:
///
/// @verbatim
/// dilution 0 1 1
/// r_index
/// @endverbatim
///
/// @note Can only be used for spherical cells.
///
class Dilution : public BaseReaction {
  
 public:
  
  ///
  /// @brief Main constructor
  ///
  /// This is the main constructor which checks and sets the parameters and
  /// variable indices that defines the neighborhood rule.
  ///
  /// @param paraValue Vector with parameters used.
  ///
  /// @param indValue Vector of vectors with variable indices used by the
  /// reaction.
  ///
  /// @see BaseReaction::createReaction(std::vector<double> &, std::vector<
  /// std::vector<size_t> > &, const std::string &)
  ///
  Dilution(std::vector<double> &paraValue, 
	   std::vector< std::vector<size_t> > &indValue );
  
  ///
  /// @brief Derivative function for this reaction class
  ///
  /// @see BaseReaction::derivs(Compartment &compartment,size_t species,...)
  ///
  void derivs(Compartment &compartment,size_t species,
	      std::vector< std::vector<double> > &y,
	      std::vector< std::vector<double> > &dydt );

  void derivsWithAbs(Compartment &compartment,size_t species,
	      std::vector< std::vector<double> > &y,
	      std::vector< std::vector<double> > &dydt,
	      std::vector< std::vector<double> > &sdydt);
};

///
/// @brief Calculates or reads the volume and adds it to variable index provided
///
/// This function uses the built in function in Compartment to calculate the 
/// current volume if the Compartment is a known geometry (e.g. sphere) or reads
/// it otherwise. It does it in the update (not derivs) and initiate, and stores
/// the volume at variable index provided.
///
/// In a model file the reaction is given by:
///
/// @verbatim
/// UpdateVolume 0 1 1
/// vol_index
/// @endverbatim
///
/// @see Compartment.getVolume().
///
class UpdateVolume : public BaseReaction {
  
 public:
  
  ///
  /// @brief Main constructor
  ///
  /// This is the main constructor which checks and sets the parameters and
  /// variable indices that defines the neighborhood rule.
  ///
  /// @param paraValue Vector with parameters used.
  ///
  /// @param indValue Vector of vectors with variable indices used by the
  /// reaction.
  ///
  /// @see BaseReaction::createReaction(std::vector<double> &, std::vector<
  /// std::vector<size_t> > &, const std::string &)
  ///
  UpdateVolume(std::vector<double> &paraValue, 
	       std::vector< std::vector<size_t> > &indValue );
  
  ///
  /// @brief Derivative function for this reaction class
  ///
  /// @see BaseReaction::derivs(Compartment &compartment,size_t species,...)
  ///
  void derivs(Compartment &compartment,size_t species,
	      std::vector< std::vector<double> > &y,
	      std::vector< std::vector<double> > &dydt );
  ///
  /// @brief Derivative function for this reaction class calculating the absolute value for noise solvers
  ///
  /// @see BaseReaction::derivsWithAbs(Compartment &compartment,size_t species,...)
  ///
  void derivsWithAbs(Compartment &compartment,size_t species,DataMatrix &y,DataMatrix &dydt,DataMatrix &sdydt);
  ///
  /// @brief Initialization function for this reaction class
  ///
  /// @see BaseReaction::initiate(double t,DataMatrix &y)
  ///
  void initiate(double t,DataMatrix &y);
  ///
  /// @brief Update function for this reaction class
  ///
  /// @see BaseReaction::update(double h, double t, ...)
  ///
  void update(double h, double t, DataMatrix &y);
};

//!The class EllipticGrowthExponentialTrunc 
/*! EllipticGrowthExponentialTrunc assumes a constant compartment
  growth prop to focal distance D, truncated at DMax.*/
class EllipticGrowthExponentialTrunc : public BaseReaction {
  
 public:
  
	///
	/// @brief Main constructor
	///
	/// This is the main constructor which checks and sets the parameters and
	/// variable indices that defines the neighborhood rule.
	///
	/// @param paraValue Vector with parameters used.
	///
	/// @param indValue Vector of vectors with variable indices used by the
	/// reaction.
	///
	/// @see BaseReaction::createReaction(std::vector<double> &, std::vector<
	/// std::vector<size_t> > &, const std::string &)
	///
  EllipticGrowthExponentialTrunc(std::vector<double> &paraValue, 
		 std::vector< std::vector<size_t> > &indValue );
  
  ///
  /// @brief Derivative function for this reaction class
  ///
  /// @see BaseReaction::derivs(Compartment &compartment,size_t species,...)
  ///
  void derivs(Compartment &compartment,size_t varIndex,
	      std::vector< std::vector<double> > &y,
	      std::vector< std::vector<double> > &dydt );
};

/// @brief CigarGrowthExponentialTrunc assumes a constant compartment
/// growth prop to focal distance D, truncated at DMax.*/
///
/// Describing an exponential
/// truncated constant growth using the values in y and add the results
/// to dydt. Compartment is the cell and varIndex is the index of the
/// focal length variable D to be updated.  The parameters are k_growth,
/// DMax, k_spring in dD/dt=k_growth*D(1-D/DMax). Finally, we imagine a
/// spring with spring constant k_spring attached between the two focal
/// points, and update the cell coordinates accordingly. NOTE!
/// Coordinates are not updated using this class right now.
///
class CigarGrowthExponentialTrunc : public BaseReaction {
  
 public:
  
	///
	/// @brief Main constructor
	///
	/// This is the main constructor which checks and sets the parameters and
	/// variable indices that defines the neighborhood rule.
	///
	/// @param paraValue Vector with parameters used.
	///
	/// @param indValue Vector of vectors with variable indices used by the
	/// reaction.
	///
	/// @see BaseReaction::createReaction(std::vector<double> &, std::vector<
	/// std::vector<size_t> > &, const std::string &)
	///
  CigarGrowthExponentialTrunc(std::vector<double> &paraValue, 
			      std::vector< std::vector<size_t> > &indValue );
  
  ///
  /// @brief Derivative function for this reaction class
  ///
  /// @see BaseReaction::derivs(Compartment &compartment,size_t species,...)
  ///
  void derivs(Compartment &compartment,size_t varIndex,
	      std::vector< std::vector<double> > &y,
	      std::vector< std::vector<double> > &dydt );

};

//!The class is used to describe growth of a sphere-bud compartment 
class SphereBudGrowthTruncNeigh : public BaseReaction {
  
 public:
  
	///
	/// @brief Main constructor
	///
	/// This is the main constructor which checks and sets the parameters and
	/// variable indices that defines the neighborhood rule.
	///
	/// @param paraValue Vector with parameters used.
	///
	/// @param indValue Vector of vectors with variable indices used by the
	/// reaction.
	///
	/// @see BaseReaction::createReaction(std::vector<double> &, std::vector<
	/// std::vector<size_t> > &, const std::string &)
	///
  SphereBudGrowthTruncNeigh(std::vector<double> &paraValue, 
			    std::vector< std::vector<size_t> > &indValue );
  
  ///
  /// @brief Derivative function for this reaction class
  ///
  /// @see BaseReaction::derivs(Compartment &compartment,size_t species,...)
  ///
  void derivs(Compartment &compartment,size_t varIndex,
	      std::vector< std::vector<double> > &y,
	      std::vector< std::vector<double> > &dydt );
};

///
/// @brief Calculates a new radius from adding r+0.5dr of internal
/// neighbor and 0.5dr from itself
class LensRadiusGrowth : public BaseReaction {
  
 public:
  
	///
	/// @brief Main constructor
	///
	/// This is the main constructor which checks and sets the parameters and
	/// variable indices that defines the neighborhood rule.
	///
	/// @param paraValue Vector with parameters used.
	///
	/// @param indValue Vector of vectors with variable indices used by the
	/// reaction.
	///
	/// @see BaseReaction::createReaction(std::vector<double> &, std::vector<
	/// std::vector<size_t> > &, const std::string &)
	///
  LensRadiusGrowth(std::vector<double> &paraValue, 
									 std::vector< std::vector<size_t> > &indValue );
  
  ///
  /// @brief Derivative function for this reaction class
  ///
  /// @see BaseReaction::derivs(Compartment &compartment,size_t species,...)
  ///
  void derivs(Compartment &compartment,size_t varIndex,
							std::vector< std::vector<double> > &y,
							std::vector< std::vector<double> > &dydt );
};

class LensLayerGrowth : public BaseReaction {
  
 public:
  
	///
	/// @brief Main constructor
	///
	/// This is the main constructor which checks and sets the parameters and
	/// variable indices that defines the neighborhood rule.
	///
	/// @param paraValue Vector with parameters used.
	///
	/// @param indValue Vector of vectors with variable indices used by the
	/// reaction.
	///
	/// @see BaseReaction::createReaction(std::vector<double> &, std::vector<
	/// std::vector<size_t> > &, const std::string &)
	///
  LensLayerGrowth(std::vector<double> &paraValue, 
									std::vector< std::vector<size_t> > &indValue );
  
  ///
  /// @brief Derivative function for this reaction class
  ///
  /// @see BaseReaction::derivs(Compartment &compartment,size_t species,...)
  ///
  void derivs(Compartment &compartment,size_t varIndex,
							std::vector< std::vector<double> > &y,
							std::vector< std::vector<double> > &dydt );
};

class LensDilution : public BaseReaction {
  
public:
  
	///
	/// @brief Main constructor
	///
	/// This is the main constructor which checks and sets the parameters and
	/// variable indices that defines the neighborhood rule.
	///
	/// @param paraValue Vector with parameters used.
	///
	/// @param indValue Vector of vectors with variable indices used by the
	/// reaction.
	///
	/// @see BaseReaction::createReaction(std::vector<double> &, std::vector<
	/// std::vector<size_t> > &, const std::string &)
	///
  LensDilution(std::vector<double> &paraValue, 
							 std::vector< std::vector<size_t> > &indValue );
  
  ///
  /// @brief Derivative function for this reaction class
  ///
  /// @see BaseReaction::derivs(Compartment &compartment,size_t species,...)
  ///
  void derivs(Compartment &compartment,size_t varIndex,
							std::vector< std::vector<double> > &y,
							std::vector< std::vector<double> > &dydt );
};

#endif
