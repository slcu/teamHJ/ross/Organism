//
// Filename     : massAction.cc
// Description  : Classes describing mass action reactions
// Author(s)    : Henrik Jonsson (henrik@thep.lu.se)
// Created      : November 2003
// Revision     : $Id: massAction.cc 664 2016-02-26 17:15:54Z andre $
//
#include"massAction.h"
#include"baseReaction.h"
#include"../organism.h"

namespace MassAction
{
  General::
  General(std::vector<double> &paraValue, 
	  std::vector< std::vector<size_t> > &indValue ) 
  {  
    //
    // Do some checks on the parameters and variable indeces
    //
    if( paraValue.size()!=1 ) {
      std::cerr << "MassAction::General::General() "
		<< "Uses only one parameter k_f\n";
      exit(0);
    }
    if( indValue.size() !=2 ) {
      std::cerr << "MassAction::General::General() "
		<< "Two levels of variable indeces are used.\n"
		<< "One for reactants and one for products\n";
      exit(0);
    }
    if( indValue[0].size()<1 ) {
      std::cerr << "MassAction::General::General() "
		<< "If no reactants given, there will be no reaction...\n";
      exit(0);
    }
    //
    // Set the variable values
    //
    setId("massAction::general");
    setParameter(paraValue);  
    setVariableIndex(indValue);
    //
    // Set the parameter identities
    //
    std::vector<std::string> tmp( numParameter() );
    tmp.resize( numParameter() );
    tmp[0] = "k_f";
    setParameterId( tmp );
  }
  
  void General::
  derivs(Compartment &compartment,size_t varIndex,DataMatrix &y,DataMatrix &dydt) 
  {  
    double rate = parameter(0);
    if( numVariableIndex(0) )
      for( size_t i=0 ; i< numVariableIndex(0) ; i++ )
	rate *= y[compartment.index()][variableIndex(0,i)];
    else //No reaction defined...
      return;
    
    if (rate<=0.0) //No update
      return;
    
    for( size_t i=0 ; i< numVariableIndex(0) ; ++i )
      dydt[compartment.index()][variableIndex(0,i)] -= rate;
    
    for( size_t i=0 ; i< numVariableIndex(1) ; ++i )
      dydt[compartment.index()][variableIndex(1,i)] += rate;
  }

  void General::
  derivsWithAbs(Compartment &compartment,size_t varIndex,DataMatrix &y,DataMatrix &dydt, DataMatrix &sdydt) 
  {  
    double rate = parameter(0);
    if( numVariableIndex(0) )
      for( size_t i=0 ; i< numVariableIndex(0) ; i++ )
	rate *= y[compartment.index()][variableIndex(0,i)];
    else //No reaction defined...
      return;
    
    if (rate<=0.0) //No update
      return;
    
    for( size_t i=0 ; i< numVariableIndex(0) ; ++i ) {
      dydt[compartment.index()][variableIndex(0,i)] -= rate;
      sdydt[compartment.index()][variableIndex(0,i)] += std::fabs(rate);
    }
    
    for( size_t i=0 ; i< numVariableIndex(1) ; ++i ) {
      dydt[compartment.index()][variableIndex(1,i)] += rate;
      sdydt[compartment.index()][variableIndex(1,i)] += std::fabs(rate);
    }
  }
  
  size_t General::
  Jacobian(Compartment &compartment,size_t varIndex,DataMatrix &y,JacobianMatrix &A) 
  {  
    double rate = parameter(0);
    if( numVariableIndex(0) )
      for (size_t k=0; k< numVariableIndex(0); ++k)
	rate *= y[compartment.index()][variableIndex(0,k)];
    else //No reaction defined...
      return 0;
    if (rate<=0.0) //No update
      return 1;
    //
    // Update all A
    //
    for (size_t k=0; k<numVariableIndex(0); ++k ) {
      double element = rate / y[compartment.index()][variableIndex(0,k)];
      size_t j = compartment.index()*y[0].size() + variableIndex(0,k); 
      // Reactants
      for (size_t kk=0; kk<numVariableIndex(0); ++kk ) {
	size_t i = compartment.index()*y[0].size() + variableIndex(0,kk); 
	A(i,j) -= element;
      }
      // products
      for (size_t kk=0; kk<numVariableIndex(1); ++kk ) {
	size_t i = compartment.index()*y[0].size() + variableIndex(1,kk); 
	A(i,j) += element;
      }
    }
    return 1;
  }
  
  double General::
  propensity(Compartment &compartment,size_t varIndex,DataMatrix &y)
  {
    double rate = parameter(0);
    if( numVariableIndex(0) )
      for (size_t k=0; k<numVariableIndex(0); ++k)
	rate *= y[compartment.index()][variableIndex(0,k)];
    else //No reaction defined...
      return 0.0;
    if (rate<=0.0) //No update
      return 0.0;
    return rate;
  }
  
  void General::
  discreteUpdate(Compartment &compartment,size_t varIndex,DataMatrix &y)
  {
    if( numVariableIndex(0) )
      for (size_t k=0; k<numVariableIndex(0); ++k)
	y[compartment.index()][variableIndex(0,k)] -= 1.0;
    if( numVariableIndex(1) )
      for (size_t k=0; k<numVariableIndex(1); ++k)
	y[compartment.index()][variableIndex(1,k)] += 1.0;
  }
  
  OneToOne::
  OneToOne(std::vector<double> &paraValue, 
	   std::vector< std::vector<size_t> > &indValue ) 
  {  
    //
    // Do some checks on the parameters and variable indeces
    //
    if( paraValue.size()!=1 ) {
      std::cerr << "MassAction::OneToOne::OneToOne() "
		<< "Uses only one parameter k_f\n";
      exit(0);
    }
    if( indValue.size() !=2 || indValue[0].size() !=1 
	|| indValue[1].size() !=1) {
      std::cerr << "MassAction::OneToOne::OneToOne() "
		<< "Two levels of variable indeces are used.\n"
		<< "One for a single reactant and one for a single product.\n";
      exit(0);
    }
    //
    // Set the variable values
    //
    setId("massAction::OneToOne");
    setParameter(paraValue);
    setVariableIndex(indValue);
    //
    // Set the parameter identities
    //
    std::vector<std::string> tmp( numParameter() );
    tmp.resize( numParameter() );
    tmp[0] = "k_f";
    setParameterId( tmp );
  }
  
  void OneToOne::
  derivs(Compartment &compartment,size_t varIndex,DataMatrix &y,DataMatrix &dydt) 
  {  
    double rate = parameter(0)*y[compartment.index()][variableIndex(0,0)];
    if (rate<=0.0) //No update
      return;
    dydt[compartment.index()][variableIndex(0,0)] -= rate;
    dydt[compartment.index()][variableIndex(1,0)] += rate;
  }

  void OneToOne::
  derivsWithAbs(Compartment &compartment,size_t varIndex,DataMatrix &y,DataMatrix &dydt, DataMatrix &sdydt) 
  {  
    double rate = parameter(0)*y[compartment.index()][variableIndex(0,0)];
    if (rate<=0.0) //No update
      return;
    dydt[compartment.index()][variableIndex(0,0)] -= rate;
    dydt[compartment.index()][variableIndex(1,0)] += rate;
    sdydt[compartment.index()][variableIndex(0,0)] += rate;
    sdydt[compartment.index()][variableIndex(1,0)] += rate;
  }
  
  size_t OneToOne::
  Jacobian(Compartment &compartment,size_t varIndex,DataMatrix &y,JacobianMatrix &A) 
  {  
    double rate = parameter(0);
    
    if (rate<=0.0) //No update
      return 1;
    //
    // Update all A
    //
    size_t j = compartment.index()*y[0].size() + variableIndex(0,0); 
    // Reactant
    A(j,j) -= rate;
    // product
    size_t i = compartment.index()*y[0].size() + variableIndex(1,0); 
    A(i,j) += rate;
    return 1;
  }
  
  double OneToOne::
  propensity(Compartment &compartment,size_t varIndex,DataMatrix &y)
  {
    double rate = parameter(0)*y[compartment.index()][variableIndex(0,0)];
    if (rate<=0.0) //No update
      return 0.0;
    return rate;
  }
  
  void OneToOne::
  discreteUpdate(Compartment &compartment,size_t varIndex,DataMatrix &y)
  {
    y[compartment.index()][variableIndex(0,0)] -= 1.0;
    y[compartment.index()][variableIndex(1,0)] += 1.0;
  }
  
  OneToTwo::
  OneToTwo(std::vector<double> &paraValue, 
	   std::vector< std::vector<size_t> > &indValue ) 
  {  
    //
    // Do some checks on the parameters and variable indeces
    //
    if( paraValue.size()!=1 ) {
      std::cerr << "MassAction::OneToTwo::OneToTwo() "
		<< "Uses only one parameter k_f\n";
      exit(0);
    }
    if( indValue.size() !=2 || indValue[0].size() !=1 
	|| indValue[1].size() !=2) {
      std::cerr << "MassAction::OneToTwo::OneToTwo() "
		<< "Two levels of variable indeces are used.\n"
		<< "One for a single reactant and one for two products.\n";
      exit(0);
    }
    //
    // Set the variable values
    //
    setId("massAction::OneToTwo");
    setParameter(paraValue);  
    setVariableIndex(indValue);
    //
    // Set the parameter identities
    //
    std::vector<std::string> tmp( numParameter() );
    tmp.resize( numParameter() );
    tmp[0] = "k_f";
    setParameterId( tmp );
  }
  
  void OneToTwo::
  derivs(Compartment &compartment,size_t varIndex,DataMatrix &y,DataMatrix &dydt) 
  {  
    double rate = parameter(0)*y[compartment.index()][variableIndex(0,0)];
    if (rate<=0.0) //No update
      return;
    dydt[compartment.index()][variableIndex(0,0)] -= rate;
    dydt[compartment.index()][variableIndex(1,0)] += rate;
    dydt[compartment.index()][variableIndex(1,1)] += rate;
  }

  void OneToTwo::
  derivsWithAbs(Compartment &compartment,size_t varIndex,DataMatrix &y,DataMatrix &dydt, DataMatrix &sdydt) 
  {  
    double rate = parameter(0)*y[compartment.index()][variableIndex(0,0)];
    if (rate<=0.0) //No update
      return;
    dydt[compartment.index()][variableIndex(0,0)] -= rate;
    dydt[compartment.index()][variableIndex(1,0)] += rate;
    dydt[compartment.index()][variableIndex(1,1)] += rate;
    sdydt[compartment.index()][variableIndex(0,0)] += rate;
    sdydt[compartment.index()][variableIndex(1,0)] += rate;
    sdydt[compartment.index()][variableIndex(1,1)] += rate;
  }
  
  size_t OneToTwo::
  Jacobian(Compartment &compartment,size_t varIndex,DataMatrix &y,JacobianMatrix &A) 
  {  
    double rate = parameter(0);	
    if (rate<=0.0) //No update
      return 1;
    //
    // Update all A
    //
    size_t j = compartment.index()*y[0].size() + variableIndex(0,0); 
    // Reactant
    A(j,j) -= rate;
    // products
    size_t i = compartment.index()*y[0].size() + variableIndex(1,0); 
    A(i,j) += rate;
    i = compartment.index()*y[0].size() + variableIndex(1,1); 
    A(i,j) += rate;
    return 1;
  }
  
  double OneToTwo::
  propensity(Compartment &compartment,size_t varIndex,DataMatrix &y)
  {
    double rate = parameter(0)*y[compartment.index()][variableIndex(0,0)];
    if (rate<=0.0) //No update
      return 0.0;
    return rate;
  }
  
  void OneToTwo::
  discreteUpdate(Compartment &compartment,size_t varIndex,DataMatrix &y)
  {
    y[compartment.index()][variableIndex(0,0)] -= 1.0;
    y[compartment.index()][variableIndex(1,0)] += 1.0;
    y[compartment.index()][variableIndex(1,1)] += 1.0;
  }
  
  OneToThree::
  OneToThree(std::vector<double> &paraValue, 
	     std::vector< std::vector<size_t> > &indValue ) 
  {  
    //
    // Do some checks on the parameters and variable indeces
    //
    if( paraValue.size()!=1 ) {
      std::cerr << "MassAction::OneToThree::OneToThree() "
		<< "Uses only one parameter k_f\n";
      exit(0);
    }
    if( indValue.size() !=2 || indValue[0].size() !=1 
	|| indValue[1].size() !=3) {
      std::cerr << "MassAction::OneToThree::OneToThree() "
		<< "Two levels of variable indeces are used.\n"
		<< "One for a single reactant and one for three products.\n";
      exit(0);
    }
    //
    // Set the variable values
    //
    setId("massAction::OneToThree");
    setParameter(paraValue);  
    setVariableIndex(indValue);
    //
    // Set the parameter identities
    //
    std::vector<std::string> tmp( numParameter() );
    tmp.resize( numParameter() );
    tmp[0] = "k_f";
    setParameterId( tmp );
  }
  
  void OneToThree::
  derivs(Compartment &compartment,size_t varIndex,DataMatrix &y,DataMatrix &dydt) 
  {  
    double rate = parameter(0)*y[compartment.index()][variableIndex(0,0)];
    if (rate<=0.0) //No update
      return;
    dydt[compartment.index()][variableIndex(0,0)] -= rate;
    dydt[compartment.index()][variableIndex(1,0)] += rate;
    dydt[compartment.index()][variableIndex(1,1)] += rate;
    dydt[compartment.index()][variableIndex(1,2)] += rate;
  }

  void OneToThree::
  derivsWithAbs(Compartment &compartment,size_t varIndex,DataMatrix &y,DataMatrix &dydt, DataMatrix &sdydt) 
  {  
    double rate = parameter(0)*y[compartment.index()][variableIndex(0,0)];
    if (rate<=0.0) //No update
      return;
    dydt[compartment.index()][variableIndex(0,0)] -= rate;
    dydt[compartment.index()][variableIndex(1,0)] += rate;
    dydt[compartment.index()][variableIndex(1,1)] += rate;
    dydt[compartment.index()][variableIndex(1,2)] += rate;
    sdydt[compartment.index()][variableIndex(0,0)] += rate;
    sdydt[compartment.index()][variableIndex(1,0)] += rate;
    sdydt[compartment.index()][variableIndex(1,1)] += rate;
    sdydt[compartment.index()][variableIndex(1,2)] += rate;
  }
  
  size_t OneToThree::
  Jacobian(Compartment &compartment,size_t varIndex,DataMatrix &y,JacobianMatrix &A) 
  {  
    double rate = parameter(0);	
    if (rate<=0.0) //No update
      return 1;
    //
    // Update all A
    //
    size_t j = compartment.index()*y[0].size() + variableIndex(0,0); 
    // Reactant
    A(j,j) -= rate;
    // products
    size_t i = compartment.index()*y[0].size() + variableIndex(1,0); 
    A(i,j) += rate;
    i = compartment.index()*y[0].size() + variableIndex(1,1); 
    A(i,j) += rate;
    i = compartment.index()*y[0].size() + variableIndex(1,2); 
    A(i,j) += rate;
    return 1;
  }
  
  double OneToThree::
  propensity(Compartment &compartment,size_t varIndex,DataMatrix &y)
  {
    double rate = parameter(0)*y[compartment.index()][variableIndex(0,0)];
    if (rate<=0.0) //No update
      return 0.0;
    return rate;
  }
  
  void OneToThree::
  discreteUpdate(Compartment &compartment,size_t varIndex,DataMatrix &y)
  {
    y[compartment.index()][variableIndex(0,0)] -= 1.0;
    y[compartment.index()][variableIndex(1,0)] += 1.0;
    y[compartment.index()][variableIndex(1,1)] += 1.0;
    y[compartment.index()][variableIndex(1,2)] += 1.0;
  }
  
  OneToFour::
  OneToFour(std::vector<double> &paraValue, 
	    std::vector< std::vector<size_t> > &indValue ) 
  {  
    //
    // Do some checks on the parameters and variable indeces
    //
    if( paraValue.size()!=1 ) {
      std::cerr << "MassAction::OneToFour::OneToFour() "
		<< "Uses only one parameter k_f\n";
      exit(0);
    }
    if( indValue.size() !=2 || indValue[0].size() !=1 
	|| indValue[1].size() !=4) {
      std::cerr << "MassAction::OneToFour::OneToFour() "
		<< "Two levels of variable indeces are used.\n"
		<< "One for a single reactant and one for four products.\n";
      exit(0);
    }
    //
    // Set the variable values
    //
    setId("massAction::OneToFour");
    setParameter(paraValue);  
    setVariableIndex(indValue);
    //
    // Set the parameter identities
    //
    std::vector<std::string> tmp( numParameter() );
    tmp.resize( numParameter() );
    tmp[0] = "k_f";
    setParameterId( tmp );
  }
  
  void OneToFour::
  derivs(Compartment &compartment,size_t varIndex,DataMatrix &y,DataMatrix &dydt) 
  {  
    double rate = parameter(0)*y[compartment.index()][variableIndex(0,0)];
    if (rate<=0.0) //No update
      return;
    dydt[compartment.index()][variableIndex(0,0)] -= rate;
    dydt[compartment.index()][variableIndex(1,0)] += rate;
    dydt[compartment.index()][variableIndex(1,1)] += rate;
    dydt[compartment.index()][variableIndex(1,2)] += rate;
    dydt[compartment.index()][variableIndex(1,3)] += rate;
  }

  void OneToFour::
  derivsWithAbs(Compartment &compartment,size_t varIndex,DataMatrix &y,DataMatrix &dydt, DataMatrix &sdydt) 
  {  
    double rate = parameter(0)*y[compartment.index()][variableIndex(0,0)];
    if (rate<=0.0) //No update
      return;
    dydt[compartment.index()][variableIndex(0,0)] -= rate;
    dydt[compartment.index()][variableIndex(1,0)] += rate;
    dydt[compartment.index()][variableIndex(1,1)] += rate;
    dydt[compartment.index()][variableIndex(1,2)] += rate;
    dydt[compartment.index()][variableIndex(1,3)] += rate;
    sdydt[compartment.index()][variableIndex(0,0)] += rate;
    sdydt[compartment.index()][variableIndex(1,0)] += rate;
    sdydt[compartment.index()][variableIndex(1,1)] += rate;
    sdydt[compartment.index()][variableIndex(1,2)] += rate;
    sdydt[compartment.index()][variableIndex(1,3)] += rate;
  }
  
  size_t OneToFour::
  Jacobian(Compartment &compartment,size_t varIndex,DataMatrix &y,JacobianMatrix &A) 
  {  
    double rate = parameter(0);	
    if (rate<=0.0) //No update
      return 1;
    //
    // Update all A
    //
    size_t j = compartment.index()*y[0].size() + variableIndex(0,0); 
    // Reactant
    A(j,j) -= rate;
    // products
    size_t i = compartment.index()*y[0].size() + variableIndex(1,0); 
    A(i,j) += rate;
    i = compartment.index()*y[0].size() + variableIndex(1,1); 
    A(i,j) += rate;
    i = compartment.index()*y[0].size() + variableIndex(1,2); 
    A(i,j) += rate;
    i = compartment.index()*y[0].size() + variableIndex(1,3); 
    A(i,j) += rate;
    return 1;
  }
  
  double OneToFour::
  propensity(Compartment &compartment,size_t varIndex,DataMatrix &y)
  {
    double rate = parameter(0)*y[compartment.index()][variableIndex(0,0)];
    if (rate<=0.0) //No update
      return 0.0;
    return rate;
  }
  
  void OneToFour::
  discreteUpdate(Compartment &compartment,size_t varIndex,DataMatrix &y)
  {
    y[compartment.index()][variableIndex(0,0)] -= 1.0;
    y[compartment.index()][variableIndex(1,0)] += 1.0;
    y[compartment.index()][variableIndex(1,1)] += 1.0;
    y[compartment.index()][variableIndex(1,2)] += 1.0;
    y[compartment.index()][variableIndex(1,3)] += 1.0;
  }
  
  TwoToOne::
  TwoToOne(std::vector<double> &paraValue, 
	   std::vector< std::vector<size_t> > &indValue ) 
  {  
    //
    // Do some checks on the parameters and variable indeces
    //
    if( paraValue.size()!=1 ) {
      std::cerr << "MassAction::TwoToOne::TwoToOne() "
		<< "Uses only one parameter k_f\n";
      exit(0);
    }
    if( indValue.size() !=2 || indValue[0].size() !=2 
	|| indValue[1].size() !=1) {
      std::cerr << "MassAction::TwoToOne::TwoToOne() "
		<< "Two levels of variable indeces are used.\n"
		<< "One for two reactants and one for a single product.\n";
      exit(0);
    }
    //
    // Set the variable values
    //
    setId("massAction::TwoToOne");
    setParameter(paraValue);  
    setVariableIndex(indValue);
    //
    // Set the parameter identities
    //
    std::vector<std::string> tmp( numParameter() );
    tmp.resize( numParameter() );
    tmp[0] = "k_f";
    setParameterId( tmp );
  }
  
  void TwoToOne::
  derivs(Compartment &compartment,size_t varIndex,DataMatrix &y,DataMatrix &dydt) 
  {  
    double rate = parameter(0)*y[compartment.index()][variableIndex(0,0)]
      *y[compartment.index()][variableIndex(0,1)];
    if (rate<=0.0) //No update
      return;
    dydt[compartment.index()][variableIndex(0,0)] -= rate;
    dydt[compartment.index()][variableIndex(0,1)] -= rate;
    dydt[compartment.index()][variableIndex(1,0)] += rate;
  }

  void TwoToOne::
  derivsWithAbs(Compartment &compartment,size_t varIndex,DataMatrix &y,DataMatrix &dydt, DataMatrix &sdydt) 
  {  
    double rate = parameter(0)*y[compartment.index()][variableIndex(0,0)]
      *y[compartment.index()][variableIndex(0,1)];
    if (rate<=0.0) //No update
      return;
    dydt[compartment.index()][variableIndex(0,0)] -= rate;
    dydt[compartment.index()][variableIndex(0,1)] -= rate;
    dydt[compartment.index()][variableIndex(1,0)] += rate;
    sdydt[compartment.index()][variableIndex(0,0)] += rate;
    sdydt[compartment.index()][variableIndex(0,1)] += rate;
    sdydt[compartment.index()][variableIndex(1,0)] += rate;
  }
  
  size_t TwoToOne::
  Jacobian(Compartment &compartment,size_t varIndex,DataMatrix &y,JacobianMatrix &A) 
  {  
    double rate = parameter(0);
    
    if (rate<=0.0) //No update
    return 1;
    //
    // Update all A
    //
    // Reactants
    size_t j = compartment.index()*y[0].size() + variableIndex(0,0); 
    size_t jj = compartment.index()*y[0].size() + variableIndex(0,1); 
    // product
    size_t i = compartment.index()*y[0].size() + variableIndex(1,0); 
    double element = rate*y[compartment.index()][variableIndex(0,1)];
    A(j,j) -= element;
    A(jj,j) -= element;
    A(i,j) += element;
    element = rate*y[compartment.index()][variableIndex(0,0)];
    A(jj,jj) -= element;
    A(j,jj) -= element;
    A(i,jj) += element;
    return 1;
  }
  
  double TwoToOne::
  propensity(Compartment &compartment,size_t varIndex,DataMatrix &y)
  {
    double rate = parameter(0)*y[compartment.index()][variableIndex(0,0)];
    if (variableIndex(0,0)!=variableIndex(0,1))
      rate *= y[compartment.index()][variableIndex(0,1)];
    else
      rate *= 0.5*(y[compartment.index()][variableIndex(0,1)]-1.0);
    if (rate<=0.0) //No update
      return 0.0;
    return rate;
  }
  
  void TwoToOne::
  discreteUpdate(Compartment &compartment,size_t varIndex,DataMatrix &y)
  {
    y[compartment.index()][variableIndex(0,0)] -= 1.0;
    y[compartment.index()][variableIndex(0,1)] -= 1.0;
    y[compartment.index()][variableIndex(1,0)] += 1.0;
  }
  
  ThreeToOne::
  ThreeToOne(std::vector<double> &paraValue, 
	     std::vector< std::vector<size_t> > &indValue ) 
  {  
    //
    // Do some checks on the parameters and variable indeces
    //
    if( paraValue.size()!=1 ) {
      std::cerr << "MassAction::ThreeToOne::ThreeToOne() "
		<< "Uses only one parameter k_f\n";
      exit(0);
    }
    if( indValue.size() !=2 || indValue[0].size() !=3 
	|| indValue[1].size() !=1) {
      std::cerr << "MassAction::ThreeToOne::ThreeToOne() "
		<< "Two levels of variable indeces are used.\n"
		<< "One for three reactants and one for a single product.\n";
      exit(0);
    }
    //
    // Set the variable values
    //
    setId("massAction::ThreeToOne");
    setParameter(paraValue);  
    setVariableIndex(indValue);
    //
    // Set the parameter identities
    //
    std::vector<std::string> tmp( numParameter() );
    tmp.resize( numParameter() );
    tmp[0] = "k_f";
    setParameterId( tmp );
  }
  
  void ThreeToOne::
  derivs(Compartment &compartment,size_t varIndex,DataMatrix &y,DataMatrix &dydt) 
  {  
    double rate = parameter(0)*y[compartment.index()][variableIndex(0,0)]
      *y[compartment.index()][variableIndex(0,1)]
      *y[compartment.index()][variableIndex(0,2)];
    if (rate<=0.0) //No update
      return;
    dydt[compartment.index()][variableIndex(0,0)] -= rate;
    dydt[compartment.index()][variableIndex(0,1)] -= rate;
    dydt[compartment.index()][variableIndex(0,2)] -= rate;
    dydt[compartment.index()][variableIndex(1,0)] += rate;
  }

  void ThreeToOne::
  derivsWithAbs(Compartment &compartment,size_t varIndex,DataMatrix &y,DataMatrix &dydt, DataMatrix &sdydt) 
  {  
    double rate = parameter(0)*y[compartment.index()][variableIndex(0,0)]
      *y[compartment.index()][variableIndex(0,1)]
      *y[compartment.index()][variableIndex(0,2)];
    if (rate<=0.0) //No update
      return;
    dydt[compartment.index()][variableIndex(0,0)] -= rate;
    dydt[compartment.index()][variableIndex(0,1)] -= rate;
    dydt[compartment.index()][variableIndex(0,2)] -= rate;
    dydt[compartment.index()][variableIndex(1,0)] += rate;
    sdydt[compartment.index()][variableIndex(0,0)] += rate;
    sdydt[compartment.index()][variableIndex(0,1)] += rate;
    sdydt[compartment.index()][variableIndex(0,2)] += rate;
    sdydt[compartment.index()][variableIndex(1,0)] += rate;
  }
  
  size_t ThreeToOne::
  Jacobian(Compartment &compartment,size_t varIndex,DataMatrix &y,JacobianMatrix &A) 
  {  
    double rate = parameter(0);
    
    if (rate<=0.0) //No update
      return 1;
    //
    // Update all A
    //
    // Reactants
    size_t j = compartment.index()*y[0].size() + variableIndex(0,0); 
    size_t jj = compartment.index()*y[0].size() + variableIndex(0,1); 
    size_t jjj = compartment.index()*y[0].size() + variableIndex(0,2); 
    // product
    size_t i = compartment.index()*y[0].size() + variableIndex(1,0); 
    double element = rate*y[compartment.index()][variableIndex(0,1)]*
      y[compartment.index()][variableIndex(0,2)];
    A(j,j) -= element;
    A(jj,j) -= element;
    A(jjj,j) -= element;
    A(i,j) += element;
    element = rate*y[compartment.index()][variableIndex(0,0)]*
      y[compartment.index()][variableIndex(0,2)];
    A(j,jj) -= element;
    A(jj,jj) -= element;
    A(jjj,jj) -= element;
    A(i,jj) += element;
    element = rate*y[compartment.index()][variableIndex(0,0)]*
      y[compartment.index()][variableIndex(0,1)];
    A(j,jjj) -= element;
    A(jj,jjj) -= element;
    A(jjj,jjj) -= element;
    A(i,jjj) += element;
    return 1;
  }
  
  double ThreeToOne::
  propensity(Compartment &compartment,size_t varIndex,DataMatrix &y)
  {
    double rate = parameter(0)*y[compartment.index()][variableIndex(0,0)];
    if (variableIndex(0,0)!=variableIndex(0,1)) {// 0 != 1
      rate *= y[compartment.index()][variableIndex(0,1)];
      if (variableIndex(0,0)!=variableIndex(0,2) &&
	  variableIndex(0,1)!=variableIndex(0,2) ) {// 0 != 1 != 2
	rate *= y[compartment.index()][variableIndex(0,2)];
      }
      else {// 0 != 1 && (2=0 || 2=1)
	rate *= 0.5*(y[compartment.index()][variableIndex(0,2)]-1.0);
      }
    }
    else {// 0 = 1
      rate *= 0.5*(y[compartment.index()][variableIndex(0,1)]-1.0);
      if (variableIndex(0,0)!=variableIndex(0,2)) { // 0 = 1 && 2 != 0 (&& 2 != 1)
	rate *= y[compartment.index()][variableIndex(0,2)];
      }
      else {//0=1=2
	rate *= (y[compartment.index()][variableIndex(0,2)]-2.0)/3.0;
      }
    }
    if (rate<=0.0) //No update
      return 0.0;
    return rate;
  }
  
  void ThreeToOne::
  discreteUpdate(Compartment &compartment,size_t varIndex,DataMatrix &y)
  {
    y[compartment.index()][variableIndex(0,0)] -= 1.0;
    y[compartment.index()][variableIndex(0,1)] -= 1.0;
    y[compartment.index()][variableIndex(0,2)] -= 1.0;
    y[compartment.index()][variableIndex(1,0)] += 1.0;
  }
  
  Enzymatic::
  Enzymatic(std::vector<double> &paraValue, 
	    std::vector< std::vector<size_t> > 
	    &indValue )
  {  
    // Do some checks on the parameters and variable indeces
    //
    if( paraValue.size()!=1 ) {
      std::cerr << "MassAction::Enzymatic::Enzymatic() "
		<< "Uses only one parameter k_f\n";
      exit(0);
    }
    if( indValue.size() !=3 ) {
      std::cerr << "MassAction::Enzymatic::Enzymatic() "
		<< "Three levels of variable indices are used.\n"
		<< "One for reactants, one for products and one for enzymes\n";
      exit(0);
    }
    if( indValue[0].size()<1 ) {
      std::cerr << "MassAction::Enzymatic::Enzymatic() "
		<< "If no reactants given, there will be no reaction...\n";
      exit(0);
    }
    if( indValue[2].size()<1 ) {
      std::cerr << "MassAction::Enzymatic::Enzymatic() "
		<< "If no enzymes given, there will be no reaction...\n";
      exit(0);
    }
    
    // Set the variable values
    //
    setId("massAction::enzymatic");
    setParameter(paraValue);  
    setVariableIndex(indValue);
    
    // Set the parameter identities
    //
    std::vector<std::string> tmp( numParameter() );
    tmp.resize( numParameter() );
    tmp[0] = "k_f";
    setParameterId( tmp );
  }
  
  void Enzymatic::
  derivs(Compartment &compartment,size_t varIndex,DataMatrix &y,DataMatrix &dydt) 
  {  
    double rate = parameter(0);
    if( numVariableIndex(0) )
      for( size_t i=0 ; i< numVariableIndex(0) ; i++ )
	rate *= y[compartment.index()][variableIndex(0,i)];
    else //No reaction defined...
      return;
    
    if( numVariableIndex(2) )
      for( size_t i=0 ; i< numVariableIndex(2) ; i++ )
	rate *= y[compartment.index()][variableIndex(2,i)];
    else //No reaction defined...
      return;
    
    if (rate<=0.0) //No update
      return;
    
    for( size_t i=0 ; i< numVariableIndex(0) ; i++ )
      dydt[compartment.index()][variableIndex(0,i)] -= rate;
    
    for( size_t i=0 ; i< numVariableIndex(1) ; i++ )
      dydt[compartment.index()][variableIndex(1,i)] += rate;
  }

  void Enzymatic::
  derivsWithAbs(Compartment &compartment,size_t varIndex,DataMatrix &y,DataMatrix &dydt, DataMatrix &sdydt) 
  {  
    double rate = parameter(0);
    if( numVariableIndex(0) )
      for( size_t i=0 ; i< numVariableIndex(0) ; i++ )
	rate *= y[compartment.index()][variableIndex(0,i)];
    else //No reaction defined...
      return;
    
    if( numVariableIndex(2) )
      for( size_t i=0 ; i< numVariableIndex(2) ; i++ )
	rate *= y[compartment.index()][variableIndex(2,i)];
    else //No reaction defined...
      return;
    
    if (rate<=0.0) //No update
      return;
    
    for( size_t i=0 ; i< numVariableIndex(0) ; i++ ) {
      dydt[compartment.index()][variableIndex(0,i)] -= rate;
      sdydt[compartment.index()][variableIndex(0,i)] += rate;
    }
    
    for( size_t i=0 ; i< numVariableIndex(1) ; i++ ) {
      dydt[compartment.index()][variableIndex(1,i)] += rate;
      sdydt[compartment.index()][variableIndex(1,i)] += rate;
    }
  }
  
  size_t Enzymatic::
  Jacobian(Compartment &compartment,size_t varIndex,DataMatrix &y,JacobianMatrix &A) 
  {  
    double rate = parameter(0);
    if( numVariableIndex(0) )
      for (size_t k=0; k<numVariableIndex(0); ++k)
	rate *= y[compartment.index()][variableIndex(0,k)];
    else //No reaction defined...
      return 0;
    if( numVariableIndex(2) )
      for (size_t k=0 ; k<numVariableIndex(2); ++k)
	rate *= y[compartment.index()][variableIndex(2,k)];
    else //No reaction defined...
      return 0;
    if (rate<=0.0) //No update
      return 1;
    //
    // Update all A
    //
    // Dependence on reactants
    for (size_t k=0; k<numVariableIndex(0); ++k ) {
      double element = rate / y[compartment.index()][variableIndex(0,k)];
      size_t j = compartment.index()*y[0].size() + variableIndex(0,k); 
      // Reactants
      for (size_t kk=0; kk<numVariableIndex(0); ++kk ) {
	size_t i = compartment.index()*y[0].size() + variableIndex(0,kk); 
	A(i,j) -= element;
      }
      // products
      for (size_t kk=0; kk<numVariableIndex(1); ++kk ) {
	size_t i = compartment.index()*y[0].size() + variableIndex(1,kk); 
	A(i,j) += element;
      }
    }
    // Dependence on enzymes
    for (size_t k=0; k<numVariableIndex(2); ++k ) {
      double element = rate / y[compartment.index()][variableIndex(2,k)];
      size_t j = compartment.index()*y[0].size() + variableIndex(2,k); 
      // Reactants
      for (size_t kk=0; kk<numVariableIndex(0); ++kk ) {
	size_t i = compartment.index()*y[0].size() + variableIndex(0,kk); 
	A(i,j) -= element;
      }
      // products
      for (size_t kk=0; kk<numVariableIndex(1); ++kk ) {
	size_t i = compartment.index()*y[0].size() + variableIndex(1,kk); 
	A(i,j) += element;
      }
    }
    return 1;
  }

  void Enzymatic::printCambium( std::ostream &os, size_t varIndex ) const
  {
    std::string varName=organism()->variableId(varIndex);
    std::string varNameCre=organism()->variableId(variableIndex(0,0));
    os << "Arrow[Organism[" << id() << "],{";
    //reactants
    for (size_t i=0; i<numVariableIndex(0); i++) {
      if (i!=0) {
	os << ",";
      }
      std::string varNameCre=organism()->variableId(variableIndex(0,i));
      os << varNameCre << "[i]";
    }
    os << "},{";
    //Enzymes
    for (size_t i=0; i<numVariableIndex(2); i++) {
      if (i!=0) {
	os << ",";
      }
      std::string varNameCre=organism()->variableId(variableIndex(2,i));
      os << varNameCre << "[i]";
    }
    os << "},{";
    // Products
    for (size_t i=0; i<numVariableIndex(1); i++) {
      if (i!=0) {
	os << ",";
      }
      std::string varNameCre=organism()->variableId(variableIndex(2,i));
      os << varNameCre << "[i]";
    }
    os << "},Parameters[";
    os << parameter(0);
    os << "], ParameterNames[" << parameterId(0) << "], VarIndices[{";
    //reactants
    for (size_t i=0; i<numVariableIndex(0); i++) {
      if (i!=0) {
	os << " ";
      }
      os << variableIndex(0,i);
    }
    os << "}{";
    //products
    for (size_t i=0; i<numVariableIndex(1); i++) {
      if (i!=0) {
	os << " ";
      }
      os << variableIndex(1,i);
    }
    os << "}{";
    //enzymes
    for (size_t i=0; i<numVariableIndex(2); i++) {
      if (i!=0) {
	os << " ";
      }
      os << variableIndex(2,i);
    }
    os << "}],"; 
    os << "Solve{";
    //reactants
    for (size_t i=0; i<numVariableIndex(0); i++) {
      std::string varNameCre=organism()->variableId(variableIndex(0,i));
      os << varNameCre << "[i]\' = ";
    }
    //products
    for (size_t i=0; i<numVariableIndex(1); i++) {
      std::string varNameCre=organism()->variableId(variableIndex(0,i));
      os << "-" << varNameCre << "[i]\' = ";
    }
    os << "p_0 ";
    //reactants
    for (size_t i=0; i<numVariableIndex(0); i++) {
      std::string varNameCre=organism()->variableId(variableIndex(0,i));
      os << varNameCre << "[i] ";
    }
    //Enzymes
    for (size_t i=0; i<numVariableIndex(2); i++) {
      std::string varNameCre=organism()->variableId(variableIndex(2,i));
      os << varNameCre << "[i]";
    }
    os << ", "
       << "Using[" << organism()->topology().id() << "::Cell, i]}";
    os << "]" << std::endl;
  }

  MM::
  MM(std::vector<double> &paraValue, 
     std::vector< std::vector<size_t> > &indValue )
  {  
    // Do some checks on the parameters and variable indeces
    //
    if( paraValue.size()!=2 ) {
      std::cerr << "MassAction::MM::MM() "
		<< "Uses two parameters V_max and K\n";
      exit(0);
    }
    if( indValue.size() !=2 ) {
      std::cerr << "MassAction::MM::MM() "
		<< "Two levels of variable indeces are used.\n"
		<< "One for reactant and one for product\n";
      exit(0);
    }
    if( indValue[0].size() !=1 || indValue[1].size() !=1 ) {
      std::cerr << "MassAction::MM::MM() "
		<< "This reaction needs one reactant and one product.\n";
      exit(0);
    }
    // Set the variable values
    //
    setId("massAction::MM");
    setParameter(paraValue);  
    setVariableIndex(indValue);
    
    // Set the parameter identities
    //
    std::vector<std::string> tmp( numParameter() );
    tmp.resize( numParameter() );
    tmp[0] = "V_max";
    tmp[1] = "K";
    setParameterId( tmp );
  }

  void MM::
  derivs(Compartment &compartment,size_t varIndex,DataMatrix &y,DataMatrix &dydt) 
  {  
    double rate = parameter(0)*y[compartment.index()][variableIndex(0,0)]
      / ( parameter(1) + y[compartment.index()][variableIndex(0,0)] );
    
    dydt[compartment.index()][variableIndex(0,0)] -= rate;  
    dydt[compartment.index()][variableIndex(1,0)] += rate;
  }
  
  EnzymaticMM::
  EnzymaticMM(std::vector<double> &paraValue,
	      std::vector< std::vector<size_t> > &indValue )
  {    
    // Do some checks on the parameters and variable indeces
    //
    if( paraValue.size()!=2 ) {
      std::cerr << "MassAction::EnzymaticMM::EnzymaticMM() "
		<< "Uses two parameters V_max and K\n";
      exit(0);
    }
    if( indValue.size() !=3 ) {
      std::cerr << "MassAction::EnzymaticMM::EnzymaticMM() "
		<< "Three levels of variable indeces are used.\n"
		<< "One for reactant, one for product and one for the enzyme\n";
      exit(0);
    }
    if( indValue[0].size() !=1 || indValue[1].size() !=1 || indValue[2].size() !=1 ) {
      std::cerr << "MassAction::EnzymaticMM::EnzymaticMM() "
		<< "This reaction needs one reactant one product and one enzyme\n";
      exit(0);
    }
    // Set the variable values
    //
    setId("massAction::EnzymaticMM");
    setParameter(paraValue);
    setVariableIndex(indValue);
    
    // Set the parameter identities
    //
    std::vector<std::string> tmp( numParameter() );
    tmp.resize( numParameter() );
    tmp[0] = "V_max";
    tmp[1] = "K";
    setParameterId( tmp );
  }
  
  void EnzymaticMM::
  derivs(Compartment &compartment,size_t varIndex,DataMatrix &y,DataMatrix &dydt) 
  {
    double rate = parameter(0)*y[compartment.index()][variableIndex(0,0)]
      / ( parameter(1) + y[compartment.index()][variableIndex(0,0)] );
    
    rate *= y[compartment.index()][variableIndex(2,0)];
    
    dydt[compartment.index()][variableIndex(0,0)] -= rate;
    dydt[compartment.index()][variableIndex(1,0)] += rate;
  }
  
  MMFraction::
  MMFraction(std::vector<double> &paraValue,
	     std::vector< std::vector<size_t> > &indValue )
  {    
    // Do some checks on the parameters and variable indeces
    //
    if( paraValue.size()!=3 ) {
      std::cerr << "MassAction::MMFraction::MMFraction() "
		<< "Uses three parameters V_max, K and fraction\n";
      exit(0);
    }
    if( indValue.size() !=2 ) {
      std::cerr << "MassAction::MMFraction::MMFraction() "
		<< "Two levels of variable indeces are used.\n"
		<< "One for reactant and one for product\n";
      exit(0);
    }
    if( indValue[0].size() !=1 || indValue[1].size() ) {
      std::cerr << "MassAction::MMFraction::MMFraction() "
		<< "This reaction needs one reactant and one product.\n";
      exit(0);
    }
    // Set the variable values
    //
    setId("massAction::MMFraction");
    setParameter(paraValue);
    setVariableIndex(indValue);
    
    // Set the parameter identities
    //
    std::vector<std::string> tmp( numParameter() );
    tmp.resize( numParameter() );
    tmp[0] = "V_max";
    tmp[1] = "K";
    tmp[2] = "f";
    setParameterId( tmp );
  }

  void MMFraction::
  derivs(Compartment &compartment,size_t varIndex,DataMatrix &y,DataMatrix &dydt) 
  {
    double rate = parameter(0)*parameter(2)*y[compartment.index()][variableIndex(0,0)]
      / ( parameter(1) + parameter(2)*y[compartment.index()][variableIndex(0,0)] );
    
    dydt[compartment.index()][variableIndex(0,0)] -= rate;
    dydt[compartment.index()][variableIndex(1,0)] += rate;
  }

  EnzymaticHill::
  EnzymaticHill(std::vector<double> &paraValue,
	      std::vector< std::vector<size_t> > &indValue )
  {    


  // Do some checks on the parameters and variable indeces
  //
    if( indValue.size() != 4 ) {
      std::cerr << "MassAction::EnxymaticHill() "
		<< "Four levels of variable index is used, "
		<< "reactant, product, activators and repressors.\n";
      exit(0);
    }
  if( 1 + 2*(indValue[2].size()+indValue[3].size()) != paraValue.size() ) {
    std::cerr << "Hill::Hill() "
	      << "Number of parameters does not agree with number of "
	      << "activators/repressors.\n"
	      << indValue[2].size() << " activators, " 
	      << indValue[3].size() << " repressors, and "
	      << paraValue.size() << " parameters\n"
 	      << "One plus pairs of parameters (V_max,K_half1,n_Hill1,"
 	      << "K2,n2,...) must be given.\n";
    exit(0);
  }
    // Set the variable values
    //
    setId("massAction::EnzymaticHill");
    setParameter(paraValue);
    setVariableIndex(indValue);
    
    // Set the parameter identities
    //
    std::vector<std::string> tmp( numParameter() );
    tmp.resize( numParameter() );
    tmp[0] = "V_max";
    tmp[1] = "K";
    tmp[2] = "n";
    setParameterId( tmp );
  }
  
  void EnzymaticHill::
  derivs(Compartment &compartment,size_t varIndex,DataMatrix &y,DataMatrix &dydt) 
  {

    double contribution=parameter(0);
    size_t parameterIndex=1;
    // Activator contributions
    for( size_t i=0 ; i<numVariableIndex(2) ; i++ ) {
      double c = std::pow(y[compartment.index()][variableIndex(2,i)],
			  parameter(parameterIndex+1));
      contribution *= c
	/ ( std::pow(parameter(parameterIndex),parameter(parameterIndex+1)) + c );
      parameterIndex+=2;
    }
    // Repressor contributions
    for( size_t i=0 ; i<numVariableIndex(3) ; i++ ) {
      double c = std::pow(parameter(parameterIndex),parameter(parameterIndex+1));
      contribution *= c /
	( c + std::pow(y[compartment.index()][variableIndex(3,i)],
		       parameter(parameterIndex+1)) );
      parameterIndex+=2;
    }   

    contribution *= y[compartment.index()][variableIndex(0,0)]; // value of reactant
    
    dydt[compartment.index()][variableIndex(0,0)] -= contribution;
    dydt[compartment.index()][variableIndex(1,0)] += contribution;
  }


} //end namespace MassAction
