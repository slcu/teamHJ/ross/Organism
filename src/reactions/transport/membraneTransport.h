//
// Filename     : membraneTransport.h
// Description  : Classes describing passive and active transports of molecules across membrane
// Author(s)    : Henrik Jonsson (henrik@thep.lu.se)
// Created      : December 2007
// Revision     : $Id: transport.h 203 2007-03-20 10:50:07Z henrik $
//
#ifndef MEMBRANETRANSPORT_H
#define MEMBRANETRANSPORT_H

#include<cmath>

#include"../baseReaction.h"

///
/// @brief The class ActiveTransportRestricted moves molecules between cells and walls activly
///
/// Uses T_out*A_ij*X_i for out and T_in*A_ij*X_ij for transport between and
/// compartment and neighbor. (out means comp to neigh, in neigh to comp). X
/// is the transported molecule, A is a helper molecule (in neighbor) acting
/// as in/efflux carrier.
///
class ActiveTransportRestricted : public BaseReaction {
  
 public:
  
  ActiveTransportRestricted(std::vector<double> &paraValue, 
			    std::vector< std::vector<size_t> > &indValue );
  
  void derivs(Compartment &compartment,size_t species,
	      std::vector< std::vector<double> > &y,
	      std::vector< std::vector<double> > &dydt );
};

///
/// @brief The class ActiveTransportMMRestricted moves molecules between cells
/// and walls activly.
///
/// Uses 
/// 
/// @f[ Tmax_out*A_{ij}/(k_out+A_ij)*X_i @f]
///
/// for out and
///
/// @f[ Tmax_in*A_{ij}/(k_in+A_{ij})*X_{ij} @f]
///
/// for transport between a comp and neighbor. (out means comp to neighbor, in
/// is neighbor to compartment). X is the transported molecule, A is a helper
/// acting as in/efflux carrier.
///
class ActiveTransportMMRestricted : public BaseReaction {
  
 public:
  
    ActiveTransportMMRestricted(std::vector<double> &paraValue, 
				std::vector< std::vector<size_t> > &indValue );
    
    void derivs(Compartment &compartment,size_t species,
		std::vector< std::vector<double> > &y,
		std::vector< std::vector<double> > &dydt );
};

///
/// @brief The ActiveTransportBRestricted class.
///
/// Uses T_out*A_ij*X_i for out and T_in*A_ij*X_ij for transport between and
/// compartment and neighbor. (out means comp to neigh, in is neigh to
/// comp). X is the transported molecule, A is a helper molecule promoting
/// in/efflux. The difference from ActiveTransportRestricted() is that the
/// flux depends on concentration in a compartment defined in
/// compartment.parameter.
///
class ActiveTransportBRestricted : public BaseReaction {
  
 public:
  
    ActiveTransportBRestricted(std::vector<double> &paraValue, 
			       std::vector< std::vector<size_t> > &indValue );
    
    void derivs(Compartment &compartment,size_t species,
		std::vector< std::vector<double> > &y,
		std::vector< std::vector<double> > &dydt );
};

//!The class membraneTransport is the simplest membrane transport update
class MembraneTransport : public BaseReaction {
  
 public:
  
  MembraneTransport(std::vector<double> &paraValue, 
		    std::vector< std::vector<size_t> > &indValue );
  
  void derivs(Compartment &compartment,size_t species,
	      std::vector< std::vector<double> > &y,
	      std::vector< std::vector<double> > &dydt );
};

//!The class MembraneTransportRestricted is between specific compartments
class MembraneTransportRestricted : public BaseReaction {
  
 public:
  
  MembraneTransportRestricted(std::vector<double> &paraValue, 
			      std::vector< std::vector<size_t> > &indValue );
  
  void derivs(Compartment &compartment,size_t species,
	      std::vector< std::vector<double> > &y,
	      std::vector< std::vector<double> > &dydt );
};

//!The class ActiveMembraneTransportRestricted is active membrane transport
class ActiveMembraneTransportRestricted : public BaseReaction {
  
 public:
  
  ActiveMembraneTransportRestricted(std::vector<double> &paraValue, 
				    std::vector< std::vector<size_t> > 
				    &indValue );
  
  void derivs(Compartment &compartment,size_t species,
	      std::vector< std::vector<double> > &y,
	      std::vector< std::vector<double> > &dydt );
};

//!The class ActiveMembraneTransportRestrictedMM is active membrane transport
class ActiveMembraneTransportRestrictedMM : public BaseReaction {
  
 public:
  
  ActiveMembraneTransportRestrictedMM(std::vector<double> &paraValue, 
				    std::vector< std::vector<size_t> > 
				    &indValue );
  
  void derivs(Compartment &compartment,size_t species,
	      std::vector< std::vector<double> > &y,
	      std::vector< std::vector<double> > &dydt );
};

//!The class CellWallTransportB moves molecules between cells and walls activly
class ActiveMembraneTransportBRestricted : public BaseReaction {
  
 public:
  
  ActiveMembraneTransportBRestricted(std::vector<double> &paraValue, 
				     std::vector< std::vector<size_t> > 
				     &indValue );
  
  void derivs(Compartment &compartment,size_t species,
	      std::vector< std::vector<double> > &y,
	      std::vector< std::vector<double> > &dydt );
};

//!The class CellWallTransportB moves molecules between cells and walls activly
class ActiveMembraneTransportBHillRestricted : public BaseReaction {
  
 public:
  
  ActiveMembraneTransportBHillRestricted(std::vector<double> &paraValue, 
					 std::vector< std::vector<size_t> > 
					 &indValue );
  
  void derivs(Compartment &compartment,size_t species,
	      std::vector< std::vector<double> > &y,
	      std::vector< std::vector<double> > &dydt );
};

///
/// @brief The class ActiveTransportVariable moves molecules between cells and walls activly
///
/// Uses T*A_ij*X_i for out and T*A_ji*X_j for transport between cell
/// compartment and neighbor. X is the transported molecule, A is a helper
/// molecule acting as efflux carrier and is stored in the index at second level.
///
/// @note This only works if the number of neighbors is less than the provided
/// helper concentrations.
///
class ActiveTransportVariableSimple : public BaseReaction {
  
 public:
  
  ActiveTransportVariableSimple(std::vector<double> &paraValue, 
																std::vector< std::vector<size_t> > &indValue );
  
  void derivs(Compartment &compartment,size_t species,
							std::vector< std::vector<double> > &y,
							std::vector< std::vector<double> > &dydt );
};


#endif
