//
// Filename     : membraneTransport.cc
// Description  : Classes describing updates due to molecule transports across membranes
// Author(s)    : Henrik Jonsson (henrik@thep.lu.se)
// Created      : December 2007
// Revision     : $Id: transport.cc 324 2007-12-07 15:44:17Z pontus $
//
#include"membraneTransport.h"
#include"../baseReaction.h"

ActiveTransportRestricted::ActiveTransportRestricted(std::vector<double>
						     &paraValue, 
						     std::vector< 
						     std::vector<size_t> > 
						     &indValue ) {
  
  //Do some checks on the parameters and variable indeces
  //////////////////////////////////////////////////////////////////////
  if( paraValue.size()!=2 ) {
    std::cerr << "ActiveTransportRestricted::ActiveTransportRestricted() "
	      << "Uses two parameters T_out T_in.\n";
    exit(0);
  }
  if( indValue.size() != 2 || indValue[0].size() !=3
      || indValue[1].size() !=1 ) {
    std::cerr << "ActiveTransportRestricted::ActiveTransportRestricted() "
	      << "Variable index for helper(in neighbor), compartment"
	      << " and neighbor markers, and size (2nd level) needed\n";
    exit(0);
  }
  //Set the variable values
  //////////////////////////////////////////////////////////////////////
  setId("activeTransportRestricted");
  setParameter(paraValue);  
  setVariableIndex(indValue);
  
  //Set the parameter identities
  //////////////////////////////////////////////////////////////////////
  std::vector<std::string> tmp( numParameter() );
  tmp.resize( numParameter() );
  tmp[0] = "T_out";
  tmp[1] = "T_in";
  setParameterId( tmp );
}

void ActiveTransportRestricted::derivs(Compartment &compartment,size_t species,
				       std::vector< std::vector<double> > &y,
				       std::vector< std::vector<double> > 
				       &dydt ) {
  
  //double volume,PI=3.1415;
  size_t i=compartment.index();
  double Vi = y[i][variableIndex(1,0)];//Volume of the compartment
  if( Vi<=0.0 ) {
    std::cerr << "ActiveTransportRestricted::derivs Given volume " << Vi
	      << " unphysical.\n";
    exit(-1);
  }

  //Do this update from compartmentMarked cells (not neighborMarked)
  if( y[i][variableIndex(0,1)] < 0.5 ) {
    return;
  }
  
  for( size_t n=0 ; n<compartment.numNeighbor() ; n++ ) {
    size_t j=compartment.neighbor(n);
    if( y[j][variableIndex(0,2)] > 0.5 ) {//Only when neighbor is marked
      double Vj = y[j][variableIndex(1,0)];//Volume of the compartment
      if( Vj<=0.0 ) {
	std::cerr << "ActiveTransportRestricted::derivs Given volume " << Vj
		  << " unphysical.\n";
	exit(-1);
      }
      double compConc = y[i][species];
      double neighConc = y[j][species];
      size_t helperCol=variableIndex(0,0);
      //If no neighbor area defined assume area and distance 1
      double distance = 1.0,area=1.0;
      if( compartment.numNeighborArea()>n ) {
	area = compartment.neighborArea(n);
	distance=0.0;
	for( size_t d=compartment.topologyStart() ; d<compartment.numDimension() 
	       ; d++ )
	  distance += (y[j][d]-y[i][d])*(y[j][d]-y[i][d]);
	if( distance<=0 ) {
	  std::cerr << "ActiveTransportRestricted::derivs Calculated "
		    << "distance " << distance << " unphysical.\n";
	  exit(-1);
	}
	distance = sqrt(distance);
      }
      //T_out
      double tmp = parameter(0)*compConc*y[j][helperCol]*area/distance;
      dydt[i][species] -= tmp/Vi;
      dydt[j][species] += tmp/Vj;
      //T_in
      tmp = parameter(1)*neighConc*y[j][helperCol]*area/distance;
      dydt[i][species] += tmp/Vi;
      dydt[j][species] -= tmp/Vj;
    }
  }
}

ActiveTransportMMRestricted::ActiveTransportMMRestricted(std::vector<double>
							 &paraValue, 
							 std::vector< 
							 std::vector<size_t> > 
							 &indValue ) {
  
  //Do some checks on the parameters and variable indeces
  //////////////////////////////////////////////////////////////////////
  if( paraValue.size()!=4 ) {
    std::cerr << "ActiveTransportMMRestricted::ActiveTransportMMRestricted() "
	      << "Uses four parameters Tmax_out k_out Tmax_in "
	      << "k_in.\n";
    exit(0);
  }
  if( indValue.size() != 2 || indValue[0].size() !=3
      || indValue[1].size() !=1 ) {
    std::cerr << "ActiveTransportMMRestricted::ActiveTransportMMRestricted() "
	      << "Variable index for helper, compartment and "
	      << "neighbor markers, and size (2nd level) needed.\n";
    exit(0);
  }
  //Set the variable values
  //////////////////////////////////////////////////////////////////////
  setId("activeTransportMMRestricted");
  setParameter(paraValue);  
  setVariableIndex(indValue);
  
  //Set the parameter identities
  //////////////////////////////////////////////////////////////////////
  std::vector<std::string> tmp( numParameter() );
  tmp.resize( numParameter() );
  tmp[0] = "Tmax_out";
  tmp[1] = "k_out";
  tmp[2] = "Tmax_in";
  tmp[3] = "k_in";
  setParameterId( tmp );
}

void ActiveTransportMMRestricted::derivs(Compartment &compartment,size_t species,
					 std::vector< std::vector<double> > &y,
					 std::vector< std::vector<double> > 
					 &dydt ) {
  
  size_t i=compartment.index();
  double Vi = y[i][variableIndex(1,0)];//Volume of the compartment
  if( Vi<=0.0 ) {
    std::cerr << "ActiveTransportMMRestricted::derivs Given volume " << Vi
	      << " unphysical.\n";
    exit(-1);
  }
  //Do this update from marked compartments only
  if( y[i][variableIndex(0,1)] < 0.5 ) {
    return;
  }
  
  for( size_t n=0 ; n<compartment.numNeighbor() ; n++ ) {
    size_t j=compartment.neighbor(n);
    if( y[j][variableIndex(0,2)] > 0.5 ) {//Only when neighbor is marked
      
      double Vj = y[j][variableIndex(1,0)];//Volume of the compartment
      if( Vj<=0.0 ) {
	std::cerr << "ActiveTransportMMRestricted::derivs Given volume " << Vj
		  << " unphysical.\n";
	exit(-1);
      }
      double cellConc = y[i][species];
      double wallConc = y[j][species];
      
      //Tmax_out,k_out
      double tmp = parameter(0)*cellConc*y[j][variableIndex(0,0)]/
	(parameter(1)+y[j][variableIndex(0,0)]);
      dydt[i][species] -= tmp/Vi;
      dydt[j][species] += tmp/Vj;
      //Tmax_in,k_in
      tmp = parameter(2)*wallConc*y[j][variableIndex(0,0)]/
	(parameter(3)+y[i][variableIndex(0,0)]);
      dydt[i][species] += tmp/Vi;
      dydt[j][species] -= tmp/Vj;
    }
  }
}

ActiveTransportBRestricted::ActiveTransportBRestricted(std::vector<double>
						       &paraValue, 
						       std::vector< 
						       std::vector<size_t> > 
						       &indValue ) {
  
  //Do some checks on the parameters and variable indeces
  //////////////////////////////////////////////////////////////////////
  if( paraValue.size()!=2 ) {
    std::cerr << "ActiveTransportBRestricted::ActiveTransportBRestricted() "
	      << "Uses two parameters T_out T_in.\n";
    exit(0);
  }
  if( indValue.size() != 2 || indValue[0].size() !=3
      || indValue[1].size() !=1 ) {
    std::cerr << "ActiveTransportBRestricted::ActiveTransportBRestricted() "
	      << "Variable index for helper, compMarker"
	      << " and neighMarker, and size (2nd level) needed.\n";
    exit(0);
  }
  //Set the variable values
  //////////////////////////////////////////////////////////////////////
  setId("activeTransportBRestricted");
  setParameter(paraValue);  
  setVariableIndex(indValue);
  
  //Set the parameter identities
  //////////////////////////////////////////////////////////////////////
  std::vector<std::string> tmp( numParameter() );
  tmp.resize( numParameter() );
  tmp[0] = "T_out";
  tmp[1] = "T_in";
  setParameterId( tmp );
}

void ActiveTransportBRestricted::derivs(Compartment &compartment,size_t species,
					std::vector< std::vector<double> > &y,
					std::vector< std::vector<double> > 
					&dydt ) {
    
  //double volume,PI=3.1415;
  size_t i=compartment.index();
  
  //Get compartment volume and assure it is physical
  double Vi = y[i][variableIndex(1,0)];
  assert( Vi>0.0 );
  
  //Do this update from marked compartments
  if( y[i][variableIndex(0,1)] < 0.5 ) {
    return;
  }
  //Check that the compartment have parameters corresponding 
  //to the neighboring CELLS 
  //Caveat: this is now handled by multiple layers of neighs 
  assert( compartment.numNeighbor() == compartment.numParameter() );
  
  for( size_t n=0 ; n<compartment.numNeighbor() ; n++ ) {
    size_t j=compartment.neighbor(n);
    if( y[j][variableIndex(0,2)] > 0.5 ) {//Only when neighbor is marked

      size_t jHelper = static_cast<size_t>( compartment.parameter(n) );
      //Get neighbor volume and make sure it is physical 

      double Vj = y[j][variableIndex(1,0)];
      assert( Vj>0.0 );

      double compConc = y[i][species];
      double neighConc = y[j][species];
      size_t helperCol=variableIndex(0,0);
      double tmp;
      //Only update if jHelper is not the background
      if( jHelper != static_cast<size_t>(-1) ) {
	//If no neighbor area defined assume area and distance equal to 1
	double distance = 1.0,area=1.0;
	if( compartment.numNeighborArea()>n ) {
	  area = compartment.neighborArea(n);
	  distance=0.0;
	  for( size_t d=compartment.topologyStart() ; d<compartment.numDimension() 
		 ; d++ )
	    distance += (y[j][d]-y[i][d])*(y[j][d]-y[i][d]);
	  assert( distance>0.0 );;
	  distance = sqrt(distance);
	}
	//T_out
	tmp = parameter(0)*compConc*y[jHelper][helperCol]*area/distance;
	dydt[i][species] -= tmp/Vi;
	dydt[j][species] += tmp/Vj;
	//T_in
	tmp = parameter(1)*neighConc*y[jHelper][helperCol]*area/distance;
	dydt[i][species] += tmp/Vi;
	dydt[j][species] -= tmp/Vj;
      }
    }
  }
}

MembraneTransport::MembraneTransport(std::vector<double> &paraValue, 
				     std::vector< std::vector<size_t> > 
				     &indValue ) {
  
  //Do some checks on the parameters and variable indeces
  //////////////////////////////////////////////////////////////////////
  if( paraValue.size()!=1 ) {
    std::cerr << "MembraneTransport::MembraneTransport() "
	      << "Uses only one parameter P (m/s).\n";
    exit(0);
  }
  if( indValue.size() != 1 || indValue[0].size() !=1 ) {
    std::cerr << "MembraneTransport::MembraneTransport() "
	      << "Variable index for size parameter needed.\n";
    exit(0);
  }
  //Set the variable values
  //////////////////////////////////////////////////////////////////////
  setId("membraneTransport");
  setParameter(paraValue);  
  setVariableIndex(indValue);
  
  //Set the parameter identities
  //////////////////////////////////////////////////////////////////////
  std::vector<std::string> tmp( numParameter() );
  tmp.resize( numParameter() );
  tmp[0] = "P";
  setParameterId( tmp );
}

void MembraneTransport::derivs(Compartment &compartment,size_t species,
			       std::vector< std::vector<double> > &y,
			       std::vector< std::vector<double> > &dydt ) {
  
  size_t i=compartment.index();
  double Vi = y[i][variableIndex(0,0)];//Volume of the compartment
  if( Vi<=0.0 ) {
    std::cerr << "MembraneTransport::derivs Given volume " << Vi 
	      << " unphysical.\n";
    exit(-1);
  }
  
  for( size_t n=0 ; n<compartment.numNeighbor() ; n++ ) {
    size_t j=compartment.neighbor(n);
    //Only update if compartments index less than neighbors
    if( i<j ) {
      double Vj = y[j][variableIndex(0,0)];//Volume of the compartment neigh
      if( Vj<=0.0 ) {
	std::cerr << "MembraneTransport::derivs Given volume " << Vj
		  << " unphysical.\n";
	exit(-1);
      }
      //If no neighbor area defined assume area 1
      double area=1.0;
      if( compartment.numNeighborArea()>n )
	area = compartment.neighborArea(n);

      double diff = y[j][species]-y[i][species];
      dydt[i][species] += parameter(0)*diff*area/Vi;
      dydt[j][species] -= parameter(0)*diff*area/Vj;
    }
  }
}

MembraneTransportRestricted::MembraneTransportRestricted(std::vector<double> 
							 &paraValue, 
							 std::vector< 
							 std::vector<size_t> > 
							 &indValue ) {
  
  //Do some checks on the parameters and variable indeces
  //////////////////////////////////////////////////////////////////////
  if( paraValue.size()!=2 ) {
    std::cerr << "MembraneTransportRestricted::MembraneTransportRestricted() "
	      << "Uses two parameters P_out and P_in (m/s).\n";
    exit(0);
  }
  if( indValue.size() != 2 || indValue[0].size() !=2 || 
      indValue[1].size() !=1 ) {
    std::cerr << "MembraneTransportRestricted::MembraneTransportRestricted() "
	      << "Variable indices for two restrictMarkers, and size"
	      << " (second level) needed.\n";
    exit(0);
  }
  if( indValue[0][0] == indValue[0][1] && paraValue[0] != paraValue[1] ) {
    std::cerr << "MembraneTransportRestricted::MembraneTransportRestricted() "
	      << "Same type of compartments requires same P_out and P_in.\n";
    exit(0);
  }
  //Set the variable values
  //////////////////////////////////////////////////////////////////////
  setId("membraneTransportRestricted");
  setParameter(paraValue);  
  setVariableIndex(indValue);
  
  //Set the parameter identities
  //////////////////////////////////////////////////////////////////////
  std::vector<std::string> tmp( numParameter() );
  tmp.resize( numParameter() );
  tmp[0] = "P_out";
  tmp[1] = "P_in";
  setParameterId( tmp );
}

void MembraneTransportRestricted::derivs(Compartment &compartment,size_t species,
					 std::vector< std::vector<double> > &y,
					 std::vector< std::vector<double> > 
					 &dydt ) {
  
  size_t i=compartment.index();
  //Check that this compartment has neighbors and is marked for diffusion
  if( y[i][variableIndex(0,0)] < 0.5 || compartment.numNeighbor()<1 )
    return;
  
  //Get volume and assure it is physical
  double Vi = y[i][variableIndex(1,0)];
  assert( Vi>0.0 );
  
  for( size_t n=0 ; n<compartment.numNeighbor() ; n++ ) {
    size_t j=compartment.neighbor(n);
    //Only update if neighbor marked for this diffusion
    //If same type of compartments only update once (i>j)
    if( y[j][variableIndex(0,1)] > 0.5 )
      if( variableIndex(0,0) != variableIndex(0,1) || i>j ) {
	
	//Get neighbor volume and assure it is physical
	double Vj = y[j][variableIndex(1,0)];
	assert( Vj>0.0 );
	//If no neighbor area defined assume area 1
	double area=1.0;
	if( compartment.numNeighborArea()>n )
	  area = compartment.neighborArea(n);
	
	//D_out
	double tmp = parameter(0)*y[i][species]*area;
	dydt[i][species] -= tmp/Vi;
	dydt[j][species] += tmp/Vj;
	//D_in
	tmp = parameter(1)*y[j][species]*area;
	dydt[i][species] += tmp/Vi;
	dydt[j][species] -= tmp/Vj;
      }  
  }
}

ActiveMembraneTransportRestricted::
ActiveMembraneTransportRestricted(std::vector<double> &paraValue, 
				  std::vector< std::vector<size_t> > &indValue ) {
  
  //Do some checks on the parameters and variable indeces
  //////////////////////////////////////////////////////////////////////
  if( paraValue.size()!=2 ) {
    std::cerr << "ActiveMembraneTransportRestricted::"
	      << "ActiveMembraneTransportRestricted() "
	      << "Uses two parameters T_out T_in length/(time conc).\n";
    exit(0);
  }
  if( indValue.size() != 2 || indValue[0].size() !=3
      || indValue[1].size() !=1 ) {
    std::cerr << "ActiveMembraneTransportRestricted::"
	      << "ActiveMembraneTransportRestricted() "
	      << "Variable index for helper(in neighbor), compartment"
	      << " and neighbor markers, and size (2nd level) needed\n";
    exit(0);
  }
  //Set the variable values
  //////////////////////////////////////////////////////////////////////
  setId("activeMembraneTransportRestricted");
  setParameter(paraValue);  
  setVariableIndex(indValue);
  
  //Set the parameter identities
  //////////////////////////////////////////////////////////////////////
  std::vector<std::string> tmp( numParameter() );
  tmp.resize( numParameter() );
  tmp[0] = "T_out";
  tmp[1] = "T_in";
  setParameterId( tmp );
}

void ActiveMembraneTransportRestricted::derivs(Compartment &compartment,
					       size_t species,
					       std::vector< 
					       std::vector<double> > &y,
					       std::vector< 
					       std::vector<double> > &dydt ) {
  
  size_t i=compartment.index();
  //Do this update from compartmentMarked cells with neighbors
  if( y[i][variableIndex(0,1)] < 0.5 || compartment.numNeighbor()<1 ) {
    return;
  }
  
  //Get compartment volume and assure it is physical
  double Vi = y[i][variableIndex(1,0)];
  assert( Vi>0.0 );
  
  
  for( size_t n=0 ; n<compartment.numNeighbor() ; n++ ) {
    size_t j=compartment.neighbor(n);
    if( y[j][variableIndex(0,2)] > 0.5 ) {//Only when neighbor is marked

      //Get neighbor volume and assure it is physical
      double Vj = y[j][variableIndex(1,0)];
      assert( Vj>0.0 );

      double compConc = y[i][species];
      double neighConc = y[j][species];
      size_t helperCol=variableIndex(0,0);
      //If no neighbor area defined assume area 1 
      double area=1.0;
      if( compartment.numNeighborArea()>n )
	area = compartment.neighborArea(n);
      
      //T_out
      double tmp = parameter(0)*compConc*y[j][helperCol]*area;
      dydt[i][species] -= tmp/Vi;
      dydt[j][species] += tmp/Vj;
      //T_in
      tmp = parameter(1)*neighConc*y[j][helperCol]*area;
      dydt[i][species] += tmp/Vi;
      dydt[j][species] -= tmp/Vj;
    }
  }
}

ActiveMembraneTransportRestrictedMM::
ActiveMembraneTransportRestrictedMM(std::vector<double> &paraValue, 
				    std::vector< std::vector<size_t> > 
				    &indValue ) {
  
  //Do some checks on the parameters and variable indeces
  //////////////////////////////////////////////////////////////////////
  if( paraValue.size()!=3 ) {
    std::cerr << "ActiveMembraneTransportRestrictedMM::"
	      << "ActiveMembraneTransportRestrictedMM() "
	      << "Uses three parameters T_out T_in length/(time conc) "
	      << " and K_M.\n";
    exit(0);
  }
  if( indValue.size() != 2 || indValue[0].size() !=3
      || indValue[1].size() !=1 ) {
    std::cerr << "ActiveMembraneTransportRestrictedMM::"
	      << "ActiveMembraneTransportRestrictedMM() "
	      << "Variable index for helper(in neighbor), compartment"
	      << " and neighbor markers, and size (2nd level) needed\n";
    exit(0);
  }
  //Set the variable values
  //////////////////////////////////////////////////////////////////////
  setId("activeMembraneTransportRestrictedMM");
  setParameter(paraValue);  
  setVariableIndex(indValue);
  
  //Set the parameter identities
  //////////////////////////////////////////////////////////////////////
  std::vector<std::string> tmp( numParameter() );
  tmp.resize( numParameter() );
  tmp[0] = "T_out";
  tmp[1] = "T_in";
  tmp[2] = "K_M";
  setParameterId( tmp );
}

void ActiveMembraneTransportRestrictedMM::derivs(Compartment &compartment,
						 size_t species,
						 std::vector< 
						 std::vector<double> > &y,
						 std::vector< 
						 std::vector<double> > 
						 &dydt ) {
  
  size_t i=compartment.index();
  //Do this update from compartmentMarked cells with neighbors
  if( y[i][variableIndex(0,1)] < 0.5 || compartment.numNeighbor()<1 ) {
    return;
  }
  
  //Get compartment volume and assure it is physical
  double Vi = y[i][variableIndex(1,0)];
  assert( Vi>0.0 );
  
  for( size_t n=0 ; n<compartment.numNeighbor() ; n++ ) {
    size_t j=compartment.neighbor(n);
    if( y[j][variableIndex(0,2)] > 0.5 ) {//Only when neighbor is marked
      //Get neighbor volume and assure it is physical
      double Vj = y[j][variableIndex(1,0)];
      assert( Vj>0.0 );
      
      double compConcFac = y[i][species]/(parameter(2)+y[i][species]);
      double neighConcFac = y[j][species]/(parameter(2)+y[j][species]);
      size_t helperCol=variableIndex(0,0);
      //If no neighbor area defined assume area 1 
      double area=1.0;
      if( compartment.numNeighborArea()>n )
	area = compartment.neighborArea(n);
      
      //T_out
      double tmp = parameter(0)*compConcFac*y[j][helperCol]*area;
      dydt[i][species] -= tmp/Vi;
      dydt[j][species] += tmp/Vj;
      //T_in
      tmp = parameter(1)*neighConcFac*y[j][helperCol]*area;
      dydt[i][species] += tmp/Vi;
      dydt[j][species] -= tmp/Vj;
    }
  }
}

ActiveMembraneTransportBRestricted::
ActiveMembraneTransportBRestricted(std::vector<double> &paraValue, 
				   std::vector< std::vector<size_t> > 
				   &indValue ) {
  
  //Do some checks on the parameters and variable indeces
  //////////////////////////////////////////////////////////////////////
  if( paraValue.size()!=2 ) {
    std::cerr << "ActiveMembraneTransportBRestricted::"
	      << "ActiveMembraneTransportBRestricted() "
	      << "Uses two parameters T_out T_in (length/(time conc).\n";
    exit(0);
  }
  if( indValue.size() != 2 || indValue[0].size() !=3
      || indValue[1].size() !=1 ) {
    std::cerr << "ActiveMembraneTransportBRestricted::"
	      << "ActiveMembraneTransportBRestricted() "
	      << "Variable index for helper, compMarker"
	      << " and neighMarker, and size (2nd level) needed.\n";
    exit(0);
  }
  //Set the variable values
  //////////////////////////////////////////////////////////////////////
  setId("activeMembraneTransportBRestricted");
  setParameter(paraValue);  
  setVariableIndex(indValue);
  
  //Set the parameter identities
  //////////////////////////////////////////////////////////////////////
  std::vector<std::string> tmp( numParameter() );
  tmp.resize( numParameter() );
  tmp[0] = "T_out";
  tmp[1] = "T_in";
  setParameterId( tmp );
}

void ActiveMembraneTransportBRestricted::derivs(Compartment &compartment,
						size_t species,
						std::vector< 
						std::vector<double> > &y,
						std::vector< 
						std::vector<double> > &dydt ) {
  
  size_t i=compartment.index();
  double Vi = y[i][variableIndex(1,0)];//Volume of the compartment
  if( Vi<=0.0 ) {
    std::cerr << "ActiveMembraneTransportBRestricted::derivs Given volume " 
	      << Vi << " unphysical.\n";
    exit(-1);
  }
  //Do this update from marked compartments
  if( y[i][variableIndex(0,1)] < 0.5 ) {
    return;
  }
  //Check that the compartment have parameters corresponding to the neighbor
  //cells
  if( compartment.numNeighbor() != compartment.numParameter() ) {
    std::cerr << "ActiveMembraneTransportBRestricted::derivs "
	      << "Requires neighboring CELLS in the parameter fields"
	      << " of the compartment.\n";
    exit(0);
  }	
  
  for( size_t n=0 ; n<compartment.numNeighbor() ; n++ ) {
    size_t j=compartment.neighbor(n);
    if( y[j][variableIndex(0,2)] > 0.5 ) {//Only when neighbor is marked
      size_t jHelper = static_cast<size_t>( compartment.parameter(n) );
      double Vj = y[j][variableIndex(1,0)];//Volume of the compartment
      if( Vj<=0.0 ) {
	std::cerr << "ActiveMembraneTransportBRestricted::derivs Given volume "
		  << Vj << " unphysical.\n";
	exit(-1);
      }
      double compConc = y[i][species];
      double neighConc = y[j][species];
      size_t helperCol=variableIndex(0,0);
      double tmp;
      //Only if jHelper not background
      if( jHelper != static_cast<size_t>(-1) ) {
	//If no neighbor area defined assume area 1
	double area=1.0;
	if( compartment.numNeighborArea()>n )
	  area = compartment.neighborArea(n);

	//T_out
	tmp = parameter(0)*compConc*y[jHelper][helperCol]*area;
	dydt[i][species] -= tmp/Vi;
	dydt[j][species] += tmp/Vj;
	//T_in
	tmp = parameter(1)*neighConc*y[jHelper][helperCol]*area;
	dydt[i][species] += tmp/Vi;
	dydt[j][species] -= tmp/Vj;
      }
    }
  }
}

ActiveMembraneTransportBHillRestricted::
ActiveMembraneTransportBHillRestricted(std::vector<double> &paraValue, 
				   std::vector< std::vector<size_t> > 
				   &indValue ) {
  
  //Do some checks on the parameters and variable indeces
  //////////////////////////////////////////////////////////////////////
  if( paraValue.size()!=4 ) {
    std::cerr << "ActiveMembraneTransportBHillRestricted::"
	      << "ActiveMembraneTransportBHillRestricted() "
	      << "Uses two parameters T_out T_in (length/(time conc),"
	      << "n_H K_H.\n";
    exit(0);
  }
  if( indValue.size() != 2 || indValue[0].size() !=3
      || indValue[1].size() !=1 ) {
    std::cerr << "ActiveMembraneTransportBHillRestricted::"
	      << "ActiveMembraneTransportBHillRestricted() "
	      << "Variable index for helper, compMarker"
	      << " and neighMarker, and size (2nd level) needed.\n";
    exit(0);
  }
  //Set the variable values
  //////////////////////////////////////////////////////////////////////
  setId("activeMembraneTransportBHillRestricted");
  setParameter(paraValue);  
  setVariableIndex(indValue);
  
  //Set the parameter identities
  //////////////////////////////////////////////////////////////////////
  std::vector<std::string> tmp( numParameter() );
  tmp.resize( numParameter() );
  tmp[0] = "T_out";
  tmp[1] = "T_in";
  tmp[2] = "n_H";
  tmp[3] = "K_H";
  setParameterId( tmp );
}

void ActiveMembraneTransportBHillRestricted::
derivs(Compartment &compartment,size_t species,
       std::vector< std::vector<double> > &y,
       std::vector< std::vector<double> > &dydt ) {
  
  size_t i=compartment.index();
  double Vi = y[i][variableIndex(1,0)];//Volume of the compartment
  if( Vi<=0.0 ) {
    std::cerr << "ActiveMembraneTransportBHillRestricted::derivs() "
	      << "Given volume " << Vi << " unphysical.\n";
    exit(-1);
  }
  //Do this update from marked compartments
  if( y[i][variableIndex(0,1)] < 0.5 ) {
    return;
  }
  //Check that the compartment have parameters corresponding to the neighbor
  //cells
  if( compartment.numNeighbor() != compartment.numParameter() ) {
    std::cerr << "ActiveMembraneTransportBHillRestricted::derivs "
	      << "Requires neighboring CELLS in the parameter fields"
	      << " of the compartment.\n";
    exit(0);
  }	
  
  for( size_t n=0 ; n<compartment.numNeighbor() ; n++ ) {
    size_t j=compartment.neighbor(n);
    if( y[j][variableIndex(0,2)] > 0.5 ) {//Only when neighbor is marked
      size_t jHelper = size_t( compartment.parameter(n) );
      double Vj = y[j][variableIndex(1,0)];//Volume of the compartment
      if( Vj<=0.0 ) {
	std::cerr << "ActiveMembraneTransportBHillRestricted::derivs() "
		  << "Given volume " << Vj << " unphysical.\n";
	exit(-1);
      }
      double compConc = y[i][species];
      double neighConc = y[j][species];
      size_t helperCol=variableIndex(0,0);
      double tmp,factor;
      //Only if jHelper not background
      if( jHelper != static_cast<size_t>(-1) ) {
	//If no neighbor area defined assume area 1
	double area=1.0;
	if( compartment.numNeighborArea()>n )
	  area = compartment.neighborArea(n);
	
	//T_out
	factor = std::pow(y[jHelper][helperCol],parameter(2)) /
	  ( std::pow( parameter(3),parameter(2) ) +
	    std::pow(y[jHelper][helperCol],parameter(2) ) );
	tmp = parameter(0)*compConc*area*factor;
	dydt[i][species] -= tmp/Vi;
	dydt[j][species] += tmp/Vj;
	//T_in
	tmp = parameter(1)*neighConc*area*factor;
	dydt[i][species] += tmp/Vi;
	dydt[j][species] -= tmp/Vj;
      }
    }
  }
}

ActiveTransportVariableSimple::ActiveTransportVariableSimple(std::vector<double>
																														 &paraValue, 
																														 std::vector< 
																														 std::vector<size_t> > 
																														 &indValue ) {
  
  // Do some checks on the parameters and variable indeces
  if (paraValue.size() != 1) {
    std::cerr << "ActiveTransportVariableSimple::ActiveTransportVariableSimple() "
							<< "Uses one parameter T.\n";
    exit(0);
  }
  if (indValue.size() != 1 || indValue[0].size() < 1) {
    std::cerr << "ActiveTransportVariableSimple::ActiveTransportVariableSimple() "
	      << "Variable index for helper molecules needed in first level." << std::endl;
    exit(0);
  }
  // Set the variable values
  setId("activeTransportVariableSimple");
  setParameter(paraValue);  
  setVariableIndex(indValue);
  
  // Set the parameter identities
  std::vector<std::string> tmp( numParameter() );
  tmp.resize( numParameter() );
  tmp[0] = "T";
  setParameterId( tmp );
}

void ActiveTransportVariableSimple::derivs(Compartment &compartment,size_t species,
																					 std::vector< std::vector<double> > &y,
																					 std::vector< std::vector<double> > 
																					 &dydt ) {
  
	if (numVariableIndex(0)>compartment.numNeighbor()) {
		std::cerr << "ActiveTransportVariableSimple::derivs() "
							<< "Wrong number of neighbors!" << std::endl;
		std::cerr << "cell " << compartment.index() << " numVariableIndex " << numVariableIndex(0)
							<< " numNeighbor " << compartment.numNeighbor() << std::endl; 
		exit(-1);
	}
  size_t i=compartment.index();
	for( size_t k=0; k<numVariableIndex(0); ++k ) {
		//std::cerr << "cell " << compartment.index() << " variable " << species 
		//				<< " transported to/from neighbor " << compartment.neighbor(k) << " mediated by " 
		//				<< variableIndex(0,k) << std::endl;		
		double tmp = parameter(0)*y[i][species]*y[i][variableIndex(0,k)];
		dydt[i][species] -= tmp;
		dydt[compartment.neighbor(k)][species] += tmp;
	}
}


