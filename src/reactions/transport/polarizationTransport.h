//
// Filename     : polarizationTransport.h
// Description  : Classes describing polarized transports of molecules
// Author(s)    : Henrik Jonsson (henrik@thep.lu.se)
// Created      : December 2007
// Revision     : $Id: transport.h 203 2007-03-20 10:50:07Z henrik $
//
#ifndef POLARIZATIONTRANSPORT_H
#define POLARIZATIONTRANSPORT_H

#include<cmath>
#include"../baseReaction.h"

/// 
/// @brief This namespace contains reactions that polarizes cells depending on concentrations in 
/// neighboring cells and transport molecules according to the polarization 
/// (assumed to be fast/in quasi-equilibrium)
///
/// The Polarized transporter molecule P is divided into the cell cytosol
/// (P_i) and its membranes (P_ij) following the dynamics
///
/// @f[ dP_i/dt = -P_i \sum_k f(A_k) + b \sum_k g(A_k) P_{ik} @f]
/// @f[ dP_{ij}/dt = f(A_j) P_i - g(A_j) P_{ij} @f]
///
/// where \e k is summed over neighboring cells, \e A is the polarizing signal
/// and \e f, \e g are functions (determined by the individual reactions). 
/// The polarization is assumed to be fast and
/// the fixed point values are assumed
///  
/// @f[ 
/// P_{ij}^\ast = \frac{f(A_j)}{g(A_j)}\frac{P}{1+\sum_k
/// \frac{f(A_k)}{g(A_k)}}
/// @f]
///  
/// where \e P is the total amount of polarized molecule in a cell.
///  
/// The polarized transport of a molecule X between two neighboring cells is
/// then given by a linear (reactions called x=lin,constlin,hill,...)
///  
/// @f[ \frac{dX_i}{dt} = T ( X_i P_{ij} - X_j P_{ji} ) @f]
///  
/// or Michaelis-Menten (reactions called xMM)
///
/// @f[ \frac{dX_i}{dt} = T ( P_{ij} \frac{X_i}{(X_i + K_M)} - P_{ji} \frac{X_j}{(X_i + K_M)} ) @f]
///
/// or Michaelis-Menten with an symmetric influx mediator, L (reactions called xMMInflux)
/// 
/// @f[ \frac{dX_i}{dt} = T ( P_{ij} \frac{X_i}{(X_i+K)} \frac{L_j}{(L_i+L_j)} 
/// - P_{ji} \frac{X_j}{(X_j+K)} \frac{L_i}{(L_i+L_j)} ) @f]
///
/// description. The parameters given depend on the feedback function for the polarization 
/// and the transport mechanism used. The volumes for the cells are also included for correct
/// concentration update. These reactions follow the approach used in several publications:
/// e.g. Jonsson et al PNAS (2006), Heisler and Jonsson J P Gr Reg (2006) Sahlin et al JTB (2009) 
///
/// @see SimplifiedAuxinTransportJTB 
///
namespace PolarizationTransportFast
{
  /// 
  /// @brief This reaction polarizes cells with a linear function and transport molecules linearly
  ///
  /// The Polarized transporter molecule P is divided into the cell cytosol
  /// (P_i) and its membranes (P_ij) following the dynamics
  ///
  /// @f[ dP_i/dt = -a P_i \sum_k A_k + b \sum_k P_{ik} @f]
  /// @f[ dP_{ij}/dt = a P_i A_j - b P_{ij} @f]
  ///
  /// where \e k is summed over neighboring cells, \e A is the polarizing signal
  /// and \e a, \e b are constants. The polarization is assumed to be fast and
  /// the fixed point values are assumed
  ///  
  /// @f[ P_i = (b/a)*P / (\sum_k A_k + b/a) @f]
  /// @f[ P_{ij} = P * A_j / (\sum_k A_k + (b/a)) @f]
  ///  
  /// where \e P is the total amount of polarized molecule in a cell.
  ///  
  /// The polarized transport of a molecule X between two neighboring cells is
  /// then given by
  ///  
  /// @f[ dX_i/dt = T ( X_i P_{ij} - X_j P_{ji} ). @f]
  ///  
  /// The parameters given to this function are T and K=b/a.  The volumes for
  /// the cells are also included for correct concentration update.
  ///
  class Lin : public BaseReaction {
    
  public:
    ///
    /// @brief Main constructor
    ///
    /// This is the main constructor which sets the parameters and variable
    /// indices that defines the reaction.
    ///
    /// @param paraValue vector with parameters
    ///
    /// @param indValue vector of vectors with variable indices
    ///
    /// @see BaseReaction::createReaction(std::vector<double> &paraValue,...)
    ///
    Lin(std::vector<double> &paraValue, 
			      std::vector< std::vector<size_t> > &indValue );
    
    ///
    /// @brief Derivative function for this reaction class
    ///
    /// @see BaseReaction::derivs(Compartment &compartment,size_t species,...)
    ///
    void derivs(Compartment &compartment,size_t species,
		std::vector< std::vector<double> > &y,
		std::vector< std::vector<double> > &dydt );

    ///
    /// @brief Derivative function for this reaction class calculating the absolute value for noise solvers
    ///
    /// @see BaseReaction::derivsWithAbs(Compartment &compartment,size_t species,...)
    ///
    void derivsWithAbs(Compartment &compartment,size_t species,DataMatrix &y,DataMatrix &dydt,DataMatrix &sdydt);

    ///
    /// @brief Propensity calculation (probability to happen in [t,t+dt]) for 
    /// this reaction class
    ///
    /// @see BaseReaction::propensity(Compartment &compartment,size_t species,DataMatrix &y)
    ///
    double propensity(Compartment &compartment,size_t species,DataMatrix &y);
    
    ///
    /// @brief Discrete update for this reaction used in stochastic simulations
    ///
    /// @see BaseReaction::discreteUpdate(Compartment &compartment,size_t species,
    /// DataMatrix &y)
    ///
    void discreteUpdate(Compartment &compartment,size_t species,DataMatrix &y);    

    ///
    /// @brief Calculates Pij for all neighbors and sets the values in pij
    ///
    /// @return @f[\sum_k P_{ik} @f]
    ///
    double Pij(Compartment &compartment, size_t species, 
	       DataMatrix &y, std::vector<double> &pij);
  };
  
  /// 
  /// @brief This reaction polarizes cells with a const+linear function and transport 
  /// molecules linearly
  ///
  /// The Polarized transporter molecule P is divided into the cell cytosol
  /// (@f$P_i@f$) and its membranes (@f$P_ij@f$) following the dynamics
  ///
  /// @f[ \frac{dP_i}{dt} = -a P_i \sum_k (c+A_k) + b \sum_k P_{ik} @f]
  /// @f[ \frac{dP_{ij}}{dt} = a P_i (c+A_j) - b P_{ij} @f]
  ///
  /// where k is summed over neighboring cells, A is the polarizing signal and
  /// a,b,c are constants. The polarization is assumed to be fast and the fixed
  /// point values are assumed
  ///
  /// @f[ P_i = (b/a)*P / (\sum_k A_k + (b/a)) @f]
  /// @f[ P_{ij} = \frac{P (c+A_j)}{(\sum_k (c+A_k) + (b/a))} @f]
  ///
  /// where P is the total amount of polarized molecule in a cell.
  ///
  /// The polarized transport of a molecule X between two neighboring cells is
  /// then given by
  ///
  /// @f[ \frac{dX_i}{dt} = T ( P_{ij}*X_i - P_{ji}*X_j ). @f]
  ///
  /// The parameters given to this function are T and K'=b/a, and c.  a_ij is
  /// the crossing area. The volumes for the cells are also included for correct
  /// concentration update.
  ///
  class Constlin : public BaseReaction {
    
  public:
    
    ///
    /// @brief Main constructor
    ///
    /// This is the main constructor which sets the parameters and variable
    /// indices that defines the reaction.
    ///
    /// @param paraValue vector with parameters
    ///
    /// @param indValue vector of vectors with variable indices
    ///
    /// @see BaseReaction::createReaction(std::vector<double> &paraValue,...)
    ///
    Constlin(std::vector<double> &paraValue, 
	     std::vector< std::vector<size_t> > &indValue );
    
    ///
    /// @brief Derivative function for this reaction class
    ///
    /// @see BaseReaction::derivs(Compartment &compartment,size_t species,...)
    ///
    void derivs(Compartment &compartment,size_t species,
		std::vector< std::vector<double> > &y,
		std::vector< std::vector<double> > &dydt );
  };

  /// 
  /// @brief This reaction polarizes cells with a Hill function and transport molecules linearly
  ///
  class Hill : public BaseReaction {
    
  public:
    
    ///
    /// @brief Main constructor
    ///
    /// This is the main constructor which sets the parameters and variable
    /// indices that defines the reaction.
    ///
    /// @param paraValue vector with parameters
    ///
    /// @param indValue vector of vectors with variable indices
    ///
    /// @see BaseReaction::createReaction(std::vector<double> &paraValue,...)
    ///
    Hill(std::vector<double> &paraValue, 
	 std::vector< std::vector<size_t> > &indValue );
    
    ///
    /// @brief Derivative function for this reaction class
    ///
    /// @see BaseReaction::derivs(Compartment &compartment,size_t species,...)
    ///
    void derivs(Compartment &compartment,size_t species,
		std::vector< std::vector<double> > &y,
		std::vector< std::vector<double> > &dydt );
  };
  
  ///
  /// @brief This reaction polarizes cells with a general (f/g) function and transport molecules linearly
  ///
  /// The Polarized transporter molecule @f$P@f$ is divided into the cell
  /// cytosol (@f$P_i@f$) and its membranes (@f$P_{ij}@f$) following the dynamics
  /// @f[
  /// \frac{dP_i}{dt} = - \sum_j f(A_j)\cdot P_i + \sum_j g(A_j)\cdot P_{ij} 
  /// @f]
  /// @f[
  /// \frac{dP_{ij}}{dt} = f(A_j)\cdot P_i - f(A_j)\cdot P_{ij}
  /// @f]
  ///
  /// where @f$j@f$ is summed over neighboring cells, @f$A@f$ is the
  /// polarizing signal and @f$f(A)@f$ and @f$g(A)@f$ are arbitrary
  /// functions. The polarization is assumed to be fast and the fixed
  /// point values are assumed to be
  /// @f[ 
  /// P_{ij}^\ast =
  /// \frac{f(A_j)}{g(A_j)}\frac{P}{1+\sum_k
  /// \frac{f(A_k)}{g(A_k)}}
  /// @f]
  /// where @f$k@f$ is summed over all neighboring cells and @f$P@f$ is
  /// the total amount of polarized molecule in a cell. The polarized
  /// transport of a molecule X between two neighboring cells is then
  /// given by
  /// @f[ \frac{dX_i}{dt} = - T (X_i\cdot P_{ij} -
  /// X_j\cdot P_{ji}).
  /// @f]
  /// 
  /// The parameters given to this function are @f$T@f$, @f$k_f@f$,
  /// @f$n_f@f$, @f$k_g@f$ and @f$n_g@f$. First level variables are
  /// @f$P@f$ and @f$X@f$.
  ///
  /// @note g=0 works in analysis of setting to quasiequilibrium but fails in this reaction.
  ///
  /// @note You can use this function for temporary testin feedback and transport functions
  /// by updating the f and g function
  ///
  class General : public BaseReaction 
  {
  public:
    ///
    /// @brief Main constructor
    ///
    /// This is the main constructor which sets the parameters and variable
    /// indices that defines the reaction.
    ///
    /// @param paraValue vector with parameters
    ///
    /// @param indValue vector of vectors with variable indices
    ///
    /// @see BaseReaction::createReaction(std::vector<double> &paraValue,...)
    ///
    General(std::vector<double> &paraValue, 
	    std::vector< std::vector<size_t> > &indValue );
    
    ///
    /// @brief Derivative function for this reaction class
    ///
    /// @see BaseReaction::derivs(Compartment &compartment,size_t species,...)
    ///
    void derivs(Compartment &compartment,size_t species,
		std::vector< std::vector<double> > &y,
		std::vector< std::vector<double> > &dydt );
    ///
    /// @brief Hard coded general function f(A).
    ///
    double f(double a, double k, double n);
    ///
    /// @brief Hard coded general function g(A).
    ///
    double g(double a, double k, double n);
  };

  ///
  /// @brief This reaction 'polarizes' cells with a constant function (i.e. symmetric efflux) 
  /// and transport molecules with Michaelis-Menten
  ///
  /// The active transport of a molecule X between two neighboring
  /// cells is given by a Michaelis Menten formalism
  ///
  /// @f[ dX_i/dt = T * ( P_ij*X_i/(X_i+K_M) - P_ji*X_j/(X_j+K_M) ). @f]
  ///
  /// The parameters given to this function are T and K_M (maximal rate
  /// and MM constant). P is the efflux mediating molecule. The volumes
  /// for the cells are also included for correct concentration update.
  ///
  class ConstMM : public BaseReaction {
    
  public:
    
    ///
    /// @brief Main constructor
    ///
    /// This is the main constructor which sets the parameters and variable
    /// indices that defines the reaction.
    ///
    /// @param paraValue vector with parameters
    ///
    /// @param indValue vector of vectors with variable indices
    ///
    /// @see BaseReaction::createReaction(std::vector<double> &paraValue,...)
    ///
    ConstMM(std::vector<double> &paraValue, 
	    std::vector< std::vector<size_t> > &indValue );
    
    ///
    /// @brief Derivative function for this reaction class
    ///
    /// @see BaseReaction::derivs(Compartment &compartment,size_t species,...)
    ///
    void derivs(Compartment &compartment,size_t species,
		std::vector< std::vector<double> > &y,
		std::vector< std::vector<double> > &dydt );
  };
  
  /// 
  /// @brief This reaction polarizes cells with a linear function and transport molecules with Michaelis-Menten
  ///
  /// The Polarized transporter molecule P is divided into the cell cytosol
  /// (P_i) and its membranes (P_ij) following the dynamics
  ///
  /// @f[ dP_i/dt = -a P_i \sum_k A_k + b \sum_k P_{ik} @f]
  /// @f[ dP_{ij}/dt = a P_i A_j - b P_{ij} @f]
  ///
  /// where \e k is summed over neighboring cells, \e A is the polarizing signal
  /// and \e a, \e b are constants. The polarization is assumed to be fast and
  /// the fixed point values are assumed
  ///  
  /// @f[ P_i = (b/a)*P / (\sum_k A_k + b/a) @f]
  /// @f[ P_{ij} = P * A_j / (\sum_k A_k + (b/a)) @f]
  ///  
  /// where \e P is the total amount of polarized molecule in a cell.
  ///  
  /// The polarized transport of a molecule X between two neighboring cells is
  /// then given by
  ///  
  /// @f[ dX_i/dt = T ( P_{ij}*X_i/(X_i + K_M) - P_{ji}*X_j/(X_i + K_M)). @f]
  ///  
  /// The parameters given to this function are T, K=b/a and K_M. The volumes for
  /// the cells are also included for correct concentration update.
  ///
  class LinMM : public BaseReaction {
    
  public:
    
    ///
    /// @brief Main constructor
    ///
    /// This is the main constructor which sets the parameters and variable
    /// indices that defines the reaction.
    ///
    /// @param paraValue vector with parameters
    ///
    /// @param indValue vector of vectors with variable indices
    ///
    /// @see BaseReaction::createReaction(std::vector<double> &paraValue,...)
    ///
    LinMM(std::vector<double> &paraValue, 
	  std::vector< std::vector<size_t> > &indValue );
    
    ///
    /// @brief Derivative function for this reaction class
    ///
    /// @see BaseReaction::derivs(Compartment &compartment,size_t species,...)
    ///
    void derivs(Compartment &compartment,size_t species,
		std::vector< std::vector<double> > &y,
		std::vector< std::vector<double> > &dydt );
  };

  /// 
  /// @brief This reaction polarizes cells with a const+linear function and transport molecules with Michaelis-Menten
  ///
  /// This reaction has the same polarization rule as Constlin, but using Michaelis-Menten
  /// for describing the transport.  
  /// The polarized transport of a molecule X between two neighboring
  /// cells is then given by
  ///
  /// @f[ \frac{dX_i}{dt} = T ( P_{ij} \frac{X_i}{(X_i+K_M)} - P_{ji} \frac{X_j}{(X_j+K_M)} ) @f].
  ///
  /// The parameters given to this function are T, K and k=b/a. The
  /// volumes for the cells are also included for correct concentration
  /// update.
  ///
  class ConstlinMM : public BaseReaction {
    
  public:
    
    ///
    /// @brief Main constructor
    ///
    /// This is the main constructor which sets the parameters and variable
    /// indices that defines the reaction.
    ///
    /// @param paraValue vector with parameters
    ///
    /// @param indValue vector of vectors with variable indices
    ///
    /// @see BaseReaction::createReaction(std::vector<double> &paraValue,...)
    ///
    ConstlinMM(std::vector<double> &paraValue, 
	       std::vector< std::vector<size_t> > &indValue );
    
    ///
    /// @brief Derivative function for this reaction class
    ///
    /// @see BaseReaction::derivs(Compartment &compartment,size_t species,...)
    ///
    void derivs(Compartment &compartment,size_t species,
		std::vector< std::vector<double> > &y,
		std::vector< std::vector<double> > &dydt );
  };

  /// 
  /// @brief This reaction polarizes cells with a Hill function and transport molecules with 
  /// Michaelis-Menten
  ///
  class HillMM : public BaseReaction {
    
  public:
    
    ///
    /// @brief Main constructor
    ///
    /// This is the main constructor which sets the parameters and variable
    /// indices that defines the reaction.
    ///
    /// @param paraValue vector with parameters
    ///
    /// @param indValue vector of vectors with variable indices
    ///
    /// @see BaseReaction::createReaction(std::vector<double> &paraValue,...)
    ///
    HillMM(std::vector<double> &paraValue, 
	   std::vector< std::vector<size_t> > &indValue );
    
    ///
    /// @brief Derivative function for this reaction class
    ///
    /// @see BaseReaction::derivs(Compartment &compartment,size_t species,...)
    ///
    void derivs(Compartment &compartment,size_t species,
		std::vector< std::vector<double> > &y,
		std::vector< std::vector<double> > &dydt );
  };

  /// 
  /// @brief This reaction polarizes cells with an exponential function and transport molecules with 
  /// Michaelis-Menten
  ///
  class ExpMM : public BaseReaction {
    
  public:
    
    ///
    /// @brief Main constructor
    ///
    /// This is the main constructor which sets the parameters and variable
    /// indices that defines the reaction.
    ///
    /// @param paraValue vector with parameters
    ///
    /// @param indValue vector of vectors with variable indices
    ///
    /// @see BaseReaction::createReaction(std::vector<double> &paraValue,...)
    ///
    ExpMM(std::vector<double> &paraValue, 
	  std::vector< std::vector<size_t> > 
	  &indValue );
    
    ///
    /// @brief Derivative function for this reaction class
    ///
    /// @see BaseReaction::derivs(Compartment &compartment,size_t species,...)
    ///
    void derivs(Compartment &compartment,size_t species,
		std::vector< std::vector<double> > &y,
		std::vector< std::vector<double> > &dydt );
  };

  /// 
  /// @brief This reaction 'polarizes' cells with a const function (i.e. symmetric efflux) and transport 
  /// molecules with Michaelis-Menten including dependence of a symmetric influx transporter
  ///
  /// The transport of a molecule X between two neighboring cells is
  /// given by a Michaelis Menten formalism
  ///
  /// @f[ dX_i/dt = - T \frac{P_i L_j \frac{X_i}{X_i+K_M} -
  /// P_j L_i \frac{X_j}{X_j+K_M}}{L_i+L_j} @f]
  ///
  /// The parameters given to this function are T and K_M (max rate and MM
  /// constant). P and L are the efflux and influx carriers assumed to be
  /// symmetrically polarized. The volumes for the cells are also included
  /// for correct concentration update.
  ///
  class ConstMMInflux : public BaseReaction {
    
  public:
    
    ///
    /// @brief Main constructor
    ///
    /// This is the main constructor which sets the parameters and variable
    /// indices that defines the reaction.
    ///
    /// @param paraValue vector with parameters
    ///
    /// @param indValue vector of vectors with variable indices
    ///
    /// @see BaseReaction::createReaction(std::vector<double> &paraValue,...)
    ///
    ConstMMInflux(std::vector<double> &paraValue, 
		  std::vector< std::vector<size_t> > &indValue );
    
    ///
    /// @brief Derivative function for this reaction class
    ///
    /// @see BaseReaction::derivs(Compartment &compartment,size_t species,...)
    ///
    void derivs(Compartment &compartment,size_t species,
		std::vector< std::vector<double> > &y,
		std::vector< std::vector<double> > &dydt );
  };
  
  /// 
  /// @brief This reaction polarizes cells with a linear function and transport molecules with 
  /// Michaelis-Menten including dependence of a symmetric influx transporter
  ///
  /// This reaction acts as LinMM but with an influx component for the transport.
  /// The polarized transport of a molecule X between two neighboring
  /// cells is then given by a Michaelis Menten formalism
  /// 
  /// @f[ dX_i/dt = T * ( P_ij*X_i/(X_i+K) * L_j/(L_i+L_j) 
  /// - P_ji*X_j/(X_j+K) * L_i/(L_i+L_j) ) @f].
  ///
  /// The parameters given to this function are T and K'=b/a, and K. L is
  /// the influx carrier assumed to be symmetrically polarized. The
  /// volumes for the cells are also included for correct concentration
  /// update.
  ///
  /// @see PolarizationTransportFast::LinMM
  ///
  class LinMMInflux : public BaseReaction {
    
  public:
    
    ///
    /// @brief Main constructor
    ///
    /// This is the main constructor which sets the parameters and variable
    /// indices that defines the reaction.
    ///
    /// @param paraValue vector with parameters
    ///
    /// @param indValue vector of vectors with variable indices
    ///
    /// @see BaseReaction::createReaction(std::vector<double> &paraValue,...)
    ///
    LinMMInflux(std::vector<double> &paraValue, 
		std::vector< std::vector<size_t> > &indValue );
    
    ///
    /// @brief Derivative function for this reaction class
    ///
    /// @see BaseReaction::derivs(Compartment &compartment,size_t species,...)
    ///
    void derivs(Compartment &compartment,size_t species,
		std::vector< std::vector<double> > &y,
		std::vector< std::vector<double> > &dydt );
  };
  
  /// 
  /// @brief This reaction polarizes cells with a Hill function and transport molecules with 
  /// Michaelis-Menten including dependence of a symmetric influx transporter
  ///
  /// This reaction acts as HillMM but with an influx component for the transport.
  /// The polarized transport of a molecule X between two neighboring
  /// cells is then given by a Michaelis Menten formalism
  /// 
  /// @f[ dX_i/dt = T * ( P_ij*X_i/(X_i+K) * L_j/(L_i+L_j) 
  /// - P_ji*X_j/(X_j+K) * L_i/(L_i+L_j) ) @f].
  ///
  /// The parameters given to this function are T and K'=b/a, and K_m, K and n. 
  /// L is the influx carrier assumed to be symmetrically polarized. The
  /// volumes for the cells are also included for correct concentration
  /// update.
  ///
  /// @see PolarizationTransportFast::HillMM
  ///
  class HillMMInflux : public BaseReaction {
    
  public:
    
    ///
    /// @brief Main constructor
    ///
    /// This is the main constructor which sets the parameters and variable
    /// indices that defines the reaction.
    ///
    /// @param paraValue vector with parameters
    ///
    /// @param indValue vector of vectors with variable indices
    ///
    /// @see BaseReaction::createReaction(std::vector<double> &paraValue,...)
    ///
    HillMMInflux(std::vector<double> &paraValue, 
		std::vector< std::vector<size_t> > &indValue );
    
    ///
    /// @brief Derivative function for this reaction class
    ///
    /// @see BaseReaction::derivs(Compartment &compartment,size_t species,...)
    ///
    void derivs(Compartment &compartment,size_t species,
		std::vector< std::vector<double> > &y,
		std::vector< std::vector<double> > &dydt );
  };

  /// 
  /// @brief This reaction polarizes cells with a linear function and transport molecules with 
  /// Michaelis-Menten and calculates areas via sphere overlap
  ///
  /// This reactions is the same as LinMM, but approximates crossing areas from the overlap
  /// between spheres.
  ///
  /// @note This function is not completely tested.
  ///
  /// @see PolarizationTransportFast::LinMM
  ///
  class LinMMSpatial : public BaseReaction {
    
  public:
    
    ///
    /// @brief Main constructor
    ///
    /// This is the main constructor which sets the parameters and variable
    /// indices that defines the reaction.
    ///
    /// @param paraValue vector with parameters
    ///
    /// @param indValue vector of vectors with variable indices
    ///
    /// @see BaseReaction::createReaction(std::vector<double> &paraValue,...)
    ///
    LinMMSpatial(std::vector<double> &paraValue, 
		 std::vector< std::vector<size_t> > &indValue );
    
    ///
    /// @brief Derivative function for this reaction class
    ///
    /// @see BaseReaction::derivs(Compartment &compartment,size_t species,...)
    ///
    void derivs(Compartment &compartment,size_t species,
		std::vector< std::vector<double> > &y,
		std::vector< std::vector<double> > &dydt );
  };

  /// 
  /// @brief This reaction polarizes cells with a linear function and transport molecules with 
  /// Michaelis-Menten and reverses polarization depending on a molecular threshold
  ///
  /// This reaction is the same as LinMM except that the polarization can be switched depending on 
  /// the concentration of another molecule, given by its index.
  ///   The parameters given to this function are T and K'=b/a, K_M and Th_switch.
  ///
  /// @see PolarizationTransportFast::LinMM
  ///
  class LinMMSwitched : public BaseReaction {
    
  public:
    
    ///
    /// @brief Main constructor
    ///
    /// This is the main constructor which sets the parameters and variable
    /// indices that defines the reaction.
    ///
    /// @param paraValue vector with parameters
    ///
    /// @param indValue vector of vectors with variable indices
    ///
    /// @see BaseReaction::createReaction(std::vector<double> &paraValue,...)
    ///
    LinMMSwitched(std::vector<double> &paraValue, 
		  std::vector< std::vector<size_t> > &indValue );
    
    ///
    /// @brief Derivative function for this reaction class
    ///
    /// @see BaseReaction::derivs(Compartment &compartment,size_t species,...)
    ///
    void derivs(Compartment &compartment,size_t species,
		std::vector< std::vector<double> > &y,
		std::vector< std::vector<double> > &dydt );
  };
  
} //end namespace polarizationTransportFast

///
/// @brief Moves molecules activly with a MM type of equation and polarized
///
/// The Polarized transporter molecule P is divided into the cell cytosol
/// (@f$P_i@f$) and its membranes (@f$P_ij@f$) following the dynamics
///
/// @f[ \frac{dP_i}{dt} = -a P_i \sum_k (A_k) + b \sum_k P_{ik} @f]
/// @f[ \frac{dP_{ij}}{dt} = a P_i (A_j) - b P_{ij} @f]
///
/// where k is summed over neighboring cells (which are assumed to be in
/// neighbor level 1), A is the polarizing signal and a,b are constants. The
/// polarization is assumed to be fast and the fixed point values are assumed
///
/// @f[ P_i = \frac{(b/a)P}{(\sum_k a_k*A_k + V_i*(b/a))} @f]
/// @f[ P_{ij} = \frac{P (A_j)}{(\sum_k a_k*A_k + V_i*(b/a))} @f]
///
/// where P is the total amount of polarized molecule in a cell (P_i*V_i), and
/// a_k is the membrane area (if those are given, otherwise a_k=1).
///
/// The polarized transport of a molecule X between a cell (i) and neighboring
/// walls (ij) is then given by
///
/// @f[ dX_i/dt = 1/V_i sum_j a_ij ( T_inf P_ij*X_ij/(K+X_ij) 
/// - T_eff P_ij*X_i/(K+X_i) ). @f]
/// @f[ dX_ij/dt = 1/V_ij sum_j a_ij ( -T_inf P_ij*X_ij/(K+X_ij) 
/// + T_eff P_ij*X_i/(K+X_i) ). @f]
///
/// The parameters given to this function are T_eff, T_inf and K'=b/a. a_ij is
/// the crossing area. The volumes for the cells are also included for correct
/// concentration update. The derivative is updated from cellular
/// compartments.
///
class PolarizationLinFastTransportRestrictedMM : public BaseReaction {
  
 public:
  
  PolarizationLinFastTransportRestrictedMM(std::vector<double> 
					   &paraValue, 
					   std::vector< std::vector<size_t> > 
					   &indValue );
  
  void derivs(Compartment &compartment,size_t species,
	      std::vector< std::vector<double> > &y,
	      std::vector< std::vector<double> > &dydt );
};

///
/// @brief Moves molecules activly with a MM type of equation and polarized
///
/// The Polarized transporter molecule P is divided into the cell cytosol
/// (P_i) and its membranes (P_ij) following the dynamics
///  
/// @f[ dP_i/dt = -a P_i \sum_k f(A_k) + b \sum_k P_{ik} @f]
/// @f[ dP_{ij}/dt = a P_i f(A_j) - b P_{ij} @f] 
///
/// where k is summed over neighboring cells (which are assumed to be in
/// neighboe level 1), f(A_j)=((1-c)+cA_j^n/(Kh^n+a_j^n)) is the polarizing
/// signal and a,b are constants. The polarization is assumed to be fast and
/// the fixed point values are assumed
///  
/// @f[ P_i = (b/a)*P / (\sum_k a_k*f(A_k) + V_i*(b/a)) @f]
/// @f[ P_{ij} = P * f(A_j) / (\sum_k a_k*f(A_k) + V_i*(b/a)) @f]
///
/// where P is the total amount of polarized molecule in a cell (P_i*V_i), and
/// a_k is the membrane area (if those are given, otherwise a_k=1).
///  
/// The polarized transport of a molecule X between a cell (i) and neighboring
/// walls (ij) is then given by
///  
/// @f[ dX_i/dt = 1/V_i sum_j a_ij ( T_inf P_ij*X_ij/(K+X_ij) 
/// - T_eff P_ij*X_i/(K+X_i) ). @f]
/// @f[ dX_ij/dt = 1/V_ij sum_j a_ij ( -T_inf P_ij*X_ij/(K+X_ij) 
/// + T_eff P_ij*X_i/(K+X_i) ). @f]
///
/// The parameters given to this function are T_eff, T_inf and K'=b/a, c, Kh,
/// n, and K.  a_ij is the crossing area. The volumes for the cells are also
/// included for correct concentration update. The derivative is updated from
/// cellular compartments.
///
class PolarizationConstHillFastTransportRestrictedMM : 
public BaseReaction {
  
 public:
  
  PolarizationConstHillFastTransportRestrictedMM(std::vector<double> 
						 &paraValue, 
						 std::vector< 
						 std::vector<size_t> > 
						 &indValue );
  
  void derivs(Compartment &compartment,size_t species,
	      std::vector< std::vector<double> > &y,
	      std::vector< std::vector<double> > &dydt );
};

///
/// @brief Moves molecules activly with a MM type of equation and unpolarized
///
/// The transporter molecule P is divided into the cell cytosol (P_i) and its
/// membranes (P_ij) following the dynamics
///
/// @f[ \frac{dP_i}{dt} = -a P_i \sum_k 1 + b \sum_k P_{ik} @f]
/// @f[ \frac{dP_{ij}}{dt} = a P_i - b P_{ij} @f]
///
/// where k is summed over neighboring cells (which are assumed to be in
/// neighbor level 1), a,b are constants. The membranization is assumed to be
/// fast and the fixed point values are assumed
///
/// @f[ P_i = \frac{(b/a)*P}{(\sum_k a_k*1 + V_i*(b/a))} @f]
/// @f[ P_ij = \frac{P * 1}{(\sum_k a_k*1 + V_i*(b/a))} @f]
///
/// where P is the total amount of transport molecule in a cell (P_i*V_i), and
/// a_k is the membrane area (if those are given, otherwise a_k=1).
///
/// The polarized transport of a molecule X between a cell (i) and neighboring
/// walls (ij) is then given by
///
/// @f[ dX_i/dt = 1/V_i sum_j a_{ij} ( T_inf P_{ij}*X_{ij}/(K+X_{ij}) 
/// - T_{eff} P_{ij}*X_i/(K+X_i) ) . @f]
/// @f[dX_{ij}/dt = 1/V_{ij} sum_j a_{ij} ( -T_inf P_{ij}*X_ij/(K+X_{ij}) 
/// + T_{eff} P_{ij}*X_i/(K+X_i) ). @f]
///
/// The parameters given to this function are T_eff, T_inf and K'=b/a, and K.
/// a_ij is the crossing area. The volumes for the cells are also included for
/// correct concentration update. The derivative is updated from cellular
/// compartments.
///
class PolarizationConstFastTransportRestrictedMM : public BaseReaction {
  
 public:
  
  PolarizationConstFastTransportRestrictedMM(std::vector<double> 
					     &paraValue, 
					     std::vector< 
					     std::vector<size_t> > 
					     &indValue );
  
    void derivs(Compartment &compartment,size_t species,
		std::vector< std::vector<double> > &y,
		std::vector< std::vector<double> > &dydt );
};

///
/// @brief Moves molecules activly with a MM type of equation and unpolarized
///
/// The transporter molecule P is divided into the cell cytosol (P_i) and its
/// membranes (P_ij) following the dynamics
///
/// @f[ \frac{dP_i}{dt} = -a P_i \sum_k 1 + b \sum_k P_{ik} @f]
/// @f[ \frac{dP_{ij}}{dt} = a P_i - b P_{ij} @f] 
///
/// where k is summed over neighboring cells (which are assumed to be in
/// neighboe level 1), a,b are constants. The membranization is assumed to be
/// fast and the fixed point values are assumed
///
/// @f[ P_i = \frac{(b/a)*P}{(\sum_k a_k*1 + V_i*(b/a))} @f]
/// @f[ P_{ij} = \frac{P * 1}{(\sum_k a_k*1 + V_i*(b/a))} @f]
///
/// where P is the total amount of transport molecule in a cell (P_i*V_i), and
/// a_k is the membrane area (if those are given, otherwise a_k=1).
///
/// The polarized transport of a molecule X between a cell (i) and neighboring
/// walls (ij) is then given by
///
/// @f[ dX_i/dt = 1/V_i sum_j a_ij ( T_inf P_ij*f_ij*X_ij/(K+f_ij*X_ij) 
/// - T_eff P_ij*f_i*X_i/(K+f_i*X_i) ). @f]
/// @f[ dX_ij/dt = 1/V_ij sum_j a_ij ( -T_inf P_ij*f_ij*X_ij/(K+f_ij*X_ij) 
/// + T_eff P_ij*f_i*X_i/(K+f_i*X_i) ). @f]
///
/// The parameters given to this function are T_eff, T_inf and K'=b/a, , K,
/// f_i, and f_ij.  a_ij is the crossing area. The volumes for the cells are
/// also included for correct concentration update. The derivative is updated
/// from cellular compartments.
///
class PolarizationConstFastTransportFractionRestrictedMM : 
public BaseReaction {
  
 public:
  
  PolarizationConstFastTransportFractionRestrictedMM(std::vector<double> 
						     &paraValue, 
						     std::vector< 
						     std::vector<size_t> > 
						     &indValue );
  
  void derivs(Compartment &compartment,size_t species,
	      std::vector< std::vector<double> > &y,
	      std::vector< std::vector<double> > &dydt );
};

///
/// @brief Moves molecules activly with a MM type of equation and polarized
///
/// The transporter molecule P is divided into the cell cytosol (P_i) and its
/// membranes (P_ij) following the dynamics
///
/// @f[ dP_i/dt = -a P_i \sum_k f(A_k) + b \sum_k P_{ik} @f]
/// @f[ dP_{ij}/dt = a P_i f(A_j) - b P_{ij} @f] 
///
/// where k is summed over neighboring cells (which are assumed to be in
/// neighboe level 1), f(A_j)=((1-c)+cA_j^n/(Kh^n+a_j^n)) is the polarizing
/// signal and a,b are constants. The polarization is assumed to be fast and
/// the fixed point values are assumed
///  
/// @f[ P_i = (b/a)*P / (\sum_k a_k*f(A_k) + V_i*(b/a)) @f]
/// @f[ P_{ij} = P * f(A_j) / (\sum_k a_k*f(A_k) + V_i*(b/a)) @f]
///
/// where P is the total amount of polarized molecule in a cell (P_i*V_i), and
/// a_k is the membrane area (if those are given, otherwise a_k=1).
///  
/// The polarized transport of a molecule X between a cell (i) and neighboring
/// walls (ij) is then given by
///
/// @f[ dX_i/dt = 1/V_i sum_j a_ij ( T_inf P_ij*f_ij*X_ij/(K+f_ij*X_ij) 
/// - T_eff P_ij*f_i*X_i/(K+f_i*X_i) ). @f]
/// @f[ dX_ij/dt = 1/V_ij sum_j a_ij ( -T_inf P_ij*f_ij*X_ij/(K+f_ij*X_ij) 
/// + T_eff P_ij*f_i*X_i/(K+f_i*X_i) ). @f]
///
/// The parameters given to this function are T_eff, T_inf and K'=b/a, , K,
/// f_i, and f_ij.  a_ij is the crossing area. The volumes for the cells are
/// also included for correct concentration update. The derivative is updated
/// from cellular compartments.
///
class PolarizationConstHillFastTransportFractionRestrictedMM : 
public BaseReaction 
{
 public:
  
  PolarizationConstHillFastTransportFractionRestrictedMM(std::vector<double> 
							 &paraValue, 
							 std::vector< 
							 std::vector<size_t> > 
							 &indValue );
  
  void derivs(Compartment &compartment,size_t species,
	      std::vector< std::vector<double> > &y,
	      std::vector< std::vector<double> > &dydt );
};

/// @brief The class polarizationTransportFastGeneralFGRestricted
///
/// The Polarized transporter molecule \f$P\f$ is divided into the cell
/// cytosol (\f$P_i\f$) and its membranes (\f$P_{ij}\f$) following the
/// dynamics
///
///  \f[ \frac{dP_i}{dt} = - P_i sum_k f(A_{i,k}) + sum_k g(A_{i,k})P_{i,k}
///  \f]
///
/// \f[ \frac{dP_{i,j}}{dt} = P_i f(A_{i,j}) - P_{i,j} g(A_{i,j}) \f]
///
///  where \f$k\f$ is summed over neighboring cells, \f$A_i\f$ is the
///  polarizing signal in cell \f$i\f$ and \f$A_{i,j}\f$ is the polarizing
///  signal in the wall between cell \f$i\f$ and \f$j\f$. The polarization is
///  assumed to be fast and the fixed point values are assumed
///
///  \f[ P^\ast_{i,j} = \frac
///  {\frac{f(A_{i,j})}{g(A_{i,j})}P_{\textnormal{tot}}}
///  {1+sum_k\frac{f(A_{i,k})}{g(A_{i,k})}} \f]
///
///  where \f$P_{\textnormal{tot}}\f$ is the total amount of polarized
///  molecule in a cell.
///
///  The polarized transport of a molecule \f$A\f$ between two neighboring
///  compartments (of different types) are then given by
///  
///  \f[ \frac{dA_{i}}{dt} = - T A_i sum_k P^\ast_{i,k} \f] \f[
///  \frac{dA_{i,j}}{dt} = T(P^\ast_{i,j} A_i + P^\ast_{j,i} A_j) \f]
///
class PolarizationTransportFastGeneralFGRestricted : public BaseReaction
{
 public:
  PolarizationTransportFastGeneralFGRestricted(
					       std::vector<double> &paraValue, 
					       std::vector< std::vector<size_t> > &indValue);
  
  void  derivs(Compartment &compartment, size_t species,
	       std::vector< std::vector<double> > &y,
	       std::vector< std::vector<double> > &dydt);
  
  ///
  /// @brief Function to be used in PolarizationTransportFastGeneralFGRestricted
  ///
  double f(double arg);
  
  ///
  /// @brief Function to be used in PolarizationTransportFastGeneralFGRestricted
  ///
  double g(double arg);			       
};

#endif
