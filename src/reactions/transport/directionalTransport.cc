//
// Filename     : directionalTransport.cc
// Description  : Classes describing updates due to directional molecule transports
// Author(s)    : Henrik Jonsson (henrik@thep.lu.se)
// Created      : December 2007
// Revision     : $Id: transport.cc 324 2007-12-07 15:44:17Z pontus $
//
#include"directionalTransport.h"
#include"../baseReaction.h"

DirectionalTransport::
DirectionalTransport(std::vector<double> &paraValue, 
										 std::vector< std::vector<size_t> > &indValue ) {
  
  //Do some checks on the parameters and variable indeces
  //////////////////////////////////////////////////////////////////////
  if( paraValue.size()!=1 ) {
    std::cerr << "DirectionalTransport::DirectionalTransport() "
							<< "uses only one parameter T_strength.\n";
    exit(0);
  }
  if( (indValue.size() != 1 && indValue.size() != 2 ) || 
			indValue[0].size() != 1 || 
			(indValue.size()==2 && indValue[1].size() !=1)  ) {
    std::cerr << "DirectionalTransport::DirectionalTransport() "
							<< "directional signal index (x,y,z?) needed in level 1.\n" 
							<< "Variable index for size parameter could be at second level.\n";
    exit(0);
  }
  //Set the variable values
  //////////////////////////////////////////////////////////////////////
  setId("directionalTransport");
  setParameter(paraValue);  
  setVariableIndex(indValue);
  
  //Set the parameter identities
  //////////////////////////////////////////////////////////////////////
  std::vector<std::string> tmp( numParameter() );
  tmp.resize( numParameter() );
  tmp[0] = "T_strength";
  setParameterId( tmp );
}

void DirectionalTransport::derivs(Compartment &compartment,size_t species,
																	std::vector< std::vector<double> > &y,
																	std::vector< std::vector<double> > &dydt ) {
  
	size_t dimension = compartment.numTopologyVariable()-1;
  size_t i=compartment.index();
	double Vi=0,Vj=0;
	if( numVariableIndexLevel()>1 ) {
		Vi = y[i][variableIndex(1,0)];//Volume of the compartment
		if( Vi<=0.0 ) {
			std::cerr << "DirectionalTransport::derivs Given volume " << Vi
								<< " unphysical.\n";
			exit(-1);
		}
  }
  //Check all neighbors
  for( size_t n=0 ; n<compartment.numNeighbor() ; n++ ) {
    size_t j=compartment.neighbor(n);
    //Only transport from this cell if this condition is satisfied
    //(Otherwise transport in opposite direction) 
    if( parameter(0)*(y[j][variableIndex(0,0)]-y[i][variableIndex(0,0)]) 
				> 0.0 ) {
      if( numVariableIndexLevel()>1 ) {
				Vj = y[j][variableIndex(1,0)];//Volume of the compartment
				if( Vj<=0.0 ) {
					std::cerr << "DirectionalTransport::derivs Given volume " << Vj
										<< " unphysical.\n";
					exit(-1);
				}
      }
      //Directional coefficient normalization constant ( |xi-xj|/|ri-rj| )
      //Assuming positional variables in the beginning of a y-row
      double distance=0.0;
      for( size_t d=0 ; d<dimension ; d++ )
				distance += (y[i][d]-y[j][d])*(y[i][d]-y[j][d]);
      distance = sqrt( distance );
      double norm;
      if( distance>0.0 )
				norm = fabs(y[j][variableIndex(0,0)]-y[i][variableIndex(0,0)])
					/distance;
      else {
				std::cerr << "DirectionalTransport::derivs() "
									<< "distance between cells <= 0 ("
									<< distance << ")." << std::endl;
				exit(-1);
      }
      
      double coeff = y[i][species]*norm;
			if( numVariableIndexLevel()>1 ) {
				dydt[i][species] -= fabs( parameter(0) ) * coeff / Vi; 
				dydt[j][species] += fabs( parameter(0) ) * coeff / Vj;
			}
			else {
				dydt[i][species] -= fabs( parameter(0) ) * coeff;
				dydt[j][species] += fabs( parameter(0) ) * coeff;
			}
    }  
  }
}

DirectionalTransportMM::DirectionalTransportMM(std::vector<double> &paraValue, 
					       std::vector< std::vector<size_t> > 
					       &indValue ) {
  
  //Do some checks on the parameters and variable indeces
  //////////////////////////////////////////////////////////////////////
  if( paraValue.size()!=2 ) {
    std::cerr << "DirectionalTransportMM::DirectionalTransportMM() "
	      << "uses two parameters T_strength and K_M.\n";
    exit(0);
  }
  if( indValue.size() != 2 || indValue[0].size() !=1 
      || indValue[1].size() !=1  ) {
    std::cerr << "DirectionalTransportMM::DirectionalTransportMM() "
	      << "directional signal index (x,y,z?) needed in level 1.\n" 
	      << "Variable index for size parameter needed at second level.\n";
    exit(0);
  }
  //Set the variable values
  //////////////////////////////////////////////////////////////////////
  setId("directionalTransportMM");
  setParameter(paraValue);  
  setVariableIndex(indValue);
  
  //Set the parameter identities
  //////////////////////////////////////////////////////////////////////
  std::vector<std::string> tmp( numParameter() );
  tmp.resize( numParameter() );
  tmp[0] = "T_strength";
  tmp[1] = "K_M";
  setParameterId( tmp );
}

void DirectionalTransportMM::derivs(Compartment &compartment,size_t species,
				  std::vector< std::vector<double> > &y,
				  std::vector< std::vector<double> > &dydt ) {
  
  size_t i=compartment.index();
  double Vi = y[i][variableIndex(1,0)];//Volume of the compartment
  if( Vi<=0.0 ) {
    std::cerr << "DirectionalTransportMM::derivs Given volume " << Vi
	      << " unphysical.\n";
    exit(-1);
  }
  //Check all neighbors
  for( size_t n=0 ; n<compartment.numNeighbor() ; n++ ) {
    size_t j=compartment.neighbor(n);
    //Only transport from this cell if this condition is satisfied
    //(Otherwise transport in opposite direction) 
    if( parameter(0)*(y[j][variableIndex(0,0)]-y[i][variableIndex(0,0)]) 
	> 0.0 ) {
      
      double Vj = y[j][variableIndex(1,0)];//Volume of the compartment
      if( Vj<=0.0 ) {
	std::cerr << "DirectionalTransportMM::derivs Given volume " << Vj
		  << " unphysical.\n";
	exit(-1);

      }
      
      //Directional coefficient normalization constant ( |xi-xj|/|ri-rj| )
      //Assuming positional variables in the beginning of a y-row
      double distance=0.0;
      for( size_t d=0 ; d<compartment.numDimension() ; d++ )
	distance += (y[i][d]-y[j][d])*(y[i][d]-y[j][d]);
      distance = sqrt( distance );
      double norm;
      if( distance>0.0 )
	norm = fabs(y[j][variableIndex(0,0)]-y[i][variableIndex(0,0)])
	  /distance;
      else {
	std::cerr << "DirectionalTransportMM::derivs() "
		  << "distance between cells <= 0.\n";
	exit(-1);
      }
      
      double coeff = norm * y[i][species]/(y[i][species]+parameter(1));
      dydt[i][species] -= fabs( parameter(0) ) * coeff;
      dydt[j][species] += fabs( parameter(0) ) * coeff;
      //dydt[i][species] -= fabs( parameter(0) ) * coeff / Vi; 
      //dydt[j][species] += fabs( parameter(0) ) * coeff / Vj;
    }  
  }
}

DirectionalTransportSwitchedMM::
DirectionalTransportSwitchedMM(std::vector<double> &paraValue, 
			       std::vector< std::vector<size_t> > 
			       &indValue ) {
  
  //Do some checks on the parameters and variable indeces
  //////////////////////////////////////////////////////////////////////
  if( paraValue.size()!=3 ) {
    std::cerr << "DirectionalTransportSwitchedMM::"
	      << "DirectionalTransportSwitchedMM() "
	      << "uses three parameters T_strength and K_M Th_switch.\n";
    exit(0);
  }
  if( indValue.size() != 2 || indValue[0].size() !=2 
      || indValue[1].size() !=1  ) {
    std::cerr << "DirectionalTransportSwitchedMM::"
	      << "DirectionalTransportSwitchedMM() "
	      << "directional signal index (x,y,z) and switch molecule needed"
	      << " in level 1.\n" 
	      << "Variable index for size parameter needed at second level.\n";
    exit(0);
  }
  //Set the variable values
  //////////////////////////////////////////////////////////////////////
  setId("directionalTransportSwitchedMM");
  setParameter(paraValue);  
  setVariableIndex(indValue);
  
  //Set the parameter identities
  //////////////////////////////////////////////////////////////////////
  std::vector<std::string> tmp( numParameter() );
  tmp.resize( numParameter() );
  tmp[0] = "T_strength";
  tmp[1] = "K_M";
  tmp[2] = "Th_sw";
  setParameterId( tmp );
}

void DirectionalTransportSwitchedMM::
derivs(Compartment &compartment,size_t species,
       std::vector< std::vector<double> > &y,
       std::vector< std::vector<double> > &dydt ) {
  
  size_t i=compartment.index();
  //Check for the switched behavior
  //(Caveat:is only checked for this cell and not its neighbor...)
  double T = parameter(0);
  if( y[i][variableIndex(0,1)] > parameter(2) )
    T = -T;
  
  double Vi = y[i][variableIndex(1,0)];//Volume of the compartment
  if( Vi<=0.0 ) {
    std::cerr << "DirectionalTransportSwitchedMM::derivs Given volume " << Vi
	      << " unphysical.\n";
    exit(-1);
  }
  //Check all neighbors
  for( size_t n=0 ; n<compartment.numNeighbor() ; n++ ) {
    size_t j=compartment.neighbor(n);
    //Only transport from this cell if this condition is satisfied
    //(Otherwise transport in opposite direction) 
    if( T*(y[j][variableIndex(0,0)]-y[i][variableIndex(0,0)]) 
	> 0.0 ) {
      
      double Vj = y[j][variableIndex(1,0)];//Volume of the compartment
      if( Vj<=0.0 ) {
	std::cerr << "DirectionalTransportSwitchedMM::derivs Given volume " 
		  << Vj << " unphysical.\n";
	exit(-1);
      }
      
      //Directional coefficient normalization constant ( |xi-xj|/|ri-rj| )
      //Assuming positional variables in the beginning of a y-row
      double distance=0.0;
      for( size_t d=0 ; d<compartment.numDimension() ; d++ )
	distance += (y[i][d]-y[j][d])*(y[i][d]-y[j][d]);
      distance = sqrt( distance );
      double norm;
      if( distance>0.0 )
	norm = fabs(y[j][variableIndex(0,0)]-y[i][variableIndex(0,0)])
	  /distance;
      else {
	std::cerr << "DirectionalTransportSwitchedMM::derivs() "
		  << "distance between cells <= 0.\n";
	exit(-1);
      }
      
      double coeff = norm * y[i][species]/(y[i][species]+parameter(1));
      dydt[i][species] -= std::fabs(T)*coeff;
      dydt[j][species] += std::fabs(T)*coeff;
      //dydt[i][species] -= fabs( parameter(0) ) * coeff / Vi; 
      //dydt[j][species] += fabs( parameter(0) ) * coeff / Vj;
    }  
  }
}



