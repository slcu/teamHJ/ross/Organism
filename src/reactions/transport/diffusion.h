//
// Filename     : diffusion.h
// Description  : Classes describing diffusional transports of molecules
// Author(s)    : Henrik Jonsson (henrik@thep.lu.se)
// Created      : December 2007
// Revision     : $Id: transport.h 203 2007-03-20 10:50:07Z henrik $
//
#ifndef DIFFUSION_H
#define DIFFUSION_H

#include<cmath>

#include"../baseReaction.h"

///
/// @brief The class Diffusion is a diffusion update including topological properties
///
/// Generates a diffusion update where the topological properties such as
/// volumes are taken into account according to:
///
/// @f[ \frac{dy_{ij}}{dt} = \frac{A_n}{d_n} D (y_{nj}-y_{ij})/V_i @f]
///
/// and 
///
/// @f[ \frac{dy_{nj}}{dt} = \frac{A_n}{d_n} D (y_{nj}-y_{ij})/V_n. @f]
///
/// where \e j is the molecular index. The update only applies if the neighbor
/// index \e n is larger than \e i. The update needs the size index as a
/// variable index (V_i,V_n). If applicable the A_n and d_n is the cross
/// section area and distance between the compartments.
///
class Diffusion : public BaseReaction {
  
 public:
  
  ///
  /// @brief Main constructor
  ///
  /// This is the main constructor which sets the parameters and variable
  /// indices that defines the reaction.
  ///
  /// @param paraValue vector with parameters
  ///
  /// @param indValue vector of vectors with variable indices
  ///
  /// @see BaseReaction::createReaction(std::vector<double> &paraValue,...)
  ///
  Diffusion(std::vector<double> &paraValue, 
	    std::vector< std::vector<size_t> > &indValue );
  
  ///
  /// @brief Derivative function for this reaction class
  ///
  /// @see BaseReaction::derivs(Compartment &compartment,size_t species,...)
  ///
  void derivs(Compartment &compartment,size_t species,DataMatrix &y,DataMatrix &dydt);
};

///
/// @brief The class DiffusionSimple represents the simplest diffusion update
/// NOT including topological properties
///
/// Generates a diffusion update where NO topological properties such as
/// volumes are taken into account according to:
///
/// @f[ \frac{dy_{ij}}{dt} = D (y_{nj}-y_{ij}) @f]
///
/// and 
///
/// @f[ \frac{dy_{nj}}{dt} = D (y_{nj}-y_{ij}) @f]
///
/// where \e j is the molecular index. The update only applies if the neighbor
/// index \e n is larger than \e i.
///
class DiffusionSimple : public BaseReaction {
  
 public:
  
  ///
  /// @brief Main constructor
  ///
  /// This is the main constructor which sets the parameters and variable
  /// indices that defines the reaction.
  ///
  /// @param paraValue vector with parameters
  ///
  /// @param indValue vector of vectors with variable indices
  ///
  /// @see BaseReaction::createReaction(std::vector<double> &paraValue,...)
  ///
  DiffusionSimple(std::vector<double> &paraValue, 
		  std::vector< std::vector<size_t> > &indValue );
  
  ///
  /// @brief Derivative function for this reaction class
  ///
  /// @see BaseReaction::derivs(Compartment &compartment,size_t species,...)
  ///
  void derivs(Compartment &compartment,size_t species,DataMatrix &y,DataMatrix &dydt);

  ///
  /// @brief Derivative function for this reaction class calculating the absolute value for noise solvers
  ///
  /// @see BaseReaction::derivsWithAbs(Compartment &compartment,size_t species,...)
  ///
  void derivsWithAbs(Compartment &compartment,size_t species,DataMatrix &y,DataMatrix &dydt,DataMatrix &sdydt);

  ///
  /// @brief Jacobian function for this reaction class
  ///
  /// @see BaseReaction::Jacobian(Compartment &compartment,size_t species,...)
  ///
  size_t Jacobian(Compartment &compartment,size_t species,DataMatrix &y,JacobianMatrix &A);

  ///
  /// @brief Propensity calculation (probability to happen in [t,t+dt]) for this reaction class
  ///
  /// @see BaseReaction::propensity(Compartment &compartment,size_t species,DataMatrix &y)
  ///
  double propensity(Compartment &compartment,size_t species,DataMatrix &y);

  ///
  /// @brief Discrete update for this reaction used in stochastic simulations
  ///
  /// @see BaseReaction::discreteUpdate(Compartment &compartment,size_t species,DataMatrix &y)
  ///
  void discreteUpdate(Compartment &compartment,size_t species,DataMatrix &y);
  ///
  /// @brief Prints the reaction in Cambium format
  ///
  /// @see BaseReaction::printCambium()
  ///
  void printCambium( std::ostream &os, size_t varIndex ) const;
};

/// @brief The class DiffusionSimpleSpatial is the simplest diffusion update
///
/// Deriving the time derivative contribution from a degradation using the
/// values in y and add the results to dydt. Compartment is the cell and
/// species is the index of the variable to be updated. The parameter is D in
/// dc/dt = D*(c_n-c) (and dc_n/dt = -D*(c_n-c)). The update only applys if
/// the neighbor index is larger.  
///
class DiffusionSimpleSpatial : public BaseReaction {
  
 public:
  
  DiffusionSimpleSpatial(std::vector<double> &paraValue, 
			 std::vector< std::vector<size_t> > &indValue );
  
  void derivs(Compartment &compartment,size_t species,DataMatrix &y,DataMatrix &dydt);
};

///
/// @brief The class DiffusionRestricted is diffusion between specific compartments
///
/// A restricted diffusion (btw allowed compartm). This is the same as
/// diffusion but is restricted to only apply between restricted compartments
/// (e.g. cells and walls, or cells and cells) marked in variableIndex (0,0)
/// and (0,1).
///
class DiffusionRestricted : public BaseReaction {
  
 public:
  
  DiffusionRestricted(std::vector<double> &paraValue, 
		      std::vector< std::vector<size_t> > &indValue );
  
  void derivs(Compartment &compartment,size_t species,DataMatrix &y,DataMatrix &dydt);

  ///
  /// @brief Derivative function for this reaction class calculating the absolute value for noise solvers
  ///
  /// @see BaseReaction::derivsWithAbs(Compartment &compartment,size_t species,...)
  ///
  void derivsWithAbs(Compartment &compartment,size_t species,DataMatrix &y,DataMatrix &dydt,DataMatrix &sdydt);

};

/// @brief The class DiffusionSimpleRestricted is diffusion between specific compartments
///
/// This is the same as diffusion but is restricted to only apply between
/// restricted compartments (e.g. cells and walls, or cells and cells) marked
/// in variableIndex (0,0) and (0,1).
///
class DiffusionSimpleRestricted : public BaseReaction {
  
 public:
  
  DiffusionSimpleRestricted(std::vector<double> &paraValue, 
			    std::vector< std::vector<size_t> > &indValue );
  
  void derivs(Compartment &compartment,size_t species,DataMatrix &y,DataMatrix &dydt);
};

#endif
