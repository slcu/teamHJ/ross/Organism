//
// Filename     : diffusion.cc
// Description  : Classes describing transport via diffusion 
// Author(s)    : Henrik Jonsson (henrik@thep.lu.se)
// Created      : December 2007
// Revision     : $Id: transport.cc 324 2007-12-07 15:44:17Z pontus $
//
#include"diffusion.h"
#include"../baseReaction.h"
#include"../../common/myRandom.h"
#include"../../organism.h"

Diffusion::Diffusion(std::vector<double> &paraValue, 
		     std::vector< std::vector<size_t> > 
		     &indValue ) {
  
  // Do some checks on the parameters and variable indeces
  //
  if( paraValue.size()!=1 ) {
    std::cerr << "Diffusion::Diffusion() "
	      << "Uses only one parameter D.\n";
    exit(0);
  }
  if( indValue.size() != 1 || indValue[0].size() !=1 ) {
    std::cerr << "Diffusion::Diffusion() "
	      << "Variable index for size parameter needed.\n";
    exit(0);
  }
  // Set the variable values
  //
  setId("diffusion");
  setParameter(paraValue);  
  setVariableIndex(indValue);
  
  // Set the parameter identities
  //
  std::vector<std::string> tmp( numParameter() );
  tmp.resize( numParameter() );
  tmp[0] = "D";
  setParameterId( tmp );
}

void Diffusion::
derivs(Compartment &compartment,size_t species,DataMatrix &y,DataMatrix &dydt) 
{    
  size_t i=compartment.index();
  double Vi = y[i][variableIndex(0,0)];//Volume of the compartment
  if( Vi<=0.0 ) {
    std::cerr << "Diffusion::derivs Given volume " << Vi 
	      << " unphysical.\n";
    exit(-1);
  }  
  for( size_t n=0 ; n<compartment.numNeighbor() ; n++ ) {
    size_t j=compartment.neighbor(n);
    //Only update if compartments index less than neighbors
    if( i<j ) {
      double Vj = y[j][variableIndex(0,0)];//Volume of the compartment neigh
      if( Vj<=0.0 ) {
	std::cerr << "Diffusion::derivs Given volume " << Vj
		  << " unphysical.\n";
	exit(-1);
      }
      //If no neighbor area defined assume area and distance 1
      double distance = 1.0,area=1.0;
      if( compartment.numNeighborArea()>n ) {
	area = compartment.neighborArea(n);
	distance=0.0;
	size_t dimension=compartment.numTopologyVariable()-1;
	for (size_t d=compartment.topologyStart(); d<dimension; d++ )
	  distance += (y[j][d]-y[i][d])*(y[j][d]-y[i][d]);
	//Check that distance is larger than zero
	if( distance<=0 ) {
	  std::cerr << "Diffusion::derivs Calculated distance " 
		    << distance << " unphysical.\n";
	  exit(-1);
	}
	distance = sqrt(distance);
      }
      double diff = y[j][species]-y[i][species];
      dydt[i][species] += parameter(0)*diff*area/(Vi*distance);
      dydt[j][species] -= parameter(0)*diff*area/(Vj*distance);
    }
  }
}

DiffusionSimple::DiffusionSimple(std::vector<double> &paraValue, 
				 std::vector< std::vector<size_t> > &indValue ) 
{  
  //
  // Do some checks on the parameters and variable indeces
  //
  if( paraValue.size()!=1 ) {
    std::cerr << "DiffusionSimple::DiffusionSimple() "
	      << "Uses only one parameter D.\n";
    exit(0);
  }
  if( indValue.size() != 0 ) {
    std::cerr << "DiffusionSimple::DiffusionSimple() "
	      << "No variable index used.\n";
    exit(0);
  }
  //
  // Set the variable values
  //
  setId("diffusionSimple");
  setParameter(paraValue);  
  setVariableIndex(indValue);
  //
  // Set the parameter identities
  //
  std::vector<std::string> tmp( numParameter() );
  tmp.resize( numParameter() );
  tmp[0] = "D";
  setParameterId( tmp );
}

void DiffusionSimple::
derivs(Compartment &compartment,size_t species,DataMatrix &y,DataMatrix &dydt) 
{    
  size_t i=compartment.index();
  for( size_t n=0 ; n<compartment.numNeighbor() ; n++ ) {
    size_t j=compartment.neighbor(n);
    //Only update if compartments index less than neighbors
    if( i<j ) {
      double diff = parameter(0)*(y[j][species]-y[i][species]);
      dydt[i][species] += diff;
      dydt[j][species] -= diff;
    }
  }
}

void DiffusionSimple::
derivsWithAbs(Compartment &compartment,size_t species,DataMatrix &y,DataMatrix &dydt,DataMatrix &sdydt) 
{    
  size_t i=compartment.index();
  for( size_t n=0 ; n<compartment.numNeighbor() ; n++ ) {
    size_t j=compartment.neighbor(n);
    //Only update if compartments index less than neighbors
    if( i<j ) {
      double diff = parameter(0)*(y[j][species]-y[i][species]);
      dydt[i][species] += diff;
      dydt[j][species] -= diff;
      sdydt[i][species] += std::fabs(diff);
      sdydt[j][species] += std::fabs(diff);
    }
  }
}

size_t DiffusionSimple::
Jacobian(Compartment &compartment,size_t species,DataMatrix &y,JacobianMatrix &A) 
{    
  size_t ci=compartment.index();
  for (size_t n=0; n<compartment.numNeighbor(); ++n) {
    size_t cj=compartment.neighbor(n);
    //Only update if compartments index less than neighbors
    if( ci<cj ) {
      size_t i = ci*y[0].size() + species; 
      size_t j = ci*y[0].size() + species; 
      A(i,i) -= parameter(0);
      A(j,j) -= parameter(0);
      A(i,j) += parameter(0);
      A(j,i) += parameter(0);
    }
  }
  return 1;
}

double DiffusionSimple::
propensity(Compartment &compartment,size_t species,DataMatrix &y)
{
  return parameter(0)*y[compartment.index()][species]*compartment.numNeighbor();
}

void DiffusionSimple::
discreteUpdate(Compartment &compartment,size_t species,DataMatrix &y)
{
	size_t neigh = static_cast<size_t> (myRandom::ran3()*compartment.numNeighbor());
	y[compartment.index()][species] -= 1.0;
	y[compartment.neighbor(neigh)][species] += 1.0;
}

void DiffusionSimple::printCambium( std::ostream &os, size_t varIndex ) const
{
  std::string varName=organism()->variableId(varIndex);
  os << "Arrow[Organism[" << id() << "],{"
     << varName << "[i]},{},{"
     << varName << "[j]},";
  os << "Parameters["
     << parameter(0);
  os << "], ParameterNames[" << parameterId(0) << "], VarIndices[],"; 
  os << "Solve{" << varName << "[i]\' =  - p_0 ("
     << varName << "[i] - "
     << varName << "[j])"
     <<", ";
  os << varName << "[j]\' =   p_0 ("
     << varName << "[i] - "
     << varName << "[j])"
     <<", "
     << "Using[{nbr,"
     << organism()->topology().id() << "::Cell, i,"
     << organism()->topology().id() << "::Cell, j}"
     << "}";
  os << "]" << std::endl;
}

DiffusionSimpleSpatial::DiffusionSimpleSpatial(std::vector<double> &paraValue, 
					       std::vector< std::vector<size_t> > &indValue ) 
{  
  //
  // Do some checks on the parameters and variable indeces
  //
  if( paraValue.size()!=1 ) {
    std::cerr << "DiffusionSimpleSpatial::DiffusionSimpleSpatial() "
	      << "Uses only one parameter D.\n";
    exit(0);
  }
  if( indValue.size() != 0 ) {
    std::cerr << "DiffusionSimpleSpatial::DiffusionSimpleSpatial() "
	      << "No variable index used.\n";
    exit(0);
  }
  //
  // Set the variable values
  //
  setId("diffusionSimpleSpatial");
  setParameter(paraValue);  
  setVariableIndex(indValue);
  //
  // Set the parameter identities
  //
  std::vector<std::string> tmp( numParameter() );
  tmp.resize( numParameter() );
  tmp[0] = "D";
  setParameterId( tmp );
}

void DiffusionSimpleSpatial::
derivs(Compartment &compartment,size_t species,DataMatrix &y,DataMatrix &dydt) 
{  
  size_t i=compartment.index();
  for( size_t n=0 ; n<compartment.numNeighbor() ; n++ ) {
    size_t j=compartment.neighbor(n);
    //Only update if compartments index less than neighbors
    if( i<j ) {
      //Area calculation
      double area;
      double d2=0.0;
      for( size_t dim=0 ; dim<compartment.numDimension() ; dim++ )
				d2 += (y[i][dim]-y[j][dim])*(y[i][dim]-y[j][dim]);
      double d = std::sqrt(d2);
      double ri=y[i][compartment.numDimension()];
      double rj=y[j][compartment.numDimension()];
      if( d>ri+rj ) area=0.0;
      else if( d<ri || d<rj ) {
	area = ri<rj ? ri : rj;
      }
      else
	area = std::sqrt( 0.25*(d2-2.*(ri*ri-rj*rj)*(ri*ri-rj*rj) +
				(ri-rj)*(ri-rj)/d2 ) );
      double diff = y[j][species]-y[i][species];
      dydt[i][species] += parameter(0)*area*diff;
      dydt[j][species] -= parameter(0)*area*diff;
    }
  }
}

DiffusionRestricted::DiffusionRestricted(std::vector<double> &paraValue, 
								 std::vector< std::vector<size_t> > 
								 &indValue )
{
	//Do some checks on the parameters and variable indeces
	//////////////////////////////////////////////////////////////////////
	if (paraValue.size() != 2) {
		std::cerr << "DiffusionRestricted::DiffusionRestricted() "
				<< "Uses two parameters D_out and D_in.\n";
		exit(EXIT_FAILURE);
	}
	if (indValue.size() != 2 || indValue[0].size() != 2 || 
	    indValue[1].size() != 1) {
		std::cerr << "DiffusionRestricted::DiffusionRestricted() "
				<< "Variable indices for two restrictMarkers, and size"
				<< " (second level) needed.\n";
		exit(EXIT_FAILURE);
	}
	if (indValue[0][0] == indValue[0][1] && paraValue[0] != paraValue[1]) {
		std::cerr << "DiffusionRestricted::DiffusionRestricted() "
				<< "Same type of compartments requires same D_out and D_in.\n";
		exit(EXIT_FAILURE);
	}
	//Set the variable values
	//////////////////////////////////////////////////////////////////////
	setId("diffusionRestricted");
	setParameter(paraValue);  
	setVariableIndex(indValue);
	
	//Set the parameter identities
	//////////////////////////////////////////////////////////////////////
	std::vector<std::string> tmp(numParameter());
	tmp.resize(numParameter());
	tmp[0] = "D_out";
	tmp[1] = "D_in";
	setParameterId(tmp);
}

void DiffusionRestricted::
derivs(Compartment &compartment,size_t species,DataMatrix &y,DataMatrix &dydt)
{
	size_t i = compartment.index();
	//Check that this compartment is marked for this diffusion
	if (y[i][variableIndex(0,0)] < 0.5)
		return;

	static size_t dimension = compartment.numTopologyVariable() - 1; // Static?
	
	//Extract the volume of the compartment and assure it is physical
	double Vi = y[i][variableIndex(1,0)];
	assert( Vi>0.0 );
	for (size_t n = 0; n < compartment.numNeighbor(); n++) {
		size_t j = compartment.neighbor(n);
		//Only update if neighbor marked for this diffusion
		//If same type of compartments only update once (i>j)
		if (y[j][variableIndex(0,1)] > 0.5) {
			if (variableIndex(0,0) != variableIndex(0,1) || i > j) {
				
				//Get volume of neighbor and assure it is physical
				double Vj = y[j][variableIndex(1,0)];
				assert( Vj>0.0 );
				
				//If no neighbor area defined assume area and distance 1
				double distance = 1.0, area = 1.0;
				if (compartment.numNeighborArea() > n) {
					area = compartment.neighborArea(n);
					distance = 0.0;
					for (size_t d = compartment.topologyStart(); d < dimension; d++)
						distance += (y[j][d] - y[i][d]) * (y[j][d] - y[i][d]);
					assert( distance>0.0 );
					distance = std::sqrt(distance);
					assert( distance>0.0 );
				}
			
				//D_out
				double tmp = parameter(0) * y[i][species] * area / distance;
				dydt[i][species] -= tmp / Vi;
				dydt[j][species] += tmp / Vj;
				//D_in
				tmp = parameter(1) * y[j][species] * area / distance;
				dydt[i][species] += tmp / Vi;
				dydt[j][species] -= tmp / Vj;
			}  
		}
	}
}

void DiffusionRestricted::
derivsWithAbs(Compartment &compartment,size_t species,DataMatrix &y,DataMatrix &dydt, DataMatrix &sdydt)
{
	size_t i = compartment.index();
	//Check that this compartment is marked for this diffusion
	if (y[i][variableIndex(0,0)] < 0.5)
		return;

	static size_t dimension = compartment.numTopologyVariable() - 1; // Static?
	
	//Extract the volume of the compartment and assure it is physical
	double Vi = y[i][variableIndex(1,0)];
	assert( Vi>0.0 );
	for (size_t n = 0; n < compartment.numNeighbor(); n++) {
		size_t j = compartment.neighbor(n);
		//Only update if neighbor marked for this diffusion
		//If same type of compartments only update once (i>j)
		if (y[j][variableIndex(0,1)] > 0.5) {
			if (variableIndex(0,0) != variableIndex(0,1) || i > j) {
				
				//Get volume of neighbor and assure it is physical
				double Vj = y[j][variableIndex(1,0)];
				assert( Vj>0.0 );
				
				//If no neighbor area defined assume area and distance 1
				double distance = 1.0, area = 1.0;
				if (compartment.numNeighborArea() > n) {
					area = compartment.neighborArea(n);
					distance = 0.0;
					for (size_t d = compartment.topologyStart(); d < dimension; d++)
						distance += (y[j][d] - y[i][d]) * (y[j][d] - y[i][d]);
					assert( distance>0.0 );
					distance = std::sqrt(distance);
					assert( distance>0.0 );
				}
			
				//D_out
				double tmp = parameter(0) * y[i][species] * area / distance;
				dydt[i][species] -= tmp / Vi;
				dydt[j][species] += tmp / Vj;

				sdydt[i][species] += std::fabs(tmp / Vi);
				sdydt[j][species] += std::fabs(tmp / Vj);
				
				//D_in
				tmp = parameter(1) * y[j][species] * area / distance;
				dydt[i][species] += tmp / Vi;
				dydt[j][species] -= tmp / Vj;

				sdydt[i][species] += std::fabs(tmp / Vi);
				sdydt[j][species] += std::fabs(tmp / Vj);

			}  
		}
	}
}


DiffusionSimpleRestricted::DiffusionSimpleRestricted(
  std::vector<double> &paraValue, 
  std::vector< std::vector<size_t> > &indValue) 
{
 
  //Do some checks on the parameters and variable indeces
  //////////////////////////////////////////////////////////////////////
  if (paraValue.size() != 2) {
    std::cerr << "DiffusionSimpleRestricted::DiffusionSimpleRestricted() "
	      << "Uses two parameters D_out and D_in.\n";
    exit(0);
  }
  if (indValue[0].size() != 2) {
    std::cerr << "DiffusionRestricted::DiffusionRestricted() "
	      << "Variable indices for two restrictMarkers.\n";
    exit(0);
  }
  if (indValue[0][0] == indValue[0][1] && paraValue[0] != paraValue[1]) {
    std::cerr << "DiffusionRestricted::DiffusionRestricted() "
	      << "Same type of compartments requires same D_out and D_in.\n";
    exit(0);
  }
  //Set the variable values
  //////////////////////////////////////////////////////////////////////
  setId("diffusionSimpleRestricted");
  setParameter(paraValue);  
  setVariableIndex(indValue);
  
  //Set the parameter identities
  //////////////////////////////////////////////////////////////////////
  std::vector<std::string> tmp( numParameter() );
  tmp.resize(numParameter());
  tmp[0] = "D_out";
  tmp[1] = "D_in";
  setParameterId(tmp);
}

void DiffusionSimpleRestricted::
derivs(Compartment &compartment,size_t species,DataMatrix &y,DataMatrix &dydt)
{  
  size_t i = compartment.index();

  //Check that this compartment is marked for this diffusion
  if (y[i][variableIndex(0,0)] < 0.5)
    return;

  for (size_t n = 0; n < compartment.numNeighbor(); n++) {
    size_t j = compartment.neighbor(n);

    //Only update if neighbor marked for this diffusion
    //If same type of compartments only update once (i>j)

    if (y[j][variableIndex(0,1)] > 0.5) {
      if (variableIndex(0,0) != variableIndex(0,1) || i > j) {
	//D_out
	double tmp = parameter(0) * y[i][species];
	dydt[i][species] -= tmp;
	dydt[j][species] += tmp;
	//D_in
	tmp = parameter(1) * y[j][species];
	dydt[i][species] += tmp;
	dydt[j][species] -= tmp;
      }  
    }
  }
}



