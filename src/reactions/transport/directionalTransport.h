//
// Filename     : directionalTransport.h
// Description  : Classes describing directional transports of molecules
// Author(s)    : Henrik Jonsson (henrik@thep.lu.se)
// Created      : December 2007
// Revision     : $Id: transport.h 203 2007-03-20 10:50:07Z henrik $
//
#ifndef DIRECTIONALTRANSPORT_H
#define DIRECTIONALTRANSPORT_H

#include<cmath>

#include"../baseReaction.h"

///
/// @brief The class directionalTransport moves molecules in a specific direction
///
/// A strength constant, and a direction variable index is needed. If the
/// constant is less than zero the transport is in the negative direction. The
/// transport is proportional with the molecular content, and normalized as
/// direction length divided by total length between the two cells.
///
/// The parameter is T_strength in 
///
/// @f[ dc/dt = -|T_strength|*c*|x-x_n|/|r-r_n|*V_m/V*\Theta @f]
///
/// and
///
/// @f[ dc_n/dt = -dc/dt @f] 
///
/// The absolute value of the parameter concerns the reversed direction if the
/// parameter is negative. V_m is the average volume. x is the direction
/// given, r is the cell position, and c is the concentration of the
/// transported species. The Theta result in an application only if transport
/// out from this cell. The update will be mirrored by a similar from j to i
/// (if that is the direction instead). The update needs the directional
/// variable as index in the first level and the size index as a variable
/// index in the second level.
///
class DirectionalTransport : public BaseReaction {
  
 public:
  
  DirectionalTransport(std::vector<double> &paraValue, 
		       std::vector< std::vector<size_t> > &indValue );
  
  void derivs(Compartment &compartment,size_t species,
	      std::vector< std::vector<double> > &y,
	      std::vector< std::vector<double> > &dydt );
};

///
/// @brief The class directionalTransport moves molecules in a specific direction
///
/// A strength constant, and a direction variable index is needed. If the
/// constant is less than zero the transport is in the negative direction. The
/// transport is proportional with the molecular content, and normalized as
/// direction length divided by total length between the two cells. The
/// transport equation is in the Michaelis Menten formalism (which is the only
/// thing that differs from the DirectionalTransport class).
///
/// The parameter is T_strength in 
///
/// @f[ dc/dt = -|T_strength|*c/(c+K_M)*|x-x_n|/|r-r_n|/V * \Theta @f]
/// 
/// and 
/// 
/// @f[ dc_n/dt = -dc/dt @f]. 
/// 
/// The absolute value of the parameter concerns the reversed direction if the
/// parameter is negative. V_m is the average volume. x is the direction
/// given, r is the cell position, and c is the concentration of the
/// transported species. The Theta result in an application only if transport
/// out from this cell. The update will be mirrored by a similar from j to i
/// (if that is the direction instead). The update needs the directional
/// variable as index in the first level and the size index as a variable
/// index in the second level.
///
class DirectionalTransportMM : public BaseReaction {
  
 public:
  
  DirectionalTransportMM(std::vector<double> &paraValue, 
			 std::vector< std::vector<size_t> > &indValue );
  
  void derivs(Compartment &compartment,size_t species,
	      std::vector< std::vector<double> > &y,
	      std::vector< std::vector<double> > &dydt );
};

///
/// @brief The class directionalTransportSwitchedMM moves molecules in a
/// specific direction
///
/// A strength constant, and a direction variable index is needed. If the
/// constant is less than zero the transport is in the negative direction. The
/// transport is proportional with the molecular content, and normalized as
/// direction length divided by total length between the two cells. The
/// transport equation is in the Michaelis Menten formalism. A molecule switch
/// is implemented to chnge the direction in some cells. (which is the only
/// thing that differs from the DirectionalTransportMM class).
///
/// The parameter is T_strength in 
/// 
/// @f[ dc/dt = -|T_strength|*c/(c+K_M)*|x-x_n|/|r-r_n|/V * \Theta @f]
///
/// and
/// 
/// @f[ dc_n/dt = -dc/dt. @f]
/// 
/// The absolute value of the parameter concerns the reversed direction if the
/// parameter is negative. The switching is checking for a molecule
/// concentration and reverses the direction if the concentration is above a
/// threshold. V_m is the average volume. x is the direction given, r is the
/// cell position, and c is the concentration of the transported species. The
/// Theta result in an addition only if transport out from this cell. The
/// update will be mirrored by a similar from j to i (if that is the direction
/// instead). The update needs the directional variable as index in the first
/// level and the size index as a variable index in the second level.
///
class DirectionalTransportSwitchedMM : public BaseReaction {
  
 public:
  
  DirectionalTransportSwitchedMM(std::vector<double> &paraValue, 
				 std::vector< std::vector<size_t> > &indValue );
  
  void derivs(Compartment &compartment,size_t species,
	      std::vector< std::vector<double> > &y,
	      std::vector< std::vector<double> > &dydt );
};

#endif
