///
/// Filename     : polarizationTransport.cc
/// Description  : Classes describing updates due to polarized molecule transports
/// Author(s)    : Henrik Jonsson (henrik@thep.lu.se)
/// Created      : December 2007
/// Revision     : $Id: transport.cc 324 2007-12-07 15:44:17Z pontus $
///
#include"polarizationTransport.h"
#include"../baseReaction.h"
#include"../../common/myRandom.h"

namespace PolarizationTransportFast
{

  Lin::Lin(std::vector<double> 
	   &paraValue, 
	   std::vector< 
	     std::vector<size_t> > 
	   &indValue ) {
    
  // Do some checks on the parameters and variable indeces
  //
    if( paraValue.size()!=2 ) {
      std::cerr << "Lin::Lin() "
		<< "uses two parameters (D and alpha/beta).\n";
      exit(0);
    }
    if( indValue.size() != 2 || indValue[0].size() !=2 
	|| indValue[1].size() !=1  ) {
      std::cerr << "PolarizationTransportFast::Lin::() "
		<< "Polarized molecule + polarization signal indeces needed in "
		<< "level 1.\n" 
		<< "Variable index for size parameter needed at second level.\n";
      exit(0);
    }
    // Set the variable values
    //
    setId("polarizationTransportFast::Lin");
    setParameter(paraValue);  
    setVariableIndex(indValue);
    
    // Set the parameter identities
    //
    std::vector<std::string> tmp( numParameter() );
    tmp.resize( numParameter() );
    tmp[0] = "T";
    tmp[1] = "b/a";
    setParameterId( tmp );
  }

  void Lin::derivs(Compartment &compartment,size_t species,
		   std::vector< std::vector<double> > &y,
		   std::vector< std::vector<double> > 
		   &dydt ) {
    
    size_t i=compartment.index();
    double Vi = y[i][variableIndex(1,0)];//Volume of the compartment
    if( Vi<=0.0 ) {
      std::cerr << "PolarizationTransportFast::Lin::derivs() Given volume " << Vi
		<< " unphysical.\n";
      exit(-1);
    }
    
    std::vector<double> pij;
    Pij(compartment,species,y,pij);
    
    for( size_t n=0 ; n<compartment.numNeighbor() ; n++ ) {
      size_t j=compartment.neighbor(n);
      double Vj = y[j][variableIndex(1,0)];//Volume of the compartment
      if( Vj<=0.0 ) {
	std::cerr << "PolarizationTransportFast::Lin::derivs() Given volume " << Vj
		  << " unphysical.\n";
	exit(-1);
      }
      
      double coeff = y[i][species]*pij[n];
      //dydt[i][species] -= parameter(0)*coeff/Vi;
      //dydt[j][species] += parameter(0)*coeff/Vj;
      dydt[i][species] -= parameter(0)*coeff;
      dydt[j][species] += parameter(0)*coeff;
    }
  }

  void Lin::derivsWithAbs(Compartment &compartment,size_t species,
			  DataMatrix &y,
			  DataMatrix &dydt, 
			  DataMatrix &sdydt ) 
  {
    
    size_t i=compartment.index();
    double Vi = y[i][variableIndex(1,0)];//Volume of the compartment
    if( Vi<=0.0 ) {
      std::cerr << "PolarizationTransportFast::Lin::derivs() Given volume " << Vi
		<< " unphysical." << std::endl;
      exit(-1);
    }
    
    std::vector<double> pij;
    Pij(compartment,species,y,pij);
    
    for( size_t n=0 ; n<compartment.numNeighbor() ; n++ ) {
      size_t j=compartment.neighbor(n);
      double Vj = y[j][variableIndex(1,0)];//Volume of the compartment
      if( Vj<=0.0 ) {
	std::cerr << "PolarizationTransportFast::Lin::derivs() Given volume " << Vj
		  << " unphysical." << std::endl;
	exit(-1);
      }
      
      double coeff = y[i][species]*pij[n];
      //dydt[i][species] -= parameter(0)*coeff/Vi;
      //dydt[j][species] += parameter(0)*coeff/Vj;
      //sdydt[i][species] += parameter(0)*coeff/Vi;
      //sdydt[j][species] += parameter(0)*coeff/Vj;
      double rate = parameter(0)*coeff;
      dydt[i][species] -= rate;
      dydt[j][species] += rate;
      sdydt[i][species] += rate;
      sdydt[j][species] += rate;
    }
  }
  
  double Lin::Lin::
  propensity(Compartment &compartment,size_t species,DataMatrix &y)
  {
    std::vector<double> pij;
    double pSum = Pij(compartment,species,y,pij);    
    return parameter(0)*pSum*y[compartment.index()][species];
  }
  
  void Lin::Lin::
  discreteUpdate(Compartment &compartment,size_t species,DataMatrix &y)
  {
    std::vector<double> pij;
    double pSum = Pij(compartment,species,y,pij);    
    double val = myRandom::ran3()*pSum;

    // finding the correct reaction
    size_t neigh=0;
    double low=0.0,high=pij[0];
    if (val<=pij[0]) {
      neigh=0;
      //std::cerr << neigh << " 0 < " << pSum << " < " << pij[0] << std::endl;
    }
    else {
      for(size_t n=1; n<compartment.numNeighbor(); ++n) {
	low += pij[n-1];	
	high += pij[n];
	if ((val<=high) && (val>low)) {
	  neigh=n;
	  //std::cerr << n << " " << high << " > " << val << " > " << low << std::endl;
	  break;
	}
      }
    }    
    y[compartment.index()][species] -= 1.0;
    y[compartment.neighbor(neigh)][species] += 1.0;
  }  
  
  double Lin::Lin::
  Pij(Compartment &compartment, size_t species, DataMatrix &y, std::vector<double> &pij) {
    
    size_t i=compartment.index();
    pij.resize(compartment.numNeighbor(),0.0);
    // Polarization coefficient normalization constant
    double sum=parameter(1);
    for( size_t n=0 ; n<compartment.numNeighbor() ; n++ )
      sum += y[ compartment.neighbor(n) ][ variableIndex(0,1) ];
    
    double pSum = 0.0;
    if (sum!=0.0) {
      for( size_t n=0 ; n<compartment.numNeighbor() ; n++ )
	pSum += pij[n] = y[i][variableIndex(0,0)] * 
	  y[compartment.neighbor(n)][ variableIndex(0,1) ] /sum;
    }
    return pSum;
  }
  
  Constlin::Constlin(std::vector<double> 
		     &paraValue, 
		     std::vector< 
		       std::vector<size_t> > 
		     &indValue ) 
  {
    // Do some checks on the parameters and variable indeces
    //
    if( paraValue.size()!=3 ) {
      std::cerr << "PolarizationTransportFast::Constlin::Constlin() "
		<< "uses three parameters (T, K and b/a).\n";
      exit(0);
    }
    if( indValue.size() != 2 || indValue[0].size() !=2 
	|| indValue[1].size() !=1  ) {
      std::cerr << "PolarizationTransportFast::Constlin::Constlin() "
		<< "Polarized molecule + polarization signal indeces needed in "
		<< "level 1.\n" 
		<< "Variable index for size parameter needed at second level.\n";
      exit(0);
    }
    // Set the variable values
    //
    setId("polarizationTransportFast::constlin");
    setParameter(paraValue);  
    setVariableIndex(indValue);
    
    // Set the parameter identities
    //
    std::vector<std::string> tmp( numParameter() );
    tmp.resize( numParameter() );
    tmp[0] = "T";
    tmp[1] = "K";
    tmp[1] = "b/a";
    setParameterId( tmp );
  }
  
  void Constlin::derivs(Compartment &compartment,size_t species,
			std::vector< std::vector<double> > &y,
			std::vector< std::vector<double> > 
			&dydt )
  {  
    size_t i=compartment.index();
    double Vi = y[i][variableIndex(1,0)];//Volume of the compartment
    if( Vi<=0.0 ) {
      std::cerr << "PolarizationTransportFast::Constlin::derivs() Given volume " << Vi
		<< " unphysical.\n";
      exit(-1);
    }
    // Polarization coefficient normalization constant
    double sum=parameter(1) * compartment.numNeighbor();
    for( size_t n=0 ; n<compartment.numNeighbor() ; n++ )
      sum += y[ compartment.neighbor(n) ][ variableIndex(0,1) ];
    sum += parameter(2);
    double invSum = 1.0/sum;//To use mult instead of div
    
    for( size_t n=0 ; n<compartment.numNeighbor() ; n++ ) {
      size_t j=compartment.neighbor(n);
      double Vj = y[j][variableIndex(1,0)];//Volume of the compartment
      if( Vj<=0.0 ) {
	std::cerr << "PolarizationTransportFast::Constlin::derivs Given volume " << Vj
		  << " unphysical.\n";
	exit(-1);
      }
      
      double Pij = ( parameter(1) + y[j][ variableIndex(0,1) ]) * 
	y[i][variableIndex(0,0)] * invSum;
      double coeff = y[i][species]*Pij;
      //dydt[i][species] -= parameter(0)*coeff/Vi;
      //dydt[j][species] += parameter(0)*coeff/Vj;
      dydt[i][species] -= parameter(0)*coeff;
      dydt[j][species] += parameter(0)*coeff;
    }
  }
  
  Hill::
  Hill(std::vector<double> &paraValue, 
       std::vector< std::vector<size_t> > &indValue ) 
  {  
    // Do some checks on the parameters and variable indeces
    //
    if( paraValue.size()!=4 ) {
      std::cerr << "PolarizationTransportFast::Hill::"
		<< "Hill() "
		<< "uses four parameters (T, b/a and K_H,n_H).\n";
      exit(0);
    }
    if( indValue.size() != 2 || indValue[0].size() !=2 
	|| indValue[1].size() !=1  ) {
      std::cerr << "PolarizationTransportFast::Hill::"
		<< "Hill() "
		<< "Polarized molecule + polarization signal indeces needed in "
		<< "level 1.\n" 
		<< "Variable index for size parameter needed at second level.\n";
      exit(0);
    }
    // Set the variable values
    //
    setId("polarizationTransportFast::hill");
    setParameter(paraValue);  
    setVariableIndex(indValue);
    
    // Set the parameter identities
    //
    std::vector<std::string> tmp( numParameter() );
    tmp.resize( numParameter() );
    tmp[0] = "T";
    tmp[1] = "b/a";
    tmp[2] = "K_H";
    tmp[3] = "n_H";
    setParameterId( tmp );
  }

  void Hill::
  derivs(Compartment &compartment,size_t species,
	 std::vector< std::vector<double> > &y,
	 std::vector< std::vector<double> > &dydt ) 
  {
    size_t i=compartment.index();
    if( compartment.numNeighbor() == 0 ) return;
    double Vi = y[i][variableIndex(1,0)];//Volume of the compartment
    if( Vi<=0.0 ) {
      std::cerr << "PolarizationTransportFast::Hill::derivs() Given volume " << Vi
		<< " unphysical.\n";
      exit(-1);
    }
    // Polarization coefficient normalization constant
    double sum=0.0;
    for( size_t n=0 ; n<compartment.numNeighbor() ; n++ ) {
      double Ak = y[ compartment.neighbor(n) ][ variableIndex(0,1) ]; 
      sum += std::pow(Ak,parameter(3))/
	(std::pow(parameter(2),parameter(3)) + std::pow(Ak,parameter(3)) );
    }
    sum /= compartment.numNeighbor();
    sum += parameter(1);
    double invSum = 1.0/sum;//To use mult instead of div
    
    for( size_t n=0 ; n<compartment.numNeighbor() ; n++ ) {
      size_t j=compartment.neighbor(n);
      double Vj = y[j][variableIndex(1,0)];//Volume of the compartment
      if( Vj<=0.0 ) {
	std::cerr << "PolarizationTransportFast::Hill::derivs Given volume " << Vj
		  << " unphysical.\n";
	exit(-1);
      }
      double Aj = y[j][ variableIndex(0,1) ];
      double Pij = y[i][variableIndex(0,0)] * invSum *
	std::pow(Aj,parameter(3))/
	(std::pow(parameter(2),parameter(3)) + std::pow(Aj,parameter(3)) );
      
      double coeff = Pij*y[i][species];
      //dydt[i][species] -= parameter(0)*coeff/Vi;
      //dydt[j][species] += parameter(0)*coeff/Vj;
      dydt[i][species] -= parameter(0)*coeff;
      dydt[j][species] += parameter(0)*coeff;
    }
  }

  General::General(
		   std::vector<double> &paraValue, 
		   std::vector< std::vector<size_t> > &indValue)
  {
    // Do some checks on the parameters and variable indeces
    //
    if (paraValue.size() != 5) {
      std::cerr << "PolarizationTransportFast::General::" 
		<< "General() "
		<< "uses five parameters (T, k_f, n_f, k_g and n_g).\n";
      exit(EXIT_FAILURE);
    }
    
    if (indValue.size() != 2 || indValue[0].size() != 2 
	|| indValue[1].size() != 1) {
      std::cerr << "PolarizationTransportFast::"
		<< "General::General() "
		<< "Polarized molecule + polarization signal indeces needed in "
		<< "level 1.\n" 
		<< "Variable index for size parameter needed at second level.\n";
      exit(EXIT_FAILURE);
    }
    // Set the variable values
    //
    setId("polarizationTransportFast::general");
    setParameter(paraValue);  
    setVariableIndex(indValue);
    
    // Set the parameter identities
    //
    std::vector<std::string> tmp( numParameter() );
    tmp.resize(numParameter());
    tmp[0] = "T";
    tmp[1] = "k_f";
    tmp[2] = "n_f";
    tmp[3] = "k_g";
    tmp[4] = "n_g";	
    setParameterId( tmp );
  }

  void General::derivs(
		       Compartment &compartment, 
		       size_t species,
		       std::vector< std::vector<double> > &y,
		       std::vector< std::vector<double> > &dydt)
  {
    double k_f = parameter(1);
    double n_f = parameter(2);
    double k_g = parameter(3);
    double n_g = parameter(4);
    double a_j;
    
    size_t i = compartment.index();
    
    // Polarization coefficient normalization constant
    double sum = 0.0;
    for (size_t n = 0; n < compartment.numNeighbor(); n++) {
      double a_j = y[compartment.neighbor(n)][variableIndex(0,1)];
      sum += f(a_j, k_f, n_f) / g(a_j, k_g, n_g);
    }
    sum += 1;
    
    for (size_t n = 0; n < compartment.numNeighbor(); n++) {
      size_t j = compartment.neighbor(n);
      a_j = y[j][variableIndex(0,1)];
      
      double coeff = 0.0;
      coeff = y[i][species] * y[i][variableIndex(0,0)];
      coeff *= f(a_j, k_f, n_f) / g(a_j, k_g, n_g);
      
      if (sum != 0.0)
	coeff /= sum;
      else {
	std::cerr << "PolarizationTransportFast::General::derivs() " 
		  << "sum == 0.0.\n";
	exit(EXIT_FAILURE);
      }
      dydt[i][species] -= parameter(0)*coeff;
      dydt[j][species] += parameter(0)*coeff;
    }
  }
  
  double General::f(double a, double k, double n)
  {
    return k * std::pow(a, n);
  }
  
  double General::g(double a, double k, double n)
  {
    return k;
  }
  
  ConstMM::
  ConstMM(std::vector<double> &paraValue, 
	  std::vector< std::vector<size_t> > &indValue ) 
  {
    // Do some checks on the parameters and variable indeces
    //
    if( paraValue.size()!=2 ) {
      std::cerr << "PolarizationTransport::ConstMM::ConstMM() "
		<< "uses two parameters (T and K_M).\n";
      exit(0);
    }
    if( indValue.size() != 2 || indValue[0].size() !=1 
	|| indValue[1].size() !=1  ) {
      std::cerr << "PolarizationTransport::ConstMM::ConstMM() "
		<< "Polarized molecule index needed in "
		<< "level 1.\n" 
		<< "Variable index for size parameter needed "
		<< "at second level.\n";
      exit(0);
    }
    // Set the variable values
    //
    setId("polarizationTransportFast::constMM");
    setParameter(paraValue);  
    setVariableIndex(indValue);
    
    // Set the parameter identities
    //
    std::vector<std::string> tmp( numParameter() );
    tmp.resize( numParameter() );
    tmp[0] = "T";
    tmp[1] = "K_M";
    setParameterId( tmp );
  }

  void ConstMM::derivs(Compartment &compartment,size_t species,
		       std::vector< std::vector<double> > &y,
		       std::vector< std::vector<double> > 
		       &dydt ) 
  {  
    size_t i=compartment.index();
    if( compartment.numNeighbor() == 0 ) return;
    double Vi = y[i][variableIndex(1,0)];//Volume of the compartment
    if( Vi<=0.0 ) {
      std::cerr << "PolarizationTransportFast::ConstMM::derivs() Given volume " << Vi
		<< " unphysical.\n";
      exit(-1);
    }
    for( size_t n=0 ; n<compartment.numNeighbor() ; n++ ) {
      size_t j=compartment.neighbor(n);
      if( i<j ) {
	double Vj = y[j][variableIndex(1,0)];//Volume of the compartment
	if( Vj<=0.0 ) {
	  std::cerr << "PolarizationTransportFast::ConstMM::derivs() Given volume " << Vj
		    << " unphysical.\n";
	  exit(-1);
	}
	double Pij = y[i][variableIndex(0,0)];
	double Pji = y[j][variableIndex(0,0)];
	// i -> j
	double coeff = Pij*y[i][species]/( parameter(2)+y[i][species] );
	//dydt[i][species] -= parameter(0)*coeff/Vi;
	//dydt[j][species] += parameter(0)*coeff/Vj;
	dydt[i][species] -= parameter(0)*coeff;
	dydt[j][species] += parameter(0)*coeff;
	// j -> i
	coeff = Pji*y[i][species]/( parameter(2)+y[i][species] );
	//dydt[i][species] += parameter(0)*coeff/Vi;
	//dydt[j][species] -= parameter(0)*coeff/Vj;
	dydt[i][species] += parameter(0)*coeff;
	dydt[j][species] -= parameter(0)*coeff;
      }
    }
  }
  
  LinMM::LinMM(std::vector<double> 
	       &paraValue, 
	       std::vector< 
		 std::vector<size_t> > 
	       &indValue ) {
    
    // Do some checks on the parameters and variable indeces
    //
    if( paraValue.size()!=3 ) {
      std::cerr << "PolarizationTransportFast::LinMM::LinMM() "
		<< "uses three parameters (T, b/a and K_M).\n";
      exit(0);
    }
    if( indValue.size() != 2 || indValue[0].size() !=2 
	|| indValue[1].size() !=1  ) {
      std::cerr << "PolarizationTransportFast::LinMM::LinMM() "
		<< "Polarized molecule + polarization signal indeces needed in "
		<< "level 1.\n" 
		<< "Variable index for size parameter needed at second level.\n";
      exit(0);
    }
    // Set the variable values
    //
    setId("polarizationTransportFast::linMM");
    setParameter(paraValue);  
    setVariableIndex(indValue);
    
    // Set the parameter identities
    //
    std::vector<std::string> tmp( numParameter() );
    tmp.resize( numParameter() );
    tmp[0] = "T";
    tmp[1] = "b/a";
    tmp[2] = "K_M";
    setParameterId( tmp );
  }

  void LinMM::derivs(Compartment &compartment,size_t species,
		     std::vector< std::vector<double> > &y,
		     std::vector< std::vector<double> > 
		     &dydt ) {
    
    size_t i=compartment.index();
    if( compartment.numNeighbor() == 0 ) return;
    double Vi = y[i][variableIndex(1,0)];//Volume of the compartment
    if( Vi<=0.0 ) {
      std::cerr << "PolarizationTransportFast::LinMM::derivs() Given volume " << Vi
		<< " unphysical.\n";
      exit(-1);
    }
    // Polarization coefficient normalization constant
    double sum=0.0;
    for( size_t n=0 ; n<compartment.numNeighbor() ; n++ )
      sum += y[ compartment.neighbor(n) ][ variableIndex(0,1) ];
    sum /= compartment.numNeighbor();//To lower dep on num neighs
    sum += parameter(1);
    double invSum = 1.0/sum;//To use mult instead of div
    
    for( size_t n=0 ; n<compartment.numNeighbor() ; n++ ) {
      size_t j=compartment.neighbor(n);
      double Vj = y[j][variableIndex(1,0)];//Volume of the compartment
      if( Vj<=0.0 ) {
	std::cerr << "PolarizationTransportFast::LinMM::derivs Given volume " << Vj
		  << " unphysical.\n";
	exit(-1);
      }
      
      double Pij = y[i][variableIndex(0,0)] * 
	y[j][ variableIndex(0,1) ] * invSum;
      double coeff = Pij*y[i][species]/( parameter(2)+y[i][species] );
      //dydt[i][species] -= parameter(0)*coeff/Vi;
      //dydt[j][species] += parameter(0)*coeff/Vj;
      dydt[i][species] -= parameter(0)*coeff;
      dydt[j][species] += parameter(0)*coeff;
    }
  }

  ConstlinMM::ConstlinMM(std::vector<double> 
			 &paraValue, 
			 std::vector< 
			   std::vector<size_t> > 
			 &indValue ) 
  {
    // Do some checks on the parameters and variable indeces
    //
    if( paraValue.size()!=4 ) {
      std::cerr << "PolarizationTransportFast::ConstlinMM::ConstlinMM()"
		<< " uses four parameters (T, K, b/a and K_M).\n";
      exit(0);
    }
    if( indValue.size() != 2 || indValue[0].size() !=2 
	|| indValue[1].size() !=1  ) {
      std::cerr << "PolarizationTransportFast::ConstlinMM::ConstlinMM() "
		<< "Polarized molecule + polarization signal indeces needed in "
		<< "level 1.\n" 
		<< "Variable index for size parameter needed at second level.\n";
      exit(0);
    }
    // Set the variable values
    //
    setId("polarizationTransportFast::copnstlinMM");
    setParameter(paraValue);  
    setVariableIndex(indValue);
    
    // Set the parameter identities
    //
    std::vector<std::string> tmp( numParameter() );
    tmp.resize( numParameter() );
    tmp[0] = "T";
    tmp[1] = "K";
    tmp[2] = "b/a";
    tmp[3] = "K_M";
    setParameterId( tmp );
  }
  
  void ConstlinMM::
  derivs(Compartment &compartment,size_t species,
	 std::vector< std::vector<double> > &y,
	 std::vector< std::vector<double> > 
	 &dydt )
  {
    size_t i=compartment.index();
    double Vi = y[i][variableIndex(1,0)];//Volume of the compartment
    if( Vi<=0.0 ) {
      std::cerr << "PolarizationTrnasportFast::ConstlinMM::derivs() Given volume " << Vi
		<< " unphysical.\n";
      exit(-1);
    }
    // Polarization coefficient normalization constant
    double sum=parameter(1) * compartment.numNeighbor();
    for( size_t n=0 ; n<compartment.numNeighbor() ; n++ )
      sum += y[ compartment.neighbor(n) ][ variableIndex(0,1) ];
    sum += parameter(2);
    double invSum = 1.0/sum;//To use mult instead of div
    
    for( size_t n=0 ; n<compartment.numNeighbor() ; n++ ) {
      size_t j=compartment.neighbor(n);
      double Vj = y[j][variableIndex(1,0)];//Volume of the compartment
      if( Vj<=0.0 ) {
	std::cerr << "PolarizationTransportFast::ConstlinMM::derivs() Given volume " << Vj
		  << " unphysical.\n";
	exit(-1);
      }
      
      double Pij = ( parameter(1) + y[j][ variableIndex(0,1) ]) * 
	y[i][variableIndex(0,0)] * invSum;
      double coeff = Pij*y[i][species]/( y[i][species]+parameter(3) );
      //dydt[i][species] -= parameter(0)*coeff/Vi;
      //dydt[j][species] += parameter(0)*coeff/Vj;
      dydt[i][species] -= parameter(0)*coeff;
      dydt[j][species] += parameter(0)*coeff;
    }
  }

  HillMM::
  HillMM(std::vector<double> &paraValue, 
	 std::vector< std::vector<size_t> > &indValue ) 
  {  
    // Do some checks on the parameters and variable indeces
    //
    if( paraValue.size()!=5 ) {
      std::cerr << "PolarizationTransportFast::HillMM::"
		<< "HillMM() "
		<< "uses five parameters (T, b/a, K_H,n_H and K_M).\n";
      exit(0);
    }
    if( indValue.size() != 2 || indValue[0].size() !=2 
	|| indValue[1].size() !=1  ) {
      std::cerr << "PolarizationTransportFast::HillMM::"
		<< "HillMM() "
		<< "Polarized molecule + polarization signal indeces needed in "
		<< "level 1.\n" 
		<< "Variable index for size parameter needed at second level.\n";
      exit(0);
    }
    // Set the variable values
    //
    setId("polarizationTransportFast::hillMM");
    setParameter(paraValue);  
    setVariableIndex(indValue);
    
    // Set the parameter identities
    //
    std::vector<std::string> tmp( numParameter() );
    tmp.resize( numParameter() );
    tmp[0] = "T";
    tmp[1] = "b/a";
    tmp[2] = "K_H";
    tmp[3] = "n_H";
    tmp[4] = "K_M";
    setParameterId( tmp );
  }

  void HillMM::
  derivs(Compartment &compartment,size_t species,
	 std::vector< std::vector<double> > &y,
	 std::vector< std::vector<double> > &dydt ) 
  {  
    size_t i=compartment.index();
    if( compartment.numNeighbor() == 0 ) return;
    double Vi = y[i][variableIndex(1,0)];//Volume of the compartment
    if( Vi<=0.0 ) {
      std::cerr << "PolarizationTransportFast::HillMM::derivs Given volume " << Vi
		<< " unphysical.\n";
      exit(-1);
    }
    // Polarization coefficient normalization constant
    double sum=0.0;
    for( size_t n=0 ; n<compartment.numNeighbor() ; n++ ) {
      double Ak = y[ compartment.neighbor(n) ][ variableIndex(0,1) ]; 
      sum += std::pow(Ak,parameter(3))/
	(std::pow(parameter(2),parameter(3)) + std::pow(Ak,parameter(3)) );
    }
    sum /= compartment.numNeighbor();
    sum += parameter(1);
    double invSum = 1.0/sum;//To use mult instead of div
    
    for( size_t n=0 ; n<compartment.numNeighbor() ; n++ ) {
      size_t j=compartment.neighbor(n);
      double Vj = y[j][variableIndex(1,0)];//Volume of the compartment
      if( Vj<=0.0 ) {
	std::cerr << "HillMM::derivs Given volume " 
		  << Vj << " unphysical.\n";
	exit(-1);
      }
      double Aj = y[j][ variableIndex(0,1) ];
      double Pij = y[i][variableIndex(0,0)] * invSum *
	std::pow(Aj,parameter(3))/
	(std::pow(parameter(2),parameter(3)) + std::pow(Aj,parameter(3)) );
      
      double coeff = Pij*y[i][species]/(parameter(4)+y[i][species]);
      //dydt[i][species] -= parameter(0)*coeff/Vi;
      //dydt[j][species] += parameter(0)*coeff/Vj;
      dydt[i][species] -= parameter(0)*coeff;
      dydt[j][species] += parameter(0)*coeff;
    }
  }
  
  ExpMM::
  ExpMM(std::vector<double> &paraValue, 
	std::vector< std::vector<size_t> > 
	&indValue ) 
  {  
    // Do some checks on the parameters and variable indeces
    //
    if( paraValue.size()!=3 ) {
      std::cerr << "PolarizationTransportFast::ExpMM::"
		<< "ExpMM() "
		<< "uses five parameters (T, b, and K_M).\n";
      exit(0);
    }
    if( indValue.size() != 2 || indValue[0].size() !=2 
	|| indValue[1].size() !=1  ) {
      std::cerr << "PolarizationTransportFast::ExpMM::"
		<< "ExpMM() "
		<< "Polarized molecule + polarization signal indeces needed in "
		<< "level 1.\n" 
		<< "Variable index for size parameter needed at second level.\n";
      exit(0);
    }
    // Set the variable values
    //
    setId("polarizationTransportFast::expMM");
    setParameter(paraValue);  
    setVariableIndex(indValue);
    
    // Set the parameter identities
    //
    std::vector<std::string> tmp( numParameter() );
    tmp.resize( numParameter() );
    tmp[0] = "T";
    tmp[1] = "b";
    tmp[2] = "K_M";
    setParameterId( tmp );
  }

  void ExpMM::
  derivs(Compartment &compartment,size_t species,
	 std::vector< std::vector<double> > &y,
	 std::vector< std::vector<double> > &dydt ) 
  {  
    size_t i=compartment.index();
    if( compartment.numNeighbor() == 0 ) return;
    double Vi = y[i][variableIndex(1,0)];//Volume of the compartment
    if( Vi<=0.0 ) {
      std::cerr << "PolarizationTransportFast::ExpMM::derivs() Given volume " << Vi
		<< " unphysical.\n";
      exit(-1);
    }
    // Polarization coefficient normalization constant
    double sum=0.0;
    for( size_t n=0 ; n<compartment.numNeighbor() ; n++ ) {
      double Ak = y[ compartment.neighbor(n) ][ variableIndex(0,1) ]; 
      sum += std::pow(parameter(1),Ak);
    }
    //sum /= compartment.numNeighbor();
    double invSum = 1.0/sum;//To use mult instead of div
    
    for( size_t n=0 ; n<compartment.numNeighbor() ; n++ ) {
      size_t j=compartment.neighbor(n);
      double Vj = y[j][variableIndex(1,0)];//Volume of the compartment
      if( Vj<=0.0 ) {
	std::cerr << "PolarizationTransportFast::ExpMM::derivs Given volume " 
		  << Vj << " unphysical.\n";
	exit(-1);
      }
      double Aj = y[j][ variableIndex(0,1) ];
      double Pij = y[i][variableIndex(0,0)] * invSum *
	std::pow(parameter(1),Aj);
      
      double coeff = Pij*y[i][species]/(parameter(2)+y[i][species]);
      //dydt[i][species] -= parameter(0)*coeff/Vi;
      //dydt[j][species] += parameter(0)*coeff/Vj;
      dydt[i][species] -= parameter(0)*coeff;
      dydt[j][species] += parameter(0)*coeff;
    }
  }

  ConstMMInflux::
  ConstMMInflux(std::vector<double> &paraValue, 
		std::vector< std::vector<size_t> > 
		&indValue ) 
  {    
    // Do some checks on the parameters and variable indeces
    //
    if( paraValue.size()!=2 ) {
      std::cerr << "PolarizedTransportFast::ConstMMInflux::"
		<< "ConstMMInflux() "
		<< "uses two parameters (T and K_M).\n";
      exit(0);
    }
    if( indValue.size() != 2 || indValue[0].size() !=2 
	|| indValue[1].size() !=1  ) {
      std::cerr << "PolarizedTransportFast::ConstMMInflux::"
		<< "ConstMMInflux() "
		<< "Efflux molecule "
		<< "and influx carrier indeces needed in level 1.\n" 
		<< "Variable index for size parameter needed at "
		<< "second level.\n";
      exit(0);
    }
    // Set the variable values
    //
    setId("polarizedTransportFast::constMMInflux");
    setParameter(paraValue);  
    setVariableIndex(indValue);
    
    // Set the parameter identities
    //
    std::vector<std::string> tmp( numParameter() );
    tmp.resize( numParameter() );
    tmp[0] = "T";
    tmp[1] = "K_M";
    setParameterId( tmp );
  }

  void ConstMMInflux::
  derivs(Compartment &compartment,size_t species,
	 std::vector< std::vector<double> > &y,
	 std::vector< std::vector<double> > &dydt ) 
  {  
    size_t i=compartment.index();
    if( compartment.numNeighbor() == 0 ) return;
    double Vi = y[i][variableIndex(1,0)];//Volume of the compartment
    if( Vi<=0.0 ) {
      std::cerr << "PolarizedTransportFast::ConstMMInflux::derivs() Given volume " << Vi
		<< " unphysical.\n";
      exit(-1);
    }
    
    for( size_t n=0 ; n<compartment.numNeighbor() ; n++ ) {
      size_t j=compartment.neighbor(n);
      if( i<j ) {
	double Vj = y[j][variableIndex(1,0)];//Volume of the compartment
	if( Vj<=0.0 ) {
	  std::cerr << "PolarizedTransportFast::ConstMMInflux::derivs() "
		    << "Given volume " << Vj << " unphysical.\n";
	  exit(-1);
	}
	
	double Pij = y[i][variableIndex(0,0)];
	double Pji = y[j][variableIndex(0,0)];
	double Lij = y[i][variableIndex(0,1)];
	double Lji = y[j][variableIndex(0,1)];
	
	double influxSum = Lij+Lji;
	if( influxSum<=0.0 ) return;
	// i -> j
	double coeff = Pij*Lji*y[i][species] /
	  ( ( parameter(1)+y[i][species] )*influxSum );
	//dydt[i][species] -= parameter(0)*coeff/Vi;
	//dydt[j][species] += parameter(0)*coeff/Vj;
	dydt[i][species] -= parameter(0)*coeff;
	dydt[j][species] += parameter(0)*coeff;
	// j -> i
	coeff = Pji*Lij*y[j][species] /
	  ( ( parameter(1)+y[j][species] )*influxSum );
	//dydt[i][species] += parameter(0)*coeff/Vi;
	//dydt[j][species] -= parameter(0)*coeff/Vj;
	dydt[i][species] += parameter(0)*coeff;
	dydt[j][species] -= parameter(0)*coeff;
      }
    }
  }
  
  LinMMInflux::
  LinMMInflux(std::vector<double> &paraValue, 
	      std::vector< std::vector<size_t> > 
	      &indValue ) 
  {  
    // Do some checks on the parameters and variable indeces
    //
    if( paraValue.size()!=3 ) {
      std::cerr << "PolarizationTransportFast::LinMMInflux::"
		<< "LinMMInflux() "
		<< "uses three parameters (T, b/a and K_M).\n";
      exit(0);
    }
    if( indValue.size() != 2 || indValue[0].size() !=3 
	|| indValue[1].size() !=1  ) {
      std::cerr << "PolarizationTransportFast::LinMMInflux::LinMMInflux() "
		<< "Polarized efflux molecule, polarization signal, "
		<< "and influx carrier indeces needed in level 1.\n" 
		<< "Variable index for size parameter needed at "
		<< "second level.\n";
      exit(0);
    }
    // Set the variable values
    //
    setId("polarizationTransportFast::linMMInflux");
    setParameter(paraValue);  
    setVariableIndex(indValue);
    // Set the parameter identities
    //
    std::vector<std::string> tmp( numParameter() );
    tmp.resize( numParameter() );
    tmp[0] = "T";
    tmp[1] = "b/a";
    tmp[2] = "K_M";
    setParameterId( tmp );
  }
  
  void LinMMInflux::
  derivs(Compartment &compartment,size_t species,
	 std::vector< std::vector<double> > &y,
	 std::vector< std::vector<double> > &dydt ) 
  {  
    size_t i=compartment.index();
    if( compartment.numNeighbor() == 0 ) return;
    double Vi = y[i][variableIndex(1,0)];//Volume of the compartment
    if( Vi<=0.0 ) {
      std::cerr << "PolarizationTransportFast::LinMMInflux::derivs() Given volume " 
		<< Vi << " unphysical.\n";
      exit(-1);
    }
    // Polarization coefficient normalization constant
    double sum=0.0;
    for( size_t n=0 ; n<compartment.numNeighbor() ; n++ )
      sum += y[ compartment.neighbor(n) ][ variableIndex(0,1) ];
    sum /= compartment.numNeighbor();//To lower dep on num neighs
    sum += parameter(1);
    double invSum = 1.0/sum;//To use mult instead of div
    
    for( size_t n=0 ; n<compartment.numNeighbor() ; n++ ) {
      size_t j=compartment.neighbor(n);
      double Vj = y[j][variableIndex(1,0)];//Volume of the compartment
      if( Vj<=0.0 ) {
	std::cerr << "PolarizationTransportFast::LinMMInflux::derivs() "
		  << "Given volume " << Vj << " unphysical.\n";
	exit(-1);
      }    
      double Pij = y[i][variableIndex(0,0)] * 
	y[j][ variableIndex(0,1) ] * invSum;
      double influxSum = y[i][variableIndex(0,2)]+y[j][variableIndex(0,2)];
      if( influxSum<=0.0 ) return;
      double coeff = Pij*y[i][species]*y[j][variableIndex(0,2)] /
	( ( parameter(2)+y[i][species] )*influxSum );
      //dydt[i][species] -= parameter(0)*coeff/Vi;
      //dydt[j][species] += parameter(0)*coeff/Vj;
      dydt[i][species] -= parameter(0)*coeff;
      dydt[j][species] += parameter(0)*coeff;
    }
  }
  
  HillMMInflux::
  HillMMInflux(std::vector<double> &paraValue, 
	       std::vector< std::vector<size_t> > 
	       &indValue ) 
  {  
    // Do some checks on the parameters and variable indeces
    //
    if( paraValue.size()!=5 ) {
      std::cerr << "PolarizationTransportFast::HillMMInflux::"
		<< "HillMMInflux() "
		<< "uses five parameters (T, b/a, K_M, K, n).\n";
      exit(0);
    }
    if( indValue.size() != 2 || indValue[0].size() !=3 
	|| indValue[1].size() !=1  ) {
      std::cerr << "PolarizationTransportFast::HillMMInflux::HillMMInflux() "
		<< "Polarized efflux molecule, polarization signal, "
		<< "and influx carrier indeces needed in level 1.\n" 
		<< "Variable index for size parameter needed at "
		<< "second level.\n";
      exit(0);
    }
    // Set the variable values
    //
    setId("polarizationTransportFast::hillMMInflux");
    setParameter(paraValue);  
    setVariableIndex(indValue);
    
    //Set the parameter identities
    //
    std::vector<std::string> tmp( numParameter() );
    tmp.resize( numParameter() );
    tmp[0] = "T";
    tmp[1] = "b/a";
    tmp[2] = "K_M";
    tmp[2] = "K_H";
    tmp[2] = "n_H";
    setParameterId( tmp );
  }
  
  void HillMMInflux::
  derivs(Compartment &compartment,size_t species,
	 std::vector< std::vector<double> > &y,
	 std::vector< std::vector<double> > &dydt ) 
  {  
    size_t i=compartment.index();
    if( compartment.numNeighbor() == 0 ) return;
    double Vi = y[i][variableIndex(1,0)];//Volume of the compartment
    if( Vi<=0.0 ) {
      std::cerr << "PolarizationTransportFast::HillMMInflux::derivs() "
		<< "Given volume " << Vi
		<< " unphysical.\n";
      exit(-1);
    }
    // Polarization coefficient normalization constant
    double sum=0.0;
    double KtoN = std::pow( parameter(3),parameter(4) );
    for( size_t n=0 ; n<compartment.numNeighbor() ; n++ ) {
      double ytoN = std::pow( y[ compartment.neighbor(n) ]
			      [ variableIndex(0,1) ],parameter(4) );
      sum += ytoN/(KtoN+ytoN);
    }
    sum /= compartment.numNeighbor();//To lower dep on num neighs
    sum += parameter(1);
    double invSum = 1.0/sum;//To use mult instead of div
    
    for( size_t n=0 ; n<compartment.numNeighbor() ; n++ ) {
      size_t j=compartment.neighbor(n);
      double Vj = y[j][variableIndex(1,0)];//Volume of the compartment
      if( Vj<=0.0 ) {
	std::cerr << "PolarizationTransportFast::HillMMInflux::derivs "
		  << "Given volume " << Vj << " unphysical.\n";
	exit(-1);
      }
      
      double ytoN = std::pow(y[j][ variableIndex(0,1) ],parameter(4));
      double Pij = y[i][variableIndex(0,0)] * 
	( ytoN/(KtoN+ytoN) ) * invSum;
      double influxSum = y[i][variableIndex(0,2)]+y[j][variableIndex(0,2)];
      if( influxSum<=0.0 ) return;
      double coeff = Pij*y[i][species]*y[j][variableIndex(0,2)] /
	( ( parameter(2)+y[i][species] )*influxSum );
      //dydt[i][species] -= parameter(0)*coeff/Vi;
      //dydt[j][species] += parameter(0)*coeff/Vj;
      dydt[i][species] -= parameter(0)*coeff;
      dydt[j][species] += parameter(0)*coeff;
    }
  }
  
  LinMMSpatial::LinMMSpatial(std::vector<double> 
			     &paraValue, 
			     std::vector< 
			       std::vector<size_t> > 
			     &indValue ) 
{    
    // Do some checks on the parameters and variable indeces
    //
    if( paraValue.size()!=3 ) {
      std::cerr << "PolarizationTransportFast::LinMMSpatial::LinMMSpatial() "
		<< "uses three parameters (T, b/a and K_M).\n";
      exit(0);
    }
    if( indValue.size() != 2 || indValue[0].size() !=2 
	|| indValue[1].size() !=1  ) {
      std::cerr << "PolarizationTransportFast::LinMMSpatial::LinMMSpatial() "
		<< "Polarized molecule + polarization signal indeces needed in "
		<< "level 1.\n" 
		<< "Variable index for size parameter needed at second level.\n";
      exit(0);
    }
    // Set the variable values
    //
    setId("polarizationTransportFast::LinMMSpatial");
    setParameter(paraValue);  
    setVariableIndex(indValue);
    
    // Set the parameter identities
    //
    std::vector<std::string> tmp( numParameter() );
    tmp.resize( numParameter() );
    tmp[0] = "T";
    tmp[1] = "b/a";
    tmp[2] = "K_M";
    setParameterId( tmp );
  }
  
  void LinMMSpatial::derivs(Compartment &compartment,size_t species,
			    std::vector< std::vector<double> > &y,
			    std::vector< std::vector<double> > 
			    &dydt ) 
  {    
    size_t i=compartment.index();
    if( compartment.numNeighbor() == 0 ) return;
    double Vi = y[i][variableIndex(1,0)];//Volume of the compartment
    if( Vi<=0.0 ) {
      std::cerr << "PolarizationTransportFast::LinMMSpatial::derivs() Given volume " 
		<< Vi << " unphysical.\n";
      exit(-1);
    }
    // Areas
    double areaSum=0.0;
    std::vector<double> area(compartment.numNeighbor());
    for( size_t k=0 ; k<compartment.numNeighbor() ; k++ ) {
      size_t n=compartment.neighbor(k);
      double d2=0.0;
      for( size_t dim=0 ; dim<compartment.numDimension() ; dim++ )
	d2 += (y[i][dim]-y[n][dim])*(y[i][dim]-y[n][dim]);
      double d = std::sqrt(d2);
      double ri=y[i][compartment.numDimension()];
      double rn=y[n][compartment.numDimension()];
      if( d>ri+rn ) area[k]=0.0;
      else if( d<ri || d<rn ) {
	area[k] = ri<rn ? ri : rn;
      }
      else
	area[k] = std::sqrt( 0.25*(d2-2.*(ri*ri-rn*rn)*(ri*ri-rn*rn) +
				   (ri-rn)*(ri-rn)/d2 ) );
      areaSum += area[k];
      if( areaSum<=0.0 ) areaSum=1.0;
    }
    // Polarization coefficient normalization constant
    double sum=0.0;
    for( size_t n=0 ; n<compartment.numNeighbor() ; n++ )
      sum += y[ compartment.neighbor(n) ][ variableIndex(0,1) ]*area[n]/areaSum;
    //sum /= compartment.numNeighbor();//To lower dep on num neighs
    sum += parameter(1);
    double invSum = 1.0/sum;//To use mult instead of div
    
    for( size_t n=0 ; n<compartment.numNeighbor() ; n++ ) {
      size_t j=compartment.neighbor(n);
      double Vj = y[j][variableIndex(1,0)];//Volume of the compartment
      if( Vj<=0.0 ) {
	std::cerr << "LinMMSpatial::derivs Given volume " << Vj
		  << " unphysical.\n";
	exit(-1);
      }
      
      double Pij = y[i][variableIndex(0,0)] * 
	y[j][ variableIndex(0,1) ] * invSum;
      double coeff = area[n]*Pij*y[i][species]/( parameter(2)+y[i][species] );
      //dydt[i][species] -= parameter(0)*coeff/Vi;
      //dydt[j][species] += parameter(0)*coeff/Vj;
      dydt[i][species] -= parameter(0)*coeff;
      dydt[j][species] += parameter(0)*coeff;
    }
  }

  LinMMSwitched::
  LinMMSwitched(std::vector<double> 
		&paraValue, 
		std::vector< 
		  std::vector<size_t> > 
		&indValue ) 
  {
    // Do some checks on the parameters and variable indeces
    //
    if( paraValue.size()!=4 ) {
      std::cerr << "PolarizationTransportFast::LinMMSwitched::"
		<< "LinMMSwitched() "
		<< "uses three parameters (T, b/a,K_M and Th_switch).\n";
      exit(0);
    }
    if( indValue.size() != 2 || indValue[0].size() !=3 
	|| indValue[1].size() !=1  ) {
      std::cerr << "PolarizationTransportFast::LinMMSwitched::LinMMSwitched() "
		<< "Polarized molecule + polarization signal + switch molecule "
		<< "indeces needed in"
		<< "level 1.\n" 
		<< "Variable index for size parameter needed at second level.\n";
      exit(0);
    }
    // Set the variable values
    //
    setId("polarizationTransportFast::linMMSwitched");
    setParameter(paraValue);  
    setVariableIndex(indValue);
    
    // Set the parameter identities
    //
    std::vector<std::string> tmp( numParameter() );
    tmp.resize( numParameter() );
    tmp[0] = "T";
    tmp[1] = "b/a";
    tmp[2] = "K_M";
    tmp[3] = "Th_switch";
    setParameterId( tmp );
  }

  void LinMMSwitched::
  derivs(Compartment &compartment,size_t species,
	 std::vector< std::vector<double> > &y,
	 std::vector< std::vector<double> > 
	 &dydt )
  {  
    size_t i=compartment.index();
    if( compartment.numNeighbor() == 0 ) return;
    double Vi = y[i][variableIndex(1,0)];//Volume of the compartment
    if( Vi<=0.0 ) {
      std::cerr << "PolarizationTransportFast::LinMMSwitched::derivs() Given volume " 
		<< Vi << " unphysical.\n";
      exit(-1);
    }
    // Polarization coefficient normalization constant
    double sum=0.0;
    double min = y[ compartment.neighbor(0) ][ variableIndex(0,1) ];
    double max=min;
    for( size_t n=0 ; n<compartment.numNeighbor() ; n++ ) {
      sum += y[ compartment.neighbor(n) ][ variableIndex(0,1) ];
      if( y[ compartment.neighbor(n) ][ variableIndex(0,1) ]<min )
	min = y[ compartment.neighbor(n) ][ variableIndex(0,1) ];
      if( y[ compartment.neighbor(n) ][ variableIndex(0,1) ]>max )
	max = y[ compartment.neighbor(n) ][ variableIndex(0,1) ];
    }
    sum /= compartment.numNeighbor();
    sum += parameter(1);
    double invSum = 1.0;
    if( sum>0 )
      invSum /= sum;//To use mult instead of div
    
    for( size_t n=0 ; n<compartment.numNeighbor() ; n++ ) {
      size_t j=compartment.neighbor(n);
      double Vj = y[j][variableIndex(1,0)];//Volume of the compartment
      if( Vj<=0.0 ) {
	std::cerr << "PolarizationtransportFast::LinMMSwitched::derivs() Given volume " 
		  << Vj << " unphysical.\n";
	exit(-1);
      }
      
      double Pij; 
      if( y[i][variableIndex(0,2)] > parameter(3) )
	Pij = y[i][variableIndex(0,0)] * y[j][ variableIndex(0,1) ] * invSum;
      else
	Pij = y[i][variableIndex(0,0)] * (max+min-y[j][ variableIndex(0,1) ]) 
	  * invSum;
      double coeff = Pij*y[i][species]/( parameter(2)+y[i][species] );
      //dydt[i][species] -= parameter(0)*coeff/Vi;
      //dydt[j][species] += parameter(0)*coeff/Vj;
      dydt[i][species] -= parameter(0)*coeff;
      dydt[j][species] += parameter(0)*coeff;
    }
  }
  
} //end namespace PolarizationTransportFast

//!Contructor for a polarized cell-wall transport
PolarizationLinFastTransportRestrictedMM::
PolarizationLinFastTransportRestrictedMM(std::vector<double> &paraValue, 
					 std::vector< std::vector<size_t> > 
					 &indValue ) {
  
  //Do some checks on the parameters and variable indeces
  //////////////////////////////////////////////////////////////////////
  if( paraValue.size()!=4 ) {
    std::cerr << "PolarizationLinFastTransportRestrictedMM::"
	      << "PolarizationLinFastTransportRestrictedMM() "
	      << "uses four parameters (T_eff, T_inf, b/a, and K_M).\n";
    exit(0);
  }
  if( indValue.size() != 2 || indValue[0].size() !=4 
      || indValue[1].size() !=1  ) {
    std::cerr << "PolarizationLinFastTransportRestrictedMM::"
	      << "PolarizationLinFastTransportRestrictedMM() "
	      << "Polarized molecule + polarization signal + "
	      << "cell marker + wall marker "
	      << "indeces needed in"
	      << "level 1.\n" 
	      << "Variable index for size parameter needed at second level.\n";
    exit(0);
  }
  //Set the variable values
  //////////////////////////////////////////////////////////////////////
  setId("polarizationLinFastTransportRestrictedMM");
  setParameter(paraValue);  
  setVariableIndex(indValue);
  
  //Set the parameter identities
  //////////////////////////////////////////////////////////////////////
  std::vector<std::string> tmp( numParameter() );
  tmp.resize( numParameter() );
  tmp[0] = "T_eff";
  tmp[1] = "T_inf";
  tmp[2] = "b/a";
  tmp[3] = "K_M";
  setParameterId( tmp );
}

//! Derivative contribution for an active transport in polarized direction
/*! Deriving the time derivative contribution from a degradation using
  the values in y and add the results to dydt. Compartment is the cell
  and species is the index of the variable to be updated.*/
void PolarizationLinFastTransportRestrictedMM::
derivs(Compartment &compartment,size_t species,
       std::vector< std::vector<double> > &y,
       std::vector< std::vector<double> > 
       &dydt ) {
  
  size_t i=compartment.index();
  //Only if the comp has neighbors and is a cell
  if( compartment.numNeighbor() == 0 || 
      y[i][variableIndex(0,2)]<0.5 ) return;
  
  //Extract compartment volume and make sure it is physical
  double Vi = y[i][variableIndex(1,0)];
  assert( Vi>0.0 );
  //Polarization from CELL neighbors at level 1.
  assert( compartment.numNeighborAtLevel(1) == 
	  compartment.numNeighbor() );
  
  //Calulate normalization
  //////////////////////////////////////////////////////////////////////
  double sum=parameter(2);
  if( compartment.numNeighbor()==compartment.numNeighborArea() ) {
    sum *=Vi;
    for( size_t n=0 ; n<compartment.numNeighborAtLevel(1) ; n++ )
      sum += y[ compartment.neighborAtLevel(1,n) ][ variableIndex(0,1) ]*
	compartment.neighborArea(n);
  }
  else
    for( size_t n=0 ; n<compartment.numNeighbor() ; n++ )
      sum += y[ compartment.neighborAtLevel(1,n) ][ variableIndex(0,1) ];
  
  double invSum = 1.0;
  if( sum>0.0 )
    invSum /= sum;//To use mult instead of div
  
  //Update all compartments
  //////////////////////////////////////////////////////////////////////
  for( size_t n=0 ; n<compartment.numNeighbor() ; n++ ) {
    size_t j=compartment.neighbor(n);

    //Make sure neighbor marked as wall
    assert( y[j][variableIndex(0,3)]>0.5 );

    //Extract neighbor volume and make sure it is physical
    double Vj = y[j][variableIndex(1,0)];
    assert( Vj>0.0 );

    double Pij = y[i][variableIndex(0,0)] * Vi * 
      y[compartment.neighborAtLevel(1,n)][variableIndex(0,1)] * invSum;
    if( compartment.numNeighbor()==compartment.numNeighborArea() ) {
      double coeff = compartment.neighborArea(n)*Pij*
	y[i][species]/( parameter(3)+y[i][species] );
      dydt[i][species] -= parameter(0)*coeff/Vi;
      dydt[j][species] += parameter(0)*coeff/Vj;
      coeff = compartment.neighborArea(n)*Pij*
	y[j][species]/( parameter(3)+y[j][species] );
      dydt[i][species] += parameter(1)*coeff/Vi;
      dydt[j][species] -= parameter(1)*coeff/Vj;
    }
    else {
      double coeff = Pij*y[i][species]/( parameter(3)+y[i][species] );
      dydt[i][species] -= parameter(0)*coeff/Vi;
      dydt[j][species] += parameter(0)*coeff/Vj;
      coeff = Pij*y[j][species]/( parameter(3)+y[j][species] );
      dydt[i][species] += parameter(1)*coeff/Vi;
      dydt[j][species] -= parameter(1)*coeff/Vj;
    }
  }
}

PolarizationConstHillFastTransportRestrictedMM::
PolarizationConstHillFastTransportRestrictedMM(std::vector<double> 
					       &paraValue, 
					       std::vector< 
					       std::vector<size_t> > 
					       &indValue ) {
  
  //Do some checks on the parameters and variable indeces
  //////////////////////////////////////////////////////////////////////
  if( paraValue.size()!=7 ) {
    std::cerr << "PolarizationConstHillFastTransportRestrictedMM::"
	      << "PolarizationConstHillFastTransportRestrictedMM() "
	      << "uses seven parameters (T_eff, T_inf, b/a, and c, "
	      << "K_H, n,and K_M).\n";
    exit(0);
  }
  if( indValue.size() != 2 || indValue[0].size() !=4 
      || indValue[1].size() !=1  ) {
    std::cerr << "PolarizationConstHillFastTransportRestrictedMM::"
	      << "PolarizationConstHillFastTransportRestrictedMM() "
	      << "Polarized molecule + polarization signal + "
	      << "cell marker + wall marker "
	      << "indeces needed in"
	      << "level 1.\n" 
	      << "Variable index for size parameter needed at second level.\n";
    exit(0);
  }
  //Check parameters
  //////////////////////////////////////////////////////////////////////
  if( paraValue[0]<0.0 || paraValue[1]<0.0 || paraValue[2]<0.0 ||
      paraValue[3]<0.0 || paraValue[4]<0.0 || paraValue[5]<0.0 ||
      paraValue[6]<0.0 ) {
    std::cerr << "PolarizationConstHillFastTransportRestrictedMM::"
	      << "PolarizationConstHillFastTransportRestrictedMM()"
	      << " Parameters need to be positive.\n";
    exit(0);
  }
  if( paraValue[3]>1.0 ) {
    std::cerr << "PolarizationConstHillFastTransportRestrictedMM::"
	      << "PolarizationConstHillFastTransportRestrictedMM()"
	      << " Parameter c (Lin/Hill fraction need to be "
	      << "smaller than 1.\n";
    exit(0);
  }
  //Set the variable values
  //////////////////////////////////////////////////////////////////////
  setId("polarizationConstHillFastTransportRestrictedMM");
  setParameter(paraValue);  
  setVariableIndex(indValue);
  
  //Set the parameter identities
  //////////////////////////////////////////////////////////////////////
  std::vector<std::string> tmp( numParameter() );
  tmp.resize( numParameter() );
  tmp[0] = "T_eff";
  tmp[1] = "T_inf";
  tmp[2] = "b/a";
  tmp[3] = "c_p";
  tmp[4] = "K_H";
  tmp[5] = "n";
  tmp[6] = "K_M";
  setParameterId( tmp );
}

//! Derivative contribution for an active transport in polarized direction
/*! Deriving the time derivative contribution from a degradation using
  the values in y and add the results to dydt. Compartment is the cell
  and species is the index of the variable to be updated.*/
void PolarizationConstHillFastTransportRestrictedMM::
derivs(Compartment &compartment,size_t species,
       std::vector< std::vector<double> > &y,
       std::vector< std::vector<double> > 
       &dydt ) {
  
  size_t i=compartment.index();
  //Only if the comp has neighbors and is a cell
  if( compartment.numNeighbor() == 0 || 
      y[i][variableIndex(0,2)]<0.5 ) return;
  
  static double KToNHill = std::pow(parameter(4),parameter(5));
  //Polarization function from CELL neighbors at level 1.
  assert( compartment.numNeighborAtLevel(1) == 
	  compartment.numNeighbor() );
  //Extract volume of the compartment and make sure it is physical
  double Vi = y[i][variableIndex(1,0)];
  assert( Vi>0.0 );
  
  //Calculate f(n) and normalization
  //////////////////////////////////////////////////////////////////////
  std::vector<double> f(compartment.numNeighbor() );
  for( size_t n=0 ; n<compartment.numNeighborAtLevel(1) ; n++ ) {
    double yToN = std::pow(y[ compartment.neighborAtLevel(1,n) ]
			   [ variableIndex(0,1) ] , parameter(5));
    f[n] = (1.0-parameter(3)) + parameter(3)*yToN/(KToNHill+yToN);
  }

  double sum=parameter(2);
  if( compartment.numNeighbor()==compartment.numNeighborArea() ) {
    sum *=Vi;
    for( size_t n=0 ; n<compartment.numNeighborAtLevel(1) ; n++ )
      sum += f[n]*compartment.neighborArea(n);
  }
  else
    for( size_t n=0 ; n<compartment.numNeighbor() ; n++ )
      sum += f[n];
  
  double invSum = 1.0;
  if( sum>0.0 )
    invSum /= sum;//To use mult instead of div
  
  //Update all compartments
  //////////////////////////////////////////////////////////////////////
  for( size_t n=0 ; n<compartment.numNeighbor() ; n++ ) {
    size_t j=compartment.neighbor(n);
    
    //Make sure neighbor marked as wall 
    assert( y[j][variableIndex(0,3)]>0.5 ); 
    
    //Extract volume of neighbor and make sure it is physical
    double Vj = y[j][variableIndex(1,0)];
    assert( Vj>0.0 );
    
    double Pij = y[i][variableIndex(0,0)] * Vi * f[n] * invSum;
    if( compartment.numNeighbor()==compartment.numNeighborArea() ) {
      double coeff = compartment.neighborArea(n)*Pij*
	y[i][species]/( parameter(6)+y[i][species] );
      dydt[i][species] -= parameter(0)*coeff/Vi;
      dydt[j][species] += parameter(0)*coeff/Vj;
      coeff = compartment.neighborArea(n)*Pij*
	y[j][species]/( parameter(6)+y[j][species] );
      dydt[i][species] += parameter(1)*coeff/Vi;
      dydt[j][species] -= parameter(1)*coeff/Vj;
    }
    else {
      double coeff = Pij*y[i][species]/( parameter(6)+y[i][species] );
      dydt[i][species] -= parameter(0)*coeff/Vi;
      dydt[j][species] += parameter(0)*coeff/Vj;
      coeff = Pij*y[j][species]/( parameter(6)+y[j][species] );
      dydt[i][species] += parameter(1)*coeff/Vi;
      dydt[j][species] -= parameter(1)*coeff/Vj;
    }
  }
}

PolarizationConstFastTransportRestrictedMM::
PolarizationConstFastTransportRestrictedMM(std::vector<double> 
					   &paraValue, 
					   std::vector< 
					   std::vector<size_t> > 
					   &indValue ) {
  
  //Do some checks on the parameters and variable indeces
  //////////////////////////////////////////////////////////////////////
  if( paraValue.size()!=4 ) {
    std::cerr << "PolarizationConstFastTransportRestrictedMM::"
	      << "PolarizationConstFastTransportRestrictedMM() "
	      << "uses seven parameters (T_eff, T_inf, b/a, and K_M).\n";
    exit(0);
  }
  if( indValue.size() != 2 || indValue[0].size() !=3 
      || indValue[1].size() !=1  ) {
    std::cerr << "PolarizationConstFastTransportRestrictedMM::"
	      << "PolarizationConstFastTransportRestrictedMM() "
	      << "Polarized molecule + "
	      << "cell marker + wall marker "
	      << "indeces needed in"
	      << "level 1.\n" 
	      << "Variable index for size parameter needed at "
	      << "second level.\n";
    exit(0);
  }
  //Check parameters
  //////////////////////////////////////////////////////////////////////
  if( paraValue[0]<0.0 || paraValue[1]<0.0 || paraValue[2]<0.0 ||
      paraValue[3]<0.0 ) {
    std::cerr << "PolarizationConstFastTransportRestrictedMM::"
	      << "PolarizationConstFastTransportRestrictedMM()"
	      << " Parameters need to be positive.\n";
    exit(0);
  }
  //Set the variable values
  //////////////////////////////////////////////////////////////////////
  setId("polarizationConstFastTransportRestrictedMM");
  setParameter(paraValue);  
  setVariableIndex(indValue);
  
  //Set the parameter identities
  //////////////////////////////////////////////////////////////////////
  std::vector<std::string> tmp( numParameter() );
  tmp.resize( numParameter() );
  tmp[0] = "T_eff";
  tmp[1] = "T_inf";
  tmp[2] = "b/a";
  tmp[3] = "K_M";
  setParameterId( tmp );
}

void PolarizationConstFastTransportRestrictedMM::
derivs(Compartment &compartment,size_t species,
       std::vector< std::vector<double> > &y,
       std::vector< std::vector<double> > 
       &dydt ) {
  
  size_t i=compartment.index();
  //Only if the comp has neighbors and is a cell
  if( compartment.numNeighbor() == 0 || 
      y[i][variableIndex(0,1)]<0.5 ) return;
  
  //Polarization function from CELL neighbors at level 1.
  assert( compartment.numNeighborAtLevel(1) == 
	  compartment.numNeighbor() );
  //Extract volume of the compartment and make sure it is physical
  double Vi = y[i][variableIndex(1,0)];
  assert( Vi>0.0 );
  
  //Calculate normalization
  //////////////////////////////////////////////////////////////////////
  double sum=parameter(2);
  if( compartment.numNeighbor()==compartment.numNeighborArea() ) {
    sum *=Vi;
    for( size_t n=0 ; n<compartment.numNeighborAtLevel(1) ; n++ )
      sum += compartment.neighborArea(n);
  }
  else
    sum += compartment.numNeighbor();//assuming area=1
  
  double invSum = 1.0;
  if( sum>0.0 )
    invSum /= sum;//To use mult instead of div
  
  //Update all compartments
  //////////////////////////////////////////////////////////////////////
  for( size_t n=0 ; n<compartment.numNeighbor() ; n++ ) {
    size_t j=compartment.neighbor(n);
    
    //Make sure neighbor marked as wall 
    assert( y[j][variableIndex(0,2)]>0.5 ); 
    
    //Extract volume of neighbor and make sure it is physical
    double Vj = y[j][variableIndex(1,0)];
    assert( Vj>0.0 );
    
    double Pij = y[i][variableIndex(0,0)] * Vi * invSum;
    if( compartment.numNeighbor()==compartment.numNeighborArea() ) {
      double coeff = compartment.neighborArea(n)*Pij*
	y[i][species]/( parameter(3)+y[i][species] );
      dydt[i][species] -= parameter(0)*coeff/Vi;
      dydt[j][species] += parameter(0)*coeff/Vj;
      coeff = compartment.neighborArea(n)*Pij*
	y[j][species]/( parameter(3)+y[j][species] );
      dydt[i][species] += parameter(1)*coeff/Vi;
      dydt[j][species] -= parameter(1)*coeff/Vj;
    }
    else {
      double coeff = Pij*y[i][species]/( parameter(3)+y[i][species] );
      dydt[i][species] -= parameter(0)*coeff/Vi;
      dydt[j][species] += parameter(0)*coeff/Vj;
      coeff = Pij*y[j][species]/( parameter(3)+y[j][species] );
      dydt[i][species] += parameter(1)*coeff/Vi;
      dydt[j][species] -= parameter(1)*coeff/Vj;
    }
  }
}

PolarizationConstFastTransportFractionRestrictedMM::
PolarizationConstFastTransportFractionRestrictedMM(std::vector<double> 
					   &paraValue, 
					   std::vector< 
					   std::vector<size_t> > 
					   &indValue ) {
  
  //Do some checks on the parameters and variable indeces
  //////////////////////////////////////////////////////////////////////
  if( paraValue.size()!=6 ) {
    std::cerr << "PolarizationConstFastTransportFractionRestrictedMM::"
	      << "PolarizationConstFastTransportFractionRestrictedMM() "
	      << "uses six parameters (T_eff, T_inf, b/a, K_M, f_i, f_ij).\n";
    exit(0);
  }
  if( indValue.size() != 2 || indValue[0].size() !=3 
      || indValue[1].size() !=1  ) {
    std::cerr << "PolarizationConstFastTransportFractionRestrictedMM::"
	      << "PolarizationConstFastTransportFractionRestrictedMM() "
	      << "Polarized molecule + "
	      << "cell marker + wall marker "
	      << "indeces needed in"
	      << "level 1.\n" 
	      << "Variable index for size parameter needed at "
	      << "second level.\n";
    exit(0);
  }
  //Check parameters
  //////////////////////////////////////////////////////////////////////
  if( paraValue[0]<0.0 || paraValue[1]<0.0 || paraValue[2]<0.0 ||
      paraValue[3]<0.0 || paraValue[4]<0.0 || paraValue[4]>1.0 ||
      paraValue[5]<0.0 || paraValue[5]>1.0 ) {
    std::cerr << "PolarizationConstFastTransportFractionRestrictedMM::"
	      << "PolarizationConstFastTransportFractionRestrictedMM()"
	      << " Parameters need to be positive (and fractions).\n";
    exit(0);
  }
  //Set the variable values
  //////////////////////////////////////////////////////////////////////
  setId("polarizationConstFastTransportFractionRestrictedMM");
  setParameter(paraValue);  
  setVariableIndex(indValue);
  
  //Set the parameter identities
  //////////////////////////////////////////////////////////////////////
  std::vector<std::string> tmp( numParameter() );
  tmp.resize( numParameter() );
  tmp[0] = "T_eff";
  tmp[1] = "T_inf";
  tmp[2] = "b/a";
  tmp[3] = "K_M";
  tmp[4] = "f_i";
  tmp[5] = "f_ij";
  setParameterId( tmp );
}

void PolarizationConstFastTransportFractionRestrictedMM::
derivs(Compartment &compartment,size_t species,
       std::vector< std::vector<double> > &y,
       std::vector< std::vector<double> > 
       &dydt ) {
  
  size_t i=compartment.index();
  //Only if the comp has neighbors and is a cell
  if( compartment.numNeighbor() == 0 || 
      y[i][variableIndex(0,1)]<0.5 ) return;
  
  //Extract volume of the compartment and make sure it is physical
  double Vi = y[i][variableIndex(1,0)];
  assert( Vi>0.0 );
  
  //Calculate normalization
  //////////////////////////////////////////////////////////////////////
  double invSum=1.0;
	//if ( compartment.numNeighborAtLevel(1)) {
	//assert(compartment.numNeighborAtLevel(1)==compartment.numNeighbor());
		double sum=parameter(2);
		if( compartment.numNeighbor()==compartment.numNeighborArea() ) {
			sum *=Vi;
			for( size_t n=0 ; n<compartment.numNeighborAtLevel(1) ; ++n )
				sum += compartment.neighborArea(n);
		}
		else
			sum += compartment.numNeighbor();//assuming area=1
		
		//double invSum = 1.0;
		if( sum>0.0 )
			invSum /= sum;//To use mult instead of div
		//}
		
  //Update all compartments
  //////////////////////////////////////////////////////////////////////
  for( size_t n=0 ; n<compartment.numNeighbor() ; n++ ) {
    size_t j=compartment.neighbor(n);
    
    //Make sure neighbor marked as wall 
    //assert( y[j][variableIndex(0,2)]>0.5 ); 
		if ( y[j][variableIndex(0,2)]>0.5 ) { 
			//Extract volume of neighbor and make sure it is physical
			double Vj = y[j][variableIndex(1,0)];
			assert( Vj>0.0 );
			
			double Pij = y[i][variableIndex(0,0)] * Vi  * invSum;
			if( compartment.numNeighbor()==compartment.numNeighborArea() ) {
				double coeff = compartment.neighborArea(n)*Pij*
					parameter(4)*y[i][species]/( parameter(3)+parameter(4)*y[i][species] );
				dydt[i][species] -= parameter(0)*coeff/Vi;
				dydt[j][species] += parameter(0)*coeff/Vj;
				coeff = compartment.neighborArea(n)*Pij*
					parameter(5)*y[j][species]/( parameter(3)+parameter(5)*y[j][species] );
				dydt[i][species] += parameter(1)*coeff/Vi;
				dydt[j][species] -= parameter(1)*coeff/Vj;
			}
			else {
				double coeff = Pij*parameter(4)*y[i][species]/
					( parameter(3)+parameter(4)*y[i][species] );
				dydt[i][species] -= parameter(0)*coeff/Vi;
				dydt[j][species] += parameter(0)*coeff/Vj;
				coeff = Pij*parameter(5)*y[j][species]/
					( parameter(3)+parameter(5)*y[j][species] );
				dydt[i][species] += parameter(1)*coeff/Vi;
				dydt[j][species] -= parameter(1)*coeff/Vj;
			}
		}
	}
}

PolarizationConstHillFastTransportFractionRestrictedMM::
PolarizationConstHillFastTransportFractionRestrictedMM(std::vector<double>
											&paraValue, 
											std::vector< std::vector<size_t> > &indValue)
{  
	//Do some checks on the parameters and variable indeces
	//////////////////////////////////////////////////////////////////////
	if (paraValue.size() != 9) {
		std::cerr << "PolarizationConstHillFastTransportRestrictedMM::"
				<< "PolarizationConstHillFastTransportRestrictedMM() "
				<< "uses seven parameters (T_eff, T_inf, b/a, and c, "
				<< "K_H, n,and K_M, f_i and f_ij).\n";
		exit(EXIT_FAILURE);
	}
	if (indValue.size() != 2 || indValue[0].size() != 4 
	    || indValue[1].size() != 1) {
		std::cerr << "PolarizationConstHillFastTransportRestrictedMM::"
				<< "PolarizationConstHillFastTransportRestrictedMM() "
				<< "Polarized molecule + polarization signal + "
				<< "cell marker + wall marker "
				<< "indeces needed in"
				<< "level 1.\n" 
				<< "Variable index for size parameter needed at second level.\n";
		exit(EXIT_FAILURE);
	}
	//Check parameters
	//////////////////////////////////////////////////////////////////////
	if (paraValue[0] < 0.0 || paraValue[1] < 0.0 || paraValue[2] < 0.0 ||
	    paraValue[3] < 0.0 || paraValue[4] < 0.0 || paraValue[5] < 0.0 ||
	    paraValue[6] < 0.0 ) {
		std::cerr << "PolarizationConstHillFastTransportRestrictedMM::"
				<< "PolarizationConstHillFastTransportRestrictedMM()"
				<< " Parameters need to be positive.\n";
		exit(EXIT_FAILURE);
	}
	if (paraValue[3] > 1.0) {
		std::cerr << "PolarizationConstHillFastTransportRestrictedMM::"
				<< "PolarizationConstHillFastTransportRestrictedMM()"
				<< " Parameter c (Lin/Hill fraction need to be "
				<< "smaller than 1.\n";
		exit(EXIT_FAILURE);
	}
	//Set the variable values
	//////////////////////////////////////////////////////////////////////
	setId("polarizationConstHillFastTransportRestrictedMM");
	setParameter(paraValue);  
	setVariableIndex(indValue);
	
	//Set the parameter identities
	//////////////////////////////////////////////////////////////////////
	std::vector<std::string> tmp(numParameter());
	tmp.resize(numParameter());
	tmp[0] = "T_eff";
	tmp[1] = "T_inf";
	tmp[2] = "b/a";
	tmp[3] = "c_p";
	tmp[4] = "K_H";
	tmp[5] = "n";
	tmp[6] = "K_M";
	tmp[7] = "f_i";
	tmp[8] = "f_ij";
	setParameterId(tmp);
}

void PolarizationConstHillFastTransportFractionRestrictedMM::
derivs(Compartment &compartment,size_t species,
       std::vector< std::vector<double> > &y,
       std::vector< std::vector<double> > 
       &dydt ) {
  
  size_t i=compartment.index();
  //Only if the comp has neighbors and is a cell
  if( compartment.numNeighbor() == 0 || 
      y[i][variableIndex(0,2)]<0.5 ) return;
  
  static double KToNHill = std::pow(parameter(4),parameter(5));
  //Polarization function from CELL neighbors at level 1.
  assert( compartment.numNeighborAtLevel(1) == 
					compartment.numNeighbor() );
  //Extract volume of the compartment and make sure it is physical
  double Vi = y[i][variableIndex(1,0)];
  assert( Vi>0.0 );
  
  //Calculate f(n) and normalization
  //////////////////////////////////////////////////////////////////////
  std::vector<double> f(compartment.numNeighbor() );
  for( size_t n=0 ; n<compartment.numNeighborAtLevel(1) ; n++ ) {
    double yToN = std::pow(y[ compartment.neighborAtLevel(1,n) ]
													 [ variableIndex(0,1) ] , parameter(5));
    f[n] = (1.0-parameter(3)) + parameter(3)*yToN/(KToNHill+yToN);
  }
	
  double sum=parameter(2);
  if( compartment.numNeighbor()==compartment.numNeighborArea() ) {
    sum *=Vi;
    for( size_t n=0 ; n<compartment.numNeighborAtLevel(1) ; n++ )
      sum += f[n]*compartment.neighborArea(n);
  }
  else
    for( size_t n=0 ; n<compartment.numNeighbor() ; n++ )
      sum += f[n];
  
  double invSum = 1.0;
  if( sum>0.0 )
    invSum /= sum;//To use mult instead of div
  
  //Update all compartments
  //////////////////////////////////////////////////////////////////////
  for( size_t n=0 ; n<compartment.numNeighbor() ; n++ ) {
    size_t j=compartment.neighbor(n);
    
    //Make sure neighbor marked as wall 
    assert( y[j][variableIndex(0,3)]>0.5 ); 
    
    //Extract volume of neighbor and make sure it is physical
    double Vj = y[j][variableIndex(1,0)];
    assert( Vj>0.0 );
    
    double Pij = y[i][variableIndex(0,0)] * Vi * f[n] * invSum;
    if( compartment.numNeighbor()==compartment.numNeighborArea() ) {
      double coeff = compartment.neighborArea(n)*Pij*
				parameter(7)*y[i][species] /
				( parameter(6)+parameter(7)*y[i][species] );
      dydt[i][species] -= parameter(0)*coeff/Vi;
      dydt[j][species] += parameter(0)*coeff/Vj;
      coeff = compartment.neighborArea(n)*Pij*
				parameter(8)*y[j][species] /
				( parameter(6)+parameter(8)*y[j][species] );
      dydt[i][species] += parameter(1)*coeff/Vi;
      dydt[j][species] -= parameter(1)*coeff/Vj;
    }
    else {
      double coeff = Pij*y[i][species]/( parameter(6)+y[i][species] );
      dydt[i][species] -= parameter(0)*coeff/Vi;
      dydt[j][species] += parameter(0)*coeff/Vj;
      coeff = Pij*y[j][species]/( parameter(6)+y[j][species] );
      dydt[i][species] += parameter(1)*coeff/Vi;
      dydt[j][species] -= parameter(1)*coeff/Vj;
    }
  }
}

PolarizationTransportFastGeneralFGRestricted::PolarizationTransportFastGeneralFGRestricted(
  std::vector<double> &paraValue,
  std::vector< std::vector<size_t> > &indValue) 
{
  
  //Do some checks on the parameters and variable indeces
  //////////////////////////////////////////////////////////////////////
  if(paraValue.size() != 5) {
    std::cerr << "PolarizationTransportFastGeneralFGRestricted::"
	      << "PolarizationTransportFastGeneralFGRestricted() "
	      << "uses five parameters (T, k_f, n_f, k_g, n_g).\n";
    exit(0);
  }
  if(indValue.size() != 2 || indValue[0].size() != 2 
     || indValue[1].size() != 2) {
    std::cerr << "PolarizationTransportFastGeneralFGRestricted::"
	      << "PolarizationTransportFastGeneralFGRestricted()\n "
	      << " - Level 1: Polarized molecule + polarization signal.\n"
	      << " - Level 2: Markers for restricted transport.\n";
    exit(0);
  }

  //Set the variable values
  //////////////////////////////////////////////////////////////////////
  setId("polarizationTransportFastGeneralFGRestricted");
  setParameter(paraValue);  
  setVariableIndex(indValue);
  
  //Set the parameter identities
  //////////////////////////////////////////////////////////////////////
  std::vector<std::string> tmp(numParameter());
  tmp.resize(numParameter());
  tmp[0] = "T";
  tmp[1] = "k_f";
  tmp[2] = "n_f";
  tmp[3] = "k_g";
  tmp[4] = "n_n";
  setParameterId(tmp);
}

void PolarizationTransportFastGeneralFGRestricted::derivs(
  Compartment &compartment,size_t species,
  std::vector< std::vector<double> > &y,
  std::vector< std::vector<double> > &dydt )
{
  size_t i = compartment.index();

  //Check that this compartment is marked for this transport
  if (y[i][variableIndex(1,0)] < 0.5)
    return;

  //Polarization coefficient normalization constant
  double sum = 0.0;
  double tmp;
  for(size_t n = 0; n < compartment.numNeighbor(); n++) {
    tmp = f(y[compartment.neighbor(n)][variableIndex(0,1)]);
    tmp /= g(y[compartment.neighbor(n)][variableIndex(0,1)]);
    sum += tmp;
  }
  sum += 1;

  for(size_t n = 0; n < compartment.numNeighbor(); n++) {
    size_t j = compartment.neighbor(n);
 
    //Only update if neighbor marked for this transport
    //If same type of compartments only update once (i>j)

    double Pij, coeff;
    if (y[j][variableIndex(1,1)] > 0.5) {
      if (variableIndex(1,0) != variableIndex(1,1) || i > j) {
	Pij = f(y[j][variableIndex(0,1)]);
	Pij /= g(y[j][variableIndex(0,1)]);
	Pij *= y[i][variableIndex(0,0)];
	Pij /= sum;
  	coeff = y[i][species] * Pij;

	dydt[i][species] -= parameter(0)*coeff;
	dydt[j][species] += parameter(0)*coeff;
      }
    }
  }
}

double PolarizationTransportFastGeneralFGRestricted::f(double arg)
{
  return parameter(1) * pow(arg, parameter(2));
}

double PolarizationTransportFastGeneralFGRestricted::g(double arg)
{
  return parameter(3) * pow(arg, parameter(4));
}
