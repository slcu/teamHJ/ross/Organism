#ifndef CLVCRN_H
#define CLVCRN_H

#include "../baseReaction.h"

namespace clawus {

  ///
  /// @brief CLV1 CRN clawus network including wt, four single and four clv3 mutants
  ///
  /// This is the submitted version of the model.
  ///
  class Clvcrn : public BaseReaction {
  public:
    ///
    /// @brief Main constructor
    ///
    /// This is the main constructor which sets the parameters and variable
    /// indices that defines the reaction.
    ///
    /// @param paraValue vector with parameters
    ///
    /// @param indValue vector of vectors with variable indices
    ///
    /// @see BaseReaction::createReaction(std::vector<double> &paraValue,...)
    ///
    Clvcrn(std::vector<double> &paraValue, 
	   std::vector< std::vector<size_t> > &indValue );
    ///
    /// @brief Derivative function for this reaction class
    ///
    /// @see BaseReaction::derivs(Compartment &compartment,size_t species,...)
    ///
    void derivs(Compartment &compartment,size_t varIndex,
		std::vector< std::vector<double> > &y,
		std::vector< std::vector<double> > &dydt );
  private:
    size_t c3_1,c_1,c1_1,cc3_1,cc13_1,x_1,w_1,c3_2,c_2,c1_2,cc3_2,cc13_2,x_2,w_2,
      c3_3,c_3,c1_3,cc3_3,cc13_3,x_3,w_3,c3_4,c_4,c1_4,cc3_4,cc13_4,x_4,w_4,c3_5,
      c_5,c1_5,cc3_5,cc13_5,x_5,w_5,c3_6,c_6,c1_6,cc3_6,cc13_6,x_6,w_6,c3_7,c_7,c1_7,
      cc3_7,cc13_7,x_7,w_7,c3_8,c_8,c1_8,cc3_8,cc13_8,x_8,w_8,c3_9,c_9,c1_9,cc3_9,
      cc13_9,x_9,w_9;
    double Kpow_;
  };

  ///
  /// @brief CLV1 CRN clawus network including wt and 8 mutants
  ///
  /// This is a model with non-linear clv3 interactions trying to
  /// explain the clv3 mutant bufferning.
  ///
  class ClvcrnNonLin : public BaseReaction {
  public:
    ///
    /// @brief Main constructor
    ///
    /// This is the main constructor which sets the parameters and variable
    /// indices that defines the reaction.
    ///
    /// @param paraValue vector with parameters
    ///
    /// @param indValue vector of vectors with variable indices
    ///
    /// @see BaseReaction::createReaction(std::vector<double> &paraValue,...)
    ///
    ClvcrnNonLin(std::vector<double> &paraValue, 
	    std::vector< std::vector<size_t> > &indValue );
    ///
    /// @brief Derivative function for this reaction class
    ///
    /// @see BaseReaction::derivs(Compartment &compartment,size_t species,...)
    ///
    void derivs(Compartment &compartment,size_t varIndex,
		std::vector< std::vector<double> > &y,
		std::vector< std::vector<double> > &dydt );
  private:
    size_t c3_1,c_1,c1_1,cc3_1,cc13_1,x_1,w_1,c3_2,c_2,c1_2,cc3_2,cc13_2,x_2,w_2,
      c3_3,c_3,c1_3,cc3_3,cc13_3,x_3,w_3,c3_4,c_4,c1_4,cc3_4,cc13_4,x_4,w_4,c3_5,
      c_5,c1_5,cc3_5,cc13_5,x_5,w_5,c3_6,c_6,c1_6,cc3_6,cc13_6,x_6,w_6,c3_7,c_7,c1_7,
      cc3_7,cc13_7,x_7,w_7,c3_8,c_8,c1_8,cc3_8,cc13_8,x_8,w_8,c3_9,c_9,c1_9,cc3_9,
      cc13_9,x_9,w_9,cm3_1,cm3_2,cm3_3,cm3_4,cm3_5,cm3_6,cm3_7,cm3_8,cm3_9;
    double Kpow_;
  };
}
#endif 
