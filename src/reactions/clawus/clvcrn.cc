#include "clvcrn.h"
#include <cmath>

clawus::Clvcrn::Clvcrn(std::vector<double> &paraValue, 
		       std::vector< std::vector<size_t> > 
		       &indValue ) 
{
  //Do some checks on the parameters and variable indeces
  //
  if( paraValue.size()!=20 ) {
    std::cerr << "Clvcrn::Clvcrn() "
	      << "Uses 20 parameters." << std::endl;
    exit(0);
  }
  if ( indValue.size() !=1 && 
       indValue[0].size() != 1) { 
    std::cerr << "ClvCrn::Clvcrn() "
	      << "uses one index, start of the variables in order C1 C2 C3 C13 C23 X W " 
	      << "times wt+8 mutants (63)." << std::endl; 
    exit(0);
  }
  //Set the variable values
  //
  setId("clvcrn");
  setParameter(paraValue);  
  Kpow_ = std::pow(parameter(7),parameter(8));
  setVariableIndex(indValue);
  c1_1 = variableIndex(0,0);
  c_1 = c1_1+1;
  c3_1 = c1_1+2;
  cc13_1 = c1_1+3;
  cc3_1 = c1_1+4;
  x_1 = c1_1+5;
  w_1 = c1_1+6;

  c1_2 = c1_1+7;
  c_2 = c1_2+1;
  c3_2 = c1_2+2;
  cc13_2 = c1_2+3;
  cc3_2 = c1_2+4;
  x_2 = c1_2+5;
  w_2 = c1_2+6;

  c1_3 = c1_1+14;
  c_3 = c1_3+1;
  c3_3 = c1_3+2;
  cc13_3 = c1_3+3;
  cc3_3 = c1_3+4;
  x_3 = c1_3+5;
  w_3 = c1_3+6;

  c1_4 = c1_1+21;
  c_4 = c1_4+1;
  c3_4 = c1_4+2;
  cc13_4 = c1_4+3;
  cc3_4 = c1_4+4;
  x_4 = c1_4+5;
  w_4 = c1_4+6;

  c1_5 = c1_1+28;
  c_5 = c1_5+1;
  c3_5 = c1_5+2;
  cc13_5 = c1_5+3;
  cc3_5 = c1_5+4;
  x_5 = c1_5+5;
  w_5 = c1_5+6;

  c1_6 = c1_1+35;
  c_6 = c1_6+1;
  c3_6 = c1_6+2;
  cc13_6 = c1_6+3;
  cc3_6 = c1_6+4;
  x_6 = c1_6+5;
  w_6 = c1_6+6;

  c1_7 = c1_1+42;
  c_7 = c1_7+1;
  c3_7 = c1_7+2;
  cc13_7 = c1_7+3;
  cc3_7 = c1_7+4;
  x_7 = c1_7+5;
  w_7 = c1_7+6;

  c1_8 = c1_1+49;
  c_8 = c1_8+1;
  c3_8 = c1_8+2;
  cc13_8 = c1_8+3;
  cc3_8 = c1_8+4;
  x_8 = c1_8+5;
  w_8 = c1_8+6;

  c1_9 = c1_1+56;
  c_9 = c1_9+1;
  c3_9 = c1_9+2;
  cc13_9 = c1_9+3;
  cc3_9 = c1_9+4;
  x_9 = c1_9+5;
  w_9 = c1_9+6;
  
  //Set the parameter identities
  //
  std::vector<std::string> tmp( numParameter() );
  tmp[0] = "p_0 k_1";
  tmp[1] = "p_1 k_2";
  tmp[2] = "p_2 k_3";
  tmp[3] = "p_3 k_4";
  tmp[4] = "p_4 k_5";
  tmp[5] = "p_5 k_6";
  tmp[6] = "p_6 k_7";
  tmp[7] = "p_7 K";
  tmp[8] = "p_8 n";
  tmp[9] = "p_9 t_1";
  tmp[10] = "p_10 s_1";
  tmp[11] = "p_11 t_2";
  tmp[12] = "p_12 s_2";
  tmp[13] = "p_13 t_3";
  tmp[14] = "p_14 s_3";
  tmp[15] = "p_15 t_4";
  tmp[16] = "p_16 s_4";
  tmp[17] = "p_17 d_w";
  tmp[18] = "p_18 k_w";
  tmp[19] = "p_19 k_3^{weak}";
  setParameterId( tmp );
}

void clawus::Clvcrn::derivs(Compartment &compartment,size_t varIndex,
			    std::vector< std::vector<double> > &y,
			    std::vector< std::vector<double> > &dydt ) 
{
  size_t i=compartment.index();
  //
  // --- WT ---
  //
  // CLV3
  dydt[i][c3_1] += parameter(13)*(parameter(14)-y[i][c3_1]) + parameter(18)*y[i][w_1]
    + parameter(1)*y[i][cc13_1]-parameter(0)*y[i][c3_1]*y[i][c1_1]
    + parameter(4)*y[i][cc3_1] - parameter(3)*y[i][c3_1]*y[i][c_1];
  // CRN
  dydt[i][c_1] += parameter(11)*(parameter(12)-y[i][c_1])
    + parameter(4)*y[i][cc3_1]-parameter(3)*y[i][c3_1]*y[i][c_1];
  // CLV1
  dydt[i][c1_1] += parameter(9)*(parameter(10)-y[i][c1_1])
    + parameter(1)*y[i][cc13_1]-parameter(0)*y[i][c3_1]*y[i][c1_1];
  // CLV3-CRN
  dydt[i][cc3_1] += parameter(3)*y[i][c3_1]*y[i][c_1]-(parameter(4)+parameter(11))*y[i][cc3_1];
  // CLV3-CLV1
  dydt[i][cc13_1] += parameter(0)*y[i][c3_1]*y[i][c1_1]-(parameter(1)+parameter(9))*y[i][cc13_1];
  // X
  dydt[i][x_1] += parameter(15)*(parameter(16)-y[i][x_1])
    + parameter(2)*y[i][cc13_1] + parameter(5)*y[i][cc3_1];
  // WUS
  double hill = Kpow_/( Kpow_+std::pow(y[i][x_1],parameter(8)) );
  dydt[i][w_1] += parameter(6)*hill - parameter(17)*y[i][w_1];
  //
  // --- clv1-11 ---(null)
  //
  // CLV3
  dydt[i][c3_2] += parameter(13)*(parameter(14)-y[i][c3_2]) + parameter(18)*y[i][w_2]
    + parameter(1)*y[i][cc13_2]-parameter(0)*y[i][c3_2]*y[i][c1_2]
    + parameter(4)*y[i][cc3_2] - parameter(3)*y[i][c3_2]*y[i][c_2];
  // CRN
  dydt[i][c_2] += parameter(11)*(parameter(12)-y[i][c_2])
    + parameter(4)*y[i][cc3_2]-parameter(3)*y[i][c3_2]*y[i][c_2];
  // CLV1
  //dydt[i][c1_2] += parameter(9)*(parameter(10)-y[i][c1_2])
  //+ parameter(1)*y[i][cc13_2]-parameter(0)*y[i][c3_2]*y[i][c1_2];
  // CLV3-CRN
  dydt[i][cc3_2] += parameter(3)*y[i][c3_2]*y[i][c_2]-(parameter(4)+parameter(11))*y[i][cc3_2];
  // CLV3-CLV1
  //dydt[i][cc13_2] += parameter(0)*y[i][c3_2]*y[i][c1_2]-(parameter(1)+parameter(9))*y[i][cc13_2];
  // X
  dydt[i][x_2] += parameter(15)*(parameter(16)-y[i][x_2])
    + parameter(2)*y[i][cc13_2] + parameter(5)*y[i][cc3_2];
  // WUS
  hill = Kpow_/( Kpow_+std::pow(y[i][x_2],parameter(8)) );
  dydt[i][w_2] += parameter(6)*hill - parameter(17)*y[i][w_2];

  //
  // --- clv1-1 ---(LoS)
  //
  // CLV3
  dydt[i][c3_3] += parameter(13)*(parameter(14)-y[i][c3_3]) + parameter(18)*y[i][w_3]
    + parameter(1)*y[i][cc13_3]-parameter(0)*y[i][c3_3]*y[i][c1_3]
    + parameter(4)*y[i][cc3_3] - parameter(3)*y[i][c3_3]*y[i][c_3];
  // CRN
  dydt[i][c_3] += parameter(11)*(parameter(12)-y[i][c_3])
    + parameter(4)*y[i][cc3_3]-parameter(3)*y[i][c3_3]*y[i][c_3];
  // CLV1
  dydt[i][c1_3] += parameter(9)*(parameter(10)-y[i][c1_3])
    + parameter(1)*y[i][cc13_3]-parameter(0)*y[i][c3_3]*y[i][c1_3];
  // CLV3-CRN
  dydt[i][cc3_3] += parameter(3)*y[i][c3_3]*y[i][c_3]-(parameter(4)+parameter(11))*y[i][cc3_3];
  // CLV3-CLV1
  dydt[i][cc13_3] += parameter(0)*y[i][c3_3]*y[i][c1_3]-(parameter(1)+parameter(9))*y[i][cc13_3];
  // X
  dydt[i][x_3] += parameter(15)*(parameter(16)-y[i][x_3])
    + parameter(19)*y[i][cc13_3] + parameter(5)*y[i][cc3_3];
  // WUS
  hill = Kpow_/( Kpow_+std::pow(y[i][x_3],parameter(8)) );
  dydt[i][w_3] += parameter(6)*hill - parameter(17)*y[i][w_3];

  //
  // --- clv2-1/crn-1 ---(null)
  //
  // CLV3
  dydt[i][c3_4] += parameter(13)*(parameter(14)-y[i][c3_4]) + parameter(18)*y[i][w_4]
    + parameter(1)*y[i][cc13_4]-parameter(0)*y[i][c3_4]*y[i][c1_4]
    + parameter(4)*y[i][cc3_4] - parameter(3)*y[i][c3_4]*y[i][c_4];
  // CRN
  //dydt[i][c_4] += parameter(11)*(parameter(12)-y[i][c_4])
  //+ parameter(4)*y[i][cc3_4]-parameter(3)*y[i][c3_4]*y[i][c_4];
  // CLV1
  dydt[i][c1_4] += parameter(9)*(parameter(10)-y[i][c1_4])
    + parameter(1)*y[i][cc13_4]-parameter(0)*y[i][c3_4]*y[i][c1_4];
  // CLV3-CRN
  //dydt[i][cc3_4] += parameter(3)*y[i][c3_4]*y[i][c_4]-(parameter(4)+parameter(11))*y[i][cc3_4];
  // CLV3-CLV1
  dydt[i][cc13_4] += parameter(0)*y[i][c3_4]*y[i][c1_4]-(parameter(1)+parameter(9))*y[i][cc13_4];
  // X
  dydt[i][x_4] += parameter(15)*(parameter(16)-y[i][x_4])
    + parameter(2)*y[i][cc13_4] + parameter(5)*y[i][cc3_4];
  // WUS
  hill = Kpow_/( Kpow_+std::pow(y[i][x_4],parameter(8)) );
  dydt[i][w_4] += parameter(6)*hill - parameter(17)*y[i][w_4];

  //
  // --- crn-1 ---(LoS)
  //
  // CLV3
  dydt[i][c3_5] += parameter(13)*(parameter(14)-y[i][c3_5]) + parameter(18)*y[i][w_5]
    + parameter(1)*y[i][cc13_5]-parameter(0)*y[i][c3_5]*y[i][c1_5]
    + parameter(4)*y[i][cc3_5] - parameter(3)*y[i][c3_5]*y[i][c_5];
  // CRN
  dydt[i][c_5] += parameter(11)*(parameter(12)-y[i][c_5])
    + parameter(4)*y[i][cc3_5]-parameter(3)*y[i][c3_5]*y[i][c_5];
  // CLV1
  dydt[i][c1_5] += parameter(9)*(parameter(10)-y[i][c1_5])
    + parameter(1)*y[i][cc13_5]-parameter(0)*y[i][c3_5]*y[i][c1_5];
  // CLV3-CRN
  dydt[i][cc3_5] += parameter(3)*y[i][c3_5]*y[i][c_5]-(parameter(4)+parameter(11))*y[i][cc3_5];
  // CLV3-CLV1
  dydt[i][cc13_5] += parameter(0)*y[i][c3_5]*y[i][c1_5]-(parameter(1)+parameter(9))*y[i][cc13_5];
  // X
  dydt[i][x_5] += parameter(15)*(parameter(16)-y[i][x_5])
    + parameter(2)*y[i][cc13_5];
  // WUS
  hill = Kpow_/( Kpow_+std::pow(y[i][x_5],parameter(8)) );
  dydt[i][w_5] += parameter(6)*hill - parameter(17)*y[i][w_5];

  //
  // --- clv3-2 ---
  //
  // CLV3
    dydt[i][c3_6] += parameter(13)*(0.0-y[i][c3_6]) + 0.0*y[i][w_6]
  + parameter(1)*y[i][cc13_6]-parameter(0)*y[i][c3_6]*y[i][c1_6]
  + parameter(4)*y[i][cc3_6] - parameter(3)*y[i][c3_6]*y[i][c_6];
  // CRN
  dydt[i][c_6] += parameter(11)*(parameter(12)-y[i][c_6])
  + parameter(4)*y[i][cc3_6]-parameter(3)*y[i][c3_6]*y[i][c_6];
  // CLV1
  dydt[i][c1_6] += parameter(9)*(parameter(10)-y[i][c1_6])
  + parameter(1)*y[i][cc13_6]-parameter(0)*y[i][c3_6]*y[i][c1_6];
  // CLV3-CRN
  dydt[i][cc3_6] += parameter(3)*y[i][c3_6]*y[i][c_6]-(parameter(4)+parameter(11))*y[i][cc3_6];
  // CLV3-CLV1
  dydt[i][cc13_6] += parameter(0)*y[i][c3_6]*y[i][c1_6]-(parameter(1)+parameter(9))*y[i][cc13_6];
  // X
  dydt[i][x_6] += parameter(15)*(parameter(16)-y[i][x_6])
  + parameter(2)*y[i][cc13_6] + parameter(5)*y[i][cc3_6];
  // WUS
  hill = Kpow_/( Kpow_+std::pow(y[i][x_6],parameter(8)) );
  dydt[i][w_6] += parameter(6)*hill - parameter(17)*y[i][w_6];

  //
  // --- 0.3*clv3 ---
  //
  // CLV3
  dydt[i][c3_7] += parameter(13)*(0.3*parameter(14)-y[i][c3_7]) + 0.3*parameter(18)*y[i][w_7]
    + parameter(1)*y[i][cc13_7]-parameter(0)*y[i][c3_7]*y[i][c1_7]
    + parameter(4)*y[i][cc3_7] - parameter(3)*y[i][c3_7]*y[i][c_7];
  // CRN
  dydt[i][c_7] += parameter(11)*(parameter(12)-y[i][c_7])
    + parameter(4)*y[i][cc3_7]-parameter(3)*y[i][c3_7]*y[i][c_7];
  // CLV1
  dydt[i][c1_7] += parameter(9)*(parameter(10)-y[i][c1_7])
    + parameter(1)*y[i][cc13_7]-parameter(0)*y[i][c3_7]*y[i][c1_7];
  // CLV3-CRN
  dydt[i][cc3_7] += parameter(3)*y[i][c3_7]*y[i][c_7]-(parameter(4)+parameter(11))*y[i][cc3_7];
  // CLV3-CLV1
  dydt[i][cc13_7] += parameter(0)*y[i][c3_7]*y[i][c1_7]-(parameter(1)+parameter(9))*y[i][cc13_7];
  // X
  dydt[i][x_7] += parameter(15)*(parameter(16)-y[i][x_7])
    + parameter(2)*y[i][cc13_7] + parameter(5)*y[i][cc3_7];
  // WUS
  hill = Kpow_/( Kpow_+std::pow(y[i][x_7],parameter(8)) );
  dydt[i][w_7] += parameter(6)*hill - parameter(17)*y[i][w_7];
  
  //
  // --- 3*clv3 ---
  //
  // CLV3
  dydt[i][c3_8] += parameter(13)*(3.0*parameter(14)-y[i][c3_8]) + 3.0*parameter(18)*y[i][w_8]
    + parameter(1)*y[i][cc13_8]-parameter(0)*y[i][c3_8]*y[i][c1_8]
    + parameter(4)*y[i][cc3_8] - parameter(3)*y[i][c3_8]*y[i][c_8];
  // CRN
  dydt[i][c_8] += parameter(11)*(parameter(12)-y[i][c_8])
    + parameter(4)*y[i][cc3_8]-parameter(3)*y[i][c3_8]*y[i][c_8];
  // CLV1
  dydt[i][c1_8] += parameter(9)*(parameter(10)-y[i][c1_8])
    + parameter(1)*y[i][cc13_8]-parameter(0)*y[i][c3_8]*y[i][c1_8];
  // CLV3-CRN
  dydt[i][cc3_8] += parameter(3)*y[i][c3_8]*y[i][c_8]-(parameter(4)+parameter(11))*y[i][cc3_8];
  // CLV3-CLV1
  dydt[i][cc13_8] += parameter(0)*y[i][c3_8]*y[i][c1_8]-(parameter(1)+parameter(9))*y[i][cc13_8];
  // X
  dydt[i][x_8] += parameter(15)*(parameter(16)-y[i][x_8])
    + parameter(2)*y[i][cc13_8] + parameter(5)*y[i][cc3_8];
  // WUS
  hill = Kpow_/( Kpow_+std::pow(y[i][x_8],parameter(8)) );
  dydt[i][w_8] += parameter(6)*hill - parameter(17)*y[i][w_8];

  //
  // --- 100*clv3 ---
  //
  // CLV3
  dydt[i][c3_9] += parameter(13)*(100.0*parameter(14)-y[i][c3_9]) + 100.0*parameter(18)*y[i][w_9]
    + parameter(1)*y[i][cc13_9]-parameter(0)*y[i][c3_9]*y[i][c1_9]
    + parameter(4)*y[i][cc3_9] - parameter(3)*y[i][c3_9]*y[i][c_9];
  // CRN
  dydt[i][c_9] += parameter(11)*(parameter(12)-y[i][c_9])
    + parameter(4)*y[i][cc3_9]-parameter(3)*y[i][c3_9]*y[i][c_9];
  // CLV1
  dydt[i][c1_9] += parameter(9)*(parameter(10)-y[i][c1_9])
    + parameter(1)*y[i][cc13_9]-parameter(0)*y[i][c3_9]*y[i][c1_9];
  // CLV3-CRN
  dydt[i][cc3_9] += parameter(3)*y[i][c3_9]*y[i][c_9]-(parameter(4)+parameter(11))*y[i][cc3_9];
  // CLV3-CLV1
  dydt[i][cc13_9] += parameter(0)*y[i][c3_9]*y[i][c1_9]-(parameter(1)+parameter(9))*y[i][cc13_9];
  // X
  dydt[i][x_9] += parameter(15)*(parameter(16)-y[i][x_9])
    + parameter(2)*y[i][cc13_9] + parameter(5)*y[i][cc3_9];
  // WUS
  hill = Kpow_/( Kpow_+std::pow(y[i][x_9],parameter(8)) );
  dydt[i][w_9] += parameter(6)*hill - parameter(17)*y[i][w_9];
}

clawus::ClvcrnNonLin::ClvcrnNonLin(std::vector<double> &paraValue, 
		       std::vector< std::vector<size_t> > 
		       &indValue ) 
{
  //Do some checks on the parameters and variable indeces
  //
  if( paraValue.size()!=26 ) {
    std::cerr << "ClvcrnNonLin::ClvcrnNonLin() "
	      << "Uses 26 parameters." << std::endl;
    exit(0);
  }
  if ( indValue.size() !=1 && 
       indValue[0].size() != 1) { 
    std::cerr << "ClvCrnNonLin::ClvcrnNonLin() "
	      << "uses one index, start of the variables in order C1 C2 C3 C13 C23 X W " 
	      << "times mutants." << std::endl; 
    exit(0);
  }
  //Set the variable values
  //
  setId("clvcrnNonLin");
  setParameter(paraValue);  
  Kpow_ = std::pow(parameter(7),parameter(8));
  setVariableIndex(indValue);
  c1_1 = variableIndex(0,0);
  c_1 = c1_1+1;
  c3_1 = c1_1+2;
  cc13_1 = c1_1+3;
  cc3_1 = c1_1+4;
  x_1 = c1_1+5;
  w_1 = c1_1+6;

  c1_2 = c1_1+7;
  c_2 = c1_2+1;
  c3_2 = c1_2+2;
  cc13_2 = c1_2+3;
  cc3_2 = c1_2+4;
  x_2 = c1_2+5;
  w_2 = c1_2+6;

  c1_3 = c1_1+14;
  c_3 = c1_3+1;
  c3_3 = c1_3+2;
  cc13_3 = c1_3+3;
  cc3_3 = c1_3+4;
  x_3 = c1_3+5;
  w_3 = c1_3+6;

  c1_4 = c1_1+21;
  c_4 = c1_4+1;
  c3_4 = c1_4+2;
  cc13_4 = c1_4+3;
  cc3_4 = c1_4+4;
  x_4 = c1_4+5;
  w_4 = c1_4+6;

  c1_5 = c1_1+28;
  c_5 = c1_5+1;
  c3_5 = c1_5+2;
  cc13_5 = c1_5+3;
  cc3_5 = c1_5+4;
  x_5 = c1_5+5;
  w_5 = c1_5+6;

  c1_6 = c1_1+35;
  c_6 = c1_6+1;
  c3_6 = c1_6+2;
  cc13_6 = c1_6+3;
  cc3_6 = c1_6+4;
  x_6 = c1_6+5;
  w_6 = c1_6+6;

  c1_7 = c1_1+42;
  c_7 = c1_7+1;
  c3_7 = c1_7+2;
  cc13_7 = c1_7+3;
  cc3_7 = c1_7+4;
  x_7 = c1_7+5;
  w_7 = c1_7+6;

  c1_8 = c1_1+49;
  c_8 = c1_8+1;
  c3_8 = c1_8+2;
  cc13_8 = c1_8+3;
  cc3_8 = c1_8+4;
  x_8 = c1_8+5;
  w_8 = c1_8+6;

  c1_9 = c1_1+56;
  c_9 = c1_9+1;
  c3_9 = c1_9+2;
  cc13_9 = c1_9+3;
  cc3_9 = c1_9+4;
  x_9 = c1_9+5;
  w_9 = c1_9+6;

  cm3_1 = c1_1+63;
  cm3_2 = cm3_1+1;
  cm3_3 = cm3_1+2;
  cm3_4 = cm3_1+3;
  cm3_5 = cm3_1+4;
  cm3_6 = cm3_1+5;
  cm3_7 = cm3_1+6;
  cm3_8 = cm3_1+7;
  cm3_9 = cm3_1+8;
  
  //Set the parameter identities
  //
  std::vector<std::string> tmp( numParameter() );
  tmp[0] = "p_0 k_1";
  tmp[1] = "p_1 k_2";
  tmp[2] = "p_2 k_3";
  tmp[3] = "p_3 k_4";
  tmp[4] = "p_4 k_5";
  tmp[5] = "p_5 k_6";
  tmp[6] = "p_6 k_7";
  tmp[7] = "p_7 K";
  tmp[8] = "p_8 n";
  tmp[9] = "p_9 t_1";
  tmp[10] = "p_10 s_1";
  tmp[11] = "p_11 t_2";
  tmp[12] = "p_12 s_2";
  tmp[13] = "p_13 t_3";
  tmp[14] = "p_14 s_3";
  tmp[15] = "p_15 t_4";
  tmp[16] = "p_16 s_4";
  tmp[17] = "p_17 d_w";
  tmp[18] = "p_18 k_w";
  tmp[19] = "p_19 k_3^{weak}";
  tmp[20] = "p_20 V1";
  tmp[21] = "p_21 K1";
  tmp[22] = "p_22 n1";
  tmp[23] = "p_23 V2";
  tmp[24] = "p_24 K2";
  tmp[25] = "p_25 n2";
  setParameterId( tmp );
}

void clawus::ClvcrnNonLin::derivs(Compartment &compartment,size_t varIndex,
			    std::vector< std::vector<double> > &y,
			    std::vector< std::vector<double> > &dydt ) 
{
  size_t i=compartment.index();
  //
  // --- WT ---
  //
  // CLV3 mRNA/lignad
  dydt[i][cm3_1] += parameter(13)*(parameter(14)-y[i][cm3_1]) + parameter(18)*y[i][w_1];
  // CLV3 'effective' ligand
  //dydt[i][c3_1] += parameter(13)*(parameter(14)-y[i][c3_1]) + parameter(18)*y[i][w_1]
  double c1Pow = std::pow(y[i][cm3_1],parameter(22));
  double c2Pow = std::pow(y[i][cm3_1],parameter(25));
  double K1Pow = std::pow(parameter(21),parameter(22));
  double K2Pow = std::pow(parameter(24),parameter(25));
  dydt[i][c3_1] += parameter(20)*c1Pow/(K1Pow+c1Pow)
    + parameter(23)*c2Pow*y[i][cm3_1]/(K2Pow+c2Pow) - y[i][c3_1]
    + parameter(1)*y[i][cc13_1]-parameter(0)*y[i][c3_1]*y[i][c1_1]
    + parameter(4)*y[i][cc3_1] - parameter(3)*y[i][c3_1]*y[i][c_1];
  // CRN
  dydt[i][c_1] += parameter(11)*(parameter(12)-y[i][c_1])
    + parameter(4)*y[i][cc3_1]-parameter(3)*y[i][c3_1]*y[i][c_1];
  // CLV1
  dydt[i][c1_1] += parameter(9)*(parameter(10)-y[i][c1_1])
    + parameter(1)*y[i][cc13_1]-parameter(0)*y[i][c3_1]*y[i][c1_1];
  // CLV3-CRN
  dydt[i][cc3_1] += parameter(3)*y[i][c3_1]*y[i][c_1]-(parameter(4)+parameter(11))*y[i][cc3_1];
  // CLV3-CLV1
  dydt[i][cc13_1] += parameter(0)*y[i][c3_1]*y[i][c1_1]-(parameter(1)+parameter(9))*y[i][cc13_1];
  // X
  dydt[i][x_1] += parameter(15)*(parameter(16)-y[i][x_1])
    + parameter(2)*y[i][cc13_1] + parameter(5)*y[i][cc3_1];
  // WUS
  double hill = Kpow_/( Kpow_+std::pow(y[i][x_1],parameter(8)) );
  dydt[i][w_1] += parameter(6)*hill - parameter(17)*y[i][w_1];
  //
  // --- clv1-11 ---(null)
  //
  // CLV3
  dydt[i][cm3_2] += parameter(13)*(parameter(14)-y[i][cm3_2]) + parameter(18)*y[i][w_2];
    // CLV3 'effective' ligand
  c1Pow = std::pow(y[i][cm3_2],parameter(22));
  c2Pow = std::pow(y[i][cm3_2],parameter(25));
  dydt[i][c3_2] += parameter(20)*c1Pow/(K1Pow+c1Pow)
    + parameter(23)*c2Pow*y[i][cm3_2]/(K2Pow+c2Pow) - y[i][c3_2]
    + parameter(1)*y[i][cc13_2]-parameter(0)*y[i][c3_2]*y[i][c1_2]
    + parameter(4)*y[i][cc3_2] - parameter(3)*y[i][c3_2]*y[i][c_2];
  // CRN
  dydt[i][c_2] += parameter(11)*(parameter(12)-y[i][c_2])
    + parameter(4)*y[i][cc3_2]-parameter(3)*y[i][c3_2]*y[i][c_2];
  // CLV1
  //dydt[i][c1_2] += parameter(9)*(parameter(10)-y[i][c1_2])
  //+ parameter(1)*y[i][cc13_2]-parameter(0)*y[i][c3_2]*y[i][c1_2];
  // CLV3-CRN
  dydt[i][cc3_2] += parameter(3)*y[i][c3_2]*y[i][c_2]-(parameter(4)+parameter(11))*y[i][cc3_2];
  // CLV3-CLV1
  //dydt[i][cc13_2] += parameter(0)*y[i][c3_2]*y[i][c1_2]-(parameter(1)+parameter(9))*y[i][cc13_2];
  // X
  dydt[i][x_2] += parameter(15)*(parameter(16)-y[i][x_2])
    + parameter(2)*y[i][cc13_2] + parameter(5)*y[i][cc3_2];
  // WUS
  hill = Kpow_/( Kpow_+std::pow(y[i][x_2],parameter(8)) );
  dydt[i][w_2] += parameter(6)*hill - parameter(17)*y[i][w_2];

  //
  // --- clv1-1 ---(LoS)
  //
  // CLV3
  dydt[i][cm3_3] += parameter(13)*(parameter(14)-y[i][cm3_3]) + parameter(18)*y[i][w_3];
    // CLV3 'effective' ligand
  c1Pow = std::pow(y[i][cm3_3],parameter(22));
  c2Pow = std::pow(y[i][cm3_3],parameter(25));
  dydt[i][c3_3] += parameter(20)*c1Pow/(K1Pow+c1Pow)
    + parameter(23)*c2Pow*y[i][cm3_3]/(K2Pow+c2Pow) - y[i][c3_3]
    + parameter(1)*y[i][cc13_3]-parameter(0)*y[i][c3_3]*y[i][c1_3]
    + parameter(4)*y[i][cc3_3] - parameter(3)*y[i][c3_3]*y[i][c_3];
  // CRN
  dydt[i][c_3] += parameter(11)*(parameter(12)-y[i][c_3])
    + parameter(4)*y[i][cc3_3]-parameter(3)*y[i][c3_3]*y[i][c_3];
  // CLV1
  dydt[i][c1_3] += parameter(9)*(parameter(10)-y[i][c1_3])
    + parameter(1)*y[i][cc13_3]-parameter(0)*y[i][c3_3]*y[i][c1_3];
  // CLV3-CRN
  dydt[i][cc3_3] += parameter(3)*y[i][c3_3]*y[i][c_3]-(parameter(4)+parameter(11))*y[i][cc3_3];
  // CLV3-CLV1
  dydt[i][cc13_3] += parameter(0)*y[i][c3_3]*y[i][c1_3]-(parameter(1)+parameter(9))*y[i][cc13_3];
  // X
  dydt[i][x_3] += parameter(15)*(parameter(16)-y[i][x_3])
    + parameter(19)*y[i][cc13_3] + parameter(5)*y[i][cc3_3];
  // WUS
  hill = Kpow_/( Kpow_+std::pow(y[i][x_3],parameter(8)) );
  dydt[i][w_3] += parameter(6)*hill - parameter(17)*y[i][w_3];

  //
  // --- clv2-1/crn-1 ---(null)
  //
  // CLV3
  dydt[i][cm3_4] += parameter(13)*(parameter(14)-y[i][cm3_4]) + parameter(18)*y[i][w_4];
    // CLV3 'effective' ligand
  c1Pow = std::pow(y[i][cm3_4],parameter(22));
  c2Pow = std::pow(y[i][cm3_4],parameter(25));
  dydt[i][c3_4] += parameter(20)*c1Pow/(K1Pow+c1Pow)
    + parameter(23)*c2Pow*y[i][cm3_4]/(K2Pow+c2Pow) - y[i][c3_4]
    + parameter(1)*y[i][cc13_4]-parameter(0)*y[i][c3_4]*y[i][c1_4]
    + parameter(4)*y[i][cc3_4] - parameter(3)*y[i][c3_4]*y[i][c_4];
  // CRN
  //dydt[i][c_4] += parameter(11)*(parameter(12)-y[i][c_4])
  //+ parameter(4)*y[i][cc3_4]-parameter(3)*y[i][c3_4]*y[i][c_4];
  // CLV1
  dydt[i][c1_4] += parameter(9)*(parameter(10)-y[i][c1_4])
    + parameter(1)*y[i][cc13_4]-parameter(0)*y[i][c3_4]*y[i][c1_4];
  // CLV3-CRN
  //dydt[i][cc3_4] += parameter(3)*y[i][c3_4]*y[i][c_4]-(parameter(4)+parameter(11))*y[i][cc3_4];
  // CLV3-CLV1
  dydt[i][cc13_4] += parameter(0)*y[i][c3_4]*y[i][c1_4]-(parameter(1)+parameter(9))*y[i][cc13_4];
  // X
  dydt[i][x_4] += parameter(15)*(parameter(16)-y[i][x_4])
    + parameter(2)*y[i][cc13_4] + parameter(5)*y[i][cc3_4];
  // WUS
  hill = Kpow_/( Kpow_+std::pow(y[i][x_4],parameter(8)) );
  dydt[i][w_4] += parameter(6)*hill - parameter(17)*y[i][w_4];

  //
  // --- crn-1 ---(LoS)
  //
  // CLV3
  dydt[i][cm3_5] += parameter(13)*(parameter(14)-y[i][cm3_5]) + parameter(18)*y[i][w_5];
    // CLV3 'effective' ligand
  c1Pow = std::pow(y[i][cm3_5],parameter(22));
  c2Pow = std::pow(y[i][cm3_5],parameter(25));
  dydt[i][c3_5] += parameter(20)*c1Pow/(K1Pow+c1Pow)
    + parameter(23)*c2Pow*y[i][cm3_5]/(K2Pow+c2Pow) - y[i][c3_5]
    + parameter(1)*y[i][cc13_5]-parameter(0)*y[i][c3_5]*y[i][c1_5]
    + parameter(4)*y[i][cc3_5] - parameter(3)*y[i][c3_5]*y[i][c_5];
  // CRN
  dydt[i][c_5] += parameter(11)*(parameter(12)-y[i][c_5])
    + parameter(4)*y[i][cc3_5]-parameter(3)*y[i][c3_5]*y[i][c_5];
  // CLV1
  dydt[i][c1_5] += parameter(9)*(parameter(10)-y[i][c1_5])
    + parameter(1)*y[i][cc13_5]-parameter(0)*y[i][c3_5]*y[i][c1_5];
  // CLV3-CRN
  dydt[i][cc3_5] += parameter(3)*y[i][c3_5]*y[i][c_5]-(parameter(4)+parameter(11))*y[i][cc3_5];
  // CLV3-CLV1
  dydt[i][cc13_5] += parameter(0)*y[i][c3_5]*y[i][c1_5]-(parameter(1)+parameter(9))*y[i][cc13_5];
  // X
  dydt[i][x_5] += parameter(15)*(parameter(16)-y[i][x_5])
    + parameter(2)*y[i][cc13_5];
  // WUS
  hill = Kpow_/( Kpow_+std::pow(y[i][x_5],parameter(8)) );
  dydt[i][w_5] += parameter(6)*hill - parameter(17)*y[i][w_5];

  //
  // --- clv3-2 ---
  //
  // CLV3
  dydt[i][cm3_6] += parameter(13)*(0.0-y[i][cm3_6]) + 0.0*y[i][w_6];
    // CLV3 'effective' ligand
  c1Pow = std::pow(y[i][cm3_6],parameter(22));
  c2Pow = std::pow(y[i][cm3_6],parameter(25));
  dydt[i][c3_6] += parameter(20)*c1Pow/(K1Pow+c1Pow)
    + parameter(23)*c2Pow*y[i][cm3_6]/(K2Pow+c2Pow) - y[i][c3_6]
    + parameter(1)*y[i][cc13_6]-parameter(0)*y[i][c3_6]*y[i][c1_6]
    + parameter(4)*y[i][cc3_6] - parameter(3)*y[i][c3_6]*y[i][c_6];
  // CRN
  dydt[i][c_6] += parameter(11)*(parameter(12)-y[i][c_6])
    + parameter(4)*y[i][cc3_6]-parameter(3)*y[i][c3_6]*y[i][c_6];
  // CLV1
  dydt[i][c1_6] += parameter(9)*(parameter(10)-y[i][c1_6])
    + parameter(1)*y[i][cc13_6]-parameter(0)*y[i][c3_6]*y[i][c1_6];
  // CLV3-CRN
  dydt[i][cc3_6] += parameter(3)*y[i][c3_6]*y[i][c_6]-(parameter(4)+parameter(11))*y[i][cc3_6];
  // CLV3-CLV1
  dydt[i][cc13_6] += parameter(0)*y[i][c3_6]*y[i][c1_6]-(parameter(1)+parameter(9))*y[i][cc13_6];
  // X
  dydt[i][x_6] += parameter(15)*(parameter(16)-y[i][x_6])
    + parameter(2)*y[i][cc13_6] + parameter(5)*y[i][cc3_6];
  // WUS
  hill = Kpow_/( Kpow_+std::pow(y[i][x_6],parameter(8)) );
  dydt[i][w_6] += parameter(6)*hill - parameter(17)*y[i][w_6];

  //
  // --- 0.3*clv3 ---
  //
  // CLV3
  dydt[i][cm3_7] += parameter(13)*(0.3*parameter(14)-y[i][cm3_7]) + 0.3*parameter(18)*y[i][w_7];
    // CLV3 'effective' ligand
  c1Pow = std::pow(y[i][cm3_7],parameter(22));
  c2Pow = std::pow(y[i][cm3_7],parameter(25));
  dydt[i][c3_7] += parameter(20)*c1Pow/(K1Pow+c1Pow)
    + parameter(23)*c2Pow*y[i][cm3_7]/(K2Pow+c2Pow) - y[i][c3_7]
    + parameter(1)*y[i][cc13_7]-parameter(0)*y[i][c3_7]*y[i][c1_7]
    + parameter(4)*y[i][cc3_7] - parameter(3)*y[i][c3_7]*y[i][c_7];
  // CRN
  dydt[i][c_7] += parameter(11)*(parameter(12)-y[i][c_7])
    + parameter(4)*y[i][cc3_7]-parameter(3)*y[i][c3_7]*y[i][c_7];
  // CLV1
  dydt[i][c1_7] += parameter(9)*(parameter(10)-y[i][c1_7])
    + parameter(1)*y[i][cc13_7]-parameter(0)*y[i][c3_7]*y[i][c1_7];
  // CLV3-CRN
  dydt[i][cc3_7] += parameter(3)*y[i][c3_7]*y[i][c_7]-(parameter(4)+parameter(11))*y[i][cc3_7];
  // CLV3-CLV1
  dydt[i][cc13_7] += parameter(0)*y[i][c3_7]*y[i][c1_7]-(parameter(1)+parameter(9))*y[i][cc13_7];
  // X
  dydt[i][x_7] += parameter(15)*(parameter(16)-y[i][x_7])
    + parameter(2)*y[i][cc13_7] + parameter(5)*y[i][cc3_7];
  // WUS
  hill = Kpow_/( Kpow_+std::pow(y[i][x_7],parameter(8)) );
  dydt[i][w_7] += parameter(6)*hill - parameter(17)*y[i][w_7];

  //
  // --- 3*clv3 ---
  //
  // CLV3
  dydt[i][cm3_8] += parameter(13)*(3.0*parameter(14)-y[i][cm3_8]) + 3.0*parameter(18)*y[i][w_8];
  // CLV3 'effective' ligand
  c1Pow = std::pow(y[i][cm3_8],parameter(22));
  c2Pow = std::pow(y[i][cm3_8],parameter(25));
  dydt[i][c3_8] += parameter(20)*c1Pow/(K1Pow+c1Pow)
    + parameter(23)*c2Pow*y[i][cm3_8]/(K2Pow+c2Pow) - y[i][c3_8]
    + parameter(1)*y[i][cc13_8]-parameter(0)*y[i][c3_8]*y[i][c1_8]
    + parameter(4)*y[i][cc3_8] - parameter(3)*y[i][c3_8]*y[i][c_8];
  // CRN
  dydt[i][c_8] += parameter(11)*(parameter(12)-y[i][c_8])
    + parameter(4)*y[i][cc3_8]-parameter(3)*y[i][c3_8]*y[i][c_8];
  // CLV1
  dydt[i][c1_8] += parameter(9)*(parameter(10)-y[i][c1_8])
    + parameter(1)*y[i][cc13_8]-parameter(0)*y[i][c3_8]*y[i][c1_8];
  // CLV3-CRN
  dydt[i][cc3_8] += parameter(3)*y[i][c3_8]*y[i][c_8]-(parameter(4)+parameter(11))*y[i][cc3_8];
  // CLV3-CLV1
  dydt[i][cc13_8] += parameter(0)*y[i][c3_8]*y[i][c1_8]-(parameter(1)+parameter(9))*y[i][cc13_8];
  // X
  dydt[i][x_8] += parameter(15)*(parameter(16)-y[i][x_8])
    + parameter(2)*y[i][cc13_8] + parameter(5)*y[i][cc3_8];
  // WUS
  hill = Kpow_/( Kpow_+std::pow(y[i][x_8],parameter(8)) );
  dydt[i][w_8] += parameter(6)*hill - parameter(17)*y[i][w_8];

  //
  // --- 100*clv3 ---
  //
  // CLV3
  dydt[i][cm3_9] += parameter(13)*(100.0*parameter(14)-y[i][cm3_9]) + 100.0*parameter(18)*y[i][w_9];
  // CLV3 'effective' ligand
  c1Pow = std::pow(y[i][cm3_9],parameter(22));
  c2Pow = std::pow(y[i][cm3_9],parameter(25));
  dydt[i][c3_9] += parameter(20)*c1Pow/(K1Pow+c1Pow)
    + parameter(23)*c2Pow*y[i][cm3_9]/(K2Pow+c2Pow) - y[i][c3_9]
    + parameter(1)*y[i][cc13_9]-parameter(0)*y[i][c3_9]*y[i][c1_9]
    + parameter(4)*y[i][cc3_9] - parameter(3)*y[i][c3_9]*y[i][c_9];
  // CRN
  dydt[i][c_9] += parameter(11)*(parameter(12)-y[i][c_9])
    + parameter(4)*y[i][cc3_9]-parameter(3)*y[i][c3_9]*y[i][c_9];
  // CLV1
  dydt[i][c1_9] += parameter(9)*(parameter(10)-y[i][c1_9])
    + parameter(1)*y[i][cc13_9]-parameter(0)*y[i][c3_9]*y[i][c1_9];
  // CLV3-CRN
  dydt[i][cc3_9] += parameter(3)*y[i][c3_9]*y[i][c_9]-(parameter(4)+parameter(11))*y[i][cc3_9];
  // CLV3-CLV1
  dydt[i][cc13_9] += parameter(0)*y[i][c3_9]*y[i][c1_9]-(parameter(1)+parameter(9))*y[i][cc13_9];
  // X
  dydt[i][x_9] += parameter(15)*(parameter(16)-y[i][x_9])
    + parameter(2)*y[i][cc13_9] + parameter(5)*y[i][cc3_9];
  // WUS
  hill = Kpow_/( Kpow_+std::pow(y[i][x_9],parameter(8)) );
  dydt[i][w_9] += parameter(6)*hill - parameter(17)*y[i][w_9];

}
