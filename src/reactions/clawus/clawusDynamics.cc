#include "clawusDynamics.h"

namespace clawus
{
	ClawusDynamics::ClawusDynamics(std::vector<double> &paraValue, std::vector< std::vector<size_t> > &indValue)
	: firstRun_(true)
	{
		if (paraValue.size() != 36) {
			std::cerr << "clawus::ClawusDynamics::ClawusDynamics: Uses thirtysix parameters:\n"
			<< "k1, "    // 0
			<< "k2, "    // 1
			<< "k3, "    // 2
			<< "k4, "    // 3
			<< "k5, "    // 4
			<< "k6, "    // 5
			<< "k7, "    // 6
			<< "k8, "    // 7
			<< "k9, "    // 8
			<< "k10, "   // 9
			<< "k11, "   // 10
			<< "k12, "   // 11
			<< "k13, "   // 12
			<< "k14, "   // 13
			<< "k15, "   // 14
			<< "p1, "    // 15
			<< "d1, "    // 16
			<< "p2, "    // 17
			<< "d2, "    // 18
			<< "p3, "    // 19
			<< "d3, "    // 20
			<< "p4, "    // 21
			<< "d4, "    // 22
			<< "p5, "    // 23
			<< "d5, "    // 24
			<< "p6, "    // 25
			<< "d6, "    // 26 
			<< "p7, "    // 27
			<< "d7, "    // 28
			<< "ICLV1 ," // 29
			<< "ICLV2 ," // 30
			<< "ICLV3 ," // 31
			<< "IX, "    // 32
			<< "IWUS, "  // 33
			<< "IP1, "   // 34
			<< "IP2.\n"; // 35
			exit(EXIT_FAILURE);
		}

		for (size_t i = 0; i < indValue.size(); ++i) {
			if (indValue[i].size() != 19) {
				std::cerr << "clawus::ClawusDynamics::ClawusDynamics\n"
				<< "Nineteen variables are needed: "
				<< "CLV1, " // 0
				<< "CLV2, " // 1
				<< "CLV3, " // 2
				<< "C13, "  // 3
				<< "C23, "  // 4
				<< "C123, " // 5
				<< "X, "    // 6
				<< "WUS, "  // 7
				<< "P1, "   // 8
				<< "P2, "   // 9
				<< "CP13, " // 10
				<< "CP12, " // 11
				<< "B1, "   // 12 (C23 -> X)
				<< "B2, "   // 13 (Production of CLV1)
 				<< "B3, "   // 14 (Production of CLV2)
				<< "B4, "   // 15 (C13 + C23 <-> C123)
				<< "B5, "   // 16 (C13 -> X)
				<< "B6, "   // 17 (P1 Pathway)
				<< "B7.\n"; // 18 (P2 Pathway)
				exit(EXIT_FAILURE);
			}
		}

		setId("clawus::clawusDynamics");
		setParameter(paraValue);  
		setVariableIndex(indValue);
		
		std::vector<std::string> tmp(numParameter());
		tmp.resize(numParameter());
		tmp[0] = "k1";
		tmp[1] = "k2";
		tmp[2] = "k3";
		tmp[3] = "k4";
		tmp[4] = "k5";
		tmp[5] = "k6";
		tmp[6] = "k7";
		tmp[7] = "k8";
		tmp[8] = "k9";
		tmp[9] = "k10";
		tmp[10] = "k11";
		tmp[11] = "k12";
		tmp[12] = "k13";
		tmp[13] = "k14";
		tmp[14] = "k15";
		tmp[15] = "p1";
		tmp[16] = "d1";
		tmp[17] = "p2";
		tmp[18] = "d2";
		tmp[19] = "p3";
		tmp[20] = "d3";
		tmp[21] = "p4";
		tmp[22] = "d4";
		tmp[23] = "p5";
		tmp[24] = "d5";
		tmp[25] = "p6";
		tmp[26] = "d6";
		tmp[27] = "p7";
		tmp[28] = "d8";
		tmp[29] = "ICLV1";
		tmp[30] = "ICLV2";
		tmp[31] = "ICLV3";
		tmp[32] = "IX";
		tmp[33] = "IWUS";
		tmp[34] = "IP1";
		tmp[35] = "IP2";
		setParameterId(tmp);
	}
		
	void ClawusDynamics::derivs(Compartment &compartment, size_t species, std::vector< std::vector<double> > &y, std::vector< std::vector<double> > &dydt)
	{
		size_t i = compartment.index();

		// Parameter values.
		const double k1 = parameter(0);
		const double k2 = parameter(1);
		const double k3 = parameter(2);
		const double k4 = parameter(3);
		const double k5 = parameter(4);
		const double k6 = parameter(5);
		const double k7 = parameter(6);
		const double k8 = parameter(7);
		const double k9 = parameter(8);
		const double k10 = parameter(9);
		const double k11 = parameter(10);
		const double k12 = parameter(11);
		const double k13 = parameter(12);
		const double k14 = parameter(13);
		const double k15 = parameter(14);
		const double p1 = parameter(15);
		const double d1 = parameter(16);
		const double p2 = parameter(17);
		const double d2 = parameter(18);
		const double p3 = parameter(19);
		const double d3 = parameter(20);
		const double p4 = parameter(21);
		const double d4 = parameter(22);
		const double p5 = parameter(23);
		const double d5 = parameter(24);
		const double p6 = parameter(25);
		const double d6 = parameter(26);
		const double p7 = parameter(27);
		const double d7 = parameter(28);
		const double ICLV1 = parameter(29);
		const double ICLV2 = parameter(30);
		const double ICLV3 = parameter(31);
		const double IX = parameter(32);
		const double IWUS = parameter(33);
		const double IP1 = parameter(34);
		const double IP2 = parameter(35);

		if (firstRun_) {
			for (size_t level = 0; level < numVariableIndexLevel(); ++level) {

				const size_t iCLV1 = variableIndex(level, 0);
				const size_t iCLV2 = variableIndex(level, 1);
				const size_t iCLV3 = variableIndex(level, 2);
				// const size_t iC13 = variableIndex(level, 3);
				// const size_t iC23 = variableIndex(level, 4);
				// const size_t iC123 = variableIndex(level, 5);
				const size_t iX = variableIndex(level, 6);
				const size_t iWUS = variableIndex(level, 7);
				const size_t iP1 = variableIndex(level, 8);
				const size_t iP2 = variableIndex(level, 9);
				// const size_t iCP13 = variableIndex(level, 10);
				// const size_t iCP23 = variableIndex(level, 11);
				// const size_t iB1 = variableIndex(level, 12);
				// const size_t iB2 = variableIndex(level, 13);
				// const size_t iB3 = variableIndex(level, 14);
				// const size_t iB4 = variableIndex(level, 15);
				// const size_t iB5 = variableIndex(level, 16);
				// const size_t iB6 = variableIndex(level, 17);
				// const size_t iB7 = variableIndex(level, 18);
				
				// Variable values.
				double &CLV1 = y[i][iCLV1];
				double &CLV2 = y[i][iCLV2];
				double &CLV3 = y[i][iCLV3];
				// double &C13 = y[i][iC13];
				// double &C23 = y[i][iC23];
				// double &C123 = y[i][iC123];
				double &X = y[i][iX];
				double &WUS = y[i][iWUS];
				double &P1 = y[i][iP1];
				double &P2 = y[i][iP2];
				// double &CP13 = y[i][iCP13];
				// double &CP23 = y[i][iCP23];
				// double &B1 = y[i][iB1];
				// double &B2 = y[i][iB2];
				// double &B3 = y[i][iB3];
				// double &B4 = y[i][iB4];
				// double &B5 = y[i][iB5];
				// double &B6 = y[i][iB6];
				// double &B7 = y[i][iB7];

				// Set initial concentrations from parameter values.
				CLV1 = ICLV1;
				CLV2 = ICLV2;
				CLV3 = ICLV3;
				X = IX;
				WUS = IWUS;
				P1 = IP1;
				P2 = IP2;
			}

			firstRun_ = false;
		}

		for (size_t level = 0; level < numVariableIndexLevel(); ++level) {
			// Variable indices.
 			const size_t iCLV1 = variableIndex(level, 0);
			const size_t iCLV2 = variableIndex(level, 1);
			const size_t iCLV3 = variableIndex(level, 2);
			const size_t iC13 = variableIndex(level, 3);
			const size_t iC23 = variableIndex(level, 4);
			const size_t iC123 = variableIndex(level, 5);
			const size_t iX = variableIndex(level, 6);
			const size_t iWUS = variableIndex(level, 7);
			const size_t iP1 = variableIndex(level, 8);
			const size_t iP2 = variableIndex(level, 9);
			const size_t iCP13 = variableIndex(level, 10);
			const size_t iCP23 = variableIndex(level, 11);
			const size_t iB1 = variableIndex(level, 12);
			const size_t iB2 = variableIndex(level, 13);
			const size_t iB3 = variableIndex(level, 14);
			const size_t iB4 = variableIndex(level, 15);
			const size_t iB5 = variableIndex(level, 16);
			const size_t iB6 = variableIndex(level, 17);
			const size_t iB7 = variableIndex(level, 18);

			// Variable values.
			const double &CLV1 = y[i][iCLV1];
			const double &CLV2 = y[i][iCLV2];
			const double &CLV3 = y[i][iCLV3];
			const double &C13 = y[i][iC13];
			const double &C23 = y[i][iC23];
			const double &C123 = y[i][iC123];
			const double &X = y[i][iX];
			const double &WUS = y[i][iWUS];
			const double &P1 = y[i][iP1];
			const double &P2 = y[i][iP2];
			const double &CP13 = y[i][iCP13];
			const double &CP23 = y[i][iCP23];
			const double &B1 = y[i][iB1];
			const double &B2 = y[i][iB2];
			const double &B3 = y[i][iB3];
			const double &B4 = y[i][iB4];
			const double &B5 = y[i][iB5];
			const double &B6 = y[i][iB6];
			const double &B7 = y[i][iB7];

			// Variable derivatives.
			double &dCLV1dt = dydt[i][iCLV1];
			double &dCLV2dt = dydt[i][iCLV2];
			double &dCLV3dt = dydt[i][iCLV3];
			double &dC13dt = dydt[i][iC13];
			double &dC23dt = dydt[i][iC23];
			double &dC123dt = dydt[i][iC123];
			double &dXdt = dydt[i][iX];
			double &dWUSdt = dydt[i][iWUS];
			double &dP1dt = dydt[i][iP1];
			double &dP2dt = dydt[i][iP2];
			double &dCP13dt = dydt[i][iCP13];
			double &dCP23dt = dydt[i][iCP23];

			// Ordinary differential equations.

			// CLV1 Pathway (B5).
			dCLV1dt += -k1 * CLV1 * CLV3 + k2 * C13;
			dCLV3dt += -k1 * CLV1 * CLV3 + k2 * C13;
			dC13dt += k1 * CLV1 * CLV3 - k2 * C13;
			dXdt += B5 * k3 * C13;

			// CLV2 Pathway (B1).
			dCLV2dt += -k4 * CLV2 * CLV3 + k5 * C23;
			dCLV3dt += -k4 * CLV2 * CLV3 + k5 * C23;
			dC23dt += k4 * CLV2 * CLV3 - k5 * C23;
			dXdt += B1 * k6 * C23;

			// X -> WUS Pathway.
			dWUSdt += -k7 * X * WUS;

			// CLV1, CLV2, and CLV3 production and degradation (B2, B3).
			dCLV1dt += B2 * p1 - d1 * CLV1;
			dCLV2dt += B3 * p2 - d2 * CLV2;
			dCLV3dt += p3 - d3 * CLV3;

			// X and WUS production and degradation.
			dXdt += p4 - d4 * X;
			dWUSdt += p5 - d5 * WUS;

			// C13 + C23 interference (B4).
			dC13dt += B4 * (-k8 * C13 * C23 + k9 * C123);
			dC23dt += B4 * (-k8 * C13 * C23 + k9 * C123);
			dC123dt += B4 * (k8 * C13 * C23 - k9 * C123);
			
			// First alternative pathway.
			dP1dt += -k10 * CLV3 * P1 + k11 * CP13;
			dCLV3dt += -k10 * CLV3 * P1 + k11 * CP13;
			dCP13dt += k10 * CLV3 * P1 - k11 * CP13;
			dXdt += k12 * CP13;

			// Second alternative pathway.
 			dP2dt += -k13 * CLV3 * P2 + k14 * CP23;
			dCLV3dt += -k13 * CLV3 * P2 + k14 * CP23;
			dCP23dt += k13 * CLV3 * P2 - k14 * CP23;
			dXdt += -k15 * X * CP23;

			// P1 and P2 production and degradation.
			dP1dt += B6 * p6 - d6 * P1;
			dP2dt += B7 * p7 - d7 * P2;
		}
	}
}





