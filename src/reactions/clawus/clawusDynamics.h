#ifndef CLAWUS_CLAWUSDYNAMICS_H
#define CLAWUS_CLAWUSDYNAMICS_H

#include "../baseReaction.h"

namespace clawus
{
	class ClawusDynamics : public BaseReaction
	{
	public:
		ClawusDynamics(std::vector<double> &paraValue, std::vector< std::vector<size_t> > &indValue);
		
		void derivs(Compartment &compartment,size_t species, std::vector< std::vector<double> > &y, std::vector< std::vector<double> > &dydt);
	private:
		bool firstRun_;
	};
}

#endif /* CLAWUS_CLAWUSDYNAMICS_H */
