//
// Filename     : adhocReaction.h
// Description  : Classes describing some adhoc updates
// Author(s)    : Henrik Jonsson (henrik@thep.lu.se)
// Created      : October 2008
// Revision     : $Id:$
//
#ifndef ADHOCREACTION_H
#define ADHOCREACTION_H

#include "baseReaction.h"

///
/// @brief SumCompartmentVar sums up the value for a variable in a specified set of compartments
///
/// 
/// This method sums up the variable value in a specified set of compartments, to be
/// able to compare with coarse grained data or the total amount. It saves the sum in all
/// compartments included in the set. It has no derivative contribution, but
/// does the summing in an update.
///
/// @f[ y_{ij} = p_{0} \sum_k y_{kl} @f]
///
/// where the compartment i is included among the set of k and $f[y_{kl}$f] is the concentration 
/// (at index l) to be summed. $f[p_{0}$f] is a conversion factor that can be used to change 
/// units.
///
/// In a model file the reaction is defined as:
///
/// @verbatim
/// SumCompartmentVar 1 K+1 N_k1 [N_k2] ...
/// p_0
/// conc_index save_index
/// compartmentStart_index compartmentEnd_index
/// [compartmentStart_index compartmentEnd_index]
/// ...
/// @endverbatim
///
///
class SumCompartmentVar : public BaseReaction {
  
 public:
  
  ///
  /// @brief Main constructor
  ///
  /// This is the main constructor which checks and sets the parameters and
  /// variable indices that defines the reaction.
  ///
  /// @param paraValue vector with parameters
  ///
  /// @param indValue vector of vectors with variable indices
  ///
  /// @see BaseReaction::createReaction(std::vector<double> &paraValue,...)
  ///
  SumCompartmentVar(std::vector<double> &paraValue, 
		     std::vector< std::vector<size_t> > &indValue );
  
  ///
  /// @brief Derivative function for this reaction class
  ///
  /// @see BaseReaction::derivs(Compartment &compartment,size_t species,...)
  ///
  void derivs(Compartment &compartment,size_t specie,DataMatrix &y,DataMatrix &dydt);	
  
  ///
  /// @brief Update function for this reaction class
  ///
  /// @see BaseReaction::update(double h, double t, ...)
  ///
  void update(double h, double t, DataMatrix &y);
};

///
/// @brief SumCompartmentMass sums up the mass in a specified set of compartments
///
/// 
/// This method sums up the mass in a specified set of compartments, to be
/// able to compare with coarse grained data. It saves the summed mass in all
/// compartments included in the set. It has no derivative contribution, but
/// does the summing in an update.
///
/// @f[ y_{ij} = p_{0} \sum_k V_{k} y_{kl} @f]
///
/// where the compartment i is included among the set of k $f[V_{k}$f] is the
/// volume and $f[y_{kl}$f] is the concentration (at index l) to be
/// summed. $f[p_{0}$f] is a conversion factor usually set to get correct
/// units.
///
/// In a model file the reaction is defined as:
///
/// @verbatim
/// SumCompartmentMass 1 K+1 N_k1 [N_k2] ...
/// p_0
/// V_index conc_index save_index
/// compartmentStart_index compartmentEnd_index
/// [compartmentStart_index compartmentEnd_index]
/// ...
/// @endverbatim
///
///
class SumCompartmentMass : public BaseReaction {
  
 public:
  
  ///
  /// @brief Main constructor
  ///
  /// This is the main constructor which checks and sets the parameters and
  /// variable indices that defines the reaction.
  ///
  /// @param paraValue vector with parameters
  ///
  /// @param indValue vector of vectors with variable indices
  ///
  /// @see BaseReaction::createReaction(std::vector<double> &paraValue,...)
  ///
  SumCompartmentMass(std::vector<double> &paraValue, 
		     std::vector< std::vector<size_t> > &indValue );
  
  ///
  /// @brief Derivative function for this reaction class
  ///
  /// @see BaseReaction::derivs(Compartment &compartment,size_t species,...)
  ///
  void derivs(Compartment &compartment,size_t specie,DataMatrix &y,DataMatrix &dydt);	
  
  ///
  /// @brief Update function for this reaction class
  ///
  /// @see BaseReaction::update(double h, double t, ...)
  ///
  void update(double h, double t, DataMatrix &y);
};

///
/// @brief Includes diffusion divided among neighbors + ino background
///
/// This method implements diffusion out of a compartment into its
/// neighbors and to the background. The diffusion is divided among
/// neighbors and background to estimate the crossing area. 
/// A caveat is that this approximated area does not have to 
/// be the same from one cell to another as from the other to 
/// the first.
///
/// @f[ \frac{dy_{ij}}{dt} = - \frac{p_{0}}{N+p_{1}} \sum_n y_{ij}  @f]
///
/// where the diffusion is for all compartments i for molecule j.
/// N is the number of neighbors and the sum is over the neighbors.
/// Each neighbor recieves the same amount that is diffused from this cell
/// while there is an additional leakage to the background set by p1.
///
/// In a model file the reaction is defined as:
///
/// @verbatim
/// diffusionDivided 2 0
/// p_0 p_1
/// @endverbatim
///
class DiffusionDivided : public BaseReaction {
  
 public:
  
  ///
  /// @brief Main constructor
  ///
  /// This is the main constructor which checks and sets the parameters and
  /// variable indices that defines the reaction.
  ///
  /// @param paraValue vector with parameters
  ///
  /// @param indValue vector of vectors with variable indices
  ///
  /// @see BaseReaction::createReaction(std::vector<double> &paraValue,...)
  ///
  DiffusionDivided(std::vector<double> &paraValue, 
		   std::vector< std::vector<size_t> > &indValue );
  
  ///
  /// @brief Derivative function for this reaction class
  ///
  /// @see BaseReaction::derivs(Compartment &compartment,size_t species,...)
  ///
  void derivs(Compartment &compartment,size_t specie,DataMatrix &y,DataMatrix &dydt);	

  ///
  /// @brief Propensity calculation (probability to happen in [t,t+dt]) for this reaction class
  ///
  /// @see BaseReaction::propensity(Compartment &compartment,size_t species,DataMatrix &y)
  ///
  double propensity(Compartment &compartment,size_t species,DataMatrix &y);

  ///
  /// @brief Discrete update for this reaction used in stochastic simulations
  ///
  /// @see BaseReaction::discreteUpdate(Compartment &compartment,size_t species,DataMatrix &y)
  ///
  void discreteUpdate(Compartment &compartment,size_t species,DataMatrix &y);
};

///
/// @brief Adds the variables in the list given plus a constant
///
/// Adds a list of variables and saves it in the specie value.
/// This is done in the update function and nothing is done in the 
/// derivs function.
/// In a model file the reaction is defined as:
///
/// @verbatim
/// add 1 1 N
/// const
/// X_1 ... X_N
/// @endverbatim
///
class Add : public BaseReaction {
  
 public:
  
  ///
  /// @brief Main constructor
  ///
  /// This is the main constructor which checks and sets the parameters and
  /// variable indices that defines the reaction.
  ///
  /// @param paraValue vector with parameters
  ///
  /// @param indValue vector of vectors with variable indices
  ///
  /// @see BaseReaction::createReaction(std::vector<double> &paraValue,...)
  Add(std::vector<double> &paraValue, 
      std::vector< std::vector<size_t> > &indValue );
  
  ///
  /// @brief This class does not use derivatives for updates.
  ///
  /// @see BaseReaction::derivs(Compartment &compartment,size_t species,...)
  ///
  void derivs(Compartment &compartment,size_t specie,DataMatrix &y,DataMatrix &dydt);

  ///
  /// @brief Update function for this reaction class
  ///
  /// @see BaseReaction::update(double h, double t, ...)
  ///
  void update(double h, double t, DataMatrix &y);
};

///
/// @brief Sets a specific value of a variable between derivative updates
/// (e.g. to generate constant boundary condition)
///
/// Sets a value (given as the sole parameter to this reaction) to a specific species
/// This is done in the update function and nothing is done in the 
/// derivs function. To be able to restrict which cells/compartments to set the value
/// , i.i. to generate boundary conditions, a variable index can be given where a flag can be
/// set such that 1 means the value is set in this compartment, and 0 menas it is not.
///
/// In a model file the reaction is defined as:
///
/// @verbatim
/// setRestricted 1 2 1 1
/// value
/// i_toset
/// i_flag
/// @endverbatim
///
/// where value is the value set, itoset is the index of the species that should be set and
/// iflag is the index of the variable marking if the value is set (y[i][iflag]=1) or not
/// (y[i][iflag]=0).
///
class SetRestricted : public BaseReaction {
  
 public:
  
  ///
  /// @brief Main constructor
  ///
  /// This is the main constructor which checks and sets the parameters and
  /// variable indices that defines the reaction.
  ///
  /// @param paraValue vector with parameters
  ///
  /// @param indValue vector of vectors with variable indices
  ///
  /// @see BaseReaction::createReaction(std::vector<double> &paraValue,...)
  SetRestricted(std::vector<double> &paraValue, 
		std::vector< std::vector<size_t> > &indValue );
  
  ///
  /// @brief This class does not use derivatives for updates.
  ///
  /// @see BaseReaction::derivs(Compartment &compartment,size_t species,...)
  ///
  void derivs(Compartment &compartment,size_t specie,DataMatrix &y,DataMatrix &dydt);
  
  ///
  /// @brief Update function for this reaction class
  ///
  /// @see BaseReaction::update(double h, double t, ...)
  ///
  void update(double h, double t, DataMatrix &y);
};

///
/// @brief Turns off one and turns on compartment markers at thereshold
///
/// Adds a list of variables and saves it in the specie value.
/// This is done in the update function and nothing is done in the 
/// derivs function.
/// In a model file the reaction is defined as:
///
/// @verbatim
/// SwitchCompartmentThreshold 1 1 N
/// threshold negativeFlag
/// removeIndex addIndex
/// @endverbatim
///
class SwitchCompartmentThreshold : public BaseReaction {
  
 public:
  ///
  /// @brief Main constructor
  ///
  /// This is the main constructor which checks and sets the parameters and
  /// variable indices that defines the reaction.
  ///
  /// @param paraValue vector with parameters
  ///
  /// @param indValue vector of vectors with variable indices
  ///
  /// @see BaseReaction::createReaction(std::vector<double> &paraValue,...)
  SwitchCompartmentThreshold(std::vector<double> &paraValue, 
			     std::vector< std::vector<size_t> > &indValue );
		
  ///
  /// @brief This class does not use derivatives for updates.
  ///
  /// @see BaseReaction::derivs(Compartment &compartment,size_t species,...)
  ///
  void derivs(Compartment &compartment,size_t specie,DataMatrix &y,DataMatrix &dydt);

  ///
  /// @brief Update function for this reaction class
  ///
  /// @see BaseReaction::update(double h, double t, ...)
  ///
		void update(double h, double t, DataMatrix &y);
};


///
/// @brief Turns off one and turns on compartment markers at thereshold
///
/// Above a certain threshold of an upstream variable, it makes donwstream variables switch from 0 to 1.
/// This switch can be reversed, switching from 1 to 0 when the upstream variable is below such treshold.
/// This is done in the update function and nothing is done in the 
/// derivs function. 
/// In a model file the reaction is defined as:
///
/// @verbatim
/// ThresholdSwitch 2 2 1 N
/// threshold 
/// switchtype
/// Indexvariable
/// list of variable indices downstream the switch
/// @endverbatim
///
/// The switchtype parameter takes the values 0 and 1 for defining the reversible and irreversible switch, respectively.  

class ThresholdSwitch : public BaseReaction {
  
 public:
  ///
  /// @brief Main constructor
  ///
  /// This is the main constructor which checks and sets the parameters and
  /// variable indices that defines the reaction.
  ///
  /// @param paraValue vector with parameters
  ///
  /// @param indValue vector of vectors with variable indices
  ///
  /// @see BaseReaction::createReaction(std::vector<double> &paraValue,...)
  ThresholdSwitch(std::vector<double> &paraValue, 
			     std::vector< std::vector<size_t> > &indValue );
		
  ///
  /// @brief This class does not use derivatives for updates.
  ///
  /// @see BaseReaction::derivs(Compartment &compartment,size_t species,...)
  ///
  void derivs(Compartment &compartment,size_t specie,DataMatrix &y,DataMatrix &dydt);
  void derivsWithAbs(Compartment &compartment,size_t specie,DataMatrix &y,DataMatrix &dydt,DataMatrix &sdydt);
  ///
  /// @brief Update function for this reaction class
  ///
  /// @see BaseReaction::update(double h, double t, ...)
  ///
	void update(double h, double t, DataMatrix &y);
};

///
/// @brief A reaction to double all production terms, e.g. to simulate
/// double DNA in marked cells
///
/// This reaction doubles the derivative contribution when applied.
/// If defined after all transcriptional processes but before degradation it
/// can resemble several copies of DNA.
///
/// @f[ \frac{dy_{ij}}{dt} -> p_{0}* \frac{dy_{ij}}{dt} @f]
///
/// if
///
/// @f[ y_{ik} != 0 @f]
///
/// where p0 is the factor multiplied, index j marks the variable to change and k
/// the variable (flag) checked for if the update will be done.
/// In a model file the reaction is defined as:
///
/// @verbatim
/// factorOnDerivsRestricted 1 1 1
/// p_factor
/// y_flag
/// @endverbatim
///
/// @note Need to know what you are doing (direct multiplication of derivs...).
///
class FactorOnDerivsRestricted : public BaseReaction {
  
 public:
  
  ///
  /// @brief Main constructor
  ///
  /// This is the main constructor which checks and sets the parameters and
  /// variable indices that defines the reaction.
  ///
  /// @param paraValue vector with parameters
  ///
  /// @param indValue vector of vectors with variable indices
  ///
  /// @see BaseReaction::createReaction(std::vector<double> &paraValue,...)
  ///
  FactorOnDerivsRestricted(std::vector<double> &paraValue, 
			   std::vector< std::vector<size_t> > &indValue );
  ///
  /// @brief Derivative function for this reaction class
  ///
  /// @see BaseReaction::derivs(Compartment &compartment,size_t species,...)
  ///
  void derivs(Compartment &compartment,size_t specie,DataMatrix &y,DataMatrix &dydt);	
  ///
  /// @brief Derivative function for this reaction class calculating the absolute value for noise solvers
  ///
  /// @see BaseReaction::derivsWithAbs(Compartment &compartment,size_t species,...)
  ///
  void derivsWithAbs(Compartment &compartment,size_t species,DataMatrix &y,DataMatrix &dydt,DataMatrix &sdydt);
  //
  // @brief Propensity calculation (probability to happen in [t,t+dt]) for this reaction class
  //
  // @see BaseReaction::propensity(Compartment &compartment,size_t species,DataMatrix &y)
  //
  // double propensity(Compartment &compartment,size_t species,DataMatrix &y);
  // HJ: not obvious how this should be implemented for stochastic solvers...
};

///
/// @brief A reaction to multiply all production terms with a factor, e.g. to simulate
/// multiple copies of DNA in cells 
///
/// This reaction multiplies the derivative contribution whit a factor, f, when a variable is
/// above a threshold.
/// If threshold is on size variable and the reaction is defined after all transcriptional
/// processes but before degradation it can simulate several copies of DNA.
///
/// @f[ \frac{dy_{ij}}{dt} -> f \frac{dy_{ij}}{dt} @f]
///
/// if
///
/// @f[ y_{ik} > p_{0} @f]
///
/// and
/// 
/// @f[ f > (int) \frac{y_{ik}}{p_{0}} @f]
///
/// where p0 is the threshold determining the factor, index j marks the variable updated and k
/// the variable checked for if (what multiple of) threshold is reached (supplied).
/// In a model file the reaction is defined as:
///
/// @verbatim
/// factorOnDerivsMultiple 1 1 1
/// p_threshold
/// y_threshold
/// @endverbatim
///
/// @note Need to know what you are doing (direct multiplication of derivs...).
///
class FactorOnDerivsMultiple : public BaseReaction {
  
 public:
  
  ///
  /// @brief Main constructor
  ///
  /// This is the main constructor which checks and sets the parameters and
  /// variable indices that defines the reaction.
  ///
  /// @param paraValue vector with parameters
  ///
  /// @param indValue vector of vectors with variable indices
  ///
  /// @see BaseReaction::createReaction(std::vector<double> &paraValue,...)
  ///
  FactorOnDerivsMultiple(std::vector<double> &paraValue, 
			 std::vector< std::vector<size_t> > &indValue );
  ///
  /// @brief Derivative function for this reaction class
  ///
  /// @see BaseReaction::derivs(Compartment &compartment,size_t species,...)
  ///
  void derivs(Compartment &compartment,size_t specie,DataMatrix &y,DataMatrix &dydt);	
  ///
  /// @brief Derivative function for this reaction class calculating the absolute value for noise solvers
  ///
  /// @see BaseReaction::derivsWithAbs(Compartment &compartment,size_t species,...)
  ///
  void derivsWithAbs(Compartment &compartment,size_t species,DataMatrix &y,DataMatrix &dydt,DataMatrix &sdydt);
  //
  // @brief Propensity calculation (probability to happen in [t,t+dt]) for this reaction class
  //
  // @see BaseReaction::propensity(Compartment &compartment,size_t species,DataMatrix &y)
  //
  //double propensity(Compartment &compartment,size_t species,DataMatrix &y);
  // HJ: not obvious how this should be implemented for stochastic solvers...

};

#endif
