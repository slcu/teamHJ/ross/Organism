//
// Filename     : parameterUpdate.h
// Description  : Includes reactions for updating parameter values 
// Author(s)    : Henrik Jonsson (henrik@thep.lu.se)
// Created      : April 2012
// Revision     : $Id:$
//
#ifndef PARAMETERUPDATE_H
#define PARAMETERUPDATE_H

#include"parameterUpdate/parameterExponential.h"
#include"parameterUpdate/parameterCopy.h"
#include"parameterUpdate/parameterStochastic.h"

#endif
