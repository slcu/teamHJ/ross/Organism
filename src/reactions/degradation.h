#ifndef DEGRADATION_H
#define DEGRADATION_H
//
// Filename     : degradation.h
// Description  : Classes describing degradation of molecules
// Author(s)    : Henrik Jonsson (henrik@thep.lu.se)
// Created      : October 2003
// Revision     : $Id: degradation.h 646 2015-08-23 18:35:06Z henrik $
//

#include "baseReaction.h"

///
/// @brief DegradationLinear applies a degradation rate proportional to a
/// number of user defined variables plus itself
///
/// The variable update is given by 
///
/// @f[ \frac{dc}{dt} = - k_d c \prod_i C_i @f]
///
/// where @f$ k_d @f$ is the degradation rate and @f$ c @f$ is the
/// concentration of the degraded molecule and the product is over the user
/// supplied variables (indices) @f$ C_i @f$.
///
/// In a model file the reaction is defined as
///
/// @verbatim
/// degradationLinear 1 1 N_i
/// k_c
/// index1 ... indexN_i
/// @endverbatim
///
/// Alternatively if no index supplied (degradation proportional to itself)
/// the first line can be replaced by 'degradationLinear 1 0'.
///
class DegradationLinear : public BaseReaction {
  
 public:
  
  ///
  /// @brief Main constructor
  ///
  /// This is the main constructor which sets the parameters and variable
  /// indices that defines the reaction.
  ///
  /// @param paraValue vector with parameters
  ///
  /// @param indValue vector of vectors with variable indices
  ///
  /// @see BaseReaction::createReaction(std::vector<double> &paraValue,...)
  ///
  DegradationLinear(std::vector<double> &paraValue, 
		    std::vector< std::vector<size_t> > &indValue );
  
  ///
  /// @brief Derivative function for this reaction class
  ///
  /// @see BaseReaction::derivs(Compartment &compartment,size_t species,...)
  ///
  void derivs(Compartment &compartment,size_t species,DataMatrix &y,DataMatrix &dydt);

  ///
  /// @brief Derivative function for this reaction class calculating the absolute value for noise solvers
  ///
  /// @see BaseReaction::derivsWithAbs(Compartment &compartment,size_t species,...)
  ///
  void derivsWithAbs(Compartment &compartment,size_t species,DataMatrix &y,DataMatrix &dydt,DataMatrix &sdydt);
};

///
/// @brief Implements a degradation proportional to its own concentration
///
/// The update to the variable is
///
/// @f[ \frac{dc}{dt} -= k_d c @f]
///
/// where @f$ k_d @f$ is the degradation rate.
///
/// In a model file the reaction is defined as
///
/// @verbatim
/// degradationOne 1 0
/// k_d
/// @endverbatim
///
class DegradationOne : public BaseReaction {
  
 public:
  
	///
	/// @brief Main constructor
	///
	/// This is the main constructor which sets the parameters and variable
	/// indices that defines the reaction.
	///
	/// @param paraValue vector with parameters
	///
	/// @param indValue vector of vectors with variable indices
	///
	/// @see BaseReaction::createReaction(std::vector<double> &paraValue,...)
	///
  DegradationOne(std::vector<double> &paraValue, 
								 std::vector< std::vector<size_t> > &indValue );
  
	///
	/// @brief Derivative function for this reaction class
	///
	/// @see BaseReaction::derivs(Compartment &compartment,size_t species,...)
	///
  void derivs(Compartment &compartment,size_t species,DataMatrix &y,DataMatrix &dydt);

  ///
  /// @brief Derivative function for this reaction class calculating the absolute value for noise solvers
  ///
  /// @see BaseReaction::derivsWithAbs(Compartment &compartment,size_t species,...)
  ///
  void derivsWithAbs(Compartment &compartment,size_t species,DataMatrix &y,DataMatrix &dydt,DataMatrix &sdydt);

	///
	/// @brief Jacobian function for this reaction class
	///
	/// @see BaseReaction::Jacobian(Compartment &compartment,size_t species,...)
	///
  size_t Jacobian(Compartment &compartment,size_t species,DataMatrix &y,JacobianMatrix &A);

  ///
  /// @brief Propensity calculation (probability to happen in [t,t+dt]) for this reaction class
  ///
  /// @see BaseReaction::propensity(Compartment &compartment,size_t species,DataMatrix &y)
  ///
  double propensity(Compartment &compartment,size_t species,DataMatrix &y);

  ///
  /// @brief Discrete update for this reaction used in stochastic simulations
  ///
  /// @see BaseReaction::discreteUpdate(Compartment &compartment,size_t species,DataMatrix &y)
  ///
  void discreteUpdate(Compartment &compartment,size_t species,DataMatrix &y);

  ///
  /// @brief Prints the reaction in Cambium format
  ///
  /// @see BaseReaction::printCambium()
  ///
  void printCambium( std::ostream &os, size_t varIndex ) const;
};

///
/// @brief Implements a degradation proportional to its own concentration and
/// an additional variable.
///
/// The update to the variable is
///
/// @f[ \frac{dc}{dt} -= k_d c C @f]
///
/// where @f$ k_d @f$ is the degradation rate, and C is the user supplied
/// variable.
///
/// In a model file the reaction is defined as
///
/// @verbatim
/// degradationTwo 1 1 1
/// k_d
/// index1
/// @endverbatim
///
class DegradationTwo : public BaseReaction {
  
 public:
  
	///
	/// @brief Main constructor
	///
	/// This is the main constructor which sets the parameters and variable
	/// indices that defines the reaction.
	///
	/// @param paraValue vector with parameters
	///
	/// @param indValue vector of vectors with variable indices
	///
	/// @see BaseReaction::createReaction(std::vector<double> &paraValue,...)
	///
  DegradationTwo(std::vector<double> &paraValue, 
								 std::vector< std::vector<size_t> > &indValue );
  
  ///
  /// @brief Derivative function for this reaction class
  ///
  /// @see BaseReaction::derivs(Compartment &compartment,size_t species,...)
  ///
  void derivs(Compartment &compartment,size_t species,DataMatrix &y,DataMatrix &dydt);

  ///
  /// @brief Derivative function for this reaction class calculating the absolute value for noise solvers
  ///
  /// @see BaseReaction::derivsWithAbs(Compartment &compartment,size_t species,...)
  ///
  void derivsWithAbs(Compartment &compartment,size_t species,DataMatrix &y,DataMatrix &dydt,DataMatrix &sdydt);

  ///
  /// @brief Jacobian function for this reaction class
  ///
  /// @see BaseReaction::Jacobian(Compartment &compartment,size_t species,...)
  ///
  size_t Jacobian(Compartment &compartment,size_t species,DataMatrix &y,JacobianMatrix &A);
  ///
  /// @brief Propensity calculation (probability to happen in [t,t+dt]) for this reaction class
  ///
  /// @see BaseReaction::propensity(Compartment &compartment,size_t species,DataMatrix &y)
  ///
  double propensity(Compartment &compartment,size_t species,DataMatrix &y);
  ///
  /// @brief Discrete update for this reaction used in stochastic simulations
  ///
  /// @see BaseReaction::discreteUpdate(Compartment &compartment,size_t species,DataMatrix &y)
  ///
  void discreteUpdate(Compartment &compartment,size_t species,DataMatrix &y);
  ///
  /// @brief Prints the reaction in Cambium format
  ///
  /// @see BaseReaction::printCambium()
  ///
  void printCambium( std::ostream &os, size_t varIndex ) const;
};

///
/// @brief Implements a degradation proportional to its own concentration and
/// two additional variables.
///
/// The update to the variable is
///
/// @f[ \frac{dc}{dt} -= k_d c C_1 C_2 @f]
///
/// where @f$ k_d @f$ is the degradation rate, and @f$ C_1,C_2 @f$ are the
/// user supplied variable.
///
/// In a model file the reaction is defined as
///
/// @verbatim
/// degradationThree 1 1 2
/// k_d
/// index1 index2
/// @endverbatim
///
class DegradationThree : public BaseReaction {
  
 public:
  
	///
	/// @brief Main constructor
	///
	/// This is the main constructor which sets the parameters and variable
	/// indices that defines the reaction.
	///
	/// @param paraValue vector with parameters
	///
	/// @param indValue vector of vectors with variable indices
	///
	/// @see BaseReaction::createReaction(std::vector<double> &paraValue,...)
	///
  DegradationThree(std::vector<double> &paraValue, 
									 std::vector< std::vector<size_t> > &indValue );
  
	///
	/// @brief Derivative function for this reaction class
	///
	/// @see BaseReaction::derivs(Compartment &compartment,size_t species,...)
	///
  void derivs(Compartment &compartment,size_t species,DataMatrix &y,DataMatrix &dydt);
};

///
/// @brief Implements a degradation proportional to its own concentration and
/// a Hill function of another alternatively a Hill function of its own concentration.
///
/// The update to the variable is
///
/// @f[ \frac{dc}{dt} -= k_{d} c \frac{C_{1}^{n}}{(C_{1}^{n}+K^{n})} @f]
///
/// or
///
/// @f[ \frac{dc}{dt} -= k_{d} c \frac{C_{1}^{n}}{(C_{1}^{n}+K^{n})} x_{r0} ... x_{rN} @f]
///
/// where @f$ k_d @f$ is the degradation rate, and @f$ C_1 @f$ is the
/// user supplied variable. @f$c@f$ is the concentration of the degraded variable,
/// and the @f$ x_{ri}@f$ 's are the restricing variables (if any are supplied)
/// Note that this reaction always multiplies the rate with the value of the
/// degraded variable itself!
/// 
///
/// In a model file the reaction is defined as
///
/// @verbatim
/// degradationHill 3 1 1
/// k_d K n
/// index1
/// @endverbatim
///
/// or, if the function should be multiplied with restricting variables, 
///
/// @verbatim
/// degradationHill 3 2 N 1
/// k_d K n
/// ri_0 ... ri_N
/// index1
/// @endverbatim
///
/// where the indices ri are the restricting variables
///
class DegradationHill : public BaseReaction {
  
 public:
  
  ///
  /// @brief Main constructor
  ///
  /// This is the main constructor which sets the parameters and variable
  /// indices that defines the reaction.
  ///
  /// @param paraValue vector with parameters
  ///
  /// @param indValue vector of vectors with variable indices
  ///
  /// @see BaseReaction::createReaction(std::vector<double> &paraValue,...)
  ///
  DegradationHill(std::vector<double> &paraValue, 
		  std::vector< std::vector<size_t> > &indValue );
  
  ///
  /// @brief Derivative function for this reaction class
  ///
  /// @see BaseReaction::derivs(Compartment &compartment,size_t species,...)
  ///
  void derivs(Compartment &compartment,size_t species,DataMatrix &y,DataMatrix &dydt);
};

///
/// @brief Implements a degradation proportional to its own concentration and
/// a (decreasing) Hill function of another.
///
/// The update to the variable is
///
/// @f[ \frac{dc}{dt} -= k_d c K^n/(C_1^n+K^n) @f]
///
/// where @f$ k_d @f$ is the degradation rate, and @f$ C_1 @f$ is the
/// user supplied variable.
///
/// In a model file the reaction is defined as
///
/// @verbatim
/// degradatioHillR 3 1 1
/// k_d K n
/// index1
/// @endverbatim
///
class DegradationHillR : public BaseReaction {
  
 public:
  
	///
	/// @brief Main constructor
	///
	/// This is the main constructor which sets the parameters and variable
	/// indices that defines the reaction.
	///
	/// @param paraValue vector with parameters
	///
	/// @param indValue vector of vectors with variable indices
	///
	/// @see BaseReaction::createReaction(std::vector<double> &paraValue,...)
	///
  DegradationHillR(std::vector<double> &paraValue, 
									std::vector< std::vector<size_t> > &indValue );
  
	///
	/// @brief Derivative function for this reaction class
	///
	/// @see BaseReaction::derivs(Compartment &compartment,size_t species,...)
	///
  void derivs(Compartment &compartment,size_t species,DataMatrix &y,DataMatrix &dydt);
};

///
/// @brief Molecular degradation for cells above/below a threshold position 
///
/// This class uses a threshold in a given dimension to determine if a
/// constant (rate @f$ k_d @f$) molecular degradation should be
/// applied. Threshold value is given as parameter, and a second parameter is
/// the sign which is determining if degradation is applied above (sign=1) or
/// below (sign=-1) the threshold value. The thrshold variable index is also
/// given.
///
/// In a model file the reaction is defined as
///
/// @verbatim
/// degradationSpatialThreshold 3 1 1
/// k_d var_th sign
/// index1
/// @endverbatim
///
class DegradationSpatialThreshold : public BaseReaction {
  
 public:
  
	///
	/// @brief Main constructor
	///
	/// This is the main constructor which sets the parameters and variable
	/// indices that defines the reaction.
	///
	/// @param paraValue vector with parameters
	///
	/// @param indValue vector of vectors with variable indices
	///
	/// @see BaseReaction::createReaction(std::vector<double> &paraValue,...)
	///
  DegradationSpatialThreshold(std::vector<double> &paraValue, 
													 std::vector< std::vector<size_t> > &indValue );
  
	///
	/// @brief Derivative function for this reaction class
	///
	/// @see BaseReaction::derivs(Compartment &compartment,size_t species,...)
	///
  void derivs(Compartment &compartment,size_t species,DataMatrix &y,DataMatrix &dydt);
};

///
/// @brief Molecular degradation for cells above/below a threshold position (implemented as Hill function) 
///
/// This class implements a spatial degradation implemented as a Hill function
/// it uses 4 parameters, K_d, X_th, n_th and X_sign. K_d is the maximal strength of the degradation, the limit
/// of the function when the given dimension tends towards 
/// -inf or +inf, is dX/dt = 0 or dX/dt = - k_d * X. X_th is the spatial threshold,
/// the degradation is half its maximal value where the given dimension is equal to X_th.
/// n is the slope of the change between no degradation and degradation, the higher n is, 
/// the sharper the change. X_sign can be 1 or -1, with 1 the degradation is applied
/// above the threshold value, with -1 the degradation is applied below.
/// The dimension variable index is also given.
///
/// In a model file the reaction is defined as
///
/// @verbatim
/// degradationSpatialThreshold 4 1 1
/// k_d var_th n sign
/// dimension
/// @endverbatim
///
class DegradationSpatialThresholdHill : public BaseReaction {
  
 public:
  
	///
	/// @brief Main constructor
	///
	/// This is the main constructor which sets the parameters and variable
	/// indices that defines the reaction.
	///
	/// @param paraValue vector with parameters
	///
	/// @param indValue vector of vectors with variable indices
	///
	/// @see BaseReaction::createReaction(std::vector<double> &paraValue,...)
	///
  DegradationSpatialThresholdHill(std::vector<double> &paraValue, 
													 std::vector< std::vector<size_t> > &indValue );
  
	///
	/// @brief Derivative function for this reaction class
	///
	/// @see BaseReaction::derivs(Compartment &compartment,size_t species,...)
	///
  void derivs(Compartment &compartment,size_t species,DataMatrix &y,DataMatrix &dydt);
};



#endif


