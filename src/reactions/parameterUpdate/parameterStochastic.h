//
// Filename     : parameterStochastic.h
// Description  : Stochastic update of parameter values
// Author(s)    : André Larsson (andre@thep.lu.se)
//              : Henrik Jonsson (henrik@thep.lu.se)
// Created      : Aug 2013
// Revision     : $Id:$
//

#ifndef PARAMETERSTOCHASTIC_H
#define PARAMETERSTOCHASTIC_H

#include<cmath>
#include <ctime>            // std::time

#include <boost/random/linear_congruential.hpp>
#include <boost/random/uniform_real.hpp>
#include <boost/random/variate_generator.hpp>

// According to the boost documentation this should be included:
// Sun CC doesn't handle boost::iterator_adaptor yet
#if !defined(__SUNPRO_CC) || (__SUNPRO_CC > 0x530)
#include <boost/generator_iterator.hpp>
#endif

#ifdef BOOST_NO_STDC_NAMESPACE
namespace std {
  using ::time;
}
#endif

#include"../baseReaction.h"
#include"../../organism.h"

typedef boost::minstd_rand base_generator_type;

///
/// @brief Updates a list of parameters and includes a stochastic part.
///
/// A list of parameters given will be updated according to the rule below
/// between the variable updates. Hence, no derivs is defined but initiate 
/// sets the parameter(s) to p0 and update sets the value given the current 
/// time. 
///
/// @f[ p_{i} = p_{0} + p_{1} o @f]
///
/// where \e i is the parameter index for the parameter to be updated,
/// t is the time and o is a random number drawn from a uniform distribution between [-0.5:05). The frequency by which the parameter is updated depends
/// on the step-length taken by the solver. The parameter @f$ p2 @f$ sets
/// the maximum update freqency (num updates/simulation time). This parameter
/// together with boundaries on the solver step-length can be used to
/// indirectly set the a consistent update frequency for the parameter.
///
/// In the model file the reaction is given by:
/// @verbatim
/// parameterStochastic 3 1 N
/// p0 p1 p2
/// pi ... pN
/// @endverbatim
///
/// @note Right now the parameter(s) is not initiated which means that
/// the parameter value should be set to p0 of this reaction for not
/// generating a jump in value.
///
class ParameterStochastic : public BaseReaction {

 private:
  base_generator_type generator;
  double last_update;
  
 public:
  
  ///
  /// @brief Main constructor
  ///
  /// This is the main constructor which sets the parameters and variable
  /// indices that defines the reaction.
  ///
  /// @param paraValue vector with parameters
  ///
  /// @param indValue vector of vectors with variable indices
  ///
  /// @see BaseReaction::createReaction(std::vector<double> &paraValue,...)
  ///
  ParameterStochastic(std::vector<double> &paraValue, 
			  std::vector< std::vector<size_t> > &indValue );
  
  ///
  /// @brief Initialization function for this reaction class
  ///
  /// @see BaseReaction::initiate(double t,DataMatrix &y)
  ///
  void initiate(double t,DataMatrix &y);

  ///
  /// @brief Derivative function for this reaction class
  ///
  /// @see BaseReaction::derivs(Compartment &compartment,size_t species,...)
  ///
  void derivs(Compartment &compartment,size_t species,DataMatrix &y,DataMatrix &dydt);

  ///
  /// @brief Update function for this reaction class
  ///
  /// @see BaseReaction::update(double h, double t, ...)
  ///

  void update(double h, double t,
	      std::vector< std::vector<double> > &y);

};

#endif
