//
// Filename     : parameterCopy.h
// Description  : Copies a parameter value into another
// Author(s)    : Henrik Jonsson (henrik@thep.lu.se)
// Created      : April 2012
// Revision     : $Id:$
//
#ifndef PARAMETERCOPY_H
#define PARAMETERCOPY_H

#include<cmath>

#include"../baseReaction.h"
#include"../../organism.h"

///
/// @brief Copies a list of parameters to another parameter's value
///
/// A list of parameters given will be copied from a single value.
/// This is done either before the simulation only, or between derivative updates.
///
/// @f[ p_{i} = p_{j} @f]
///
/// where \e i is the parameter index for the parameter(s) to be updated,
/// and \e j is the value to be copied. If @f$p_{0}@f$ is set to 0, this will
/// only be done before the simulations starts (normal case assuming the parameter value
/// does not change), and if set to 1, it is done between all model derivative update steps.
///
/// In the model file the reaction is given by:
/// @verbatim
/// parameterCopy 1 1 N
/// p0
/// pj
/// pi ... pN
/// @endverbatim
///
/// @note Right now the parameter(s) is not initiated which means that
/// the parameter value should be set to p0 of this reaction for not
/// generating a jump in value.
///
class ParameterCopy : public BaseReaction {
  
 public:
  
  ///
  /// @brief Main constructor
  ///
  /// This is the main constructor which sets the parameters and variable
  /// indices that defines the reaction.
  ///
  /// @param paraValue vector with parameters
  ///
  /// @param indValue vector of vectors with variable indices
  ///
  /// @see BaseReaction::createReaction(std::vector<double> &paraValue,...)
  ///
  ParameterCopy(std::vector<double> &paraValue, 
		std::vector< std::vector<size_t> > &indValue );
  
  ///
  /// @brief Initialization function for this reaction class
  ///
  /// @see BaseReaction::initiate(double t,DataMatrix &y)
  ///
  void initiate(double t,DataMatrix &y);

  ///
  /// @brief Derivative function for this reaction class
  ///
  /// @see BaseReaction::derivs(Compartment &compartment,size_t species,...)
  ///
  void derivs(Compartment &compartment,size_t species,DataMatrix &y,DataMatrix &dydt);
  
  ///
  /// @brief Jacobian function for this reaction class
  ///
  /// @see BaseReaction::Jacobian(Compartment &compartment,size_t species,...)
  ///
  size_t Jacobian(Compartment &compartment,size_t species,DataMatrix &y,JacobianMatrix &A);  

  ///
  /// @brief Update function for this reaction class
  ///
  /// @see BaseReaction::update(double h, double t, ...)
  ///
  void update(double h, double t,
	      std::vector< std::vector<double> > &y);  
};

#endif
