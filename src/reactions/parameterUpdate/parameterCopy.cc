//
// Filename     : parameterCopy.cc
// Description  : Copies a parameter value into another
// Author(s)    : Henrik Jonsson (henrik@thep.lu.se)
// Created      : April 2012
// Revision     : $Id:$
//

#include"parameterCopy.h"

ParameterCopy::
ParameterCopy(std::vector<double> &paraValue, 
	      std::vector< std::vector<size_t> > &indValue ) 
{
  //
  // Do some checks on the parameters and variable indeces
  //
  if( paraValue.size()!=1 || (paraValue[0]!=0.0 && paraValue[0]!=1.0) ) {
    std::cerr << "ParameterCopy::ParameterCopy() "
	      << "A flag parameter used for setting if the parameter value(s)"
	      << "should be updated only before (p_0=0) or also during (P_0=1) "
	      << "the simulation." << std::endl;
    exit(0);
  }
  if( indValue.size()!=2 || indValue[0].size()!=1 || indValue[1].size()<1 ) {
    std::cerr << "ParameterCopy::ParameterCopy() "
	      << "Two levels of indices is used, "
	      << "where the parameter to be copied is stored in the first, "
	      << "and the parameter(s) to be updated in the second, given in "
	      << "Organism order." << std::endl;
    exit(0);
  }
  //
  // Set the variable values
  //
  setId("parameterCopy");
  setParameter(paraValue);  
  setVariableIndex(indValue);
  //
  // Set the parameter identities
  //
  std::vector<std::string> tmp( numParameter() );
  tmp.resize( numParameter() );
  tmp[0] = "update_flag";
  setParameterId( tmp );
}

void ParameterCopy::
derivs(Compartment &compartment,size_t species,DataMatrix &y,DataMatrix &dydt) 
{
  // Nothing to be done for the derivative function.
}

void ParameterCopy::
initiate(double t,DataMatrix &y) 
{
  for (size_t i=0; i<numVariableIndex(1); ++i) {
    assert( variableIndex(1,i)<organism_->numParameter());
    organism()->setParameter(variableIndex(1,i), organism()->parameter(variableIndex(0,0)));
  }
}

size_t ParameterCopy::
Jacobian(Compartment &compartment,size_t species,DataMatrix &y,JacobianMatrix &A) 
{
  // Nothing to be done for the jacobian function
  return 1;
}
void ParameterCopy::
update(double h, double t,DataMatrix &y) 
{
  if (parameter(0)==1.0) {
    for (size_t i=0; i<numVariableIndex(1); ++i) {
      assert( variableIndex(1,i)<organism_->numParameter());
      organism()->setParameter(variableIndex(1,i), organism()->parameter(variableIndex(0,0)));
    }
  }
}
