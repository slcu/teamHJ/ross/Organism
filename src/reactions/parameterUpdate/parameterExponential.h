//
// Filename     : parameterExponential.h
// Description  : Exponential update of parameter values
// Author(s)    : Henrik Jonsson (henrik@thep.lu.se)
// Created      : April 2012
// Revision     : $Id:$
//
#ifndef PARAMETEREXPONENTIAL_H
#define PARAMETEREXPONENTIAL_H

#include<cmath>

#include"../baseReaction.h"
#include"../../organism.h"

///
/// @brief Updates a list of parameters according to an exponential dependence on time
///
/// A list of parameters given will be updated according to the rule below
/// between the variable updates. Hence, no derivs is defined but initiate 
/// sets the parameter(s) to p0 and update sets the value given the current 
/// time. 
///
/// @f[ p_{i} = p_{0} e^{p_{1}t}@f]
///
/// where \e i is the parameter index for the parameter to be updated,
/// t is the time. The update is 
///
/// In the model file the reaction is given by:
/// @verbatim
/// parameterExponentialPos 2 1 N
/// p0 p1
/// pi ... pN
/// @endverbatim
///
/// @note Right now the parameter(s) is not initiated which means that
/// the parameter value should be set to p0 of this reaction for not
/// generating a jump in value.
///
class ParameterExponentialPos : public BaseReaction {
  
 public:
  
  ///
  /// @brief Main constructor
  ///
  /// This is the main constructor which sets the parameters and variable
  /// indices that defines the reaction.
  ///
  /// @param paraValue vector with parameters
  ///
  /// @param indValue vector of vectors with variable indices
  ///
  /// @see BaseReaction::createReaction(std::vector<double> &paraValue,...)
  ///
  ParameterExponentialPos(std::vector<double> &paraValue, 
			  std::vector< std::vector<size_t> > &indValue );
  
  ///
  /// @brief Initialization function for this reaction class
  ///
  /// @see BaseReaction::initiate(double t,DataMatrix &y)
  ///
  void initiate(double t,DataMatrix &y);

  ///
  /// @brief Derivative function for this reaction class
  ///
  /// @see BaseReaction::derivs(Compartment &compartment,size_t species,...)
  ///
  void derivs(Compartment &compartment,size_t species,DataMatrix &y,DataMatrix &dydt);

  ///
  /// @brief Update function for this reaction class
  ///
  /// @see BaseReaction::update(double h, double t, ...)
  ///

  void update(double h, double t,
	      std::vector< std::vector<double> > &y);

};

///
/// @brief Updates a list of parameters according to an exponential dependence on time
///
/// A list of parameters given will be updated according to the rule below
/// between the variable updates. Hence, no derivs is defined but initiate 
/// sets the parameter(s) to p0 and update sets the value given the current 
/// time. 
///
/// @f[ p_{i} = p_{0} e^{-p_{1}t}@f]
///
/// where \e i is the parameter index for the parameter to be updated,
/// t is the time. The update is 
///
/// In the model file the reaction is given by:
/// @verbatim
/// parameterExponentialNeg 2 1 N
/// p0 p1
/// pi ... pN
/// @endverbatim
///
/// @note Right now the parameter(s) is not initiated which means that
/// the parameter value should be set to p0 of this reaction for not
/// generating a jump in value.
///
class ParameterExponentialNeg : public BaseReaction {

 public:
  
  ///
  /// @brief Main constructor
  ///
  /// This is the main constructor which sets the parameters and variable
  /// indices that defines the reaction.
  ///
  /// @param paraValue vector with parameters
  ///
  /// @param indValue vector of vectors with variable indices
  ///
  /// @see BaseReaction::createReaction(std::vector<double> &paraValue,...)
  ///
  ParameterExponentialNeg(std::vector<double> &paraValue, 
			  std::vector< std::vector<size_t> > &indValue );
  
  ///
  /// @brief Initialization function for this reaction class
  ///
  /// @see BaseReaction::initiate(double t,DataMatrix &y)
  ///
  void initiate(double t,DataMatrix &y);

  ///
  /// @brief Derivative function for this reaction class
  ///
  /// @see BaseReaction::derivs(Compartment &compartment,size_t species,...)
  ///
  void derivs(Compartment &compartment,size_t species,DataMatrix &y,DataMatrix &dydt);

  ///
  /// @brief Update function for this reaction class
  ///
  /// @see BaseReaction::update(double h, double t, ...)
  ///
  void update(double h, double t,
	      std::vector< std::vector<double> > &y);

};

#endif
