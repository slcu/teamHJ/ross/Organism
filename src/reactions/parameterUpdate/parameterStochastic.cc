//
// Filename     : parameterStochastic.cc
// Description  : Exponential update of parameter values
// Author(s)    : André Larsson (andre@thep.lu.se)
//              : Henrik Jonsson (henrik@thep.lu.se)
// Created      : Aug 2013
// Revision     : $Id:$
//

#include"parameterStochastic.h"

ParameterStochastic::
ParameterStochastic(std::vector<double> &paraValue, 
			std::vector< std::vector<size_t> > &indValue ): generator(static_cast<unsigned int>(std::time(0)))
{
  // Note: random number generator initialized in the initialization list above

  last_update = 0.0;

  //
  // Do some checks on the parameters and variable indeces
  //
  if( paraValue.size()!=3 ) {
    std::cerr << "ParameterStochastic::ParameterStochastic() "
	      << "Three parameters used "
	      << "p0 average parameter value, p1 width of noise, p2 maximum update frequency. p = p0 + p1*o" << std::endl;
    exit(0);
  }
  if( indValue.size()!=1 || indValue[0].size()<1 ) {
    std::cerr << "ParameterExponentialPos::ParameterExponentialPos() "
	      << "One level of indices is used, "
	      << "where the parameter to be updated are stored, "
	      << "given in Organism order." << std::endl;
    exit(0);
  }

  //
  // Set the variable values
  //
  setId("parameterStochastic");
  setParameter(paraValue);  
  setVariableIndex(indValue);
  //
  // Set the parameter identities
  //
  std::vector<std::string> tmp( numParameter() );
  tmp.resize( numParameter() );
  tmp[0] = "const";
  tmp[1] = "exp";
  setParameterId( tmp );

}

void ParameterStochastic::
derivs(Compartment &compartment,size_t species,DataMatrix &y,DataMatrix &dydt) 
{
    // Nothing to be done for the derivative function.
}

void ParameterStochastic::
initiate(double t,DataMatrix &y)
{
  for (size_t i=0; i<numVariableIndex(0); ++i) {
    assert( variableIndex(0,i)<organism_->numParameter());
    organism()->setParameter( variableIndex(0,i), parameter(0) );
  }
}

void ParameterStochastic::
update(double h, double t,DataMatrix &y) 
{
  // only update value every n time step
  if( (t - last_update) > 1.0/parameter(2)){
    last_update += 1.0/parameter(2);
    
    // Define a uniform random number distribution which produces "double"
    // values callable as the function uni().

    boost::uniform_real<> uni_dist(-0.5, 0.5);
    boost::variate_generator<base_generator_type&, boost::uniform_real<> > uni(generator, uni_dist);

    for (size_t i=0; i<numVariableIndex(0); ++i) {
      assert( variableIndex(0,i)<organism_->numParameter());
      organism()->setParameter( variableIndex(0,i), parameter(0) + parameter(1)*uni());
    }
  }
}
