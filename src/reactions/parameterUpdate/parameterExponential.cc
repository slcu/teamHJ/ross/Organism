//
// Filename     : parameterExponential.cc
// Description  : Exponential update of parameter values
// Author(s)    : Henrik Jonsson (henrik@thep.lu.se)
// Created      : April 2012
// Revision     : $Id:$
//

#include"parameterExponential.h"

ParameterExponentialPos::
ParameterExponentialPos(std::vector<double> &paraValue, 
			std::vector< std::vector<size_t> > &indValue ) 
{
  //
  // Do some checks on the parameters and variable indeces
  //
  if( paraValue.size()!=2 ) {
    std::cerr << "ParameterExponentialPos::ParameterExponentialPos() "
	      << "two parameters used "
	      << "p = p0 exp(p1*t)." << std::endl;
    exit(0);
  }
  if( indValue.size()!=1 || indValue[0].size()<1 ) {
    std::cerr << "ParameterExponentialPos::ParameterExponentialPos() "
	      << "One level of indices is used, "
	      << "where the parameter to be updated are stored, "
	      << "given in Organism order." << std::endl;
    exit(0);
  }
  //
  // Set the variable values
  //
  setId("parameterExponentialPos");
  setParameter(paraValue);  
  setVariableIndex(indValue);
  //
  // Set the parameter identities
  //
  std::vector<std::string> tmp( numParameter() );
  tmp.resize( numParameter() );
  tmp[0] = "const";
  tmp[1] = "exp";
  setParameterId( tmp );
  
}

void ParameterExponentialPos::
derivs(Compartment &compartment,size_t species,DataMatrix &y,DataMatrix &dydt) 
{
  // Nothing to be done for the derivative function.
}

void ParameterExponentialPos::
initiate(double t,DataMatrix &y) 
{
  for (size_t i=0; i<numVariableIndex(0); ++i) {
    assert( variableIndex(0,i)<organism_->numParameter());
    organism()->setParameter(variableIndex(0,i), parameter(0)*std::exp(parameter(1)*t) );
  }
}

void ParameterExponentialPos::
update(double h, double t,DataMatrix &y) 
{
  for (size_t i=0; i<numVariableIndex(0); ++i) {
    assert( variableIndex(0,i)<organism_->numParameter());
    organism()->setParameter(variableIndex(0,i), parameter(0)*std::exp(parameter(1)*t) );
  }
}

ParameterExponentialNeg::
ParameterExponentialNeg(std::vector<double> &paraValue, 
			std::vector< std::vector<size_t> > &indValue ) 
{
  //
  // Do some checks on the parameters and variable indeces
  //
  if( paraValue.size()!=2 ) {
    std::cerr << "ParameterExponentialNeg::ParameterExponentialNeg() "
	      << "two parameters used "
	      << "p = p0 exp(p1*t)." << std::endl;
    exit(0);
  }
  if( indValue.size()!=1 || indValue[0].size()<1 ) {
    std::cerr << "ParameterExponentialNeg::ParameterExponentialNeg() "
	      << "One level of indices is used, "
	      << "where the parameter to be updated are stored, "
	      << "given in Organism order." << std::endl;
    exit(0);
  }
  //
  // Set the variable values
  //
  setId("parameterExponentialNeg");
  setParameter(paraValue);  
  setVariableIndex(indValue);
  //
  // Set the parameter identities
  //
  std::vector<std::string> tmp( numParameter() );
  tmp.resize( numParameter() );
  tmp[0] = "const";
  tmp[1] = "exp";
  setParameterId( tmp );
}

void ParameterExponentialNeg::
derivs(Compartment &compartment,size_t species,DataMatrix &y,DataMatrix &dydt) 
{
  // Nothing to be done for the derivative function.
}

void ParameterExponentialNeg::
initiate(double t,DataMatrix &y) 
{
  for (size_t i=0; i<numVariableIndex(0); ++i) {
    assert( variableIndex(0,i)<organism_->numParameter());
    organism()->setParameter(variableIndex(0,i), parameter(0)*std::exp(-parameter(1)*t) );
  }
}

void ParameterExponentialNeg::
update(double h, double t,DataMatrix &y) 
{
  for (size_t i=0; i<numVariableIndex(0); ++i) {
    assert( variableIndex(0,i)<organism_->numParameter());
    organism()->setParameter(variableIndex(0,i), parameter(0)*std::exp(-parameter(1)*t) );
  }
}
