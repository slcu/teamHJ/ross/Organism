//
// Filename     : stochastic.cc
// Description  : Classes describing stochastic creation of molecules
// Author(s)    : André Larsson (andre@thep.lu.se)
//              : Henrik Jonsson (henrik@thep.lu.se)
// Created      : August 2013
// Revision     : $Id: creation.cc 565 2013-02-08 12:44:44Z jeremy $
//

#include"baseReaction.h"
#include"stochastic.h"

namespace stochastic{

  gammaOU::gammaOU(std::vector<double> &paraValue, 
				 std::vector< std::vector<size_t> > 
				 &indValue ) 
  {  
    // Do some checks on the parameters and variable indices
    if( paraValue.size()!=3 ) {
      std::cerr << "stochastic::gammaOU::gammaOU() "
		<< "Uses three parameters lambda (sampling frequency), nu (shape), mean=nu/alpha (shape/rate) \n";
      exit(0);
    }
    if( indValue.size() != 0 && indValue.size() != 1 ) {
      std::cerr << "stochastic::gammaOU::gammaOU() "
		<< "At most one level of variables are allowed.\n";
      exit(0);
    }
	
    // Set the variable values
    setId("stochastic::gammaOU");
    setParameter(paraValue);  
    setVariableIndex(indValue);
  
    // Set the parameter identities
    std::vector<std::string> tmp( numParameter() );
    tmp.resize( numParameter() );
    tmp[0] = "lambda";
    tmp[1] = "nu";
    tmp[2] = "nu/alpha";
    setParameterId( tmp );

  }

  void gammaOU::
  initiate(double t,DataMatrix &y) 
  {
    // Create exponential distributions
    // This cannot be done in the constructor which does not know
    // of values taken from parameter files (if used)
    // Note that the parameter sent to exp dist is 1/mean in boost
    double nu = parameter(1);
    double alpha = parameter(1)/parameter(2);
    boost_rand_ln.CreateExponential( parameter(0)*nu );
    boost_rand_a.CreateExponential( alpha );
  }


  void gammaOU::
  derivs(Compartment &compartment,size_t varIndex,DataMatrix &y,DataMatrix &dydt) 
  {  
    // Do nothing
  }

  void gammaOU::
  update(double h, double t,DataMatrix &y) 
  {
    double tmp, pse, xpse;
    
    for( size_t i=0; i< y.size(); ++i ){
      for( size_t k=0; k<numVariableIndex(0); ++k ){
	
	pse = boost_rand_ln.Exponential();
	xpse = 0;
	while (pse <= h){
	  tmp = boost_rand_a.Exponential();
	  xpse = xpse + std::exp(-parameter(0)*(h - pse) )*tmp;
	  pse = pse + boost_rand_ln.Exponential();
	}

	y[i][variableIndex(0,k)] =  std::exp(-parameter(0)*h) * y[i][variableIndex(0,k)] + xpse;

      }
    }
  }

}
