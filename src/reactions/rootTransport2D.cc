/*
 * Filename     : rootTransport2D.cc
 * Description  : Classes describing updates due to molecule transports
 * Author(s)    : Pontus (pontus@thep.lu.se)
 * Created      : October 2007
 * Revision     : $Id: 
 */

#include"baseReaction.h"
#include"rootTransport2D.h"

RootTransport2D::RootTransport2D(std::vector<double> 
					   &paraValue, 
					   std::vector< 
					   std::vector<size_t> > 
					   &indValue ) {
  
  //Do some checks on the parameters and variable indeces
  //////////////////////////////////////////////////////////////////////
  if( paraValue.size()!=5 ) {
    std::cerr << "RootTransport::"
	      << "RootTransport() "
	      << "uses five parameters (T_eff, T_inf, K_M, f_i, f_ij).\n";
    exit(0);
  }
  if( indValue.size() != 3 || indValue[0].size() !=2 
      || indValue[1].size() !=1 || indValue[2].size() !=1  ) {
    std::cerr << "RootTransport::"
							<< "RootTransport() "
							<< "cell marker + wall marker "
							<< "indices needed in"
							<< "level 1.\n"
							<< "Polarized molecule in neigh-file index needed att level 2\n" 
							<< "Variable index for size parameter needed at "
							<< "level 3.\n";
    exit(0);
  }
  //Check parameters
  //////////////////////////////////////////////////////////////////////
  if( paraValue[0]<0.0 || paraValue[1]<0.0 || paraValue[2]<0.0 ||
      paraValue[3]<0.0 || paraValue[4]<0.0 || paraValue[4]>1 ) {
    std::cerr << "RootTransport::"
	      << "RootTransport()"
	      << " Parameters need to be positive (and fractions).\n";
    exit(0);
  }
  //Set the variable values
  //////////////////////////////////////////////////////////////////////
  setId("polarizationConstFastTransportFractionRestrictedMM");
  setParameter(paraValue);  
  setVariableIndex(indValue);
  
  //Set the parameter identities
  //////////////////////////////////////////////////////////////////////
  std::vector<std::string> tmp( numParameter() );
  tmp.resize( numParameter() );
  tmp[0] = "T_eff";
  tmp[1] = "T_inf";
	tmp[2] = "K_M";
  tmp[3] = "f_i";
  tmp[4] = "f_ij";
  setParameterId( tmp );
	
}

void RootTransport2D::derivs(Compartment &compartment,size_t species,
       std::vector< std::vector<double> > &y,
       std::vector< std::vector<double> > 
       &dydt ) {
  
  size_t i=compartment.index();
  //Only if the comp has neighbors and is a cell
	if( compartment.numNeighbor() == 0 || 
      y[i][variableIndex(0,0)]<0.5 ) return;
  
  //Extract volume of the compartment and make sure it is physical
  double Vi = y[i][variableIndex(2,0)];
	//size_t index =variableIndex(1,0);	
	
	if ( Vi<0.0 ) {
		std::cerr << "RootTransport2D::derivs: volume must be positive\n";
		std::cerr << Vi << " " << i << " i \n";
		exit(-1);
	}
  
//Update all compartments
  //////////////////////////////////////////////////////////////////////
  for( size_t n=0 ; n<compartment.numNeighbor() ; n++ ) {
    size_t j=compartment.neighbor(n);
    		
    //Make sure neighbor marked as wall and that it contains transporter 
		if ( y[j][variableIndex(0,1)]>0.5 
				 && compartment.neighborVariable(variableIndex(1,0),n)>0 ) { 
			//Extract volume of neighbor and make sure it is physical
			double Vj = y[j][variableIndex(2,0)];
			if ( Vj<0.0 ) {
				std::cerr << "RootTransport2D::derivs: volume must be positive\n";
				std::cerr << Vj << " " << j << " j \n";
				exit(-1);
			}
			
			if( compartment.numNeighbor()==compartment.numNeighborArea() ) {
				double coeff = compartment.neighborArea(n)*
					parameter(3)*y[i][species]/( parameter(2)+parameter(3)*y[i][species] );
				
				//std::cerr << compartment.neighborVariable(index,n) << "\n";
				dydt[i][species] -= parameter(0)*coeff/Vi;
				dydt[j][species] += parameter(0)*coeff/Vj;
				coeff = compartment.neighborArea(n)*
					parameter(4)*y[j][species]/( parameter(2)+parameter(4)*y[j][species] );
				dydt[i][species] += parameter(1)*coeff/Vi;
				dydt[j][species] -= parameter(1)*coeff/Vj;
			}
			else {
				std::cerr << "Warning\n";
				exit(-1);
			}
		}
	}
}
