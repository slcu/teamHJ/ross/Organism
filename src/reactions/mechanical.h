//
// Filename     : mechanical.h
// Description  : Only includes files describing mechanical properties
// Author(s)    : Henrik Jonsson (henrik@thep.lu.se)
// Created      : April 2010
// Revision     : $Id: mechanical.h 441 2010-04-23 13:15:48Z henrik $
//
#ifndef MECHANICAL_H
#define MECHANICAL_H

#include"mechanical/ellipticSpring.h"
#include"mechanical/sphereBudSpring.h"
#include"mechanical/sphereSpring.h"
#include"mechanical/mechanicalCigar.h"
#include"mechanical/sphereForce.h"
#include"mechanical/sphereWall.h"

#endif
