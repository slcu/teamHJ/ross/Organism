//
// Filename     : creation.cc
// Description  : Classes describing creation updates
// Author(s)    : Henrik Jonsson (henrik@thep.lu.se)
//              : Pontus Melke
// Created      : December 2003
// Revision     : $Id: creation.cc 673 2017-05-02 10:03:14Z korsbo $
//

#include<cmath>

#include"baseReaction.h"
#include"creation.h"
#include"../organism.h"

CreationLinear::CreationLinear(std::vector<double> &paraValue, 
			       std::vector< std::vector<size_t> > 
			       &indValue ) 
{  
	// Do some checks on the parameters and variable indeces
	if( paraValue.size()!=1 ) {
		std::cerr << "CreationLinear::CreationLinear() "
			  << "Uses only one parameter k_c\n";
		exit(0);
	}
	if( indValue.size() != 0 && indValue.size() != 1 ) {
		std::cerr << "CreationLinear::CreationLinear() "
			  << "At most one level of variables are allowed.\n";
		exit(0);
	}
	
	// Set the variable values
	setId("creationLinear");
	setParameter(paraValue);  
	setVariableIndex(indValue);
  
	// Set the parameter identities
	std::vector<std::string> tmp( numParameter() );
	tmp.resize( numParameter() );
	tmp[0] = "k_c";
	setParameterId( tmp );
}

void CreationLinear::
derivs(Compartment &compartment,size_t varIndex,DataMatrix &y,DataMatrix &dydt) 
{  
	double rate = parameter(0);
	if( numVariableIndexLevel() )
	  for( size_t k=0; k<numVariableIndex(0); ++k )
	    rate *= y[compartment.index()][ variableIndex(0,k) ];
	dydt[compartment.index()][varIndex] += rate;
}

void CreationLinear::
derivsWithAbs(Compartment &compartment,size_t varIndex,DataMatrix &y,DataMatrix &dydt,DataMatrix &sdydt) 
{  
	double rate = parameter(0);
	if( numVariableIndexLevel() )
	  for( size_t k=0; k<numVariableIndex(0); ++k )
	    rate *= y[compartment.index()][ variableIndex(0,k) ];
	dydt[compartment.index()][varIndex] += rate;
	sdydt[compartment.index()][varIndex] += rate;
}

CreationZero::
CreationZero(std::vector<double> &paraValue, 
	     std::vector< std::vector<size_t> > 
	     &indValue ) 
{  
  // Do some checks on the parameters and variable indices
  if( paraValue.size()!=1 ) {
    std::cerr << "CreationZero::CreationZero() "
	      << "Uses only zero parameter k_c\n";
    exit(0);
  }
  if( indValue.size() ) {
    std::cerr << "CreationZero::CreationZero() "
	      << "No variable index is used.\n";
    exit(0);
  }
  
  // Set the variable values
  setId("creationZero");
  setParameter(paraValue);  
  setVariableIndex(indValue);
  
  // Set the parameter identities
  std::vector<std::string> tmp( numParameter() );
  tmp.resize( numParameter() );
  tmp[0] = "k_c";
  setParameterId( tmp );
}

void CreationZero::
derivs(Compartment &compartment,size_t varIndex,DataMatrix &y,DataMatrix &dydt) 
{  
  dydt[compartment.index()][varIndex] += parameter(0);
}

void CreationZero::
derivsWithAbs(Compartment &compartment,size_t varIndex,DataMatrix &y,DataMatrix &dydt,DataMatrix &sdydt) 
{  
  dydt[compartment.index()][varIndex] += parameter(0);
  sdydt[compartment.index()][varIndex] += parameter(0);
}

size_t CreationZero::
Jacobian(Compartment &compartment,size_t varIndex,DataMatrix &y,JacobianMatrix &A) 
{  
  return 0;
}

double CreationZero::
propensity(Compartment &compartment,size_t varIndex,DataMatrix &y)
{
  return parameter(0);
}

void CreationZero::
discreteUpdate(Compartment &compartment,size_t varIndex,DataMatrix &y)
{
  y[compartment.index()][varIndex] += 1.0;
}

void CreationZero::printCambium( std::ostream &os, size_t varIndex ) const
{
  std::string varName=organism()->variableId(varIndex);
  os << "Arrow[Organism[" << id() << "],{},{},{" << varName << "[i]},Parameters[";
  os << parameter(0);
  os << "], ParameterNames[" << parameterId(0) << "], VarIndices[],"; 
  os << "Solve{" << varName << "[i]\' = p_0, "
     << "Using[" << organism()->topology().id() << "::Cell, i]}";
  os << "]" << std::endl;
}

CreationOne::CreationOne(std::vector<double> &paraValue, 
												 std::vector< std::vector<size_t> > &indValue ) 
{  
	// Do some checks on the parameters and variable indices
	if( paraValue.size()!=1 ) {
		std::cerr << "CreationOne::CreationOne() "
			  << "Uses only one parameter k_c\n";
		exit(0);
	}
	if( indValue.size() != 1 || indValue[0].size() != 1  ) {
		std::cerr << "CreationOne::CreationOne() "
							<< "One variable index needed.\n";
		exit(0);
	}
	
	// Set the variable values
	setId("creationOne");
	setParameter(paraValue);  
	setVariableIndex(indValue);
  
	// Set the parameter identities
	std::vector<std::string> tmp( numParameter() );
	tmp.resize( numParameter() );
	tmp[0] = "k_c";
	setParameterId( tmp );
}

void CreationOne::
derivs(Compartment &compartment,size_t varIndex,DataMatrix &y,DataMatrix &dydt) 
{
	dydt[compartment.index()][varIndex] += parameter(0)*
		y[compartment.index()][ variableIndex(0,0) ];
}

void CreationOne::
derivsWithAbs(Compartment &compartment,size_t varIndex,DataMatrix &y,DataMatrix &dydt,DataMatrix &sdydt) 
{
  double add = parameter(0)*y[compartment.index()][ variableIndex(0,0) ];
  dydt[compartment.index()][varIndex] += add;
  sdydt[compartment.index()][varIndex] += add;
}

size_t CreationOne::
Jacobian(Compartment &compartment,size_t varIndex,DataMatrix &y,JacobianMatrix &A) 
{
	size_t i = compartment.index()*y[0].size() + varIndex; 
	size_t j = compartment.index()*y[0].size() + variableIndex(0,0); 
	A(i,j) += parameter(0);
	return 0;
}

double CreationOne::
propensity(Compartment &compartment,size_t varIndex,DataMatrix &y)
{
  return parameter(0)*y[compartment.index()][ variableIndex(0,0) ];
}

void CreationOne::
discreteUpdate(Compartment &compartment,size_t varIndex,DataMatrix &y)
{
	y[compartment.index()][varIndex] += 1.0;
}

void CreationOne::printCambium( std::ostream &os, size_t varIndex ) const
{
  std::string varName=organism()->variableId(varIndex);
  std::string varNameCre=organism()->variableId(variableIndex(0,0));
  os << "Arrow[Organism[" << id() << "],{},{" << varNameCre << "[i]},{"
     << varName
     << "[i]}, Parameters[";
  os << parameter(0);
  os << "], ParameterNames[" << parameterId(0) << "], VarIndices[{"
     << variableIndex(0,0) << "}],"; 
  os << "Solve{" << varName << "[i]\' = p_0 " << varNameCre << "[i], "
     << "Using[" << organism()->topology().id() << "::Cell, i]}";
  os << "]" << std::endl;
}

CreationTwo::CreationTwo(std::vector<double> &paraValue, 
			 std::vector< std::vector<size_t> > 
			 &indValue ) 
{  
	// Do some checks on the parameters and variable indeces
	if( paraValue.size()!=1 ) {
		std::cerr << "CreationTwo::CreationTwo() "
			  << "Uses only one parameter k_c\n";
		exit(0);
	}
	if( indValue.size() != 1 || indValue[0].size() != 2  ) {
		std::cerr << "CreationTwo::CreationTwo() "
			  << "Two variable index needed.\n";
		exit(0);
	}

	// Set the variable values
	setId("creationTwo");
	setParameter(paraValue);  
	setVariableIndex(indValue);
  
	// Set the parameter identities
	std::vector<std::string> tmp( numParameter() );
	tmp.resize( numParameter() );
	tmp[0] = "k_c";
	setParameterId( tmp );
}

void CreationTwo::
derivs(Compartment &compartment,size_t varIndex,DataMatrix &y,DataMatrix &dydt) 
{  
	dydt[compartment.index()][varIndex] += parameter(0)*
		y[compartment.index()][ variableIndex(0,0) ]*
		y[compartment.index()][ variableIndex(0,1) ];
}

void CreationTwo::
derivsWithAbs(Compartment &compartment,size_t varIndex,DataMatrix &y,DataMatrix &dydt,DataMatrix &sdydt) 
{  
  double add = parameter(0)*y[compartment.index()][ variableIndex(0,0) ]*
    y[compartment.index()][ variableIndex(0,1) ];
  
  dydt[compartment.index()][varIndex] += add;
  sdydt[compartment.index()][varIndex] += add;
}

size_t CreationTwo::
Jacobian(Compartment &compartment,size_t varIndex,DataMatrix &y,JacobianMatrix &A) 
{  
	if (variableIndex(0,0)==variableIndex(0,1)) {
		size_t i = compartment.index()*y[0].size() + varIndex; 
		size_t j = compartment.index()*y[0].size() + variableIndex(0,0); 
		A(i,j) += 2.0*parameter(0)*y[compartment.index()][ variableIndex(0,0) ];
	}
	else {
		size_t i = compartment.index()*y[0].size() + varIndex; 
		size_t j = compartment.index()*y[0].size() + variableIndex(0,0); 
		A(i,j) += parameter(0)*y[compartment.index()][ variableIndex(0,1) ];
		j = compartment.index()*y[0].size() + variableIndex(0,1); 
		A(i,j) += parameter(0)*y[compartment.index()][ variableIndex(0,0) ];
	}
	return 1;
}

CreationZeroDegradationOne::CreationZeroDegradationOne(std::vector<double>
                                                       &paraValue,
                                                       std::vector<
                                                       std::vector<size_t> >
                                                       &indValue ) 
{  
	// Do some checks on the parameters and variable indeces
	if( paraValue.size()!=2 ) {
		std::cerr << "CreationZeroDegradationOne::CreationZeroDegradationOne() "
			  << "Uses two parameters K,k_d\n";
		exit(0);
	}
	if( indValue.size() ) {
		std::cerr << "CreationZeroDegradationOne::CreationZeroDegradationOne() "
			  << "No variable index is used.\n";
		exit(0);
	}

	// Set the variable values
	setId("creationZeroDegradationOne");
	setParameter(paraValue);
	setVariableIndex(indValue);
  
	// Set the parameter identities
	std::vector<std::string> tmp( numParameter() );
	tmp.resize( numParameter() );
	tmp[0] = "K";
	tmp[1] = "k_d";
	setParameterId( tmp );
}

void CreationZeroDegradationOne::
derivs(Compartment &compartment,size_t varIndex,DataMatrix &y,DataMatrix &dydt) 
{  
	dydt[compartment.index()][varIndex] += parameter(0)*
		(1.0-parameter(1)*y[compartment.index()][varIndex]);
}

size_t CreationZeroDegradationOne::
Jacobian(Compartment &compartment,size_t varIndex,DataMatrix &y,JacobianMatrix &A) 
{  
	size_t i = compartment.index()*y[0].size() + varIndex; 
	A(i,i) -= parameter(0)*parameter(1);
	return 0;
}

CreationZeroDegradationTwo::CreationZeroDegradationTwo(std::vector<double>
                                                       &paraValue,
                                                       std::vector<
                                                       std::vector<size_t> >
                                                       &indValue ) 
{	
	// Do some checks on the parameters and variable indeces
	if( paraValue.size()!=2 ) {
		std::cerr << "CreationZeroDegradationTwo::CreationZeroDegradationTwo() "
			  << "Uses two parameters K,k_d\n";
		exit(0);
	}
	if( indValue.size()!=1 || indValue[0].size() != 1 ) {
		std::cerr << "CreationZeroDegradationTwo::CreationZeroDegradationTwo() "
			  << "one variable index is used.\n";
		exit(0);
	}

	// Set the variable values
	setId("creationZeroDegradationTwo");
	setParameter(paraValue);
	setVariableIndex(indValue);
	
	// Set the parameter identities
	std::vector<std::string> tmp( numParameter() );
	tmp.resize( numParameter() );
	tmp[0] = "K";
	tmp[1] = "k_d";
	setParameterId( tmp );
}

void CreationZeroDegradationTwo::
derivs(Compartment &compartment,size_t varIndex,DataMatrix &y,DataMatrix &dydt) 
{  
	dydt[compartment.index()][varIndex] += parameter(0)*
		(1.0-parameter(1)*y[compartment.index()][varIndex]*
		 y[compartment.index()][variableIndex(0,0)]);
}

CreationOneDegradationOne::CreationOneDegradationOne(std::vector<double> 
						     &paraValue, 
						     std::vector< 
						     std::vector<size_t> > 
						     &indValue ) 
{  
	// Do some checks on the parameters and variable indeces
	if( paraValue.size()!=2 ) {
		std::cerr << "CreationOneDegradationOne::CreationOneDegradationOne() "
			  << "Uses two parameters K,k_d\n";
		exit(0);
	}
	if( indValue.size()!=1 || indValue[0].size() != 1  ) {
		std::cerr << "CreationOneDegradationOne::CreationOneDegradationOne() "
			  << "one variable indices are used.\n";
		exit(0);
	}
	
	// Set the variable values
	setId("creationOneDegradationOne");
	setParameter(paraValue);  
	setVariableIndex(indValue);
  
	// Set the parameter identities
	std::vector<std::string> tmp( numParameter() );
	tmp.resize( numParameter() );
	tmp[0] = "K";
	tmp[1] = "k_d";
	setParameterId( tmp );
}

void CreationOneDegradationOne::
derivs(Compartment &compartment,size_t varIndex,DataMatrix &y,DataMatrix &dydt) 
{  
	dydt[compartment.index()][varIndex] += parameter(0)*
		(y[compartment.index()][variableIndex(0,0)]-parameter(1)*y[compartment.index()][varIndex]);
}

CreationOneDegradationTwo::CreationOneDegradationTwo(std::vector<double>
						     &paraValue,
						     std::vector<
						     std::vector<size_t> >
						     &indValue ) 
{  
	// Do some checks on the parameters and variable indeces
	if( paraValue.size()!=2 ) {
		std::cerr << "CreationOneDegradationTwo::CreationOneDegradationTwo() "
			  << "Uses two parameters K,k_d\n";
		exit(0);
	}
	if( indValue.size()!=1 || indValue[0].size() != 2  ) {
		std::cerr << "CreationOneDegradationTwo::CreationOneDegradationTwo() "
			  << "two variable indices are used.\n";
		exit(0);
	}

	// Set the variable values
	setId("creationOneDegradationTwo");
	setParameter(paraValue);
	setVariableIndex(indValue);
  
	// Set the parameter identities
	std::vector<std::string> tmp( numParameter() );
	tmp.resize( numParameter() );
	tmp[0] = "K";
	tmp[1] = "k_d";
	setParameterId( tmp );
}

void CreationOneDegradationTwo::
derivs(Compartment &compartment,size_t varIndex,DataMatrix &y,DataMatrix &dydt) 
{
	dydt[compartment.index()][varIndex] += parameter(0)*
		( y[compartment.index()][variableIndex(0,0)] -
		  parameter(1)*y[compartment.index()][varIndex]*
		  y[compartment.index()][variableIndex(0,1)] );
}

CreationSpatialThreshold::CreationSpatialThreshold(std::vector<double> &paraValue, 
						   std::vector< std::vector<size_t> > 
						   &indValue ) 
{  
	// Do some checks on the parameters and variable indeces
	if( paraValue.size()!=3 ) {
		std::cerr << "CreationSpatialThreshold::CreationSpatialThreshold() "
			  << "Uses three parameters k_c X_th X_sign\n";
		exit(0);
	}
	if( indValue.size() != 1 || indValue[0].size() != 1 ) {
		std::cerr << "CreationSpatialThreshold::CreationSpatialThreshold()"
			  << "Variable index indicating threshold variable needed.\n";
		exit(0);
	}
	//Sign should be -/+1
	if( paraValue[2] != -1 && paraValue[2] != 1 ) {
		std::cerr << "CreationSpatialThreshold::CreationSpatialThreshold() "
			  << "X_sign should be +/-1\n";
		exit(0);
	}
	
	// Set the variable values
	setId("creationSpatialThreshold");
	setParameter(paraValue);  
	setVariableIndex(indValue);
  
	// Set the parameter identities
	std::vector<std::string> tmp( numParameter() );
	tmp.resize( numParameter() );
	tmp[0] = "k_c";
	tmp[1] = "X_th";
	tmp[2] = "X_sign";  
	setParameterId( tmp );
}

void CreationSpatialThreshold::
derivs(Compartment &compartment,size_t varIndex,DataMatrix &y,DataMatrix &dydt) 
{  
	if( y[compartment.index()][variableIndex(0,0)]*parameter(2)>parameter(1)*parameter(2) )
		dydt[compartment.index()][varIndex] += parameter(0);
}

CreationSpatialSphere::CreationSpatialSphere(std::vector<double> &paraValue, 
					     std::vector< std::vector<size_t> > 
					     &indValue ) 
{  
	// Do some checks on the parameters and variable indeces
	if( paraValue.size()!=4 ) {
		std::cerr << "CreationSpatialSphere::CreationSpatialSphere() "
			  << "Uses four parameters V_max R(K_Hill) n_Hill and R_sign\n";
		exit(0);
	}
	if( indValue.size() != 0 ) {
	  std::cerr << "CreationSpatialSphere::CreationSpatialSphere() "
		    << "No variable index needed." << std::endl;
	  exit(0);
	}
	// Sign should be -/+1
	if( paraValue[3] != -1 && paraValue[3] != 1 ) {
	  std::cerr << "CreationSpatialSphere::CreationSpatialSphere() "
		    << "R_sign should be +/-1 to set production inside/outside R"
		    << std::endl;
	  exit(0);
	}
	
	// Set the variable values
	setId("creationSpatialSphere");
	setParameter(paraValue);  
	setVariableIndex(indValue);
  
	// Set the parameter identities
	std::vector<std::string> tmp( numParameter() );
	tmp.resize( numParameter() );
	tmp[0] = "V_max";
	tmp[1] = "R (K_Hill)";
	tmp[2] = "n_Hill";
	tmp[3] = "R_sign";  
	setParameterId( tmp );
	powK_ = std::pow(parameter(1),parameter(2));
}

void CreationSpatialSphere::
derivs(Compartment &compartment,size_t varIndex,DataMatrix &y,DataMatrix &dydt) 
{  
	size_t dimension = compartment.numTopologyVariable()-1;
	double r=0.0;
	
	for (size_t i=0; i<dimension; i++)
	  r += y[compartment.index()][ i ]*y[compartment.index()][ i ];
	r = std::sqrt(r);

	double powR = std::pow(r,parameter(2));
	powK_ = std::pow(parameter(1),parameter(2));
	
	if (parameter(3)>0.0) 
	  dydt[compartment.index()][varIndex] += parameter(0)*powR/(powK_+powR);
	else
	  dydt[compartment.index()][varIndex] += parameter(0)*powK_/(powK_+powR);
}


// To be used in combination with ParabolaSphereWall
// Creates new molecules according to a Hill function in the outer layer of ParabolaSphereWall
// parameters: C, H, R, X0, growth (must be the same as the parameters of ParabolaSphereWall), V, K (width of the outer layer and Hill switch),N (exponent of Hill)
CreationSpatialParabolaSphere::CreationSpatialParabolaSphere(std::vector<double> &paraValue, 
					     std::vector< std::vector<size_t> > 
					     &indValue ) 
{  
	// Do some checks on the parameters and variable indeces
	if( paraValue.size()!=8 ) {
		std::cerr << "CreationSpatialParabolaSphere::CreationSpatialParabolaSphere() "
			  << "Uses eight parameters C, H, R, X0, growth, V, K, N\n";
		exit(0);
	}
	if( indValue.size() != 0 ) {
	  std::cerr << "CreationSpatialParabolaSphere::CreationSpatialParabolaSphere() "
		    << "No variable index needed." << std::endl;
	  exit(0);
	}
	
	// Set the variable values
	setId("creationSpatialParabolaSphere");
	setParameter(paraValue);  
	setVariableIndex(indValue);
  
	// Set the parameter identities
	std::vector<std::string> tmp( numParameter() );
	tmp.resize( numParameter() );
	tmp[0] = "C";
	tmp[1] = "H";
	tmp[2] = "R";
	tmp[3] = "X0";
	tmp[4] = "growth";
	tmp[5] = "V";
	tmp[6] = "K";
	tmp[7] = "N";
}

void CreationSpatialParabolaSphere::
derivs(Compartment &compartment,size_t varIndex,DataMatrix &y,DataMatrix &dydt) 
{  
      size_t dimension = compartment.numTopologyVariable()-1;
      double C = parameter(0);
      double H = parameter(1);
      double R = parameter(2);
      double X0 = parameter(3);
      double growth = parameter(4);
      double V = parameter(5);
      double K = parameter(6);
      double N = parameter(7);
      
      double x_sphere_center;
      double stuff = std::sqrt(4 * C * C * X0 * X0 + 1);
      x_sphere_center = X0-(2*R*C*X0*(1 - growth))/stuff;
      double z_sphere_center;
      z_sphere_center = (- C * X0*X0 - R * (- growth + 1) * (1/stuff)) + H;
      std::vector<double> center(dimension);
      center[1] = 0;
      center[0] = x_sphere_center;
      center[dimension-1] = z_sphere_center;
      double r = 0;
      for( size_t d=0 ; d<dimension ; d++ )
	r += (y[compartment.index()][d] - center[d])*(y[compartment.index()][d] - center[d]);
      r = std::sqrt(r);
      
      double H2 = H - K;
      double C2 = C * (H - K) / (H + K * K * (C + 1 / (4 * H)) - 2 * K * std::sqrt(H * C + 1/4));
      
      double H3 = 0;
      for (size_t d=0; d<dimension -1; d++)
	H3 += y[compartment.index()][d]*y[compartment.index()][d];
      H3 = y[compartment.index()][dimension-1]+C2*H3;
      
      // Hill function with activation out of boundary parabola and activation out of boundary sphere
      dydt[compartment.index()][varIndex] += V * (std::pow(H3,N) / (std::pow(H3,N) + std::pow(H2,N))) * (std::pow(r,N) / (std::pow(r,N) + std::pow(R-K,N)));
      
}

CreationSpatialParabolaSphereGrowth::CreationSpatialParabolaSphereGrowth(std::vector<double> &paraValue, 
					     std::vector< std::vector<size_t> > 
					     &indValue ) 
{  
	// Do some checks on the parameters and variable indeces
	if( paraValue.size()!=9 ) {
		std::cerr << "CreationSpatialParabolaSphereGrowth::CreationSpatialParabolaSphereGrowth() "
			  << "Uses eight parameters C, H, R, X0, size, growth rate, V, K, N\n";
		exit(0);
	}
	if( indValue.size() != 0 ) {
	  std::cerr << "CreationSpatialParabolaSphereGrowth::CreationSpatialParabolaSphereGrowth() "
		    << "No variable index needed." << std::endl;
	  exit(0);
	}
	
	// Set the variable values
	setId("creationSpatialParabolaSphereGrowth");
	setParameter(paraValue);  
	setVariableIndex(indValue);
  
	// Set the parameter identities
	std::vector<std::string> tmp( numParameter() );
	tmp.resize( numParameter() );
	tmp[0] = "C";
	tmp[1] = "H";
	tmp[2] = "R";
	tmp[3] = "X0";
	tmp[4] = "growth init";
	tmp[5] = "growth rate";
	tmp[6] = "V";
	tmp[7] = "K";
	tmp[8] = "N";
}

void CreationSpatialParabolaSphereGrowth::
derivs(Compartment &compartment,size_t varIndex,DataMatrix &y,DataMatrix &dydt) 
{  
      size_t dimension = compartment.numTopologyVariable()-1;
      double C = parameter(0);
      double H = parameter(1);
      double R = parameter(2);
      double X0 = parameter(3);
      double growth = parameter(4);
      double V = parameter(6);
      double K = parameter(7);
      double N = parameter(8);
      
      double x_sphere_center;
      double stuff = std::sqrt(4 * C * C * X0 * X0 + 1);
      x_sphere_center = X0-(2*R*C*X0*(1 - growth))/stuff;
      double z_sphere_center;
      z_sphere_center = (- C * X0*X0 - R * (- growth + 1) * (1/stuff)) + H;
      std::vector<double> center(dimension);
      center[1] = 0;
      center[0] = x_sphere_center;
      center[dimension-1] = z_sphere_center;
      double r = 0;
      for( size_t d=0 ; d<dimension ; d++ )
	r += (y[compartment.index()][d] - center[d])*(y[compartment.index()][d] - center[d]);
      r = std::sqrt(r);
      
      double H2 = H - K;
      double C2 = C * (H - K) / (H + K * K * (C + 1 / (4 * H)) - 2 * K * std::sqrt(H * C + 1/4));
      
      double H3 = 0;
      for (size_t d=0; d<dimension -1; d++)
	H3 += y[compartment.index()][d]*y[compartment.index()][d];
      H3 = y[compartment.index()][dimension-1]+C2*H3;
      
      // Hill function with activation out of boundary parabola and activation out of boundary sphere
      dydt[compartment.index()][varIndex] += V * (std::pow(H3,N) / (std::pow(H3,N) + std::pow(H2,N))) * (std::pow(r,N) / (std::pow(r,N) + std::pow(R-K,N)));
      
}

void CreationSpatialParabolaSphereGrowth::
update(double h, double t, std::vector< std::vector<double> > &y)
{
  if( parameter(4)<1.0)
    setParameter(4,parameter(4)+parameter(5)*h);
  if (parameter(4)>1.0)
    setParameter(4,1.0);
}


CreationPrimordium::CreationPrimordium(std::vector<double> &paraValue, 
					     std::vector< std::vector<size_t> > 
					     &indValue ) 
{  
	// Do some checks on the parameters and variable indeces
  if( paraValue.size()!=10 ) {
    std::cerr << "CreationPrimordium::CreationPrimordium() "
    << "Uses 10 parameters C, H, R, A, G0, G (from PrimordiumGrowth)\n"
    << "and V (max creation rate), K (L1 width) and n (steepness of the creation)\n"
    << "final parameter: E (0/no crease 1/crease)\n";
    exit(0);
  }
  if( indValue.size() != 0 ) {
    std::cerr << "CreationPrimordium::CreationPrimordium() "
    << "No variable index needed." << std::endl;
    exit(0);
  }
  
  // Set the variable values
  setId("creationPrimordium");
  setParameter(paraValue);  
  setVariableIndex(indValue);
  
  // Set the parameter identities
  std::vector<std::string> tmp( numParameter() );
  tmp.resize( numParameter() );
  tmp[0] = "C";
  tmp[1] = "H";
  tmp[2] = "R";
  tmp[3] = "A";
  tmp[4] = "G0";
  tmp[5] = "G";
  tmp[6] = "V";
  tmp[7] = "K";
  tmp[8] = "N";
  tmp[9] = "E";
}

void CreationPrimordium::
derivs(Compartment &compartment,size_t varIndex,DataMatrix &y,DataMatrix &dydt) 
{  
  size_t i=compartment.index();
  double C = parameter(0);
  double H = parameter(1);
  double R = parameter(2);
  double A = parameter(3);
  double G0 = parameter(4);
  double G = parameter(5);
  double V = parameter(6);
  double K = parameter(7);
  double N = parameter(8);
  double E = parameter(9);
  
  // to Hill will be added the contribution of each elements
  double Hill = V;
  
  bool in_parabola = y[i][2] < H - C*(y[i][0]*y[i][0] + y[i][1]*y[i][1]);
  
  double t = (y[i][1]+y[i][2]*A)/(1+A*A);
  double kc = std::sqrt( y[i][0]*y[i][0] + (y[i][1]-t)*(y[i][1]-t) + (y[i][2]- A*t)*(y[i][2]- A*t));
  double ks = std::sqrt( y[i][0]*y[i][0] + (y[i][1]-(G0+G))*(y[i][1]-(G0+G)) + (y[i][2]-((G+G0)*A))*(y[i][2]-((G0+G)*A)) );

  // excludes sphere or cylinder depending on the position of the cell
  if (y[i][2] >= (-(1/A)*y[i][1] + A*(G0+G) + (G0+G)/A)) 
    {kc += R;}
  else
    {ks += R;}

  if (in_parabola){
    // inner parabola
    double h = H - K;
    double c = C * h / (H + K * K * (C + 1 / (4 * H)) - 2 * K * std::sqrt(H * C + 1/4));
    // computes the h3 parameter of a parabola containing the current cell with c as width (z = h3 - c(x**2 + y**2))
    double  h3 = y[i][2]+c*(std::pow(y[i][0],2)+std::pow(y[i][1],2));
    // uses h3 as variable and h as k-parameter of an activating Hill function
    Hill *=  std::pow(h3,N) / (std::pow(h3,N)+std::pow(h,N));
    if (E != 0) {E = Hill;}}
  
  // in cylinder
  if (kc <= R){
    Hill *= std::pow(kc,N) / (std::pow(kc,N)+std::pow(R-K,N));}

  // in sphere
  if (ks <= R){
    Hill *= std::pow(ks,N) / (std::pow(ks,N)+std::pow(R-K,N));}
      
  if (in_parabola && E != 0 && (y[i][2] > A * y[i][1]) ) {
    // double h4 =  y[i][2]+C*(std::pow(y[i][0],2)+std::pow(y[i][1],2));
    // Hill = E * ( std::pow(y[i][2],N)/(std::pow(y[i][2],N) + std::pow(A * y[i][1],N) )) * (std::pow(H,N)/(std::pow(h4,N)+std::pow(h4,N)))
    Hill = E
      ;}
  // Hill function with activation out of boundary parabola and activation out of boundary sphere
  dydt[compartment.index()][varIndex] += Hill;
      
}

void CreationPrimordium::
update(double h, double t, std::vector< std::vector<double> > &y)
{
  setParameter(4,parameter(4)+parameter(5)*h);
}


CreationSpatialSphereCylinder::
CreationSpatialSphereCylinder(std::vector<double> &paraValue, 
			      std::vector< std::vector<size_t> > 
			      &indValue ) 
{  
	// Do some checks on the parameters and variable indices
	if( paraValue.size()!=3 ) {
		std::cerr << "CreationSpatialSphereCylinder::"
			  << "CreationSpatialSphereCylinder() "
			  << "Uses three parameters k_c R and R_sign\n";
		exit(0);
	}
	if( indValue.size() != 0 ) {
		std::cerr << "CreationSpatialSphereCylinder::"
			  << "CreationSpatialSphereCylinder() "
			  << "No variable index needed.\n";
		exit(0);
	}
	//k and R should be nonnegative
	if( paraValue[0] < 0.0 || paraValue[1] < 0.0 ) {
		std::cerr << "CreationSpatialSphereCylinder::"
			  << "CreationSpatialSphereCylinder() "
			  << "k_c and R should be nonnegative\n";
		exit(0);
	}
	//Sign should be -/+1
	if( paraValue[2] != -1 && paraValue[2] != 1 ) {
		std::cerr << "CreationSpatialSphereCylinder::CreationSpatialSphereCylinder() "
			  << "R_sign should be +/-1\n";
		exit(0);
	}

	// Set the variable values
	setId("creationSpatialSphereCylinder");
	setParameter(paraValue);  
	setVariableIndex(indValue);
  
	// Set the parameter identities
	std::vector<std::string> tmp( numParameter() );
	tmp.resize( numParameter() );
	tmp[0] = "k_c";
	tmp[1] = "R";
	tmp[2] = "R_sign";  
	setParameterId( tmp );
}

void CreationSpatialSphereCylinder::
derivs(Compartment &compartment,size_t varIndex,DataMatrix &y,DataMatrix &dydt) 
{  
	size_t dimension = compartment.numTopologyVariable()-1;
	size_t i=compartment.index();
	double r=0.0;
	for(size_t d=0 ; d<dimension-1 ; d++ )
		r += y[i][d]*y[i][d];
	if( y[i][dimension-1]>0.0 )
		r += y[i][dimension-1]*y[i][dimension-1];
	r = std::sqrt(r);
  
	if( r*parameter(2)>parameter(1)*parameter(2) )
		dydt[i][varIndex] += parameter(0);
}

CreationSpatialMax::CreationSpatialMax(std::vector<double> &paraValue, 
				       std::vector< std::vector<size_t> > 
				       &indValue ) 
{  
	// Do some checks on the parameters and variable indeces
	if( paraValue.size()!=2 ) {
		std::cerr << "CreationSpatialMax::CreationSpatialMax() "
			  << "Uses two parameters k_c X_sign\n";
		exit(0);
	}
	if( indValue.size() != 1 || indValue[0].size() != 1 ) {
		std::cerr << "CreationSpatialMax::CreationSpatialMax()"
			  << "Variable index indicating max/min variable needed.\n";
		exit(0);
	}
	//Sign should be -/+1
	if( paraValue[1] != -1 && paraValue[1] != 1 ) {
		std::cerr << "CreationSpatialMax::CreationSpatialMax() "
			  << "X_sign should be +/-1\n";
		exit(0);
	}
	
	// Set the variable values
	setId("creationSpatialMax");
	setParameter(paraValue);  
	setVariableIndex(indValue);
  
	// Set the parameter identities
	std::vector<std::string> tmp( numParameter() );
	tmp.resize( numParameter() );
	tmp[0] = "k_c";
	tmp[1] = "X_sign";
	setParameterId( tmp );
}

void CreationSpatialMax::
derivs(Compartment &compartment,size_t varIndex,DataMatrix &y,DataMatrix &dydt) 
{
	if( compartment.index()>0 )
		return;
	
	double minmax=y[0][variableIndex(0,0)];
	size_t minmaxI=0;
	
	if( parameter(1)==1 ) {
		for( size_t i=0 ; i<y.size() ; ++i ) {
			if( y[i][variableIndex(0,0)]>minmax ) {
				minmax=y[i][variableIndex(0,0)];
				minmaxI=i;					
			}
		}
	}
	else if( parameter(1) ==-1 ) {
		for( size_t i=0 ; i<y.size() ; ++i ) {
			if( y[i][variableIndex(0,0)]<minmax ) {
				minmax=y[i][variableIndex(0,0)];
				minmaxI=i;								
			}
		}
	}
	else {
		std::cerr << "CreationSpatialMax::derivs() Wrong value for p_1"
			  << std::endl;
		exit(-1);
	}
	dydt[minmaxI][varIndex] += parameter(0);
}

CreationExponent::CreationExponent(std::vector<double> &paraValue, 
				   std::vector< std::vector<size_t> > 
				   &indValue ) 
{  
	// Do some checks on the parameters and variable indeces
	if( paraValue.size()!=2 ) {
		std::cerr << "CreationExponent::CreationExponent() "
			  << "Uses two parameters k_c and k_e\n";
		exit(0);
	}
	if( indValue.size() != 1 || indValue[0].size() != 2) {
		std::cerr << "CreationExponent::CreationExponent() "
			  << "Uses two variable indices.\n";
		exit(0);
	}
	
	// Set the variable values
	setId("creationExponent");
	setParameter(paraValue);  
	setVariableIndex(indValue);
  
	// Set the parameter identities
	std::vector<std::string> tmp( numParameter() );
	tmp.resize( numParameter() );
	tmp[0] = "k_c";
	tmp[0] = "k_e";
	setParameterId( tmp );
}

void CreationExponent::
derivs(Compartment &compartment,size_t varIndex,DataMatrix &y,DataMatrix &dydt) 
{  
	dydt[compartment.index()][varIndex] += parameter(0) * std::pow(
		y[compartment.index()][ variableIndex(0,0) ],
		parameter(1)*y[compartment.index()][ variableIndex(0,1) ] );
}

Constant::Constant(std::vector<double> &paraValue, 
		   std::vector< std::vector<size_t> > 
		   &indValue ) 
{  
	// Do some checks on the parameters and variable indeces
	if( paraValue.size()!=1 ) {
		std::cerr << "Constant::Constant() "
			  << "Use one parameter, value\n";
		exit(0);
	}
	if( indValue.size() != 1 || indValue[0].size() != 1  ) {
		std::cerr << "Constant::Constant() "
			  << "One variable index needed.\n";
		exit(0);
	}
  
	
	// Set the variable values
	setId("constant");
	setParameter(paraValue);  
	setVariableIndex(indValue);
  
	// Set the parameter identities
	std::vector<std::string> tmp( numParameter() );
	tmp.resize( numParameter() );
	tmp[0] = "value";
	setParameterId( tmp );
}

void Constant::
derivs(Compartment &compartment,size_t varIndex,DataMatrix &y,DataMatrix &dydt ) 
{ 
	if (y[compartment.index()][ variableIndex(0,0) ] )
		y[compartment.index()][ varIndex ]=parameter(0);
}

CreationNeighbor::CreationNeighbor(std::vector<double> &paraValue, 
				   std::vector< std::vector<size_t> > 
				   &indValue ) 
{  
	// Do some checks on the parameters and variable indeces
	if( paraValue.size()!=1 ) {
		std::cerr << "CreationNeighbor::CreationNeighbor() "
			  << "Uses only one parameter k_c\n";
		exit(0);
	}
	if( indValue.size() != 2 ) {
		std::cerr << "CreationNeighbor::CreationNeighbor()"
			  << "Two levels of variables are used, the first with membrane variables, "
			  << "the second with neighbor variable.\n";
		exit(0);
	}
	
	// Set the variable values
	setId("creationNeighbor");
	setParameter(paraValue);  
	setVariableIndex(indValue);
  
	// Set the parameter identities
	std::vector<std::string> tmp( numParameter() );
	tmp.resize( numParameter() );
	tmp[0] = "k_c";
	setParameterId( tmp );
}

void CreationNeighbor::
derivs(Compartment &compartment,size_t varIndex,DataMatrix &y,DataMatrix &dydt) 
{  
	if (numVariableIndex(0)) {
		if (numVariableIndex(0)>compartment.numNeighbor()) {
			std::cerr << "CreationNeighbor:::derivs() "
				  << "Wrong number of neighbors!" << std::endl;
			std::cerr << "cell " << compartment.index() << " numVariableIndex " << numVariableIndex(0)
				  << " numNeighbor " << compartment.numNeighbor() << std::endl; 
			exit(-1);
		}
		for( size_t k=0; k<numVariableIndex(0); ++k ) {
			//std::cerr << "cell " << compartment.index() << " variable " << variableIndex(0,k) 
			//				<< " updated from neighbor " << compartment.neighbor(k) << " var " 
			//				<< variableIndex(1,0) << std::endl;
			dydt[compartment.index()][variableIndex(0,k)] += parameter(0)*y[compartment.neighbor(k)][variableIndex(1,0)]*y[compartment.neighbor(k)][variableIndex(1,0)];
		}
	}
}


CreationSinus::CreationSinus(std::vector<double> &paraValue, 
														 std::vector< std::vector<size_t> > 
														 &indValue ) 
{  
	// Do some checks on the parameters and variable indeces
	if( paraValue.size()!=3 ) {
		std::cerr << "CreationSinus::CreationSinus() "
							<< "Uses three parameters amplitude, period, and phase." << std::endl;
		exit(0);
	}
	if( indValue.size() != 0 ) {
	  std::cerr << "CreationSinus::CreationSinus() "
							<< "No variable index needed." << std::endl;
	  exit(0);
	}
	
	// Set the variable values
	setId("creationSinus");
	setParameter(paraValue);  
	setVariableIndex(indValue);
  
	// Set the parameter identities
	std::vector<std::string> tmp( numParameter() );
	tmp.resize( numParameter() );
	tmp[0] = "amplitude";
	tmp[1] = "period";
	tmp[2] = "phase";
}

void CreationSinus::
derivs(Compartment &compartment,size_t varIndex,DataMatrix &y,DataMatrix &dydt) 
{  
	dydt[compartment.index()][varIndex] += parameter(0)*
		( 1.0 + std::sin(6.28*(time_/parameter(1) + parameter(2) ) ) );
      
}

void CreationSinus::
update(double h, double t, std::vector< std::vector<double> > &y)
{
	time_=t;
}

void CreationSinus::
initiate(double t, std::vector< std::vector<double> > &y)
{
	time_=t;
}



ConstantInterval::ConstantInterval(std::vector<double> &paraValue, 
		   std::vector< std::vector<size_t> > 
		   &indValue ) 
{  
	// Do some checks on the parameters and variable indeces
	if( paraValue.size()==0 || (paraValue.size()%2)!=0) {
		std::cerr << "ConstantInterval::ConstantInterval() "
			  << "Parameters must come in pairs.\n";
		exit(0);
	}
	if( indValue.size() != 0 && indValue.size() != 1 ) {
		std::cerr << "ConstantInterval::ConstantInterval() "
			  << "At most one level of variables are allowed.\n";
		exit(0);
	}
	if (indValue.size())
		if( indValue[0].size() != paraValue.size()/2 ) {
			std::cerr << "ConstantInterval::ConstantInterval() "
				<< "Wrong number of concentrations specified.\n"
				<< "Either specify 0 concentrations, or one for each time interval\n";
			exit(0);
		}
  
	
	// Set the variable values
	setId("constantInterval");
	setParameter(paraValue);  
	setVariableIndex(indValue);
  
	// Set the parameter identities
	std::vector<std::string> tmp( numParameter() );
	tmp.resize( numParameter() );
	tmp[0] = "value";
	setParameterId( tmp );
}

void ConstantInterval::
derivs(Compartment &compartment,size_t varIndex,DataMatrix &y,DataMatrix &dydt ) 
{
	size_t currentTimeIndex = 0;
	size_t currentConcentrationIndex = 0;
	size_t valueIndex = 0;
	double value = 0;
	bool updateConcentration = false;
	double tiny = 1e-30;
//	bool updateConcentration = false;

	// Find which time interval we should be in at the current time
	//for (size_t timeIndex = 0; timeIndex < numTimeSteps; ++timeIndex) {
	for (size_t timeIndex = 0; timeIndex < numParameter(); timeIndex += 2) {
		if ( 
				parameter(timeIndex) <= time_ - tiny 
				&&
			   	parameter(timeIndex) >= parameter(currentTimeIndex)
		   ){
			currentTimeIndex=timeIndex;
			updateConcentration = true;
		}
	}

	// if the solver time lies within a specified interval, update
	if (updateConcentration) {
		valueIndex = currentTimeIndex+1; 
		currentConcentrationIndex = currentTimeIndex / 2;
		value = parameter(valueIndex);

		if ( numVariableIndexLevel() )
			value *= y[compartment.index()]
			          [variableIndex(0,currentConcentrationIndex)];

		y[compartment.index()][varIndex] = value; 
		dydt[compartment.index()][varIndex] = 0;
	}
}

void ConstantInterval::
update(double h, double t, std::vector< std::vector<double> > &y)
{
	// make time accessible for the derivs function
	time_ = t;
}

void ConstantInterval::
initiate(double t, std::vector< std::vector<double> > &y)
{
	// make time accessible for the derivs function
	time_=t;
}
