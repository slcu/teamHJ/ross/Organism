//
// Filename     : grn.cc
// Description  : Classes describing gene regulatory updates
// Author(s)    : Henrik Jonsson (henrik@thep.lu.se)
// Created      : October 2003
// Revision     : $Id: grn.cc 669 2016-05-12 08:53:12Z korsbo $
//

#include"baseReaction.h"
#include"grn.h"
#include"../organism.h"

Grn::Grn(std::vector<double> &paraValue, 
				 std::vector< std::vector<size_t> > 
				 &indValue ) 
{
  // Do some checks on the parameters and variable indeces
  //
  if( paraValue.size()<2 ) {
    std::cerr << "Grn::Grn() At least two parameters (h,tau) must be given.\n";
    exit(0);
  }
  if( indValue.size()>1 ) {
    std::cerr << "Grn::Grn() "
							<< "At most one level of variable index can be used.\n";
    exit(0);
  }
  if( ( indValue.size() && paraValue.size() != indValue[0].size()+2 ) ||
      ( indValue.size()==0 && paraValue.size() != 2 ) ) {
    std::cerr << "Grn::Grn() "
							<< "Wrong number of parameters/variable indeces given."
							<< std::endl;
		
    std::cerr << paraValue.size() << " " << indValue.size();
    if( indValue.size() )
      std::cerr << " " << indValue[0].size();
    std::cerr << "\n";
    exit(0);
  }
  
  // Set the variable values
  //
  setId("grn");
  setParameter(paraValue);  
  setVariableIndex(indValue);
  
  // Set the parameter identities
  //
  std::vector<std::string> tmp( numParameter() );
  tmp.resize( numParameter() );
  tmp[0] = "h";
  tmp[1] = "tau";
  for(size_t i=2; i<numParameter(); i++ )
    tmp[i] = "T";
  setParameterId( tmp );
}

void Grn::derivs(Compartment &compartment,size_t species,
		 DataMatrix &y,
		 DataMatrix &dydt ) 
{
  //Threshold
  double u = parameter(0);//h
  
  // Internal contribution
  size_t add=2;
  if( numVariableIndexLevel() )
    for(size_t b=0; b<numVariableIndex(0); b++ )
      u += parameter(b + add)*y[compartment.index()][variableIndex(0,b)];
  
  // Apply sigmoid and tau parameter
  if(parameter(1)>0.)
    dydt[compartment.index()][species] += sigmoid(u)/parameter(1);
  else {
    std::cerr << "Grn::derivs Division by tau=0." << std::endl;
    exit(-1);
  }
}

void Grn::derivsWithAbs(Compartment &compartment,size_t species,
			DataMatrix &y,
			DataMatrix &dydt,
			DataMatrix &sdydt ) 
{
  //Threshold
  double u = parameter(0);//h
  
  // Internal contribution
  size_t add=2;
  if( numVariableIndexLevel() )
    for(size_t b=0; b<numVariableIndex(0); b++ )
      u += parameter(b + add)*y[compartment.index()][variableIndex(0,b)];
  
  // Apply sigmoid and tau parameter
  if(parameter(1)>0.) {
    double rate = sigmoid(u)/parameter(1);
    dydt[compartment.index()][species] += rate;
    sdydt[compartment.index()][species] += rate;
  }
  else {
    std::cerr << "Grn::derivs Division by tau=0." << std::endl;
    exit(-1);
  }
}

void Grn::printCambium( std::ostream &os, size_t varIndex ) const
{
  std::string varName=organism()->variableId(varIndex);
  os << "Arrow[Organism[" << id() << "],{},{";
  for (size_t i=0; i<numVariableIndex(0); i++) {
    if (i!=0) {
      os << ",";
    }
    std::string varNameCre=organism()->variableId(variableIndex(0,i));
    os << varNameCre << "[i]";
  }
  os << "},{"
     << varName
     << "[i]}, Parameters[";
  for (size_t i=0; i<numParameter(); i++) {
    if (i!=0) {
      os << ",";
    }
    os << parameter(i);
  }
  os << "], ParameterNames[";
  for (size_t i=0; i<numParameter(); i++) {
    if (i!=0) {
      os << ",";
    }
    os << parameterId(i);
  }
  os << "], VarIndices[{";
  for (size_t i=0; i<numVariableIndex(0); i++) {
    if (i!=0) {
      os << ",";
    }
    os << variableIndex(0,i);
  }
  os << "}],"; 
  os << "Solve{" << varName << "[i]\' = (1./p_1) sigmoid( p_0 +";
  for (size_t i=0; i<numVariableIndex(0); i++) {
    std::string varNameCre=organism()->variableId(variableIndex(0,i));
    os << "p_" << i+2 << " " << varNameCre << "[i] + ";
  }
  os << "} Where{sigmoid(x) = 0.5 (1 + x / sqrt( 1 + x x) )} ";
  os << "Using[" << organism()->topology().id() << "::Cell, i]}";
  os << "]" << std::endl;
}

Gsrn::Gsrn(std::vector<double> &paraValue, 
					 std::vector< std::vector<size_t> > 
					 &indValue ) 
{
  // Do some checks on the parameters and variable indeces
  //
  if( paraValue.size()<2 ) {
    std::cerr << "Gsrn::Gsrn() "
	      << "At least two parameters (h,tau) must be given." << std::endl;
    exit(0);
  }
  if( indValue.size()>2 ) {
    std::cerr << "Gsrn::Gsrn() "
	      << "At most two levels of variable indeces can be used." << std::endl;
    exit(0);
  }
  if( ( indValue.size()==2 && 
	paraValue.size() != indValue[0].size()+indValue[1].size()+2 ) ||
      ( indValue.size()==1 && paraValue.size() != indValue[0].size()+2 ) ||
      ( indValue.size()==0 && paraValue.size() != 2 )
      ) {
    std::cerr << "Gsrn::Gsrn() "
	      << "Wrong number of parameters/variable indeces given.\n";
    exit(0);
  }
  
  // Set the variable values
  //
  setId("gsrn");
  setParameter(paraValue);  
  setVariableIndex(indValue);
  
  // Set the parameter identities
  //
  std::vector<std::string> tmp( numParameter() );
  tmp[0] = "h";
  tmp[1] = "tau";
  size_t add=2;
  for( size_t i=0 ; i<numVariableIndex(0) ; i++ )
    tmp[i+add] = "T";
  if( indValue.size()>1 ) {
    add = 2+numVariableIndex(0);
    for( size_t i=0 ; i<numVariableIndex(1) ; i++ )
      tmp[i+add] = "T_hat";
  }
  setParameterId( tmp );
}

void Gsrn::derivs(Compartment &compartment,size_t species,
		  DataMatrix &y,
		  DataMatrix &dydt ) 
{
  // Threshold
  double u = parameter(0);//h
  
  // Internal contribution
  size_t addIndex=2;//avoiding h and tau
  if( numVariableIndexLevel() )
    for(size_t b=0 ; b<numVariableIndex(0) ; b++ )
      u += parameter(addIndex+b)*y[compartment.index()][variableIndex(0,b)];
  
  // Neighbor contributions (direct)
  if( numVariableIndexLevel()>1 ) {
    addIndex += numVariableIndex(0);//after local T parameters
    for( size_t n=0 ; n<compartment.numNeighbor() ; n++ )
      for(size_t b=0 ; b<numVariableIndex(1) ; b++ )
	u += parameter(addIndex+b)
	  *y[compartment.neighbor(n)][variableIndex(1,b)];
  }
  
  // Apply sigmoid and tau parameter
  if( parameter(1)>0. )
    dydt[compartment.index()][species] += sigmoid(u)/parameter(1);
  else {
    std::cerr << "Gsrn::derivs Division by tau=0.\n";
    exit(-1);
  }
}

void Gsrn::derivsWithAbs(Compartment &compartment,size_t species,
			 DataMatrix &y,
			 DataMatrix &dydt,
			 DataMatrix &sdydt ) 
{
  // Threshold
  double u = parameter(0);//h
  
  // Internal contribution
  size_t addIndex=2;//avoiding h and tau
  if( numVariableIndexLevel() )
    for(size_t b=0 ; b<numVariableIndex(0) ; b++ )
      u += parameter(addIndex+b)*y[compartment.index()][variableIndex(0,b)];
  
  // Neighbor contributions (direct)
  if( numVariableIndexLevel()>1 ) {
    addIndex += numVariableIndex(0);//after local T parameters
    for( size_t n=0 ; n<compartment.numNeighbor() ; n++ )
      for(size_t b=0 ; b<numVariableIndex(1) ; b++ )
	u += parameter(addIndex+b)
	  *y[compartment.neighbor(n)][variableIndex(1,b)];
  }
  
  // Apply sigmoid and tau parameter
  if( parameter(1)>0. ) {
    double rate = sigmoid(u)/parameter(1);
      dydt[compartment.index()][species] += rate;
      sdydt[compartment.index()][species] += rate;
  }
  else {
    std::cerr << "Gsrn::derivs Division by tau=0.\n";
    exit(-1);
  }
}

Gsrn2::Gsrn2(std::vector<double> &paraValue, 
	     std::vector< std::vector<size_t> > 
	     &indValue ) 
{  
  // Do some checks on the parameters and variable indeces
  //
  if( paraValue.size()<2 ) {
    std::cerr << "Grns2::Grns2() "
	      << "At least two parameters (h,tau) must be given.\n";
    exit(0);
  }
  if( indValue.size()>3 ) {
    std::cerr << "Grns2::Grns2() "
	      << "At most three levels of variable indeces can be used.\n";
    exit(0);
  }
  if( 
     ( indValue.size()==3 && 
       paraValue.size() != indValue[0].size()+indValue[1].size()+
       0.5*indValue[2].size()+2 ) ||
     ( indValue.size()==2 && 
       paraValue.size() != indValue[0].size()+indValue[1].size()+2 ) ||
     ( indValue.size()==1 && paraValue.size() != indValue[0].size()+2 ) ||
     ( indValue.size()==0 && paraValue.size() != 2 ) 
      ) {
    std::cerr << "Grns2::Grns2() "
	      << "Wrong number of parameters/variable indeces given.\n";
    std::cerr << paraValue.size() << " " << indValue.size() << " ";
    for( size_t i=0 ; i<indValue.size() ; i++ )
      std::cerr << indValue[i].size() << " ";
    std::cerr << "\n";
    exit(0);
  }
  if( indValue.size()==3 && indValue[2].size()%2 ) {
    std::cerr << "Grns2::Grns2() "
	      << "Ligand/Receptor indeces must come in pairs.\n";
    exit(0);
  }
  
  // Set the variable values
  //
  setId("gsrn2");
  setParameter(paraValue);  
  setVariableIndex(indValue);
	
  // Set the parameter identities
  //
  std::vector<std::string> tmp( numParameter() );
  tmp[0] = "h";
  tmp[1] = "tau";
  size_t add=2;
  for( size_t i=0 ; i<numVariableIndex(0) ; i++ )
    tmp[i+add] = "T";
  if( indValue.size()>1 ) {
    add += numVariableIndex(0);
    for( size_t i=0 ; i<numVariableIndex(1) ; i++ )
      tmp[i+add] = "T_hat";
  }
  if( indValue.size()>1 ) {
    add += numVariableIndex(1);
    for( size_t i=add ; i<tmp.size() ; i++ )
      tmp[i] = "T_T";
  }
  setParameterId( tmp );
}

void Gsrn2::derivs(Compartment &compartment,size_t species,
		   DataMatrix &y,
		   DataMatrix &dydt ) 
{
  // Threshold
  double u = parameter(0);//h
  
  size_t addIndex=2;//avoiding h and tau
  // Internal contribution
  if( numVariableIndexLevel() )
    for( size_t b=0 ; b<numVariableIndex(0) ; b++ )
      u += parameter(addIndex+b)*y[compartment.index()][variableIndex(0,b)];
  
  // Neighbor contributions (direct)
  if( numVariableIndexLevel()>1 ) {
    addIndex += numVariableIndex(0);//after local T parameters
    for( size_t n=0 ; n<compartment.numNeighbor() ; n++ )
      for( size_t b=0 ; b<numVariableIndex(1) ; b++ )
	u += parameter(addIndex+b)
	  *y[compartment.neighbor(n)][variableIndex(1,b)];
  }
  // Neighbor contributions ligand receptor version
  if( numVariableIndexLevel()>2 ) {
    addIndex += numVariableIndex(1);//after first neighbor T parameters
    for( size_t n=0 ; n<compartment.numNeighbor() ; n++ ) {
      size_t parIndexCount=0;
      for( size_t b=0 ; b<numVariableIndex(2) ; b+=2 )
	u += parameter(addIndex+parIndexCount++)
	  *y[compartment.index()][variableIndex(2,b)]
	  *y[compartment.neighbor(n)][variableIndex(2,b+1)];
    }
  }
  // Apply sigmoid and tau parameter
  if( parameter(1)>0. )
    dydt[compartment.index()][species] += sigmoid(u)/parameter(1);
  else {
    std::cerr << "Gsrn2::derivs Division by tau=0.\n";
    exit(-1);
  }
}

void Gsrn2::derivsWithAbs(Compartment &compartment,size_t species,
			  DataMatrix &y,
			  DataMatrix &dydt,
			  DataMatrix &sdydt ) 
{
  // Threshold
  double u = parameter(0);//h
  
  size_t addIndex=2;//avoiding h and tau
  // Internal contribution
  if( numVariableIndexLevel() )
    for( size_t b=0 ; b<numVariableIndex(0) ; b++ )
      u += parameter(addIndex+b)*y[compartment.index()][variableIndex(0,b)];
  
  // Neighbor contributions (direct)
  if( numVariableIndexLevel()>1 ) {
    addIndex += numVariableIndex(0);//after local T parameters
    for( size_t n=0 ; n<compartment.numNeighbor() ; n++ )
      for( size_t b=0 ; b<numVariableIndex(1) ; b++ )
	u += parameter(addIndex+b)
	  *y[compartment.neighbor(n)][variableIndex(1,b)];
  }
  // Neighbor contributions ligand receptor version
  if( numVariableIndexLevel()>2 ) {
    addIndex += numVariableIndex(1);//after first neighbor T parameters
    for( size_t n=0 ; n<compartment.numNeighbor() ; n++ ) {
      size_t parIndexCount=0;
      for( size_t b=0 ; b<numVariableIndex(2) ; b+=2 )
	u += parameter(addIndex+parIndexCount++)
	  *y[compartment.index()][variableIndex(2,b)]
	  *y[compartment.neighbor(n)][variableIndex(2,b+1)];
    }
  }
  // Apply sigmoid and tau parameter
  if( parameter(1)>0. ) {
    double rate = sigmoid(u)/parameter(1);
      dydt[compartment.index()][species] += rate;
      sdydt[compartment.index()][species] += rate;
  }
  else {
    std::cerr << "Gsrn2::derivs Division by tau=0.\n";
    exit(-1);
  }
}

MichaelisMenten::MichaelisMenten(std::vector<double> &paraValue, 
				 std::vector< std::vector<size_t> > 
				 &indValue ) 
{	
  // Do some checks on the parameters and variable indeces
  //
  if( paraValue.size() !=2 ) {
    std::cerr << "MichaelisMenten::MichaelisMenten() "
	      << "Two parameters (v_max,K_M) must be given.\n";
    exit(0);
  }
  if( indValue.size() != 1 ) {
    std::cerr << "MichaelisMenten::MichaelisMenten() "
	      << "One level of variable index is used.\n";
    exit(0);
  }
  if( indValue.size() && indValue[0].size() != 1 ) {
    std::cerr << "MichaelisMenten::MichaelisMenten() "
	      << "One variable index used.\n";
    exit(0);
  }
  
  // Set the variable values
  //
  setId("michaelisMenten");
  setParameter(paraValue);  
  setVariableIndex(indValue);
  
  // Set the parameter identities
  //
  std::vector<std::string> tmp( numParameter() );
  tmp.resize( numParameter() );
  tmp[0] = "v_max";
  tmp[1] = "K_M";
  setParameterId( tmp );
}

void MichaelisMenten::derivs(Compartment &compartment,size_t species,
			     DataMatrix &y,
			     DataMatrix &dydt ) 
{	
  dydt[compartment.index()][species] += parameter(0)*
    y[compartment.index()][variableIndex(0,0)]/ 
    ( parameter(1) + y[compartment.index()][variableIndex(0,0)] );
}

void MichaelisMenten::derivsWithAbs(Compartment &compartment,size_t species,
				    DataMatrix &y,
				    DataMatrix &dydt,
				    DataMatrix &sdydt ) 
{	
  double rate = parameter(0)*y[compartment.index()][variableIndex(0,0)]/ 
    ( parameter(1) + y[compartment.index()][variableIndex(0,0)] );
  dydt[compartment.index()][species] += rate;
  sdydt[compartment.index()][species] += rate;
}

double MichaelisMenten::
propensity(Compartment &compartment,size_t species,DataMatrix &y)
{
  return parameter(0)*y[compartment.index()][variableIndex(0,0)]/ 
    ( parameter(1) + y[compartment.index()][variableIndex(0,0)] );
}

void MichaelisMenten::
discreteUpdate(Compartment &compartment,size_t species,DataMatrix &y)
{
  y[compartment.index()][species] += 1.0;
}

MichaelisMentenRestricted::MichaelisMentenRestricted(std::vector<double> 
						     &paraValue,
						     std::vector< 
						       std::vector<size_t> >
						     &indValue ) 
{  
  // Do some checks on the parameters and variable indeces
  //
  if( paraValue.size() !=2 ) {
    std::cerr << "MichaelisMentenRestricted::MichaelisMentenRestricted() "
	      << "Two parameters (v_max,K_M) must be given.\n";
    exit(0);
  }
  if( indValue.size() != 1 ) {
    std::cerr << "MichaelisMentenRestricted::MichaelisMentenRestricted() "
	      << "One level of variable index is used.\n";
    exit(0);
	}
  if( indValue.size() && indValue[0].size() < 2 ) {
    std::cerr << "MichaelisMentenRetsricted::MichaelisMentenRestricted() "
	      << "At least two variable indices must  used,"
	      << "the fist is the normal enzyme concentration and"
	      << " the others are restriction variables.\n";
    exit(0);
  }
  
  // Set the variable values
  //
  setId("michaelisMentenRestricted");
  setParameter(paraValue);
  setVariableIndex(indValue);
  
  // Set the parameter identities
  //
  std::vector<std::string> tmp( numParameter() );
  tmp.resize( numParameter() );
  tmp[0] = "v_max";
  tmp[1] = "K_M";
  setParameterId( tmp );
}

void MichaelisMentenRestricted::derivs(Compartment &compartment,size_t species,
				       DataMatrix &y,
				       DataMatrix 
				       &dydt ) 
{	
  double factor = parameter(0);
  
  for(size_t i=0; i<numVariableIndex(0); ++i)
    factor *=y[compartment.index()][variableIndex(0,i)];
  
  dydt[compartment.index()][species] += 
    factor /(parameter(1) + y[compartment.index()][variableIndex(0,0)]);
}

void MichaelisMentenRestricted::derivsWithAbs(Compartment &compartment,size_t species,
					      DataMatrix &y,
					      DataMatrix &dydt,
					      DataMatrix &sdydt ) 
{	
  double factor = parameter(0);
  
  for(size_t i=0; i<numVariableIndex(0); ++i)
    factor *=y[compartment.index()][variableIndex(0,i)];
  
  double rate = factor /(parameter(1) + y[compartment.index()][variableIndex(0,0)]);
  dydt[compartment.index()][species] += rate;
  sdydt[compartment.index()][species] += rate;    
}

Hill::Hill(std::vector<double> &paraValue, 
	   std::vector< std::vector<size_t> > 
	   &indValue ) {
  
  // Do some checks on the parameters and variable indeces
  //
  if( indValue.size() != 2 ) {
    std::cerr << "Hill::Hill() "
	      << "Two levels of variable index is used, "
	      << "activators and repressors.\n";
    exit(0);
  }
  if( 1 + 2*(indValue[0].size()+indValue[1].size()) != paraValue.size() ) {
    std::cerr << "Hill::Hill() "
	      << "Number of parameters does not agree with number of "
	      << "activators/repressors.\n"
	      << indValue[0].size() << " activators, " 
	      << indValue[1].size() << " repressors, and "
	      << paraValue.size() << " parameters\n"
 	      << "One plus pairs of parameters (V_max,K_half1,n_Hill1,"
 	      << "K2,n2,...) must be given.\n";
    exit(0);
  }
  
  // Set the variable values
  //
  setId("hill");
  setParameter(paraValue);  
  setVariableIndex(indValue);
  
  // Set the parameter identities
  //
  std::vector<std::string> tmp( numParameter() );
  tmp.resize( numParameter() );
  tmp[0] = "V_max";
  tmp[1] = "K_half";
  tmp[2] = "n_Hill";
  setParameterId( tmp );
}

void Hill::
derivs(Compartment &compartment,size_t species,DataMatrix &y,DataMatrix &dydt) 
{
  double contribution=parameter(0);
  size_t parameterIndex=1;
  // Activator contributions
  for( size_t i=0 ; i<numVariableIndex(0) ; i++ ) {
    double c = std::pow(y[compartment.index()][variableIndex(0,i)],
			parameter(parameterIndex+1));
    contribution *= c
      / ( std::pow(parameter(parameterIndex),parameter(parameterIndex+1)) + c );
    parameterIndex+=2;
  }
  // Repressor contributions
  for( size_t i=0 ; i<numVariableIndex(1) ; i++ ) {
    double c = std::pow(parameter(parameterIndex),parameter(parameterIndex+1));
    contribution *= c /
      ( c + std::pow(y[compartment.index()][variableIndex(1,i)],
		     parameter(parameterIndex+1)) );
    parameterIndex+=2;
  }   
  dydt[compartment.index()][species] += contribution; 
}

void Hill::
derivsWithAbs(Compartment &compartment,size_t species,DataMatrix &y,DataMatrix &dydt,DataMatrix &sdydt) 
{
  double contribution=parameter(0);
  size_t parameterIndex=1;
  // Activator contributions
  for( size_t i=0 ; i<numVariableIndex(0) ; i++ ) {
    double c = std::pow(y[compartment.index()][variableIndex(0,i)],
			parameter(parameterIndex+1));
    contribution *= c
      / ( std::pow(parameter(parameterIndex),parameter(parameterIndex+1)) + c );
    parameterIndex+=2;
  }
  // Repressor contributions
  for( size_t i=0 ; i<numVariableIndex(1) ; i++ ) {
    double c = std::pow(parameter(parameterIndex),parameter(parameterIndex+1));
    contribution *= c /
      ( c + std::pow(y[compartment.index()][variableIndex(1,i)],
		     parameter(parameterIndex+1)) );
    parameterIndex+=2;
  }   
  dydt[compartment.index()][species] += contribution; 
  sdydt[compartment.index()][species] += contribution; 
}

size_t Hill::
Jacobian(Compartment &compartment,size_t species,DataMatrix &y,JacobianMatrix &A) 
{
  double contribution=parameter(0);
  size_t parameterIndex=1;
  // Activator contributions
  for (size_t k=0; k<numVariableIndex(0); ++k) {
    double c = std::pow(y[compartment.index()][variableIndex(0,k)],
			parameter(parameterIndex+1));
    contribution *= c / ( std::pow(parameter(parameterIndex),parameter(parameterIndex+1)) + c );
    parameterIndex+=2;
  }
  // Repressor contributions
  for (size_t k=0; k<numVariableIndex(1); ++k) {
    double c = std::pow(parameter(parameterIndex),parameter(parameterIndex+1));
    contribution *= c / 
      ( c + std::pow(y[compartment.index()][variableIndex(1,k)],parameter(parameterIndex+1)) );
    parameterIndex+=2;
  }   
  if (contribution<=0.0)
    return 1;
  //
  // Adding into A 
  //
  size_t i = compartment.index()*y[0].size() + species; 
  parameterIndex = 1;
  // Activator contributions
  for (size_t k=0; k<numVariableIndex(0); ++k) {
    size_t j = compartment.index()*y[0].size() + variableIndex(0,k); 
    double c = std::pow(parameter(parameterIndex),
			parameter(parameterIndex+1));
    double cc = c + std::pow(y[compartment.index()][variableIndex(0,k)],
			     parameter(parameterIndex+1));
    A(i,j) += contribution*c*parameter(parameterIndex+1) / 
      (cc*y[compartment.index()][variableIndex(0,k)]);
    parameterIndex+=2;
  }
  // Repressor contributions
  for (size_t k=0; k<numVariableIndex(1); ++k) {
    size_t j = compartment.index()*y[0].size() + variableIndex(1,k); 
    double c = std::pow(parameter(parameterIndex),
			parameter(parameterIndex+1));
    double cc = c + std::pow(y[compartment.index()][variableIndex(1,k)],
				parameter(parameterIndex+1));
    A(i,j) -= contribution*parameter(parameterIndex+1)*
      std::pow(y[compartment.index()][variableIndex(1,k)],parameter(parameterIndex+1)-1) / (cc);
    parameterIndex+=2;
  }
  return 1;
}

double Hill::
propensity(Compartment &compartment,size_t varIndex,DataMatrix &y)
{
  double contribution=parameter(0);
  size_t parameterIndex=1;
  // Activator contributions
  for( size_t i=0 ; i<numVariableIndex(0) ; i++ ) {
    double c = std::pow(y[compartment.index()][variableIndex(0,i)],
			parameter(parameterIndex+1));
    contribution *= c
      / ( std::pow(parameter(parameterIndex),parameter(parameterIndex+1)) + c );
    parameterIndex+=2;
  }
  // Repressor contributions
  for( size_t i=0 ; i<numVariableIndex(1) ; i++ ) {
    double c = std::pow(parameter(parameterIndex),parameter(parameterIndex+1));
    contribution *= c /
      ( c + std::pow(y[compartment.index()][variableIndex(1,i)],
		     parameter(parameterIndex+1)) );
    parameterIndex+=2;
  }   
  return contribution; 
}

void Hill::
discreteUpdate(Compartment &compartment,size_t species,DataMatrix &y)
{
  y[compartment.index()][species] += 1.0;
}

HillRestricted::HillRestricted(std::vector<double> &paraValue,
	   std::vector< std::vector<size_t> >
	   &indValue ) {

  // Do some checks on the parameters and variable indeces
  //
  if( indValue.size() != 3 ) {
    std::cerr << "HillRestricted::HillRestricted() "
	      << "Three levels of variable index is used, "
	      << "restricting variable, activators and repressors.\n";
    exit(EXIT_FAILURE);
  }
  if( 1 + 2*(indValue[2].size()+indValue[1].size()) != paraValue.size() ) {
    std::cerr << "HillRestricted::HillRestricted() "
	      << "Number of parameters does not agree with number of "
	      << "activators/repressors.\n"
	      << indValue[1].size() << " activators, "
	      << indValue[2].size() << " repressors, and "
	      << paraValue.size() << " parameters\n"
 	      << "One plus pairs of parameters (V_max,K_half1,n_HillRestricted1,"
 	      << "K2,n2,...) must be given.\n";
    exit(EXIT_FAILURE);
  }
  /*if( indValue[0].size() != 1 ) {
      std::cerr << "HillRestricted::HillRestricted() "
  	      << "Number of Restricting variables must be one"
  	      << "(eg. hillRestricted x 3 1 x x)\n";
      exit(0);
    }*/

  // Set the variable values
  //
  setId("hillRestricted");
  setParameter(paraValue);
  setVariableIndex(indValue);

  // Set the parameter identities
  //
  std::vector<std::string> tmp( numParameter() );
  tmp.resize( numParameter() );
  tmp[0] = "V_max";
  tmp[1] = "K_half";
  tmp[2] = "n_HillRestricted";
  setParameterId( tmp );
}

void HillRestricted::
derivs(Compartment &compartment,size_t species,DataMatrix &y,DataMatrix &dydt)
{
  double contribution=parameter(0); //*y[compartment.index()][variableIndex(0,0)];
  // Restrictors
  for(size_t i=0 ; i<numVariableIndex(0) ; i++) {
    contribution *= y[compartment.index()][variableIndex(0,i)];
  }
  size_t parameterIndex=1;
  // Activator contributions
  for( size_t i=0 ; i<numVariableIndex(1) ; i++ ) {
    double c = std::pow(y[compartment.index()][variableIndex(1,i)],
			parameter(parameterIndex+1));
    contribution *= c
      / ( std::pow(parameter(parameterIndex),parameter(parameterIndex+1)) + c );
    parameterIndex+=2;
  }
  // Repressor contributions
  for( size_t i=0 ; i<numVariableIndex(2) ; i++ ) {
    double c = std::pow(parameter(parameterIndex),parameter(parameterIndex+1));
    contribution *= c /
      ( c + std::pow(y[compartment.index()][variableIndex(2,i)],
		     parameter(parameterIndex+1)) );
    parameterIndex+=2;
  }
  dydt[compartment.index()][species] += contribution;
}

void HillRestricted::
derivsWithAbs(Compartment &compartment,size_t species,DataMatrix &y,DataMatrix &dydt,DataMatrix &sdydt)
{
  double contribution=parameter(0); //*y[compartment.index()][variableIndex(0,0)];
  // Restrictors
  for(size_t i=0 ; i<numVariableIndex(0) ; i++) {
    contribution *= y[compartment.index()][variableIndex(0,i)];
  }
  size_t parameterIndex=1;
  // Activator contributions
  for( size_t i=0 ; i<numVariableIndex(1) ; i++ ) {
    double c = std::pow(y[compartment.index()][variableIndex(1,i)],
			parameter(parameterIndex+1));
    contribution *= c
      / ( std::pow(parameter(parameterIndex),parameter(parameterIndex+1)) + c );
    parameterIndex+=2;
  }
  // Repressor contributions
  for( size_t i=0 ; i<numVariableIndex(2) ; i++ ) {
    double c = std::pow(parameter(parameterIndex),parameter(parameterIndex+1));
    contribution *= c /
      ( c + std::pow(y[compartment.index()][variableIndex(2,i)],
		     parameter(parameterIndex+1)) );
    parameterIndex+=2;
  }
  dydt[compartment.index()][species] += contribution;
  sdydt[compartment.index()][species] += contribution;
}

size_t HillRestricted::
Jacobian(Compartment &compartment,size_t species,DataMatrix &y,JacobianMatrix &A) 
{
  double contribution=parameter(0);
  // Restrictors
  for(size_t i=0 ; i<numVariableIndex(1) ; i++) {
    contribution *= y[compartment.index()][variableIndex(0,i)];
  }
  size_t parameterIndex=1;
  // Activator contributions
  for (size_t k=0; k<numVariableIndex(1); ++k) {
    double c = std::pow(y[compartment.index()][variableIndex(1,k)],
			parameter(parameterIndex+1));
    contribution *= c / ( std::pow(parameter(parameterIndex),parameter(parameterIndex+1)) + c );
    parameterIndex+=2;
  }
  // Repressor contributions
  for (size_t k=0; k<numVariableIndex(2); ++k) {
    double c = std::pow(parameter(parameterIndex),parameter(parameterIndex+1));
    contribution *= c / 
      ( c + std::pow(y[compartment.index()][variableIndex(2,k)],parameter(parameterIndex+1)) );
    parameterIndex+=2;
  }   
  if (contribution<=0.0)
    return 1;
  //
  // Adding into A 
  //
  size_t i = compartment.index()*y[0].size() + species; 

  // Restrictor contributions
  for (size_t k=0; k<numVariableIndex(0); ++k) {
    size_t j = compartment.index()*y[0].size() + variableIndex(0,k); 
    A(i,j) += contribution/y[compartment.index()][variableIndex(0,k)];
  }

  parameterIndex = 1;
  // Activator contributions
  for (size_t k=0; k<numVariableIndex(1); ++k) {
    size_t j = compartment.index()*y[0].size() + variableIndex(1,k); 
    double c = std::pow(parameter(parameterIndex),
			parameter(parameterIndex+1));
    double cc = c + std::pow(y[compartment.index()][variableIndex(1,k)],
			     parameter(parameterIndex+1));
    A(i,j) += contribution*c*parameter(parameterIndex+1) / 
      (cc*y[compartment.index()][variableIndex(1,k)]);
    parameterIndex+=2;
  }
  // Repressor contributions
  for (size_t k=0; k<numVariableIndex(2); ++k) {
    size_t j = compartment.index()*y[0].size() + variableIndex(2,k); 
    double c = std::pow(parameter(parameterIndex),
			parameter(parameterIndex+1));
    double cc = c + std::pow(y[compartment.index()][variableIndex(2,k)],
				parameter(parameterIndex+1));
    A(i,j) -= contribution*parameter(parameterIndex+1)*
      std::pow(y[compartment.index()][variableIndex(2,k)],parameter(parameterIndex+1)-1) / (cc);
    parameterIndex+=2;
  }
  return 1;
}

HillRepressor::HillRepressor(std::vector<double> &paraValue, 
			     std::vector< std::vector<size_t> > &indValue ) 
{
  //
  // Do some checks on the parameters and variable indeces
  //
  if( paraValue.size() !=3 ) {
    std::cerr << "HillRepressor::HillRepressor() "
	      << "Three parameters (V_max,K_half,n_Hill) must be given.\n";
    exit(0);
  }
  if( indValue.size() != 1 ) {
    std::cerr << "HillRepressor::HillRepressor() "
	      << "One level of variable index is used.\n";
    exit(0);
  }
  if( indValue.size() && indValue[0].size() != 1 ) {
    std::cerr << "HillRepressor::HillRepressor() "
	      << "One variable index used.\n";
    exit(0);
  } 
  //
  // Set the variable values
  //
  setId("hillRepressor");
  setParameter(paraValue);  
  setVariableIndex(indValue);
  //
  // Set the parameter identities
  //
  std::vector<std::string> tmp( numParameter() );
  tmp.resize( numParameter() );
  tmp[0] = "V_max";
  tmp[1] = "K_half";
  tmp[2] = "n_Hill";
  setParameterId( tmp );
}

void HillRepressor::
derivs(Compartment &compartment,size_t species,DataMatrix &y,DataMatrix &dydt) 
{  
  double c = pow(parameter(1),parameter(2));
  dydt[compartment.index()][species] += parameter(0)*c /
    ( c + pow(y[compartment.index()][variableIndex(0,0)],parameter(2)) );
}

size_t HillRepressor::
Jacobian(Compartment &compartment,size_t species,DataMatrix &y,JacobianMatrix &A) 
{  
  double c = pow(parameter(1),parameter(2));
  size_t i = compartment.index()*y[0].size() + species; 
  size_t j = compartment.index()*y[0].size() + variableIndex(0,0); 
  double cc = c + pow(y[compartment.index()][variableIndex(0,0)],parameter(2));
  A(i,j) -= parameter(0)*parameter(2)*std::pow(y[compartment.index()][variableIndex(0,0)],parameter(2)-1)*c  / (cc*cc);
  return 1;
}

HillGeneralOne::HillGeneralOne(std::vector<double> &paraValue, 
			       std::vector< std::vector<size_t> > 
			       &indValue ) 
{
  // Do some checks on the parameters and variable indeces
  //
  if (indValue.size() != 1 || indValue[0].size() != 1) {
    std::cerr << "HillGeneralOne::HillGeneralOne() "
	      << "One variable index (TF) is used." << std::endl;
    exit(EXIT_FAILURE);
  }
  if (paraValue.size() != 4) {
    std::cerr << "HillGeneralOne::HillGeneralOne() "
	      << "Uses four parameters (Vunbound, Vbound, K_H, n_H)."
	      << std::endl;
    exit(EXIT_FAILURE);
  }
  
  // Set the variable values
  //
  setId("hillGeneralOne");
  setParameter(paraValue);  
  setVariableIndex(indValue);
  
  // Set the parameter identities
  //
  std::vector<std::string> tmp( numParameter() );
  tmp.resize( numParameter() );
  tmp[0] = "V_unbound";
  tmp[1] = "V_bound";
  tmp[2] = "K_half";
  tmp[3] = "n_Hill";
  setParameterId( tmp );
}

void HillGeneralOne::
derivs(Compartment &compartment,size_t species,DataMatrix &y,DataMatrix &dydt) 
{
  double tfPow = std::pow(y[compartment.index()][variableIndex(0,0)],parameter(3)); 
  double KPow = std::pow(parameter(2),parameter(3));   
  dydt[compartment.index()][species] += (parameter(0)*KPow + parameter(1)*tfPow) /
    (KPow+tfPow);
}

HillGeneralTwo::HillGeneralTwo(std::vector<double> &paraValue, 
			       std::vector< std::vector<size_t> > 
			       &indValue ) 
{
  // Do some checks on the parameters and variable indeces
  //
  if (indValue.size() != 1 || indValue[0].size() != 2) {
    std::cerr << "HillGeneralTwo::HillGeneralTwo() "
	      << "Two variable indices (TFs) are used." << std::endl;
    exit(EXIT_FAILURE);
  }
  if (paraValue.size() != 8) {
    std::cerr << "HillGeneralTwo::HillGeneralTwo() "
	      << "Uses eight parameters (Vunbound, Vfirstbound, Vsecondbound, Vbothbound, "
	      << "K1_H, n1_H, K2_H, n2_H)." << std::endl;
    exit(EXIT_FAILURE);
  }
  
  // Set the variable values
  //
  setId("hillGeneralTwo");
  setParameter(paraValue);  
  setVariableIndex(indValue);
  
  // Set the parameter identities
  //
  std::vector<std::string> tmp( numParameter() );
  tmp.resize( numParameter() );
  tmp[0] = "V_unbound";
  tmp[1] = "V_firstbound";
  tmp[2] = "V_secondbound";
  tmp[3] = "V_bothbound";
  tmp[4] = "K1_half";
  tmp[5] = "n1_Hill";
  tmp[6] = "K2_half";
  tmp[7] = "n2_Hill";
  setParameterId( tmp );
}

void HillGeneralTwo::
derivs(Compartment &compartment,size_t species,DataMatrix &y,DataMatrix &dydt) 
{
  double tf1Pow = std::pow(y[compartment.index()][variableIndex(0,0)],parameter(5)); 
  double tf2Pow = std::pow(y[compartment.index()][variableIndex(0,1)],parameter(7)); 
  double K1Pow = std::pow(parameter(4),parameter(5));   
  double K2Pow = std::pow(parameter(6),parameter(7));   
  dydt[compartment.index()][species] += (parameter(0)*K1Pow*K2Pow + parameter(1)*tf1Pow*K2Pow + 
					 parameter(2)*K1Pow*tf2Pow + parameter(3)*tf1Pow*tf2Pow)
    / ( (K1Pow+tf1Pow)*(K2Pow+tf2Pow) );
}

void HillGeneralTwo::
derivsWithAbs(Compartment &compartment,size_t species,DataMatrix &y,DataMatrix &dydt,DataMatrix &sdydt) 
{
  double tf1Pow = std::pow(y[compartment.index()][variableIndex(0,0)],parameter(5)); 
  double tf2Pow = std::pow(y[compartment.index()][variableIndex(0,1)],parameter(7)); 
  double K1Pow = std::pow(parameter(4),parameter(5));   
  double K2Pow = std::pow(parameter(6),parameter(7));   
  double contribution = (parameter(0)*K1Pow*K2Pow + parameter(1)*tf1Pow*K2Pow + 
			 parameter(2)*K1Pow*tf2Pow + parameter(3)*tf1Pow*tf2Pow)
    / ( (K1Pow+tf1Pow)*(K2Pow+tf2Pow) );
  
  dydt[compartment.index()][species] += contribution;
  sdydt[compartment.index()][species] += contribution;
}

HillGeneralThree::HillGeneralThree(std::vector<double> &paraValue, 
				   std::vector< std::vector<size_t> > 
				   &indValue ) 
{
  // Do some checks on the parameters and variable indeces
  //
  if (indValue.size() != 1 || indValue[0].size() != 3) {
    std::cerr << "HillGeneralThree::HillGeneralThree() "
	      << "Three variable indices (TFs) are used." << std::endl;
    exit(EXIT_FAILURE);
  }
  if (paraValue.size() != 14) {
    std::cerr << "HillGeneralThree::HillGeneralThree() "
	      << "Uses fourteen parameters (V000, V100, V010, V001, V110, V101, V011, V111, "
	      << "K1_H, n1_H, K2_H, n2_H, K3_H, n3_H)." << std::endl;
    exit(EXIT_FAILURE);
  }
  
  // Set the variable values
  //
  setId("hillGeneralThree");
  setParameter(paraValue);  
  setVariableIndex(indValue);
  
  // Set the parameter identities
  //
  std::vector<std::string> tmp( numParameter() );
  tmp.resize( numParameter() );
  tmp[0] = "V_000";
  tmp[1] = "V_100";
  tmp[2] = "V_010";
  tmp[3] = "V_001";
  tmp[4] = "V_110";
  tmp[5] = "V_101";
  tmp[6] = "V_011";
  tmp[7] = "V_111";
  tmp[8] = "K1_half";
  tmp[9] = "n1_Hill";
  tmp[10] = "K2_half";
  tmp[11] = "n2_Hill";
  tmp[12] = "K3_half";
  tmp[13] = "n3_Hill";
  setParameterId( tmp );
}

void HillGeneralThree::
derivs(Compartment &compartment,size_t species,DataMatrix &y,DataMatrix &dydt) 
{
  double tf1Pow = std::pow(y[compartment.index()][variableIndex(0,0)],parameter(9)); 
  double tf2Pow = std::pow(y[compartment.index()][variableIndex(0,1)],parameter(11)); 
  double tf3Pow = std::pow(y[compartment.index()][variableIndex(0,2)],parameter(13)); 
  double K1Pow = std::pow(parameter(8),parameter(9));   
  double K2Pow = std::pow(parameter(10),parameter(11));   
  double K3Pow = std::pow(parameter(12),parameter(13));   

  dydt[compartment.index()][species] += (parameter(0)*K1Pow*K2Pow*K3Pow + parameter(1)*tf1Pow*K2Pow*K3Pow
					 + parameter(2)*K1Pow*tf2Pow*K3Pow + parameter(3)*K1Pow*K2Pow*tf3Pow
					 + parameter(4)*tf1Pow*tf2Pow*K3Pow + parameter(5)*tf1Pow*K2Pow*tf3Pow
					 + parameter(6)*K1Pow*tf2Pow*tf3Pow + parameter(7)*tf1Pow*tf2Pow*tf3Pow )
					 / ( (K1Pow+tf1Pow)*(K2Pow+tf2Pow)*(K3Pow+tf3Pow) );
}

NonComBinding::NonComBinding(std::vector<double> &paraValue, 
			       std::vector< std::vector<size_t> > 
			       &indValue ) 
{
  // Do some checks on the parameters and variable indeces
  //
  if (indValue.size() != 1 || indValue[0].size() != 2) {
    std::cerr << "NonComBinding::NonComBinding() "
	      << "Two variable indices (TFs) are used." << std::endl;
    exit(EXIT_FAILURE);
  }
  if (paraValue.size() != 9) {
    std::cerr << "NonComBinding::NonComBinding() "
	      << "Uses nine parameters (Vunbound, Vfirstbound, Vsecondbound, Vbothbound, "
	      << "K1, n1_H, K2, K2' n2_H)." << std::endl;
    exit(EXIT_FAILURE);
  }
  
  // Set the variable values
  //
  setId("nonComBinding");
  setParameter(paraValue);  
  setVariableIndex(indValue);
  
  // Set the parameter identities
  //
  std::vector<std::string> tmp( numParameter() );
  tmp.resize( numParameter() );
  tmp[0] = "V_unbound";
  tmp[1] = "V_firstbound";
  tmp[2] = "V_secondbound";
  tmp[3] = "V_bothbound";
  tmp[4] = "K1_half";
  tmp[5] = "n1_Hill";
  tmp[6] = "K2_half";
  tmp[7] = "K2'_half";
  tmp[8] = "n2_Hill";
  setParameterId( tmp );
}

void NonComBinding::
derivs(Compartment &compartment,size_t species,DataMatrix &y,DataMatrix &dydt) 
{
  // rescaled concentrations taken to the appropiate exponent
  double tf1Sc = std::pow( y[compartment.index()][variableIndex(0,0)]/parameter(4),parameter(5)); 
  double tf2Sc = std::pow(y[compartment.index()][variableIndex(0,1)]/parameter(6),parameter(8)); 
  double tf2PSc = std::pow(y[compartment.index()][variableIndex(0,1)]/parameter(7),parameter(8)); 

  dydt[compartment.index()][species] += (parameter(0) + parameter(1)*tf1Sc + parameter(2)*tf2Sc + parameter(3)*tf1Sc*tf2PSc)
    / (1 + tf1Sc + tf2Sc + tf1Sc*tf2PSc );
}

SheaAckers::SheaAckers(std::vector<double> &paraValue, 
	   std::vector< std::vector<size_t> > 
	   &indValue ) {

  //Do some checks on the parameters and variable indeces
  //////////////////////////////////////////////////////////////////////
  if( indValue.size() != 2 ) {
    std::cerr << "SheaAckers::SheaAckers() "
	      << "Two levels of variable index is used, "
	      << "activators and repressors.\n";
    exit(0);
  }
  if( (indValue[0].size()+indValue[1].size()+1) != paraValue.size() ) {
    std::cerr << "SheaAckers::SheaAckers() "
	      << "Number of parameters does not agree with number of "
	      << "activators/repressors.\n"
	      << indValue[0].size() << " activators, " 
	      << indValue[1].size() << " repressors, and "
	      << paraValue.size() << " parameters\n"
 	      << "One parameter per variable plus one"
 	      << " must be given.\n";
    exit(0);
  }
  
  //Set the variable values
  //////////////////////////////////////////////////////////////////////
  setId("sheaAckers");
  setParameter(paraValue);  
  setVariableIndex(indValue);
  
  // Checks that the same species isn't 2 times in the same part (2 times in the denominator or 2 times in the numerator)
	/*for(size_t i = 0; i < indValue[0].size(); ++i){
		for(size_t j = i+1; j < indValue[0].size(); ++i){
			if(variableIndex(0,i) == variableIndex(0,j)){
				std::cerr << "SheaAckers::SheaAckers() " 
				<< "The same species has been put multiple times into the numerator" 
				<< "species indice " << variableIndex(0,i) << " and " << variableIndex(0,j) << std::endl;
			}
		}
	}

	for(size_t i = 0; i < indValue[1].size(); ++i){
		for(size_t j = i+1; j < indValue[1].size(); ++i){
			if(variableIndex(1,i) == variableIndex(1,j)){
				std::cerr << "SheaAckers::SheaAckers() " 
				<< "The same species has been put multiple times into the denominator" 
				<< "species indice " << variableIndex(0,i) << " and " << variableIndex(0,j) << std::endl;
			}
		}
	}*/




  // Set the parameter identities
  //
  std::vector<std::string> tmp( numParameter() );
  tmp.resize( numParameter() );
  tmp[0] = "a_0";
  setParameterId( tmp );
}

void SheaAckers::
derivs(Compartment &compartment,size_t species,DataMatrix &y,DataMatrix &dydt) 
{
  // Activator contributions (nominator)
  size_t pCount=0;
  double nominator = parameter(pCount++);
  for( size_t i=0 ; i<numVariableIndex(0) ; i++ ) {
    nominator += parameter(pCount++)*y[compartment.index()][variableIndex(0,i)];
  }
  // Repressor contributions
  double denominator=1.0;
  for( size_t i=0 ; i<numVariableIndex(1) ; i++ ) {
    denominator += parameter(pCount++)*y[compartment.index()][variableIndex(1,i)];
  }   
  dydt[compartment.index()][species] += nominator/denominator; 
}

#define numSpecies() y[0].size()

size_t SheaAckers::
Jacobian(Compartment &compartment,size_t species,DataMatrix &y,JacobianMatrix &A)
{
  if (parameter(0) != 0.) {
    std::cerr << "SheaAckers::Jacobian" << std::endl
	      << "Only correct if constant contribution not present (p_0=0)"
	      << std::endl;
    exit(EXIT_FAILURE);
  }
	std::vector<double> coefficients_num(numSpecies(), 0.);
	std::vector<double> coefficients_denom(numSpecies(), 0.);
	
	size_t pCount=0;
	for( size_t i=0 ; i<numVariableIndex(0) ; i++ ) {
		coefficients_num[variableIndex(0,i)] += parameter(pCount++);
	}

	for( size_t i=0 ; i<numVariableIndex(1) ; i++ ) {
		coefficients_denom[variableIndex(1,i)] += parameter(pCount++);
	}   
	
	size_t i_1 = compartment.index()*y[0].size() + species; 
	// finds the coefficient of this species a_n in the numerator and a_d the denominator

	pCount=0;
	double nominator = 0.0;
	for( size_t i=0 ; i<numVariableIndex(0) ; i++ ) {
		nominator += parameter(pCount++)*y[compartment.index()][variableIndex(0,i)];
	}
	double denominator=1.0;
	for( size_t i=0 ; i<numVariableIndex(1) ; i++ ) {
		denominator += parameter(pCount++)*y[compartment.index()][variableIndex(1,i)];
	}

	for(size_t i = 0; i < numSpecies(); ++i){
		if((coefficients_num[i] != 0) || (coefficients_denom[i] != 0)){
			size_t j = compartment.index()*y[0].size() + i;
			A(i_1,j) += (coefficients_num[i] * denominator - nominator * coefficients_denom[i]) / (denominator * denominator);
		}		
	}
	return 1;
}

SheaAckersDimer::
SheaAckersDimer(std::vector<double> &paraValue, 
		std::vector< std::vector<size_t> > 
		&indValue ) {

  // Do some checks on the parameters and variable indeces
  //
  if( indValue.size() != 4 ) {
    std::cerr << "SheaAckersDimer::SheaAckersDimer() "
	      << "Four levels of variable index is used, "
	      << "activators dimeractivators repressors and dimerrepressors.\n";
    exit(0);
  }
  if( (indValue[0].size()+0.5*indValue[1].size()+indValue[2].size()+0.5*indValue[3].size()+1) 
      != paraValue.size() ) {
    std::cerr << "SheaAckersDimer::SheaAckersDimer() "
	      << "Number of parameters does not agree with number of "
	      << "activators/repressors.\n"
	      << indValue[0].size() << " activators, " 
	      << indValue[1].size() << " dimeractivators, and "
	      << indValue[2].size() << " repressors, and "
	      << indValue[3].size() << " dimerrepressors, and "
	      << paraValue.size() << " parameters\n"
 	      << "One parameter per variable"
 	      << " must be given.\n";
    exit(0);
  }
  
  // Set the variable values
  //
  setId("sheaAckersDimer");
  setParameter(paraValue);
  setVariableIndex(indValue);
  
  
  // Check if there is not a self-dimer, for which the jacobian will not be defined
  /*for( size_t i=0 ; i<numVariableIndex(1) ; i+=2 ) {
    if(variableIndex(1,i) == variableIndex(1,i+1)){
    std::cerr << "WARNING ! The jacobian is not defined with an auto-dimer inside the sheaAckersDimer equation\n";
    }
    }
    for( size_t i=0 ; i<numVariableIndex(3) ; i+=2 ) {
    if(variableIndex(3,i) == variableIndex(3,i+1)){
    std::cerr << "WARNING ! The jacobian is not defined with an auto-dimer inside the sheaAckersDimer equation\n";
    }
    } */
  //No need : the jacobian is valid with self dimers, 
  
  // Set the parameter identities
  //
  std::vector<std::string> tmp( numParameter() );
  tmp.resize( numParameter() );
  tmp[0] = "a_1";
  setParameterId( tmp );
}

void SheaAckersDimer::
derivs(Compartment &compartment,size_t species,DataMatrix &y,DataMatrix &dydt) 
{
  // Activator contributions (nominator)
  size_t pCount=0;
  double nominator = parameter(pCount++);
  for( size_t i=0 ; i<numVariableIndex(0) ; i++ ) {
    nominator += parameter(pCount++)*y[compartment.index()][variableIndex(0,i)];
  }
  for( size_t i=0 ; i<numVariableIndex(1) ; i+=2 ) {
    nominator += parameter(pCount++)*
      y[compartment.index()][variableIndex(1,i)]
      *y[compartment.index()][variableIndex(1,i+1)];
  }

  // Repressor contributions
  double denominator=1.0;
  for( size_t i=0 ; i<numVariableIndex(2) ; i++ ) {
    denominator += parameter(pCount++)*y[compartment.index()][variableIndex(2,i)];
  }   
  for( size_t i=0 ; i<numVariableIndex(3) ; i+=2 ) {
    denominator += parameter(pCount++)*
      y[compartment.index()][variableIndex(3,i)]*
      y[compartment.index()][variableIndex(3,i+1)];
  }   
  dydt[compartment.index()][species] += nominator/denominator; 
}

size_t SheaAckersDimer::
Jacobian(Compartment &compartment,size_t species,DataMatrix &y,JacobianMatrix &A)
{
  if (parameter(0) != 0.) {
    std::cerr << "SheaAckersDimer::Jacobian" << std::endl
	      << "Only correct if constant contribution not present (p_0=0)"
	      << std::endl;
    exit(EXIT_FAILURE);
  }
	std::vector<double> coeff_deriv_num(numSpecies(), 0.);
	std::vector<double> coeff_deriv_denom(numSpecies(), 0.);
	
	// Activator contributions (nominator)
	size_t pCount=0;
	double nominator = 0.0;
	for( size_t i=0 ; i<numVariableIndex(0) ; i++ ) {
		nominator += parameter(pCount)*y[compartment.index()][variableIndex(0,i)];
		coeff_deriv_num[variableIndex(0,i)] += parameter(pCount);
		pCount++;
	}
	for( size_t i=0 ; i<numVariableIndex(1) ; i+=2 ) {
		nominator += parameter(pCount)*
		y[compartment.index()][variableIndex(1,i)]
		*y[compartment.index()][variableIndex(1,i+1)];
		coeff_deriv_num[variableIndex(1,i)] += parameter(pCount)*y[compartment.index()][variableIndex(1,i+1)];
		coeff_deriv_num[variableIndex(1,i+1)] += parameter(pCount)*y[compartment.index()][variableIndex(1,i)];	
		pCount++;
	}

	// Repressor contributions
	double denominator=1.0;
	for( size_t i=0 ; i<numVariableIndex(2) ; i++ ) {
		denominator += parameter(pCount)*y[compartment.index()][variableIndex(2,i)];
		coeff_deriv_denom[variableIndex(2,i)] += parameter(pCount);
		pCount++;
	}   
	for( size_t i=0 ; i<numVariableIndex(3) ; i+=2 ) {
		denominator += parameter(pCount)*
		y[compartment.index()][variableIndex(3,i)]*
		y[compartment.index()][variableIndex(3,i+1)];
		coeff_deriv_denom[variableIndex(3,i)] += parameter(pCount)*y[compartment.index()][variableIndex(3,i+1)];
		coeff_deriv_denom[variableIndex(3,i+1)] += parameter(pCount)*y[compartment.index()][variableIndex(3,i)];	
		pCount++;
	}   

	size_t i_1 = compartment.index()*y[0].size() + species; 
	for(size_t i = 0; i < numSpecies(); ++i){
		if((coeff_deriv_num[i] != 0) || (coeff_deriv_denom[i] != 0)){
			size_t j = compartment.index()*y[0].size() + i;
			A(i_1,j) += (coeff_deriv_num[i] * denominator - nominator * coeff_deriv_denom[i]) / (denominator * denominator);
		}		
	}
	return 1;
}  
