//
// Filename     : BenchMarkModel.h
// Description  : Classes describing a benchmark model of 1 cell and 8 variables
// Author(s)    : Philippe Robert (philippe.robert@ens-lyon.fr)
// Created      : July 2010
//

#ifndef BENCHMARK_H
#define BENCHMARK_H

#include"baseReaction.h"
#include<cmath>

///
/// @brief Benchmark Model of a three step pathway problem
///
/// Source : A hybrid approach for efficient and robust parameter estimation in biochemical pathways, Maria Rodriguez-Fernandez et al., BioSystems 83 (2006) 248-265
///
/// There is 8 molecules in the metabolic pathway :
///
/// P -> M2 -> M1 -> S is the metabolic pathway
///   E3    E2   E1    are the enzymes
///
/// G1, G2, G3 are the respective mRNA for E1, E2, E3
/// The regulations loop are : P -> (-) on G1, G2 and G3, and S -> (+) on G1
///
/// The equations are :
///
/// @f[
/// \frac{dG_1}{dt} = \frac{V_1}{1+{\frac{p}{Ki_1}}^{ni_1} + {\frac{Ka_1}{S}}^{na_1}} - k_1.G_1
/// \frac{dG_2}{dt} = \frac{V_2}{1+{\frac{p}{Ki_2}}^{ni_2} + {\frac{Ka_2}{M1}}^{na_2}} - k_2.G_2
/// \frac{dG_3}{dt} = \frac{V_3}{1+{\frac{p}{Ki_3}}^{ni_2} + {\frac{Ka_3}{M2}}^{na_3}} - k_3.G_3
/// @f]
/// @f[
/// \frac{dE_1}{dt} = \frac{V_4.G_1}{K_4+G_1} - k_4.E_1
/// \frac{dE_2}{dt} = \frac{V_5.G_2}{K_5+G_2} - k_5.E_1
/// \frac{dE_3}{dt} = \frac{V_6.G_3}{K_6+G_3} - k_6.E_1
/// @f]
/// @f[
/// \frac{dM_1}{dt} = \frac{kcat_1.E_1.\frac{1}{Km_1}.(S-M_1)}{1+frac{S}{Km_1}+\frac{M_1}{Km_2}} - \frac{kcat_2.E_2.\frac{1}{Km_3}.(M1-M2)}{1+\frac{M1}{Km_3}+\frac{M_2}{Km_4}}
/// \frac{dM_2}{dt} = \frac{kcat_2.E_2.\frac{1}{Km_3}.(M1-M2)}{1+\frac{M1}{Km_3}+\frac{M_2}{Km_4}} - \frac{kcat_3.E_3.\frac{1}{Km_5}.(M2-P)}{1+\frac{M2}{Km_5}+\frac{p}{Km_6}}
/// @f]
///
///	36 parameters are needed :
///	@f$ V_1, Ki1, ni1, Ka1, na1, k_1 @f$
///	@f$ V_2, Ki2, ni2, Ka2, na2, k_2 @f$
///	@f$ V_3, Ki3, ni3, Ka3, na3, k_3 @f$
///	@f$ V_4, K4, k_4 @f$
///	@f$ V_5, K5, k_5 @f$
///	@f$ V_6, K6, k_6 @f$
///	@f$ kcat1, Km1, Km2 @f$
///	@f$ kcat2, Km3, Km4 @f$
///	@f$ kcat3, Km5, Km6 @f$
///
///	 8 variables (initial values) must be specified :
///	@f$ G1, G2, G3, E1, E2, E3, M1, M2, S, P @f$. P and S are supposed constants.
///

class benchMarkMendez : public BaseReaction {
	
public:
	///
	/// @brief Main constructor
	///
	/// This is the main constructor which sets the parameters and variable
	/// indices that defines the reaction.
	///
	/// @param paraValue vector with parameters
	///
	/// @param indValue vector of vectors with variable indices
	///
	/// @see BaseReaction::createReaction(std::vector<double> &paraValue,...)
	///
	benchMarkMendez(std::vector<double> &paraValue, 
			std::vector< std::vector<size_t> > &indValue );
	///
	/// @brief Derivative function for this reaction class
	///
	/// @see BaseReaction::derivs(Compartment &compartment,size_t species,...)
	///
	void derivs(Compartment &compartment,size_t species,
			  std::vector< std::vector<double> > &y,
			  std::vector< std::vector<double> > &dydt );
};

#endif
