#ifndef CREATION_H
#define CREATION_H
//
// Filename     : creation.h
// Description  : Classes describing creation of molecules
// Author(s)    : Henrik Jonsson (henrik@thep.lu.se)
//              : Pontus Melke
// Created      : December 2003
// Revision     : $Id: creation.h 673 2017-05-02 10:03:14Z korsbo $
//

#include"baseReaction.h"
#include"../common/typedefs.h"

///
/// @brief CreationLinear applies a creation rate proportional to a number of
/// user defined variables
///
/// The variable update is given by 
///
/// @f[ \frac{dc}{dt} = k_c \prod_i C_i @f]
///
/// where @f$ k_c @f$ is the rate constant and the product is over the user
/// supplied variables (indices) @f$ C_i @f$.
///
/// In a model file the reaction is defined as
///
/// @verbatim
/// creationLinear 1 1 N_i
/// k_c
/// index1 ... indexN_i
/// @endverbatim
///
/// Alternatively if no index supplied (constant production) the first line
/// can be replaced by 'creationLinear 1 0'.
///
class CreationLinear : public BaseReaction {
  
 public:
  
  ///
  /// @brief Main constructor
  ///
  /// This is the main constructor which sets the parameters and variable
  /// indices that defines the reaction.
  ///
  /// @param paraValue vector with parameters
  ///
  /// @param indValue vector of vectors with variable indices
  ///
  /// @see BaseReaction::createReaction(std::vector<double>&,...)
  ///
  CreationLinear(std::vector<double> &paraValue, 
		 std::vector< std::vector<size_t> > &indValue );
  
  ///
  /// @brief Derivative function for this reaction class
  ///
  /// @see BaseReaction::derivs(Compartment &compartment,size_t species,...)
  ///
  void derivs(Compartment &compartment,size_t species,DataMatrix &y,DataMatrix &dydt );

  ///
  /// @brief Derivative function for this reaction class calculating the absolute value for noise solvers
  ///
  /// @see BaseReaction::derivsWithAbs(Compartment &compartment,size_t species,...)
  ///
  void derivsWithAbs(Compartment &compartment,size_t species,DataMatrix &y,DataMatrix &dydt,DataMatrix &sdydt);
};
///
/// @brief CreationZero applies a constant creation 
///
/// The variable update is given by 
///
/// @f[ \frac{dc}{dt} = k_c @f]
///
/// where @f$ k_c @f$ is a constant parameter.
///
/// In a model file the reaction is defined as
///
/// @verbatim
/// creationZero 1 0
/// k_c
/// @endverbatim
///
class CreationZero : public BaseReaction {
  
 public:
  
  ///
  /// @brief Main constructor
  ///
  /// This is the main constructor which sets the parameters and variable
  /// indices that defines the reaction.
  ///
  /// @param paraValue vector with parameters
  ///
  /// @param indValue vector of vectors with variable indices
  ///
  /// @see BaseReaction::createReaction(std::vector<double>&,...)
  ///
  CreationZero(std::vector<double> &paraValue, 
	       std::vector< std::vector<size_t> > &indValue );
  ///
  /// @brief Derivative function for this reaction class
  ///
  /// @see BaseReaction::derivs(Compartment &compartment,size_t species,...)
  ///
  void derivs(Compartment &compartment,size_t species,DataMatrix &y,DataMatrix &dydt );

  ///
  /// @brief Derivative function for this reaction class calculating the absolute value for noise solvers
  ///
  /// @see BaseReaction::derivsWithAbs(Compartment &compartment,size_t species,...)
  ///
  void derivsWithAbs(Compartment &compartment,size_t species,DataMatrix &y,DataMatrix &dydt,DataMatrix &sdydt);
  
  ///
  /// @brief Jacobian function for this reaction class
  ///
  /// @see BaseReaction::Jacobian(Compartment &compartment,size_t species,...)
  ///
  size_t Jacobian(Compartment &compartment,size_t species,DataMatrix &y,JacobianMatrix &A);

  ///
  /// @brief Propensity calculation (probability to happen in [t,t+dt]) for this reaction class
  ///
  /// @see BaseReaction::propensity(Compartment &compartment,size_t species,DataMatrix &y)
  ///
  double propensity(Compartment &compartment,size_t species,DataMatrix &y);

  ///
  /// @brief Discrete update for this reaction used in stochastic simulations
  ///
  /// @see BaseReaction::discreteUpdate(Compartment &compartment,size_t species,DataMatrix &y)
  ///
  void discreteUpdate(Compartment &compartment,size_t species,DataMatrix &y);
  ///
  /// @brief Prints the reaction in Cambium format
  ///
  /// @see BaseReaction::printCambium()
  ///
  void printCambium( std::ostream &os, size_t varIndex ) const;
};

/// @brief CreationOne assumes a creation proportional to a species
/// concentration
///
/// The variable update is given by 
///
/// @f[ \frac{dc}{dt} = k_c C @f]
///
/// where @f$ k_c @f$ is a constant parameter and @f$ C @f$ is the value of a
/// variable which index is provided by the user.
///
/// In a model file the reaction is defined as
///
/// @verbatim
/// creationOne 1 1 1
/// k_c
/// index1
/// @endverbatim
///
class CreationOne : public BaseReaction {
  
 public:
  
  ///
  /// @brief Main constructor
  ///
  /// This is the main constructor which sets the parameters and variable
  /// indices that defines the reaction.
  ///
  /// @param paraValue vector with parameters
  ///
  /// @param indValue vector of vectors with variable indices
  ///
  /// @see BaseReaction::createReaction(std::vector<double>&,...)
  ///
  CreationOne(std::vector<double> &paraValue, 
	      std::vector< std::vector<size_t> > &indValue );
  
  ///
  /// @brief Derivative function for this reaction class
  ///
  /// @see BaseReaction::derivs(Compartment &compartment,size_t species,...)
  ///
  void derivs(Compartment &compartment,size_t species,DataMatrix &y,DataMatrix &dydt);
  
  ///
  /// @brief Derivative function for this reaction class calculating the absolute value for noise solvers
  ///
  /// @see BaseReaction::derivsWithAbs(Compartment &compartment,size_t species,...)
  ///
  void derivsWithAbs(Compartment &compartment,size_t species,DataMatrix &y,DataMatrix &dydt,DataMatrix &sdydt);

  ///
  /// @brief Jacobian function for this reaction class
  ///
  /// @see BaseReaction::Jacobian(Compartment &compartment,size_t species,...)
  ///
  size_t Jacobian(Compartment &compartment,size_t species,DataMatrix &y,JacobianMatrix &A);

  ///
  /// @brief Propensity calculation (probability to happen in [t,t+dt]) for this reaction class
  ///
  /// @see BaseReaction::propensity(Compartment &compartment,size_t species,DataMatrix &y)
  ///
  double propensity(Compartment &compartment,size_t species,DataMatrix &y);

  ///
  /// @brief Discrete update for this reaction used in stochastic simulations
  ///
  /// @see BaseReaction::discreteUpdate(Compartment &compartment,size_t species,DataMatrix &y)
  ///
  void discreteUpdate(Compartment &compartment,size_t species,DataMatrix &y);
  ///
  /// @brief Prints the reaction in Cambium format
  ///
  /// @see BaseReaction::printCambium()
  ///
  void printCambium( std::ostream &os, size_t varIndex ) const;
};

///
/// @brief CreationTwo assumes a creation proportional two a species
/// concentration
///
/// The variable update is given by 
///
/// @f[ \frac{dc}{dt} = k_c C_{1} C_{2} @f]
///
/// where @f$ k_c @f$ is a constant parameter and @f$ C_{1},C_{2} @f$ are the
/// values of variables which indices are provided by the user. Note that the
/// same index can be provided twice.
///
/// In a model file the reaction is defined as
///
/// @verbatim
/// creationTwo 1 1 2
/// k_c
/// index1 index2
/// @endverbatim
///
class CreationTwo : public BaseReaction {
  
 public:
  
  ///
  /// @brief Main constructor
  ///
  /// This is the main constructor which sets the parameters and variable
  /// indices that defines the reaction.
  ///
  /// @param paraValue vector with parameters
  ///
  /// @param indValue vector of vectors with variable indices
  ///
  /// @see BaseReaction::createReaction(std::vector<double>&,...)
  ///
  CreationTwo(std::vector<double> &paraValue, 
	      std::vector< std::vector<size_t> > &indValue );
  
  ///
  /// @brief Derivative function for this reaction class
  ///
  /// @see BaseReaction::derivs(Compartment &compartment,size_t species,...)
  ///
  void derivs(Compartment &compartment,size_t species,DataMatrix &y,DataMatrix &dydt );
  
  ///
  /// @brief Derivative function for this reaction class calculating the absolute value for noise solvers
  ///
  /// @see BaseReaction::derivsWithAbs(Compartment &compartment,size_t species,...)
  ///
  void derivsWithAbs(Compartment &compartment,size_t species,DataMatrix &y,DataMatrix &dydt,DataMatrix &sdydt);

  ///
  /// @brief Jacobian function for this reaction class
  ///
  /// @see BaseReaction::Jacobian(Compartment &compartment,size_t species,...)
  ///
  size_t Jacobian(Compartment &compartment,size_t species,DataMatrix &y,JacobianMatrix &A);
};

///
/// @brief CreationZeroDegradationOne uses contant creation/degradation
///
/// The variable update is given by 
///
/// @f[ \frac{dc}{dt} = K - K k_d c @f]
///
/// where @f$ K @f$ is the production rate and @f$ K k_d @f$ is the degradation
/// rate. 
///
/// In a model file the reaction is defined as
///
/// @verbatim
/// creationZeroDegradationOne 2 0
/// K k_d
/// @endverbatim
///
class CreationZeroDegradationOne : public BaseReaction {
  
 public:
  
  ///
  /// @brief Main constructor
  ///
  /// This is the main constructor which sets the parameters and variable
  /// indices that defines the reaction.
  ///
  /// @param paraValue vector with parameters
  ///
  /// @param indValue vector of vectors with variable indices
  ///
  /// @see BaseReaction::createReaction(std::vector<double>&,...)
  ///
  CreationZeroDegradationOne(std::vector<double> &paraValue,
                             std::vector< std::vector<size_t> > &indValue );
  
  ///
  /// @brief Derivative function for this reaction class
  ///
  /// @see BaseReaction::derivs(Compartment &compartment,size_t species,...)
  ///
  void derivs(Compartment &compartment,size_t species,DataMatrix &y,DataMatrix &dydt );
  
  ///
  /// @brief Jacobian function for this reaction class
  ///
  /// @see BaseReaction::Jacobian(Compartment &compartment,size_t species,...)
  ///
  size_t Jacobian(Compartment &compartment,size_t species,DataMatrix &y,JacobianMatrix &A);
};

///
/// @brief CreationZeroDegradationTwo uses constant creation and degradation
/// dependent on additional variable
///
/// The variable update is given by 
///
/// @f[ \frac{dc}{dt} = K - K k_d c C @f]
///
/// where @f$ K @f$ is the production rate and @f$ K k_d @f$ is the degradation
/// rate. C is the variable value for the index provided by the user.
///
/// In a model file the reaction is defined as
///
/// @verbatim
/// creationZeroDegradationTwo 2 1 1
/// K k_d
/// index1
/// @endverbatim
///
class CreationZeroDegradationTwo : public BaseReaction {
  
 public:
  
  ///
  /// @brief Main constructor
  ///
  /// This is the main constructor which sets the parameters and variable
  /// indices that defines the reaction.
  ///
  /// @param paraValue vector with parameters
  ///
  /// @param indValue vector of vectors with variable indices
  ///
  /// @see BaseReaction::createReaction(std::vector<double>&,...)
  ///
  CreationZeroDegradationTwo(std::vector<double> &paraValue,
                             std::vector< std::vector<size_t> > &indValue );
  
  ///
  /// @brief Derivative function for this reaction class
  ///
  /// @see BaseReaction::derivs(Compartment &compartment,size_t species,...)
  ///
  void derivs(Compartment &compartment,size_t species,DataMatrix &y,DataMatrix &dydt );
};

///
/// @brief CreationOneDegradationOne uses creation proportional to variable
/// value and degradation dependent itself
///
/// The variable update is given by 
///
/// @f[ \frac{dc}{dt} = K C - K k_d c @f]
///
/// where @f$ K @f$ is the production rate and @f$ K k_d @f$ is the degradation
/// rate. C is the variable value for the index provided by the user.
///
/// In a model file the reaction is defined as
///
/// @verbatim
/// creationOneDegradationOne 2 1 1
/// K k_d
/// index1
/// @endverbatim
///
class CreationOneDegradationOne : public BaseReaction {
  
 public:
  
  ///
  /// @brief Main constructor
  ///
  /// This is the main constructor which sets the parameters and variable
  /// indices that defines the reaction.
  ///
  /// @param paraValue vector with parameters
  ///
  /// @param indValue vector of vectors with variable indices
  ///
  /// @see BaseReaction::createReaction(std::vector<double>&,...)
  ///
  CreationOneDegradationOne(std::vector<double> &paraValue,
			    std::vector< std::vector<size_t> > &indValue );
  
  ///
  /// @brief Derivative function for this reaction class
  ///
  /// @see BaseReaction::derivs(Compartment &compartment,size_t species,...)
  ///
  void derivs(Compartment &compartment,size_t species,DataMatrix &y,DataMatrix &dydt );
};

///
/// @brief CreationOneDegradationTwo uses creation and degradation
/// proportional to additional variable values
///
/// The variable update is given by 
///
/// @f[ \frac{dc}{dt} = K C_1 - K k_d c C_2 @f]
///
/// where @f$ K @f$ is the production rate and @f$ K k_d @f$ is the
/// degradation rate. @f$ C_1,C_2 @f$ is the variable values for the indices
/// provided by the user.
///
/// In a model file the reaction is defined as
///
/// @verbatim
/// creationOneDegradationTwo 2 1 2
/// K k_d
/// index1 index2
/// @endverbatim
///
class CreationOneDegradationTwo : public BaseReaction {
  
 public:  
  ///
  /// @brief Main constructor
  ///
  /// This is the main constructor which sets the parameters and variable
  /// indices that defines the reaction.
  ///
  /// @param paraValue vector with parameters
  ///
  /// @param indValue vector of vectors with variable indices
  ///
  /// @see BaseReaction::createReaction(std::vector<double>&,...)
  ///
  CreationOneDegradationTwo(std::vector<double> &paraValue,
			    std::vector< std::vector<size_t> > &indValue );  
  ///
  /// @brief Derivative function for this reaction class
  ///
  /// @see BaseReaction::derivs(Compartment &compartment,size_t species,...)
  ///
  void derivs(Compartment &compartment,size_t species,DataMatrix &y,DataMatrix &dydt );
};

///
/// @brief Molecular creation for cells above/below a threshold position 
///
/// This class uses a threshold in a given dimension to determine if a
/// constant (rate @f$ k_c @f$) molecular creation should be
/// applied. Threshold value is given as parameter, and a second parameter is
/// the sign which is determining if creation is applied above (sign=1) or
/// below (sign=-1) the threshold value. The thrshold variable index is also
/// given.
///
/// In a model file the reaction is defined as
///
/// @verbatim
/// creationSpatialThreshold 3 1 1
/// k_c var_th sign
/// index1
/// @endverbatim
///
class CreationSpatialThreshold : public BaseReaction {
  
 public:
  
  ///
  /// @brief Main constructor
  ///
  /// This is the main constructor which sets the parameters and variable
  /// indices that defines the reaction.
  ///
  /// @param paraValue vector with parameters
  ///
  /// @param indValue vector of vectors with variable indices
  ///
  /// @see BaseReaction::createReaction(std::vector<double>&,...)
  ///
  CreationSpatialThreshold(std::vector<double> &paraValue, 
			   std::vector< std::vector<size_t> > &indValue );
  
  ///
  /// @brief Derivative function for this reaction class
  ///
  /// @see BaseReaction::derivs(Compartment &compartment,size_t species,...)
  ///
  void derivs(Compartment &compartment,size_t species,DataMatrix &y,DataMatrix &dydt );
};

///
/// @brief Molecular creation for cells above/below a radial threshold
///
/// This class uses a threshold in the radius and uses a Hill formalism to 
/// calculate the creation rate. Threshold value and Hill coefficient is given as
/// parameters, The sign parameter sets whether
/// creation is applied above (sign=1) or below (sign=-1) the threshold
/// value.
///
/// In a model file the reaction is defined as
///
/// @verbatim
/// creationSpatialSphere 4 0
/// V_max R_th(K_Hill) n_Hill sign
/// @endverbatim
///
class CreationSpatialSphere : public BaseReaction {
  
 private:
  double powK_;

 public:
  
  ///
  /// @brief Main constructor
  ///
  /// This is the main constructor which sets the parameters and variable
  /// indices that defines the reaction.
  ///
  /// @param paraValue vector with parameters
  ///
  /// @param indValue vector of vectors with variable indices
  ///
  /// @see BaseReaction::createReaction(std::vector<double>&,...)
  ///
  CreationSpatialSphere(std::vector<double> &paraValue, 
			std::vector< std::vector<size_t> > &indValue );
  
  ///
  /// @brief Derivative function for this reaction class
  ///
  /// @see BaseReaction::derivs(Compartment &compartment,size_t species,...)
  ///
  void derivs(Compartment &compartment,size_t species,DataMatrix &y,DataMatrix &dydt );
};

///
/// @brief Molecular creation for cells in the outer layer of a shape created with ParabolaSphereWall
///
/// This class uses a threshold and a Hill formalism to 
/// calculate the creation rate. Threshold value and Hill coefficient are given as
/// parameters. The parameters of the reaction must be the same as the parameters of ParabolaSphereWall minus the force.
/// In addition 3 parameters for the Hill function are given: V, K (=width of outer cell layer) and N.
///
/// In a model file the reaction is defined as
///
/// @verbatim
/// creationSpatialSphere 8 0
/// C H R X0 growth V K N
/// @endverbatim
///
class CreationSpatialParabolaSphere : public BaseReaction {

 public:
  
  ///
  /// @brief Main constructor
  ///
  /// This is the main constructor which sets the parameters and variable
  /// indices that defines the reaction.
  ///
  /// @param paraValue vector with parameters
  ///
  /// @param indValue vector of vectors with variable indices
  ///
  /// @see BaseReaction::createReaction(std::vector<double>&,...)
  ///
  CreationSpatialParabolaSphere(std::vector<double> &paraValue, 
			std::vector< std::vector<size_t> > &indValue );
  
  ///
  /// @brief Derivative function for this reaction class
  ///
  /// @see BaseReaction::derivs(Compartment &compartment,size_t species,...)
  ///
  void derivs(Compartment &compartment,size_t species,DataMatrix &y,DataMatrix &dydt );
};

///
/// @brief Molecular creation for cells in the outer layer of a shape created with ParabolaSphereWallGrowth
///
/// This class uses a threshold and a Hill formalism to 
/// calculate the creation rate. Threshold value and Hill coefficient are given as
/// parameters. The parameters of the reaction must be the same as the parameters of ParabolaSphereWallGrowth minus the force.
/// In addition 3 parameters for the Hill function are given: V, K (=width of outer cell layer) and N.
///
/// In a model file the reaction is defined as
///
/// @verbatim
/// creationSpatialSphere 9 0
/// C H R X0 growth_init growth_rate V K N
/// @endverbatim
///
class CreationSpatialParabolaSphereGrowth : public BaseReaction {

 public:
  
  ///
  /// @brief Main constructor
  ///
  /// This is the main constructor which sets the parameters and variable
  /// indices that defines the reaction.
  ///
  /// @param paraValue vector with parameters
  ///
  /// @param indValue vector of vectors with variable indices
  ///
  /// @see BaseReaction::createReaction(std::vector<double>&,...)
  ///
  CreationSpatialParabolaSphereGrowth(std::vector<double> &paraValue, 
			std::vector< std::vector<size_t> > &indValue );
  
  ///
  /// @brief Derivative function for this reaction class
  ///
  /// @see BaseReaction::derivs(Compartment &compartment,size_t species,...)
  ///
  void derivs(Compartment &compartment,size_t species,DataMatrix &y,DataMatrix &dydt );

  ///
  /// @see BaseReaction::update()
  ///
  void update(double h, double t,
	      std::vector< std::vector<double> > &y);
};

///
/// @brief Molecular creation for cells in the outer layer of a shape created with PrimordiumGrowth
///
/// This class uses a threshold and a Hill formalism to 
/// calculate the creation rate. Threshold value and Hill coefficient are given as
/// parameters. The parameters of the reaction must be the same as the parameters of ParabolaSphereWallGrowth minus the force.
/// In addition 3 parameters for the Hill function are given: V, K (=width of outer cell layer) and N.
///
/// In a model file the reaction is defined as
///
/// @verbatim
/// creationSpatialSphere 9 0
/// C H R X0 growth_init growth_rate V K N
/// @endverbatim
///
class CreationPrimordium : public BaseReaction {

 public:
  
  ///
  /// @brief Main constructor
  ///
  /// This is the main constructor which sets the parameters and variable
  /// indices that defines the reaction.
  ///
  /// @param paraValue vector with parameters
  ///
  /// @param indValue vector of vectors with variable indices
  ///
  /// @see BaseReaction::createReaction(std::vector<double>&,...)
  ///
  CreationPrimordium(std::vector<double> &paraValue, 
			std::vector< std::vector<size_t> > &indValue );
  
  ///
  /// @brief Derivative function for this reaction class
  ///
  /// @see BaseReaction::derivs(Compartment &compartment,size_t species,...)
  ///
  void derivs(Compartment &compartment,size_t species,DataMatrix &y,DataMatrix &dydt );

  ///
  /// @see BaseReaction::update()
  ///
  void update(double h, double t,
	      std::vector< std::vector<double> > &y);
};


///
/// @brief Assumes a constant production if a cell is outside a radius on a
/// sphere/cylinder
///
/// A creation rate of k in all cells outside (sign=1) or inside (sign=-1) the
/// specified spatial radius @f$ R_th @f$ (on sphere or cylinder). The sphere
/// is used for the highest positional variable (y in 2D and z in 3D) above
/// zero and the cylinder is used below.
//
/// In a model file the reaction is defined as
///
/// @verbatim
/// creationSpatialSphereCylinder 3 0
/// k_c R_th sign
/// @endverbatim
///
class CreationSpatialSphereCylinder : public BaseReaction {
  
 public:
  
  ///
  /// @brief Main constructor
  ///
  /// This is the main constructor which sets the parameters and variable
  /// indices that defines the reaction.
  ///
  /// @param paraValue vector with parameters
  ///
  /// @param indValue vector of vectors with variable indices
  ///
  /// @see BaseReaction::createReaction(std::vector<double>&,...)
  ///
  CreationSpatialSphereCylinder(std::vector<double> &paraValue, 
				std::vector< std::vector<size_t> > 
				&indValue );
  
  ///
  /// @brief Derivative function for this reaction class
  ///
  /// @see BaseReaction::derivs(Compartment &compartment,size_t species,...)
  ///
  void derivs(Compartment &compartment,size_t species,DataMatrix &y,DataMatrix &dydt );
};

///
/// @brief Molecular creation for a cell positioned at a maximal or minimal
/// spatial location
///
/// This class finds a cell in maximal or minimal position in a given
/// dimension for which a constant molecular creation is applied. Creation
/// rate is given as first parameter, the second is if creation should be
/// applied to the maximal (1) or minimal (-1) cell, and the third sets wheter
/// the cell's neighbors also should have constant production (1) or not (0).
/// The spatial variable index is also given.
///
/// This reaction has the unconvential feature that it is updated for
/// compartment index 0 only, i.e. when the loop is zero it will locate the
/// max/min position and do the update for that cell. This is to avoid
/// calculateing max/min several times.
///
/// In a model file the reaction is defined as
///
/// @verbatim
/// creationSpatialMax 2 1 1
/// k_c sign
/// index1
/// @endverbatim
///
class CreationSpatialMax : public BaseReaction {
  
 public:
  
  ///
  /// @brief Main constructor
  ///
  /// This is the main constructor which sets the parameters and variable
  /// indices that defines the reaction.
  ///
  /// @param paraValue vector with parameters
  ///
  /// @param indValue vector of vectors with variable indices
  ///
  /// @see BaseReaction::createReaction(std::vector<double>&,...)
  ///
  CreationSpatialMax(std::vector<double> &paraValue, 
		     std::vector< std::vector<size_t> > &indValue );
  
  ///
  /// @brief Derivative function for this reaction class
  ///
  /// @see BaseReaction::derivs(Compartment &compartment,size_t species,...)
  ///
  void derivs(Compartment &compartment,size_t species,DataMatrix &y,DataMatrix &dydt );
};

///
/// @brief CreationExponent applies a creation rate proportional to an
/// exponent of one variable to a second variable.
///
/// The variable update is given by 
///
/// @f[ \frac{dc}{dt} = k_c C_1^{k_eC_2} @f]
///
/// where @f$ k_c @f$ is the rate constant, @f$ k_e @f$ is a constant, and the
/// @f$ C_1,C_2 @f$ are user supplied variables.
///
/// In a model file the reaction is defined as
///
/// @verbatim
/// creationExponent 2 1 2
/// k_c k_e
/// index1 index2
/// @endverbatim
///
class CreationExponent : public BaseReaction {
  
 public:
  
  ///
  /// @brief Main constructor
  ///
  /// This is the main constructor which sets the parameters and variable
  /// indices that defines the reaction.
  ///
  /// @param paraValue vector with parameters
  ///
  /// @param indValue vector of vectors with variable indices
  ///
  /// @see BaseReaction::createReaction(std::vector<double>&,...)
  ///
  CreationExponent(std::vector<double> &paraValue, 
		   std::vector< std::vector<size_t> > &indValue );
  
  ///
  /// @brief Derivative function for this reaction class
  ///
  /// @see BaseReaction::derivs(Compartment &compartment,size_t species,...)
  ///
  void derivs(Compartment &compartment,size_t species,DataMatrix &y,DataMatrix &dydt );
};

///
/// @brief Constant keeps a given concentration constant
///
class Constant : public BaseReaction {
  
 public:  
  ///
  /// @brief Main constructor
  ///
  /// This is the main constructor which sets the parameters and variable
  /// indices that defines the reaction.
  ///
  /// @param paraValue vector with parameters
  ///
  /// @param indValue vector of vectors with variable indices
  ///
  /// @see BaseReaction::createReaction(std::vector<double>&,...)
  ///
  Constant(std::vector<double> &paraValue, 
	   std::vector< std::vector<size_t> > &indValue );  
  ///
  /// @brief Derivative function for this reaction class
  ///
  /// @see BaseReaction::derivs(Compartment &compartment,size_t species,...)
  ///
  void derivs(Compartment &compartment,size_t species,DataMatrix &y,DataMatrix &dydt );
};


///
/// @brief CreationNeighbor applies a creation rate proportional to a
/// user defined variable in the neighboring cells
///
/// The variable update is given by 
///
/// @f[ \frac{dcx}{dt} = k_c C_xi @f]
///
/// where @f$ k_c @f$ is the rate constant and the 
/// supplied variable (index) @f$ C_xi @f$ is the concentration in the neighbor.
///
/// In a model file the reaction is defined as
///
/// @verbatim
/// creationNeighbor 1 N_x 1
/// k_c
/// index1 ... indexN_x
/// indexInNeighbor
/// @endverbatim
///
/// Note that this updates as many variables that there are neighbors,
/// typically used to update pin concentrations in membranes from auxin in
/// neighbors.
///
class CreationNeighbor : public BaseReaction {
  
 public:  
  ///
  /// @brief Main constructor
  ///
  /// This is the main constructor which sets the parameters and variable
  /// indices that defines the reaction.
  ///
  /// @param paraValue vector with parameters
  ///
  /// @param indValue vector of vectors with variable indices
  ///
  /// @see BaseReaction::createReaction(std::vector<double>&,...)
  ///
  CreationNeighbor(std::vector<double> &paraValue, 
		   std::vector< std::vector<size_t> > &indValue );  
  ///
  /// @brief Derivative function for this reaction class
  ///
  /// @see BaseReaction::derivs(Compartment &compartment,size_t species,...)
  ///
  void derivs(Compartment &compartment,size_t species,DataMatrix &y,DataMatrix &dydt );
};

///
/// @brief Molecular creation oscillating with a sinusodial shape
///
/// Creation reaction describing an oscillatory behavior described as 
/// 
/// @f[\frac{dc}{dt} = p_{0}/2 (1+sin( \frac{(p_{1}*time+p_{2})}{2\pi}) @f]
///
/// where p0 is the amplitude, p1 the period, p3 the phase and time (updated
/// locally during simulation.
///
/// In a model file the reaction is defined as
///
/// @verbatim
/// creationSinus 3 0
/// amp period phase
/// @endverbatim
///
class CreationSinus : public BaseReaction {

 private:

	double time_;

 public:
  
  ///
  /// @brief Main constructor
  ///
  /// This is the main constructor which sets the parameters and variable
  /// indices that defines the reaction.
  ///
  /// @param paraValue vector with parameters
  ///
  /// @param indValue vector of vectors with variable indices
  ///
  /// @see BaseReaction::createReaction(std::vector<double>&,...)
  ///
  CreationSinus(std::vector<double> &paraValue, 
								std::vector< std::vector<size_t> > &indValue );
  
  ///
  /// @brief Derivative function for this reaction class
  ///
  /// @see BaseReaction::derivs(Compartment &compartment,size_t species,...)
  ///
  void derivs(Compartment &compartment,size_t species,DataMatrix &y,DataMatrix &dydt );

  ///
  /// @see BaseReaction::update()
  ///
  void update(double h, double t,
	      std::vector< std::vector<double> > &y);

  ///
  /// @see BaseReaction::update()
  ///
  void initiate(double t,
								std::vector< std::vector<double> > &y);
};


///
/// @brief Set concentration to specific value at given times.
///
/// Specify timepoint/value pairs. At any such timepoint the value 
/// (concentration) of the species will be set to the specified value. The 
/// derivative of the species is also set to 0.
///
/// Beware that since the derivative is set to 0 the order in which you specify
/// reactions may matter.
/// 
/// @f[\frac{dc}{dt} = p_{0}/2 (1+sin( \frac{(p_{1}*time+p_{2})}{2\pi}) @f]
///
/// @f[\frac{dc(t_x)}{dt} = 0 @f]
///
/// @f[c(t_x) = v_x @f]
///
/// In a model file the reaction is defined as
///
/// @verbatim
/// constantInterval 2*N_t 0
/// t_1 t_2 ... t_n
/// v_1 v_2 ... v_n
/// @endverbatim
///
/// Where N_t is the number of timepoints specified, t_i is the starting point
/// for a new interval, and v_i the value of the concentration during this 
/// interval of time.
///
///
/// You could also specify a concentration to multipy each interval value with.
/// You must then specify one concentration for each time interval, and the 
/// final concentration of any interval will be:
/// @f[c = v_x \cdot c_x @f]
///
/// @verbatim
/// constantInterval 2*N_t 1 N_t
/// t_1 v_1 t_2 v_2 ... t_n v_n
/// c_1 c_2 ... c_n
/// 
/// @endverbatim
///



class ConstantInterval : public BaseReaction {

 private:
	 //size_t valueIndex_;
	 double time_;

 public:
  
  ///
  /// @brief Main constructor
  ///
  /// This is the main constructor which sets the parameters and variable
  /// indices that defines the reaction.
  ///
  /// @param paraValue vector with parameters
  ///
  /// @param indValue vector of vectors with variable indices
  ///
  /// @see BaseReaction::createReaction(std::vector<double>&,...)
  ///
  ConstantInterval(std::vector<double> &paraValue, 
								std::vector< std::vector<size_t> > &indValue );
  
  ///
  /// @brief Derivative function for this reaction class
  ///
  /// @see BaseReaction::derivs(Compartment &compartment,size_t species,...)
  ///
  void derivs(Compartment &compartment,size_t species,DataMatrix &y,DataMatrix &dydt );

  ///
  /// @see BaseReaction::update()
  ///
  void update(double h, double t,
	      std::vector< std::vector<double> > &y);

  ///
  /// @see BaseReaction::update()
  ///
  void initiate(double t, std::vector< std::vector<double> > &y);
};

#endif


