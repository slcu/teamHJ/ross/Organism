//
// Filename     : BenchMarkModel.h
// Description  : Classes describing a benchmark model of 1 cell and 8 variables
// Author(s)    : Philippe Robert (philippe.robert@ens-lyon.fr)
// Created      : July 2010
//

#include"BenchMarkMendez.h"
#include"baseReaction.h"


benchMarkMendez::benchMarkMendez(std::vector<double> &paraValue, 
				 std::vector< std::vector<size_t> > 
				 &indValue ) 
{
	//Do some checks on the parameters and variable indeces
	//////////////////////////////////////////////////////////////////////
	if( paraValue.size()!=36 ) {
		std::cerr << "benchMarkMendez::benchMarkMendez() "
				<< "Uses 36 parameters; \n"
				<< "V_1, Ki1, ni1, Ka1, na1, k_1,, V_2, Ki2, ni2, Ka2, na2, k_2, V_3, Ki3, ni3, Ka3, na3, k_3, V_4, K4, k_4, V_5, K5, k_5, V_6, K6, k_6, kcat1, Km1, Km2, kcat2, Km3, Km4, kcat3, Km5, Km6 \n\n";
		exit(0);
	}
	if( indValue.size() != 1 || indValue[0].size() != 10 ) {
		std::cerr << "benchMarkMendez::benchMarkMendez()"
			  << "Ten variable indices needed G1, G2, G3, E1, E2, E3, M1, M2, S, P .\n";
		exit(0);
	}
	//Set the variable values
	//////////////////////////////////////////////////////////////////////
	setId("benchMarkMendez");
	setParameter(paraValue);  
	setVariableIndex(indValue);
	
	//Set the parameter identities
	//////////////////////////////////////////////////////////////////////
	std::vector<std::string> tmp( numParameter() );
	tmp.resize( numParameter() );
	tmp[0] = "V_1";
	tmp[1] = "Ki1";
	tmp[2] = "ni1";
	tmp[3] = "Ka1";
	tmp[4] = "na1";
	tmp[5] = "k_1";

	tmp[6] = "V_2";
	tmp[7] = "Ki2";
	tmp[8] = "ni2";
	tmp[9] = "Ka2";
	tmp[10] = "na2";
	tmp[11] = "k_2";

	tmp[12] = "V_3";
	tmp[13] = "Ki3";
	tmp[14] = "ni3";
	tmp[15] = "Ka3";
	tmp[16] = "na3";
	tmp[17] = "k_3";

	tmp[18] = "V_4";
	tmp[19] = "K4";
	tmp[20] = "k_4";
	tmp[21] = "V_5";
	tmp[22] = "K5";
	tmp[23] = "k_5";
	tmp[24] = "V_6";
	tmp[25] = "K6";
	tmp[26] = "k_6";

	tmp[27] = "kcat1";
	tmp[28] = "Km1";
	tmp[29] = "Km2";
	tmp[30] = "kcat2";
	tmp[31] = "Km3";
	tmp[32] = "Km4";
	tmp[33] = "kcat3";
	tmp[34] = "Km5";
	tmp[35] = "Km6";

	setParameterId( tmp );
}

//! Derivative contribution for the benchMark model
/*! Deriving the time derivative contribution using
  the values in y and add the results to dydt. Compartment is the cell
*/

void benchMarkMendez::derivs(Compartment &compartment,size_t species,
				   std::vector< std::vector<double> > &y,
				   std::vector< std::vector<double> > &dydt ) 
{
	// Values of the variables
	double G1 = y[compartment.index()][variableIndex(0,0)];
	double G2 = y[compartment.index()][variableIndex(0,1)];
	double G3 = y[compartment.index()][variableIndex(0,2)];
	double E1 = y[compartment.index()][variableIndex(0,3)];
	double E2 = y[compartment.index()][variableIndex(0,4)];
	double E3 = y[compartment.index()][variableIndex(0,5)];
	double M1 = y[compartment.index()][variableIndex(0,6)];
	double M2 = y[compartment.index()][variableIndex(0,7)];
	double S = y[compartment.index()][variableIndex(0,8)];
	double P = y[compartment.index()][variableIndex(0,9)];

	//Derivative contributions
	
#define V1 parameter(0)
#define Ki1 parameter(1)
#define ni1 parameter(2)
#define Ka1 parameter(3)
#define na1 parameter(4)
#define k_1 parameter(5)
	
	dydt[compartment.index()][variableIndex(0,0)] += 
		(V1 / (1 + std::pow((P/Ki1),ni1) + std::pow((Ka1/S),na1))) - 
		k_1*G1;

#define V2 parameter(6)
#define Ki2 parameter(7)
#define ni2 parameter(8)
#define Ka2 parameter(9)
#define na2 parameter(10)
#define k_2 parameter(11)

	dydt[compartment.index()][variableIndex(0,1)] += 
		(V2 / (1 + std::pow((P/Ki2),ni2) + std::pow((Ka2/M1),na2))) - 
		k_2*G2;

#define V3 parameter(12)
#define Ki3 parameter(13)
#define ni3 parameter(14)
#define Ka3 parameter(15)
#define na3 parameter(16)
#define k_3 parameter(17)

	dydt[compartment.index()][variableIndex(0,2)] += 
		(V3 / (1 + std::pow((P/Ki3),ni3) + std::pow((Ka3/M2),na3))) - 
		k_3*G3;
	
#define V_4 parameter(18)
#define K4 parameter(19)
#define k_4 parameter(20)

	dydt[compartment.index()][variableIndex(0,3)] += 
		((V_4 * G1) / (K4 + G1)) - (k_4 * E1);

#define V_5 parameter(21)
#define K5 parameter(22)
#define k_5 parameter(23)

	dydt[compartment.index()][variableIndex(0,4)] += 
		((V_5 * G2) / (K5 + G2)) - (k_5 * E2);

#define V_6 parameter(24)
#define K6 parameter(25)
#define k_6 parameter(26)

	dydt[compartment.index()][variableIndex(0,5)] += 
		((V_6 * G3) / (K6 + G3)) - (k_6 * E3);

#define kcat1 parameter(27)
#define Km1 parameter(28)
#define Km2 parameter(29)
#define kcat2 parameter(30)
#define Km3 parameter(31)
#define Km4 parameter(32)

	dydt[compartment.index()][variableIndex(0,6)] += 
		((kcat1 * E1 * (S - M1) / Km1) /
		(1 + (S / Km1) + (M1 / Km2))) -
		((kcat2 * E2 * (M1 - M2) / Km3) /
		(1 + (M1 / Km3) + (M2 / Km4)));

#define kcat3 parameter(33)
#define Km5 parameter(34)
#define Km6 parameter(35)


	dydt[compartment.index()][variableIndex(0,7)] += 
		((kcat2 * E2 * (M1 - M2) / Km3) /
		(1 + (M1 / Km3) + (M2 / Km4))) -
		((kcat3 * E3 * (M2 - P) / Km5) /
		(1 + (M2 / Km5) + (P / Km6)));
		
}





