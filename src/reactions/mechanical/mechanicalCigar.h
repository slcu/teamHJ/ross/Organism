/**
 * Filename     : mechanicalCigar.h
 * Description  : Classes describing mechanics for bacteria (cigar shaped)
 * Author(s)    : Henrik Jonsson (henrik@thep.lu.se)
 * Created      : October 2003
 * Revision     : $Id:$
 */
#ifndef MECHANICALCIGAR_H
#define MECHANICALCIGAR_H

#include"../baseReaction.h"
#include<cmath>




//!WallSpringEllipstic simulates an unmovable wall (for a cell colony)
class WallSpringCigar : public BaseReaction {
  
 public:
  
	///
	/// @brief Main constructor
	///
	/// This is the main constructor which sets the parameters and variable
	/// indices that defines the reaction.
	///
	/// @param paraValue vector with parameters
	///
	/// @param indValue vector of vectors with variable indices
	///
	/// @see BaseReaction::createReaction(std::vector<double> &paraValue,...)
	///
  WallSpringCigar(std::vector<double> &paraValue, 
	     std::vector< std::vector<size_t> > &indValue );
  
	///
	/// @brief Derivative function for this reaction class
	///
	/// @see BaseReaction::derivs(Compartment &compartment,size_t species,...)
	///
  void derivs(Compartment &compartment,size_t varIndex,
	      std::vector< std::vector<double> > &y,
	      std::vector< std::vector<double> > &dydt );
};

class SpringAsymmetricCigar: public BaseReaction {
  
 public:
  
	///
	/// @brief Main constructor
	///
	/// This is the main constructor which sets the parameters and variable
	/// indices that defines the reaction.
	///
	/// @param paraValue vector with parameters
	///
	/// @param indValue vector of vectors with variable indices
	///
	/// @see BaseReaction::createReaction(std::vector<double> &paraValue,...)
	///
  SpringAsymmetricCigar(std::vector<double> &paraValue, 
			std::vector< std::vector<size_t> > &indValue );
  
	///
	/// @brief Derivative function for this reaction class
	///
	/// @see BaseReaction::derivs(Compartment &compartment,size_t species,...)
	///
  void derivs(Compartment &compartment,size_t varIndex,
	      std::vector< std::vector<double> > &y,
	      std::vector< std::vector<double> > &dydt ); 

  void updateCigaraDirection(size_t i, size_t j, size_t x_a, size_t y_a,  size_t x_b, size_t y_b,
			     double t_cell, double a_cell, double b_cell, double a_neigh,
			     const double n_aCell[], 
			     double& ForceCell, double thetaCell, 
			     const double n_aNeigh[], 
			     const double normal[], const double crossPoint[],
			     double& ForceNeigh, double thetaNeigh,
			     std::vector< std::vector<double> > &y,
			     std::vector< std::vector<double> > &dydt
			     );

  void updateCigarbDirection(size_t i, size_t j, size_t x_a, size_t y_a, size_t x_b, size_t y_b,
			     double t_cell, double a_cell, double b_cell, double a_neigh,
			     const double n_aCell[], 
			     double& ForceCell, double thetaCell, 
			     const double n_aNeigh[], 
			     const double normal[], const double crossPoint[],
			     double& ForceNeigh,double thetaNeigh,
			     std::vector< std::vector<double> > &y,
			     std::vector< std::vector<double> > &dydt
			     );
    };

///
/// @brief Describes repulsive forces for a cell-cell interaction w Cigar shape
///
class SpringRepulsiveCigar: public BaseReaction {
  
 public:
  
	///
	/// @brief Main constructor
	///
	/// This is the main constructor which sets the parameters and variable
	/// indices that defines the reaction.
	///
	/// @param paraValue vector with parameters
	///
	/// @param indValue vector of vectors with variable indices
	///
	/// @see BaseReaction::createReaction(std::vector<double> &paraValue,...)
	///
  SpringRepulsiveCigar(std::vector<double> &paraValue, 
		       std::vector< std::vector<size_t> > &indValue );
  
	///
	/// @brief Derivative function for this reaction class
	///
	/// @see BaseReaction::derivs(Compartment &compartment,size_t species,...)
	///
  void derivs(Compartment &compartment,size_t varIndex,
	      std::vector< std::vector<double> > &y,
	      std::vector< std::vector<double> > &dydt ); 
};

//!SpringElasticWallEllipse simulates an elastic wall (for a cell colony)
class SpringWallCigar2 : public BaseReaction {
  
 public:
  
	///
	/// @brief Main constructor
	///
	/// This is the main constructor which sets the parameters and variable
	/// indices that defines the reaction.
	///
	/// @param paraValue vector with parameters
	///
	/// @param indValue vector of vectors with variable indices
	///
	/// @see BaseReaction::createReaction(std::vector<double> &paraValue,...)
	///
  SpringWallCigar2(std::vector<double> &paraValue, 
		   std::vector< std::vector<size_t> > &indValue );
  
	///
	/// @brief Derivative function for this reaction class
	///
	/// @see BaseReaction::derivs(Compartment &compartment,size_t species,...)
	///
  void derivs(Compartment &compartment,size_t varIndex,
	      std::vector< std::vector<double> > &y,
	      std::vector< std::vector<double> > &dydt );
};

//!SpringElasticWallEllipse simulates an elastic wall (for a cell colony)
class SpringElasticWallCigar2 : public BaseReaction {
  
 private:

  std::vector< std::vector<double> > xW_;
  std::vector< std::vector<double> > dxW_;
  double dRelax_;
  
 public:
  
	///
	/// @brief Main constructor
	///
	/// This is the main constructor which sets the parameters and variable
	/// indices that defines the reaction.
	///
	/// @param paraValue vector with parameters
	///
	/// @param indValue vector of vectors with variable indices
	///
	/// @see BaseReaction::createReaction(std::vector<double> &paraValue,...)
	///
  SpringElasticWallCigar2(std::vector<double> &paraValue, 
			  std::vector< std::vector<size_t> > &indValue );
  
	///
	/// @brief Derivative function for this reaction class
	///
	/// @see BaseReaction::derivs(Compartment &compartment,size_t species,...)
	///
  void derivs(Compartment &compartment,size_t varIndex,
	      std::vector< std::vector<double> > &y,
	      std::vector< std::vector<double> > &dydt );

  void update(double h, double t,
							std::vector< std::vector<double> > &y);
  void print(std::ofstream &os);
};

///
/// @brief LagrangianCigar simulates a cigar shaped cell using a Lagrangian approach
///
class LagrangianCigar : public BaseReaction {
  
 public:
  
	///
	/// @brief Main constructor
	///
	/// This is the main constructor which sets the parameters and variable
	/// indices that defines the reaction.
	///
	/// @param paraValue vector with parameters
	///
	/// @param indValue vector of vectors with variable indices
	///
	/// @see BaseReaction::createReaction(std::vector<double> &paraValue,...)
	///
  LagrangianCigar(std::vector<double> &paraValue, 
									std::vector< std::vector<size_t> > &indValue );
  
	///
	/// @brief Derivative function for this reaction class
	///
	/// @see BaseReaction::derivs(Compartment &compartment,size_t species,...)
	///
  void derivs(Compartment &compartment,size_t varIndex,
							std::vector< std::vector<double> > &y,
							std::vector< std::vector<double> > &dydt );
};

///
/// @brief LagrangianWallCigar simulates a cigar shaped cell-wall interaction using a Lagrangian approach
///
class LagrangianWallCigar : public BaseReaction {
  
 public:
  
	///
	/// @brief Main constructor
	///
	/// This is the main constructor which sets the parameters and variable
	/// indices that defines the reaction.
	///
	/// @param paraValue vector with parameters
	///
	/// @param indValue vector of vectors with variable indices
	///
	/// @see BaseReaction::createReaction(std::vector<double> &paraValue,...)
	///
  LagrangianWallCigar(std::vector<double> &paraValue, 
		   std::vector< std::vector<size_t> > &indValue );
  
	///
	/// @brief Derivative function for this reaction class
	///
	/// @see BaseReaction::derivs(Compartment &compartment,size_t species,...)
	///
  void derivs(Compartment &compartment,size_t varIndex,
	      std::vector< std::vector<double> > &y,
	      std::vector< std::vector<double> > &dydt );
};
//!PerforatedLagrangianCigar simulates a cigar shaped cell using a Lagrangian approach
class PerforatedLagrangianWallCigar : public BaseReaction {
  
 public:
  
	///
	/// @brief Main constructor
	///
	/// This is the main constructor which sets the parameters and variable
	/// indices that defines the reaction.
	///
	/// @param paraValue vector with parameters
	///
	/// @param indValue vector of vectors with variable indices
	///
	/// @see BaseReaction::createReaction(std::vector<double> &paraValue,...)
	///
  PerforatedLagrangianWallCigar(std::vector<double> &paraValue, 
		   std::vector< std::vector<size_t> > &indValue );
  
	///
	/// @brief Derivative function for this reaction class
	///
	/// @see BaseReaction::derivs(Compartment &compartment,size_t species,...)
	///
  void derivs(Compartment &compartment,size_t varIndex,
	      std::vector< std::vector<double> > &y,
	      std::vector< std::vector<double> > &dydt );
};

//!LagrangianCigarNew same as LagrangianCigar but with a better overlap algorithm
class LagrangianCigarNew : public BaseReaction {
  
 public:
  
	///
	/// @brief Main constructor
	///
	/// This is the main constructor which sets the parameters and variable
	/// indices that defines the reaction.
	///
	/// @param paraValue vector with parameters
	///
	/// @param indValue vector of vectors with variable indices
	///
	/// @see BaseReaction::createReaction(std::vector<double> &paraValue,...)
	///
  LagrangianCigarNew(std::vector<double> &paraValue, 
		   std::vector< std::vector<size_t> > &indValue );
  
	///
	/// @brief Derivative function for this reaction class
	///
	/// @see BaseReaction::derivs(Compartment &compartment,size_t species,...)
	///
  void derivs(Compartment &compartment,size_t varIndex,
	      std::vector< std::vector<double> > &y,
	      std::vector< std::vector<double> > &dydt );
};
//!Same as LagrangianCigarNew but it allows for  explicit friction parameter
class LagrangianCigarFriction : public BaseReaction {
  
 public:
  
	///
	/// @brief Main constructor
	///
	/// This is the main constructor which sets the parameters and variable
	/// indices that defines the reaction.
	///
	/// @param paraValue vector with parameters
	///
	/// @param indValue vector of vectors with variable indices
	///
	/// @see BaseReaction::createReaction(std::vector<double> &paraValue,...)
	///
  LagrangianCigarFriction(std::vector<double> &paraValue, 
		   std::vector< std::vector<size_t> > &indValue );
  
	///
	/// @brief Derivative function for this reaction class
	///
	/// @see BaseReaction::derivs(Compartment &compartment,size_t species,...)
	///
  void derivs(Compartment &compartment,size_t varIndex,
	      std::vector< std::vector<double> > &y,
	      std::vector< std::vector<double> > &dydt );
};

//!LagrangianCigar simulates a cigar shaped cell using a Lagrangian approach
class LagrangianWallCigarNew : public BaseReaction {
  
 public:
  
	///
	/// @brief Main constructor
	///
	/// This is the main constructor which sets the parameters and variable
	/// indices that defines the reaction.
	///
	/// @param paraValue vector with parameters
	///
	/// @param indValue vector of vectors with variable indices
	///
	/// @see BaseReaction::createReaction(std::vector<double> &paraValue,...)
	///
  LagrangianWallCigarNew(std::vector<double> &paraValue, 
		   std::vector< std::vector<size_t> > &indValue );
  
	///
	/// @brief Derivative function for this reaction class
	///
	/// @see BaseReaction::derivs(Compartment &compartment,size_t species,...)
	///
  void derivs(Compartment &compartment,size_t varIndex,
	      std::vector< std::vector<double> > &y,
	      std::vector< std::vector<double> > &dydt );
};

//!Simulates a cigar-wall interaction based on a potential approach
class MultipleWallCigar : public BaseReaction {
  
 public:
  
	///
	/// @brief Main constructor
	///
	/// This is the main constructor which sets the parameters and variable
	/// indices that defines the reaction.
	///
	/// @param paraValue vector with parameters
	///
	/// @param indValue vector of vectors with variable indices
	///
	/// @see BaseReaction::createReaction(std::vector<double> &paraValue,...)
	///
  MultipleWallCigar(std::vector<double> &paraValue, 
		    std::vector< std::vector<size_t> > &indValue );
  
	///
	/// @brief Derivative function for this reaction class
	///
	/// @see BaseReaction::derivs(Compartment &compartment,size_t species,...)
	///
  void derivs(Compartment &compartment,size_t varIndex,
	      std::vector< std::vector<double> > &y,
	      std::vector< std::vector<double> > &dydt );
};

class CigarAdhesion : public BaseReaction {
  
 public:
  
	///
	/// @brief Main constructor
	///
	/// This is the main constructor which sets the parameters and variable
	/// indices that defines the reaction.
	///
	/// @param paraValue vector with parameters
	///
	/// @param indValue vector of vectors with variable indices
	///
	/// @see BaseReaction::createReaction(std::vector<double> &paraValue,...)
	///
  CigarAdhesion(std::vector<double> &paraValue, 
		    std::vector< std::vector<size_t> > &indValue );
  
	///
	/// @brief Derivative function for this reaction class
	///
	/// @see BaseReaction::derivs(Compartment &compartment,size_t species,...)
	///
  void derivs(Compartment &compartment,size_t varIndex,
	      std::vector< std::vector<double> > &y,
	      std::vector< std::vector<double> > &dydt );
};

///
/// @brief The class WallCigar3D defines a three-dimensional wall  
///
///This class takes at least 12 arguments, where the first three are the force
///constant @f$k@f$, the second is the "cigar-radius" @f$b@f$ and the third
///allows the potential from the wall to start at a distance @f$D@f$ from the
///wall. The following arguments are three or more coordinates defining the
///wall boundaries.
///
///NOTE: A wall must be defined as a convex polygon!
class WallCigar3D : public BaseReaction {
  
 public:
	///
	/// @brief Main constructor
	///
	/// This is the main constructor which sets the parameters and variable
	/// indices that defines the reaction.
	///
	/// @param paraValue vector with parameters
	///
	/// @param indValue vector of vectors with variable indices
	///
	/// @see BaseReaction::createReaction(std::vector<double> &paraValue,...)
	///
  WallCigar3D(std::vector<double> &paraValue, 
		    std::vector< std::vector<size_t> > &indValue );
	///
	/// @brief Derivative function for this reaction class
	///
	/// @see BaseReaction::derivs(Compartment &compartment,size_t species,...)
	///
  void derivs(Compartment &compartment,size_t varIndex,
	      std::vector< std::vector<double> > &y,
	      std::vector< std::vector<double> > &dydt );
};
#endif // MECHANICALCIGAR_H



