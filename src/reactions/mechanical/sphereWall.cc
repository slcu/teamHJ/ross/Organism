//
// Filename     : sphereWall.cc
// Description  : Classes describing forces from walls applied to spherical cells
// Author(s)    : Henrik Jonsson (henrik@thep.lu.se)
// Created      : April 2010
// Revision     : $Id:$
//
//#include <map>
//#include <iomanip>

#include"../baseReaction.h"
#include "sphereWall.h"

namespace Sphere {

  SphericalWall::SphericalWall(std::vector<double> &paraValue, 
			       std::vector< std::vector<size_t> > 
			       &indValue ) {
    
    //Do some checks on the parameters and variable indeces
    //////////////////////////////////////////////////////////////////////
    if (paraValue.size()!=5) {
      std::cerr << "SphericalWall::SphericalWall() "
		<< "Uses 5 parameters k, X, Y, Z and R." << std::endl
		<< "In 2D simulations, the Z will be disregarded." << std::endl;
      exit(0);
    }
    if (indValue.size()) { 
      std::cerr << "SphericalWall::SphericalWall() "
		<< "No variable indices are used.\n";
      exit(0);
    }
    //Set the variable values
    //////////////////////////////////////////////////////////////////////
    setId("sphere::sphericalWall");
    setParameter(paraValue);  
    setVariableIndex(indValue);
    
    //Set the parameter identities
    //////////////////////////////////////////////////////////////////////
    std::vector<std::string> tmp( numParameter() );
    tmp[0] = "k";
    tmp[1] = "X";
    tmp[2] = "Y";
    tmp[3] = "Z";
    tmp[4] = "R";
    setParameterId( tmp );
  }
  
  void SphericalWall::derivs(Compartment &compartment,size_t varIndex,
			     std::vector< std::vector<double> > &y,
			      std::vector< std::vector<double> > &dydt ) {
    
    size_t i=compartment.index();
    size_t dimension = compartment.numTopologyVariable()-1;
    // calculate the overlap
    
    
    double d = y[i][dimension] + parameter(4);      
    double r = 0.;
    for(size_t dim=0 ; dim<dimension ; dim++ )
      r += (y[i][varIndex+dim]-parameter(dim+1))*
	(y[i][varIndex+dim]-parameter(dim+1));
    r = sqrt(r);
    
    double rCoff = parameter(0)*(1.-(d/r));
    
    // do the update
    //rCoff<0.0: if parameter(0)>0 -> inside (act), if parameter(0)<0 -> outside (act)
    if (rCoff<0.0) {
      for (size_t dim=0; dim<dimension; ++dim)
	dydt[i][varIndex+dim] -= parameter(0)*rCoff*(y[i][varIndex+dim]-parameter(dim+1));
    }
  }
  
} //namespace Sphere
