///
/// Filename     : sphereBudSpring.h
/// Description  : Classes describing springs between spherical cells
/// Author(s)    : Henrik Jonsson (henrik@thep.lu.se)
/// Created      : March 2010
/// Revision     : $Id:$
///
#ifndef SPHEREBUDSPRING_H
#define SPHEREBUDSPRING_H

#include"../baseReaction.h"
#include<cmath>

//!SpringAsymmetricSphereBud uses an asymmetric(adh,rep) spring between comp.
class SpringAsymmetricSphereBud : public BaseReaction {
  
 public:
  
	///
	/// @brief Main constructor
	///
	/// This is the main constructor which sets the parameters and variable
	/// indices that defines the reaction.
	///
	/// @param paraValue vector with parameters
	///
	/// @param indValue vector of vectors with variable indices
	///
	/// @see BaseReaction::createReaction(std::vector<double> &paraValue,...)
	///
  SpringAsymmetricSphereBud(std::vector<double> &paraValue, 
			    std::vector< std::vector<size_t> > &indValue );
  
  void derivs(Compartment &compartment,size_t varIndex,
	      std::vector< std::vector<double> > &y,
	      std::vector< std::vector<double> > &dydt );
};

#endif //SPHEREBUDSPRING_H
