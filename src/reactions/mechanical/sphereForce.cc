//
// Filename     : sphereForce.cc
// Description  : Classes describing forces applied to spherical cells
// Author(s)    : Henrik Jonsson (henrik@thep.lu.se)
// Created      : April 2010
// Revision     : $Id:$
//
//#include <map>
//#include <iomanip>

#include"../baseReaction.h"
#include "sphereForce.h"

namespace Sphere {

  ForceDirection::ForceDirection(std::vector<double> &paraValue, 
				 std::vector< std::vector<size_t> > 
				 &indValue ) {
    
    //Do some checks on the parameters and variable indeces
    //////////////////////////////////////////////////////////////////////
    if (paraValue.size()!=2 || (paraValue[1]!=0 && paraValue[1]!=1)) {
      std::cerr << "ForceDirection::ForceDirection() "
		<< "Uses two parameters c linearFlag (0,1).\n";
      exit(0);
    }
    if (indValue.size()!=1 || indValue[0].size()!=1) { 
      std::cerr << "ForceDirection::ForceDirection() "
		<< "One variable index specifying direction is used.\n";
      exit(0);
    }
    //Set the variable values
    //////////////////////////////////////////////////////////////////////
    setId("Sphere::forceDirection");
    setParameter(paraValue);  
    setVariableIndex(indValue);
    
    //Set the parameter identities
    //////////////////////////////////////////////////////////////////////
    std::vector<std::string> tmp( numParameter() );
    tmp[0] = "c";
    tmp[1] = "linearFlag";
    setParameterId( tmp );
  }
  
  void ForceDirection::derivs(Compartment &compartment,size_t varIndex,
			      std::vector< std::vector<double> > &y,
			      std::vector< std::vector<double> > &dydt ) {
    
    size_t i=compartment.index();
    size_t numCompartment = y.size();
    size_t direction = variableIndex(0,0);
    assert(variableIndex(0,0)<compartment.numTopologyVariable()-1);//within spatial variables

    static double meanPos=0.0;
    if (i==0) {//update meanPos
      meanPos=0.0;
      for (size_t ii=0; ii<numCompartment; ++ii)
	meanPos += y[ii][direction];
      meanPos /= numCompartment;
    }
    if (parameter(1)==0) {
      if ((y[i][direction]-meanPos)>0.0)
	dydt[i][direction] += parameter(0);
      else
	dydt[i][direction] -= parameter(0);
    }
    else
      dydt[i][direction] += parameter(0)*(y[i][direction]-meanPos);
  }

  ForceCellDirection::ForceCellDirection(std::vector<double> &paraValue, 
					 std::vector< std::vector<size_t> > 
					 &indValue ) {
    
    //Do some checks on the parameters and variable indeces
    //////////////////////////////////////////////////////////////////////
    if (paraValue.size()!=1) {
      std::cerr << "ForceCellDirection::ForceCellDirection() "
		<< "Uses one parameter c.\n";
      exit(0);
    }
    if (indValue.size()!=1 || indValue[0].size()!=2) { 
      std::cerr << "ForceCellDirection::ForceCellDirection() "
		<< "Two variable indices specifying forceFlag and direction vector start"
		<< " is used.\n";
      exit(0);
    }
    //Set the variable values
    //////////////////////////////////////////////////////////////////////
    setId("Sphere::forceCellDirection");
    setParameter(paraValue);  
    setVariableIndex(indValue);
    
    //Set the parameter identities
    //////////////////////////////////////////////////////////////////////
    std::vector<std::string> tmp( numParameter() );
    tmp[0] = "c";
    setParameterId( tmp );
  }
  
  void ForceCellDirection::derivs(Compartment &compartment,size_t varIndex,
				  std::vector< std::vector<double> > &y,
				  std::vector< std::vector<double> > &dydt ) {
    
    size_t i=compartment.index();
    size_t dimension = compartment.numTopologyVariable()-1;
    size_t forceFlag = variableIndex(0,0);
    size_t direction = variableIndex(0,1);
    
    if (y[i][forceFlag]) {
      for (size_t d=0; d<dimension; ++d)
	dydt[i][d] += parameter(0)*y[i][direction+d];
    }
  }  

  ForceVelocityDirection::ForceVelocityDirection(std::vector<double> &paraValue, 
					 std::vector< std::vector<size_t> > 
					 &indValue ) 
  {
    //
    //Do some checks on the parameters and variable indeces
    //
    if (paraValue.size()!=1) {
      std::cerr << "Sphere::ForceVelocityDirection::ForceVelocityDirection() "
		<< "Uses one parameter c." << std::endl;
      exit(0);
    }
    if (indValue.size()!=1 || indValue[0].size()!=1) { 
      std::cerr << "Sphere::ForceVelocityDirection::ForceVelocityDirection() "
		<< "One variable index specifying forceFlag is used. Direction vector"
		<< " (from velocities) is assumed to start from index 0." << std::endl;
      exit(0);
    }
    //
    //Set the variable values
    //
    setId("sphere::forceVelocityDirection");
    setParameter(paraValue);  
    setVariableIndex(indValue);
    //
    //Set the parameter identities
    //
    std::vector<std::string> tmp( numParameter() );
    tmp[0] = "c";
    setParameterId( tmp );
  }
  
  void ForceVelocityDirection::derivs(Compartment &compartment,size_t varIndex,
				      std::vector< std::vector<double> > &y,
				      std::vector< std::vector<double> > &dydt ) {
    
    size_t i=compartment.index();
    size_t dimension = compartment.numTopologyVariable()-1;
    size_t forceFlag = variableIndex(0,0);
    
    if (y[i][forceFlag]) {
      std::vector<double> force(dimension);
      double length=0.0;
      for (size_t d=0; d<dimension; ++d) {
	force[d] = dydt[i][d];
	length += force[d]*force[d];
      }
      length = parameter(0)/std::sqrt(length);
      for (size_t d=0; d<dimension; ++d)
	dydt[i][d] += length*force[d];
    }
  }  

	ForceRadial::
	ForceRadial(std::vector<double> &paraValue, 
							std::vector< std::vector<size_t> > 
							&indValue )
	{ 
		// Do some checks on the parameters and variable indeces
		//
		if( paraValue.size()!=2 || ( paraValue[1]!=0 && paraValue[1]!=1) ) {
			std::cerr << "Sphere::ForceRadial::ForceRadial() "
								<< "Uses two parameters k_r and r_pow (0,1)\n";
			exit(EXIT_FAILURE);
		}  
		if( indValue.size() != 0 ) {
			std::cerr << "Sphere::ForceRadial::ForceRadial() "
								<< "No variable index is used (x,[y,[z]] updated)."
								<< std::endl;
			exit(EXIT_FAILURE);
		}
		// Set the variable values
		//
		setId("sphere::forceRadial");
		setParameter(paraValue);  
		setVariableIndex(indValue);
		
		// Set the parameter identities
		//
		std::vector<std::string> tmp( numParameter() );
		tmp.resize( numParameter() );
		tmp[0] = "k_r";
		tmp[0] = "r_pow";
		setParameterId( tmp );
	}
	
  void ForceRadial::
	derivs(Compartment &compartment,size_t varIndex,
				 std::vector< std::vector<double> > &y,
				 std::vector< std::vector<double> > &dydt )
	{
		size_t i=compartment.index();
    size_t dimension = compartment.numTopologyVariable()-1;
		
		double fac=parameter(0);
		if( parameter(1)==0.0 ) {
			double r=0.0;
			for( size_t d=0 ; d<dimension ; ++d )
				r += y[i][d]*y[i][d];
			if( r>0.0 )
				r = std::sqrt(r);
			if( r>0.0 )
				fac /= r;
			else
				fac=0.0;
		}
		for( size_t d=0 ; d<dimension ; ++d )
			dydt[i][d] += fac*y[i][d];
	}
	
} //namespace Sphere
