/**
 * Filename     : mechanical.cc
 * Description  : Classes describing updates due to mechanical interaction
 * Author(s)    : Henrik Jonsson (henrik@thep.lu.se)
 * Created      : October 2003
 * Revision     : $Id: mechanical.cc 311 2007-11-08 08:40:54Z pontus $
 */

#include <map>
#include <iomanip>

#include "../../common/vector3.h"
#include"../mechanical.h"
#include"../baseReaction.h"
#include "../../common/mySignal.h"

Spring::Spring(std::vector<double> &paraValue, 
	       std::vector< std::vector<size_t> > 
	       &indValue ) {
  
  //Do some checks on the parameters and variable indeces
  //////////////////////////////////////////////////////////////////////
  if( paraValue.size()!=2 ) {
    std::cerr << "Spring::Spring() "
	      << "Uses two parameters r_relax K_force.\n";
    exit(0);
  }
  if( indValue.size() ) { 
    std::cerr << "Spring::Spring() "
	      << "No variable indeces used.\n";
    exit(0);
  }
  //Set the variable values
  //////////////////////////////////////////////////////////////////////
  setId("spring");
  setParameter(paraValue);  
  setVariableIndex(indValue);
  
  //Set the parameter identities
  //////////////////////////////////////////////////////////////////////
  std::vector<std::string> tmp( numParameter() );
  tmp[0] = "r_relax";
  tmp[1] = "K_force";
  setParameterId( tmp );
}

void Spring::derivs(Compartment &compartment,size_t varIndex,
		    std::vector< std::vector<double> > &y,
		    std::vector< std::vector<double> > &dydt ) {
  
  size_t i=compartment.index();
  size_t dimension = compartment.numTopologyVariable()-1;
  
  for( size_t n=0 ; n<compartment.numNeighbor() ; n++ ) {
    size_t j=compartment.neighbor(n);
    //Only update if compartments index less than neighbors
    if( i<j ) {
      double d = parameter(0)*( y[i][dimension] + y[j][dimension] );      
      double r = 0.;
      for(size_t dim=0 ; dim<dimension ; dim++ )
	r += (y[i][varIndex+dim]-y[j][varIndex+dim])*
	  (y[i][varIndex+dim]-y[j][varIndex+dim]);
      r = sqrt(r);
      
      if( r<=0. ) {
	std::cerr << "Spring::derivs() "
		  << "Two cells on top of each other, exiting\n";
	std::cerr << "r=" << r << ", d=" << d << " (" << i << "," << j 
		  << ")\n";
	exit(-1);
      }
      double rCoff = parameter(1)*(1.-(d/r));
      //Update for each dimension
      for(size_t dim=0 ; dim<dimension ; dim++ ) {
	double div = (y[i][varIndex+dim]-y[j][varIndex+dim])*rCoff;
	dydt[i][varIndex+dim] -= div;
	dydt[j][varIndex+dim] += div;
      }
    }
  }
}

SpringAsymmetric::SpringAsymmetric(std::vector<double> &paraValue, 
				   std::vector< std::vector<size_t> > 
				   &indValue ) {
  
  //Do some checks on the parameters and variable indeces
  //////////////////////////////////////////////////////////////////////
  if( paraValue.size()!=3 ) {
    std::cerr << "SpringAsymmetric::SpringAsymmetric() "
	      << "Uses three parameters r_relax K_force K_adhesion.\n";
    exit(0);
  }
  if( indValue.size() ) {
    std::cerr << "SpringAsymmetric::SpringAsymmetric() "
	      << "No variable indeces is used.\n";
    exit(0);
  }
  //Set the variable values
  //////////////////////////////////////////////////////////////////////
  setId("springAsymmetric");
  setParameter(paraValue);  
  setVariableIndex(indValue);
  
  //Set the parameter identities
  //////////////////////////////////////////////////////////////////////
  std::vector<std::string> tmp( numParameter() );
  tmp[0] = "r_relax";
  tmp[1] = "K_force";
  tmp[2] = "K_adhFrac";
  setParameterId( tmp );
}

void SpringAsymmetric::derivs(Compartment &compartment,size_t varIndex,
			      std::vector< std::vector<double> > &y,
			      std::vector< std::vector<double> > &dydt ) {
  
  size_t i=compartment.index();
  size_t dimension = compartment.numTopologyVariable()-1;

  for( size_t n=0 ; n<compartment.numNeighbor() ; n++ ) {
    size_t j=compartment.neighbor(n);
    //Only update if compartments index less than neighbors
    if( i<j ) {
      double d = parameter(0)*( y[i][dimension] + y[j][dimension] );      
      double r = 0.;
      for(size_t dim=0 ; dim<dimension ; dim++ )
	r += (y[i][varIndex+dim]-y[j][varIndex+dim])*
	  (y[i][varIndex+dim]-y[j][varIndex+dim]);
      r = sqrt(r);
      
      if( r<=0. ) {
	std::cerr << "SpringAsymmetric::derivs() "
		  << "Two cells on top of each other, exiting\n";
	std::cerr << "r=" << r << ", d=" << d << " (" << i << "," << j 
		  << ")\n";
	exit(-1);
      }
      double rCoff = parameter(1)*(1.-(d/r));
      if( r>d )
	rCoff *=parameter(2);
      //Update for each dimension
      for(size_t dim=0 ; dim<dimension ; dim++ ) {
	double div = (y[i][varIndex+dim]-y[j][varIndex+dim])*rCoff;
	dydt[i][varIndex+dim] -= div;
	dydt[j][varIndex+dim] += div;
      }
    }
  }
}

void SpringAsymmetric::derivsWithAbs(Compartment &compartment,size_t varIndex,
				     DataMatrix &y,
				     DataMatrix &dydt,
				     DataMatrix &sdydt)
{  
  return derivs(compartment, varIndex, y, dydt);
}

SpringTruncated::SpringTruncated(std::vector<double> &paraValue, 
				 std::vector< std::vector<size_t> > 
				 &indValue ) {
  
  //Do some checks on the parameters and variable indeces
  //////////////////////////////////////////////////////////////////////
  if( paraValue.size()!=3 ) {
    std::cerr << "SpringTruncated::SpringTruncated() "
	      << "Uses three parameters r_relax K_force K_adhFrac.\n";
    exit(0);
  }
  if( indValue.size() ) {
    std::cerr << "SpringTruncated::SpringTruncated() "
	      << "No variable indeces is used.\n";
    exit(0);
  }
  //Set the variable values
  //////////////////////////////////////////////////////////////////////
  setId("springTruncated");
  setParameter(paraValue);  
  setVariableIndex(indValue);
  
  //Set the parameter identities
  //////////////////////////////////////////////////////////////////////
  std::vector<std::string> tmp( numParameter() );
  tmp[0] = "r_relax";
  tmp[1] = "K_force";
  tmp[2] = "K_adhFrac";
  
  setParameterId( tmp );
}

void SpringTruncated::derivs(Compartment &compartment,size_t varIndex,
			     std::vector< std::vector<double> > &y,
			     std::vector< std::vector<double> > &dydt ) {
  
  size_t i=compartment.index();
  size_t dimension = compartment.numTopologyVariable()-1;

  for( size_t n=0 ; n<compartment.numNeighbor() ; n++ ) {
    size_t j=compartment.neighbor(n);
    //Only update if compartments index less than neighbors
    if( i<j ) {
      double d = parameter(0)*( y[i][dimension] + y[j][dimension] );      
      double r = 0.;
      for(size_t dim=0 ; dim<dimension ; dim++ )
	r += (y[i][varIndex+dim]-y[j][varIndex+dim])*
	  (y[i][varIndex+dim]-y[j][varIndex+dim]);
      r = sqrt(r);
      
      if( r<=0. ) {
	std::cerr << "SpringTruncated::derivs() "
		  << "Two cells on top of each other, exiting\n";
	std::cerr << "r=" << r << ", d=" << d << " (" << i << "," << j 
		  << ")\n";
	exit(-1);
      }
      double rCoff = parameter(1)*(1.-(d/r));//Normal spring
      if( r>d ) {// multiply with truncation coefficient
	rCoff *= parameter(2); //different attracting force than repelling
	//Caveat: Can be done more nice using the mu,T parameters
	double mu = d*d*0.05;
	double T = mu;
	double aux = exp( -(.5*(d-r)*(d-r) - mu)/T );
	rCoff *= aux/(1. + aux);
      }
      //Update for each dimension
      for(size_t dim=0 ; dim<dimension ; dim++ ) {
	double div = (y[i][varIndex+dim]-y[j][varIndex+dim])*rCoff;
	dydt[i][varIndex+dim] -= div;
	dydt[j][varIndex+dim] += div;
      }
    }
  }
}

SpringAdhesionRestrictedSphere::
SpringAdhesionRestrictedSphere(std::vector<double> &paraValue, 
			       std::vector< std::vector<size_t> > 
			       &indValue ) {
  
  //Do some checks on the parameters and variable indeces
  //
  if( paraValue.size()!=2 ) {
    std::cerr << "SpringAdhesionRestrictedSphere::"
	      << "SpringAdhesionRestrictedSphere() "
	      << "Uses three parameters r_relax K_adh.\n";
    exit(0);
  }
  if( indValue.size() != 1 || indValue[0].size() != 1 ) {
    std::cerr << "SpringAdhesionRestrictedSphere::"
	      << "SpringAdhesionRestrictedSphere() "
	      << "One variable index for restricting molecule used.\n";
    exit(0);
  }
  //Set the variable values
  //
  setId("springAdhesionRestrictedSphere");
  setParameter(paraValue);  
  setVariableIndex(indValue);
  
  //Set the parameter identities
  //
  std::vector<std::string> tmp( numParameter() );
  tmp[0] = "r_relax";
  tmp[1] = "K_adh";
  
  setParameterId( tmp );
}

void SpringAdhesionRestrictedSphere::
derivs(Compartment &compartment,size_t varIndex,
       std::vector< std::vector<double> > &y,
       std::vector< std::vector<double> > &dydt ) {
  
  size_t i=compartment.index();
  size_t dimension = compartment.numTopologyVariable()-1;
  
  for( size_t n=0 ; n<compartment.numNeighbor() ; n++ ) {
    size_t j=compartment.neighbor(n);
    //Only update if compartments index less than neighbors
    if( i<j ) {
      double d = parameter(0)*( y[i][dimension] + y[j][dimension] );      
      double r = 0.;
      for(size_t dim=0 ; dim<dimension ; dim++ )
	r += (y[i][varIndex+dim]-y[j][varIndex+dim])*
	  (y[i][varIndex+dim]-y[j][varIndex+dim]);
      r = sqrt(r);
      
      if( r<=0. ) {
	std::cerr << "SpringAdhesionRestrictedSphere::derivs() "
		  << "Two cells on top of each other, exiting\n";
	std::cerr << "r=" << r << ", d=" << d << " (" << i << "," << j 
		  << ")\n";
	exit(-1);
      }
      double rCoff = parameter(1)*
	(y[i][variableIndex(0,0)]+y[j][variableIndex(0,0)])*
	(1.-(d/r));//Normal spring
      if( r>d ) {//Only adhesion 
	//Update for each dimension
	for(size_t dim=0 ; dim<dimension ; dim++ ) {
	  double div = (y[i][varIndex+dim]-y[j][varIndex+dim])*rCoff;
	  dydt[i][varIndex+dim] -= div;
	  dydt[j][varIndex+dim] += div;
	}
      }
    }
  }
}

SpringAsymmetricSphere::SpringAsymmetricSphere(std::vector<double> &paraValue, 
					       std::vector< std::vector<size_t> > 
					       &indValue ) {
  
  //Do some checks on the parameters and variable indeces
  //////////////////////////////////////////////////////////////////////
  if( paraValue.size()!=4 ) {
    std::cerr << "SpringAsymmetricSphere::SpringAsymmetricSphere() "
	      << "Uses three parameters r_relax K_force K_adhesion R_spere.\n";
    exit(0);
  }
  if( indValue.size() ) {
    std::cerr << "SpringAsymmetricSphere::SpringAsymmetricSphere() "
	      << "No variable indeces is used.\n";
    exit(0);
  }
  //Set the variable values
  //////////////////////////////////////////////////////////////////////
  setId("springAsymmetricSphere");
  setParameter(paraValue);  
  setVariableIndex(indValue);
  
  //Set the parameter identities
  //////////////////////////////////////////////////////////////////////
  std::vector<std::string> tmp( numParameter() );
  tmp[0] = "r_relax";
  tmp[1] = "K_force";
  tmp[2] = "K_adhFrac";
  tmp[3] = "R_sphere";
  setParameterId( tmp );
}

//! Derivative for a mechanical interaction from springs on a sphere surface.
/*! Deriving the time derivative contribution from a mechanical spring
  using the values in y and add the results to dydt. The calculations
  are made assuming a 2D sphere surface world (3D but r is constant).
*/
void SpringAsymmetricSphere::derivs(Compartment &compartment,size_t varIndex,
																		std::vector< std::vector<double> > &y,
																		std::vector< std::vector<double> > 
																		&dydt ) 
{
  if( compartment.numNeighbor()<1 ) return;
  size_t i=compartment.index();
  double R = parameter(3);
	size_t dimension = compartment.numTopologyVariable()-1;
	size_t rCol = dimension;
  //double eps=0.1;
  
  //Make sure cell position is on the sphere
  double norm=0.0;
  for(size_t dim=0 ; dim<dimension ; ++dim )
    norm += y[i][varIndex+dim]*y[i][varIndex+dim];
  if( norm<0.75*R*R || norm>1.25*R*R ) {
    std::cerr << "SpringAsymmetricSphereCylinder::derivsSphere() "
							<< " Position vector too far from sphere.\n";
    exit(-1);
  }
  norm = R/std::sqrt(norm);
  for(size_t dim=0 ; dim<dimension ; ++dim )
    y[i][varIndex+dim] *=norm;
  
  std::vector<double> A(compartment.numDimension()),B(compartment.numDimension())
    ,W(compartment.numDimension()),dX(compartment.numDimension());
  
  double AA=0.0;
  for(size_t dim=0 ; dim<dimension ; ++dim ) {
    A[dim] = y[i][varIndex+dim];
    AA += A[dim]*A[dim];
  }
  for( size_t n=0 ; n<compartment.numNeighbor() ; ++n ) {
    size_t j=compartment.neighbor(n);
    //Use values assuming on sphere
    norm=0.0;
    for(size_t dim=0 ; dim<dimension ; ++dim )
      norm += y[j][varIndex+dim]*y[j][varIndex+dim];
    if( norm<0.75*R*R || norm>1.25*R*R ) {
      std::cerr << "SpringAsymmetricSphereCylinder::derivsSphere() "
								<< " Neighbor position vector too far from sphere.\n";
      std::cerr << i << " " << j << " " << y[j][0] << " " << y[j][1] << " "
								<< y[j][2] << " " << std::sqrt(y[j][0]*y[j][0]+y[j][1]*y[j][1]
																							 +y[j][2]*y[j][2]) << "\n";
      exit(-1);
    }
    norm = R/std::sqrt(norm);
    double AB=0.,BB=0.;
    for(size_t dim=0 ; dim<dimension ; ++dim ) {
      B[dim] = y[j][varIndex+dim]*norm;//Using values on sphere
      AB += A[dim]*B[dim];
      BB += B[dim]*B[dim];
    }
    if( AB<=0.0 || AB>R*R ) {
      std::cerr << "SpringAsymmetricSphereCylinder::derivsSphere() "
								<< " Vector product strange.\n";
      exit(-1);
    }
    
    double rirj = parameter(0)*( y[i][rCol]+y[j][rCol] );
    double distance = R*std::acos(AB/(R*R));
    if( distance<=0. ) {
      std::cerr << "SpringAsymmetricSphere::derivs() "
								<< "Two cells on top of each other, exiting\n";
      std::cerr << "distance=" << distance << ", k*(r_i+r_j)=" << rirj 
								<< " (" << i << "," << j << ")\n";
      exit(-1);
    }
    double rCoff = parameter(1)*(distance-rirj);
    if( distance>rirj )
      rCoff *=parameter(2);
    //Define vector in plane orthogonal to A and pointing towards B
    //and of length 1.
    double norm=0.0;
    for(size_t dim=0 ; dim<dimension ; ++dim ) {
      W[dim] = B[dim] - AB*A[dim]/(R*R);
      norm += W[dim]*W[dim];
    }
    if( norm<=0.0 ) {
      std::cerr << "SpringAsymmetricSphere::derivs() "
								<< "No length on W vector\n";
      exit(-1);
    }
    norm = 1./std::sqrt(norm);
    for(size_t dim=0 ; dim<dimension ; ++dim )
      W[dim] *= norm;
    //Update for each dimension
    for(size_t dim=0 ; dim<dimension ; ++dim ) {
      dX[dim] += rCoff*W[dim];
    }
  }
  //Increase dX size to after projection onto the sphere be of length |dX|
  double length = 0.;
  //for(size_t dim=0 ; dim<compartment.numDimension() ; dim++ )
  //length += dX[dim]*dX[dim];
  //length = std::sqrt(length);
  //   if(length>y[i][compartment.numDimension()] )
  //     std::cerr << "dX["<<i<<"](l): " << length << "\n";
  //norm = R*std::tan(length/R)/length;
  //   if( std::fabs(norm-1)>eps )
  //     std::cerr << "dX["<<i<<"](n): " << norm << "\n";
  //for(size_t dim=0 ; dim<compartment.numDimension() ; dim++ )
  //dX[dim] *= norm;
//   double AdX=0.0;
//   for(size_t dim=0 ; dim<compartment.numDimension() ; dim++ )
//     AdX += A[dim]*dX[dim];
//   if( AdX>eps )
//     std::cerr << "AdX["<<i<<"]: " << AdX << "\n";

  //Project onto sphere surface (normalize A+dX to lye on sphere)
  std::vector<double> yTmp(dimension);
  length=0.;
  for(size_t dim=0 ; dim<dimension ; ++dim ) {
    yTmp[dim] = A[dim]+dX[dim]; 
    length += yTmp[dim]*yTmp[dim];
  }
  length = std::sqrt(length);
//   if( std::fabs(length-R)>eps )
//     std::cerr << "A+dX["<<i<<"](l): " << length << "\n";
  norm = R/length;
  for(size_t dim=0 ; dim<dimension ; ++dim )
    yTmp[dim] *= norm;
  //Add the final contribution for lying on the sphere
  for(size_t dim=0 ; dim<dimension ; ++dim )
    dydt[i][varIndex+dim] += yTmp[dim] - A[dim];
}

SpringAsymmetricCylinder::SpringAsymmetricCylinder(std::vector<double> 
																									 &paraValue, 
																									 std::vector< std::vector
																									 <size_t> > &indValue ) 
{
	// Do some checks on the parameters and variable indeces
  if( paraValue.size()!=4 ) {
    std::cerr << "SpringAsymmetricCylinder::SpringAsymmetricCylinder() "
							<< "Uses three parameters r_relax K_force K_adhesion "
							<< "R_cylinder.\n";
    exit(0);
  }
  if( indValue.size() ) {
    std::cerr << "SpringAsymmetricCylinder::SpringAsymmetricCylinder() "
							<< "No variable indeces is used.\n";
    exit(0);
  }

  // Set the variable values
  setId("springAsymmetricCylinder");
  setParameter(paraValue);  
  setVariableIndex(indValue);
  
  // Set the parameter identities
  std::vector<std::string> tmp( numParameter() );
  tmp[0] = "r_relax";
  tmp[1] = "K_force";
  tmp[2] = "K_adhFrac";
  tmp[3] = "R_cylinder";
  setParameterId( tmp );
}

//! Derivative for a mechanical interaction from springs on a cylinder surface.
/*! Deriving the time derivative contribution from a mechanical spring
  using the values in y and add the results to dydt. The calculations
  are made assuming a 2D cylinder surface world (3D but r is constant).
  The z-axis is assumed to be along the cylinder 'length'.
*/
void SpringAsymmetricCylinder::derivs(Compartment &compartment,size_t varIndex,
																			std::vector< std::vector<double> > &y,
																			std::vector< std::vector<double> > 
																			&dydt ) 
{
  if( compartment.numNeighbor()<1 ) return;
  size_t i=compartment.index();
  double R = parameter(3);
  double PI=3.141592654;
	size_t rCol = compartment.numTopologyVariable()-1;
  //double eps=0.01;
  //size_t zCol = compartment.numDimension()-1;

  //Make sure cell position is on the cylinder
  //double norm=0.0;
  //for(size_t dim=0 ; dim<zCol ; dim++ )
  //norm += y[i][varIndex+dim]*y[i][varIndex+dim];
  //norm = R/std::sqrt(norm);
  //for(size_t dim=0 ; dim<zCol ; dim++ )
  //y[i][varIndex+dim] *=norm;
  
  
  double iX = y[i][varIndex];
  double iY = y[i][varIndex+1];
  double iZ = y[i][varIndex+2];
  double iPhi = std::atan( std::fabs(iY/iX) );
  if( iX>0 && iY<0 )
    iPhi = 2*PI-iPhi;
  else if( iX<0 && iY>0 )
    iPhi = PI-iPhi;
  else if( iX<0 && iY<0 )
    iPhi = PI+iPhi;
  
  for( size_t n=0 ; n<compartment.numNeighbor() ; ++n ) {
    size_t j=compartment.neighbor(n);
    //Make sure cell position is on the cylinder
    //norm=0.0;
    //for(size_t dim=0 ; dim<zCol ; dim++ )
    //norm += y[j][varIndex+dim]*y[j][varIndex+dim];
    //norm = R/std::sqrt(norm);
    //for(size_t dim=0 ; dim<compartment.numDimension() ; dim++ )
    //y[j][varIndex+dim] *=norm;
    double jX = y[j][varIndex];
    double jY = y[j][varIndex+1];
    double jZ = y[j][varIndex+2];
    double jPhi = std::atan( std::fabs(jY/jX) );
    if( jX>0 && jY<0 )
      jPhi = 2*PI-jPhi;
    else if( jX<0 && jY>0 )
      jPhi = PI-jPhi;
    else if( jX<0 && jY<0 )
      jPhi = PI+jPhi;
    
    double rirj = parameter(0)*( y[i][rCol]+y[j][rCol] );
    double distance = std::sqrt( (iZ-jZ)*(iZ-jZ)+R*R*(iPhi-jPhi)*(iPhi-jPhi) );
    if( distance<=0. ) {
      std::cerr << "SpringAsymmetricCylinder::derivs() "
								<< "Two cells on top of each other, exiting\n";
      std::cerr << "distance=" << distance << ", k*(r_i+r_j)=" << rirj 
								<< " (" << i << "," << j << ")\n";
      exit(-1);
    }
    double rCoff = parameter(1)*(1.0 - rirj/distance);
    if( distance>rirj )
      rCoff *=parameter(2);
    
    //z-contribution
    dydt[i][varIndex+2] += rCoff*(jZ-iZ);
    //x and y contribution from the phi-contribution
    double dPhi = rCoff*R*R*(jPhi-iPhi);
    dydt[i][varIndex] += R*( std::cos(iPhi+dPhi) - std::cos(iPhi) );
    dydt[i][varIndex+1] += R*( std::sin(iPhi+dPhi) - std::sin(iPhi) );
  }
}

SpringAsymmetricSphereCylinder::
SpringAsymmetricSphereCylinder(std::vector<double> &paraValue, 
															 std::vector< std::vector<size_t> > &indValue ) 
{  
  // Do some checks on the parameters and variable indeces
  if( paraValue.size()!=4 ) {
    std::cerr << "SpringAsymmetricSphereCylinder::"
							<< "SpringAsymmetricSphereCylinder() "
							<< "Uses three parameters r_relax K_force K_adhesion "
							<< "R_sphere/cylinder.\n";
    exit(0);
  }
  if( indValue.size() ) {
    std::cerr << "SpringAsymmetricSphereCylinder::"
							<< "SpringAsymmetricSphereCylinder() "
							<< "No variable indeces is used.\n";
    exit(0);
  }
	
  // Set the variable values
  setId("springAsymmetricSphereCylinder");
  setParameter(paraValue);  
  setVariableIndex(indValue);
  
  // Set the parameter identities
  std::vector<std::string> tmp( numParameter() );
  tmp[0] = "r_relax";
  tmp[1] = "K_force";
  tmp[2] = "K_adhFrac";
  tmp[3] = "R_sphere/cylinder";
  setParameterId( tmp );
}

void SpringAsymmetricSphereCylinder::derivs(Compartment &compartment,
																						size_t varIndex,
																						std::vector< std::vector<double> > 
																						&y,
																						std::vector< std::vector<double> > 
																						&dydt ) 
{
  size_t i=compartment.index();
  if( y[i][varIndex+2]>0.0 )
    derivsSphere(compartment,varIndex,y,dydt);
  else
    derivsCylinder(compartment,varIndex,y,dydt);
}

void SpringAsymmetricSphereCylinder::
derivsCylinder(Compartment &compartment,size_t varIndex,
							 std::vector< std::vector<double> > &y,
							 std::vector< std::vector<double> > 
							 &dydt ) 
{  
  if( compartment.numNeighbor()<1 ) return;
  size_t i=compartment.index();
  double R = parameter(3);
  size_t zCol = compartment.numTopologyVariable()-2;
  size_t rCol = compartment.numTopologyVariable()-1;
  double PI=3.141592654;
  double dPhi = 0.0;//temporary variable adding contribution from all neigh
  //double eps=0.01;
	
  //Make sure cell position is on the cylinder
  double norm=0.0;
  for(size_t dim=0 ; dim<zCol ; dim++ )
    norm += y[i][varIndex+dim]*y[i][varIndex+dim];
  norm = R/std::sqrt(norm);
  for(size_t dim=0 ; dim<zCol ; dim++ )
    y[i][varIndex+dim] *=norm;
	
  double iX = y[i][varIndex];
  double iY = y[i][varIndex+1];
  double iZ = y[i][varIndex+2];
  double iPhi = std::atan( std::fabs(iY/iX) );
  if( iX>0 && iY<0 )
    iPhi = 2*PI-iPhi;
  else if( iX<0 && iY>0 )
    iPhi = PI-iPhi;
  else if( iX<0 && iY<0 )
    iPhi = PI+iPhi;
  
  for( size_t n=0 ; n<compartment.numNeighbor() ; ++n ) {
    size_t j=compartment.neighbor(n);
    //Make sure cell position is on the cylinder
    norm=0.0;
    //for(size_t dim=0 ; dim<zCol ; dim++ )
    //norm += y[j][varIndex+dim]*y[j][varIndex+dim];
    //norm = R/std::sqrt(norm);
    //for(size_t dim=0 ; dim<compartment.numDimension() ; dim++ )
    //y[j][varIndex+dim] *=norm;
		
    double jX = y[j][varIndex];
    double jY = y[j][varIndex+1];
    double jZ = y[j][varIndex+2];
    double jPhi = std::atan( std::fabs(jY/jX) );
    if( jX>0 && jY<0 )
      jPhi = 2*PI-jPhi;
    else if( jX<0 && jY>0 )
      jPhi = PI-jPhi;
    else if( jX<0 && jY<0 )
      jPhi = PI+jPhi;
		
    double deltaPhi = jPhi-iPhi;
    if( deltaPhi<-PI )
      deltaPhi += 2*PI;
    else if( deltaPhi>PI )
      deltaPhi -= 2*PI;
    if( deltaPhi<-PI || deltaPhi>PI ) {
      std::cerr << "::derivsCylinder() delta-phi strange.\n"
								<< iPhi << " " << jPhi << " " << deltaPhi << " (" 
								<< jPhi-iPhi << ")  "
								<< R*std::cos(iPhi) << " " << R*std::sin(iPhi) << " "
								<< R*std::cos(jPhi) << " " << R*std::sin(jPhi) << " "
								<< R*std::cos(iPhi+deltaPhi) << " " 
								<< R*std::sin(iPhi+deltaPhi) << "\n";
      exit(-1);
    }        
    double rirj = parameter(0)*( y[i][rCol]+y[j][rCol] );
    double distance = std::sqrt( (iZ-jZ)*(iZ-jZ)+R*R*deltaPhi*deltaPhi );
    if( distance<=0. ) {
      std::cerr << "SpringAsymmetricCylinder::derivs() "
								<< "Two cells on top of each other, exiting\n";
      std::cerr << "distance=" << distance << ", k*(r_i+r_j)=" << rirj 
								<< " (" << i << "," << j << ")\n";
      exit(-1);
    }
    double rCoff = parameter(1)*(1.0 - rirj/distance);
    if( distance>rirj )
      rCoff *=parameter(2);
    
    //z-contribution
    dydt[i][varIndex+2] += rCoff*(jZ-iZ);
    //x and y contribution from the phi-contribution
    dPhi += rCoff*R*R*deltaPhi;
  }
  dydt[i][varIndex] = R*( std::cos(iPhi+dPhi) - std::cos(iPhi) );
  dydt[i][varIndex+1] = R*( std::sin(iPhi+dPhi) - std::sin(iPhi) );
}

void SpringAsymmetricSphereCylinder::
derivsSphere(Compartment &compartment,size_t varIndex,
						 std::vector< std::vector<double> > &y,
						 std::vector< std::vector<double> > 
						 &dydt ) 
{
  if( compartment.numNeighbor()<1 ) return;
  size_t i=compartment.index();
  double R = parameter(3);
	size_t dimension = compartment.numTopologyVariable()-1;
	size_t rCol = dimension;
  //double eps=0.1;
  
  //Make sure cell position is on the sphere
  double norm=0.0;
  for(size_t dim=0 ; dim<dimension ; ++dim )
    norm += y[i][varIndex+dim]*y[i][varIndex+dim];
  if( norm<0.75*R*R || norm>1.25*R*R ) {
    std::cerr << "SpringAsymmetricSphereCylinder::derivsSphere() "
							<< " Position vector too far from sphere.\n";
    exit(-1);
  }
  norm = R/std::sqrt(norm);
  for(size_t dim=0 ; dim<dimension ; ++dim )
    y[i][varIndex+dim] *=norm;
  
  std::vector<double> A(dimension),B(dimension)
    ,W(dimension),dX(dimension);
  
  double AA=0.0;
  for(size_t dim=0 ; dim<dimension ; ++dim ) {
    A[dim] = y[i][varIndex+dim];
    AA += A[dim]*A[dim];
  }
  for( size_t n=0 ; n<compartment.numNeighbor() ; ++n ) {
    size_t j=compartment.neighbor(n);
    //Use values assuming on sphere
    norm=0.0;
    for(size_t dim=0 ; dim<dimension ; ++dim )
      norm += y[j][varIndex+dim]*y[j][varIndex+dim];
    if( norm<0.75*R*R || norm>1.25*R*R ) {
      std::cerr << "SpringAsymmetricSphereCylinder::derivsSphere() "
								<< " Neighbor position vector too far from sphere.\n";
      std::cerr << i << " " << j << " " << y[j][0] << " " << y[j][1] << " "
								<< y[j][2] << " " << std::sqrt(y[j][0]*y[j][0]+y[j][1]*y[j][1]
																							 +y[j][2]*y[j][2]) << "\n";
      exit(-1);
    }
    norm = R/std::sqrt(norm);
    double AB=0.,BB=0.;
    for(size_t dim=0 ; dim<dimension ; ++dim ) {
      B[dim] = y[j][varIndex+dim]*norm;//Using values on sphere
      AB += A[dim]*B[dim];
      BB += B[dim]*B[dim];
    }
    if( AB<=0.0 || AB>R*R ) {
      std::cerr << "SpringAsymmetricSphereCylinder::derivsSphere() "
								<< " Vector product strange.\n";
      exit(-1);
    }
    
    double rirj = parameter(0)*( y[i][rCol] + y[j][rCol] );
    double distance = R*std::acos(AB/(R*R));
    if( distance<=0. ) {
      std::cerr << "SpringAsymmetricSphere::derivs() "
								<< "Two cells on top of each other, exiting\n";
      std::cerr << "distance=" << distance << ", k*(r_i+r_j)=" << rirj 
								<< " (" << i << "," << j << ")\n";
      exit(-1);
    }
    double rCoff = parameter(1)*(distance-rirj);
    if( distance>rirj )
      rCoff *=parameter(2);
    //Define vector in plane orthogonal to A and pointing towards B
    //and of length 1.
    double norm=0.0;
    for(size_t dim=0 ; dim<dimension ; dim++ ) {
      W[dim] = B[dim] - AB*A[dim]/(R*R);
      norm += W[dim]*W[dim];
    }
    if( norm<=0.0 ) {
      std::cerr << "SpringAsymmetricSphere::derivs() "
								<< "No length on W vector\n";
      exit(-1);
    }
    norm = 1./std::sqrt(norm);
    for(size_t dim=0 ; dim<dimension ; dim++ )
      W[dim] *= norm;
    //Update for each dimension
    for(size_t dim=0 ; dim<dimension ; dim++ ) {
      dX[dim] += rCoff*W[dim];
    }
  }
  //Increase dX size to after projection onto the sphere be of length |dX|
  double length = 0.;
  //for(size_t dim=0 ; dim<dimension ; dim++ )
  //length += dX[dim]*dX[dim];
  //length = std::sqrt(length);
  //   if(length>y[i][dimension] )
  //     std::cerr << "dX["<<i<<"](l): " << length << "\n";
  //norm = R*std::tan(length/R)/length;
  //   if( std::fabs(norm-1)>eps )
  //     std::cerr << "dX["<<i<<"](n): " << norm << "\n";
  //for(size_t dim=0 ; dim<dimension ; dim++ )
  //dX[dim] *= norm;
//   double AdX=0.0;
//   for(size_t dim=0 ; dim<dimension ; dim++ )
//     AdX += A[dim]*dX[dim];
//   if( AdX>eps )
//     std::cerr << "AdX["<<i<<"]: " << AdX << "\n";
	
  //Project onto sphere surface (normalize A+dX to lye on sphere)
  std::vector<double> yTmp(dimension);
  length=0.;
  for(size_t dim=0 ; dim<dimension ; dim++ ) {
    yTmp[dim] = A[dim]+dX[dim]; 
    length += yTmp[dim]*yTmp[dim];
  }
  length = std::sqrt(length);
//   if( std::fabs(length-R)>eps )
//     std::cerr << "A+dX["<<i<<"](l): " << length << "\n";
  norm = R/length;
  for(size_t dim=0 ; dim<dimension ; dim++ )
    yTmp[dim] *= norm;
  //Add the final contribution for lying on the sphere
  for(size_t dim=0 ; dim<dimension ; dim++ )
    dydt[i][varIndex+dim] += yTmp[dim] - A[dim];
}

//!Constructor for the SphereCylinderWall class
SphereCylinderWall::SphereCylinderWall(std::vector<double> &paraValue, 
				       std::vector< std::vector<size_t> > 
				       &indValue ) {
  
  //Do some checks on the parameters and variable indeces
  //////////////////////////////////////////////////////////////////////
  if( paraValue.size()!=2 ) {
    std::cerr << "SphereCylinderWall::SphereCylinderWall() "
	      << "Uses two parameters K_force and R.\n";
    exit(0);
  }
  if( indValue.size() ) {
    std::cerr << "SphereCylinderWall::SphereCylinderWall() "
	      << "No variable indeces is used.\n";
    exit(0);
  }
  //Set the variable values
  //////////////////////////////////////////////////////////////////////
  setId("sphereCylinderWall");
  setParameter(paraValue);  
  setVariableIndex(indValue);
  
  //Set the parameter identities
  //////////////////////////////////////////////////////////////////////
  std::vector<std::string> tmp( numParameter() );
  tmp[0] = "K_force";
  tmp[1] = "R";
  setParameterId( tmp );
}

//! Derivative contribution for a mechanical interaction from a spring.
/*! Deriving the time derivative contribution from a mechanical spring
  using the values in y and add the results to dydt. The only
  difference from a normal spring is that an extra parameter defining
  the strength of the adhesion compared to the repelling spring
  constant is needed for adhesion K=K_force*K_adhFrac.
*/
void SphereCylinderWall::derivs(Compartment &compartment,size_t varIndex,
			      std::vector< std::vector<double> > &y,
			      std::vector< std::vector<double> > &dydt ) {

  static size_t dimension=compartment.numTopologyVariable()-1;
  if( dimension != 2 && dimension != 3 ) {
    std::cerr << "SphereCylinderWall::derivs Not available for "
	      << dimension << " dimensions.\n";
    exit(0);
  }
  size_t i=compartment.index();

  if( y[i][dimension-1]>=0.0 ) {
    //On half-sphere
    double r=0.0;
    for( size_t d=0 ; d<dimension ; d++ )
      r += y[i][d]*y[i][d];
    r = std::sqrt(r);
    if( r<=parameter(1) ) return;
    double coeff = (parameter(1)-r)*parameter(0)/r;
    for( size_t d=0 ; d<dimension ; d++ )
      dydt[i][d] += coeff*y[i][d];
  }
  else {
    //On cylinder    
    double r=0.0;
    for( size_t d=0 ; d<dimension-1 ; d++ )
      r += y[i][d]*y[i][d];
    r = std::sqrt(r);
    if( r<=parameter(1) ) return;
    double coeff = (parameter(1)-r)*parameter(0)/r;
    for( size_t d=0 ; d<dimension-1 ; d++ )
      dydt[i][d] += coeff*y[i][d];    
  }
}

ParabolaSphereWall::ParabolaSphereWall(std::vector<double> &paraValue, 
				       std::vector< std::vector<size_t> > 
				       &indValue ) {
  
  //Do some checks on the parameters and variable indeces
  //////////////////////////////////////////////////////////////////////
  if( paraValue.size()!=6 ) {
    std::cerr << "ParabolaSphereWall::ParabolaSphereWall() "
	      << "Uses six parameters K_force, C and H for parabola, R, X0 and growth for sphere\n";
    exit(0);
  }
  if( indValue.size() ) {
    std::cerr << "ParabolaSphereWall::ParabolaSphereWall() "
	      << "No variable indeces is used.\n";
    exit(0);
  }
  //Set the variable values
  //////////////////////////////////////////////////////////////////////
  setId("parabolaSphereWall");
  setParameter(paraValue);  
  setVariableIndex(indValue);
  
  //Set the parameter identities
  //////////////////////////////////////////////////////////////////////
  std::vector<std::string> tmp( numParameter() );
  tmp[0] = "K_force";
  tmp[1] = "C";
  tmp[2] = "H";
  tmp[3] = "R";
  tmp[4] = "X0";
  tmp[5] = "growth";
  setParameterId( tmp );
}

//! Derivative contribution for a mechanical interaction from a spring.
/*! Deriving the time derivative contribution from a mechanical spring
  using the values in y and add the results to dydt. The only
  difference from a normal spring is that an extra parameter defining
  the strength of the adhesion compared to the repelling spring
  constant is needed for adhesion K=K_force*K_adhFrac.
*/
void ParabolaSphereWall::derivs(Compartment &compartment,size_t varIndex,
			      std::vector< std::vector<double> > &y,
			      std::vector< std::vector<double> > &dydt ) {

  static size_t dimension=compartment.numTopologyVariable()-1;
  if( dimension != 2 && dimension != 3 ) {
    std::cerr << "ParabolaSphereWall::derivs Not available for "
	      << dimension << " dimensions.\n";
    exit(0);
  }
  size_t i=compartment.index();

  double F = parameter(0);
  double C = parameter(1);
  double H = parameter(2);
  double R = parameter(3);
  double x0 = parameter(4);
  double growth = parameter(5);
  
  double h=H;
  for( size_t d=0 ; d<dimension-1 ; d++ )
    h +=-C* y[i][d]*y[i][d];
  
  double x_sphere_center;
  double stuff = std::sqrt(4 * C * C * x0 * x0 + 1);
  x_sphere_center = x0-(2*R*C*x0*(1 - growth))/stuff;
  double z_sphere_center;
  z_sphere_center = (- C * x0*x0 - R * (- growth + 1) * (1/stuff)) + H;
    
  double r = 0;
  std::vector<double> center(dimension);
  center[1] = 0;
  center[0] = x_sphere_center;
  center[dimension-1] = z_sphere_center;
  for( size_t d=0 ; d<dimension ; d++ )
    r += (y[i][d] - center[d])*(y[i][d] - center[d]);
  r = std::sqrt(r);
  
  if (y[i][dimension-1] >= h and (r > R)) {
    if (r < 1.1 * R) {
      for (size_t d=0; d < dimension; d++)
// 	dydt[i][d] += -F*std::fabs(y[i][d]-center[d])*(r-R);
	dydt[i][d] += F*(center[d]-y[i][d])*(r-R);      
    }
    else {
      double coeff = 0;
      for( size_t d=0 ; d<dimension - 1; d++ )
	coeff += y[i][d]*y[i][d];
      coeff = y[i][dimension-1]+C*coeff; //for a parabola of formula: z = h - c(x*2+y*2), computes h knowing c,x,y,z
      coeff = F*(H-coeff); // computes a force to apply based on the difference between h and H
      for( size_t d=0 ; d<dimension ; d++ )
	dydt[i][d] += coeff*y[i][d];
    }
  }
}

ParabolaSphereWallGrowth::ParabolaSphereWallGrowth(std::vector<double> &paraValue, 
				       std::vector< std::vector<size_t> > 
				       &indValue ) {
  
  //Do some checks on the parameters and variable indeces
  //
  if( paraValue.size()!=7 ) {
    std::cerr << "ParabolaSphereWallGrowth::ParabolaSphereWallGrowth() "
	      << "Uses six parameters K_force, C and H for parabola, R, X0"
	      << ", primordia size (0-1) and primordia growth rate (for sphere)" << std::endl;
    exit(0);
  }
  if( indValue.size() ) {
    std::cerr << "ParabolaSphereWallGrowth::ParabolaSphereWallGrowth() "
	      << "No variable indeces is used." << std::endl;
    exit(0);
  }
  //Set the variable values
  //
  setId("parabolaSphereWallGrowth");
  setParameter(paraValue);  
  setVariableIndex(indValue);
  
  //Set the parameter identities
  //
  std::vector<std::string> tmp( numParameter() );
  tmp[0] = "K_force";
  tmp[1] = "C";
  tmp[2] = "H";
  tmp[3] = "R";
  tmp[4] = "X0";
  tmp[5] = "size";
  tmp[6] = "growth";
  setParameterId( tmp );
}

void ParabolaSphereWallGrowth::derivs(Compartment &compartment,size_t varIndex,
				      std::vector< std::vector<double> > &y,
				      std::vector< std::vector<double> > &dydt ) {

  static size_t dimension=compartment.numTopologyVariable()-1;
  if( dimension != 2 && dimension != 3 ) {
    std::cerr << "ParabolaSphereWallGrowth::derivs Not available for "
	      << dimension << " dimensions.\n";
    exit(0);
  }
  size_t i=compartment.index();

  double F = parameter(0);
  double C = parameter(1);
  double H = parameter(2);
  double R = parameter(3);
  double x0 = parameter(4);
  double growth = parameter(5);
  
  double h=H;
  for( size_t d=0 ; d<dimension-1 ; d++ )
    h +=-C* y[i][d]*y[i][d];
  
  double x_sphere_center;
  double stuff = std::sqrt(4 * C * C * x0 * x0 + 1);
  x_sphere_center = x0-(2*R*C*x0*(1 - growth))/stuff;
  double z_sphere_center;
  z_sphere_center = (- C * x0*x0 - R * (- growth + 1) * (1/stuff)) + H;
    
  double r = 0;
  std::vector<double> center(dimension);
  center[1] = 0;
  center[0] = x_sphere_center;
  center[dimension-1] = z_sphere_center;
  for( size_t d=0 ; d<dimension ; d++ )
    r += (y[i][d] - center[d])*(y[i][d] - center[d]);
  r = std::sqrt(r);
  
  if (y[i][dimension-1] >= h and (r > R)) {
    if (r < 1.1 * R) {
      for (size_t d=0; d < dimension; d++)
// 	dydt[i][d] += -F*std::fabs(y[i][d]-center[d])*(r-R);
	dydt[i][d] += F*(center[d]-y[i][d])*(r-R);      
    }
    else {
      double coeff = 0;
      for( size_t d=0 ; d<dimension - 1; d++ )
	coeff += y[i][d]*y[i][d];
      coeff = y[i][dimension-1]+C*coeff; //for a parabola of formula: z = h - c(x*2+y*2), computes h knowing c,x,y,z
      coeff = F*(H-coeff); // computes a force to apply based on the difference between h and H
      for( size_t d=0 ; d<dimension ; d++ )
	dydt[i][d] += coeff*y[i][d];
    }
  }
}

void ParabolaSphereWallGrowth::
update(double h, double t, std::vector< std::vector<double> > &y)
{
  if( parameter(5)<1.0)
    setParameter(5,parameter(5)+parameter(6)*h);
  if (parameter(5)>1.0)
    setParameter(5,1.0);
}

PrimordiumGrowth::PrimordiumGrowth(std::vector<double> &paraValue, 
				       std::vector< std::vector<size_t> > 
				       &indValue ) {
  
  //Do some checks on the parameters and variable indeces
  //
  if( paraValue.size()!=7 ) {
    std::cerr << "PrimordiumGrowth::PrimordiumGrowth() "
	      << "Uses 7 parameters: F (force), C (parabola angle) and H (parabola height) for parabola, R "
      	      << "(primordium radius), A (primordium angle), G0 (position of the "
	      << "center of the half sphere) and G (growth rate)" << std::endl;
    exit(0);
  }
  if( indValue.size() ) {
    std::cerr << "PrimordiumGrowth::PrimordiumGrowth() "
	      << "No variable indeces is used." << std::endl;
    exit(0);
  }
  //Set the variable values
  //
  setId("primordiumGrowth");
  setParameter(paraValue);  
  setVariableIndex(indValue);
  
  //Set the parameter identities
  //
  std::vector<std::string> tmp( numParameter() );
  tmp[0] = "F";
  tmp[1] = "C";
  tmp[2] = "H";
  tmp[3] = "R";
  tmp[4] = "A";
  tmp[5] = "G0";
  tmp[6] = "G";
  setParameterId( tmp );
}

void PrimordiumGrowth::derivs(Compartment &compartment,size_t varIndex,
				      std::vector< std::vector<double> > &y,
				      std::vector< std::vector<double> > &dydt ) {

  static size_t dimension=compartment.numTopologyVariable()-1;
  if( dimension != 3 ) {
    std::cerr << "PrimordiumGrowth::derivs Not available for "
	      << dimension << " dimensions.\n";
    exit(0);
  }
  size_t i=compartment.index();

  double F = parameter(0);
  double C = parameter(1);
  double H = parameter(2);
  double R = parameter(3);
  double A = parameter(4);
  double G0 = parameter(5);
  double G = parameter(6);

  //cell in parabola
  if ( y[i][2] >= H - C * (y[i][0]*y[i][0] + y[i][1]*y[i][1])) {
    // checks if possibly close to either cylinder or sphere
    if (y[i][2] <= -(1/A)*y[i][1] + A*(G0+G) + (G0+G)/A) {
      //cylinder
      double t = (y[i][1]+y[i][2]*A)/(1+A*A);
      double distance_to_cylinder = std::sqrt( y[i][0]*y[i][0] + (y[i][1]-t)*(y[i][1]-t) + (y[i][2]- A*t)*(y[i][2]- A*t));
      if (distance_to_cylinder >= R){
	if (distance_to_cylinder <= R+0.1*R){
	  dydt[i][0] += -F*y[i][0];
	  dydt[i][1] += -F*(y[i][1] - t);
	  dydt[i][2] += -F*(y[i][2] - A*t);
	}
	else {
	  dydt[i][0] += -F*y[i][0];
	  dydt[i][1] += -F*y[i][1];
	  dydt[i][2] += -F*y[i][2];
	}
      }
    }
    else {
      //sphere
      double distance_to_sphere = std::sqrt( y[i][0]*y[i][0] + (y[i][1]-(G0+G))*(y[i][1]-(G0+G)) + (y[i][2]-((G+G0)*A))*(y[i][2]-((G0+G)*A)) );
      if (distance_to_sphere >= R){
	if (distance_to_sphere <= R+0.1*R){
	  dydt[i][0] += -F*y[i][0];
	  dydt[i][1] += -F*(y[i][1] - (G0+G));
	  dydt[i][2] += -F*(y[i][2] - ((G0+G)*A));
	}
	else {
	  dydt[i][0] += -F*y[i][0];
	  dydt[i][1] += -F*y[i][1];
	  dydt[i][2] += -F*y[i][2];
	}
      }
    }    
  }
}

void PrimordiumGrowth::
update(double h, double t, std::vector< std::vector<double> > &y)
{
  setParameter(5,parameter(5)+parameter(6)*h);
}


//!Constructor for the EllipticRepulsion  class
EllipticRepulsion::EllipticRepulsion(std::vector<double> &paraValue, 
				   std::vector< std::vector<size_t> > 
				   &indValue ) {
  
  //Do some checks on the parameters and variable indeces
  //////////////////////////////////////////////////////////////////////
  if( paraValue.size()!=3 ) {
    std::cerr << "EllipticRepulsion::EllipticRepulsion() "
	      << "Uses three parameters K_force, b and d_frac.\n";
    exit(0);
  }
  if( indValue.size() ) {
    std::cerr << "EllipticRepulsion::EllipticRepulsion() "
	      << "No variable indeces is used.\n";
    exit(0);
  }
  //Set the variable values
  //////////////////////////////////////////////////////////////////////
  setId("ellipticRepulsion");
  setParameter(paraValue);  
  setVariableIndex(indValue);
  
  //Set the parameter identities
  //////////////////////////////////////////////////////////////////////
  std::vector<std::string> tmp( numParameter() );
  tmp[0] = "K_force";
  tmp[1] = "b";
  tmp[2] = "d_frac";
  
  setParameterId( tmp );
}
	
//! Derivative contribution for a mechanical interaction from a spring.
/*! Deriving the time derivative contribution from a mechanical spring
  using the values in y and add the results to dydt. The only
  difference from a normal spring is that an extra parameter defining
  the strength of the adhesion compared to the repelling spring
  constant is needed for adhesion K=K_force*K_adhFrac.
*/
void EllipticRepulsion::derivs(Compartment &compartment,size_t varIndex,
			      std::vector< std::vector<double> > &y,
			      std::vector< std::vector<double> > &dydt ) {

  //static int tmpT=0;

  //Checking for correct number of variables
  if( compartment.numTopologyVariable() != 5 ) {
    std::cerr << "EllipticRepulsion::derivs() "
	      << "Topological error, " << compartment.numTopologyVariable() 
	      << " is not equal to 5\n";
    exit(-1);
  }
  //Listing the columns used
  size_t i=compartment.index(), x_a=varIndex, y_a=varIndex+1, 
    x_b=varIndex+2, y_b=varIndex+3; 
  
  //Setting values for the ellips in question
  double pi = 3.141592654;
 
  bool solutionFlag;  
  double a_cell = parameter(2)*0.5*sqrt( (y[i][x_b]-y[i][x_a])*(y[i][x_b]-y[i][x_a])+
			    (y[i][y_b]-y[i][y_a])*(y[i][y_b]-y[i][y_a]) );
  double b_cell = parameter(2)*parameter(1);
  if(a_cell < b_cell) {
    std::cerr << "ElliptivRepulsion::derivs() "
	      << "The major axis a = " << a_cell 
	      << " must be greater than the minor axis b = " 
	      << b_cell << "\n";
    exit(-1); 
  } 
  //double c_cell = sqrt(a_cell*a_cell - b_cell*b_cell);
  double n_aCell[2] = { (y[i][x_b]-y[i][x_a])/(2*a_cell),
			(y[i][y_b]-y[i][y_a])/(2*a_cell) };
  double n_bCell[2] = { n_aCell[1], -n_aCell[0] };
  
  //finding neigbors and update ellips and its neighbors	       
  for( size_t n=0 ; n<compartment.numNeighbor() ; n++ ) {
    size_t j=compartment.neighbor(n);
    //Setting values for the neigboring ellipse
   
    double a_neigh=0.5*sqrt( (y[j][x_b]-y[j][x_a])*(y[j][x_b]-y[j][x_a])+
			     (y[j][y_b]-y[j][y_a])*(y[j][y_b]-y[j][y_a]) );
    double c_neigh = sqrt(a_neigh*a_neigh - b_cell*b_cell);
    double n_aNeigh[2] = { (y[j][x_b]-y[j][x_a])/(2*a_neigh),
			   (y[j][y_b]-y[j][y_a])/(2*a_neigh) };
   
    double I = 2.0*(b_cell*b_cell + a_neigh*a_neigh)/4;
    //find where the line x + n_a*t crosses neighboring ellipse,
    //i.e solve alpha*t^2 + beta*t + gamma = 0
    double t1, t2, t;  
    double alpha = 16*(c_neigh*c_neigh*
		       (n_aCell[0]*n_aNeigh[0]+n_aCell[1]*n_aNeigh[1])*
		       (n_aCell[0]*n_aNeigh[0] + n_aCell[1]*n_aNeigh[1])- 
		       a_neigh*a_neigh);
    double B = 4.0*(c_neigh*(y[i][x_a]*n_aNeigh[0] + y[i][y_a]*n_aNeigh[1])-
		  c_neigh*(y[j][x_a]*n_aNeigh[0] + y[j][y_a]*n_aNeigh[1])-
		  a_neigh*a_neigh -a_neigh*c_neigh );
    
    double beta = 8*c_neigh*
      (n_aCell[0]*n_aNeigh[0]+n_aCell[1]*n_aNeigh[1])*B-
      32*a_neigh*a_neigh*( (y[i][x_a]*n_aCell[0] + y[i][y_a]*n_aCell[1])-
			   (y[j][x_a]*n_aCell[0] + y[j][y_a]*n_aCell[1])-
			   (a_neigh + c_neigh)*
			   (n_aCell[0]*n_aNeigh[0]+n_aCell[1]*n_aNeigh[1]) 
			   );
    double gamma = B*B-16*a_neigh*a_neigh*
      ( (y[i][x_a]-y[j][x_a] - (a_neigh+c_neigh)*n_aNeigh[0])*
	(y[i][x_a]-y[j][x_a] - (a_neigh+c_neigh)*n_aNeigh[0])+
	(y[i][y_a]-y[j][y_a] - (a_neigh+c_neigh)*n_aNeigh[1])*
	(y[i][y_a]-y[j][y_a] - (a_neigh+c_neigh)*n_aNeigh[1]) );
    
    //Find angle theta between n_aNeigh and the x-axis
    double theta;
    double  fx = 0.5*(y[j][x_b] - y[j][x_a]);
    double  fy = 0.5*(y[j][y_b] - y[j][y_a]);
      
    if(fx == 0) {
      if(fy == 0)
	theta = 0.0;
      else if(fy > 0.0)
	theta = 0.5*pi;
      else //if(fy < 0)
	theta = 1.5*pi;
    }
    else if(fx > 0 && fy >= 0)
      theta = atan(fy/fx);
    else if(fx < 0 && fy >= 0)
      theta = pi + atan(fy/fx);
    else if(fx < 0 && fy < 0)
      theta = pi + atan(fy/fx);
    else //if(fx > 0 && fy < 0)
      theta = 2*pi + atan(fy/fx);
    
    if(alpha != 0.0) {
      double d = beta*beta/(4*alpha*alpha) - gamma/alpha;
      if(d >= 0.0) {
	t1 = -beta/(2*alpha) + sqrt(d);
	t2 = -beta/(2*alpha) - sqrt(d);
	if(t1 < 2*a_cell && t1 > 0) {
	  solutionFlag = 1;
	  if(t2 < 2*a_cell && t2 > 0)
	    t = t1<t2 ? t1:t2;
	  else
	    t = t1;
	} else if(t2 < 2*a_cell && t2 > 0) {
	  solutionFlag = 1;
	  if(t1 < 2*a_cell && t1 > 0)
	    t = t1<t2 ? t1:t2;
	  else
	    t = t2;
	} 
	else {
	  solutionFlag = 0;
	}
	//if( solutionFlag ) {
	//std::cerr << "a " << d << " " << t << " (" << t1 << "," 
	//    << t2 << ") "
	//    << n_aCell[0] << " " << n_aCell[1] << " " << a_cell 
	//    << " " << alpha << " " << beta << " " << gamma << "\n"; 
	//}
      }
      else {
	solutionFlag = 0;//d<0.0
      }
    }
    else {
      if(beta != 0) {
	t = -gamma/beta;
	if(t < 2*a_cell && t > 0)
	  solutionFlag = 1;
	else
	  solutionFlag = 0;	
      }
      else 
	solutionFlag = 0;
    }
    
    //if solutions are found, ellipses are updatet
    if(solutionFlag) {
      //std::cerr << tmpT++ << " " << i << " " << j << " " << a_cell << " " 
      //	<< b_cell << " " << t << " a\n"; 
      double x_cross = y[i][x_a] + n_aCell[0]*t;
      double y_cross = y[i][y_a] + n_aCell[1]*t;
      double F[2];
      if (t <= a_cell) {
	F[0] = parameter(0)*t*n_aCell[0];
	F[1] = parameter(0)*t*n_aCell[1];
      }
      else {
	F[0] = -parameter(0)*(2.0*a_cell-t)*n_aCell[0];
	F[1] = -parameter(0)*(2.0*a_cell-t)*n_aCell[1];
      }
      
      //Contributions to the cell in question
      dydt[i][x_a] += F[0];
      dydt[i][y_a] += F[1];
      dydt[i][x_b] += F[0];
      dydt[i][y_b] += F[1];
      
      //Contributions to the neighboring cell      
      //find the direction to the center of mass of the neighboring cell
      double dist = sqrt( ( (y[j][x_a]+a_neigh*n_aNeigh[0] - x_cross)*
			    (y[j][x_a]+a_neigh*n_aNeigh[0] - x_cross) )+
			  ( (y[j][y_a]+a_neigh*n_aNeigh[1] - y_cross)*
			    (y[j][y_a]+a_neigh*n_aNeigh[1] - y_cross) ) );
      double n_com[2] = { (y[j][x_a]+a_neigh*n_aNeigh[0] - x_cross)/dist,
			  (y[j][y_a]+a_neigh*n_aNeigh[1] - y_cross)/dist };
//       if( F[0]*n_com[0] + F[1]*n_com[1] > 0.0 ) {
// 	//Would result in pushing in direction away from com
// 	std::cerr << "EllipticRepulsion::derivs() Wrong sign on force.\n"
// 		  << "Cells " << i << " -> " << j << "\n";
// 	std::cerr << "\n1\n2 " << y[i].size() << "\n";
// 	for( size_t k=0 ; k<y[i].size() ; k++ )
// 	  std::cerr << y[i][k] << " ";
// 	std::cerr << "\n";
// 	for( size_t k=0 ; k<y[j].size() ; k++ )
// 	  std::cerr << y[j][k] << " ";
// 	std::cerr << "\n\n";
	
// 	exit(-1);
//       }
      //Contributions from the COM-component of the force
      dydt[j][x_a] -= (F[0]*n_com[0] + F[1]*n_com[1])*n_com[0];
      dydt[j][y_a] -= (F[0]*n_com[0] + F[1]*n_com[1])*n_com[1];
      dydt[j][x_b] -= (F[0]*n_com[0] + F[1]*n_com[1])*n_com[0];
      dydt[j][y_b] -= (F[0]*n_com[0] + F[1]*n_com[1])*n_com[1];
      
      //Find component of the force perpendicular to COM component
      double n_perp[2]= {n_com[1], -n_com[0]};
      if(n_perp[0]*F[0] +n_perp[1]*F[1] > 0) {
	n_perp[0] = - n_com[1];
	n_perp[1] =  n_com[0];
      }
      
      //Rotational contibutions 
      double F_perp =-(F[0]*n_perp[0] + F[1]*n_perp[1]); 
      //change sign if negative rotation (theta-dt)
      if( n_perp[0]*n_com[1]-n_perp[1]*n_com[0]<0 )
	F_perp = -F_perp;
      double deriv = a_neigh*F_perp*dist / I;
      dydt[j][x_a] += deriv*sin(theta);
      dydt[j][y_a] -= deriv*cos(theta); 
      dydt[j][x_b] -= deriv*sin(theta);
      dydt[j][y_b] += deriv*cos(theta); 
    }
    
    //find where the line x + n_b*t crosses neighboring ellipse,
    //i.e solve alpha*t^2 + beta*t + gamma = 0
    solutionFlag = 0;
    alpha = 16*(c_neigh*c_neigh*
		(n_bCell[0]*n_aNeigh[0]+n_bCell[1]*n_aNeigh[1])*
		(n_bCell[0]*n_aNeigh[0] + n_bCell[1]*n_aNeigh[1]) - 
		a_neigh*a_neigh);
    
    B = 4.0*(c_neigh*(y[i][x_a]*n_aNeigh[0] + y[i][y_a]*n_aNeigh[1]) +
	     c_neigh*a_cell*(n_aCell[0]*n_aNeigh[0]+n_aCell[1]*n_aNeigh[1])-
	     c_neigh*(y[j][x_a]*n_aNeigh[0] + y[j][y_a]*n_aNeigh[1]) -
	     a_neigh*a_neigh - a_neigh*c_neigh ); 
    
    beta = 8*c_neigh*(n_bCell[0]*n_aNeigh[0] + n_bCell[1]*n_aNeigh[1])*B-
      32*a_neigh*a_neigh*
      ( (y[i][x_a]*n_bCell[0] + y[i][y_a]*n_bCell[1]) -
	(y[j][x_a]*n_bCell[0] + y[j][y_a]*n_bCell[1]) -
	(a_neigh + c_neigh)*
	(n_bCell[0]*n_aNeigh[0]+n_bCell[1]*n_aNeigh[1]) );
    gamma = B*B - 16*a_neigh*a_neigh*
      ( (y[i][x_a] + a_cell*n_aCell[0]- y[j][x_a]- (a_neigh + c_neigh)*
	 n_aNeigh[0])*
	(y[i][x_a] + a_cell*n_aCell[0] - y[j][x_a] - (a_neigh + c_neigh)*
	 n_aNeigh[0]) +
	(y[i][y_a]+ a_cell*n_aCell[1] - y[j][y_a] - (a_neigh + c_neigh)*
	 n_aNeigh[1])*
	(y[i][y_a]+ a_cell*n_aCell[1]- y[j][y_a] - (a_neigh + c_neigh)*
	 n_aNeigh[1]) );
    
    if(alpha != 0.0) {
      double d = beta*beta/(4*alpha*alpha) - gamma/alpha;
      if(d>= 0.0) {
	t1 = -beta/(2*alpha) + sqrt(d);
	t2 = -beta/(2*alpha) - sqrt(d);
	if(t1 < b_cell && t1 > - b_cell) {
	  solutionFlag = 1;
	  t = t1;
	  if(t2 < b_cell && t2 > -b_cell){
	    t = t2<t1 ? t2 : t1;
	  }
       
	} else if(t2 < b_cell && t2 > -b_cell) {
	  solutionFlag = 1;
	  t = t2;
	  if(t1 < b_cell && t1 > -b_cell) {
	    t = t2<t1 ? t2 : t1;
	  }	  
	} 
	else {
	  solutionFlag = 0;
	}
      }
      else {
	solutionFlag = 0;
      }
    }
    else {
      if(beta != 0) {
	t = -gamma/beta;
	if(t < b_cell && t > - b_cell)
	  solutionFlag = 1;
	else
	  solutionFlag = 0;	
      }
      else 
	solutionFlag = 0;
    }
    
    //if solutions are found, ellipses are updated
    if(solutionFlag) {
      //std::cerr << tmpT++ << " " << i << " " << j << " " << a_cell << " " 
      //	<< b_cell << " " << t << " b\n"; 
      double x_cross = y[i][x_a] + a_cell*n_aCell[0] + n_bCell[0]*t;
      double y_cross = y[i][y_a] + a_cell*n_aCell[1] + n_bCell[1]*t;
      double F[2];
      if (t >= 0 && t<=b_cell ) {
	F[0] = -parameter(0)*(b_cell-t)*n_bCell[0];
	F[1] = -parameter(0)*(b_cell-t)*n_bCell[1];
      }
      else if( t<0 && t>=-b_cell ) {
	F[0] = parameter(0)*(b_cell+t)*n_bCell[0];
	F[1] = parameter(0)*(b_cell+t)*n_bCell[1];
      }
      else {
	F[0]=F[1]=0.0;
	std::cerr << "EllipticRepulsion.derivs() Warning t outside used\n";  
      }
      //Contributions to the cell in question
      dydt[i][x_a] += F[0];
      dydt[i][y_a] += F[1];
      dydt[i][x_b] += F[0];
      dydt[i][y_b] += F[1];
      
      //Contributions to the neighboring cell
      //find the direction to the center of mass of the neighboring cell
      double dist = sqrt( 
			 ( (y[j][x_a] + a_neigh*n_aNeigh[0] - x_cross)*
			   (y[j][x_a] + a_neigh*n_aNeigh[0] - x_cross) ) +
			 ( (y[j][y_a] + a_neigh*n_aNeigh[1] - y_cross)*
			   (y[j][y_a] + a_neigh*n_aNeigh[1] - y_cross) ) );
      double n_com[2];
      n_com[0] = (y[j][x_a] + a_neigh*n_aNeigh[0] - x_cross)/dist;
      n_com[1] = (y[j][y_a] + a_neigh*n_aNeigh[1] - y_cross)/dist;
//       if( F[0]*n_com[0] + F[1]*n_com[1] > 0.0 ) {
// 	//Would result in pushing in direction away from com
// 	std::cerr << "EllipticRepulsion::derivs() Wrong sign on force.\n"
// 		  << "Cells " << i << " ->(b) " << j << "\n";
// 	std::cerr << "\n1\n2 " << y[i].size() << "\n";
// 	for( size_t k=0 ; k<y[i].size() ; k++ )
// 	  std::cerr << y[i][k] << " ";
// 	std::cerr << "\n";
// 	for( size_t k=0 ; k<y[j].size() ; k++ )
// 	  std::cerr << y[j][k] << " ";
// 	std::cerr << "\n\n";	
// 	exit(-1);
//       }
      //Contributions from the COM-component of the force
      dydt[j][x_a] -= (F[0]*n_com[0] + F[1]*n_com[1])*n_com[0];
      dydt[j][y_a] -= (F[0]*n_com[0] + F[1]*n_com[1])*n_com[1];
      dydt[j][x_b] -= (F[0]*n_com[0] + F[1]*n_com[1])*n_com[0];
      dydt[j][y_b] -= (F[0]*n_com[0] + F[1]*n_com[1])*n_com[1];
      
      //Find component of the force perpendicular to COM component
      double n_perp[2]= {n_com[1], -n_com[0]};
      //if(n_perp[0]*n_bCell[0] +n_perp[1]*n_bCell[1] > 0) {
      if(n_perp[0]*F[0] +n_perp[1]*F[1] > 0) {
	n_perp[0] = - n_com[1];
	n_perp[1] =  n_com[0];
      }
      
      //Rotational contibutions 
      double F_perp =-(F[0]*n_perp[0] + F[1]*n_perp[1]); 
      //change sign if negative rotation (theta-dt)
      if( n_perp[0]*n_com[1]-n_perp[1]*n_com[0]<0 )
	F_perp = -F_perp;
      double deriv = a_neigh*F_perp*dist /I;
      dydt[j][x_a] += deriv*sin(theta);
      dydt[j][y_a] -= deriv*cos(theta); 
      dydt[j][x_b] -= deriv*sin(theta);
      dydt[j][y_b] += deriv*cos(theta);       
    }
  }
}


//!Constructor for the NormalEllipticRepulsion  class
NormalEllipticRepulsion::NormalEllipticRepulsion(std::vector<double> &paraValue, 
				   std::vector< std::vector<size_t> > 
				   &indValue ) {
  
  //Do some checks on the parameters and variable indeces
  //////////////////////////////////////////////////////////////////////
  if( paraValue.size()!=4 ) {
    std::cerr << "NormalEllipticRepulsion::NormalEllipticRepulsion() "
	      << "Uses four parameters K_force, b, d_overlap and K_adh.\n";
    exit(0);
  }
  if( indValue.size() != 1 ) {
    std::cerr << "NormalEllipticRepulsion::NormalEllipticRepulsion() "
	      << "One variable index is used, Force.\n";
    exit(0);
  }
  //Set the variable values
  //////////////////////////////////////////////////////////////////////
  setId("ellipticRepulsion");
  setParameter(paraValue);  
  setVariableIndex(indValue);
  
  //Set the parameter identities
  //////////////////////////////////////////////////////////////////////
  std::vector<std::string> tmp( numParameter() );
  tmp[0] = "K_force";
  tmp[1] = "b";
  tmp[2] = "d_overlap";
  tmp[3] = "K_adh";
  setParameterId( tmp );
}
	
//! Derivative contribution for a mechanical interaction from a spring.
/*! Deriving the time derivative contribution from a mechanical spring
  using the values in y and add the results to dydt. The only
  difference from a normal spring is that an extra parameter defining
  the strength of the adhesion compared to the repelling spring
  constant is needed for adhesion K=K_force*K_adhFrac.
*/
void NormalEllipticRepulsion::derivs(Compartment &compartment,size_t varIndex,
			      std::vector< std::vector<double> > &y,
			      std::vector< std::vector<double> > &dydt ) {

  //static int tmpT=0;
  
  //Checking for correct number of variables
  if( compartment.numTopologyVariable() != 5 ) {
    std::cerr << "NormalEllipticRepulsion::derivs() "
	      << "Topological error, " << compartment.numTopologyVariable() 
	      << " is not equal to 5\n";
    exit(-1);
  }
  //Listing the columns used
  size_t i=compartment.index(), x_a=varIndex, y_a=varIndex+1, 
    x_b=varIndex+2, y_b=varIndex+3; 
  
  if(!i) {
    for(size_t k = 0; k<y.size(); k++)
      y[k][variableIndex(0,0)] = 0;
  }
  //Setting values for the ellipse in question
  double ForceCell = 0;
  double pi = 3.141592654;
 
  bool solutionFlag;
  
  double a_cell = 0.5*sqrt( (y[i][x_b]-y[i][x_a])*(y[i][x_b]-y[i][x_a])+
			    (y[i][y_b]-y[i][y_a])*(y[i][y_b]-y[i][y_a]) );
  double b_cell = parameter(1);
  if(a_cell < b_cell) {
    std::cerr << "NormalElliptivRepulsion::derivs() "
	      << "The major axis a = " << a_cell 
	      << " must be greater than the minor axis b = " 
	      << b_cell << "\n";
    exit(-1); 
  } 
  double c_cell = sqrt(a_cell*a_cell - b_cell*b_cell);
  double n_aCell[2] = { (y[i][x_b]-y[i][x_a])/(2*a_cell),
			(y[i][y_b]-y[i][y_a])/(2*a_cell) };
  double n_bCell[2] = { n_aCell[1], -n_aCell[0] };
  double ICell = 5.0*(b_cell*b_cell + a_cell*a_cell)/4;
 //Find angle theta between n_aNeigh and the x-axis
  double thetaCell;

  double  dx = 0.5*(y[i][x_b] - y[i][x_a]);
  double  dy = 0.5*(y[i][y_b] - y[i][y_a]);
  
  if(dx == 0) {
    if(dy == 0)
      thetaCell = 0.0;
    else if(dy > 0.0)
      thetaCell = 0.5*pi;
    else //if(dy < 0)
      thetaCell = 1.5*pi;
  }
  else if(dx > 0 && dy >= 0)
    thetaCell = atan(dy/dx);
  else if(dx < 0 && dy >= 0)
    thetaCell = pi + atan(dy/dx);
  else if(dx < 0 && dy < 0)
    thetaCell = pi + atan(dy/dx);
  else //if(dx > 0 && dy < 0)
    thetaCell = 2*pi + atan(dy/dx);

  //find neigbors and update the ellips and its neighbors	       
  for( size_t n=0 ; n<compartment.numNeighbor() ; n++ ) {
    double ForceNeigh = 0;
    size_t j=compartment.neighbor(n);
    //Setting values for the neigboring ellipse
    
    double a_neigh=0.5*sqrt( (y[j][x_b]-y[j][x_a])*(y[j][x_b]-y[j][x_a])+
			     (y[j][y_b]-y[j][y_a])*(y[j][y_b]-y[j][y_a]) );
    double c_neigh = sqrt(a_neigh*a_neigh - b_cell*b_cell);
    double n_aNeigh[2] = { (y[j][x_b]-y[j][x_a])/(2*a_neigh),
			   (y[j][y_b]-y[j][y_a])/(2*a_neigh) };

    double INeigh = 5.0*(b_cell*b_cell + a_neigh*a_neigh)/4;
    //find where the line x + n_a*t crosses neighboring ellipse,
    //i.e solve alpha*t^2 + beta*t + gamma = 0
    double t1, t2, t;  
    double alpha = 16*(c_neigh*c_neigh*
		       (n_aCell[0]*n_aNeigh[0]+n_aCell[1]*n_aNeigh[1])*
		       (n_aCell[0]*n_aNeigh[0] + n_aCell[1]*n_aNeigh[1])- 
		       a_neigh*a_neigh);
    double B = 4.0*(c_neigh*(y[i][x_a]*n_aNeigh[0] + y[i][y_a]*n_aNeigh[1])-
		    c_neigh*(y[j][x_a]*n_aNeigh[0] + y[j][y_a]*n_aNeigh[1])-
		    a_neigh*a_neigh -a_neigh*c_neigh );
    
    double beta = 8*c_neigh*
      (n_aCell[0]*n_aNeigh[0]+n_aCell[1]*n_aNeigh[1])*B-
      32*a_neigh*a_neigh*( (y[i][x_a]*n_aCell[0] + y[i][y_a]*n_aCell[1])-
			   (y[j][x_a]*n_aCell[0] + y[j][y_a]*n_aCell[1])-
			   (a_neigh + c_neigh)*
			   (n_aCell[0]*n_aNeigh[0]+n_aCell[1]*n_aNeigh[1]) 
			   );
    double gamma = B*B-16*a_neigh*a_neigh*
      ( (y[i][x_a]-y[j][x_a] - (a_neigh+c_neigh)*n_aNeigh[0])*
	(y[i][x_a]-y[j][x_a] - (a_neigh+c_neigh)*n_aNeigh[0])+
	(y[i][y_a]-y[j][y_a] - (a_neigh+c_neigh)*n_aNeigh[1])*
	(y[i][y_a]-y[j][y_a] - (a_neigh+c_neigh)*n_aNeigh[1]) );
    
    //Find angle theta between n_aNeigh and the x-axis
    double thetaNeigh;
    dx = 0.5*(y[j][x_b] - y[j][x_a]);
    dy = 0.5*(y[j][y_b] - y[j][y_a]);
    if(dx == 0) {
      if(dy == 0)
	thetaNeigh = 0.0;
      else if(dy > 0.0)
	thetaNeigh = 0.5*pi;
      else //if(dy < 0)
	thetaNeigh = 1.5*pi;
    }
    else if(dx > 0 && dy >= 0)
      thetaNeigh = atan(dy/dx);
    else if(dx < 0 && dy >= 0)
      thetaNeigh = pi + atan(dy/dx);
    else if(dx < 0 && dy < 0)
      thetaNeigh = pi + atan(dy/dx);
    else //if(dx > 0 && dy < 0)
      thetaNeigh = 2*pi + atan(dy/dx);
    
    if(alpha != 0.0) {
      double d = beta*beta/(4*alpha*alpha) - gamma/alpha;
      if(d >= 0.0) {
	t1 = -beta/(2*alpha) + sqrt(d);
	t2 = -beta/(2*alpha) - sqrt(d);
	if(t1 < 2*a_cell && t1 > 0) {
	  solutionFlag = 1;
	  if(t2 < 2*a_cell && t2 > 0)
	    t = t1<t2 ? t1:t2;
	  else
	    t = t1;
	} else if(t2 < 2*a_cell && t2 > 0) {
	  solutionFlag = 1;
	  if(t1 < 2*a_cell && t1 > 0)
	    t = t1<t2 ? t1:t2;
	  else
	    t = t2;
	} 
	else {
	  solutionFlag = 0;
	}
	//if( solutionFlag ) {
	//std::cerr << "a " << d << " " << t << " (" << t1 << "," 
	//    << t2 << ") "
	//    << n_aCell[0] << " " << n_aCell[1] << " " << a_cell 
	//    << " " << alpha << " " << beta << " " << gamma << "\n"; 
	//}
      }
      else {
	solutionFlag = 0;//d<0.0
      }
    }
    else {
      if(beta != 0) {
	t = -gamma/beta;
	if(t < 2*a_cell && t > 0)
	  solutionFlag = 1;
	else
	  solutionFlag = 0;	
      }
      else 
	solutionFlag = 0;
    }
    
    //if solutions are found, ellipses are updated
    if(solutionFlag) {
      //std::cerr << tmpT++ << " " << i << " " << j << " " << a_cell << " " 
      //	<< b_cell << " " << t << " a\n"; 
      double x_cross = y[i][x_a] + n_aCell[0]*t;
      double y_cross = y[i][y_a] + n_aCell[1]*t;
      
      //Find normal direction at the crossing point in the neighbor 
      //ellipse reference frame
      double normalTmp[2],normal[2];
      double  x_crossNeighTmp = x_cross - y[j][x_a] - a_neigh*n_aNeigh[0];
      double  y_crossNeighTmp = y_cross - y[j][y_a] - a_neigh*n_aNeigh[1];
      //rotate it -theta
      double  x_crossNeigh = cos(thetaNeigh)*x_crossNeighTmp+sin(thetaNeigh)*y_crossNeighTmp;
      double  y_crossNeigh = -sin(thetaNeigh)*x_crossNeighTmp+cos(thetaNeigh)*y_crossNeighTmp;
      //Get normal
      normalTmp[0] = (2*x_crossNeigh/(a_neigh*a_neigh)) / sqrt( 4*x_crossNeigh*x_crossNeigh /(a_neigh*a_neigh*a_neigh*a_neigh) +4*y_crossNeigh*y_crossNeigh /(b_cell*b_cell*b_cell*b_cell) );
      normalTmp[1] =(2*y_crossNeigh/(b_cell*b_cell)) / sqrt( 4*x_crossNeigh*x_crossNeigh /(a_neigh*a_neigh*a_neigh*a_neigh) +4*y_crossNeigh*y_crossNeigh /(b_cell*b_cell*b_cell*b_cell) );
      //rotate it theta
      normal[0] = cos(thetaNeigh)*normalTmp[0]-sin(thetaNeigh)*normalTmp[1];
      normal[1] = sin(thetaNeigh)*normalTmp[0]+cos(thetaNeigh)*normalTmp[1];
      
      //Find the force in the normal direction
      double F[2];
      double distance;
      if (t <= a_cell) {
	distance = a_cell-t;
	if(t >= parameter(2)) {
	F[0] = parameter(0)*(t-parameter(2))*normal[0];
	F[1] = parameter(0)*(t-parameter(2))*normal[1];
	ForceCell += sqrt( F[0]*F[0] + F[1]*F[1] );
	ForceNeigh += ForceCell;
	}
	else {
	  F[0] = parameter(0)*parameter(3)*(t-parameter(2))*normal[0];
	  F[1] = parameter(0)*parameter(3)*(t-parameter(2))*normal[1];
	}
      }
      else {
	distance = t - a_cell;
	if(t <= 2*a_cell-parameter(2) ) {
	  F[0] = parameter(0)*(2.0*a_cell-parameter(2)-t)*normal[0];
	  F[1] = parameter(0)*(2.0*a_cell-parameter(2)-t)*normal[1];
	  ForceCell += sqrt( F[0]*F[0] + F[1]*F[1] );
	  ForceNeigh += ForceCell;
	}
	else {
	  F[0] = parameter(0)*parameter(3)*(2.0*a_cell-parameter(2)-t)*normal[0];
	  F[1] = parameter(0)*parameter(3)*(2.0*a_cell-parameter(2)-t)*normal[1];
	}
      }

      double n_com[2] = {(y[i][x_a]+a_cell*n_aCell[0] - x_cross),
			 (y[i][y_a]+a_cell*n_aCell[1] - y_cross) };
      double norm = std::sqrt( n_com[0]*n_com[0]+n_com[1]*n_com[1] );
      n_com[0] /= norm;
      n_com[1] /= norm;
      if( F[0]*n_com[0]+F[1]*n_com[1]<0.0 ) {
	F[0]=F[1]=0.0;
	//std::cerr << "NormalEllipticRepulsion::derivs() "
	//  << "wrong center of mass contribution on i (a).\n";
	//std::cerr << "Fn_com " << F[0]*n_com[0]+F[1]*n_com[1] << "\n";
	//std::cerr << "xyCross: " << x_cross << " " << y_cross << "\n";
	//std::cerr << "xyCrossNeigh: " << x_crossNeigh << " " << y_crossNeigh << "\n";
	//std::cerr << "thetaNeigh: " << thetaNeigh << "\n";
	//std::cerr << "normalTmp: " << normalTmp[0] << " " << normalTmp[1] << "\n";
	//std::cerr << "normal: " << normal[0] << " " << normal[1] << "\n";
	//std::cerr << "n_com: " << n_com[0] << " " << n_com[1] << "\n";
	//std::cerr << "F: " << F[0] << " " << F[1] << "\n";
	//exit(-1);
      }
     
      //Contributions to the COM of the cell in question
      dydt[i][x_a] += (F[0]*n_com[0] + F[1]*n_com[1])*n_com[0];
      dydt[i][y_a] += (F[0]*n_com[0] + F[1]*n_com[1])*n_com[1];
      dydt[i][x_b] += (F[0]*n_com[0] + F[1]*n_com[1])*n_com[0];
      dydt[i][y_b] += (F[0]*n_com[0] + F[1]*n_com[1])*n_com[1];
      
      //Find component of the force perpendicular to COM component
       double n_perp[2]= {n_com[1], -n_com[0]};
       if(n_perp[0]*F[0] +n_perp[1]*F[1] < 0) {
	 n_perp[0] = - n_com[1];
	 n_perp[1] =  n_com[0];
       }
      
      //Rotational contribution of the cell in question
      double F_perp =(F[0]*n_perp[0] + F[1]*n_perp[1]); 
      //change sign if negative rotation (thetaNeigh-dt)
      if( n_perp[0]*n_com[1]-n_perp[1]*n_com[0]<0 )
	F_perp = -F_perp;
      double deriv = a_cell*F_perp*distance / ICell;
      dydt[i][x_a] += deriv*sin(thetaCell);
      dydt[i][y_a] -= deriv*cos(thetaCell); 
      dydt[i][x_b] -= deriv*sin(thetaCell);
      dydt[i][y_b] += deriv*cos(thetaCell); 
      
      //Contributions to the neighboring cell      
      //find the direction to the center of mass of the neighboring cell
      double dist = sqrt( ( (y[j][x_a]+a_neigh*n_aNeigh[0] - x_cross)*
			    (y[j][x_a]+a_neigh*n_aNeigh[0] - x_cross) )+
			  ( (y[j][y_a]+a_neigh*n_aNeigh[1] - y_cross)*
			    (y[j][y_a]+a_neigh*n_aNeigh[1] - y_cross) ) );
      n_com[0] = (y[j][x_a]+a_neigh*n_aNeigh[0] - x_cross)/dist;
      n_com[1] = (y[j][y_a]+a_neigh*n_aNeigh[1] - y_cross)/dist;
//       if( F[0]*n_com[0] + F[1]*n_com[1] > 0.0 ) {
// 	//Would result in pushing in direction away from com
// 	std::cerr << "NormalEllipticRepulsion::derivs() Wrong sign on force.\n"
// 		  << "Cells " << i << " -> " << j << "\n";
// 	std::cerr << "\n1\n2 " << y[i].size() << "\n";
// 	for( size_t k=0 ; k<y[i].size() ; k++ )
// 	  std::cerr << y[i][k] << " ";
// 	std::cerr << "\n";
// 	for( size_t k=0 ; k<y[j].size() ; k++ )
// 	  std::cerr << y[j][k] << " ";
// 	std::cerr << "\n\n";
	
// 	exit(-1);
//       }
      //Contributions from the COM-component of the force
      dydt[j][x_a] -= (F[0]*n_com[0] + F[1]*n_com[1])*n_com[0];
      dydt[j][y_a] -= (F[0]*n_com[0] + F[1]*n_com[1])*n_com[1];
      dydt[j][x_b] -= (F[0]*n_com[0] + F[1]*n_com[1])*n_com[0];
      dydt[j][y_b] -= (F[0]*n_com[0] + F[1]*n_com[1])*n_com[1];
      
      //Find component of the force perpendicular to COM component
      n_perp[0]= n_com[1];
      n_perp[1]= -n_com[0];
      if(n_perp[0]*F[0] +n_perp[1]*F[1] > 0) {
	n_perp[0] = - n_com[1];
	n_perp[1] =  n_com[0];
      }
      
      //Rotational contibutions 
      F_perp =-(F[0]*n_perp[0] + F[1]*n_perp[1]); 
      //change sign if negative rotation (thetaNeigh-dt)
      if( n_perp[0]*n_com[1]-n_perp[1]*n_com[0]<0 )
	F_perp = -F_perp;
      deriv = a_neigh*F_perp*dist / INeigh;
      dydt[j][x_a] += deriv*sin(thetaNeigh);
      dydt[j][y_a] -= deriv*cos(thetaNeigh); 
      dydt[j][x_b] -= deriv*sin(thetaNeigh);
      dydt[j][y_b] += deriv*cos(thetaNeigh); 
    }

    //Divide the major-axis in numStep steps 
    //and check for overlap in the b-direction at each of these points
    int numStep = 20;
    double step = a_cell/numStep;
    
    for(int k = 0; k < numStep-1; k++ ) { 
      solutionFlag = 0;
      alpha = 16*(c_neigh*c_neigh*
		  (n_bCell[0]*n_aNeigh[0]+n_bCell[1]*n_aNeigh[1])*
		  (n_bCell[0]*n_aNeigh[0] + n_bCell[1]*n_aNeigh[1]) - 
		  a_neigh*a_neigh);
      
      B = 4.0*(c_neigh*(y[i][x_a]*n_aNeigh[0] + y[i][y_a]*n_aNeigh[1]) +
	       step*c_neigh*
	       (n_aCell[0]*n_aNeigh[0]+n_aCell[1]*n_aNeigh[1])-
	       c_neigh*(y[j][x_a]*n_aNeigh[0] + y[j][y_a]*n_aNeigh[1]) -
	       a_neigh*a_neigh - a_neigh*c_neigh ); 
      
      beta = 8*c_neigh*(n_bCell[0]*n_aNeigh[0] + n_bCell[1]*n_aNeigh[1])*B-
	32*a_neigh*a_neigh*
	( (y[i][x_a]*n_bCell[0] + y[i][y_a]*n_bCell[1]) -
	  (y[j][x_a]*n_bCell[0] + y[j][y_a]*n_bCell[1]) -
	  (a_neigh + c_neigh)*
	  (n_bCell[0]*n_aNeigh[0]+n_bCell[1]*n_aNeigh[1]) );
      gamma = B*B - 16*a_neigh*a_neigh*
	( (y[i][x_a] + step*n_aCell[0]- y[j][x_a]- (a_neigh + c_neigh)*
	   n_aNeigh[0])*
	  (y[i][x_a] + step*n_aCell[0] - y[j][x_a] - (a_neigh + c_neigh)*
	   n_aNeigh[0]) +
	  (y[i][y_a]+ step*n_aCell[1] - y[j][y_a] - (a_neigh + c_neigh)*
	   n_aNeigh[1])*
	  (y[i][y_a]+ step*n_aCell[1]- y[j][y_a] - (a_neigh + c_neigh)*
	   n_aNeigh[1]) );
      
      double root = (a_cell*a_cell*a_cell*a_cell + 
		     (step- a_cell)*(step- a_cell)*(c_cell*c_cell - a_cell*a_cell) -
		     a_cell*a_cell*c_cell*c_cell) / (a_cell*a_cell);
      if(root < 0) {
	std::cerr << "Warning! False root\n";
	exit(-1);
      }
      
      double limit = sqrt(root);
      if(alpha != 0.0) {
	double d = beta*beta/(4*alpha*alpha) - gamma/alpha;
	if(d>= 0.0) {
	  t1 = -beta/(2*alpha) + sqrt(d);
	  t2 = -beta/(2*alpha) - sqrt(d);
	  if(t1 < limit && t1 > - limit) {
	    solutionFlag = 1;
	    t = t1;
	    if(t2 < limit && t2 > -limit){
	      t = t2<t1 ? t2 : t1;
	    }
	    
	  } else if(t2 < limit && t2 > -limit) {
	    solutionFlag = 1;
	    t = t2;
	    if(t1 < limit && t1 > -limit) {
	      t = t2<t1 ? t2 : t1;
	    }	  
	  } 
	  else {
	    solutionFlag = 0;
	  }
	}
	else {
	  solutionFlag = 0;
	}
      }
      else {
	if(beta != 0) {
	  t = -gamma/beta;
	  if(t < limit && t > -limit)
	    solutionFlag = 1;
	  else
	    solutionFlag = 0;	
	}
	else 
	  solutionFlag = 0;
      }
      
      //if solutions are found, ellipses are updated
      if(solutionFlag) {
	//std::cerr << tmpT++ << " " << i << " " << j << " " << a_cell << " " 
	//	<< b_cell << " " << t << " b\n"; 
	double x_cross = y[i][x_a] + step*n_aCell[0] + n_bCell[0]*t;
	double y_cross = y[i][y_a] + step*n_aCell[1] + n_bCell[1]*t;
	
	//Find normal direction at the crossing point in ellipse reference frame
	double normalTmp[2],normal[2];
	double  x_crossNeighTmp = x_cross - y[j][x_a] - a_neigh*n_aNeigh[0];
	double  y_crossNeighTmp = y_cross - y[j][y_a] - a_neigh*n_aNeigh[1];
	//Rotate theta
	double  x_crossNeigh = cos(thetaNeigh)*x_crossNeighTmp + sin(thetaNeigh)*y_crossNeighTmp;
	double  y_crossNeigh = -sin(thetaNeigh)*x_crossNeighTmp + cos(thetaNeigh)*y_crossNeighTmp;
	normalTmp[0] = (2*x_crossNeigh/(a_neigh*a_neigh)) / sqrt( 4*x_crossNeigh*x_crossNeigh /(a_neigh*a_neigh*a_neigh*a_neigh) +4*y_crossNeigh*y_crossNeigh /(b_cell*b_cell*b_cell*b_cell) );
	normalTmp[1] =(2*y_crossNeigh/(b_cell*b_cell)) / sqrt( 4*x_crossNeigh*x_crossNeigh /(a_neigh*a_neigh*a_neigh*a_neigh) +4*y_crossNeigh*y_crossNeigh /(b_cell*b_cell*b_cell*b_cell) );
	//rotate it -theta
	normal[0] = cos(thetaNeigh)*normalTmp[0] - sin(thetaNeigh)*normalTmp[1];
	normal[1] = sin(thetaNeigh)*normalTmp[0] + cos(thetaNeigh)*normalTmp[1];
	
	//Find the force in the normal direction
	double F[2]; 
	double distance = fabs(t);
	if (t >= 0 && t <= limit ) {
	  if(t <= limit - parameter(2)) {
	    F[0] = parameter(0)*(limit - parameter(2) - t)*normal[0];
	    F[1] = parameter(0)*(limit - parameter(2) - t)*normal[1];
	    ForceCell += sqrt(F[0]*F[0] + F[1]*F[1] );
	    ForceNeigh += ForceCell;
	  }
	  else {
	    F[0] = parameter(0)*parameter(3)*(limit - parameter(2) - t)*normal[0];
	    F[1] = parameter(0)*parameter(3)*(limit - parameter(2) - t)*normal[1];
	  }
	}
	else if( t<0 && t>= -limit  ) {
	  if(t >= -limit + parameter(2)) {
	    F[0] = parameter(0)*(limit - parameter(2) + t)*normal[0];
	    F[1] = parameter(0)*(limit - parameter(2) + t)*normal[1];
	    ForceCell += sqrt(F[0]*F[0] + F[1]*F[1] );
	    ForceNeigh += ForceCell;
	  }
	  else {
	    F[0] = parameter(0)*parameter(3)*(limit - parameter(2) + t )*normal[0];
	    F[1] = parameter(0)*parameter(3)*(limit - parameter(2) + t )*normal[1];
	  }
	}
	else {
	  F[0]=F[1]=0.0;
	  std::cerr << "NormalEllipticRepulsion.derivs() Warning t outside used\n";  
	}
	double n_com[2] = { (y[i][x_a]+a_cell*n_aCell[0] - x_cross),
			    (y[i][y_a]+a_cell*n_aCell[1] - y_cross) };
	double norm = std::sqrt( n_com[0]*n_com[0] + n_com[1]*n_com[1] );
	n_com[0] /= norm;
	n_com[1] /= norm;
	
	if( F[0]*n_com[0]+F[1]*n_com[1]<0.0 ) {
	  F[0]=F[1]=0.0;
	  //std::cerr << "NormalEllipticRepulsion::derivs() "
	  //    << "wrong center of mass contribution on i (0.5*a).\n";
	  //std::cerr << "Fn_com " << F[0]*n_com[0]+F[1]*n_com[1] << "\n";
	  //std::cerr << "xyCross: " << x_cross << " " << y_cross << "\n";
	  //std::cerr << "xyCrossNeigh: " << x_crossNeigh << " " << y_crossNeigh << "\n";
	  //std::cerr << "thetaNeigh: " << thetaNeigh << "\n";
	  //std::cerr << "normalTmp: " << normalTmp[0] << " " << normalTmp[1] << "\n";
	  //std::cerr << "normal: " << normal[0] << " " << normal[1] << "\n";
	  //std::cerr << "n_com: " << n_com[0] << " " << n_com[1] << "\n";
	  //std::cerr << "F: " << F[0] << " " << F[1] << "\n";
	  //exit(-1);
	}
	
	
	//Contributions to the COM of the cell in question
	dydt[i][x_a] += (F[0]*n_com[0] + F[1]*n_com[1])*n_com[0];
	dydt[i][y_a] += (F[0]*n_com[0] + F[1]*n_com[1])*n_com[1];
	dydt[i][x_b] += (F[0]*n_com[0] + F[1]*n_com[1])*n_com[0];
	dydt[i][y_b] += (F[0]*n_com[0] + F[1]*n_com[1])*n_com[1];
	
	//Find component of the force perpendicular to COM component
	double n_perp[2]= {n_com[1], -n_com[0]};
	if(n_perp[0]*F[0] +n_perp[1]*F[1] > 0) {
	  n_perp[0] = - n_com[1];
	  n_perp[1] =  n_com[0];
	}
	
	//Rotational contribution of the cell in question
	double F_perp =(F[0]*n_perp[0] + F[1]*n_perp[1]); 
	//change sign if negative rotation (thetaNeigh-dt)
	if( n_perp[0]*n_com[1]-n_perp[1]*n_com[0]<0 )
	  F_perp = -F_perp;
	double deriv = a_cell*F_perp*distance / ICell;
	dydt[i][x_a] += deriv*sin(thetaCell);
	dydt[i][y_a] -= deriv*cos(thetaCell); 
	dydt[i][x_b] -= deriv*sin(thetaCell);
	dydt[i][y_b] += deriv*cos(thetaCell); 
	
	//Contributions to the neighboring cell      
	//Contributions to the neighboring cell
	//find the direction to the center of mass of the neighboring cell
	double dist = sqrt( 
			   ( (y[j][x_a] + a_neigh*n_aNeigh[0] - x_cross)*
			     (y[j][x_a] + a_neigh*n_aNeigh[0] - x_cross) ) +
			   ( (y[j][y_a] + a_neigh*n_aNeigh[1] - y_cross)*
			     (y[j][y_a] + a_neigh*n_aNeigh[1] - y_cross) ) );
	
	n_com[0] = (y[j][x_a] + a_neigh*n_aNeigh[0] - x_cross)/dist;
	n_com[1] = (y[j][y_a] + a_neigh*n_aNeigh[1] - y_cross)/dist;
	//       if( F[0]*n_com[0] + F[1]*n_com[1] > 0.0 ) {
	// 	//Would result in pushing in direction away from com
	// 	std::cerr << "NormalEllipticRepulsion::derivs() Wrong sign on force.\n"
	// 		  << "Cells " << i << " ->(b) " << j << "\n";
	// 	std::cerr << "\n1\n2 " << y[i].size() << "\n";
	// 	for( int k=0 ; k<y[i].size() ; k++ )
	// 	  std::cerr << y[i][k] << " ";
	// 	std::cerr << "\n";
	// 	for( int k=0 ; k<y[j].size() ; k++ )
	// 	  std::cerr << y[j][k] << " ";
	// 	std::cerr << "\n\n";	
	// 	exit(-1);
	//       }
	//Contributions from the COM-component of the force
	dydt[j][x_a] -= (F[0]*n_com[0] + F[1]*n_com[1])*n_com[0];
	dydt[j][y_a] -= (F[0]*n_com[0] + F[1]*n_com[1])*n_com[1];
	dydt[j][x_b] -= (F[0]*n_com[0] + F[1]*n_com[1])*n_com[0];
	dydt[j][y_b] -= (F[0]*n_com[0] + F[1]*n_com[1])*n_com[1];
	
	//Find component of the force perpendicular to COM component
	n_perp[0]= n_com[1]; 
	n_perp[1]=-n_com[0];
	//if(n_perp[0]*n_bCell[0] +n_perp[1]*n_bCell[1] > 0) {
	if(n_perp[0]*F[0] +n_perp[1]*F[1] > 0) {
	  n_perp[0] = - n_com[1];
	  n_perp[1] =  n_com[0];
	}
	
	//Rotational contibutions 
	F_perp =-(F[0]*n_perp[0] + F[1]*n_perp[1]); 
	//change sign if negative rotation (thetaNeigh-dt)
	if( n_perp[0]*n_com[1]-n_perp[1]*n_com[0]<0 )
	  F_perp = -F_perp;
	deriv = a_neigh*F_perp*dist /INeigh;
	dydt[j][x_a] += deriv*sin(thetaNeigh);
	dydt[j][y_a] -= deriv*cos(thetaNeigh); 
	dydt[j][x_b] -= deriv*sin(thetaNeigh);
	dydt[j][y_b] += deriv*cos(thetaNeigh);       
      }
      
      step += 2*a_cell/numStep;
    }
    y[j][variableIndex(0,0)] += ForceNeigh;
  }
  y[i][variableIndex(0,0)] += ForceCell;
}

//!Constructor for the NormalEllipticRepulsion  class
SpringAsymmetricEllipse::
SpringAsymmetricEllipse(std::vector<double> &paraValue, 
			std::vector< std::vector<size_t> > 
			&indValue ) {
  
  //Do some checks on the parameters and variable indeces
  //////////////////////////////////////////////////////////////////////
  if( paraValue.size()!=6 ) {
    std::cerr << "SpringAsymmetricEllipse::SpringAsymmetricEllipse() "
	      << "Uses four parameters K_force, b, d_overlap "
	      << ", K_adh, Pow_rep and NumSpring.\n";
    exit(0);
  }
  if( indValue.size() > 1 || 
      (indValue.size()==1 && indValue[0].size()!=1) ) {
    std::cerr << "SpringAsymmetricEllipse::SpringAsymmetricEllipse() "
	      << "One variable index can be used, "
	      << "Index to store force.\n";
    exit(0);
  }
  //Set the variable values
  //////////////////////////////////////////////////////////////////////
  setId("springAsymmetricEllipse");
  setParameter(paraValue);  
  setVariableIndex(indValue);
  
  //Set the parameter identities
  //////////////////////////////////////////////////////////////////////
  std::vector<std::string> tmp( numParameter() );
  tmp[0] = "K_force";
  tmp[1] = "b";
  tmp[2] = "d_overlap";
  tmp[3] = "K_adh";
  tmp[4] = "Pow_rep";
  tmp[5] = "NumSpring";
  setParameterId( tmp );
}

//! Derivative contribution for a mechanical interaction from a spring.
/*! Deriving the time derivative contribution from a mechanical spring
  using the values in y and add the results to dydt. The only
  difference from a normal spring is that an extra parameter defining
  the strength of the adhesion compared to the repelling spring
  constant is needed for adhesion K=K_force*K_adhFrac.
*/
void SpringAsymmetricEllipse::
derivs(Compartment &compartment,size_t varIndex,
       std::vector< std::vector<double> > &y,
       std::vector< std::vector<double> > &dydt ) {
  
  //static int tmpT=0;
  
  //Checking for correct number of variables
  if( compartment.numTopologyVariable() != 5 ) {
    std::cerr << "SpringAsymmetricEllipse::derivs() "
	      << "Topological error, " 
	      << compartment.numTopologyVariable() 
	      << " is not equal to 5\n";
    exit(-1);
  }
  //Listing the columns used
  size_t i=compartment.index(), x_a=varIndex, y_a=varIndex+1, 
    x_b=varIndex+2, y_b=varIndex+3; 
  
  if(!i) {
    for(size_t k = 0; k<y.size(); k++)
      y[k][variableIndex(0,0)] = 0;
  }
  //Setting values for the ellipse in question
  double ForceCell = 0;
  double pi = 3.141592654;
 
  bool solutionFlag;
  
  double a_cell = 0.5*sqrt( (y[i][x_b]-y[i][x_a])*(y[i][x_b]-y[i][x_a])+
			    (y[i][y_b]-y[i][y_a])*(y[i][y_b]-y[i][y_a]) );
  double b_cell = parameter(1);
  if(a_cell < b_cell) {
    std::cerr << "SpringAsymmetricEllipse::derivs() "
	      << "The major axis a = " << a_cell 
	      << " must be greater than the minor axis b = " 
	      << b_cell << "\n";
    exit(-1); 
  } 
  double c_cell = sqrt(a_cell*a_cell - b_cell*b_cell);
  double n_aCell[2] = { (y[i][x_b]-y[i][x_a])/(2*a_cell),
			(y[i][y_b]-y[i][y_a])/(2*a_cell) };
  double n_bCell[2] = { n_aCell[1], -n_aCell[0] };
  double ICell = 5.0*(b_cell*b_cell + a_cell*a_cell)/4;
  //Find angle theta between n_aNeigh and the x-axis
  double thetaCell;
  
  double  dx = 0.5*(y[i][x_b] - y[i][x_a]);
  double  dy = 0.5*(y[i][y_b] - y[i][y_a]);
  
  if(dx == 0) {
    if(dy == 0)
      thetaCell = 0.0;
    else if(dy > 0.0)
      thetaCell = 0.5*pi;
    else //if(dy < 0)
      thetaCell = 1.5*pi;
  }
  else if(dx > 0 && dy >= 0)
    thetaCell = atan(dy/dx);
  else if(dx < 0 && dy >= 0)
    thetaCell = pi + atan(dy/dx);
  else if(dx < 0 && dy < 0)
    thetaCell = pi + atan(dy/dx);
  else //if(dx > 0 && dy < 0)
    thetaCell = 2*pi + atan(dy/dx);
  
  //find neigbors and update the ellips and its neighbors	       
  for( size_t n=0 ; n<compartment.numNeighbor() ; n++ ) {
    double ForceNeigh = 0;
    size_t j=compartment.neighbor(n);
    //Setting values for the neigboring ellipse
    
    double a_neigh=0.5*sqrt( (y[j][x_b]-y[j][x_a])*(y[j][x_b]-y[j][x_a])+
			     (y[j][y_b]-y[j][y_a])*(y[j][y_b]-y[j][y_a]) );
    double c_neigh = sqrt(a_neigh*a_neigh - b_cell*b_cell);
    double n_aNeigh[2] = { (y[j][x_b]-y[j][x_a])/(2*a_neigh),
			   (y[j][y_b]-y[j][y_a])/(2*a_neigh) };
    
    double INeigh = 5.0*(b_cell*b_cell + a_neigh*a_neigh)/4;
    //find where the line x + n_a*t crosses neighboring ellipse,
    //i.e solve alpha*t^2 + beta*t + gamma = 0
    double t1, t2, t;  
    double alpha = 16*(c_neigh*c_neigh*
		       (n_aCell[0]*n_aNeigh[0]+n_aCell[1]*n_aNeigh[1])*
		       (n_aCell[0]*n_aNeigh[0] + n_aCell[1]*n_aNeigh[1])- 
		       a_neigh*a_neigh);
    double B = 4.0*(c_neigh*(y[i][x_a]*n_aNeigh[0] + y[i][y_a]*n_aNeigh[1])-
		    c_neigh*(y[j][x_a]*n_aNeigh[0] + y[j][y_a]*n_aNeigh[1])-
		    a_neigh*a_neigh -a_neigh*c_neigh );
    
    double beta = 8*c_neigh*
      (n_aCell[0]*n_aNeigh[0]+n_aCell[1]*n_aNeigh[1])*B-
      32*a_neigh*a_neigh*( (y[i][x_a]*n_aCell[0] + y[i][y_a]*n_aCell[1])-
			   (y[j][x_a]*n_aCell[0] + y[j][y_a]*n_aCell[1])-
			   (a_neigh + c_neigh)*
			   (n_aCell[0]*n_aNeigh[0]+n_aCell[1]*n_aNeigh[1]) 
			   );
    double gamma = B*B-16*a_neigh*a_neigh*
      ( (y[i][x_a]-y[j][x_a] - (a_neigh+c_neigh)*n_aNeigh[0])*
	(y[i][x_a]-y[j][x_a] - (a_neigh+c_neigh)*n_aNeigh[0])+
	(y[i][y_a]-y[j][y_a] - (a_neigh+c_neigh)*n_aNeigh[1])*
	(y[i][y_a]-y[j][y_a] - (a_neigh+c_neigh)*n_aNeigh[1]) );
    
    //Find angle theta between n_aNeigh and the x-axis
    double thetaNeigh;
    dx = 0.5*(y[j][x_b] - y[j][x_a]);
    dy = 0.5*(y[j][y_b] - y[j][y_a]);
    if(dx == 0) {
      if(dy == 0)
	thetaNeigh = 0.0;
      else if(dy > 0.0)
	thetaNeigh = 0.5*pi;
      else //if(dy < 0)
	thetaNeigh = 1.5*pi;
    }
    else if(dx > 0 && dy >= 0)
      thetaNeigh = atan(dy/dx);
    else if(dx < 0 && dy >= 0)
      thetaNeigh = pi + atan(dy/dx);
    else if(dx < 0 && dy < 0)
      thetaNeigh = pi + atan(dy/dx);
    else //if(dx > 0 && dy < 0)
      thetaNeigh = 2*pi + atan(dy/dx);
    
    if(alpha != 0.0) {
      double d = beta*beta/(4*alpha*alpha) - gamma/alpha;
      if(d >= 0.0) {
	t1 = -beta/(2*alpha) + sqrt(d);
	t2 = -beta/(2*alpha) - sqrt(d);
	if(t1 < 2*a_cell && t1 > 0) {
	  solutionFlag = 1;
	  if(t2 < 2*a_cell && t2 > 0)
	    t = std::fabs(t1-a_cell)<std::fabs(t2-a_cell) ? t1:t2;
	  else
	    t = t1;
	} else if(t2 < 2*a_cell && t2 > 0) {
	  solutionFlag = 1;
	  //if(t1 < 2*a_cell && t1 > 0)
	  //t = t1<t2 ? t1:t2;
	  //else
	  t = t2;
	} 
	else {
	  solutionFlag = 0;
	}
	//if( solutionFlag ) {
	//std::cerr << "a " << d << " " << t << " (" << t1 << "," 
	//    << t2 << ") "
	//    << n_aCell[0] << " " << n_aCell[1] << " " << a_cell 
	//    << " " << alpha << " " << beta << " " << gamma << "\n"; 
	//}
      }
      else {
	solutionFlag = 0;//d<0.0
      }
    }
    else {
      if(beta != 0) {
	t = -gamma/beta;
	if(t < 2*a_cell && t > 0)
	  solutionFlag = 1;
	else
	  solutionFlag = 0;	
      }
      else 
	solutionFlag = 0;
    }
    
    //if solutions are found, ellipses are updated
    if(solutionFlag) {
      //std::cerr << tmpT++ << " " << i << " " << j << " " << a_cell << " " 
      //	<< b_cell << " " << t << " a\n"; 
      double x_cross = y[i][x_a] + n_aCell[0]*t;
      double y_cross = y[i][y_a] + n_aCell[1]*t;
      
      //Find normal direction at the crossing point in the neighbor 
      //ellipse reference frame
      double normalTmp[2],normal[2];
      double  x_crossNeighTmp = x_cross - y[j][x_a] - a_neigh*n_aNeigh[0];
      double  y_crossNeighTmp = y_cross - y[j][y_a] - a_neigh*n_aNeigh[1];
      //rotate it -theta
      double  x_crossNeigh = cos(thetaNeigh)*x_crossNeighTmp+sin(thetaNeigh)*y_crossNeighTmp;
      double  y_crossNeigh = -sin(thetaNeigh)*x_crossNeighTmp+cos(thetaNeigh)*y_crossNeighTmp;
      //Get normal
      normalTmp[0] = (2*x_crossNeigh/(a_neigh*a_neigh)) / sqrt( 4*x_crossNeigh*x_crossNeigh /(a_neigh*a_neigh*a_neigh*a_neigh) +4*y_crossNeigh*y_crossNeigh /(b_cell*b_cell*b_cell*b_cell) );
      normalTmp[1] =(2*y_crossNeigh/(b_cell*b_cell)) / sqrt( 4*x_crossNeigh*x_crossNeigh /(a_neigh*a_neigh*a_neigh*a_neigh) +4*y_crossNeigh*y_crossNeigh /(b_cell*b_cell*b_cell*b_cell) );
      //rotate it theta
      normal[0] = cos(thetaNeigh)*normalTmp[0]-sin(thetaNeigh)*normalTmp[1];
      normal[1] = sin(thetaNeigh)*normalTmp[0]+cos(thetaNeigh)*normalTmp[1];
      
      //Find the force in the normal direction
      double F[2];
      double distance;
      if (t <= a_cell) {
	distance = a_cell-t;
	if(t >= parameter(2)) {//repulsion
	  double fac = (t-parameter(2)); 
	  if( parameter(4) != 1.0 ) fac = std::pow(fac,parameter(4));
	  fac *= parameter(0);
	  F[0] = fac*normal[0];
	  F[1] = fac*normal[1];
	  ForceCell += fac;
	  ForceNeigh += fac;
	}
	else {//adhesion
	  double fac = parameter(0)*parameter(3)*(t-parameter(2)); 
	  F[0] = fac*normal[0];
	  F[1] = fac*normal[1];
	}
      }
      else {
	distance = t - a_cell;
	if(t <= 2*a_cell-parameter(2) ) {//repulsion
	  double fac = (2.0*a_cell-parameter(2)-t); 
	  if( parameter(4) != 1.0 ) fac = std::pow(fac,parameter(4));
	  fac *= parameter(0);
	  F[0] = fac*normal[0];
	  F[1] = fac*normal[1];
	  ForceCell += fac;
	  ForceNeigh += fac;
	}
	else {//adhesion
	  double fac = parameter(0)*parameter(3)*
	    (2.0*a_cell-parameter(2)-t); 
	  F[0] = fac*normal[0];
	  F[1] = fac*normal[1];
	}
      }
      
      double n_com[2] = {(y[i][x_a]+a_cell*n_aCell[0] - x_cross),
			 (y[i][y_a]+a_cell*n_aCell[1] - y_cross) };
      double norm = std::sqrt( n_com[0]*n_com[0]+n_com[1]*n_com[1] );
      n_com[0] /= norm;
      n_com[1] /= norm;
      if( F[0]*n_com[0]+F[1]*n_com[1]<0.0 ) {
	F[0]=F[1]=0.0;
	//std::cerr << "SpringAsymmetricEllipse::derivs() "
	//  << "wrong center of mass contribution on i (a).\n";
	//std::cerr << "Fn_com " << F[0]*n_com[0]+F[1]*n_com[1] << "\n";
	//std::cerr << "xyCross: " << x_cross << " " << y_cross << "\n";
	//std::cerr << "xyCrossNeigh: " << x_crossNeigh << " " << y_crossNeigh << "\n";
	//std::cerr << "thetaNeigh: " << thetaNeigh << "\n";
	//std::cerr << "normalTmp: " << normalTmp[0] << " " << normalTmp[1] << "\n";
	//std::cerr << "normal: " << normal[0] << " " << normal[1] << "\n";
	//std::cerr << "n_com: " << n_com[0] << " " << n_com[1] << "\n";
	//std::cerr << "F: " << F[0] << " " << F[1] << "\n";
	//exit(-1);
      }
     
      //Contributions to the COM of the cell in question
      dydt[i][x_a] += (F[0]*n_com[0] + F[1]*n_com[1])*n_com[0];
      dydt[i][y_a] += (F[0]*n_com[0] + F[1]*n_com[1])*n_com[1];
      dydt[i][x_b] += (F[0]*n_com[0] + F[1]*n_com[1])*n_com[0];
      dydt[i][y_b] += (F[0]*n_com[0] + F[1]*n_com[1])*n_com[1];
      
      //Find component of the force perpendicular to COM component
      double n_perp[2]= {n_com[1], -n_com[0]};
      if(n_perp[0]*F[0] +n_perp[1]*F[1] < 0) {
	n_perp[0] = - n_com[1];
	n_perp[1] =  n_com[0];
      }
      
      //Rotational contribution of the cell in question
      double F_perp =(F[0]*n_perp[0] + F[1]*n_perp[1]); 
      //change sign if negative rotation (thetaNeigh-dt)
      if( n_perp[0]*n_com[1]-n_perp[1]*n_com[0]<0 )
	F_perp = -F_perp;
      double deriv = a_cell*F_perp*distance / ICell;
      dydt[i][x_a] += deriv*sin(thetaCell);
      dydt[i][y_a] -= deriv*cos(thetaCell); 
      dydt[i][x_b] -= deriv*sin(thetaCell);
      dydt[i][y_b] += deriv*cos(thetaCell); 
      
      //Contributions to the neighboring cell      
      //find the direction to the center of mass of the neighboring cell
      double dist = sqrt( ( (y[j][x_a]+a_neigh*n_aNeigh[0] - x_cross)*
			    (y[j][x_a]+a_neigh*n_aNeigh[0] - x_cross) )+
			  ( (y[j][y_a]+a_neigh*n_aNeigh[1] - y_cross)*
			    (y[j][y_a]+a_neigh*n_aNeigh[1] - y_cross) ) );
      n_com[0] = (y[j][x_a]+a_neigh*n_aNeigh[0] - x_cross)/dist;
      n_com[1] = (y[j][y_a]+a_neigh*n_aNeigh[1] - y_cross)/dist;
//       if( F[0]*n_com[0] + F[1]*n_com[1] > 0.0 ) {
// 	//Would result in pushing in direction away from com
// 	std::cerr << "SpringAsymmetricEllipse::derivs() Wrong sign on force.\n"
// 		  << "Cells " << i << " -> " << j << "\n";
// 	std::cerr << "\n1\n2 " << y[i].size() << "\n";
// 	for( int k=0 ; k<y[i].size() ; k++ )
// 	  std::cerr << y[i][k] << " ";
// 	std::cerr << "\n";
// 	for( int k=0 ; k<y[j].size() ; k++ )
// 	  std::cerr << y[j][k] << " ";
// 	std::cerr << "\n\n";
	
// 	exit(-1);
//       }
      //Contributions from the COM-component of the force
      dydt[j][x_a] -= (F[0]*n_com[0] + F[1]*n_com[1])*n_com[0];
      dydt[j][y_a] -= (F[0]*n_com[0] + F[1]*n_com[1])*n_com[1];
      dydt[j][x_b] -= (F[0]*n_com[0] + F[1]*n_com[1])*n_com[0];
      dydt[j][y_b] -= (F[0]*n_com[0] + F[1]*n_com[1])*n_com[1];
      
      //Find component of the force perpendicular to COM component
      n_perp[0]= n_com[1];
      n_perp[1]= -n_com[0];
      if(n_perp[0]*F[0] +n_perp[1]*F[1] > 0) {
	n_perp[0] = - n_com[1];
	n_perp[1] =  n_com[0];
      }
      
      //Rotational contibutions 
      F_perp =-(F[0]*n_perp[0] + F[1]*n_perp[1]); 
      //change sign if negative rotation (thetaNeigh-dt)
      if( n_perp[0]*n_com[1]-n_perp[1]*n_com[0]<0 )
	F_perp = -F_perp;
      deriv = a_neigh*F_perp*dist / INeigh;
      dydt[j][x_a] += deriv*sin(thetaNeigh);
      dydt[j][y_a] -= deriv*cos(thetaNeigh); 
      dydt[j][x_b] -= deriv*sin(thetaNeigh);
      dydt[j][y_b] += deriv*cos(thetaNeigh); 
    }

    //Divide the major-axis in numStep steps 
    //and check for overlap in the b-direction at each of these points
    int numStep = int( parameter(5) );
    double step = a_cell/numStep;
    
    for(int k = 0; k < numStep-1; k++ ) { 
      solutionFlag = 0;
      alpha = 16*(c_neigh*c_neigh*
		  (n_bCell[0]*n_aNeigh[0]+n_bCell[1]*n_aNeigh[1])*
		  (n_bCell[0]*n_aNeigh[0] + n_bCell[1]*n_aNeigh[1]) - 
		  a_neigh*a_neigh);
      
      B = 4.0*(c_neigh*(y[i][x_a]*n_aNeigh[0] + y[i][y_a]*n_aNeigh[1]) +
	       step*c_neigh*
	       (n_aCell[0]*n_aNeigh[0]+n_aCell[1]*n_aNeigh[1])-
	       c_neigh*(y[j][x_a]*n_aNeigh[0] + y[j][y_a]*n_aNeigh[1]) -
	       a_neigh*a_neigh - a_neigh*c_neigh ); 
      
      beta = 8*c_neigh*(n_bCell[0]*n_aNeigh[0] + n_bCell[1]*n_aNeigh[1])*B-
	32*a_neigh*a_neigh*
	( (y[i][x_a]*n_bCell[0] + y[i][y_a]*n_bCell[1]) -
	  (y[j][x_a]*n_bCell[0] + y[j][y_a]*n_bCell[1]) -
	  (a_neigh + c_neigh)*
	  (n_bCell[0]*n_aNeigh[0]+n_bCell[1]*n_aNeigh[1]) );
      gamma = B*B - 16*a_neigh*a_neigh*
	( (y[i][x_a] + step*n_aCell[0]- y[j][x_a]- (a_neigh + c_neigh)*
	   n_aNeigh[0])*
	  (y[i][x_a] + step*n_aCell[0] - y[j][x_a] - (a_neigh + c_neigh)*
	   n_aNeigh[0]) +
	  (y[i][y_a]+ step*n_aCell[1] - y[j][y_a] - (a_neigh + c_neigh)*
	   n_aNeigh[1])*
	  (y[i][y_a]+ step*n_aCell[1]- y[j][y_a] - (a_neigh + c_neigh)*
	   n_aNeigh[1]) );
      
      double root = (a_cell*a_cell*a_cell*a_cell + 
		     (step- a_cell)*(step- a_cell)*(c_cell*c_cell - a_cell*a_cell) -
		     a_cell*a_cell*c_cell*c_cell) / (a_cell*a_cell);
      if(root < 0) {
	std::cerr << "Warning! False root\n";
	exit(-1);
      }
      
      double limit = sqrt(root);
      if(alpha != 0.0) {
	double d = beta*beta/(4*alpha*alpha) - gamma/alpha;
	if(d>= 0.0) {
	  t1 = -beta/(2*alpha) + sqrt(d);
	  t2 = -beta/(2*alpha) - sqrt(d);
	  if(t1 < limit && t1 > - limit) {
	    solutionFlag = 1;
	    t = t1;
	    if(t2 < limit && t2 > -limit){
	      t = std::fabs(t2)<std::fabs(t1) ? t2 : t1;
	    }	    
	  } else if(t2 < limit && t2 > -limit) {
	    solutionFlag = 1;
	    t = t2;
	    //if(t1 < limit && t1 > -limit) {
	    //t = t2<t1 ? t2 : t1;
	    //}	  
	  } 
	  else {
	    solutionFlag = 0;
	  }
	}
	else {
	  solutionFlag = 0;
	}
      }
      else {
	if(beta != 0) {
	  t = -gamma/beta;
	  if(t < limit && t > -limit)
	    solutionFlag = 1;
	  else
	    solutionFlag = 0;	
	}
	else 
	  solutionFlag = 0;
      }
      
      //if solutions are found, ellipses are updated
      if(solutionFlag) {
	//std::cerr << tmpT++ << " " << i << " " << j << " " << a_cell << " " 
	//	<< b_cell << " " << t << " b\n"; 
	double x_cross = y[i][x_a] + step*n_aCell[0] + n_bCell[0]*t;
	double y_cross = y[i][y_a] + step*n_aCell[1] + n_bCell[1]*t;
	
	//Find normal direction at the crossing point in ellipse reference frame
	double normalTmp[2],normal[2];
	double  x_crossNeighTmp = x_cross - y[j][x_a] - a_neigh*n_aNeigh[0];
	double  y_crossNeighTmp = y_cross - y[j][y_a] - a_neigh*n_aNeigh[1];
	//Rotate theta
	double  x_crossNeigh = cos(thetaNeigh)*x_crossNeighTmp + sin(thetaNeigh)*y_crossNeighTmp;
	double  y_crossNeigh = -sin(thetaNeigh)*x_crossNeighTmp + cos(thetaNeigh)*y_crossNeighTmp;
	normalTmp[0] = (2*x_crossNeigh/(a_neigh*a_neigh)) / sqrt( 4*x_crossNeigh*x_crossNeigh /(a_neigh*a_neigh*a_neigh*a_neigh) +4*y_crossNeigh*y_crossNeigh /(b_cell*b_cell*b_cell*b_cell) );
	normalTmp[1] =(2*y_crossNeigh/(b_cell*b_cell)) / sqrt( 4*x_crossNeigh*x_crossNeigh /(a_neigh*a_neigh*a_neigh*a_neigh) +4*y_crossNeigh*y_crossNeigh /(b_cell*b_cell*b_cell*b_cell) );
	//rotate it -theta
	normal[0] = cos(thetaNeigh)*normalTmp[0] - sin(thetaNeigh)*normalTmp[1];
	normal[1] = sin(thetaNeigh)*normalTmp[0] + cos(thetaNeigh)*normalTmp[1];
	
	//Find the force in the normal direction
	double F[2]; 
	double distance = fabs(t);
	if (t >= 0 && t <= limit ) {
	  if(t <= limit - parameter(2)) {//repression
	    double fac = (limit - parameter(2) - t);
	    if( parameter(4) != 1.0 ) fac = std::pow(fac,parameter(4));
	    fac *= parameter(0);
	    F[0] = fac*normal[0];
	    F[1] = fac*normal[1];
	    ForceCell += fac;
	    ForceNeigh += fac;
	  }
	  else {//adhesion
	    double fac = parameter(0)*parameter(3)*
	      (limit - parameter(2) - t); 
	    F[0] = fac*normal[0];
	    F[1] = fac*normal[1];
	  }
	}
	else if( t<0 && t>= -limit  ) {
	  if(t >= -limit + parameter(2)) {//repression
	    double fac = (limit - parameter(2) + t);
	    if( parameter(4) != 1.0 ) fac = std::pow(fac,parameter(4));
	    fac *= parameter(0);
	    F[0] = fac*normal[0];
	    F[1] = fac*normal[1];
	    ForceCell += fac;
	    ForceNeigh += fac;
	  }
	  else {//adhesion
	    double fac = parameter(0)*parameter(3)*
	      (limit - parameter(2) + t ); 
	    F[0] = fac*normal[0];
	    F[1] = fac*normal[1];
	  }
	}
	else {
	  F[0]=F[1]=0.0;
	  std::cerr << "SpringAsymmetricEllipse::derivs() "
		    << "Warning t outside used\n";  
	}
	double n_com[2] = { (y[i][x_a]+a_cell*n_aCell[0] - x_cross),
			    (y[i][y_a]+a_cell*n_aCell[1] - y_cross) };
	double norm = std::sqrt( n_com[0]*n_com[0] + n_com[1]*n_com[1] );
	n_com[0] /= norm;
	n_com[1] /= norm;
	
	if( F[0]*n_com[0]+F[1]*n_com[1]<0.0 ) {
	  F[0]=F[1]=0.0;
	  //std::cerr << "SpringAsymmetricEllipse::derivs() "
	  //    << "wrong center of mass contribution on i (0.5*a).\n";
	  //std::cerr << "Fn_com " << F[0]*n_com[0]+F[1]*n_com[1] << "\n";
	  //std::cerr << "xyCross: " << x_cross << " " << y_cross << "\n";
	  //std::cerr << "xyCrossNeigh: " << x_crossNeigh << " " << y_crossNeigh << "\n";
	  //std::cerr << "thetaNeigh: " << thetaNeigh << "\n";
	  //std::cerr << "normalTmp: " << normalTmp[0] << " " << normalTmp[1] << "\n";
	  //std::cerr << "normal: " << normal[0] << " " << normal[1] << "\n";
	  //std::cerr << "n_com: " << n_com[0] << " " << n_com[1] << "\n";
	  //std::cerr << "F: " << F[0] << " " << F[1] << "\n";
	  //exit(-1);
	}
	
	
	//Contributions to the COM of the cell in question
	dydt[i][x_a] += (F[0]*n_com[0] + F[1]*n_com[1])*n_com[0];
	dydt[i][y_a] += (F[0]*n_com[0] + F[1]*n_com[1])*n_com[1];
	dydt[i][x_b] += (F[0]*n_com[0] + F[1]*n_com[1])*n_com[0];
	dydt[i][y_b] += (F[0]*n_com[0] + F[1]*n_com[1])*n_com[1];
	
	//Find component of the force perpendicular to COM component
	double n_perp[2]= {n_com[1], -n_com[0]};
	if(n_perp[0]*F[0] +n_perp[1]*F[1] > 0) {
	  n_perp[0] = - n_com[1];
	  n_perp[1] =  n_com[0];
	}
	
	//Rotational contribution of the cell in question
	double F_perp =(F[0]*n_perp[0] + F[1]*n_perp[1]); 
	//change sign if negative rotation (thetaNeigh-dt)
	if( n_perp[0]*n_com[1]-n_perp[1]*n_com[0]<0 )
	  F_perp = -F_perp;
	double deriv = a_cell*F_perp*distance / ICell;
	dydt[i][x_a] += deriv*sin(thetaCell);
	dydt[i][y_a] -= deriv*cos(thetaCell); 
	dydt[i][x_b] -= deriv*sin(thetaCell);
	dydt[i][y_b] += deriv*cos(thetaCell); 
	
	//Contributions to the neighboring cell      
	//Contributions to the neighboring cell
	//find the direction to the center of mass of the neighboring cell
	double dist = sqrt( 
			   ( (y[j][x_a] + a_neigh*n_aNeigh[0] - x_cross)*
			     (y[j][x_a] + a_neigh*n_aNeigh[0] - x_cross) ) +
			   ( (y[j][y_a] + a_neigh*n_aNeigh[1] - y_cross)*
			     (y[j][y_a] + a_neigh*n_aNeigh[1] - y_cross) ) );
	
	n_com[0] = (y[j][x_a] + a_neigh*n_aNeigh[0] - x_cross)/dist;
	n_com[1] = (y[j][y_a] + a_neigh*n_aNeigh[1] - y_cross)/dist;
	//       if( F[0]*n_com[0] + F[1]*n_com[1] > 0.0 ) {
	// 	//Would result in pushing in direction away from com
	// 	std::cerr << "SpringAsymmetricEllipse::derivs() Wrong sign on force.\n"
	// 		  << "Cells " << i << " ->(b) " << j << "\n";
	// 	std::cerr << "\n1\n2 " << y[i].size() << "\n";
	// 	for( int k=0 ; k<y[i].size() ; k++ )
	// 	  std::cerr << y[i][k] << " ";
	// 	std::cerr << "\n";
	// 	for( int k=0 ; k<y[j].size() ; k++ )
	// 	  std::cerr << y[j][k] << " ";
	// 	std::cerr << "\n\n";	
	// 	exit(-1);
	//       }
	//Contributions from the COM-component of the force
	dydt[j][x_a] -= (F[0]*n_com[0] + F[1]*n_com[1])*n_com[0];
	dydt[j][y_a] -= (F[0]*n_com[0] + F[1]*n_com[1])*n_com[1];
	dydt[j][x_b] -= (F[0]*n_com[0] + F[1]*n_com[1])*n_com[0];
	dydt[j][y_b] -= (F[0]*n_com[0] + F[1]*n_com[1])*n_com[1];
	
	//Find component of the force perpendicular to COM component
	n_perp[0]= n_com[1]; 
	n_perp[1]=-n_com[0];
	//if(n_perp[0]*n_bCell[0] +n_perp[1]*n_bCell[1] > 0) {
	if(n_perp[0]*F[0] +n_perp[1]*F[1] > 0) {
	  n_perp[0] = - n_com[1];
	  n_perp[1] =  n_com[0];
	}
	
	//Rotational contibutions 
	F_perp =-(F[0]*n_perp[0] + F[1]*n_perp[1]); 
	//change sign if negative rotation (thetaNeigh-dt)
	if( n_perp[0]*n_com[1]-n_perp[1]*n_com[0]<0 )
	  F_perp = -F_perp;
	deriv = a_neigh*F_perp*dist /INeigh;
	dydt[j][x_a] += deriv*sin(thetaNeigh);
	dydt[j][y_a] -= deriv*cos(thetaNeigh); 
	dydt[j][x_b] -= deriv*sin(thetaNeigh);
	dydt[j][y_b] += deriv*cos(thetaNeigh);       
      }      
      step += 2*a_cell/numStep;
    }
    if( numVariableIndexLevel() )
      y[j][variableIndex(0,0)] += ForceNeigh;
  }//loop over neighbors
  if( numVariableIndexLevel() )
    y[i][variableIndex(0,0)] += ForceCell;
}




//!Constructor for the SpringAsymmetricSphereBud class
SpringAsymmetricSphereBud::
SpringAsymmetricSphereBud(std::vector<double> &paraValue, 
			  std::vector< std::vector<size_t> > &indValue ) {
  
  //Do some checks on the parameters and variable indeces
  //////////////////////////////////////////////////////////////////////
  if( paraValue.size()!=4 ) {
    std::cerr << "SpringAsymmetricSphereBud::SpringAsymmetricSphereBud() "
	      << "Uses three parameters r_relax K_force "
	      << "K_adhesion d_trunc.\n";
    exit(0);
  }
  if( indValue.size() ) {
    std::cerr << "SpringAsymmetricSphereBud::SpringAsymmetricSphereBud() "
	      << "No variable indeces is used.\n";
    exit(0);
  }
  //Set the variable values
  //////////////////////////////////////////////////////////////////////
  setId("springAsymmetricSphereBud");
  setParameter(paraValue);  
  setVariableIndex(indValue);
  
  //Set the parameter identities
  //////////////////////////////////////////////////////////////////////
  std::vector<std::string> tmp( numParameter() );
  tmp[0] = "r_relax";
  tmp[1] = "K_force";
  tmp[2] = "K_adhFrac";
  tmp[3] = "d_trunc";
  setParameterId( tmp );
}

//! Derivative contribution for a mechanical interaction from a spring.
/*! Deriving the time derivative contribution from a mechanical spring
  using the values in y and add the results to dydt. The difference
  from a normal spring is that an extra parameter defining the
  strength of the adhesion compared to the repelling spring constant
  is needed for adhesion K=K_force*K_adhFrac. Also d_trunc is added
  for disallowing for adhesion between spheres too far apart.
*/
void SpringAsymmetricSphereBud::
derivs(Compartment &compartment,size_t varIndex,
       std::vector< std::vector<double> > &y,
       std::vector< std::vector<double> > &dydt ) {
  
  if( compartment.numTopologyVariable() != 4 &&
      compartment.numTopologyVariable() != 6 &&
      compartment.numTopologyVariable() != 8 ) {
    std::cerr << "SpringAssymetricSphereBud::derivs() "
	      << "Wrong number of dimensions (topology variables.\n";
    exit(-1);
  }
  size_t i=compartment.index();
  size_t dimension = static_cast<size_t>( (compartment.numTopologyVariable()-2)/2 );
  for( size_t n=0 ; n<compartment.numNeighbor() ; n++ ) {
    size_t j=compartment.neighbor(n);
    //Only update if compartments index less than neighbors
    if( i<j ) {
      size_t r1Col=compartment.numTopologyVariable()-2;
      size_t r2Col=r1Col+1;
      double r11 = parameter(0)*( y[i][r1Col]+y[j][r1Col] );
      double r12 = parameter(0)*( y[i][r1Col]+y[j][r2Col] );
      double r21 = parameter(0)*( y[i][r2Col]+y[j][r1Col] );
      double r22 = parameter(0)*( y[i][r2Col]+y[j][r2Col] );
      
      double d11=0.,d12=0.,d21=0.,d22=0.;
      size_t x1Col=varIndex;
      size_t x2Col=varIndex+dimension;
      for(size_t dim=0 ; dim<dimension ; dim++ ) {
	d11 += (y[i][x1Col+dim]-y[j][x1Col+dim])*
	  (y[i][x1Col+dim]-y[j][x1Col+dim]);
	d12 += (y[i][x1Col+dim]-y[j][x2Col+dim])*
	  (y[i][x1Col+dim]-y[j][x2Col+dim]);
	d21 += (y[i][x2Col+dim]-y[j][x1Col+dim])*
	  (y[i][x2Col+dim]-y[j][x1Col+dim]);
	d22 += (y[i][x2Col+dim]-y[j][x2Col+dim])*
	  (y[i][x2Col+dim]-y[j][x2Col+dim]);
      }	
      if( d11<=0. || d12<=0. || d21<=0. || d22<=0. ) {
	std::cerr << "SpringAsymmetricSphereBud::derivs() "
		  << "Two cells on top of each other, exiting\n";
	std::cerr << i << "," << j << "\n" << "d=" << d11 << "," << d12 
		  << "," << d21 << "," << d22 << " r=" << r11 << ","  
		  << r12 << "," << r21 << "," << r22 << "\n";
	exit(-1);
      }
      d11 = sqrt(d11);
      d12 = sqrt(d12);
      d21 = sqrt(d21);
      d22 = sqrt(d22);
      
      //11
      if( d11 < parameter(3)*r11 ) {
	double rCoff = parameter(1)*(1.-(r11/d11));
	if( d11>r11 )
	  rCoff *=parameter(2);
	//Update for each dimension
	for(size_t dim=0 ; dim<dimension ; dim++ ) {
	  double div = (y[i][x1Col+dim]-y[j][x1Col+dim])*rCoff;
	  dydt[i][x1Col+dim] -= div;
	  dydt[j][x1Col+dim] += div;
	}
      }
      //12
      if( d12 < parameter(3)*r12 ) {
	double rCoff = parameter(1)*(1.-(r12/d12));
	if( d12>r12 )
	  rCoff *=parameter(2);
	//Update for each dimension
	for(size_t dim=0 ; dim<dimension ; dim++ ) {
	  double div = (y[i][x1Col+dim]-y[j][x2Col+dim])*rCoff;
	  dydt[i][x1Col+dim] -= div;
	  dydt[j][x2Col+dim] += div;
	}
      }
      //21
      if( d21 < parameter(3)*r21 ) {
	double rCoff = parameter(1)*(1.-(r21/d21));
	if( d21>r21 )
	  rCoff *=parameter(2);
	//Update for each dimension
	for(size_t dim=0 ; dim<dimension ; dim++ ) {
	  double div = (y[i][x2Col+dim]-y[j][x1Col+dim])*rCoff;
	  dydt[i][x2Col+dim] -= div;
	  dydt[j][x1Col+dim] += div;
	}
      }
      //22
      if( d22 < parameter(3)*r22 ) {
	double rCoff = parameter(1)*(1.-(r22/d22));
	if( d22>r22 )
	  rCoff *=parameter(2);
	//Update for each dimension
	for(size_t dim=0 ; dim<dimension ; dim++ ) {
	  double div = (y[i][x2Col+dim]-y[j][x2Col+dim])*rCoff;
	  dydt[i][x2Col+dim] -= div;
	  dydt[j][x2Col+dim] += div;
	}
      }
    }
  }
}

//!Constructor for the WallSpringEllipticElliptic class
WallSpringElliptic::WallSpringElliptic(std::vector<double> &paraValue, 
				   std::vector< std::vector<size_t> > 
				   &indValue ) {
  
  //Do some checks on the parameters and variable indeces
  //////////////////////////////////////////////////////////////////////
  if( paraValue.size()!=7 ) {
    std::cerr << "WallSpringElliptic::WallSpringElliptic()"
	      << "Uses seven parameters K_force x_1, y_1, x_2, y_2, normal direction (+1or -1) and b\n";
    exit(0);
  }
  if(paraValue[5] != 1 && paraValue[5] != -1) {
    std::cerr << "WallSpringElliptic::WallSpringElliptic()"
	      << " normal direction must be +1 or -1 (not " << paraValue[5] << ")\n";
    exit(0);
  }
  if( indValue.size() != 1) {
    std::cerr << "WallSpringElliptic::WallSpringElliptic()"
	      << "one variable index is used, force\n";
    exit(0);
  }
  //Set the variable values
  //////////////////////////////////////////////////////////////////////
  setId("wallSpringElliptic");
  setParameter(paraValue);  
  setVariableIndex(indValue);
  
  //Set the parameter identities
  //////////////////////////////////////////////////////////////////////
  std::vector<std::string> tmp( numParameter() );
  tmp[0] = "K_force";
  tmp[1] = "x_1";
  tmp[2] = "y_1";
  tmp[3] = "x_2";
  tmp[4] = "y_2";
  tmp[5] = "direction";
  tmp[6] = "b";
  setParameterId( tmp );
}

//! Derivative contribution for a mechanical interaction from a wall.
void WallSpringElliptic::derivs(Compartment &compartment,size_t varIndex,
			      std::vector< std::vector<double> > &y,
			      std::vector< std::vector<double> > &dydt ) {
  
  
  bool solutionFlag=0; 
  //Setting wall properties
  double K_W = parameter(0);
  double X_1 =  parameter(1), Y_1 =  parameter(2), X_2 =  parameter(3), Y_2 =  parameter(4);
  double wallLength = sqrt( (X_2 - X_1)*(X_2 - X_1) + (Y_2 - Y_1)*(Y_2 - Y_1) );
  double N_W[2] = { (X_2 - X_1)/wallLength, (Y_2 - Y_1)/wallLength } ;
  double normal_W[2];
  double wallForce = 0;
  //if parameter(5) = +1 chose right-handed direction of the normal, otherwize left-handed 
  if(parameter(5) > 0) {
    normal_W[0] = -N_W[1];
    normal_W[1] = N_W[0];
  } 
  else {
    normal_W[0] = N_W[1];
    normal_W[1] = -N_W[0];
  }
  //Setting values for the ellips in question
  double pi = 3.141592654;
  size_t i=compartment.index(), x_a=varIndex, y_a=varIndex+1, 
   x_b=varIndex+2, y_b=varIndex+3; ;
  double a_cell = 0.5*sqrt( (y[i][x_b]-y[i][x_a])*(y[i][x_b]-y[i][x_a])+
			    (y[i][y_b]-y[i][y_a])*(y[i][y_b]-y[i][y_a]) );
  double b_cell = parameter(6);
  if(a_cell < b_cell) {
    std::cerr << "WallSpringElliptic::derivs() "
	      << "The major axis a = " << a_cell 
	      << " must be greater than the minor axis b = " 
	      << b_cell << "\n";
    exit(-1); 
  } 
  double c_cell = sqrt(a_cell*a_cell - b_cell*b_cell);
  double n_aCell[2] = { (y[i][x_b]-y[i][x_a])/(2*a_cell),
			(y[i][y_b]-y[i][y_a])/(2*a_cell) };
  double n_bCell[2] = { n_aCell[1], -n_aCell[0] };
  double ICell = 5.0*(b_cell*b_cell + a_cell*a_cell)/4;
 
  //Find out if the cell is a potential neighbor to the wall
  //if it's not nothing is updated
  double scalProd =  N_W[0]*normal_W[1] - N_W[1]*normal_W[0];
  double t_dist = ( normal_W[0]*(Y_1 - y[i][y_a] - a_cell*n_aCell[1]) -
		    normal_W[1]*(X_1 - y[i][x_a] - a_cell*n_aCell[0]) )/scalProd;
  
  double crossPoint[2] = { X_1 + N_W[0]*t_dist, Y_1 + N_W[1]*t_dist };
  double distToCOM = sqrt( (crossPoint[0] - y[i][x_a] - a_cell*n_aCell[0])*
  		  (crossPoint[0] - y[i][x_a] - a_cell*n_aCell[0]) +
  		  (crossPoint[1] - y[i][y_a] - a_cell*n_aCell[1])*
  		  (crossPoint[1] - y[i][y_a] - a_cell*n_aCell[1]) );
  //if(distToCOM <= a_cell ) {
  // std::cerr << "WallSpringElliptic.derivs() A cell has passed the wall\n";
  // exit(-1);
  //}
  if(distToCOM <= a_cell ) {
    
    //Find angle theta between n_aNeigh and the x-axis
    double thetaCell;

    double  dx = 0.5*(y[i][x_b] - y[i][x_a]);
    double  dy = 0.5*(y[i][y_b] - y[i][y_a]);
    
    if(dx == 0) {
      if(dy == 0)
	thetaCell = 0.0;
      else if(dy > 0.0)
	thetaCell = 0.5*pi;
      else //if(dy < 0)
	thetaCell = 1.5*pi;
    }
    else if(dx > 0 && dy >= 0)
      thetaCell = atan(dy/dx);
    else if(dx < 0 && dy >= 0)
      thetaCell = pi + atan(dy/dx);
    else if(dx < 0 && dy < 0)
      thetaCell = pi + atan(dy/dx);
    else //if(dx > 0 && dy < 0)
      thetaCell = 2*pi + atan(dy/dx);

    //Check if the cell interacts with the wall in the a-direction
    scalProd = N_W[0]*n_aCell[1] - N_W[1]*n_aCell[0];
    
    double t_W, t_cell;
    if(scalProd) {
      t_W = ( n_aCell[0]*(Y_1- y[i][y_a])- n_aCell[1]*(X_1- y[i][x_a]) ) / scalProd;
      if( t_W >=0 && t_W <= wallLength ) {
	crossPoint[0] = X_1 + N_W[0]*t_W;
	crossPoint[1] = Y_1 + N_W[1]*t_W; 
	
	if(n_aCell[0])
	  t_cell = (X_1 + N_W[0]*t_W - y[i][x_a]) / n_aCell[0];
	else if(n_aCell[1])
	  t_cell = (Y_1 + N_W[1]*t_W - y[i][y_a]) / n_aCell[1];
	else {
	  std::cerr << "WallSpringElliptic::derivs() Warning! Cell has no length!\n";
	  exit(-1);
	}
	if(t_cell >= 0 && t_cell <= 2*a_cell)
	  solutionFlag = 1;
	else 
	  solutionFlag = 0;
      }
      else
	solutionFlag = 0;
    }
    else 
      solutionFlag = 0;
    
    //if solutions are found, ellipses are updated
    if(solutionFlag) {
      //Find the distance and the direction to the COM
      double n_com[2] = { (y[i][x_a]+a_cell*n_aCell[0] - crossPoint[0]),
			  (y[i][y_a]+a_cell*n_aCell[1] - crossPoint[1]) };
      double distance = std::sqrt( n_com[0]*n_com[0] + n_com[1]*n_com[1] );
      n_com[0] /= distance;
      n_com[1] /= distance;
     
      //Find the force in the normal direction
      double F[2]; 
      if (t_cell>=0 && t_cell <= a_cell) {
	F[0] = K_W*t_cell*normal_W[0];
	F[1] = K_W*t_cell*normal_W[1];
      }
      else if(t_cell > a_cell && t_cell <= 2*a_cell) {
	F[0] = K_W*(2.0*a_cell-t_cell)*normal_W[0];
	F[1] = K_W*(2.0*a_cell-t_cell)*normal_W[1];
      }
      else {
	F[0]=F[1]=0.0;
	std::cerr << "WallSpringElliptic::derivs() Warning t outside used\n";  
      }
      /*if( F[0]*n_com[0]+F[1]*n_com[1]<0.0 ) {
	std::cerr << "WallSpringElliptic::derivs() "
		  << "wrong center of mass contribution of wall on (a).\n";
	std::cerr << "Fn_com " << F[0]*n_com[0]+F[1]*n_com[1] << "\n";
	std::cerr << "xyCross: " << crossPoint[0] << " " << crossPoint[1] << "\n";
	std::cerr << "normal: " << normal_W[0] << " " << normal_W[1] << "\n";
	std::cerr << "n_com: " << n_com[0] << " " << n_com[1] << "\n";
	std::cerr << "F: " << F[0] << " " << F[1] << "\n";
	F[0]=F[1]=0.0;
	exit(-1);
	}*/
      
      //Force on wall
      wallForce += sqrt(F[0]*F[0] + F[1]*F[1]);

      //Contributions to the COM of the cell in question
      dydt[i][x_a] += (F[0]*n_com[0] + F[1]*n_com[1])*n_com[0];
      dydt[i][y_a] += (F[0]*n_com[0] + F[1]*n_com[1])*n_com[1];
      dydt[i][x_b] += (F[0]*n_com[0] + F[1]*n_com[1])*n_com[0];
      dydt[i][y_b] += (F[0]*n_com[0] + F[1]*n_com[1])*n_com[1];
      
      //Find component of the force perpendicular to COM component
      double n_perp[2]= {n_com[1], -n_com[0]};
      if(n_perp[0]*F[0] +n_perp[1]*F[1] < 0) {
	n_perp[0] = - n_com[1];
	n_perp[1] =  n_com[0];
      }
      
      //Rotational contribution of the cell in question
      double F_perp =(F[0]*n_perp[0] + F[1]*n_perp[1]); 
      //change sign if negative rotation (thetaNeigh-dt)
      if( n_perp[0]*n_com[1]-n_perp[1]*n_com[0]<0 )
	F_perp = -F_perp;
      double deriv = a_cell*F_perp*distance / ICell;
      dydt[i][x_a] += deriv*sin(thetaCell);
      dydt[i][y_a] -= deriv*cos(thetaCell); 
      dydt[i][x_b] -= deriv*sin(thetaCell);
      dydt[i][y_b] += deriv*cos(thetaCell); 
      
    }
    
    //Divide the major-axis in numStep steps 
    //and check for overlap in the b-direction at each of these points
    int numStep = 20;
    double step = a_cell/numStep;
    for(int k = 0; k < numStep-1; k++ ) { 
      double root = (a_cell*a_cell*a_cell*a_cell + 
		     (step- a_cell)*(step- a_cell)*(c_cell*c_cell - a_cell*a_cell) -
		     a_cell*a_cell*c_cell*c_cell) / (a_cell*a_cell);
      if(root < 0) {
	std::cerr << "WallSpringElliptic::derivs() Warning! False root\n";
	exit(-1);
      }
      double limit = sqrt(root);   
      scalProd = N_W[0]*n_bCell[1] - N_W[1]*n_bCell[0];
      double crossPoint[2];
      double t_W, t_cell;
      if(scalProd) {
	t_W = ( n_bCell[0]*(Y_1- y[i][y_a]- step*n_aCell[1])- n_bCell[1]*(X_1- y[i][x_a]- step*n_aCell[0]) ) / scalProd;
	if( t_W >=0 && t_W <= wallLength ) {
	  crossPoint[0] = X_1 + N_W[0]*t_W;
	  crossPoint[1] = Y_1 + N_W[1]*t_W; 
	  
	  if(n_bCell[0])
	    t_cell = (X_1 + N_W[0]*t_W - y[i][x_a] - step*n_aCell[0]) / n_bCell[0];
	  else if(n_bCell[1])
	    t_cell = (Y_1 + N_W[1]*t_W - y[i][y_a] - step*n_aCell[1]) / n_bCell[1];
	  else {
	    std::cerr << "WallSpringElliptic::derivs() Warning! Cell has no width!\n";
	    exit(-1);
	  }
	  
	  
	  if(t_cell >= -limit && t_cell <= limit)
	    solutionFlag = 1;
	  else 
	    solutionFlag = 0;
	}
	else
	  solutionFlag = 0;
      }
      else 
	solutionFlag = 0;
      
      //if solutions are found, ellipses are updated
      if(solutionFlag) {
	//Find the distance and the direction to the COM
	double n_com[2] = { (y[i][x_a]+a_cell*n_aCell[0] - crossPoint[0]),
			    (y[i][y_a]+a_cell*n_aCell[1] - crossPoint[1]) };
	double distance = std::sqrt( n_com[0]*n_com[0] + n_com[1]*n_com[1] );
	n_com[0] /= distance;
	n_com[1] /= distance;

	//Find the force in the normal direction
	double F[2]; 
	if (t_cell >= 0 && t_cell <= limit) {
	  F[0] = K_W*(limit - t_cell)*normal_W[0];
	  F[1] = K_W*(limit - t_cell)*normal_W[1];
	}
	else if( t_cell < 0 && t_cell >= -limit ) {
	  F[0] = K_W*(limit + t_cell)*normal_W[0];
	  F[1] = K_W*(limit + t_cell)*normal_W[1];
	}
	else {
	  F[0]=F[1]=0.0;
	  std::cerr << "WallSpringElliptic::derivs() Warning t outside used\n";  
	}
	/*if( F[0]*n_com[0]+F[1]*n_com[1]<0.0 ) {
	  std::cerr << "WallSpringElliptic::derivs() "
		    << "wrong center of mass contribution of wall on i (b).\n";
	  std::cerr << "Fn_com " << F[0]*n_com[0]+F[1]*n_com[1] << "\n";
	  std::cerr << "xyCross: " << crossPoint[0] << " " << crossPoint[1] << "\n";
	  std::cerr << "normal: " << normal_W[0] << " " << normal_W[1] << "\n";
	  std::cerr << "n_com: " << n_com[0] << " " << n_com[1] << "\n";
	  std::cerr << "F: " << F[0] << " " << F[1] << "\n";
	  F[0]=F[1]=0.0;
	  exit(-1);
	  }*/
	//Force on wall
	wallForce += sqrt(F[0]*F[0] + F[1]*F[1]);
	
	//Contributions to the COM of the cell in question
	dydt[i][x_a] += (F[0]*n_com[0] + F[1]*n_com[1])*n_com[0];
	dydt[i][y_a] += (F[0]*n_com[0] + F[1]*n_com[1])*n_com[1];
	dydt[i][x_b] += (F[0]*n_com[0] + F[1]*n_com[1])*n_com[0];
	dydt[i][y_b] += (F[0]*n_com[0] + F[1]*n_com[1])*n_com[1];
	
	//Find component of the force perpendicular to COM component
	double n_perp[2]= {n_com[1], -n_com[0]};
	if(n_perp[0]*F[0] +n_perp[1]*F[1] < 0) {
	  n_perp[0] = - n_com[1];
	  n_perp[1] =  n_com[0];
	}
	
	//Rotational contribution of the cell in question
	double F_perp =(F[0]*n_perp[0] + F[1]*n_perp[1]); 
	//change sign if negative rotation (thetaNeigh-dt)
	if( n_perp[0]*n_com[1]-n_perp[1]*n_com[0]<0 )
	  F_perp = -F_perp;
	double deriv = a_cell*F_perp*distance / ICell;
	dydt[i][x_a] += deriv*sin(thetaCell);
	dydt[i][y_a] -= deriv*cos(thetaCell); 
	dydt[i][x_b] -= deriv*sin(thetaCell);
	dydt[i][y_b] += deriv*cos(thetaCell); 
      }
      step += 2*a_cell/numStep;
    }
  }
  //force contribution of the wall
  y[i][variableIndex(0,0)] = wallForce;
}

//!Constructor for the WallSpringCigar class
WallSpringCigar::WallSpringCigar(std::vector<double> &paraValue, 
				   std::vector< std::vector<size_t> > 
				   &indValue ) {
  
  //Do some checks on the parameters and variable indeces
  //////////////////////////////////////////////////////////////////////
  if( paraValue.size()!=8 ) {
    std::cerr << "WallSpringCigar::WallSpringCigar()"
	      << "Uses seven parameters K_force x_1, y_1, x_2, y_2, normal direction (+1or -1), b, and numSteps\n";
    exit(0);
  }
  if(paraValue[5] != 1 && paraValue[5] != -1) {
    std::cerr << "WallSpringCigar::WallSpringCigar()"
	      << " normal direction must be +1 or -1 (not " << paraValue[5] << ")\n";
    exit(0);
  }
  if( indValue.size() != 1) {
    std::cerr << "WallSpringCigar::WallSpringCigar()"
	      << "one variable index is used, force\n";
    exit(0);
  }
  //Set the variable values
  //////////////////////////////////////////////////////////////////////
  setId("wallSpringCigar");
  setParameter(paraValue);  
  setVariableIndex(indValue);
  
  //Set the parameter identities
  //////////////////////////////////////////////////////////////////////
  std::vector<std::string> tmp( numParameter() );
  tmp[0] = "K_force";
  tmp[1] = "x_1";
  tmp[2] = "y_1";
  tmp[3] = "x_2";
  tmp[4] = "y_2";
  tmp[5] = "direction";
  tmp[6] = "b";
  tmp[7] = "numStep";
  setParameterId( tmp );
}

//! Derivative contribution for a mechanical interaction from a wall.
void WallSpringCigar::derivs(Compartment &compartment,size_t varIndex,
			      std::vector< std::vector<double> > &y,
			      std::vector< std::vector<double> > &dydt ) {
  
  
  bool solutionFlag=0; 
  //Setting wall properties
  double K_W = parameter(0);
  double X_1 =  parameter(1), Y_1 =  parameter(2), X_2 =  parameter(3), Y_2 =  parameter(4);
  double wallLength = sqrt( (X_2 - X_1)*(X_2 - X_1) + (Y_2 - Y_1)*(Y_2 - Y_1) );
  double N_W[2] = { (X_2 - X_1)/wallLength, (Y_2 - Y_1)/wallLength } ;
  double normal_W[2];
  double wallForce = 0;
  //if parameter(5) = +1 chose right-handed direction of the normal, otherwize left-handed 
  if(parameter(5) > 0) {
    normal_W[0] = -N_W[1];
    normal_W[1] = N_W[0];
  } 
  else {
    normal_W[0] = N_W[1];
    normal_W[1] = -N_W[0];
  }
  //Setting values for the ellips in question
  double pi = 3.141592654;
  size_t i=compartment.index(), x_a=varIndex, y_a=varIndex+1, 
   x_b=varIndex+2, y_b=varIndex+3; ;
  double a_cell = 0.5*sqrt( (y[i][x_b]-y[i][x_a])*(y[i][x_b]-y[i][x_a])+
			    (y[i][y_b]-y[i][y_a])*(y[i][y_b]-y[i][y_a]) );
  double b_cell = parameter(6);
  if(a_cell < b_cell) {
    std::cerr << "WallSpringCigar::derivs() "
	      << "The major axis a = " << a_cell 
	      << " must be greater than the minor axis b = " 
	      << b_cell << "\n";
    //exit(-1); 
  } 
 
  double n_aCell[2] = { (y[i][x_b]-y[i][x_a])/(2*a_cell),
			(y[i][y_b]-y[i][y_a])/(2*a_cell) };
  double n_bCell[2] = { n_aCell[1], -n_aCell[0] };
  double ICell = 5.0*(b_cell*b_cell + a_cell*a_cell)/4;
 
  //Find out if the cell is a potential neighbor to the wall
  //if it's not nothing is updated
  double scalProd =  N_W[0]*normal_W[1] - N_W[1]*normal_W[0];
  double t_dist = ( normal_W[0]*(Y_1 - y[i][y_a] - a_cell*n_aCell[1]) -
		    normal_W[1]*(X_1 - y[i][x_a] - a_cell*n_aCell[0]) )/scalProd;
  
  double crossPoint[2] = { X_1 + N_W[0]*t_dist, Y_1 + N_W[1]*t_dist };
  double distToCOM = sqrt( (crossPoint[0] - y[i][x_a] - a_cell*n_aCell[0])*
  		  (crossPoint[0] - y[i][x_a] - a_cell*n_aCell[0]) +
  		  (crossPoint[1] - y[i][y_a] - a_cell*n_aCell[1])*
  		  (crossPoint[1] - y[i][y_a] - a_cell*n_aCell[1]) );
  //if(distToCOM <= a_cell ) {
  // std::cerr << "WallSpringCigar.derivs() A cell has passed the wall\n";
  // exit(-1);
  //}
  if(distToCOM <= a_cell + b_cell ) {
    
    //Find angle theta between n_aNeigh and the x-axis
    double thetaCell;

    double  dx = 0.5*(y[i][x_b] - y[i][x_a]);
    double  dy = 0.5*(y[i][y_b] - y[i][y_a]);
    
    if(dx == 0) {
      if(dy == 0)
	thetaCell = 0.0;
      else if(dy > 0.0)
	thetaCell = 0.5*pi;
      else //if(dy < 0)
	thetaCell = 1.5*pi;
    }
    else if(dx > 0 && dy >= 0)
      thetaCell = atan(dy/dx);
    else if(dx < 0 && dy >= 0)
      thetaCell = pi + atan(dy/dx);
    else if(dx < 0 && dy < 0)
      thetaCell = pi + atan(dy/dx);
    else //if(dx > 0 && dy < 0)
      thetaCell = 2*pi + atan(dy/dx);

    //Check if the cell interacts with the wall in the a-direction
    scalProd = N_W[0]*n_aCell[1] - N_W[1]*n_aCell[0];
    
    double t_W, t_cell;
    if(scalProd) {
      t_W = ( n_aCell[0]*(Y_1- y[i][y_a])- n_aCell[1]*(X_1- y[i][x_a]) ) / scalProd;
      if( t_W >=0 && t_W <= wallLength ) {
	crossPoint[0] = X_1 + N_W[0]*t_W;
	crossPoint[1] = Y_1 + N_W[1]*t_W; 
	
	if(std::fabs(n_aCell[0])>std::fabs(n_aCell[1]))
	  t_cell = (X_1 + N_W[0]*t_W - y[i][x_a]) / n_aCell[0];
	else 
	  t_cell = (Y_1 + N_W[1]*t_W - y[i][y_a]) / n_aCell[1];
	
	
	if(t_cell >= -b_cell && t_cell <= 2*a_cell+b_cell)
	  solutionFlag = 1;
	else 
	  solutionFlag = 0;
      }
      else
	solutionFlag = 0;
    }
    else 
      solutionFlag = 0;
    
    //if solutions are found, ellipses are updated
    if(solutionFlag) {
      //Find the distance and the direction to the COM
      double n_com[2] = { (y[i][x_a]+a_cell*n_aCell[0] - crossPoint[0]),
			  (y[i][y_a]+a_cell*n_aCell[1] - crossPoint[1]) };
      double distance = std::sqrt( n_com[0]*n_com[0] + n_com[1]*n_com[1] );
      n_com[0] /= distance;
      n_com[1] /= distance;
     
      //Find the force in the normal direction
      double F[2]; 
      if (t_cell>=-b_cell && t_cell <= a_cell) {
	F[0] = K_W*(t_cell+b_cell)*normal_W[0];
	F[1] = K_W*(t_cell+b_cell)*normal_W[1];
      }
      else if(t_cell > a_cell && t_cell <= 2*a_cell+ b_cell) {
	F[0] = K_W*(2.0*a_cell + b_cell -t_cell)*normal_W[0];
	F[1] = K_W*(2.0*a_cell + b_cell -t_cell)*normal_W[1];
      }
      else {
	F[0]=F[1]=0.0;
	std::cerr << "WallSpringCigar::derivs() Warning t outside used\n";  
      }
      /*if( F[0]*n_com[0]+F[1]*n_com[1]<0.0 ) {
	std::cerr << "WallSpringCigar::derivs() "
		  << "wrong center of mass contribution of wall on (a).\n";
	std::cerr << "Fn_com " << F[0]*n_com[0]+F[1]*n_com[1] << "\n";
	std::cerr << "xyCross: " << crossPoint[0] << " " << crossPoint[1] << "\n";
	std::cerr << "normal: " << normal_W[0] << " " << normal_W[1] << "\n";
	std::cerr << "n_com: " << n_com[0] << " " << n_com[1] << "\n";
	std::cerr << "F: " << F[0] << " " << F[1] << "\n";
	F[0]=F[1]=0.0;
	exit(-1);
	}*/
      
      //Force on wall
      wallForce += sqrt(F[0]*F[0] + F[1]*F[1]);

      //Contributions to the COM of the cell in question
      dydt[i][x_a] += (F[0]*n_com[0] + F[1]*n_com[1])*n_com[0];
      dydt[i][y_a] += (F[0]*n_com[0] + F[1]*n_com[1])*n_com[1];
      dydt[i][x_b] += (F[0]*n_com[0] + F[1]*n_com[1])*n_com[0];
      dydt[i][y_b] += (F[0]*n_com[0] + F[1]*n_com[1])*n_com[1];
      
      //Find component of the force perpendicular to COM component
      double n_perp[2]= {n_com[1], -n_com[0]};
      if(n_perp[0]*F[0] +n_perp[1]*F[1] < 0) {
	n_perp[0] = - n_com[1];
	n_perp[1] =  n_com[0];
      }
      
      //Rotational contribution of the cell in question
      double F_perp =(F[0]*n_perp[0] + F[1]*n_perp[1]); 
      //change sign if negative rotation (thetaNeigh-dt)
      if( n_perp[0]*n_com[1]-n_perp[1]*n_com[0]<0 )
	F_perp = -F_perp;
      double deriv = a_cell*F_perp*distance / ICell;
      dydt[i][x_a] += deriv*sin(thetaCell);
      dydt[i][y_a] -= deriv*cos(thetaCell); 
      dydt[i][x_b] -= deriv*sin(thetaCell);
      dydt[i][y_b] += deriv*cos(thetaCell); 
      
    }
    
    //Divide the major-axis in numStep steps 
    //and check for overlap in the b-direction at each of these points
    int numStep = static_cast<int>(parameter(7));
    double step = 0;
    for(int k = 0; k <= numStep; k++ ) { 
      
      double limit = b_cell;   
      scalProd = N_W[0]*n_bCell[1] - N_W[1]*n_bCell[0];
      double crossPoint[2];
      double t_W, t_cell;
      if(scalProd) {
	t_W = ( n_bCell[0]*(Y_1- y[i][y_a]- step*n_aCell[1])- n_bCell[1]*(X_1- y[i][x_a]- step*n_aCell[0]) ) / scalProd;
	if( t_W >=0 && t_W <= wallLength ) {
	  crossPoint[0] = X_1 + N_W[0]*t_W;
	  crossPoint[1] = Y_1 + N_W[1]*t_W; 
	  
	  if(std::fabs(n_bCell[0])>std::fabs(n_bCell[1]) )
	    t_cell = (X_1 + N_W[0]*t_W - y[i][x_a] - step*n_aCell[0]) / n_bCell[0];
	  else
	    t_cell = (Y_1 + N_W[1]*t_W - y[i][y_a] - step*n_aCell[1]) / n_bCell[1];
	  
	  
	  if(t_cell >= -limit && t_cell <= limit)
	    solutionFlag = 1;
	  else 
	    solutionFlag = 0;
	}
	else
	  solutionFlag = 0;
      }
      else 
	solutionFlag = 0;
      
      //if solutions are found, ellipses are updated
      if(solutionFlag) {
	//Find the distance and the direction to the COM
	double n_com[2] = { (y[i][x_a]+a_cell*n_aCell[0] - crossPoint[0]),
			    (y[i][y_a]+a_cell*n_aCell[1] - crossPoint[1]) };
	double distance = std::sqrt( n_com[0]*n_com[0] + n_com[1]*n_com[1] );
	n_com[0] /= distance;
	n_com[1] /= distance;

	//Find the force in the normal direction
	double F[2]; 
	if (t_cell >= 0 && t_cell <= limit) {
	  F[0] = K_W*(limit - t_cell)*normal_W[0];
	  F[1] = K_W*(limit - t_cell)*normal_W[1];
	}
	else if( t_cell < 0 && t_cell >= -limit ) {
	  F[0] = K_W*(limit + t_cell)*normal_W[0];
	  F[1] = K_W*(limit + t_cell)*normal_W[1];
	}
	else {
	  F[0]=F[1]=0.0;
	  std::cerr << "WallSpringCigar::derivs() Warning t outside used\n";  
	}
	/*if( F[0]*n_com[0]+F[1]*n_com[1]<0.0 ) {
	  std::cerr << "WallSpringCigar::derivs() "
		    << "wrong center of mass contribution of wall on i (b).\n";
	  std::cerr << "Fn_com " << F[0]*n_com[0]+F[1]*n_com[1] << "\n";
	  std::cerr << "xyCross: " << crossPoint[0] << " " << crossPoint[1] << "\n";
	  std::cerr << "normal: " << normal_W[0] << " " << normal_W[1] << "\n";
	  std::cerr << "n_com: " << n_com[0] << " " << n_com[1] << "\n";
	  std::cerr << "F: " << F[0] << " " << F[1] << "\n";
	  F[0]=F[1]=0.0;
	  exit(-1);
	  }*/
	//Force on wall
	wallForce += sqrt(F[0]*F[0] + F[1]*F[1]);
	
	//Contributions to the COM of the cell in question
	dydt[i][x_a] += (F[0]*n_com[0] + F[1]*n_com[1])*n_com[0];
	dydt[i][y_a] += (F[0]*n_com[0] + F[1]*n_com[1])*n_com[1];
	dydt[i][x_b] += (F[0]*n_com[0] + F[1]*n_com[1])*n_com[0];
	dydt[i][y_b] += (F[0]*n_com[0] + F[1]*n_com[1])*n_com[1];
	
	//Find component of the force perpendicular to COM component
	double n_perp[2]= {n_com[1], -n_com[0]};
	if(n_perp[0]*F[0] +n_perp[1]*F[1] < 0) {
	  n_perp[0] = - n_com[1];
	  n_perp[1] =  n_com[0];
	}
	
	//Rotational contribution of the cell in question
	double F_perp =(F[0]*n_perp[0] + F[1]*n_perp[1]); 
	//change sign if negative rotation (thetaNeigh-dt)
	if( n_perp[0]*n_com[1]-n_perp[1]*n_com[0]<0 )
	  F_perp = -F_perp;
	double deriv = a_cell*F_perp*distance / ICell;
	dydt[i][x_a] += deriv*sin(thetaCell);
	dydt[i][y_a] -= deriv*cos(thetaCell); 
	dydt[i][x_b] -= deriv*sin(thetaCell);
	dydt[i][y_b] += deriv*cos(thetaCell); 
      }
      step += 2*a_cell/numStep;
    }
  }
  //force contribution of the wall
  y[i][variableIndex(0,0)] = wallForce;
}

//!Constructor for the SpringElasticWallEllipseElliptic class
SpringElasticWallEllipse::
SpringElasticWallEllipse(std::vector<double> &paraValue, 
			 std::vector< std::vector<size_t> > &indValue ) {
  
  //Do some checks on the parameters and variable indeces
  //////////////////////////////////////////////////////////////////////
  if( paraValue.size() != 10 ) {
    std::cerr << "SpringElasticWallEllipse::SpringElasticWallEllipse()"
	      << "Uses seven parameters K_force x_1, y_1, x_2, y_2, "
	      << "normalDirection_wall (+1or -1), numWallSection, "
	      << "wallMass,"
	      << "and b_ellipse and numDiscretization_ellipse\n";
    exit(0);
  }
  if( paraValue[0] < 0.0 ) {
    std::cerr << "SpringElasticWallEllipse::SpringElasticWallEllipse()"
	      << "K_force must be positive, (not " 
	      << paraValue[0] << ")\n";
    exit(0);
  }
  if(paraValue[5] != 1 && paraValue[5] != -1) {
    std::cerr << "SpringElasticWallEllipse::SpringElasticWallEllipse()"
	      << " normal direction must be +1 or -1 (not " 
	      << paraValue[5] << ")\n";
    exit(0);
  }
  if(paraValue[6] < 1.0 || paraValue[6] != double(int(paraValue[6])) ) {
    std::cerr << "SpringElasticWallEllipse::SpringElasticWallEllipse()"
	      << " numWallSection (p_6) should be a positive integer, "
	      << "(not " << paraValue[5] << ")\n";
    exit(0);
  }
  if(paraValue[9] < 0.0 || paraValue[9] != double(int(paraValue[9])) ) {
    std::cerr << "SpringElasticWallEllipse::SpringElasticWallEllipse()"
	      << " numDisc_ellipse (p_9) should be a nonneg integer, "
	      << "(not " << paraValue[5] << ")\n";
    exit(0);
  }
  if( indValue.size()>1 ||
      (indValue.size() == 1 && indValue[0].size() !=1) ) {
    std::cerr << "SpringElasticWallEllipse::SpringElasticWallEllipse()"
	      << "at most one variable index is used, "
	      << "for saving force on wall\n";
    exit(0);
  }
  //Set the variable values
  //////////////////////////////////////////////////////////////////////
  setId("springElasticWallEllipse");
  setParameter(paraValue);  
  setVariableIndex(indValue);
  
  //Set the parameter identities
  //////////////////////////////////////////////////////////////////////
  std::vector<std::string> tmp( numParameter() );
  tmp[0] = "K_force";
  tmp[1] = "x_1";
  tmp[2] = "y_1";
  tmp[3] = "x_2";
  tmp[4] = "y_2";
  tmp[5] = "normDir_w";
  tmp[6] = "N_w";
  tmp[7] = "mass_w";
  tmp[8] = "b_e";
  tmp[9] = "N_e";
  setParameterId( tmp );
  
  //Initiate the wall positional variables
  //////////////////////////////////////////////////////////////////////
  //Resize vectors
  size_t Npos = static_cast<size_t>(paraValue[6])+2;  
  xW_.resize(Npos);
  dxW_.resize(Npos);
  for( size_t i=0 ; i<Npos ; i++ ) {
    xW_[i].resize(2);
    dxW_[i].resize(2);
  }
  //Get end points from parameters
  size_t Nwall=Npos-1;
  xW_[0][0] = paraValue[1];
  xW_[0][1] = paraValue[2];
  xW_[Nwall][0] = paraValue[3];
  xW_[Nwall][1] = paraValue[4];
  //Position the rest of the variables in between end points 
  //and set the relaxing distance for the springs
  double frac = 1.0/double(Nwall);
  std::vector<double> D(2);
  D[0] = xW_[Nwall][0]-xW_[0][0];
  D[1] = xW_[Nwall][1]-xW_[0][1];
  dRelax_ = std::sqrt( D[0]*D[0] + D[1]*D[1] );
  for( size_t i=1 ; i<Nwall ; i++ ) {
    xW_[i][0] = xW_[0][0] + double(i)*D[0]*frac;
    xW_[i][1] = xW_[0][1] + double(i)*D[1]*frac;
  }
  //d::cerr << "SpringElasticWallEllipse()::SpringElasticWallEllipse()\n";
  //r( size_t i=0 ; i<xW_.size() ; i++ ) {
  //d::cerr << xW_[i][0] << " " << xW_[i][1] << "\t"
  //    << dxW_[i][0] << " " << dxW_[i][1] << "\n";
  //}
}

//! Derivative contribution for a mechanical interaction from a wall.
void SpringElasticWallEllipse::
derivs(Compartment &compartment,size_t varIndex,
       std::vector< std::vector<double> > &y,
       std::vector< std::vector<double> > &dydt ) {
    
  //Set all wall derivatives to zero and add wall-wall contributions
  if( compartment.index() == 0 ) {
    for( size_t i=0 ; i<dxW_.size() ; i++ )
      for( size_t j=0 ; j<dxW_[i].size() ; j++ )
	dxW_[i][j] = 0.0;
    for( size_t wallNum=1 ; wallNum<xW_.size() ; wallNum++ ) {
      //double K_W = parameter(0);
      double X_1 =  xW_[wallNum-1][0], 
	Y_1 = xW_[wallNum-1][1], 
	X_2 = xW_[wallNum][0], 
	Y_2 = xW_[wallNum][1];
      double wallLength = sqrt( (X_2 - X_1)*(X_2 - X_1) + 
				(Y_2 - Y_1)*(Y_2 - Y_1) );
      if( wallLength<=0.0 ) {
	std::cerr << "SpringElasticWallEllipse::derivs() "
		  << " Two consecutive wall positions wrong.\n";
	exit(-1);
      }
      double coeff = parameter(0)*(1.0-wallLength/dRelax_);;
      double divX = (X_1-X_2)*coeff;
      double divY = (Y_1-Y_2)*coeff;
      if( wallNum>1 ) {
	dxW_[wallNum-1][0] -= divX;
	dxW_[wallNum-1][1] -= divY;
      }
      if( wallNum<dxW_.size()-1 ) {
	dxW_[wallNum][0] += divX;
	dxW_[wallNum][1] += divY;
      }
    }
  }
  double wallForce = 0.0;
  double pi = 3.141592654;
  size_t i=compartment.index(), x_a=varIndex, y_a=varIndex+1, 
    x_b=varIndex+2, y_b=varIndex+3; ;
  double a_cell = 0.5*sqrt( (y[i][x_b]-y[i][x_a])*(y[i][x_b]-y[i][x_a])+
			    (y[i][y_b]-y[i][y_a])*(y[i][y_b]-y[i][y_a]) );
  double b_cell = parameter(8);
  if(a_cell < b_cell) {
    std::cerr << "SpringElasticWallEllipse::derivs() "
	      << "The major axis a = " << a_cell 
	      << " must be greater than the minor axis b = " 
	      << b_cell << "\n";
    exit(-1); 
  } 
  double c_cell = sqrt(a_cell*a_cell - b_cell*b_cell);
  //Update each wall segment
  for( size_t wallNum=1 ; wallNum<xW_.size() ; wallNum++ ) {

    bool solutionFlag=0; 
    //Setting wall properties
    double K_W = parameter(0);
    double X_1 =  xW_[wallNum-1][0], 
      Y_1 = xW_[wallNum-1][1], 
      X_2 = xW_[wallNum][0], 
      Y_2 = xW_[wallNum][1];
    double wallLength = sqrt( (X_2 - X_1)*(X_2 - X_1) + (Y_2 - Y_1)*(Y_2 - Y_1) );
    double N_W[2] = { (X_2 - X_1)/wallLength, (Y_2 - Y_1)/wallLength } ;
    double normal_W[2];
    //if parameter(5) = +1 chose right-handed direction of the normal, otherwize left-handed 
    if(parameter(5) > 0) {
      normal_W[0] = -N_W[1];
      normal_W[1] = N_W[0];
    } 
    else {
      normal_W[0] = N_W[1];
      normal_W[1] = -N_W[0];
    }
    //Setting values for the ellips in question
    double n_aCell[2] = { (y[i][x_b]-y[i][x_a])/(2*a_cell),
			  (y[i][y_b]-y[i][y_a])/(2*a_cell) };
    double n_bCell[2] = { n_aCell[1], -n_aCell[0] };
    double ICell = 5.0*(b_cell*b_cell + a_cell*a_cell)/4;
    
    //Find out if the cell is a potential neighbor to the wall
    //if it's not nothing is updated
    double scalProd =  N_W[0]*normal_W[1] - N_W[1]*normal_W[0];
    double t_dist = ( normal_W[0]*(Y_1 - y[i][y_a] - a_cell*n_aCell[1]) -
		      normal_W[1]*(X_1 - y[i][x_a] - a_cell*n_aCell[0]) )/scalProd;
    
    double crossPoint[2] = { X_1 + N_W[0]*t_dist, Y_1 + N_W[1]*t_dist };
    double distToCOM = sqrt( (crossPoint[0] - y[i][x_a] - a_cell*n_aCell[0])*
			     (crossPoint[0] - y[i][x_a] - a_cell*n_aCell[0]) +
			     (crossPoint[1] - y[i][y_a] - a_cell*n_aCell[1])*
			     (crossPoint[1] - y[i][y_a] - a_cell*n_aCell[1]) );
    //if(distToCOM <= a_cell ) {
    // std::cerr << "SpringElasticWallEllipse.derivs() A cell has passed the wall\n";
    // exit(-1);
    //}
    if(distToCOM <= a_cell ) {
      
      //Find angle theta between n_aNeigh and the x-axis
      double thetaCell;
      
      double  dx = 0.5*(y[i][x_b] - y[i][x_a]);
      double  dy = 0.5*(y[i][y_b] - y[i][y_a]);
      
      if(dx == 0) {
	if(dy == 0)
	  thetaCell = 0.0;
	else if(dy > 0.0)
	  thetaCell = 0.5*pi;
	else //if(dy < 0)
	  thetaCell = 1.5*pi;
      }
      else if(dx > 0 && dy >= 0)
	thetaCell = atan(dy/dx);
      else if(dx < 0 && dy >= 0)
	thetaCell = pi + atan(dy/dx);
      else if(dx < 0 && dy < 0)
	thetaCell = pi + atan(dy/dx);
      else //if(dx > 0 && dy < 0)
	thetaCell = 2*pi + atan(dy/dx);
      
      //Check if the cell interacts with the wall in the a-direction
      scalProd = N_W[0]*n_aCell[1] - N_W[1]*n_aCell[0];
      
      double t_W, t_cell;
      if(scalProd) {
	t_W = ( n_aCell[0]*(Y_1- y[i][y_a])- n_aCell[1]*(X_1- y[i][x_a]) ) / scalProd;
	if( t_W >=0 && t_W <= wallLength ) {
	  crossPoint[0] = X_1 + N_W[0]*t_W;
	  crossPoint[1] = Y_1 + N_W[1]*t_W; 
	  
	  if(n_aCell[0])
	    t_cell = (X_1 + N_W[0]*t_W - y[i][x_a]) / n_aCell[0];
	  else if(n_aCell[1])
	    t_cell = (Y_1 + N_W[1]*t_W - y[i][y_a]) / n_aCell[1];
	  else {
	    std::cerr << "SpringElasticWallEllipse::derivs() Warning! Cell has no length!\n";
	    exit(-1);
	  }
	  if(t_cell >= 0 && t_cell <= 2*a_cell)
	    solutionFlag = 1;
	  else 
	    solutionFlag = 0;
	}
	else
	  solutionFlag = 0;
      }
      else 
	solutionFlag = 0;
      
      //if solutions are found, ellipses are updated
      if(solutionFlag) {
	//Find the distance and the direction to the COM
	double n_com[2] = { (y[i][x_a]+a_cell*n_aCell[0] - crossPoint[0]),
			    (y[i][y_a]+a_cell*n_aCell[1] - crossPoint[1]) };
	double distance = std::sqrt( n_com[0]*n_com[0] + n_com[1]*n_com[1] );
	n_com[0] /= distance;
	n_com[1] /= distance;
	
	//Find the force in the normal direction
	double F[2]; 
	if (t_cell>=0 && t_cell <= a_cell) {
	  F[0] = K_W*t_cell*normal_W[0];
	  F[1] = K_W*t_cell*normal_W[1];
	}
	else if(t_cell > a_cell && t_cell <= 2*a_cell) {
	  F[0] = K_W*(2.0*a_cell-t_cell)*normal_W[0];
	  F[1] = K_W*(2.0*a_cell-t_cell)*normal_W[1];
	}
	else {
	  F[0]=F[1]=0.0;
	  std::cerr << "SpringElasticWallEllipse::derivs() Warning t outside used\n";  
	}
	/*if( F[0]*n_com[0]+F[1]*n_com[1]<0.0 ) {
	  std::cerr << "SpringElasticWallEllipse::derivs() "
	  << "wrong center of mass contribution of wall on (a).\n";
	  std::cerr << "Fn_com " << F[0]*n_com[0]+F[1]*n_com[1] << "\n";
	  std::cerr << "xyCross: " << crossPoint[0] << " " << crossPoint[1] << "\n";
	  std::cerr << "normal: " << normal_W[0] << " " << normal_W[1] << "\n";
	  std::cerr << "n_com: " << n_com[0] << " " << n_com[1] << "\n";
	  std::cerr << "F: " << F[0] << " " << F[1] << "\n";
	  F[0]=F[1]=0.0;
	  exit(-1);
	  }*/
	
	//Force on wall
	wallForce += sqrt(F[0]*F[0] + F[1]*F[1]);
	//Add movement to wall points (but not the end points)
	if( wallNum>1 ) {
	  dxW_[wallNum-1][0] -= F[0]/parameter(7);
	  dxW_[wallNum-1][1] -= F[1]/parameter(7);
	}
	if( wallNum<(dxW_.size()-1) ) {
	  dxW_[wallNum][0] -= F[0]/parameter(7);
	  dxW_[wallNum][1] -= F[1]/parameter(7);
	}
	//Contributions to the COM of the cell in question
	dydt[i][x_a] += (F[0]*n_com[0] + F[1]*n_com[1])*n_com[0];
	dydt[i][y_a] += (F[0]*n_com[0] + F[1]*n_com[1])*n_com[1];
	dydt[i][x_b] += (F[0]*n_com[0] + F[1]*n_com[1])*n_com[0];
	dydt[i][y_b] += (F[0]*n_com[0] + F[1]*n_com[1])*n_com[1];
	
	//Find component of the force perpendicular to COM component
	double n_perp[2]= {n_com[1], -n_com[0]};
	if(n_perp[0]*F[0] +n_perp[1]*F[1] < 0) {
	  n_perp[0] = - n_com[1];
	  n_perp[1] =  n_com[0];
	}
	
	//Rotational contribution of the cell in question
	double F_perp =(F[0]*n_perp[0] + F[1]*n_perp[1]); 
	//change sign if negative rotation (thetaNeigh-dt)
	if( n_perp[0]*n_com[1]-n_perp[1]*n_com[0]<0 )
	  F_perp = -F_perp;
	double deriv = a_cell*F_perp*distance / ICell;
	dydt[i][x_a] += deriv*sin(thetaCell);
	dydt[i][y_a] -= deriv*cos(thetaCell); 
	dydt[i][x_b] -= deriv*sin(thetaCell);
	dydt[i][y_b] += deriv*cos(thetaCell);       
      }
      
      //Divide the major-axis in numStep steps 
      //and check for overlap in the b-direction at each of these points
      int numStep = int( parameter(9) );
      double step = a_cell/numStep;
      for(int k = 0; k < numStep-1; k++ ) { 
	double root = (a_cell*a_cell*a_cell*a_cell + 
		       (step- a_cell)*(step- a_cell)*(c_cell*c_cell - a_cell*a_cell) -
		       a_cell*a_cell*c_cell*c_cell) / (a_cell*a_cell);
	if(root < 0) {
	  std::cerr << "SpringElasticWallEllipse::derivs() Warning! False root\n";
	  exit(-1);
	}
	double limit = sqrt(root);   
	scalProd = N_W[0]*n_bCell[1] - N_W[1]*n_bCell[0];
	double crossPoint[2];
	double t_W, t_cell;
	if(scalProd) {
	  t_W = ( n_bCell[0]*(Y_1- y[i][y_a]- step*n_aCell[1])- n_bCell[1]*(X_1- y[i][x_a]- step*n_aCell[0]) ) / scalProd;
	  if( t_W >=0 && t_W <= wallLength ) {
	    crossPoint[0] = X_1 + N_W[0]*t_W;
	    crossPoint[1] = Y_1 + N_W[1]*t_W; 
	    
	    if(n_bCell[0])
	      t_cell = (X_1 + N_W[0]*t_W - y[i][x_a] - step*n_aCell[0]) / n_bCell[0];
	    else if(n_bCell[1])
	      t_cell = (Y_1 + N_W[1]*t_W - y[i][y_a] - step*n_aCell[1]) / n_bCell[1];
	    else {
	      std::cerr << "SpringElasticWallEllipse::derivs() Warning! Cell has no width!\n";
	      exit(-1);
	    }
	    
	    
	    if(t_cell >= -limit && t_cell <= limit)
	      solutionFlag = 1;
	    else 
	      solutionFlag = 0;
	  }
	  else
	    solutionFlag = 0;
	}
	else 
	  solutionFlag = 0;
	
	//if solutions are found, ellipses are updated
	if(solutionFlag) {
	  //Find the distance and the direction to the COM
	  double n_com[2] = { (y[i][x_a]+a_cell*n_aCell[0] - crossPoint[0]),
			      (y[i][y_a]+a_cell*n_aCell[1] - crossPoint[1]) };
	  double distance = std::sqrt( n_com[0]*n_com[0] + n_com[1]*n_com[1] );
	  n_com[0] /= distance;
	  n_com[1] /= distance;
	  
	  //Find the force in the normal direction
	  double F[2]; 
	  if (t_cell >= 0 && t_cell <= limit) {
	    F[0] = K_W*(limit - t_cell)*normal_W[0];
	    F[1] = K_W*(limit - t_cell)*normal_W[1];
	  }
	  else if( t_cell < 0 && t_cell >= -limit ) {
	    F[0] = K_W*(limit + t_cell)*normal_W[0];
	    F[1] = K_W*(limit + t_cell)*normal_W[1];
	  }
	  else {
	    F[0]=F[1]=0.0;
	    std::cerr << "SpringElasticWallEllipse::derivs() Warning t outside used\n";  
	  }
	  /*if( F[0]*n_com[0]+F[1]*n_com[1]<0.0 ) {
	    std::cerr << "SpringElasticWallEllipse::derivs() "
	    << "wrong center of mass contribution of wall on i (b).\n";
	    std::cerr << "Fn_com " << F[0]*n_com[0]+F[1]*n_com[1] << "\n";
	    std::cerr << "xyCross: " << crossPoint[0] << " " << crossPoint[1] << "\n";
	    std::cerr << "normal: " << normal_W[0] << " " << normal_W[1] << "\n";
	    std::cerr << "n_com: " << n_com[0] << " " << n_com[1] << "\n";
	    std::cerr << "F: " << F[0] << " " << F[1] << "\n";
	    F[0]=F[1]=0.0;
	    exit(-1);
	    }*/
	  //Force on wall
	  wallForce += sqrt(F[0]*F[0] + F[1]*F[1]);
	  //Add movement to wall points (but not the end points)
	  if( wallNum>1 ) {
	    dxW_[wallNum-1][0] -= F[0]/parameter(7);
	    dxW_[wallNum-1][1] -= F[1]/parameter(7);
	  }
	  if( wallNum<(dxW_.size()-1) ) {
	    dxW_[wallNum][0] -= F[0]/parameter(7);
	    dxW_[wallNum][1] -= F[1]/parameter(7);
	  }
	  
	  //Contributions to the COM of the cell in question
	  dydt[i][x_a] += (F[0]*n_com[0] + F[1]*n_com[1])*n_com[0];
	  dydt[i][y_a] += (F[0]*n_com[0] + F[1]*n_com[1])*n_com[1];
	  dydt[i][x_b] += (F[0]*n_com[0] + F[1]*n_com[1])*n_com[0];
	  dydt[i][y_b] += (F[0]*n_com[0] + F[1]*n_com[1])*n_com[1];
	  
	  //Find component of the force perpendicular to COM component
	  double n_perp[2]= {n_com[1], -n_com[0]};
	  if(n_perp[0]*F[0] +n_perp[1]*F[1] < 0) {
	    n_perp[0] = - n_com[1];
	    n_perp[1] =  n_com[0];
	  }
	  
	  //Rotational contribution of the cell in question
	  double F_perp =(F[0]*n_perp[0] + F[1]*n_perp[1]); 
	  //change sign if negative rotation (thetaNeigh-dt)
	  if( n_perp[0]*n_com[1]-n_perp[1]*n_com[0]<0 )
	    F_perp = -F_perp;
	  double deriv = a_cell*F_perp*distance / ICell;
	  dydt[i][x_a] += deriv*sin(thetaCell);
	  dydt[i][y_a] -= deriv*cos(thetaCell); 
	  dydt[i][x_b] -= deriv*sin(thetaCell);
	  dydt[i][y_b] += deriv*cos(thetaCell); 
	}
	step += 2*a_cell/numStep;
      }
    }
  } //end foreach wallpart...

  //Save force contribution to the wall if applicable
  if( numVariableIndexLevel() && numVariableIndex(0) )
    y[i][variableIndex(0,0)] = wallForce;
}

//!Updates the wall positions
void SpringElasticWallEllipse::
update(double h, double t,
			 std::vector< std::vector<double> > &y) 
{  
  size_t end = xW_.size()-1;
  if( dxW_[0][0] != 0.0 || dxW_[0][1] != 0.0 || 
      dxW_[end][0] != 0.0 || dxW_[end][1] != 0.0 ) {
    std::cerr << "SpringElasticWallEllipse::updateWall() "
	      << " Contributions to end point!\n";
    exit(-1);
  }
  for( size_t i=1 ; i<xW_.size()-1 ; i++ ) {
    xW_[i][0] = xW_[i][0] + h*dxW_[i][0];
    xW_[i][1] = xW_[i][1] + h*dxW_[i][1];
  }
  //std::cerr << "SpringElasticWallEllipse()::update()\n";
  //for( size_t i=0 ; i<xW_.size() ; i++ ) {
  //std::cerr << xW_[i][0] << " " << xW_[i][1] << "\t"
  //    << dxW_[i][0] << " " << dxW_[i][1] << "\n";
  //}
}

//!Prints the wall positions and the internal force 
void SpringElasticWallEllipse::print(std::ofstream &os) {
  
  static unsigned int count=0;
  size_t end = xW_.size()-1;
  for( size_t i=0 ; i<end ; i++ ) {
    os << count << " " << xW_[i][0] << " " << xW_[i][1] << "\t"
       << dxW_[i][0] << " " << dxW_[i][1] << "\n";
    os << count << " " << xW_[i+1][0] << " " << xW_[i+1][1] << "\t"
       << dxW_[i+1][0] << " " << dxW_[i+1][1] << "\n\n";
  }
  count++;
}

//!Constructor for the GravitationElliptic class
GravitationElliptic::GravitationElliptic(std::vector<double> &paraValue, 
	       std::vector< std::vector<size_t> > 
	       &indValue ) {
  
  //Do some checks on the parameters and variable indeces
  //////////////////////////////////////////////////////////////////////
  if( paraValue.size()!=2 ) {
    std::cerr << "GravitationElliptic::GravitationElliptic() "
	      << "Uses two parameters g_x and g_y.\n";
    exit(0);
  }
  if( indValue.size() ) { 
    std::cerr << "GravitationElliptic::GravitationElliptic() "
	      << "No variable indeces used.\n";
    exit(0);
  }
  //Set the variable values
  //////////////////////////////////////////////////////////////////////
  setId("gravitation");
  setParameter(paraValue);  
  setVariableIndex(indValue);
  
  //Set the parameter identities
  //////////////////////////////////////////////////////////////////////
  std::vector<std::string> tmp( numParameter() );
  tmp[0] = "g_x";
  tmp[1] = "g_y";
  setParameterId( tmp );
}

void GravitationElliptic::derivs(Compartment &compartment,size_t varIndex,
		    std::vector< std::vector<double> > &y,
		    std::vector< std::vector<double> > &dydt ) {
  
    size_t i=compartment.index(), x_a=varIndex, y_a=varIndex+1, 
    x_b=varIndex+2, y_b=varIndex+3; ;
 
  dydt[i][x_a] += parameter(0);
  dydt[i][x_b] += parameter(0);
  dydt[i][y_a] += parameter(1);
  dydt[i][y_b] += parameter(1);
}

	
//!Constructor for the SpringAsymmetricCigar class
SpringAsymmetricCigar::SpringAsymmetricCigar(std::vector<double> &paraValue, 
				   std::vector< std::vector<size_t> > 
				   &indValue ) {
  
  //Do some checks on the parameters and variable indeces
  //////////////////////////////////////////////////////////////////////
  if( paraValue.size()!=6 ) {
    std::cerr << "SpringAsymmetricCigar::SpringAsymmetricCigar() "
	      << "Uses five parameters K_force, b, d_overlap ,K_adh, numSteps and numAngle.\n";
    exit(0);
  }
  if( indValue.size() != 1 ) {
    std::cerr << "SpringAsymmetricCigar::SpringAsymmetricCigar() "
	      << "One variable index is used, Force.\n";
    exit(0);
  }
  //Set the variable values
  //////////////////////////////////////////////////////////////////////
  setId("springAssymetricCigar");
  setParameter(paraValue);  
  setVariableIndex(indValue);
  
  //Set the parameter identities
  //////////////////////////////////////////////////////////////////////
  std::vector<std::string> tmp( numParameter() );
  tmp[0] = "K_force";
  tmp[1] = "b";
  tmp[2] = "d_overlap";
  tmp[3] = "K_adh";
  tmp[4] = "numSteps";
  tmp[5] = "numAngle";
  setParameterId( tmp );
}

//! Derivative contribution for a mechanical interaction from a spring.
/*! Deriving the time derivative contribution from a mechanical spring
  using the values in y and add the results to dydt. The only
  difference from a normal spring is that an extra parameter defining
  the strength of the adhesion compared to the repelling spring
  constant is needed for adhesion K=K_force*K_adhFrac.
*/
void SpringAsymmetricCigar::derivs(Compartment &compartment,size_t varIndex,
			      std::vector< std::vector<double> > &y,
			      std::vector< std::vector<double> > &dydt ) {

  //static int tmpT=0;

  //Checking for correct number of topological variables
  assert( compartment.numTopologyVariable() == 5 );

  //Listing the columns used
  size_t i=compartment.index(), x_a=varIndex, y_a=varIndex+1, 
    x_b=varIndex+2, y_b=varIndex+3; 
  
  if(!i) {
    for(size_t k = 0; k<y.size(); k++)
      y[k][variableIndex(0,0)] = 0;
  }
  //Setting values for the ellipse in question
  double ForceCell = 0;
  double pi = 3.141592654;
 
  bool solutionFlag =0;
  
  double a_cell = 0.5*sqrt( (y[i][x_b]-y[i][x_a])*(y[i][x_b]-y[i][x_a])+
			    (y[i][y_b]-y[i][y_a])*(y[i][y_b]-y[i][y_a]) );
  double b_cell = parameter(1);
  //double overlap = parameter(2);
  if(a_cell < b_cell) {
    std::cerr << "NormalElliptivRepulsion::derivs() "
	      << "The major axis a = " << a_cell 
	      << " must be greater than the minor axis b = " 
	      << b_cell << "\n";
    //exit(-1); 
  } 
   double n_aCell[2] = { (y[i][x_b]-y[i][x_a])/(2*a_cell),
			(y[i][y_b]-y[i][y_a])/(2*a_cell) };
   double n_bCell[2] = { -n_aCell[1], n_aCell[0] };
  
   //double ICell = 5.0*(b_cell*b_cell + a_cell*a_cell)/4;
  //Find angle theta between n_aNeigh and the x-axis
  double thetaCell;

  double  dx = 0.5*(y[i][x_b] - y[i][x_a]);
  double  dy = 0.5*(y[i][y_b] - y[i][y_a]);
  
  if(dx == 0) {
      if(dy == 0)
	thetaCell = 0.0;
      else if(dy > 0.0)
	thetaCell = 0.5*pi;
      else //if(dy < 0)
	thetaCell = 1.5*pi;
    }
    else if(dx > 0 && dy >= 0)
      thetaCell = atan(dy/dx);
    else if(dx < 0 && dy >= 0)
      thetaCell = pi + atan(dy/dx);
    else if(dx < 0 && dy < 0)
      thetaCell = pi + atan(dy/dx);
  else //if(dx > 0 && dy < 0)
      thetaCell = 2*pi + atan(dy/dx);

  //find neigbors and update the cigar and its neighbors	       
  for( size_t n=0 ; n<compartment.numNeighbor() ; n++ ) {
    double ForceNeigh = 0;
    size_t j=compartment.neighbor(n);
    //Setting values for the neigboring ellipse
    
    double a_neigh=0.5*sqrt( (y[j][x_b]-y[j][x_a])*(y[j][x_b]-y[j][x_a])+
			     (y[j][y_b]-y[j][y_a])*(y[j][y_b]-y[j][y_a]) );
    double n_aNeigh[2] = { (y[j][x_b]-y[j][x_a])/(2*a_neigh),
			   (y[j][y_b]-y[j][y_a])/(2*a_neigh) };

    double n_bNeigh[2] = { -n_aNeigh[1], n_aNeigh[0] };
    //double INeigh = 5.0*(b_cell*b_cell + a_neigh*a_neigh)/4;
    
    //Find angle theta between n_aNeigh and the x-axis
    double thetaNeigh;
    dx = 0.5*(y[j][x_b] - y[j][x_a]);
    dy = 0.5*(y[j][y_b] - y[j][y_a]);
    if(dx == 0) {
      if(dy == 0)
	thetaNeigh = 0.0;
      else if(dy > 0.0)
	thetaNeigh = 0.5*pi;
      else //if(dy < 0)
	thetaNeigh = 1.5*pi;
    }
    else if(dx > 0 && dy >= 0)
      thetaNeigh = atan(dy/dx);
    else if(dx < 0 && dy >= 0)
      thetaNeigh = pi + atan(dy/dx);
    else if(dx < 0 && dy < 0)
      thetaNeigh = pi + atan(dy/dx);
    else //if(dx > 0 && dy < 0)
      thetaNeigh = 2*pi + atan(dy/dx);
    
    //Find where the line x_aCell + n_a*t_cell crosses the lines 
    //x_aNeigh +- n_bNeigh*b + n_aNeigh * t_neigh
    double t_cell, t_neigh, t_cell1, t_neigh1, t_cell2, t_neigh2;
    double vectProd = n_aNeigh[0]*n_aCell[1]-n_aNeigh[1]*n_aCell[0];
    double normal[2];
    if(vectProd) {
    
      t_neigh1 = ( n_aCell[0]*( y[j][y_a] + b_cell*n_bNeigh[1] - y[i][y_a] ) -
		  n_aCell[1]*( y[j][x_a] + b_cell*n_bNeigh[0] - y[i][x_a] ) )/vectProd;
      t_neigh2 = ( n_aCell[0]*( y[j][y_a] - b_cell*n_bNeigh[1] - y[i][y_a] ) -
		  n_aCell[1]*( y[j][x_a] - b_cell*n_bNeigh[0] - y[i][x_a] ) )/vectProd;
      if( std::fabs(n_aCell[0])>std::fabs(n_aCell[1]) ){
	t_cell1 = (y[j][x_a] + b_cell*n_bNeigh[0] + n_aNeigh[0]*t_neigh1 - y[i][x_a])/n_aCell[0];
      	t_cell2 = (y[j][x_a] - b_cell*n_bNeigh[0] + n_aNeigh[0]*t_neigh2 - y[i][x_a])/n_aCell[0];
      }
      else {
	t_cell1 = (y[j][y_a] + b_cell*n_bNeigh[1] + n_aNeigh[1]*t_neigh1 - y[i][y_a])/n_aCell[1];
      	t_cell2 = (y[j][y_a] - b_cell*n_bNeigh[1] + n_aNeigh[1]*t_neigh2 - y[i][y_a])/n_aCell[1];
      }
      t_cell = fabs(t_cell1-a_cell) <= fabs(t_cell2 - a_cell) ? t_cell1 : t_cell2;
      t_neigh =fabs(t_cell1-a_cell) <= fabs(t_cell2 - a_cell) ? t_neigh1 : t_neigh2;
      normal[0] = fabs(t_cell1-a_cell) <= fabs(t_cell2 - a_cell) ? n_bNeigh[0] : -n_bNeigh[0];
      normal[1] = fabs(t_cell1-a_cell) <= fabs(t_cell2 - a_cell) ? n_bNeigh[1] : -n_bNeigh[1];
      if( ( t_cell >= -b_cell && t_cell <= 2*a_cell + b_cell) &&
	  ( t_neigh >= 0 && t_neigh <= 2*a_neigh ) ) 
	solutionFlag = 1;
      else
	solutionFlag = 0;
    }
    else 
      solutionFlag = 0;

    //If crossing point is found, the cigar is updated, othewize look for solutions on the circles
    
    if(solutionFlag) {
      
      double crossPoint[2] = {y[i][x_a] + n_aCell[0]*t_cell, y[i][y_a] + n_aCell[1]*t_cell};
      updateCigaraDirection(i,j,  x_a, y_a, x_b, y_b,t_cell, a_cell, b_cell, a_neigh, 
			    n_aCell, ForceCell, thetaCell, n_aNeigh, normal, crossPoint,
			    ForceNeigh, thetaNeigh, y, dydt); 
    }
    else {
      double t1, t2, t;  
      double beta = 2*( n_aCell[0]*( y[i][x_a] - y[j][x_a]) +
			n_aCell[1]*( y[i][y_a] - y[j][y_a]) );
      
      double gamma = (y[i][x_a] - y[j][x_a])*(y[i][x_a] - y[j][x_a]) +
	(y[i][y_a] - y[j][y_a])*(y[i][y_a] - y[j][y_a])- b_cell*b_cell;
      
      double d = beta*beta/(4) - gamma;
      if(d >= 0.0) {
	t1 = -beta/(2) + sqrt(d);
	t2 = -beta/(2) - sqrt(d);
	t = fabs(t1-a_cell) <= fabs(t2-a_cell) ? t1 : t2;
	if( ( n_aNeigh[0]*(y[i][x_a] + n_aCell[0]*t -y[j][x_a]) +
	      n_aNeigh[1]*(y[i][y_a] + n_aCell[1]*t -y[j][y_a]) <= 0 ) &&
	    ( t >= -b_cell && t <= 2*a_cell + b_cell ) ) 
	  solutionFlag = 1;
	else
	  solutionFlag = 0;
      }
      else
	solutionFlag = 0;
      //if solutions are found, cigars are updated, otherwize look at the other circle
      if(solutionFlag) {
	double crossPoint[2] = {y[i][x_a] + n_aCell[0]*t, y[i][y_a] + n_aCell[1]*t};
	//Find normal direction at the crossing point in the neighbor 
	//ellipse reference frame
	double normal[2] = {crossPoint[0] - y[j][x_a], crossPoint[1] - y[j][y_a] };
	double norm = std::sqrt(normal[0]*normal[0] + normal[1]*normal[1]);
	normal[0] /= norm;
	normal[1] /= norm;
	updateCigaraDirection(i, j,  x_a, y_a, x_b, y_b,t, a_cell, b_cell, a_neigh, 
			      n_aCell, ForceCell, thetaCell, n_aNeigh, normal, crossPoint, 
			      ForceNeigh,thetaNeigh,  y, dydt );
      } else {
	
	beta = 2*( n_aCell[0]*( y[i][x_a] - y[j][x_b]) +
		   n_aCell[1]*( y[i][y_a] - y[j][y_b]) );
	
	gamma = (y[i][x_a] - y[j][x_b])*(y[i][x_a] - y[j][x_b]) +
	  (y[i][y_a] - y[j][y_b])*(y[i][y_a] - y[j][y_b])- b_cell*b_cell;
	
	
	d = beta*beta/(4) - gamma;
	if(d >= 0.0) {
	  t1 = -beta/(2) + sqrt(d);
	  t2 = -beta/(2) - sqrt(d);
	  t = fabs(t1-a_cell) <= fabs(t2-a_cell) ? t1 : t2;
	  if( ( n_aNeigh[0]*(y[i][x_a] + n_aCell[0]*t -y[j][x_a]) +
		n_aNeigh[1]*(y[i][y_a] + n_aCell[1]*t -y[j][y_a]) >= 0 ) &&
	      ( t >= -b_cell && t <= 2*a_cell + b_cell ) ) 
	    solutionFlag = 1;
	  else
	    solutionFlag = 0;
	}
	else
	  solutionFlag = 0;
	//if solutions are found, cigars are updated
	if(solutionFlag) {
	  double crossPoint[2] = {y[i][x_a] + n_aCell[0]*t, y[i][y_a] + n_aCell[1]*t};
	  //Find normal direction at the crossing point in the neighbor 
	  //ellipse reference frame
	  double normal[2] = {crossPoint[0] - y[j][x_b], crossPoint[1] - y[j][y_b] };
	  double norm = std::sqrt(normal[0]*normal[0] + normal[1]*normal[1]);
	  normal[0] /= norm;
	  normal[1] /= norm;
	  updateCigaraDirection(i, j,  x_a, y_a, x_b, y_b,t, a_cell, b_cell, a_neigh, 
				n_aCell, ForceCell, thetaCell, n_aNeigh, normal, crossPoint, 
				ForceNeigh, thetaNeigh, y, dydt );
	}
      }
    }
    
    //Divide the major-axis in numStep steps 
    //and check for overlap in the b-direction at each of these points
    int numStep = static_cast<int>(parameter(4));
    double step = 0;
    if(numStep) {
      for(int k = 0; k <= numStep; k++ ) { 
	solutionFlag = 0;
	
	//Find where the line x_aCell + n_a*step + n_bCell*t crosses the lines 
	//x_aNeigh +- n_bNeigh*b + n_aNeigh * t_neigh
	double t_cell, t_neigh, t_cell1, t_neigh1, t_cell2, t_neigh2;
	double vectProd = n_aNeigh[0]*n_bCell[1]-n_aNeigh[1]*n_bCell[0];
	double normal[2];
	if(vectProd) {
	  t_neigh1 = ( n_bCell[0]*( y[j][y_a] + b_cell*n_bNeigh[1] - 
				    y[i][y_a] - step*n_aCell[1] ) -
		       n_bCell[1]*( y[j][x_a] + b_cell*n_bNeigh[0] - 
				    y[i][x_a] - step*n_aCell[0] ) )/vectProd;
	  t_neigh2 = ( n_bCell[0]*( y[j][y_a] - b_cell*n_bNeigh[1] - 
				    y[i][y_a] - step*n_aCell[1] ) -
		       n_bCell[1]*( y[j][x_a] - b_cell*n_bNeigh[0] - 
				    y[i][x_a] - step*n_aCell[0]) )/vectProd;
	
	  if(std::fabs(n_bCell[0])>std::fabs(n_bCell[1]) ) {
	  t_cell1 = (y[j][x_a] + b_cell*n_bNeigh[0] + n_aNeigh[0]*t_neigh1 - 
		     y[i][x_a] - step*n_aCell[0] )/n_bCell[0];
	  t_cell2 = (y[j][x_a] - b_cell*n_bNeigh[0] + n_aNeigh[0]*t_neigh2- 
		     y[i][x_a] - step*n_aCell[0])/n_bCell[0];
	  
	}
	else {
	  t_cell1 = (y[j][y_a] + b_cell*n_bNeigh[1] + n_aNeigh[1]*t_neigh1 
		     - y[i][y_a] - step*n_aCell[1] )/n_bCell[1];
	  t_cell2 = (y[j][y_a] - b_cell*n_bNeigh[1] + n_aNeigh[1]*t_neigh2 
		     - y[i][y_a] - step*n_aCell[1])/n_bCell[1];
	
	}

	t_cell = fabs(t_cell1) <= fabs(t_cell2) ? t_cell1 : t_cell2;
	t_neigh = fabs(t_cell1) <= fabs(t_cell2) ? t_neigh1 : t_neigh2;
	normal[0] = fabs(t_cell1) <= fabs(t_cell2) ? n_bNeigh[0] : -n_bNeigh[0];
	normal[1] = fabs(t_cell1) <= fabs(t_cell2) ? n_bNeigh[1] : -n_bNeigh[1];
 
	if( ( t_cell>= -b_cell && t_cell <= b_cell) &&
	    ( t_neigh >= 0 && t_neigh <= 2*a_neigh ) )
	  solutionFlag = 1;
	else
	  solutionFlag = 0;
      }
      else 
	solutionFlag = 0;
	
	//std::cerr << i << " " << j << " " << step << " "
	//  << solutionFlag << " "<< t_cell << " " << t_neigh 
	//  << " " << vectProd << "\t" << n_bCell[0] << " "
	// << n_bCell[1] << "\n";

	//If crossing point is found, the cigar is updated, otherwize look for solutions on the circles
	if(solutionFlag) {
	  double crossPoint[2] = {y[i][x_a] + n_aCell[0]*step+ 
				n_bCell[0]*t_cell, 
				  y[i][y_a] + n_aCell[1]*step+ 
				  n_bCell[1]*t_cell};
	  updateCigarbDirection(i,j,  x_a, y_a, x_b, y_b,t_cell, a_cell, 
				b_cell, a_neigh,n_aCell, ForceCell, 
				thetaCell, n_aNeigh, normal, crossPoint, 
				ForceNeigh, thetaNeigh, y, dydt);
	} 
	else {
	  double t1, t2, t;  
	  double beta = 2*( n_bCell[0]*( y[i][x_a] + step*n_aCell[0] - y[j][x_a]) +
			    n_bCell[1]*( y[i][y_a] + step*n_aCell[1] - y[j][y_a]) );
	  
	  double gamma = (y[i][x_a] + step*n_aCell[0] - y[j][x_a])*(y[i][x_a] + step*n_aCell[0] - y[j][x_a]) +
	    (y[i][y_a] + step*n_aCell[1] - y[j][y_a])*(y[i][y_a] + step*n_aCell[1]- y[j][y_a])- b_cell*b_cell;
	  
	  double d = beta*beta/(4) - gamma;
	  if(d >= 0.0) {
	    t1 = -beta/(2) + sqrt(d);
	    t2 = -beta/(2) - sqrt(d);
	    t = fabs(t1-a_cell) <= fabs(t2-a_cell) ? t1 : t2;
	    if( ( n_aNeigh[0]*(y[i][x_a] + n_aCell[0]*step+n_bCell[0]*t -y[j][x_a]) +
		  n_aNeigh[1]*(y[i][y_a] + n_aCell[1]*step+n_bCell[1]*t -y[j][y_a]) <= 0 ) &&
		( t >= -b_cell && t <= b_cell ) ) 
	      solutionFlag = 1;
	    else
	      solutionFlag = 0;
	  }
	  else
	    solutionFlag = 0;
	  
	  //if solutions are found, cigars are updated, otherwize look for solution on the other circle
	  if(solutionFlag) {
	    double crossPoint[2] = {y[i][x_a] + n_aCell[0]*step + n_bCell[0]*t, 
				    y[i][y_a] + n_aCell[1]*step + n_bCell[1]*t};
	    //Find normal direction at the crossing point in the neighbor 
	    //ellipse reference frame
	    double normal[2] = {crossPoint[0] - y[j][x_a], crossPoint[1] - y[j][y_a] };
	    double norm = std::sqrt(normal[0]*normal[0] + normal[1]*normal[1]);
	    normal[0] /= norm;
	    normal[1] /= norm;
	    updateCigarbDirection(i, j,  x_a, y_a, x_b, y_b, t, a_cell, b_cell, a_neigh, 
				  n_aCell, ForceCell, thetaCell, n_aNeigh,normal, crossPoint, 
				  ForceNeigh, thetaNeigh, y, dydt );
	  }
	  else {
	  
	    beta = 2*( n_bCell[0]*( y[i][x_a] + step*n_aCell[0] - y[j][x_b]) +
		       n_bCell[1]*( y[i][y_a] + step*n_aCell[1] - y[j][y_b]) );
	    
	    gamma = (y[i][x_a]+ step*n_aCell[0] - y[j][x_b])*(y[i][x_a]+ step*n_aCell[0] - y[j][x_b]) +
	      (y[i][y_a]+ step*n_aCell[1] - y[j][y_b])*(y[i][y_a]+ step*n_aCell[1] - y[j][y_b])- b_cell*b_cell;
	    
	    
	    d = beta*beta/(4) - gamma;
	    if(d >= 0.0) {
	      t1 = -beta/(2) + sqrt(d);
	      t2 = -beta/(2) - sqrt(d);
	      t = fabs(t1-a_cell) <= fabs(t2-a_cell) ? t1 : t2;
	      if( ( n_aNeigh[0]*(y[i][x_a] + n_aCell[0]*step+n_bCell[0]*t -y[j][x_b]) +
		    n_aNeigh[1]*(y[i][y_a] + n_aCell[1]*step+n_bCell[1]*t -y[j][y_b]) >= 0 ) &&
		  ( t >= -b_cell && t <= b_cell ) ) 
		solutionFlag = 1;
	      else
		solutionFlag = 0;
	    }
	    else
	      solutionFlag = 0;
	    //if solutions are found, cigars are updated
	    if(solutionFlag) {
	      double crossPoint[2] = {y[i][x_a] + n_aCell[0]*step + n_bCell[0]*t, 
				      y[i][y_a] + n_aCell[1]*step + n_bCell[1]*t};
	      //Find normal direction at the crossing point in the neighbor 
	      //ellipse reference frame
	      double normal[2] = {crossPoint[0] - y[j][x_b], crossPoint[1] - y[j][y_b] };
	      double norm = std::sqrt(normal[0]*normal[0] + normal[1]*normal[1]);
	      normal[0] /= norm;
	      normal[1] /= norm;
	      updateCigarbDirection(i, j, x_a, y_a, x_b, y_b, t, a_cell, b_cell, a_neigh, 
				    n_aCell, ForceCell,thetaCell, n_aNeigh, normal, crossPoint, 
				    ForceNeigh,thetaNeigh, y, dydt );
			       }
	    
	  }
	
	}
	step += 2.0*a_cell/double(numStep);
      }
    }



    //Divide the circular parts in numAngle directions and check for overlap in these directions
    int numAngle = static_cast<int>(parameter(5));
    double angle = pi/(numAngle+1);
    if(numAngle) {
      for(int k = 0; k < numAngle; k++ ) { 

	solutionFlag = 0;
	//rotate n_b
	double n_bRot[2] = {cos(angle)*n_bCell[0] - sin(angle)*n_bCell[1],
			    sin(angle)*n_bCell[0] + cos(angle)*n_bCell[1]};
	
	//Find where the line x_aCell + n_bRot*t crosses the lines 
	//x_aNeigh +- n_bNeigh*b + n_aNeigh * t_neigh
	double t_cell, t_neigh, t_cell1, t_neigh1, t_cell2, t_neigh2;
	double vectProd = n_aNeigh[0]*n_bRot[1]-n_aNeigh[1]*n_bRot[0];
	double normal[2];
	if(vectProd) {
	  t_neigh1 = ( n_bRot[0]*( y[j][y_a] + b_cell*n_bNeigh[1] - 
				    y[i][y_a]) -
		       n_bRot[1]*( y[j][x_a] + b_cell*n_bNeigh[0] - 
				    y[i][x_a]) )/vectProd;
	  t_neigh2 = ( n_bRot[0]*( y[j][y_a] - b_cell*n_bNeigh[1] - 
				    y[i][y_a]) -
		       n_bRot[1]*( y[j][x_a] - b_cell*n_bNeigh[0] - 
				   y[i][x_a]) )/vectProd;
	  
	  if(std::fabs(n_bRot[0])>std::fabs(n_bRot[1]) ) {
	  t_cell1 = (y[j][x_a] + b_cell*n_bNeigh[0] + n_aNeigh[0]*t_neigh1 - 
		     y[i][x_a])/n_bRot[0];
	  t_cell2 = (y[j][x_a] - b_cell*n_bNeigh[0] + n_aNeigh[0]*t_neigh2- 
		     y[i][x_a])/n_bRot[0];
	  
	  }
	  else {
	    t_cell1 = (y[j][y_a] + b_cell*n_bNeigh[1] + n_aNeigh[1]*t_neigh1 
		       - y[i][y_a])/n_bRot[1];
	    t_cell2 = (y[j][y_a] - b_cell*n_bNeigh[1] + n_aNeigh[1]*t_neigh2 
		       - y[i][y_a])/n_bRot[1];
	  }
	  
	  t_cell = fabs(t_cell1) <= fabs(t_cell2) ? t_cell1 : t_cell2;
	  t_neigh = fabs(t_cell1) <= fabs(t_cell2) ? t_neigh1 : t_neigh2;
	  normal[0] = fabs(t_cell1) <= fabs(t_cell2) ? n_bNeigh[0]: -n_bNeigh[0];
	  normal[1] = fabs(t_cell1) <= fabs(t_cell2) ? n_bNeigh[1]: -n_bNeigh[1];
 
	if( ( t_cell>= 0 && t_cell <= b_cell) &&
	    ( t_neigh >= 0 && t_neigh <= 2*a_neigh ) )
	  solutionFlag = 1;
	else
	  solutionFlag = 0;
	}
	else 
	  solutionFlag = 0;
	
	//std::cerr << i << " " << j << " " << step << " "
	//  << solutionFlag << " "<< t_cell << " " << t_neigh 
	//  << " " << vectProd << "\t" << n_bCell[0] << " "
	// << n_bCell[1] << "\n";

	//If crossing point is found, the cigar is updated, otherwise look for solutions on the circles
	if(solutionFlag) {
	 
	  double crossPoint[2] = {y[i][x_a] +	n_bRot[0]*t_cell, 
				  y[i][y_a] +   n_bRot[1]*t_cell};
	  updateCigarbDirection(i,j,  x_a, y_a, x_b, y_b,t_cell, a_cell, 
				b_cell, a_neigh,n_aCell, ForceCell, 
				thetaCell, n_aNeigh, normal, crossPoint, 
				ForceNeigh, thetaNeigh, y, dydt);
	} 
	else {
	  double t1, t2, t;  
	  double beta = 2*( n_bRot[0]*( y[i][x_a] - y[j][x_a]) +
			    n_bRot[1]*( y[i][y_a] - y[j][y_a]) );
	  
	  double gamma = (y[i][x_a] - y[j][x_a])*(y[i][x_a] - y[j][x_a]) +
	    (y[i][y_a] - y[j][y_a])*(y[i][y_a] -  y[j][y_a])- b_cell*b_cell;
	  
	  double d = beta*beta/(4) - gamma;
	  if(d >= 0.0) {
	    t1 = -beta/(2) + sqrt(d);
	    t2 = -beta/(2) - sqrt(d);
	    t = fabs(t1) <= fabs(t2) ? t1 : t2;
	    if( ( n_aNeigh[0]*(y[i][x_a] + n_bRot[0]*t -y[j][x_a]) +
		  n_aNeigh[1]*(y[i][y_a] + n_bRot[1]*t -y[j][y_a]) <= 0 ) &&
		( t >= 0 && t <= b_cell ) ) 
	      solutionFlag = 1;
	    else
	      solutionFlag = 0;
	  }
	  else
	    solutionFlag = 0;
	  
	  //if solutions are found, cigars are updated, otherwise look for solution on the other circle
	  if(solutionFlag) {
	    double crossPoint[2] = {y[i][x_a] + n_aCell[0]*step + n_bCell[0]*t, 
				    y[i][y_a] + n_aCell[1]*step + n_bCell[1]*t};
	    //Find normal direction at the crossing point in the neighbor 
	    //ellipse reference frame
	    double normal[2] = {crossPoint[0] - y[j][x_a], crossPoint[1] - y[j][y_a] };
	    double norm = std::sqrt(normal[0]*normal[0] + normal[1]*normal[1]);
	    normal[0] /= norm;
	    normal[1] /= norm;
	    updateCigarbDirection(i, j,  x_a, y_a, x_b, y_b, t, a_cell, b_cell, a_neigh, 
				  n_aCell, ForceCell, thetaCell, n_aNeigh,normal, crossPoint, 
				  ForceNeigh, thetaNeigh, y, dydt );
	  }
	  else { 
	    beta = 2*( n_bRot[0]*( y[i][x_a] - y[j][x_b]) +
		       n_bRot[1]*( y[i][y_a] - y[j][y_b]) );
	    
	    gamma = (y[i][x_a] - y[j][x_b])*(y[i][x_a]- y[j][x_b]) +
	      (y[i][y_a] - y[j][y_b])*(y[i][y_a]- y[j][y_b])- b_cell*b_cell;
	    
	    
	    d = beta*beta/(4) - gamma;
	    if(d >= 0.0) {
	      t1 = -beta/(2) + sqrt(d);
	      t2 = -beta/(2) - sqrt(d);
	      t = fabs(t1) <= fabs(t2) ? t1 : t2;
	      if( ( n_aNeigh[0]*(y[i][x_a] + n_bRot[0]*t -y[j][x_b]) +
		    n_aNeigh[1]*(y[i][y_a] + n_bRot[1]*t -y[j][y_b]) >= 0 ) &&
		  ( t >= 0 && t <= b_cell ) ) 
		solutionFlag = 1;
	    else
	      solutionFlag = 0;
	  }
	  else
	    solutionFlag = 0;
	  //if solutions are found, cigars are updated
	  if(solutionFlag) {
	    double crossPoint[2] = {y[i][x_a] + n_bRot[0]*t, 
				    y[i][y_a] + n_bRot[1]*t};
	    //Find normal direction at the crossing point in the neighbor 
	    //ellipse reference frame
	    double normal[2] = {crossPoint[0] - y[j][x_b], crossPoint[1] - y[j][y_b] };
	    double norm = std::sqrt(normal[0]*normal[0] + normal[1]*normal[1]);
	    normal[0] /= norm;
	    normal[1] /= norm;
	    updateCigarbDirection(i, j, x_a, y_a, x_b, y_b, t, a_cell, b_cell, a_neigh, 
				  n_aCell, ForceCell,thetaCell, n_aNeigh, normal, crossPoint, 
				  ForceNeigh,thetaNeigh, y, dydt );
	  }
	  
	  }
	}
	if(!solutionFlag) {
	  //rotate n_b
	  double n_bRot[2] = {cos(angle)*n_bCell[0] + sin(angle)*n_bCell[1],
			      -sin(angle)*n_bCell[0] + cos(angle)*n_bCell[1]};
	 
	  //Find where the line x_bCell + n_bRot*t crosses the lines 
	  //x_aNeigh +- n_bNeigh*b + n_aNeigh * t_neigh
	  double t_cell, t_neigh, t_cell1, t_neigh1, t_cell2, t_neigh2;
	  double vectProd = n_aNeigh[0]*n_bRot[1]-n_aNeigh[1]*n_bRot[0];
	  double normal[2];
	  if(vectProd) {
	    t_neigh1 = ( n_bRot[0]*( y[j][y_a] + b_cell*n_bNeigh[1] - 
				     y[i][y_b]) -
			 n_bRot[1]*( y[j][x_a] + b_cell*n_bNeigh[0] - 
				     y[i][x_b]) )/vectProd;
	    t_neigh2 = ( n_bRot[0]*( y[j][y_a] - b_cell*n_bNeigh[1] - 
				     y[i][y_b]) -
			 n_bRot[1]*( y[j][x_a] - b_cell*n_bNeigh[0] - 
				     y[i][x_b]) )/vectProd;
	  
	  if(std::fabs(n_bRot[0])>std::fabs(n_bRot[1]) ) {
	  t_cell1 = (y[j][x_a] + b_cell*n_bNeigh[0] + n_aNeigh[0]*t_neigh1 - 
		     y[i][x_b])/n_bRot[0];
	  t_cell2 = (y[j][x_a] - b_cell*n_bNeigh[0] + n_aNeigh[0]*t_neigh2- 
		     y[i][x_b])/n_bRot[0];
	  
	  }
	  else {
	    t_cell1 = (y[j][y_a] + b_cell*n_bNeigh[1] + n_aNeigh[1]*t_neigh1 
		       - y[i][y_b])/n_bRot[1];
	    t_cell2 = (y[j][y_a] - b_cell*n_bNeigh[1] + n_aNeigh[1]*t_neigh2 
		       - y[i][y_b])/n_bRot[1];
	  }
	  
	  t_cell = fabs(t_cell1) <= fabs(t_cell2) ? t_cell1 : t_cell2;
	  t_neigh = fabs(t_cell1) <= fabs(t_cell2) ? t_neigh1 : t_neigh2;
	  normal[0] = fabs(t_cell1) <= fabs(t_cell2) ? n_bNeigh[0]: -n_bNeigh[0];
	  normal[1] = fabs(t_cell1) <= fabs(t_cell2) ? n_bNeigh[1]: -n_bNeigh[1];
 
	if( ( t_cell>= 0 && t_cell <= b_cell) &&
	    ( t_neigh >= 0 && t_neigh <= 2*a_neigh ) )
	  solutionFlag = 1;
	else
	  solutionFlag = 0;
      }
      else 
	solutionFlag = 0;
	
	//std::cerr << i << " " << j << " " << step << " "
	//  << solutionFlag << " "<< t_cell << " " << t_neigh 
	//  << " " << vectProd << "\t" << n_bCell[0] << " "
	// << n_bCell[1] << "\n";

	//If crossing point is found, the cigar is updated, otherwise look for solutions on the circles
	if(solutionFlag) {
	  double crossPoint[2] = {y[i][x_a] +	n_bRot[0]*t_cell, 
				  y[i][y_a] +   n_bRot[1]*t_cell};
	  updateCigarbDirection(i,j,  x_a, y_a, x_b, y_b,t_cell, a_cell, 
				b_cell, a_neigh,n_aCell, ForceCell, 
				thetaCell, n_aNeigh, normal, crossPoint, 
				ForceNeigh, thetaNeigh, y, dydt);
	} 
	else {
	  
	  double t1, t2, t;  
	  double beta = 2*( n_bRot[0]*( y[i][x_b] - y[j][x_a]) +
			    n_bRot[1]*( y[i][y_b] - y[j][y_a]) );
	  
	  double gamma = (y[i][x_b] - y[j][x_a])*(y[i][x_b] - y[j][x_a]) +
	  (y[i][y_b] - y[j][y_a])*(y[i][y_b] -  y[j][y_a])- b_cell*b_cell;
	  
	  double d = beta*beta/(4) - gamma;
	  if(d >= 0.0) {
	    t1 = -beta/(2) + sqrt(d);
	    t2 = -beta/(2) - sqrt(d);
	    t = fabs(t1) <= fabs(t2) ? t1 : t2;
	    if( ( n_aNeigh[0]*(y[i][x_b] + n_bRot[0]*t -y[j][x_a]) +
		  n_aNeigh[1]*(y[i][y_b] + n_bRot[1]*t -y[j][y_a]) <= 0 ) &&
		( t >= 0 && t <= b_cell ) ) 
	      solutionFlag = 1;
	    else
	      solutionFlag = 0;
	  }
	  else
	    solutionFlag = 0;
	  
	  //if solutions are found, cigars are updated, otherwise look for solution on the other circle
	  if(solutionFlag) {
	    double crossPoint[2] = {y[i][x_b] + n_aCell[0]*step + n_bCell[0]*t, 
				    y[i][y_b] + n_aCell[1]*step + n_bCell[1]*t};
	    //Find normal direction at the crossing point in the neighbor 
	    //ellipse reference frame
	    double normal[2] = {crossPoint[0] - y[j][x_a], crossPoint[1] - y[j][y_a] };
	    double norm = std::sqrt(normal[0]*normal[0] + normal[1]*normal[1]);
	    normal[0] /= norm;
	    normal[1] /= norm;
	    updateCigarbDirection(i, j,  x_a, y_a, x_b, y_b, t, a_cell, b_cell, a_neigh, 
				  n_aCell, ForceCell, thetaCell, n_aNeigh,normal, crossPoint, 
				  ForceNeigh, thetaNeigh, y, dydt );
	  }
	  else { 
	    beta = 2*( n_bRot[0]*( y[i][x_b] - y[j][x_b]) +
		       n_bRot[1]*( y[i][y_b] - y[j][y_b]) );
	    
	    gamma = (y[i][x_b] - y[j][x_b])*(y[i][x_b]- y[j][x_b]) +
	      (y[i][y_b]- y[j][y_b])*(y[i][y_b]- y[j][y_b])- b_cell*b_cell;
	    
	    
	    d = beta*beta/(4) - gamma;
	    if(d >= 0.0) {
	      t1 = -beta/(2) + sqrt(d);
	      t2 = -beta/(2) - sqrt(d);
	      t = fabs(t1) <= fabs(t2) ? t1 : t2;
	      if( ( n_aNeigh[0]*(y[i][x_b] + n_bRot[0]*t -y[j][x_b]) +
		    n_aNeigh[1]*(y[i][y_b] + n_bRot[1]*t -y[j][y_b]) >= 0 ) &&
		  ( t >= 0 && t <= b_cell ) ) 
		solutionFlag = 1;
	    else
	      solutionFlag = 0;
	  }
	  else
	    solutionFlag = 0;
	  //if solutions are found, cigars are updated
	  if(solutionFlag) {
	    double crossPoint[2] = {y[i][x_b] + n_bRot[0]*t, 
				    y[i][y_b] + n_bRot[1]*t};
	    //Find normal direction at the crossing point in the neighbor 
	    //ellipse reference frame
	    double normal[2] = {crossPoint[0] - y[j][x_b], crossPoint[1] - y[j][y_b] };
	    double norm = std::sqrt(normal[0]*normal[0] + normal[1]*normal[1]);
	    normal[0] /= norm;
	    normal[1] /= norm;
	    updateCigarbDirection(i, j, x_a, y_a, x_b, y_b, t, a_cell, b_cell, a_neigh, 
				  n_aCell, ForceCell,thetaCell, n_aNeigh, normal, crossPoint, 
				  ForceNeigh,thetaNeigh, y, dydt );
	  }
	  }
	}
	}
	angle += pi/(static_cast<double>(numAngle+1) );
      }
    }




    y[j][variableIndex(0,0)] += ForceNeigh;
  }
  y[i][variableIndex(0,0)] += ForceCell;
}

    
void SpringAsymmetricCigar::
updateCigaraDirection(size_t i, size_t j, size_t x_a, size_t y_a, size_t x_b, 
		      size_t y_b, 
		      double t, double a_cell, double b_cell, double a_neigh,
		      const double n_aCell[], 
		      double& ForceCell, double thetaCell, 
		      const double n_aNeigh[], 
		      const double normal[], const double crossPoint[],	
		      double& ForceNeigh, double thetaNeigh,
		      std::vector< std::vector<double> > &y,
		      std::vector< std::vector<double> > &dydt) {

  double ICell = 4.0*(b_cell*b_cell + a_cell*a_cell)/4;
  double INeigh = 4.0*(b_cell*b_cell + a_neigh*a_neigh)/4;

  double overlap = parameter(2); 
 
  //Find crossing point
  
  //Find the force in the normal direction 
  double F[2];
  double distance;
  if (t <= a_cell) {
    distance = a_cell - t;
    if(t+b_cell >= overlap) {
      F[0] = parameter(0)*(t + b_cell - overlap)*normal[0];
      F[1] = parameter(0)*(t + b_cell - overlap)*normal[1];
      ForceCell += sqrt( F[0]*F[0] + F[1]*F[1] );
      ForceNeigh += sqrt( F[0]*F[0] + F[1]*F[1] );
    }
    else {
      F[0] = parameter(0)*parameter(3)*(t + b_cell - overlap)*normal[0];
      F[1] = parameter(0)*parameter(3)*(t + b_cell - overlap)*normal[1];
    }
  }
  else {
    distance = t - a_cell;
    if(t  <= 2*a_cell + b_cell - overlap ) {
      F[0] = parameter(0)*(2.0*a_cell + b_cell - overlap - t)*normal[0];
      F[1] = parameter(0)*(2.0*a_cell + b_cell - overlap - t)*normal[1];
      ForceCell += sqrt( F[0]*F[0] + F[1]*F[1] );
      ForceNeigh += sqrt( F[0]*F[0] + F[1]*F[1] );
    }
    else {
      F[0] = parameter(0)*parameter(3)*(2.0*a_cell + b_cell - overlap -t)*normal[0];
      F[1] = parameter(0)*parameter(3)*(2.0*a_cell + b_cell - overlap -t)*normal[1];
    }
  }
  
  double n_com[2] = {(y[i][x_a]+a_cell*n_aCell[0] - crossPoint[0]),
		     (y[i][y_a]+a_cell*n_aCell[1] - crossPoint[1]) };
  double norm = std::sqrt( n_com[0]*n_com[0]+n_com[1]*n_com[1] );
  n_com[0] /= norm;
  n_com[1] /= norm;
  if( F[0]*n_com[0]+F[1]*n_com[1]<0.0 ) {
    F[0]=F[1]=0.0;
    
  }
  
  //Contributions to the COM of the cell in question
  dydt[i][x_a] += (F[0]*n_com[0] + F[1]*n_com[1])*n_com[0];
  dydt[i][y_a] += (F[0]*n_com[0] + F[1]*n_com[1])*n_com[1];
  dydt[i][x_b] += (F[0]*n_com[0] + F[1]*n_com[1])*n_com[0];
  dydt[i][y_b] += (F[0]*n_com[0] + F[1]*n_com[1])*n_com[1];
    
  //Find component of the force perpendicular to COM component
  double n_perp[2]= {n_com[1], -n_com[0]};
  if(n_perp[0]*F[0] +n_perp[1]*F[1] < 0) {
    n_perp[0] = - n_com[1];
    n_perp[1] =  n_com[0];
  }

  //Rotational contribution of the cell in question
  double F_perp =(F[0]*n_perp[0] + F[1]*n_perp[1]); 
  //change sign if negative rotation (thetaNeigh-dt)
  if( n_perp[0]*n_com[1]-n_perp[1]*n_com[0]<0 )
    F_perp = -F_perp;
  double deriv = a_cell*F_perp*norm / ICell;
  dydt[i][x_a] += deriv*sin(thetaCell);
  dydt[i][y_a] -= deriv*cos(thetaCell); 
  dydt[i][x_b] -= deriv*sin(thetaCell);
  dydt[i][y_b] += deriv*cos(thetaCell); 
  
  //Contributions to the neighboring cell      
  //find the direction to the center of mass of the neighboring cell
  double dist = sqrt( ( (y[j][x_a]+a_neigh*n_aNeigh[0] - crossPoint[0])*
			(y[j][x_a]+a_neigh*n_aNeigh[0] - crossPoint[0]) )+
		      ( (y[j][y_a]+a_neigh*n_aNeigh[1] - crossPoint[1])*
			(y[j][y_a]+a_neigh*n_aNeigh[1] - crossPoint[1]) ) );
  n_com[0] = (y[j][x_a]+a_neigh*n_aNeigh[0] - crossPoint[0])/dist;
  n_com[1] = (y[j][y_a]+a_neigh*n_aNeigh[1] - crossPoint[1])/dist;
  
  //Contributions from the COM-component of the force
  dydt[j][x_a] -= (F[0]*n_com[0] + F[1]*n_com[1])*n_com[0];
  dydt[j][y_a] -= (F[0]*n_com[0] + F[1]*n_com[1])*n_com[1];
  dydt[j][x_b] -= (F[0]*n_com[0] + F[1]*n_com[1])*n_com[0];
  dydt[j][y_b] -= (F[0]*n_com[0] + F[1]*n_com[1])*n_com[1];
  
  //Find component of the force perpendicular to COM component
  n_perp[0]= n_com[1];
  n_perp[1]= -n_com[0];
  if(n_perp[0]*F[0] +n_perp[1]*F[1] > 0) {
    n_perp[0] = - n_com[1];
    n_perp[1] =  n_com[0];
  }

  //Rotational contibutions 
  F_perp =-(F[0]*n_perp[0] + F[1]*n_perp[1]); 
  //change sign if negative rotation (thetaNeigh-dt)
  if( n_perp[0]*n_com[1]-n_perp[1]*n_com[0]<0 )
    F_perp = -F_perp;
  deriv = a_neigh*F_perp*dist / INeigh;
  dydt[j][x_a] += deriv*sin(thetaNeigh);
  dydt[j][y_a] -= deriv*cos(thetaNeigh); 
  dydt[j][x_b] -= deriv*sin(thetaNeigh);
  dydt[j][y_b] += deriv*cos(thetaNeigh); 

}



void SpringAsymmetricCigar::updateCigarbDirection(size_t i, size_t j, size_t x_a, size_t y_a, size_t x_b, size_t y_b, 
						  double t, double a_cell, double b_cell, double a_neigh,
						  const double n_aCell[], 
						  double& ForceCell, double thetaCell ,
						  const double n_aNeigh[], 
						  const double normal[], const double crossPoint[],
						  double& ForceNeigh,double thetaNeigh,
						  std::vector< std::vector<double> > &y,
						  std::vector< std::vector<double> > &dydt) {
  double overlap = parameter(2); 
  double ICell = 4.0*(b_cell*b_cell + a_cell*a_cell)*0.25;
  double INeigh = 4.0*(b_cell*b_cell + a_neigh*a_neigh)*0.25;
  //Find the force in the normal direction
  double F[2]; 
  double distance = fabs(t);
  if (t >= 0 && t <= b_cell ) {
    if(t <= b_cell - overlap) {
      F[0] = parameter(0)*(b_cell - overlap - t)*normal[0];
      F[1] = parameter(0)*(b_cell - overlap - t)*normal[1];
      ForceCell += sqrt(F[0]*F[0] + F[1]*F[1] );
      ForceNeigh += sqrt(F[0]*F[0] + F[1]*F[1] );
    }
    else {
      F[0] = parameter(0)*parameter(3)*(b_cell - overlap - t)*normal[0];
      F[1] = parameter(0)*parameter(3)*(b_cell - overlap - t)*normal[1];
    }
  }
  else if( t<0 && t>= -b_cell  ) {
    if(t >= -b_cell + overlap) {
      F[0] = parameter(0)*(b_cell - overlap + t)*normal[0];
      F[1] = parameter(0)*(b_cell - overlap + t)*normal[1];
      ForceCell += sqrt(F[0]*F[0] + F[1]*F[1] );
      ForceNeigh += sqrt(F[0]*F[0] + F[1]*F[1] );
    }
    else {
      F[0] = parameter(0)*parameter(3)*(b_cell - overlap + t )*normal[0];
      F[1] = parameter(0)*parameter(3)*(b_cell - overlap + t )*normal[1];
    }
  }
  else {
    F[0]=F[1]=0.0;
    std::cerr << "springAssymetricCigar.updateCigarbDirection Warning t outside used\n";  
  }
  double n_com[2] = { (y[i][x_a]+a_cell*n_aCell[0] - crossPoint[0]),
		      (y[i][y_a]+a_cell*n_aCell[1] - crossPoint[1]) };
  double norm = std::sqrt( n_com[0]*n_com[0] + n_com[1]*n_com[1] );
  n_com[0] /= norm;
  n_com[1] /= norm;
  
  //if( F[0]*n_com[0]+F[1]*n_com[1]<0.0 ) {
  //F[0]=F[1]=0.0;    
  //}
  static unsigned int counter=0;
  //std::cerr << "SpringAsymmetricCigar::updateCigarbDirection\n";
  //std::cerr << counter << " " << crossPoint[0] << " " << crossPoint[1] 
  //    << "\t" << normal[0] << " " << normal[1] << "\t"
  //    << F[0] << " " << F[1] << " " 
  //    << std::sqrt(F[0]*F[0]+F[1]*F[1]) << "\t"
  //    << parameter(0) << " " << b_cell << " " << overlap << " "
  //    << t << "\t" << a_cell << " " << a_neigh << "\n";
  counter++;
  
  //Contributions to the COM of the cell in question
  dydt[i][x_a] += (F[0]*n_com[0] + F[1]*n_com[1])*n_com[0];
  dydt[i][y_a] += (F[0]*n_com[0] + F[1]*n_com[1])*n_com[1];
  dydt[i][x_b] += (F[0]*n_com[0] + F[1]*n_com[1])*n_com[0];
  dydt[i][y_b] += (F[0]*n_com[0] + F[1]*n_com[1])*n_com[1];
  
  //Find component of the force perpendicular to COM component
  double n_perp[2]= {n_com[1], -n_com[0]};
  if(n_perp[0]*F[0] +n_perp[1]*F[1] > 0) {
    n_perp[0] = - n_com[1];
    n_perp[1] =  n_com[0];
  }

  //Rotational contribution of the cell in question
  double F_perp =(F[0]*n_perp[0] + F[1]*n_perp[1]); 
  //change sign if negative rotation (thetaNeigh-dt)
  if( n_perp[0]*n_com[1]-n_perp[1]*n_com[0]<0 )
    F_perp = -F_perp;
  double deriv = a_cell*F_perp*distance / ICell;
  dydt[i][x_a] += deriv*sin(thetaCell);
  dydt[i][y_a] -= deriv*cos(thetaCell); 
  dydt[i][x_b] -= deriv*sin(thetaCell);
  dydt[i][y_b] += deriv*cos(thetaCell); 
  
  //Contributions to the neighboring cell      
  //Contributions to the neighboring cell
  //find the direction to the center of mass of the neighboring cell
  double dist = sqrt( 
		     ( (y[j][x_a] + a_neigh*n_aNeigh[0] - crossPoint[0])*
		       (y[j][x_a] + a_neigh*n_aNeigh[0] - crossPoint[0]) ) +
		     ( (y[j][y_a] + a_neigh*n_aNeigh[1] - crossPoint[1])*
		       (y[j][y_a] + a_neigh*n_aNeigh[1] - crossPoint[1]) ) );
  
  n_com[0] = (y[j][x_a] + a_neigh*n_aNeigh[0] - crossPoint[0])/dist;
  n_com[1] = (y[j][y_a] + a_neigh*n_aNeigh[1] - crossPoint[1])/dist;
  
  if( F[0]*n_com[0]+F[1]*n_com[1]>0.0 ) {
  F[0]=F[1]=0.0;    
  }

  //Contributions from the COM-component of the force
  dydt[j][x_a] -= (F[0]*n_com[0] + F[1]*n_com[1])*n_com[0];
  dydt[j][y_a] -= (F[0]*n_com[0] + F[1]*n_com[1])*n_com[1];
  dydt[j][x_b] -= (F[0]*n_com[0] + F[1]*n_com[1])*n_com[0];
  dydt[j][y_b] -= (F[0]*n_com[0] + F[1]*n_com[1])*n_com[1];
  
  //Find component of the force perpendicular to COM component
  n_perp[0]= n_com[1]; 
  n_perp[1]=-n_com[0];
  //if(n_perp[0]*n_bCell[0] +n_perp[1]*n_bCell[1] > 0) {
  if(n_perp[0]*F[0] +n_perp[1]*F[1] > 0) {
    n_perp[0] = - n_com[1];
    n_perp[1] =  n_com[0];
  }

  //Rotational contibutions 
  F_perp =-(F[0]*n_perp[0] + F[1]*n_perp[1]); 
  //change sign if negative rotation (thetaNeigh-dt)
  if( n_perp[0]*n_com[1]-n_perp[1]*n_com[0]<0 )
    F_perp = -F_perp;
  deriv = a_neigh*F_perp*dist /INeigh;
  dydt[j][x_a] += deriv*sin(thetaNeigh);
  dydt[j][y_a] -= deriv*cos(thetaNeigh); 
  dydt[j][x_b] -= deriv*sin(thetaNeigh);
  dydt[j][y_b] += deriv*cos(thetaNeigh);       
}

SpringRepulsiveCigar::
SpringRepulsiveCigar(std::vector<double> &paraValue, 
		     std::vector< std::vector<size_t> > &indValue ) {
  
  //Do some checks on the parameters and variable indeces
  //////////////////////////////////////////////////////////////////////
  if( paraValue.size()<2 || paraValue.size()>3 ) {
    std::cerr << "SpringRepulsiveCigar::SpringRepulsiveCigar() "
	      << "Uses two or three parameters K_force b [I_fac=1.0].\n";
    exit(0);
  }
  if( indValue.size()>1 ||
      (indValue.size() == 1 && indValue[0].size() != 1) ) {
    std::cerr << "SpringRepulsiveCigar::SpringRepulsiveCigar() "
	      << "One variable index can be used. Stores Force.\n";
    exit(0);
  }
  if( paraValue.size()==2 )
    paraValue.push_back(1.0);
  //Set the variable values
  //////////////////////////////////////////////////////////////////////
  setId("springRepulsiveCigar");
  setParameter(paraValue);  
  setVariableIndex(indValue);
  
  //Set the parameter identities
  //////////////////////////////////////////////////////////////////////
  std::vector<std::string> tmp( numParameter() );
  tmp[0] = "K_force";
  tmp[1] = "b";
  tmp[2] = "I_fac";
  setParameterId( tmp );
}

void SpringRepulsiveCigar::
derivs(Compartment &compartment,size_t varIndex,
       std::vector< std::vector<double> > &y,
       std::vector< std::vector<double> > &dydt ) {
  
  size_t i=compartment.index(); 
  //Set saving column to zero (UGLY)
  if( numVariableIndexLevel() && i==0 )
    for( size_t ii=0 ; ii<y.size() ; ii++ )
      y[ii][variableIndex(0,0)] = 0.0;
  
  if( compartment.numTopologyVariable() != 5 &&
      compartment.numTopologyVariable() != 7 ) {
    std::cerr << "SpringRepulsiveCigar::derivs() "
	      << "Wrong number of topology variables.\n";
    exit(-1);
  }
  size_t dimension = (compartment.numTopologyVariable()-1)/2;
  
  if( compartment.numNeighbor()<1 ) return;

  double b=parameter(1);
  //Get data columns to be used
  std::vector<size_t> x1Col(dimension),x2Col(dimension);
  for( size_t d=0 ; d<dimension ; d++ ) {
    x1Col[d] = varIndex+d;
    x2Col[d] = x1Col[d]+dimension;
  }
  //Set cell parameters
  std::vector<double> xc(dimension),nc(dimension);
  for( size_t d=0 ; d<dimension ; d++ ) {
    xc[d] = 0.5*(y[i][ x1Col[d] ]+y[i][ x2Col[d] ]);
    nc[d] = xc[d]-y[i][ x1Col[d] ];
  }
  double a = 0.0;
  for( size_t d=0 ; d<dimension ; d++ )
    a += nc[d]*nc[d];
  a = std::sqrt(a);
  
  double pi = 3.141592654;    
  double I = parameter(2)*(a*a+b*b);//With some factor?
  double phi=0.0;
  if(nc[0] == 0) {
    if(nc[1] == 0)
      phi = 0.0;
    else if(nc[1] > 0.0)
      phi = 0.5*pi;
    else if(nc[1] < 0)
      phi = 1.5*pi;
  }
  else if(nc[0] > 0 && nc[1] >= 0)
    phi = std::atan(nc[1]/nc[0]);
  else if(nc[0] < 0 && nc[1] >= 0)
    phi = pi + std::atan(nc[1]/nc[0]);
  else if(nc[0] < 0 && nc[1] < 0)
    phi = pi + std::atan(nc[1]/nc[0]);
  else if(nc[0] > 0 && nc[1] < 0)
    phi = 2*pi + std::atan(nc[1]/nc[0]);
  if( dimension==3 ) {
    std::cerr << "SpringRepulsiveCigar::derivs() "
	      << "No rotation in 3D yet...\n";
    exit(-1);
  }
  
  //double forceCell=0.0;
  //Loop over neighbors
  for( size_t k=0 ; k<compartment.numNeighbor() ; k++ ) {
    //Set neighbor parameters
    size_t j = compartment.neighbor(k);
    std::vector<double> xcNeigh(dimension),ncNeigh(dimension);
    for( size_t d=0 ; d<dimension ; d++ ) {
      xcNeigh[d] = 0.5*(y[j][ x1Col[d] ]+y[j][ x2Col[d] ]);
      ncNeigh[d] = xcNeigh[d]-y[j][ x1Col[d] ];
    }
    double aNeigh = 0.0;
    for( size_t d=0 ; d<dimension ; d++ )
      aNeigh += ncNeigh[d]*ncNeigh[d]; 
    aNeigh = std::sqrt(aNeigh);
    double INeigh = parameter(2)*(aNeigh*aNeigh+b*b);//With some factor?
    double phiNeigh=0.0;
    if(ncNeigh[0] == 0) {
      if(ncNeigh[1] == 0)
	phiNeigh = 0.0;
      else if(ncNeigh[1] > 0.0)
	phiNeigh = 0.5*pi;
      else if(ncNeigh[1] < 0)
	phiNeigh = 1.5*pi;
    }
    else if(ncNeigh[0] > 0 && ncNeigh[1] >= 0)
      phiNeigh = std::atan(ncNeigh[1]/ncNeigh[0]);
    else if(ncNeigh[0] < 0 && ncNeigh[1] >= 0)
      phiNeigh = pi + std::atan(ncNeigh[1]/ncNeigh[0]);
    else if(ncNeigh[0] < 0 && ncNeigh[1] < 0)
      phiNeigh = pi + std::atan(ncNeigh[1]/ncNeigh[0]);
    else if(ncNeigh[0] > 0 && ncNeigh[1] < 0)
      phiNeigh = 2*pi + std::atan(ncNeigh[1]/ncNeigh[0]);


    double forceTotal=0.0;
    //Find closest distance from end points to lines
    //////////////////////////////////////////////////
    //x1Cell
    std::vector<double> x1Pos(dimension),x2Pos(dimension),
      nSol(dimension);
    for( size_t d=0 ; d<dimension ; d++ )
      x1Pos[d] = y[i][x1Col[d]];
    double t = 0.0;
    for( size_t d=0 ; d<dimension ; d++ )
      t += ncNeigh[d]*(x1Pos[d]-xcNeigh[d]);
    t /= (aNeigh*aNeigh);
    if( t<-1.0 ) {//outside neigh in x1 direction
      for( size_t d=0 ; d<dimension ; d++ )
	x2Pos[d] = y[j][x1Col[d]];
    }
    else if( t>1.0 ) {//outside neigh in x2 direction
      for( size_t d=0 ; d<dimension ; d++ )
	x2Pos[d] = y[j][x2Col[d]];
    }
    else {//on line
      for( size_t d=0 ; d<dimension ; d++ )
	x2Pos[d] = xcNeigh[d]+t*ncNeigh[d];
    }
    for( size_t d=0 ; d<dimension ; d++ )
      nSol[d] = x2Pos[d]-x1Pos[d];
    double dSol = 0.0;
    for( size_t d=0 ; d<dimension ; d++ )
      dSol += nSol[d]*nSol[d]; 
    dSol = std::sqrt( dSol );

    double solNormalFac=0.0;
    for( size_t d=0 ; d<dimension ; d++ )
      solNormalFac +=nSol[d]*nc[d];
    if( dSol<2.0*b && solNormalFac<0.0 ) {
      //For solution
      ////////////////////////////////////////
      std::vector<double> F(dimension),xF(dimension),nCom(dimension),
	Ftmp(dimension),nRot(dimension);
      double overlap = 2.0*b-dSol;
      for( size_t d=0 ; d<dimension ; d++ )
	F[d] = parameter(0)*overlap*nSol[d]/dSol;
      
      //For cell
      ////////////////////////////////////////
      for( size_t d=0 ; d<dimension ; d++ ) {
	xF[d] = x1Pos[d] + b*nSol[d]/dSol;
	nCom[d] = xc[d]-xF[d];
      }
      double nComLength = 0.0;
      for( size_t d=0 ; d<dimension ; d++ )
	nComLength += nCom[d]*nCom[d];
      nComLength = std::sqrt( nComLength );
      for( size_t d=0 ; d<dimension ; d++ )
	nCom[d] /= nComLength;
      //COM-push
      ////////////////////
      double FCom = 0.0;
      for( size_t d=0 ; d<dimension ; d++ )
	FCom -= F[d]*nCom[d];
      if( FCom<0.0 ) {
	//std::cerr << "SpringRepulsiveCigar::derivs() F_com="
	//  << FCom << " strange\n";
	FCom=0.0;
	for( size_t d=0 ; d<dimension ; d++ )
	  F[d]=0.0;
      }
      for( size_t d=0 ; d<dimension ; d++ )
	Ftmp[d] = FCom*nCom[d];
      //Add update
      for( size_t d=0 ; d<dimension ; d++ ) {
	dydt[i][x1Col[d]] += Ftmp[d];
	dydt[i][x2Col[d]] += Ftmp[d];
      }
      //Rotation
      ////////////////////
      nRot[0] = nCom[1];
      nRot[1] = -nCom[0];
      double FRot = 0.0;
      for( size_t d=0 ; d<dimension ; d++ )
	FRot -= nRot[d]*F[d];
      if( FRot<0.0 ) {
	FRot = -FRot;
	for( size_t d=0 ; d<dimension ; d++ )
	  nRot[d] = -nRot[d];
      }
      //Change sign if negative rotation
      if( nRot[0]*nCom[1]-nRot[1]*nCom[0]<0.0 )
	FRot = -FRot;
      double derivs = a*nComLength*FRot/I;
      dydt[i][x1Col[0]] += derivs*std::sin(phi);
      dydt[i][x2Col[0]] -= derivs*std::sin(phi);
      dydt[i][x1Col[1]] -= derivs*std::cos(phi);
      dydt[i][x2Col[1]] += derivs*std::cos(phi);
      forceTotal += parameter(0)*overlap;
      //For neighbor
      ////////////////////////////////////////
      for( size_t d=0 ; d<dimension ; d++ ) {
	xF[d] = x2Pos[d] - b*nSol[d]/dSol;
	nCom[d] = xcNeigh[d]-xF[d];
      }
      nComLength = 0.0;
      for( size_t d=0 ; d<dimension ; d++ )
	nComLength += nCom[d]*nCom[d];
      nComLength = std::sqrt( nComLength );
      for( size_t d=0 ; d<dimension ; d++ )
	nCom[d] /= nComLength;
      
      //COM-push
      ////////////////////
      FCom = 0.0;
      for( size_t d=0 ; d<dimension ; d++ )
	FCom += F[d]*nCom[d];
      if( FCom<0.0 ) {
	//std::cerr << "SpringRepulsiveCigar::derivs() F_com="
	//  << FCom << " strange\n";
	FCom=0.0;
	for( size_t d=0 ; d<dimension ; d++ )
	  F[d]=0.0;
      }
      for( size_t d=0 ; d<dimension ; d++ )
	Ftmp[d] = FCom*nCom[d];
      //Add update
      for( size_t d=0 ; d<dimension ; d++ ) {
	dydt[j][x1Col[d]] += Ftmp[d];
	dydt[j][x2Col[d]] += Ftmp[d];
      }
      //Rotation
      ////////////////////
      nRot[0] = nCom[1];
      nRot[1] = -nCom[0];
      FRot = 0.0;
      for( size_t d=0 ; d<dimension ; d++ )
	FRot += nRot[d]*F[d];
      if( FRot<0.0 ) {
	FRot = -FRot;
	nRot[0] = -nRot[0];
	nRot[1] = -nRot[1];
      }
      //Change sign if negative rotation
      if( nRot[0]*nCom[1]-nRot[1]*nCom[0]<0.0 )
	FRot = -FRot;
      derivs = aNeigh*nComLength*FRot/INeigh;
      dydt[j][x1Col[0]] += derivs*std::sin(phiNeigh);
      dydt[j][x2Col[0]] -= derivs*std::sin(phiNeigh);
      dydt[j][x1Col[1]] -= derivs*std::cos(phiNeigh);
      dydt[j][x2Col[1]] += derivs*std::cos(phiNeigh);
      forceTotal += parameter(0)*overlap;
    }
    //x2Cell
    for( size_t d=0 ; d<dimension ; d++ )
      x1Pos[d] = y[i][x2Col[d]];
    t = 0.0;
    for( size_t d=0 ; d<dimension ; d++ )
      t += ncNeigh[d]*(x1Pos[d]-xcNeigh[d]);
    t /= aNeigh*aNeigh;
    if( t<-1.0 ) {//outside neigh in x1 direction
      for( size_t d=0 ; d<dimension ; d++ )
	x2Pos[d] = y[j][x1Col[d]];
    }
    else if( t>1.0 ) {//outside neigh in x2 direction
      for( size_t d=0 ; d<dimension ; d++ )
	x2Pos[d] = y[j][x2Col[d]];
    }
    else {//on line
      for( size_t d=0 ; d<dimension ; d++ )
	x2Pos[d] = xcNeigh[d]+t*ncNeigh[d];
    }
    for( size_t d=0 ; d<dimension ; d++ )
      nSol[d] = x2Pos[d]-x1Pos[d];
    dSol = 0.0;
    for( size_t d=0 ; d<dimension ; d++ )
      dSol += nSol[d]*nSol[d];
    dSol = std::sqrt( dSol );
    solNormalFac=0.0;
    for( size_t d=0 ; d<dimension ; d++ )
      solNormalFac +=nSol[d]*nc[d];

    if( dSol<2.0*b && solNormalFac>0.0 ) {//solution
      //For solution
      ////////////////////////////////////////
      std::vector<double> F(dimension),xF(dimension),nCom(dimension),
	Ftmp(dimension),nRot(dimension);
      double overlap = 2.0*b-dSol;
      for( size_t d=0 ; d<dimension ; d++ )
	F[d] = parameter(0)*overlap*nSol[d]/dSol;
      
      //For cell
      ////////////////////////////////////////
      double nComLength=0.0;
      for( size_t d=0 ; d<dimension ; d++ ) {
	xF[d] = x1Pos[d] + b*nSol[d]/dSol;
	nCom[d] = xc[d]-xF[d];
	nComLength += nCom[d]*nCom[d];
      }
      nComLength = std::sqrt( nComLength );
      for( size_t d=0 ; d<dimension ; d++ )
	nCom[d] /= nComLength;
      //COM-push
      ////////////////////
      double FCom = 0.0;
      for( size_t d=0 ; d<dimension ; d++ )
	FCom -= F[0]*nCom[0];
      if( FCom<0.0 ) {
	//std::cerr << "SpringRepulsiveCigar::derivs() F_com="
	//  << FCom << " strange\n";
	FCom=0.0;
	for( size_t d=0 ; d<dimension ; d++ )
	  F[d]=0.0;
      }
      for( size_t d=0 ; d<dimension ; d++ )
	Ftmp[d] = FCom*nCom[d];
      //Add update
      for( size_t d=0 ; d<dimension ; d++ ) {
	dydt[i][x1Col[d]] += Ftmp[d];
	dydt[i][x2Col[d]] += Ftmp[d];
      }
      //Rotation
      ////////////////////
      nRot[0] = nCom[1];
      nRot[1] = -nCom[0];
      double FRot = 0.0;
      for( size_t d=0 ; d<dimension ; d++ )
	FRot -= nRot[d]*F[d];
      if( FRot<0.0 ) {
	FRot = -FRot;
	nRot[0] = -nRot[0];
	nRot[1] = -nRot[1];
      }
      //Change sign if negative rotation
      if( nRot[0]*nCom[1]-nRot[1]*nCom[0]<0.0 )
	FRot = -FRot;
      double derivs = a*nComLength*FRot/I;
      dydt[i][x1Col[0]] += derivs*std::sin(phi);
      dydt[i][x2Col[0]] -= derivs*std::sin(phi);
      dydt[i][x1Col[1]] -= derivs*std::cos(phi);
      dydt[i][x2Col[1]] += derivs*std::cos(phi);
      forceTotal += parameter(0)*overlap;
      //For neighbor
      ////////////////////////////////////////
      nComLength=0.0;
      for( size_t d=0 ; d<dimension ; d++ ) {
	xF[d] = x2Pos[d] - b*nSol[d]/dSol;
	nCom[0] = xcNeigh[0]-xF[0];
	nComLength += nCom[d]*nCom[d];
      }
      nComLength = std::sqrt( nComLength );
      for( size_t d=0 ; d<dimension ; d++ )
	nCom[d] /= nComLength;
      //COM-push
      ////////////////////
      FCom = 0.0;
      for( size_t d=0 ; d<dimension ; d++ )
	FCom += F[d]*nCom[d];
      if( FCom<0.0 ) {
	//std::cerr << "SpringRepulsiveCigar::derivs() F_com="
	//  << FCom << " strange\n";
	FCom=0.0;
	for( size_t d=0 ; d<dimension ; d++ )
	  F[d]=0.0;
      }
      for( size_t d=0 ; d<dimension ; d++ )
	Ftmp[d] = FCom*nCom[d];
      //Add update
      for( size_t d=0 ; d<dimension ; d++ ) {
	dydt[j][x1Col[d]] += Ftmp[d];
	dydt[j][x2Col[d]] += Ftmp[d];
      }
      //Rotation
      ////////////////////
      nRot[0] = nCom[1];
      nRot[1] = -nCom[0];
      FRot = 0.0;
      for( size_t d=0 ; d<dimension ; d++ )
	FRot += nRot[d]*F[d];
      if( FRot<0.0 ) {
	FRot = -FRot;
	nRot[0] = -nRot[0];
	nRot[1] = -nRot[1];
      }
      //Change sign if negative rotation
      if( nRot[0]*nCom[1]-nRot[1]*nCom[0]<0.0 )
	FRot = -FRot;
      derivs = aNeigh*nComLength*FRot/INeigh;
      dydt[j][x1Col[0]] += derivs*std::sin(phiNeigh);
      dydt[j][x2Col[0]] -= derivs*std::sin(phiNeigh);
      dydt[j][x1Col[1]] -= derivs*std::cos(phiNeigh);
      dydt[j][x2Col[1]] += derivs*std::cos(phiNeigh);
      forceTotal += parameter(0)*overlap;
    }
    
    if( numVariableIndexLevel() ) {
      y[i][variableIndex(0,0)] += forceTotal;
      y[j][variableIndex(0,0)] += forceTotal;
    }
  }
}

//!Constructor for the SpringWallCigar2 class
SpringWallCigar2::SpringWallCigar2(std::vector<double> &paraValue, 
				   std::vector< std::vector<size_t> > 
				   &indValue ) {
  
  //Do some checks on the parameters and variable indeces
  //////////////////////////////////////////////////////////////////////
  if( paraValue.size()!=7 ) {
    std::cerr << "SpringWallCigar2::SpringWallCigar2()"
	      << "Uses seven parameters K_force x_1, y_1, x_2, "
	      << "y_2, normal direction (+1or -1), and b.\n";
    exit(0);
  }
  if(paraValue[5] != 1 && paraValue[5] != -1) {
    std::cerr << "SpringWallCigar2::SpringWallCigar2()"
	      << " normal direction must be +1 or -1 (not " 
	      << paraValue[5] << ")\n";
    exit(0);
  }
  if( indValue.size() > 1 || 
      ( indValue.size()==1 && indValue[0].size() != 1 )) {
    std::cerr << "SpringWallCigar2::SpringWallCigar2()"
	      << "one variable index can be used (index for force)\n";
    exit(0);
  }
  //Set the variable values
  //////////////////////////////////////////////////////////////////////
  setId("springWallCigar2");
  setParameter(paraValue);  
  setVariableIndex(indValue);
  
  //Set the parameter identities
  //////////////////////////////////////////////////////////////////////
  std::vector<std::string> tmp( numParameter() );
  tmp[0] = "K_force";
  tmp[1] = "x_1";
  tmp[2] = "y_1";
  tmp[3] = "x_2";
  tmp[4] = "y_2";
  tmp[5] = "direction";
  tmp[6] = "b";
  setParameterId( tmp );
}

//! Derivative contribution for a mechanical interaction from a wall.
void SpringWallCigar2::derivs(Compartment &compartment,size_t varIndex,
			      std::vector< std::vector<double> > &y,
			      std::vector< std::vector<double> > &dydt ) {
  
  
  //bool solutionFlag=0; 
  //Setting wall properties
  //double K_W = parameter(0);
  double X_1 = parameter(1), Y_1 =  parameter(2), 
    X_2 =  parameter(3), Y_2 =  parameter(4);
  double wallLength = std::sqrt( (X_2 - X_1)*(X_2 - X_1) + 
				 (Y_2 - Y_1)*(Y_2 - Y_1) );
  double N_W[2] = { (X_2 - X_1)/wallLength, (Y_2 - Y_1)/wallLength } ;
  double normal_W[2];
  //double wallForce = 0;
  //if parameter(5) = +1 chose right-handed direction of the normal, otherwize left-handed 
  if(parameter(5) > 0) {
    normal_W[0] = -N_W[1];
    normal_W[1] = N_W[0];
  } 
  else {
    normal_W[0] = N_W[1];
    normal_W[1] = -N_W[0];
  }

  size_t i=compartment.index(); 
  //Set saving column to zero (UGLY)
  if( numVariableIndexLevel() && i==0 )
    for( size_t ii=0 ; ii<y.size() ; ii++ )
      y[ii][variableIndex(0,0)] = 0.0;
  

  size_t dimension=2;
  if( compartment.numTopologyVariable() != 2*dimension+1 ) {
    std::cerr << "SpringRepulsiveCigar::derivs() "
	      << "Wrong number of topology variables.\n";
    exit(-1);
  }
  double b=parameter(6);
  //Get data columns to be used
  std::vector<size_t> x1Col(dimension),x2Col(dimension);
  for( size_t d=0 ; d<dimension ; d++ ) {
    x1Col[d] = varIndex+d;
    x2Col[d] = x1Col[d]+dimension;
  }
  //Set cell parameters
  std::vector<double> xc(dimension),nc(dimension);
  for( size_t d=0 ; d<dimension ; d++ ) {
    xc[d] = 0.5*(y[i][ x1Col[d] ]+y[i][ x2Col[d] ]);
    nc[d] = xc[d]-y[i][ x1Col[d] ];
  }
  double a = std::sqrt( nc[0]*nc[0] + nc[1]*nc[1] );
  double pi = 3.141592654;    
  double I = (a*a+b*b);//With some factor?
  double theta=0.0;
  if(nc[0] == 0) {
    if(nc[1] == 0)
      theta = 0.0;
    else if(nc[1] > 0.0)
      theta = 0.5*pi;
    else if(nc[1] < 0)
      theta = 1.5*pi;
  }
  else if(nc[0] > 0 && nc[1] >= 0)
    theta = std::atan(nc[1]/nc[0]);
  else if(nc[0] < 0 && nc[1] >= 0)
    theta = pi + std::atan(nc[1]/nc[0]);
  else if(nc[0] < 0 && nc[1] < 0)
    theta = pi + std::atan(nc[1]/nc[0]);
  else if(nc[0] > 0 && nc[1] < 0)
    theta = 2*pi + std::atan(nc[1]/nc[0]);
  
  double forceTotal=0.0;
  //Find closest distance from cell end points to wall line
  //////////////////////////////////////////////////
  //x1Cell
  //////////////////////////////////////////////////
  std::vector<double> x1Pos(dimension),x2Pos(dimension),
    nSol(dimension);
  x1Pos[0] = y[i][x1Col[0]];
  x1Pos[1] = y[i][x1Col[1]];
  double t = ( N_W[0]*(x1Pos[0]-X_1) +
	       N_W[1]*(x1Pos[1]-Y_1) );
  if( t<0.0 ) {//outside wall in x1 direction
    x2Pos[0] = X_1;
    x2Pos[1] = Y_1;
  }
  else if( t>wallLength ) {//outside wall in x2 direction
    x2Pos[0] = X_2;
    x2Pos[1] = Y_2;
  }
  else {//on line
    x2Pos[0] = X_1+t*N_W[0];
    x2Pos[1] = Y_1+t*N_W[1];
  }
  nSol[0] = x2Pos[0]-x1Pos[0];
  nSol[1] = x2Pos[1]-x1Pos[1];
  double dSol = std::sqrt( nSol[0]*nSol[0]+nSol[1]*nSol[1] );
  if( dSol<b && nSol[0]*nc[0]+nSol[1]*nc[1]<=0.0 ) {
    //For solution
    ////////////////////////////////////////
    std::vector<double> F(dimension),xF(dimension),nCom(dimension),
      Ftmp(dimension),nRot(dimension);
    double overlap = b-dSol;
    F[0] = parameter(0)*overlap*nSol[0]/dSol;
    F[1] = parameter(0)*overlap*nSol[1]/dSol;	
    
    xF[0] = x1Pos[0] + b*nSol[0]/dSol;
    xF[1] = x1Pos[1] + b*nSol[1]/dSol;
    nCom[0] = xc[0]-xF[0];
    nCom[1] = xc[1]-xF[1];
    double nComLength = std::sqrt( nCom[0]*nCom[0]+nCom[1]*nCom[1] );
    nCom[0] /= nComLength;
    nCom[1] /= nComLength;
    //COM-push
    ////////////////////
    double FCom = -(F[0]*nCom[0]+F[1]*nCom[1]);
    if( FCom<0.0 ) {
      FCom=F[0]=F[1]=0.0;
      std::cerr << "SpringWallCigar2::derivs() F_com strange.\n";
    }
    Ftmp[0] = FCom*nCom[0];
    Ftmp[1] = FCom*nCom[1]; 
    //Add update
    dydt[i][x1Col[0]] += Ftmp[0];
    dydt[i][x2Col[0]] += Ftmp[0];
    dydt[i][x1Col[1]] += Ftmp[1];
    dydt[i][x2Col[1]] += Ftmp[1];
    //Rotation
    ////////////////////
    nRot[0] = nCom[1];
    nRot[1] = -nCom[0];
    double FRot = -(nRot[0]*F[0]+nRot[1]*F[1]);
    if( FRot<0.0 ) {
      FRot = -FRot;
      nRot[0] = -nRot[0];
      nRot[1] = -nRot[1];
    }
    //Change sign if negative rotation
    if( nRot[0]*nCom[1]-nRot[1]*nCom[0]<0.0 )
      FRot = -FRot;
    double derivs = a*nComLength*FRot/I;
    dydt[i][x1Col[0]] += derivs*std::sin(theta);
    dydt[i][x2Col[0]] -= derivs*std::sin(theta);
    dydt[i][x1Col[1]] -= derivs*std::cos(theta);
    dydt[i][x2Col[1]] += derivs*std::cos(theta);
    forceTotal += parameter(0)*overlap;
  }
  //x2Cell
  //////////////////////////////////////////////////
  x1Pos[0] = y[i][x2Col[0]];
  x1Pos[1] = y[i][x2Col[1]];
  t = ( N_W[0]*(x1Pos[0]-X_1) + N_W[1]*(x1Pos[1]-Y_1) );
  if( t<0.0 ) {//outside wall in x1 direction
    x2Pos[0] = X_1;
    x2Pos[1] = Y_1;
  }
  else if( t>wallLength ) {//outside wall in x2 direction
    x2Pos[0] = X_2;
    x2Pos[1] = Y_2;
  }
  else {//on line
    x2Pos[0] = X_1+t*N_W[0];
    x2Pos[1] = Y_1+t*N_W[1];
  }
  nSol[0] = x2Pos[0]-x1Pos[0];
  nSol[1] = x2Pos[1]-x1Pos[1];
  dSol = std::sqrt( nSol[0]*nSol[0]+nSol[1]*nSol[1] );
  if( dSol<b && nSol[0]*nc[0]+nSol[1]*nc[1]>=0.0 ) {//solution
    //For solution
    ////////////////////////////////////////
    std::vector<double> F(dimension),xF(dimension),nCom(dimension),
      Ftmp(dimension),nRot(dimension);
    double overlap = b-dSol;
    F[0] = parameter(0)*overlap*nSol[0]/dSol;
    F[1] = parameter(0)*overlap*nSol[1]/dSol;	
    
    xF[0] = x1Pos[0] + b*nSol[0]/dSol;
    xF[1] = x1Pos[1] + b*nSol[1]/dSol;
    nCom[0] = xc[0]-xF[0];
    nCom[1] = xc[1]-xF[1];
    double nComLength = std::sqrt( nCom[0]*nCom[0]+nCom[1]*nCom[1] );
    nCom[0] /= nComLength;
    nCom[1] /= nComLength;
    //COM-push
    ////////////////////
    double FCom = -(F[0]*nCom[0]+F[1]*nCom[1]);
    if( FCom<0.0 ) {
      FCom=F[0]=F[1]=0.0;
      std::cerr << "SpringRepulsiveCigar::derivs() F_com strange.\n";
    }
    Ftmp[0] = FCom*nCom[0];
    Ftmp[1] = FCom*nCom[1]; 
    //Add update
    dydt[i][x1Col[0]] += Ftmp[0];
    dydt[i][x2Col[0]] += Ftmp[0];
    dydt[i][x1Col[1]] += Ftmp[1];
    dydt[i][x2Col[1]] += Ftmp[1];
    //Rotation
    ////////////////////
    nRot[0] = nCom[1];
    nRot[1] = -nCom[0];
    double FRot = -(nRot[0]*F[0]+nRot[1]*F[1]);
    if( FRot<0.0 ) {
      FRot = -FRot;
      nRot[0] = -nRot[0];
      nRot[1] = -nRot[1];
    }
    //Change sign if negative rotation
    if( nRot[0]*nCom[1]-nRot[1]*nCom[0]<0.0 )
      FRot = -FRot;
    double derivs = a*nComLength*FRot/I;
    dydt[i][x1Col[0]] += derivs*std::sin(theta);
    dydt[i][x2Col[0]] -= derivs*std::sin(theta);
    dydt[i][x1Col[1]] -= derivs*std::cos(theta);
    dydt[i][x2Col[1]] += derivs*std::cos(theta);
    forceTotal += parameter(0)*overlap;
  }    
  if( numVariableIndexLevel() ) {
    y[i][variableIndex(0,0)] += forceTotal;
  }
}

//!Constructor for the SpringElasticWallCigar2 class
SpringElasticWallCigar2::
SpringElasticWallCigar2(std::vector<double> &paraValue, 
			 std::vector< std::vector<size_t> > &indValue ) {
  
  //Do some checks on the parameters and variable indeces
  //////////////////////////////////////////////////////////////////////
  if( paraValue.size() != 9 ) {
    std::cerr << "SpringElasticWallCigar2::SpringElasticWallCigar2()"
	      << "Uses seven parameters K_force x_1, y_1, x_2, y_2, "
	      << "normalDirection_wall (+1or -1), numWallSection, "
	      << "wallMass,"
	      << "and b_cigar.\n";
    exit(0);
  }
  if( paraValue[0] < 0.0 ) {
    std::cerr << "SpringElasticWallCigar2::SpringElasticWallCigar2()"
	      << "K_force must be positive, (not " 
	      << paraValue[0] << ")\n";
    exit(0);
  }
  if(paraValue[5] != 1 && paraValue[5] != -1) {
    std::cerr << "SpringElasticWallCigar2::SpringElasticWallCigar2()"
	      << " normal direction must be +1 or -1 (not " 
	      << paraValue[5] << ")\n";
    exit(0);
  }
  if(paraValue[6] < 1.0 || paraValue[6] != double(int(paraValue[6])) ) {
    std::cerr << "SpringElasticWallCigar2::SpringElasticWallCigar2()"
	      << " numWallSection (p_6) should be a positive integer, "
	      << "(not " << paraValue[5] << ")\n";
    exit(0);
  }
  if( indValue.size()>1 ||
      (indValue.size() == 1 && indValue[0].size() !=1) ) {
    std::cerr << "SpringElasticWallCigar2::SpringElasticWallCigar2()"
	      << "at most one variable index is used, "
	      << "for saving force on wall\n";
    exit(0);
  }
  //Set the variable values
  //////////////////////////////////////////////////////////////////////
  setId("springElasticWallCigar2");
  setParameter(paraValue);  
  setVariableIndex(indValue);
  
  //Set the parameter identities
  //////////////////////////////////////////////////////////////////////
  std::vector<std::string> tmp( numParameter() );
  tmp[0] = "K_force";
  tmp[1] = "x_1";
  tmp[2] = "y_1";
  tmp[3] = "x_2";
  tmp[4] = "y_2";
  tmp[5] = "normDir_w";
  tmp[6] = "N_w";
  tmp[7] = "mass_w";
  tmp[8] = "b_c";
  setParameterId( tmp );
  
  //Initiate the wall positional variables
  //////////////////////////////////////////////////////////////////////
  //Resize vectors
  size_t Npos = static_cast<size_t>(paraValue[6])+2;  
  xW_.resize(Npos);
  dxW_.resize(Npos);
  for( size_t i=0 ; i<Npos ; i++ ) {
    xW_[i].resize(2);
    dxW_[i].resize(2);
  }
  //Get end points from parameters
  size_t Nwall=Npos-1;
  xW_[0][0] = paraValue[1];
  xW_[0][1] = paraValue[2];
  xW_[Nwall][0] = paraValue[3];
  xW_[Nwall][1] = paraValue[4];
  //Position the rest of the variables in between end points 
  //and set the relaxing distance for the springs
  double frac = 1.0/double(Nwall);
  std::vector<double> D(2);
  D[0] = xW_[Nwall][0]-xW_[0][0];
  D[1] = xW_[Nwall][1]-xW_[0][1];
  dRelax_ = std::sqrt( D[0]*D[0] + D[1]*D[1] );
  for( size_t i=1 ; i<Nwall ; i++ ) {
    xW_[i][0] = xW_[0][0] + double(i)*D[0]*frac;
    xW_[i][1] = xW_[0][1] + double(i)*D[1]*frac;
  }
  //d::cerr << "SpringElasticWallCigar2()::SpringElasticWallCigar2()\n";
  //r( size_t i=0 ; i<xW_.size() ; i++ ) {
  //d::cerr << xW_[i][0] << " " << xW_[i][1] << "\t"
  //    << dxW_[i][0] << " " << dxW_[i][1] << "\n";
  //}
}

//! Derivative contribution for a mechanical interaction from a wall.
void SpringElasticWallCigar2::
derivs(Compartment &compartment,size_t varIndex,
       std::vector< std::vector<double> > &y,
       std::vector< std::vector<double> > &dydt ) {

  //Set all wall derivatives to zero and add wall-wall contributions
  //////////////////////////////////////////////////
  if( compartment.index() == 0 ) {
    for( size_t i=0 ; i<dxW_.size() ; i++ )
      for( size_t j=0 ; j<dxW_[i].size() ; j++ )
	dxW_[i][j] = 0.0;
    for( size_t wallNum=1 ; wallNum<xW_.size() ; wallNum++ ) {
      //double K_W = parameter(0);
      double X_1 =  xW_[wallNum-1][0], 
	Y_1 = xW_[wallNum-1][1], 
	X_2 = xW_[wallNum][0], 
	Y_2 = xW_[wallNum][1];
      double wallLength = sqrt( (X_2 - X_1)*(X_2 - X_1) + 
				(Y_2 - Y_1)*(Y_2 - Y_1) );
      if( wallLength<=0.0 ) {
	std::cerr << "SpringElasticWallEllipse::derivs() "
		  << " Two consecutive wall positions wrong.\n";
	exit(-1);
      }
      double coeff = parameter(0)*(1.0-wallLength/dRelax_);;
      double divX = (X_1-X_2)*coeff;
      double divY = (Y_1-Y_2)*coeff;
      if( wallNum>1 ) {
	dxW_[wallNum-1][0] -= divX;
	dxW_[wallNum-1][1] -= divY;
      }
      if( wallNum<dxW_.size()-1 ) {
	dxW_[wallNum][0] += divX;
	dxW_[wallNum][1] += divY;
      }
    }
  }
  
  //double wallForce = 0.0;
  size_t i=compartment.index(); 
  //Set saving column to zero (UGLY)
  if( numVariableIndexLevel() && i==0 )
    for( size_t ii=0 ; ii<y.size() ; ii++ )
      y[ii][variableIndex(0,0)] = 0.0;
  
  size_t dimension=2;
  if( compartment.numTopologyVariable() != 2*dimension+1 ) {
    std::cerr << "SpringRepulsiveCigar::derivs() "
	      << "Wrong number of topology variables.\n";
    exit(-1);
  }
  double b=parameter(8);
  //Get data columns to be used
  std::vector<size_t> x1Col(dimension),x2Col(dimension);
  for( size_t d=0 ; d<dimension ; d++ ) {
    x1Col[d] = varIndex+d;
    x2Col[d] = x1Col[d]+dimension;
  }
  //Set cell parameters
  std::vector<double> xc(dimension),nc(dimension);
  for( size_t d=0 ; d<dimension ; d++ ) {
    xc[d] = 0.5*(y[i][ x1Col[d] ]+y[i][ x2Col[d] ]);
    nc[d] = xc[d]-y[i][ x1Col[d] ];
  }
  double a = std::sqrt( nc[0]*nc[0] + nc[1]*nc[1] );
  double pi = 3.141592654;    
  double I = (a*a+b*b);//With some factor?
  double theta=0.0;
  if(nc[0] == 0) {
    if(nc[1] == 0)
      theta = 0.0;
    else if(nc[1] > 0.0)
      theta = 0.5*pi;
    else if(nc[1] < 0)
      theta = 1.5*pi;
  }
  else if(nc[0] > 0 && nc[1] >= 0)
    theta = std::atan(nc[1]/nc[0]);
  else if(nc[0] < 0 && nc[1] >= 0)
    theta = pi + std::atan(nc[1]/nc[0]);
  else if(nc[0] < 0 && nc[1] < 0)
    theta = pi + std::atan(nc[1]/nc[0]);
  else if(nc[0] > 0 && nc[1] < 0)
    theta = 2*pi + std::atan(nc[1]/nc[0]);
  
  //Update each wall segment
  for( size_t wallNum=1 ; wallNum<xW_.size() ; wallNum++ ) {
    
    //bool solutionFlag=0; 
    //Setting wall properties
    //double K_W = parameter(0);
    double X_1 =  xW_[wallNum-1][0], 
      Y_1 = xW_[wallNum-1][1], 
      X_2 = xW_[wallNum][0], 
      Y_2 = xW_[wallNum][1];
    double wallLength = sqrt( (X_2 - X_1)*(X_2 - X_1) + (Y_2 - Y_1)*(Y_2 - Y_1) );
    double N_W[2] = { (X_2 - X_1)/wallLength, (Y_2 - Y_1)/wallLength } ;
    double normal_W[2];
    //if parameter(5) = +1 chose right-handed direction of the normal, otherwize left-handed 
    if(parameter(5) > 0) {
      normal_W[0] = -N_W[1];
      normal_W[1] = N_W[0];
    } 
    else {
      normal_W[0] = N_W[1];
      normal_W[1] = -N_W[0];
    }
    

    double forceTotal=0.0;
    //Find closest distance from cell end points to wall line
    //////////////////////////////////////////////////
    //x1Cell
    //////////////////////////////////////////////////
    std::vector<double> x1Pos(dimension),x2Pos(dimension),
      nSol(dimension);
    x1Pos[0] = y[i][x1Col[0]];
    x1Pos[1] = y[i][x1Col[1]];
    double t = ( N_W[0]*(x1Pos[0]-X_1) +
		 N_W[1]*(x1Pos[1]-Y_1) );
    if( t<0.0 ) {//outside wall in x1 direction
      x2Pos[0] = X_1;
      x2Pos[1] = Y_1;
    }
    else if( t>wallLength ) {//outside wall in x2 direction
      x2Pos[0] = X_2;
      x2Pos[1] = Y_2;
    }
    else {//on line
      x2Pos[0] = X_1+t*N_W[0];
      x2Pos[1] = Y_1+t*N_W[1];
    }
    nSol[0] = x2Pos[0]-x1Pos[0];
    nSol[1] = x2Pos[1]-x1Pos[1];
    double dSol = std::sqrt( nSol[0]*nSol[0]+nSol[1]*nSol[1] );
    if( dSol<b && nSol[0]*nc[0]+nSol[1]*nc[1]<=0.0 ) {
      //For solution
      ////////////////////////////////////////
      std::vector<double> F(dimension),xF(dimension),nCom(dimension),
	Ftmp(dimension),nRot(dimension);
      double overlap = b-dSol;
      F[0] = parameter(0)*overlap*nSol[0]/dSol;
      F[1] = parameter(0)*overlap*nSol[1]/dSol;	
      
      xF[0] = x1Pos[0] + b*nSol[0]/dSol;
      xF[1] = x1Pos[1] + b*nSol[1]/dSol;
      nCom[0] = xc[0]-xF[0];
      nCom[1] = xc[1]-xF[1];
      double nComLength = std::sqrt( nCom[0]*nCom[0]+nCom[1]*nCom[1] );
      nCom[0] /= nComLength;
      nCom[1] /= nComLength;
      //COM-push
      ////////////////////
      double FCom = -(F[0]*nCom[0]+F[1]*nCom[1]);
      if( FCom<0.0 ) {
	FCom=F[0]=F[1]=0.0;
	std::cerr << "SpringWallCigar2::derivs() F_com strange.\n";
      }
      Ftmp[0] = FCom*nCom[0];
      Ftmp[1] = FCom*nCom[1]; 
      //Add update
      dydt[i][x1Col[0]] += Ftmp[0];
      dydt[i][x2Col[0]] += Ftmp[0];
      dydt[i][x1Col[1]] += Ftmp[1];
      dydt[i][x2Col[1]] += Ftmp[1];
      //Rotation
      ////////////////////
      nRot[0] = nCom[1];
      nRot[1] = -nCom[0];
      double FRot = -(nRot[0]*F[0]+nRot[1]*F[1]);
      if( FRot<0.0 ) {
	FRot = -FRot;
	nRot[0] = -nRot[0];
	nRot[1] = -nRot[1];
      }
      //Change sign if negative rotation
      if( nRot[0]*nCom[1]-nRot[1]*nCom[0]<0.0 )
	FRot = -FRot;
      double derivs = a*nComLength*FRot/I;
      dydt[i][x1Col[0]] += derivs*std::sin(theta);
      dydt[i][x2Col[0]] -= derivs*std::sin(theta);
      dydt[i][x1Col[1]] -= derivs*std::cos(theta);
      dydt[i][x2Col[1]] += derivs*std::cos(theta);
      forceTotal += parameter(0)*overlap;
    }
    //x2Cell
    //////////////////////////////////////////////////
    x1Pos[0] = y[i][x2Col[0]];
    x1Pos[1] = y[i][x2Col[1]];
    t = ( N_W[0]*(x1Pos[0]-X_1) + N_W[1]*(x1Pos[1]-Y_1) );
    if( t<0.0 ) {//outside wall in x1 direction
      x2Pos[0] = X_1;
      x2Pos[1] = Y_1;
    }
    else if( t>wallLength ) {//outside wall in x2 direction
      x2Pos[0] = X_2;
      x2Pos[1] = Y_2;
    }
    else {//on line
      x2Pos[0] = X_1+t*N_W[0];
      x2Pos[1] = Y_1+t*N_W[1];
    }
    nSol[0] = x2Pos[0]-x1Pos[0];
    nSol[1] = x2Pos[1]-x1Pos[1];
    dSol = std::sqrt( nSol[0]*nSol[0]+nSol[1]*nSol[1] );
    if( dSol<b && nSol[0]*nc[0]+nSol[1]*nc[1]>=0.0 ) {//solution
      //For solution
      ////////////////////////////////////////
      std::vector<double> F(dimension),xF(dimension),nCom(dimension),
	Ftmp(dimension),nRot(dimension);
      double overlap = b-dSol;
      F[0] = parameter(0)*overlap*nSol[0]/dSol;
      F[1] = parameter(0)*overlap*nSol[1]/dSol;	
      
      xF[0] = x1Pos[0] + b*nSol[0]/dSol;
      xF[1] = x1Pos[1] + b*nSol[1]/dSol;
      nCom[0] = xc[0]-xF[0];
      nCom[1] = xc[1]-xF[1];
      double nComLength = std::sqrt( nCom[0]*nCom[0]+nCom[1]*nCom[1] );
      nCom[0] /= nComLength;
      nCom[1] /= nComLength;
      //COM-push
      ////////////////////
      double FCom = -(F[0]*nCom[0]+F[1]*nCom[1]);
      if( FCom<0.0 ) {
	FCom=F[0]=F[1]=0.0;
	std::cerr << "SpringRepulsiveCigar::derivs() F_com strange.\n";
      }
      Ftmp[0] = FCom*nCom[0];
      Ftmp[1] = FCom*nCom[1]; 
      //Add update
      dydt[i][x1Col[0]] += Ftmp[0];
      dydt[i][x2Col[0]] += Ftmp[0];
      dydt[i][x1Col[1]] += Ftmp[1];
      dydt[i][x2Col[1]] += Ftmp[1];
      //Rotation
      ////////////////////
      nRot[0] = nCom[1];
      nRot[1] = -nCom[0];
      double FRot = -(nRot[0]*F[0]+nRot[1]*F[1]);
      if( FRot<0.0 ) {
	FRot = -FRot;
	nRot[0] = -nRot[0];
	nRot[1] = -nRot[1];
      }
      //Change sign if negative rotation
      if( nRot[0]*nCom[1]-nRot[1]*nCom[0]<0.0 )
	FRot = -FRot;
      double derivs = a*nComLength*FRot/I;
      dydt[i][x1Col[0]] += derivs*std::sin(theta);
      dydt[i][x2Col[0]] -= derivs*std::sin(theta);
      dydt[i][x1Col[1]] -= derivs*std::cos(theta);
      dydt[i][x2Col[1]] += derivs*std::cos(theta);
      forceTotal += parameter(0)*overlap;
    }    
    if( numVariableIndexLevel() ) {
      y[i][variableIndex(0,0)] += forceTotal;
    }
  }
}

//!Updates the wall positions
void SpringElasticWallCigar2::
update(double h, double t,
			 std::vector< std::vector<double> > &y) 
{  
  size_t end = xW_.size()-1;
  if( dxW_[0][0] != 0.0 || dxW_[0][1] != 0.0 || 
      dxW_[end][0] != 0.0 || dxW_[end][1] != 0.0 ) {
    std::cerr << "SpringElasticWallCigar2::updateWall() "
	      << " Contributions to end point!\n";
    exit(-1);
  }
  for( size_t i=1 ; i<xW_.size()-1 ; i++ ) {
    xW_[i][0] = xW_[i][0] + h*dxW_[i][0];
    xW_[i][1] = xW_[i][1] + h*dxW_[i][1];
  }
  //std::cerr << "SpringElasticWallCigar2()::update()\n";
  //for( size_t i=0 ; i<xW_.size() ; i++ ) {
  //std::cerr << xW_[i][0] << " " << xW_[i][1] << "\t"
  //    << dxW_[i][0] << " " << dxW_[i][1] << "\n";
  //}
}

//!Prints the wall positions and the internal force 
void SpringElasticWallCigar2::print(std::ofstream &os) {
  
  static unsigned int count=0;
  size_t end = xW_.size()-1;
  for( size_t i=0 ; i<end ; i++ ) {
    os << count << " " << xW_[i][0] << " " << xW_[i][1] << "\t"
       << dxW_[i][0] << " " << dxW_[i][1] << "\n";
    os << count << " " << xW_[i+1][0] << " " << xW_[i+1][1] << "\t"
       << dxW_[i+1][0] << " " << dxW_[i+1][1] << "\n\n";
  }
  count++;
}

LagrangianCigar::
LagrangianCigar(std::vector<double> &paraValue, 
		     std::vector< std::vector<size_t> > &indValue ) {
  
  //Do some checks on the parameters and variable indeces
  //////////////////////////////////////////////////////////////////////
  if( paraValue.size() != 3 ) {
    std::cerr << "LagrangianCigar::LagrangianCigar() "
	      << "Uses three parameters K_Internal, K_External and b\n";
    exit(0);
  }
  if( indValue.size()>1 ||
      (indValue.size() == 1 && indValue[0].size() != 1) ) {
    std::cerr << "LagrangianCigar::LagrangianCigar() "
	      << "One variable index can be used. Stores Force.\n";
    exit(0);
  }
  if( paraValue.size()==2 )
    paraValue.push_back(1.0);
  //Set the variable values
  //////////////////////////////////////////////////////////////////////
  setId("lagrangianCigar");
  setParameter(paraValue);  
  setVariableIndex(indValue);
  
  //Set the parameter identities
  //////////////////////////////////////////////////////////////////////
  std::vector<std::string> tmp( numParameter() );
  tmp[0] = "K_int";
  tmp[1] = "K_ext";
  tmp[2] = "b";
  setParameterId( tmp );
}

void LagrangianCigar::
derivs(Compartment &compartment,size_t varIndex,
       std::vector< std::vector<double> > &y,
       std::vector< std::vector<double> > &dydt ) {
  
  
  size_t i=compartment.index(); 
  //Set saving column to zero (UGLY)
  if( numVariableIndexLevel() && i==0 )
    for( size_t ii=0 ; ii<y.size() ; ii++ )
      y[ii][variableIndex(0,0)] = 0.0;
  
  if( compartment.numTopologyVariable() != 5 ) {
    std::cerr << "LagrangianCigar::derivs() "
	      << "Wrong number of topology variables.\n";
    exit(-1);
  }
  size_t dimension = (compartment.numTopologyVariable()-1)/2;
  
  double b=parameter(2);
  //Get data columns to be used
  std::vector<size_t> xaCol(dimension),xbCol(dimension);
  for( size_t d=0 ; d<dimension ; d++ ) {
    xaCol[d] = varIndex+d;
    xbCol[d] = xaCol[d]+dimension;
  }
  size_t DCol = compartment.numTopologyVariable() - 1+varIndex;
  double D = y[i][DCol];
  //Set cell parameters
  std::vector<double> xaPos(dimension),xbPos(dimension);
  for( size_t d=0 ; d<dimension ; d++ ) {
    xaPos[d] = y[i][xaCol[d]];
    xbPos[d] = y[i][xbCol[d]];
  }
  std::vector<double> xc(dimension),n(dimension);
  for( size_t d=0 ; d<dimension ; d++ ) {
    xc[d] = 0.5*(y[i][ xaCol[d] ]+y[i][ xbCol[d] ]);
    n[d] = xc[d]-y[i][ xaCol[d] ];
  }
  double a = 0.0;
  for( size_t d=0 ; d<dimension ; d++ )
    a += n[d]*n[d];
  a = std::sqrt(a);
  for( size_t d=0 ; d<dimension ; d++ )
    n[d] = n[d]/a;
  
  for( size_t d=0 ; d<dimension ; d++ ) {
    //std::cerr << dHdxa[d] << " " << dHdxb[d] << "\n";
    dydt[i][xaCol[d]] -= 
      parameter(0)*(xaPos[d]-xbPos[d])*(2*a - D)/(2*a);
    dydt[i][xbCol[d]] += 
      parameter(0)*(xaPos[d]-xbPos[d])*(2*a - D)/(2*a);
  }  
  if( compartment.numNeighbor()<1 ) return;

  //double forceTotal=0.0,
  double  H = 0.0;
  std::vector<double> dHdxa(dimension),dHdxb(dimension);
  std::vector<double> dHdxaNeigh(dimension),dHdxbNeigh(dimension);
  //Loop over neighbors
  for( size_t k=0 ; k<compartment.numNeighbor() ; k++ ) {
    //Set neighbor parameters
    size_t j = compartment.neighbor(k);
    if( j > i) {
      //Set cell parameters
      H=0.0;
      for(size_t d =0 ; d<dimension ; d++ ) {
	dHdxa[d] = 0;
	dHdxb[d] = 0;
	dHdxaNeigh[d] = 0;
	dHdxbNeigh[d] = 0;
      }
      std::vector<double> xaPosNeigh(dimension),xbPosNeigh(dimension);
      for( size_t d=0 ; d<dimension ; d++ ) {
	xaPosNeigh[d] = y[j][xaCol[d]];
	xbPosNeigh[d] = y[j][xbCol[d]];
      }
      std::vector<double> xcNeigh(dimension),nNeigh(dimension);
      for( size_t d=0 ; d<dimension ; d++ ) {
	xcNeigh[d] = 0.5*(y[j][ xaCol[d] ]+y[j][ xbCol[d] ]);
	nNeigh[d] = xcNeigh[d]-y[j][ xaCol[d] ];
      }
      double aNeigh = 0.0;
      for( size_t d=0 ; d<dimension ; d++ )
	aNeigh += nNeigh[d]*nNeigh[d]; 
      aNeigh = std::sqrt(aNeigh);
      for( size_t d=0 ; d<dimension ; d++ )
	nNeigh[d] = nNeigh[d]/aNeigh;
      
      //Check if the two lines crosses each other    
      bool crossFlag = 0;
      double t =(xaPos[0] - xaPosNeigh[0])*nNeigh[1] -(xaPos[1] - xaPosNeigh[1])*nNeigh[0];
      double tNeigh = nNeigh[0]>nNeigh[1] ? 
	(xaPos[0] + n[0]*t - xaPosNeigh[0])/nNeigh[0] :
	(xaPos[1] + n[1]*t - xaPosNeigh[1])/nNeigh[1]; 
      if( (t <= 2*a && t >= 0) && (tNeigh <= 2*aNeigh && tNeigh >= 0) ) 
	crossFlag = 1;
      //Calculate overlap and derivatives
      ///////////////////////////////////////////////////////////
      
      //projections
      double proja = 0.0, projb = 0.0, projaNeigh = 0.0, projbNeigh = 0.0;
      for( size_t d=0 ; d<dimension ; d++ ){
	proja += nNeigh[d]*(xaPos[d] -xcNeigh[d]);
	projb += nNeigh[d]*(xbPos[d] -xcNeigh[d]);
	projaNeigh += n[d]*(xaPosNeigh[d] -xc[d]);
	projbNeigh += n[d]*(xbPosNeigh[d] -xc[d]);	
      }
      double dist = 0.0;
      
      //Distances from point xaPos
      if(proja <= aNeigh && proja >= -aNeigh) {
	for( size_t d=0 ; d<dimension ; d++ )
	  dist += (xaPos[d] - xcNeigh[d])*(xaPos[d] - xcNeigh[d]);
	dist -= proja*proja;
	dist = std::sqrt(dist);
	
	if(2*b - dist > 0) {
	  double invDist = 1/dist;
	  H += pow(2*b -dist,1.5);
	  for( size_t d=0 ; d<dimension ; d++ ) 
	    dHdxa[d] -= ((xaPos[d] - xcNeigh[d]) - nNeigh[d]*proja )* invDist;
	  //////////////////////////////////////////////////////////////////////
	  //ONLY VALID IN 2d!!!!!!!!///////////////////////////////////////////
	  //////////////////////////////////////////////////////////////////////
	  dHdxaNeigh[0] += 0.5*invDist*( xaPos[0] - xcNeigh[0] +
					 proja*(  -nNeigh[1]*nNeigh[1]*(xaPos[0] - xcNeigh[0]) +
						  nNeigh[0]*nNeigh[1]*(xaPos[1] - xcNeigh[1]) -
						  aNeigh*nNeigh[0] )/aNeigh );
	  dHdxaNeigh[1] += 0.5*invDist*( xaPos[1] - xcNeigh[1] +
					 proja*(  -nNeigh[0]*nNeigh[0]*(xaPos[1] - xcNeigh[1]) +
						  nNeigh[0]*nNeigh[1] *(xaPos[0] - xcNeigh[0]) -
						  aNeigh*nNeigh[1] )/aNeigh );
	  
	  dHdxbNeigh[0] += 0.5*invDist*( xaPos[0] - xcNeigh[0] +
					 proja*(  nNeigh[1]*nNeigh[1]*(xaPos[0] - xcNeigh[0]) -
						  nNeigh[0]*nNeigh[1]*(xaPos[1] - xcNeigh[1]) -
						  aNeigh*nNeigh[0] )/aNeigh );
	  dHdxbNeigh[1] += 0.5*invDist*( xaPos[1] - xcNeigh[1] +
					 proja*(  nNeigh[0]*nNeigh[0]*(xaPos[1] - xcNeigh[1]) -
						  nNeigh[0]*nNeigh[1]*(xaPos[0] - xcNeigh[0]) -
						  aNeigh*nNeigh[1] )/aNeigh );
	  //////////////////////////////////////////////////////////////////////
	  //////////////////////////////////////////////////////////////////////
	  //////////////////////////////////////////////////////////////////////
	}
      }
      else if(proja < -aNeigh) {
	for( size_t d=0 ; d<dimension ; d++ )
	  dist += (xaPos[d] - xaPosNeigh[d])*(xaPos[d] - xaPosNeigh[d]);
	dist = std::sqrt(dist);
	if(2*b -dist > 0) {
	  H += pow(2*b - dist, 1.5);
	  for( size_t d=0 ; d<dimension ; d++ ) {
	    double value = (xaPos[d] - xaPosNeigh[d])/dist;
	    dHdxa[d] -= value;
	    dHdxaNeigh[d] += value; 
	  }
	}
      }
      else if(proja > aNeigh) {
	for( size_t d=0 ; d<dimension ; d++ )
	  dist += (xaPos[d] - xbPosNeigh[d])*(xaPos[d] - xbPosNeigh[d]);
	dist = std::sqrt(dist);
	if(2*b -dist > 0) {
	  H += pow(2*b - dist, 1.5);
	  for( size_t d=0 ; d<dimension ; d++ ) {
	    double value = (xaPos[d] - xbPosNeigh[d])/dist;
	    dHdxa[d] -= value;
	    dHdxbNeigh[d] += value;
	  }
	}
      }
      else {
	std::cerr << "void LagrangianCigar::derivs()  Something is wrong with projection a!\n";
	std::cerr << proja << " " << aNeigh << "\n";
	exit(-1);
      }
      
      //Distances from point xbPos
      dist = 0.0;
      if(projb <=aNeigh && projb >= -aNeigh) {
	for( size_t d=0 ; d<dimension ; d++ )
	  dist += (xbPos[d] - xcNeigh[d])*(xbPos[d] - xcNeigh[d]);
	dist -= projb*projb;
	dist = std::sqrt(dist);
	if(2*b -dist > 0) {
	  double invDist = 1/dist;
	  H += pow(2*b - dist, 1.5);
	  for( size_t d=0 ; d<dimension ; d++ )
	    dHdxb[d] -= ((xbPos[d] - xcNeigh[d]) - nNeigh[d]*projb )*invDist ; 
	  //////////////////////////////////////////////////////////////////////
	  //ONLY VALID IN 2d!!!!!!!!///////////////////////////////////////////
	  //////////////////////////////////////////////////////////////////////
	  dHdxaNeigh[0] += 0.5*invDist*( xbPos[0] - xcNeigh[0] +
					 projb*(  -nNeigh[1]*nNeigh[1]*(xbPos[0] - xcNeigh[0]) +
						  nNeigh[0]*nNeigh[1]*(xbPos[1] - xcNeigh[1]) -
						  aNeigh*nNeigh[0] )/aNeigh );
	  dHdxaNeigh[1] += 0.5*invDist*( xbPos[1] - xcNeigh[1] +
					 projb*(  -nNeigh[0]*nNeigh[0]*(xbPos[1] - xcNeigh[1]) +
						  nNeigh[0]*nNeigh[1]*(xbPos[0] - xcNeigh[0]) -
						  aNeigh*nNeigh[1] )/aNeigh );
	  
	  dHdxbNeigh[0] += 0.5*invDist*( xbPos[0] - xcNeigh[0] +
					 projb*(  nNeigh[1]*nNeigh[1]*(xbPos[0] - xcNeigh[0]) -
						  nNeigh[0]*nNeigh[1]*(xbPos[1] - xcNeigh[1]) -
						  0.5*aNeigh*nNeigh[0] )/aNeigh );
	  dHdxbNeigh[1] += 0.5*invDist*( xbPos[1] - xcNeigh[1] +
					 projb*(  nNeigh[0]*nNeigh[0]*(xbPos[1] - xcNeigh[1]) -
						  nNeigh[0]*nNeigh[1]*(xbPos[0] - xcNeigh[0]) -
						  aNeigh*nNeigh[1] )/aNeigh );
	  //////////////////////////////////////////////////////////////////////
	  //////////////////////////////////////////////////////////////////////
	  //////////////////////////////////////////////////////////////////////
	}
      }
      else if(projb < -aNeigh) {
	for( size_t d=0 ; d<dimension ; d++ )
	  dist += (xbPos[d] - xaPosNeigh[d])*(xbPos[d] - xaPosNeigh[d]);
	dist = std::sqrt(dist);
	if(2*b -dist > 0) {
	  H += pow(2*b - dist, 1.5);
	  for( size_t d=0 ; d<dimension ; d++ ){
	    double value = (xbPos[d] - xaPosNeigh[d])/dist;
	    dHdxb[d] -= value;
	    dHdxaNeigh[d] += value;
	  }
	}
      }
      else if(projb > aNeigh) {
	for( size_t d=0 ; d<dimension ; d++ )
	  dist += (xbPos[d] - xbPosNeigh[d])*(xbPos[d] - xbPosNeigh[d]);
	dist = std::sqrt(dist);
	if(2*b -dist > 0) {
	  H += pow(2*b - dist, 1.5);
	  for( size_t d=0 ; d<dimension ; d++ ){
	    double value =(xbPos[d] - xbPosNeigh[d])/dist;
	    dHdxb[d] -= value;
	    dHdxbNeigh[d] += value;
	  }
	}
      }
      else {
	std::cerr << "void LagrangianCigar::derivs() Something is wrong with projection b!\n";
	std::cerr << projb << " " << aNeigh << "\n";
	exit(-1);
      }
      //Distances from point xaPosNeigh
      dist = 0.0;
      if(projaNeigh <= a && projaNeigh >= -a) {
	for( size_t d=0 ; d<dimension ; d++ )
	  dist += (xaPosNeigh[d] - xc[d])*(xaPosNeigh[d] - xc[d]);
	dist -= projaNeigh*projaNeigh;
	dist = std::sqrt(dist);
	if(2*b -dist > 0) {
	  double invDist = 1/dist;
	  H += pow(2*b - dist, 1.5);
	  for( size_t d=0 ; d<dimension ; d++ ) {
	    dHdxaNeigh[d] -= ((xaPosNeigh[d] - xc[d]) - n[d]*projaNeigh )* invDist;
	    /*dHdxa[d] -= -( (xaPosNeigh[d] - xc[d]) + projaNeigh*
	      ( - (xaPosNeigh[d] - xaPos[d]) + projaNeigh*(xbPos[d] -xaPos[d])/(2*a) )/a 
	      ) / (2*dist);
	      dHdxb[d] -= -( (xaPosNeigh[d] - xc[d]) + projaNeigh*
	      ( (xaPosNeigh[d] - xbPos[d]) - projaNeigh*(xbPos[d] -xaPos[d])/(2*a) )/a 
	      ) / (2*dist);*/
	  }
	  //////////////////////////////////////////////////////////////////////
	  //ONLY VALID IN 2d!!!!!!!!///////////////////////////////////////////
	  //////////////////////////////////////////////////////////////////////
	  dHdxa[0] += 0.5*invDist*( xaPosNeigh[0] - xc[0] +
				    projaNeigh*(  -n[1]*n[1]*(xaPosNeigh[0] - xc[0]) +
						  n[0]*n[1]*(xaPosNeigh[1] - xc[1]) -
						  a*n[0] )/a );
	  dHdxa[1] += 0.5*invDist*( xaPosNeigh[1] - xc[1] +
				    projaNeigh*(  -n[0]*n[0]*(xaPosNeigh[1] - xc[1]) +
						  n[0]*n[1]*(xaPosNeigh[0] - xc[0]) -
						  a*n[1] )/a );
	  
	  dHdxb[0] += 0.5*invDist*( xaPosNeigh[0] - xc[0] +
				    projaNeigh*(  n[1]*n[1]*(xaPosNeigh[0] - xc[0]) -
						  n[0]*n[1]*(xaPosNeigh[1] - xc[1]) -
						  a*n[0] )/a );
	  dHdxb[1] += 0.5*invDist*( xaPosNeigh[1] - xc[1] +
				    projaNeigh*(  n[0]*n[0]*(xaPosNeigh[1] - xc[1]) -
						  n[0]*n[1]*(xaPosNeigh[0] - xc[0]) -
						  a*n[1] )/a );
	  //////////////////////////////////////////////////////////////////////
	  //////////////////////////////////////////////////////////////////////
	  //////////////////////////////////////////////////////////////////////
	}
      }
      else if(projaNeigh < -a) {
	for( size_t d=0 ; d<dimension ; d++ )
	  dist += (xaPosNeigh[d] - xaPos[d])*(xaPosNeigh[d] - xaPos[d]);
	dist = std::sqrt(dist);
	if(2*b -dist > 0) {
	  H += pow(2*b - dist, 1.5);
	  for( size_t d=0 ; d<dimension ; d++ ){
	    double value = (xaPos[d] - xaPosNeigh[d])/dist;
	    dHdxa[d] -= value;
	    dHdxaNeigh[d] += value; 
	  }
	}
      }
      else if(projaNeigh > a) {
	for( size_t d=0 ; d<dimension ; d++ )
	  dist += (xaPosNeigh[d] - xbPos[d])*(xaPosNeigh[d] - xbPos[d]);
	dist = std::sqrt(dist);
	if(2*b -dist > 0) {
	  H += pow(2*b - dist, 1.5);
	  for( size_t d=0 ; d<dimension ; d++ ){
	    double value = (xbPos[d] - xaPosNeigh[d])/dist;
	    dHdxaNeigh[d] += value;
	    dHdxb[d] -= value;
	  }
	}
      }
      else {
	std::cerr << "void LagrangianCigar::derivs() Something is wrong with projection aq for neigbor!\n";
	std::cerr << projaNeigh << " " << a << "\n";
	exit(-1);
      }
      //Distances from point xbPosNeigh
      dist = 0.0;
      if(projbNeigh <= a && projbNeigh >= -a) {
	for( size_t d=0 ; d<dimension ; d++ )
	  dist += (xbPosNeigh[d] - xc[d])*(xbPosNeigh[d] - xc[d]);
	dist -= projbNeigh*projbNeigh;
	dist = std::sqrt(dist);
	if(2*b -dist > 0) {
	  double invDist = 1/dist;
	  H += pow(2*b - dist, 1.5);
	  for( size_t d=0 ; d<dimension ; d++ ) {
	    dHdxbNeigh[d] -= ((xbPosNeigh[d] - xc[d]) - n[d]*projbNeigh )* invDist;
	    /*dHdxa[d] -= -( (xbPosNeigh[d] - xc[d]) + projbNeigh*
	      ( - (xbPosNeigh[d] - xaPos[d]) + projbNeigh*(xbPos[d] -xaPos[d])/(2*a) )/a 
	      ) / (2*dist);
	      dHdxb[d] -= -( (xbPosNeigh[d] - xc[d]) + projbNeigh*
	      ( (xbPosNeigh[d] - xbPos[d]) - projbNeigh*(xbPos[d] -xaPos[d])/(2*a) )/a 
	      ) / (2*dist);*/
	  }
	  //////////////////////////////////////////////////////////////////////
	  //ONLY VALID IN 2d!!!!!!!!///////////////////////////////////////////
	  //////////////////////////////////////////////////////////////////////
	  dHdxa[0] += 0.5*invDist*( xbPosNeigh[0] - xc[0] +
				    projbNeigh*(  -n[1]*n[1]*(xbPosNeigh[0] - xc[0]) +
						  n[0]*n[1]*(xbPosNeigh[1] - xc[1]) -
						  a*n[0] )/a );
	  dHdxa[1] += 0.5*invDist*( xbPosNeigh[1] - xc[1] +
				    projbNeigh*(  -n[0]*n[0]*(xbPosNeigh[1] - xc[1]) +
						  n[0]*n[1]*(xbPosNeigh[0] - xc[0]) -
						  a*n[1] )/a );
	  
	  dHdxb[0] += 0.5*invDist*( xbPosNeigh[0] - xc[0] +
				    projbNeigh*(  n[1]*n[1]*(xbPosNeigh[0] - xc[0]) -
						  n[0]*n[1]*(xbPosNeigh[1] - xc[1]) -
						  a*n[0] )/a );
	  dHdxb[1] += 0.5*invDist*( xbPosNeigh[1] - xc[1] +
				    2*projbNeigh*(  n[0]*n[0]*(xbPosNeigh[1] - xc[1]) -
						    n[0]*n[1]*(xbPosNeigh[0] - xc[0]) -
						    a*n[1] )/a );
	  //////////////////////////////////////////////////////////////////////
	  //////////////////////////////////////////////////////////////////////
	  //////////////////////////////////////////////////////////////////////
	}
      }
      else if(projbNeigh < -a) {
	for( size_t d=0 ; d<dimension ; d++ )
	  dist += (xbPosNeigh[d] - xaPos[d])*(xbPosNeigh[d] - xaPos[d]);
	dist = std::sqrt(dist);
	if(2*b -dist > 0) {
	  H += pow(2*b - dist, 1.5);
	  for( size_t d=0 ; d<dimension ; d++ ){
	    double value = (xaPos[d] - xbPosNeigh[d])/dist;
	    dHdxa[d] -= value;
	    dHdxbNeigh[d] += value; 
	  }
	}
      }
      else if(projbNeigh > a) {
	for( size_t d=0 ; d<dimension ; d++ )
	  dist += (xbPosNeigh[d] - xbPos[d])*(xbPosNeigh[d] - xbPos[d]);
	dist = std::sqrt(dist);
	if(2*b -dist > 0) {
	  H += pow(2*b - dist, 1.5);
	  for( size_t d=0 ; d<dimension ; d++ ){
	    double value = (xbPos[d] - xbPosNeigh[d])/dist;
	    dHdxbNeigh[d] += value;
	    dHdxb[d] -=  value;
	  }
	}
      }
      else {
	std::cerr << "void LagrangianCigar::derivs() Something is wrong with projection b for neighbor!\n";
	std::cerr << projbNeigh << " " << a << "\n";
	exit(-1);
      }
      for( size_t d=0 ; d<dimension ; d++ ) {
	double fac = parameter(1)*H;
	dydt[i][xaCol[d]] -=  fac*dHdxa[d];
	dydt[i][xbCol[d]] -=  fac*dHdxb[d];
	dydt[j][xaCol[d]] -=  fac*dHdxaNeigh[d];
	dydt[j][xbCol[d]] -=  fac*dHdxbNeigh[d];
	y[i][variableIndex(0,0)] +=  fac*fac*( dHdxa[d]*dHdxa[d]+ dHdxb[d]*dHdxb[d]);
	y[j][variableIndex(0,0)] +=  fac*fac*( dHdxaNeigh[d]*dHdxaNeigh[d]+ dHdxbNeigh[d]*dHdxbNeigh[d]);
	
      }
    }
  }
  
}



//!Constructor for the LagrangianWallCigar class
LagrangianWallCigar::LagrangianWallCigar(std::vector<double> &paraValue, 
				   std::vector< std::vector<size_t> > 
				   &indValue ) {
  
  //Do some checks on the parameters and variable indeces
  //////////////////////////////////////////////////////////////////////
  if( paraValue.size()!=7 ) {
    std::cerr << "LagrangianWallCigar::LagrangianWallCigar()"
	      << "Uses seven parameters K_force x_1, y_1, x_2, "
	      << "y_2, normal direction (+1or -1), and b.\n";
    exit(0);
  }
  if(paraValue[5] != 1 && paraValue[5] != -1) {
    std::cerr << "LagrangianWallCigar::LagrangianWallCigar()"
	      << " normal direction must be +1 or -1 (not " 
	      << paraValue[5] << ")\n";
    exit(0);
  }
  if( indValue.size() > 1 || 
      ( indValue.size()==1 && indValue[0].size() != 1 )) {
    std::cerr << "LagrangianWallCigar::LagrangianWallCigar()"
	      << "one variable index can be used (index for force)\n";
    exit(0);
  }
  //Set the variable values
  //////////////////////////////////////////////////////////////////////
  setId("lagrangianWallCigar");
  setParameter(paraValue);  
  setVariableIndex(indValue);
  
  //Set the parameter identities
  //////////////////////////////////////////////////////////////////////
  std::vector<std::string> tmp( numParameter() );
  tmp[0] = "K_force";
  tmp[1] = "x_1";
  tmp[2] = "y_1";
  tmp[3] = "x_2";
  tmp[4] = "y_2";
  tmp[5] = "direction";
  tmp[6] = "b";
  setParameterId( tmp );
}

//! Derivative contribution for a mechanical interaction from a wall.
void LagrangianWallCigar::derivs(Compartment &compartment,size_t varIndex,
			      std::vector< std::vector<double> > &y,
			      std::vector< std::vector<double> > &dydt ) {
  
  
  //bool solutionFlag=0; 
  //Setting wall properties
  //double K_W = parameter(0);
  double X_1 = parameter(1), Y_1 =  parameter(2), 
    X_2 =  parameter(3), Y_2 =  parameter(4);
  double wallLength = std::sqrt( (X_2 - X_1)*(X_2 - X_1) + 
				 (Y_2 - Y_1)*(Y_2 - Y_1) );
  double N_W[2] = { (X_2 - X_1)/wallLength, (Y_2 - Y_1)/wallLength } ;
  double startPoint = X_1*N_W[0] + Y_1*N_W[1];
  double endPoint = X_2*N_W[0] + Y_2*N_W[1];
  double normal_W[2];
  //double wallForce = 0;
  //if parameter(5) = +1 chose right-handed direction of the normal, otherwize left-handed 
  if(parameter(5) > 0) {
    normal_W[0] = -N_W[1];
    normal_W[1] = N_W[0];
  } 
  else {
    normal_W[0] = N_W[1];
    normal_W[1] = -N_W[0];
  }

  size_t i=compartment.index(); 
  //Set saving column to zero (UGLY)
  if( numVariableIndexLevel() && i==0 )
    for( size_t ii=0 ; ii<y.size() ; ii++ )
      y[ii][variableIndex(0,0)] = 0.0;
  

  size_t dimension=2;
  if( compartment.numTopologyVariable() != 2*dimension+1 ) {
    std::cerr << "LagrangianWallCigar::derivs() "
	      << "Wrong number of topology variables.\n";
    exit(-1);
  }
  double b=parameter(6);
  //Get data columns to be used
  std::vector<size_t> xaCol(dimension),xbCol(dimension);
  for( size_t d=0 ; d<dimension ; d++ ) {
    xaCol[d] = varIndex+d;
    xbCol[d] = xaCol[d]+dimension;
  }
  //Set cell parameters
    std::vector<double> xaPos(dimension),xbPos(dimension);
  for( size_t d=0 ; d<dimension ; d++ ) {
    xaPos[d] = y[i][xaCol[d]];
    xbPos[d] = y[i][xbCol[d]];
  }
  std::vector<double> xc(dimension),n(dimension);
  for( size_t d=0 ; d<dimension ; d++ ) {
    xc[d] = 0.5*(y[i][ xaCol[d] ]+y[i][ xbCol[d] ]);
    n[d] = xc[d]-y[i][ xaCol[d] ];
  }
  double a = 0.0;
  for( size_t d=0 ; d<dimension ; d++ )
    a += n[d]*n[d];
  a = std::sqrt(a);
  for( size_t d=0 ; d<dimension ; d++ )
    n[d] = n[d]/a;
   
  //Find perpendicular crossing point with wall
  std::vector<double> xaPosWall(dimension),xbPosWall(dimension);
  double ta = ( (xaPos[0]-X_1)*N_W[1] - (xaPos[1]-Y_1)*N_W[0] ) /
    ( normal_W[1]*N_W[0] - normal_W[0]*N_W[1] );
  double tb = ( (xbPos[0]-X_1)*N_W[1] - (xbPos[1]- Y_1)*N_W[0] ) /
    ( normal_W[1]*N_W[0] - normal_W[0]*N_W[1] );
  for( size_t d=0 ; d<dimension ; d++ ) {
    xaPosWall[d] = xaPos[d] + normal_W[d]*ta;
    xbPosWall[d] = xbPos[d] + normal_W[d]*tb;
  }
  

    //Calculate overlap and derivatives
    ///////////////////////////////////////////////////////////
  double proja= 0, projb = 0, projaWall = 0, projbWall=0;
  for( size_t d=0 ; d<dimension ; d++ ) {
    projaWall += xaPosWall[d]*N_W[d];
    projbWall += xaPosWall[d]*N_W[d];
    proja += n[d]*(xaPosWall[d] -xc[d]);
    projb += n[d]*(xbPosWall[d] -xc[d]);
  }   
  
  //Distances from point xaPos
  double H = 0;
  std::vector<double> dHdxa(dimension), dHdxb(dimension);
  double dist = fabs(ta);
  if(projaWall <= endPoint && projaWall >= startPoint) {
    if(b -dist > 0) {
      H += pow(b - dist,1.5);
      for( size_t d=0 ; d<dimension ; d++ )
	dHdxa[d] -= (xaPos[d] - xaPosWall[d])/dist;
    }
  }
  //Distances from point xbPos
  dist = fabs(tb);
  if(projbWall <= endPoint && projbWall >= startPoint) {
    if(b -dist > 0) {
      H += pow(b - dist,1.5);
      for( size_t d=0 ; d<dimension ; d++ )
	dHdxb[d] -= (xbPos[d] - xbPosWall[d])/dist;
    }
  }

 //Distances from point xaPosWall
    dist = 0.0;
    if(projaWall <= endPoint && projaWall >= startPoint) {
      if(proja <= a && proja >= -a) {
	for( size_t d=0 ; d<dimension ; d++ )
	  dist += (xaPosWall[d] - xc[d])*(xaPosWall[d] - xc[d]);
	dist -= proja*proja;
	dist = std::sqrt(dist);
	if(b -dist > 0) {
	  double invDist = 1/dist;
	  H += pow(b - dist,1.5);
	  
	  //////////////////////////////////////////////////////////////////////
	  //ONLY VALID IN 2d!!!!!!!!///////////////////////////////////////////
	  //////////////////////////////////////////////////////////////////////
	  dHdxa[0] += 0.5*invDist*( xaPosWall[0] - xc[0] +
					 proja*(  -n[1]*n[1]*( xaPosWall[0] - xc[0] ) +
						  n[0]*n[1]*(xaPosWall[1] - xc[1]) -
						  a*n[0] )/a );
	  dHdxa[1] += 0.5*invDist*( xaPosWall[1] - xc[1] +
					 proja*(  -n[0]*n[0]*(xaPosWall[1] - xc[1]) +
						  n[0]*n[1] *(xaPosWall[0] - xc[0]) -
						  a*n[1] )/a );
	  
	  dHdxb[0] += 0.5*invDist*( xaPosWall[0] - xc[0] +
					 proja*(  n[1]*n[1]*(xaPosWall[0] - xc[0]) -
						  n[0]*n[1]*(xaPosWall[1] - xc[1]) -
						  a*n[0] )/a );
	  dHdxb[1] += 0.5*invDist*( xaPosWall[1] - xc[1] +
					 proja*(  n[0]*n[0]*(xaPosWall[1] - xc[1]) -
						  n[0]*n[1]*(xaPosWall[0] - xc[0]) -
						  a*n[1] )/a );
	  //////////////////////////////////////////////////////////////////////
	  //////////////////////////////////////////////////////////////////////
	  //////////////////////////////////////////////////////////////////////


	}
      }
      else if(proja < -a) {
	for( size_t d=0 ; d<dimension ; d++ )
	  dist += (xaPosWall[d] - xaPos[d])*(xaPosWall[d] - xaPos[d]);
	dist = std::sqrt(dist);
	if(b -dist > 0) {
	  H += pow(b - dist,1.5);
	  for( size_t d=0 ; d<dimension ; d++ ){
	    dHdxa[d] -= (xaPos[d] - xaPosWall[d])/dist;
	  
	  }
	}
      }
      else if(proja > a) {
	for( size_t d=0 ; d<dimension ; d++ )
	  dist += (xaPosWall[d] - xbPos[d])*(xaPosWall[d] - xbPos[d]);
	dist = std::sqrt(dist);
	if(b -dist > 0) {
	  H += pow(b - dist,1.5);
	  for( size_t d=0 ; d<dimension ; d++ ){
	    dHdxb[d] -= (xbPos[d] - xaPosWall[d])/dist;
	  }
	}
      }
      else {
	std::cerr << "Something is wrong with projection aq for neigbor!\n";
	exit(-1);
      }
    }
    //Distances from point xbPosWall
    dist = 0.0;
    if(projbWall <= endPoint && projbWall >= startPoint) {
      if(projb <= a && projb >= -a) {
	for( size_t d=0 ; d<dimension ; d++ )
	  dist += (xbPosWall[d] - xc[d])*(xbPosWall[d] - xc[d]);
	dist -= projb*projb;
	dist = std::sqrt(dist);
	if(b -dist > 0) {
	  double invDist = 1/dist;
	  H += pow(b - dist,1.5);
	 
	  //////////////////////////////////////////////////////////////////////
	  //ONLY VALID IN 2d!!!!!!!!///////////////////////////////////////////
	  //////////////////////////////////////////////////////////////////////
	  dHdxa[0] += 0.5*invDist*( xbPosWall[0] - xc[0] +
					 proja*(  -n[1]*n[1]*( xbPosWall[0] - xc[0] ) +
						  n[0]*n[1]*(xbPosWall[1] - xc[1]) -
						  a*n[0] )/a );
	  dHdxa[1] += 0.5*invDist*( xbPosWall[1] - xc[1] +
					 proja*(  -n[0]*n[0]*(xbPosWall[1] - xc[1]) +
						  n[0]*n[1] *(xbPosWall[0] - xc[0]) -
						  a*n[1] )/a );
	  
	  dHdxb[0] += 0.5*invDist*( xbPosWall[0] - xc[0] +
					 proja*(  n[1]*n[1]*(xbPosWall[0] - xc[0]) -
						  n[0]*n[1]*(xbPosWall[1] - xc[1]) -
						  a*n[0] )/a );
	  dHdxb[1] += 0.5*invDist*( xbPosWall[1] - xc[1] +
					 proja*(  n[0]*n[0]*(xbPosWall[1] - xc[1]) -
						  n[0]*n[1]*(xbPosWall[0] - xc[0]) -
						  a*n[1] )/a );
	  //////////////////////////////////////////////////////////////////////
	  //////////////////////////////////////////////////////////////////////
	  //////////////////////////////////////////////////////////////////////

	}
      }
      else if(projb < -a) {
	for( size_t d=0 ; d<dimension ; d++ )
	  dist += (xbPosWall[d] - xaPos[d])*(xbPosWall[d] - xaPos[d]);
	dist = std::sqrt(dist);
	if(b -dist > 0) {
	  H += pow(b - dist,1.5);
	  for( size_t d=0 ; d<dimension ; d++ ){
	    dHdxa[d] -= (xaPos[d] - xbPosWall[d])/dist;
	  }
	}
      }
      else if(projb > a) {
	for( size_t d=0 ; d<dimension ; d++ )
	  dist += (xbPosWall[d] - xbPos[d])*(xbPosWall[d] - xbPos[d]);
	dist = std::sqrt(dist);
	if(b -dist > 0) {
	  H += pow(b - dist,1.5);
	  for( size_t d=0 ; d<dimension ; d++ ){
	    dHdxb[d] -= (xbPos[d] - xbPosWall[d])/dist;
	  }
	}
      }
      else {
	std::cerr << "Something is wrong with projection aq for neigbor!\n";
	exit(-1);
      }
    }
    
    double forceTotal = 0;
    for( size_t d=0 ; d<dimension ; d++ ) {
      double fac = parameter(0)*H;
      dydt[i][xaCol[d]] -=  fac*dHdxa[d];
      dydt[i][xbCol[d]] -=  fac*dHdxb[d];
      forceTotal +=  fac*dHdxa[d]*fac*dHdxa[d]+fac*dHdxb[d]*fac*dHdxb[d];      
    }
  
  forceTotal = std::sqrt(forceTotal);
  if( numVariableIndexLevel() ) {
    y[i][variableIndex(0,0)] += forceTotal;
  } 
}


//!Constructor for the PerforatedLagrangianWallCigar class
PerforatedLagrangianWallCigar::PerforatedLagrangianWallCigar(std::vector<double> &paraValue, 
				   std::vector< std::vector<size_t> > 
				   &indValue ) {
  
  //Do some checks on the parameters and variable indeces
  //////////////////////////////////////////////////////////////////////
  if( paraValue.size()!=9 ) {
    std::cerr << "PerforatedLagrangianWallCigar::PerforatedLagrangianWallCigar()"
	      << "Uses seven parameters K_force x_1, y_1, x_2, "
	      << "y_2, normal direction (+1or -1),b, numHoles and holeSize.\n";
    exit(0);
  }
  if(paraValue[5] != 1 && paraValue[5] != -1) {
    std::cerr << "PerforatedLagrangianWallCigar::PerforatedLagrangianWallCigar()"
	      << " normal direction must be +1 or -1 (not " 
	      << paraValue[5] << ")\n";
    exit(0);
  }
  if( indValue.size() > 1 || 
      ( indValue.size()==1 && indValue[0].size() != 1 )) {
    std::cerr << "PerforatedLagrangianWallCigar::PerforatedLagrangianWallCigar()"
	      << "one variable index can be used (index for force)\n";
    exit(0);
  }
  //Set the variable values
  //////////////////////////////////////////////////////////////////////
  setId("perforatedLagrangianWallCigar");
  setParameter(paraValue);  
  setVariableIndex(indValue);
  
  //Set the parameter identities
  //////////////////////////////////////////////////////////////////////
  std::vector<std::string> tmp( numParameter() );
  tmp[0] = "K_force";
  tmp[1] = "x_1";
  tmp[2] = "y_1";
  tmp[3] = "x_2";
  tmp[4] = "y_2";
  tmp[5] = "direction";
  tmp[6] = "b";
  tmp[7] = "numHoles";
  tmp[8] = "holeSize";
  setParameterId( tmp );
}

//! Derivative contribution for a mechanical interaction from a wall.
void PerforatedLagrangianWallCigar::derivs(Compartment &compartment,size_t varIndex,
			      std::vector< std::vector<double> > &y,
			      std::vector< std::vector<double> > &dydt ) {
  
  
  //bool solutionFlag=0; 
  //Setting wall properties
  //double K_W = parameter(0);
  double X_1 = parameter(1), Y_1 =  parameter(2), 
    X_2 =  parameter(3), Y_2 =  parameter(4);
 
  double wallLength = std::sqrt( (X_2 - X_1)*(X_2 - X_1) + 
				 (Y_2 - Y_1)*(Y_2 - Y_1) );
  double N_W[2] = { (X_2 - X_1)/wallLength, (Y_2 - Y_1)/wallLength } ;
  double startPoint = X_1*N_W[0] + Y_1*N_W[1];
  double endPoint = X_2*N_W[0] + Y_2*N_W[1];
  double normal_W[2];
 
  //if parameter(5) = +1 chose right-handed direction of the normal, otherwize left-handed 
  if(parameter(5) > 0) {
    normal_W[0] = -N_W[1];
    normal_W[1] = N_W[0];
  } 
  else {
    normal_W[0] = N_W[1];
    normal_W[1] = -N_W[0];
  }

  size_t i=compartment.index(); 
  //Set saving column to zero (UGLY)
  if( numVariableIndexLevel() && i==0 )
    for( size_t ii=0 ; ii<y.size() ; ii++ )
      y[ii][variableIndex(0,0)] = 0.0;
  

  size_t dimension=2;
  if( compartment.numTopologyVariable() != 2*dimension+1 ) {
    std::cerr << "PerforatedLagrangianWallCigar::derivs() "
	      << "Wrong number of topology variables.\n";
    exit(-1);
  }
  double b=parameter(6);
  size_t numHoles = static_cast<size_t>(parameter(7));
  double holeSize = parameter(8);
  
  //Get data columns to be used
  std::vector<size_t> xaCol(dimension),xbCol(dimension);
  for( size_t d=0 ; d<dimension ; d++ ) {
    xaCol[d] = varIndex+d;
    xbCol[d] = xaCol[d]+dimension;
  }
  //Set cell parameters
    std::vector<double> xaPos(dimension),xbPos(dimension);
  for( size_t d=0 ; d<dimension ; d++ ) {
    xaPos[d] = y[i][xaCol[d]];
    xbPos[d] = y[i][xbCol[d]];
  }
  std::vector<double> xc(dimension),n(dimension);
  for( size_t d=0 ; d<dimension ; d++ ) {
    xc[d] = 0.5*(y[i][ xaCol[d] ]+y[i][ xbCol[d] ]);
    n[d] = xc[d]-y[i][ xaCol[d] ];
  }
  double a = 0.0;
  for( size_t d=0 ; d<dimension ; d++ )
    a += n[d]*n[d];
  a = std::sqrt(a);
  for( size_t d=0 ; d<dimension ; d++ )
    n[d] = n[d]/a;
  //define centers of holes
  std::vector< double > holeCenter(numHoles);
  double holeCenterTMP = startPoint;
  double increase = wallLength/( static_cast<double>(numHoles +1) );
  for(size_t nh = 0; nh < numHoles ; nh++ ) {
    holeCenterTMP += increase; 
    holeCenter[nh] = holeCenterTMP;
  }
  //Find perpendicular crossing point with wall
  std::vector<double> xaPosWall(dimension),xbPosWall(dimension);
  double ta = ( (xaPos[0]-X_1)*N_W[1] - (xaPos[1]-Y_1)*N_W[0] ) /
    ( normal_W[1]*N_W[0] - normal_W[0]*N_W[1] );
  double tb = ( (xbPos[0]-X_1)*N_W[1] - (xbPos[1]- Y_1)*N_W[0] ) /
    ( normal_W[1]*N_W[0] - normal_W[0]*N_W[1] );
  for( size_t d=0 ; d<dimension ; d++ ) {
    xaPosWall[d] = xaPos[d] + normal_W[d]*ta;
    xbPosWall[d] = xbPos[d] + normal_W[d]*tb;
  }
  //Find out if xaPosWall or xbPosWall is located in a hole
  bool flaga = 1, flagb= 1;
  //std::vector<bool> flag(numHoles);
  //for(size_t nh = 0; nh < numHoles; nh++)
  //flag[n] = 1;
  double projaWall = 0, projbWall = 0, proja=0, projb=0;
  for( size_t d=0 ; d<dimension ; d++ ) {
    projaWall += xaPosWall[d]*N_W[d];
    projbWall += xaPosWall[d]*N_W[d];
    proja += n[d]*(xaPosWall[d] -xc[d]);
    projb += n[d]*(xbPosWall[d] -xc[d]);
  }  
  for(size_t nh = 0; nh < numHoles; nh++) {
    if(projaWall <= holeCenter[nh] + 0.5*holeSize && 
       projaWall >= holeCenter[nh] - 0.5*holeSize) 
      flaga = 0;
    if(projbWall <= holeCenter[nh] + 0.5*holeSize && 
       projbWall >= holeCenter[nh] - 0.5*holeSize)
      flagb = 0;
  }
  
  //Calculate overlap and derivatives
  ///////////////////////////////////////////////////////////
    
  //Distances from point xaPos
  double H = 0;
  std::vector<double> dHdxa(dimension), dHdxb(dimension);
  for( size_t d=0 ; d<dimension ; d++ ) {
    dHdxa[d] = 0;
    dHdxb[d] = 0;
  }
  
  if(endPoint && projaWall >= startPoint) {
    double dist = 0;  
    if(flaga){
      //Distances from point xaPos
      dist = fabs(ta);
      if(b -dist > 0) {
	H += b - dist;
	for( size_t d=0 ; d<dimension ; d++ )
	  dHdxa[d] -= (xaPos[d] - xaPosWall[d])/dist;
      }
      
      //Distances from point xaPosWall
      dist = 0.0;
      if(proja <= a && proja >= -a) {
	for( size_t d=0 ; d<dimension ; d++ )
	  dist += (xaPosWall[d] - xc[d])*(xaPosWall[d] - xc[d]);
      dist -= proja*proja;
      dist = std::sqrt(dist);
      if(b -dist > 0) {
	H += b - dist;
	for( size_t d=0 ; d<dimension ; d++ ) {
	  dHdxa[d] -= -( (xaPosWall[d] - xc[d]) + proja*
			 ( - (xaPosWall[d] - xaPos[d]) + proja*(xbPos[d] -xaPos[d])/(2*a) )/a 
			 ) / (2*dist);
	  dHdxb[d] -= -( (xaPosWall[d] - xc[d]) + proja*
			 ( (xaPosWall[d] - xbPos[d]) - proja*(xbPos[d] -xaPos[d])/(2*a) )/a 
			 ) / (2*dist);
	}
      }
      }
      else if(proja < -a) {
	for( size_t d=0 ; d<dimension ; d++ )
	  dist += (xaPosWall[d] - xaPos[d])*(xaPosWall[d] - xaPos[d]);
	dist = std::sqrt(dist);
	if(b -dist > 0) {
	  H += b - dist;
	  for( size_t d=0 ; d<dimension ; d++ ){
	    dHdxa[d] -= (xaPos[d] - xaPosWall[d])/dist;
	    //dHdxb[d] += 0; 
	  }
	}
      }
      else if(proja > a) {
	for( size_t d=0 ; d<dimension ; d++ )
	  dist += (xaPosWall[d] - xbPos[d])*(xaPosWall[d] - xbPos[d]);
	dist = std::sqrt(dist);
	if(b -dist > 0) {
	  H += b - dist;
	  for( size_t d=0 ; d<dimension ; d++ ){
	    //dHdxa[d] += 0;
	    dHdxb[d] -= (xbPos[d] - xaPosWall[d])/dist;
	  }
	}
      }
      else {
	std::cerr << "Something is wrong with projection aq for neigbor!\n";
	exit(-1);
      }
    }
    
    //Distances from point xbPos
    if(flagb) {
      dist = fabs(tb);
      if(b -dist > 0) {
	H += b - dist;
	for( size_t d=0 ; d<dimension ; d++ )
	  dHdxb[d] -= (xbPos[d] - xbPosWall[d])/dist;
      }
      //Distances from point xbPosWall
      dist = 0.0;
      if(projb <= a && projb >= -a) {
	for( size_t d=0 ; d<dimension ; d++ )
	  dist += (xbPosWall[d] - xc[d])*(xbPosWall[d] - xc[d]);
	dist -= projb*projb;
	dist = std::sqrt(dist);
	if(b -dist > 0) {
	  H += b - dist;
	  for( size_t d=0 ; d<dimension ; d++ ) {
	    dHdxa[d] -= -( (xbPosWall[d] - xc[d]) + projb*
			   ( - (xbPosWall[d] - xaPos[d]) + projb*(xbPos[d] -xaPos[d])/(2*a) )/a 
			   ) / (2*dist);
	    dHdxb[d] -= -( (xbPosWall[d] - xc[d]) + projb*
			   ( (xbPosWall[d] - xbPos[d]) - projb*(xbPos[d] -xaPos[d])/(2*a) )/a 
			   ) / (2*dist);
	  }
	}
      }
      else if(projb < -a) {
	for( size_t d=0 ; d<dimension ; d++ )
	  dist += (xbPosWall[d] - xaPos[d])*(xbPosWall[d] - xaPos[d]);
	dist = std::sqrt(dist);
	if(b -dist > 0) {
	  H += b - dist;
	  for( size_t d=0 ; d<dimension ; d++ ){
	    dHdxa[d] -= (xaPos[d] - xbPosWall[d])/dist;
	    //dHdxb[d] += 0; 
	  }
	}
      }
      else if(projb > a) {
	for( size_t d=0 ; d<dimension ; d++ )
	  dist += (xbPosWall[d] - xbPos[d])*(xbPosWall[d] - xbPos[d]);
	dist = std::sqrt(dist);
	if(b -dist > 0) {
	  H += b - dist;
	  for( size_t d=0 ; d<dimension ; d++ ){
	    //dHdxa[d] += 0;
	    dHdxb[d] -= (xbPos[d] - xbPosWall[d])/dist;
	  }
	}
      }
      else {
	std::cerr << "Something is wrong with projection b for neigbor!\n";
	exit(-1);
      }
    }
    //Distance from hole edges
    if(!flaga || !flagb) {
      std::vector<double> posEdgeA(dimension),posEdgeB(dimension);
      for(size_t nh = 0; nh<numHoles; nh++) {
	
	posEdgeA[0] = X_1 + (holeCenter[nh] - 0.5*holeSize)*N_W[0];
	posEdgeA[1] = Y_1 + (holeCenter[nh] - 0.5*holeSize)*N_W[1];
	posEdgeB[0] = X_1 + (holeCenter[nh] + 0.5*holeSize)*N_W[0];
	posEdgeB[1] = Y_1 + (holeCenter[nh] + 0.5*holeSize)*N_W[1];
	
	double projEdgeA = 0.0, projEdgeB = 0.0;
	for( size_t d=0; d<dimension; d++ ){
	  projEdgeA += n[d]*( posEdgeA[d] -xc[d] );
	  projEdgeB += n[d]*( posEdgeB[d] -xc[d] );
	}

	//edge A
	if(projEdgeA <= a && projEdgeA >= -a) {
	  for( size_t d=0 ; d<dimension ; d++ )
	    dist += (posEdgeA[d] - xc[d])*(posEdgeA[d] - xc[d]);
	  dist -= projEdgeA*projEdgeA;
	  dist = std::sqrt(dist);
	  if(b -dist > 0) {
	    H += b - dist;
	    for( size_t d=0 ; d<dimension ; d++ ){
	      dHdxa[d] -= -( (posEdgeA[d] - xc[d]) + projEdgeA*
			     ( - (posEdgeA[d] - xaPos[d]) + projEdgeA*(xbPos[d] -xaPos[d])/(2*a) )/a 
			     ) / (2*dist);
	      dHdxb[d] -= -( (posEdgeA[d] - xc[d]) + projEdgeA*
			     ( (posEdgeA[d] - xbPos[d]) - projEdgeA*(xbPos[d] -xaPos[d])/(2*a) )/a 
			     ) / (2*dist);
	    }
	  }
	}
	else if(projEdgeA < -a) {
	  for( size_t d=0 ; d<dimension ; d++ )
	    dist += (posEdgeA[d] - xaPos[d])*(posEdgeA[d] - xaPos[d]);
	  dist = std::sqrt(dist);
	  if(b -dist > 0) {
	    H += b - dist;
	    for( size_t d=0 ; d<dimension ; d++ ){
	      dHdxa[d] -= (xaPos[d] - posEdgeA[d])/dist;
	      //dHdxb[d] += 0; 
	    }
	  }
	}
	else if(projEdgeA > a) {
	  for( size_t d=0 ; d<dimension ; d++ )
	    dist += (posEdgeA[d] - xbPos[d])*(posEdgeA[d] - xbPos[d]);
	  dist = std::sqrt(dist);
	  if(b -dist > 0) {
	    H += b - dist;
	    for( size_t d=0 ; d<dimension ; d++ ){
	      //dHdxa[d] += 0;
	      dHdxb[d] -= (xbPos[d] - posEdgeA[d])/dist;
	    }
	  }
	}
	else {
	  std::cerr << "Something is wrong with projection aq for neigbor!\n";
	  exit(-1);
	}
	//edge 2
	if(projEdgeB <= a && projEdgeB >= -a) {
	  for( size_t d=0 ; d<dimension ; d++ )
	    dist += (posEdgeB[d] - xc[d])*(posEdgeB[d] - xc[d]);
	  dist -= projEdgeB*projEdgeB;
	  dist = std::sqrt(dist);
	  if(b -dist > 0) {
	    H += b - dist;
	    for( size_t d=0 ; d<dimension ; d++ ) {
	      dHdxa[d] -= -( (posEdgeB[d] - xc[d]) + projEdgeB*
			     ( - (posEdgeB[d] - xaPos[d]) + projEdgeB*(xbPos[d] -xaPos[d])/(2*a) )/a 
			     ) / (2*dist);
	      dHdxb[d] -= -( (posEdgeB[d] - xc[d]) + projEdgeB*
			     ( (posEdgeB[d] - xbPos[d]) - projEdgeB*(xbPos[d] -xaPos[d])/(2*a) )/a 
			     ) / (2*dist);
	    }
	  }
	}
	else if(projEdgeB < -a) {
	  for( size_t d=0 ; d<dimension ; d++ )
	    dist += (posEdgeB[d] - xaPos[d])*(posEdgeB[d] - xaPos[d]);
	  dist = std::sqrt(dist);
	  if(b -dist > 0) {
	    H += b - dist;
	    for( size_t d=0 ; d<dimension ; d++ ){
	      dHdxa[d] -= (xaPos[d] - posEdgeB[d])/dist;
	      //dHdxb[d] += 0; 
	    }
	  }
	}
	else if(projEdgeB > a) {
	  for( size_t d=0 ; d<dimension ; d++ )
	    dist += (posEdgeB[d] - xbPos[d])*(posEdgeB[d] - xbPos[d]);
	  dist = std::sqrt(dist);
	  if(b -dist > 0) {
	    H += b - dist;
	    for( size_t d=0 ; d<dimension ; d++ ){
	      //dHdxa[d] += 0;
	      dHdxb[d] -= (xbPos[d] - posEdgeB[d])/dist;
	    }
	  }
	}
	else {
	  std::cerr << "Something is wrong with projection aq for neigbor!\n";
	  exit(-1);
	}
	
      }
    }
  }  
  
  double forceTotal = 0;
  for( size_t d=0 ; d<dimension ; d++ ) {
    double fac = parameter(0)*pow(H,1.5);
    dydt[i][xaCol[d]] -=  fac*dHdxa[d];
    dydt[i][xbCol[d]] -=  fac*dHdxb[d];
    forceTotal +=  fac*dHdxa[d]*fac*dHdxa[d]+fac*dHdxb[d]*fac*dHdxb[d];
  }
  
  forceTotal = std::sqrt(forceTotal);
  if( numVariableIndexLevel() ) {
    y[i][variableIndex(0,0)] += forceTotal;
  } 
}

//!Constructor for the lagrangianCigar class
LagrangianCigarNew::
LagrangianCigarNew(std::vector<double> &paraValue, 
		     std::vector< std::vector<size_t> > &indValue ) {
  
  //Do some checks on the parameters and variable indeces
  //////////////////////////////////////////////////////////////////////
  if( paraValue.size() != 3 ) {
    std::cerr << "LagrangianCigarNew::LagrangianCigarNew() "
	      << "Uses three parameters K_Internal, K_External and b\n";
    exit(0);
  }
  if( indValue.size() != 1 || indValue[0].size() != 5 ) {
    std::cerr << "LagrangianCigarNew::LagrangianCigarNew() "
	      << "Five variable indices must be used. Stores Force.\n";
    exit(0);
  }
  if( paraValue.size()==2 )
    paraValue.push_back(1.0);
  //Set the variable values
  //////////////////////////////////////////////////////////////////////
  setId("lagrangianCigarNew");
  setParameter(paraValue);  
  setVariableIndex(indValue);
  
  //Set the parameter identities
  //////////////////////////////////////////////////////////////////////
  std::vector<std::string> tmp( numParameter() );
  tmp[0] = "K_int";
  tmp[1] = "K_ext";
  tmp[2] = "b";
  setParameterId( tmp );
}

void LagrangianCigarNew::
derivs(Compartment &compartment,size_t varIndex,
       std::vector< std::vector<double> > &y,
       std::vector< std::vector<double> > &dydt ) {
  
  size_t i=compartment.index(); 
  //Set saving column to zero (UGLY)
  //if( numVariableIndexLevel() && i==0 )
	// for( size_t ii=0 ; ii<y.size() ; ii++ )
	//  y[ii][variableIndex(0,0)] = 0.0;
  
  if( compartment.numTopologyVariable() != 5 &&
			compartment.numTopologyVariable() != 7 ) {
    std::cerr << "LagrangianCigarNew::derivs() "
							<< "Wrong number of topology variables.\n";
    exit(-1);
  }
	
  size_t dimension=(compartment.numTopologyVariable()-1)/2;    
  double B=parameter(2);
  //Get data columns to be used
  std::vector<double> u(dimension),v(dimension),w(dimension);
  std::vector<double> dsdxa_i(dimension), dsdxb_i(dimension), 
    dsdxa_j(dimension), dsdxb_j(dimension), 
    dtdxa_i(dimension), dtdxb_i(dimension), 
    dtdxa_j(dimension), dtdxb_j(dimension); 
	
  for( size_t dim=0 ; dim<dimension ; ++dim )
    u[dim] = y[i][varIndex+dim+dimension]-y[i][varIndex+dim];
  double a = 0.0;
  for( size_t dim=0 ; dim<dimension ; ++dim )   
    a += u[dim]*u[dim];

  assert( a>0.0 );

  //contribution from the spring between the two points
  double A= std::sqrt(a);
	assert( A>0.0 );
  for( size_t dim =0 ; dim<dimension ; dim++ ) {
		//double zetaInv = 1/(0.0094*(B+A));
    double derivative =
      parameter(0)*(y[i][varIndex + dim]-y[i][varIndex + dim + dimension])*
      (A - y[i][compartment.numTopologyVariable() - 1 + varIndex])/(A);
    dydt[i][varIndex + dim] -= derivative;
    dydt[i][varIndex + dim + dimension] += derivative;
  }
	//static std::map<int,int> flagStat;
  if( compartment.numNeighbor()<1 ) return;
  //Loop over neighbors
  for( size_t k=0 ; k<compartment.numNeighbor() ; k++ ) {
    //Set neighbor parameters
    size_t j = compartment.neighbor(k);
    if( j > i) {
      for( size_t dim=0 ; dim<dimension ; ++dim ){
				v[dim] = y[j][varIndex+dim+dimension]-y[j][varIndex+dim];
				w[dim] = y[i][varIndex+dim]-y[j][varIndex+dim];
      }
      double b=0.0,c=0.0,d=0.0,e=0.0;
      for( size_t dim=0 ; dim<dimension ; ++dim ) {
				b += u[dim]*v[dim];
				c += v[dim]*v[dim];
				d += u[dim]*w[dim];
				e += v[dim]*w[dim];
      } 
			//double ANeigh = std::sqrt(c);
			assert( c>0.0 );
			double D=a*c-b*b;
      double sN,sD=D,tN,tD=D;
      double aInv = 1.0/a, cInv = 1.0/c, DInv = 1.0/D;
      int flag = -1;
      bool paraCheck = false;
			if( D<1e-10 ) {
				//almost parallel, use yi[d] to prevent division by zero
				paraCheck = true;
				
				sN = 0.0;
				sD = 1.0;
				tN = e;
				tD = c;
				flag = 1;
      }
      else {
				sN = b*e-c*d;
				tN = a*e-b*d;
				flag = 2;
				if( sN<=0.0 ) {
					sN = 0.0;
					tN = e;
					tD = c;
					flag = 1;
				}
				else if( sN>=sD ) {
					sN = sD;
					tN = e+b;
					tD = c;
					flag = 3;
				}
      }
      if( tN<=0.0 ) {
				//if(paraCheck)
				//	std::cerr << "Parallell and tN = "<< tN 
				//						<< " " << i << " " << j << "\n";
				tN = 0.0;
				flag = 4;
				if( -d<0 )
					sN = 0.0;
				else if( -d>a )
					sN = sD;
				else {
					flag = 5;
					sN = -d;
					sD = a;
				}
      }
      else if( tN>=tD ) {
				//if(paraCheck)
					//std::cerr << "Parallell and tN > tD "<< tN << " " << tD <<"\n";
				tN = tD;
				flag = 4;
				if( (-d+b)<0.0 )
					sN = 0.0;
				else if( (-d+b)>a )
					sN = sD;
				else {
					sN = -d+b;
					sD = a;
					flag = 6;
				}
      }
			
			
			//flagStat[flag]++;
			
			assert( sD != 0.0 );
			assert( tD != 0.0 );
      double sc = sN/sD; 
      double tc = tN/tD;
      //assert( sc != NaN && sc != Inf && sc != -Inf );
      //assert( tc != NaN && tc != Inf && tc != -Inf );
			
      std::vector<double> dP(dimension);
      
			double dist=0.0;
      for( size_t dim=0 ; dim<dimension ; ++dim ) {
				dP[dim] = w[dim]+sc*u[dim]-tc*v[dim];
				dist += dP[dim]*dP[dim];

      }
			
			//assert( dist>0.0 );
      if (dist <= 0) {
				std::cerr << i << " " << j << " " << dist << "\n";
				std::cerr<< y[i][0] << " " << y[i][1] << "\n";
				std::cerr<< y[i][2] << " " << y[i][3] << "\n\n";
				std::cerr<< y[j][0] << " " << y[j][1] << "\n";
				std::cerr<< y[j][2] << " " << y[j][3] << "\n";
							
				mySignal::myExit();
			}
			dist = std::sqrt(dist);
			
			
			assert( dist>0.0 );

      //only if it's a true neighbor
      if(2*B - dist > 0){

				//checks which derivative that should be used
				switch (flag) {
						case 1:
							for( size_t dim=0 ; dim<dimension ; ++dim ) {
								dsdxa_i[dim] = 0;
								dsdxb_i[dim] = 0;
								dsdxa_j[dim] = 0;
								dsdxb_j[dim] = 0;
								dtdxa_i[dim] = v[dim]*cInv;
								dtdxb_i[dim] = 0;
								dtdxa_j[dim] = (v[dim]*(2*e*cInv - 1) - w[dim] )*cInv;
								dtdxb_j[dim] = (w[dim] - 2*e*v[dim]*cInv)*cInv;
							}
							break;
						case 2:
							//assert( DInv !=NaN && DInv != Inf && DInv != -Inf );
							assert( D != 0.0 );
							for( size_t dim=0 ; dim<dimension ; ++dim ) {
								dsdxa_i[dim] = ( v[dim]*( b*(1  - 2*sN*DInv) -e ) + u[dim]*c*( 2*sN*DInv - 1 ) + c*w[dim] )*DInv;
								dsdxb_i[dim] = ( v[dim]*( e + 2*b*sN*DInv ) - c*( w[dim] + 2*sN*DInv*u[dim] )  )*DInv;
								dsdxa_j[dim] = ( v[dim]*( 2*d - b + 2*a*sN*DInv ) + u[dim]*( c - e - 2*b*sN*DInv ) - b*w[dim] )*DInv;
								dsdxb_j[dim] = ( -v[dim]*( 2*d + 2*a*sN*DInv ) + u[dim]*( e + 2*b*sN*DInv ) + b*w[dim] )*DInv;
								
								dtdxa_i[dim] = ( v[dim]*( a + d - 2*b*tN*DInv ) + u[dim]*( 2*c*tN*DInv - 2*e - b) + b*w[dim] )*DInv;
								dtdxb_i[dim] = ( v[dim]*( 2*b*tN*DInv - d ) + u[dim]*( 2*e - 2*c*tN*DInv) - b*w[dim] )*DInv;
								dtdxa_j[dim] = ( v[dim]*(2*a*tN*DInv - a) + u[dim]*( d + b - 2*tN*b*DInv ) - a*w[dim] )*DInv;
								dtdxb_j[dim] = ( a*( w[dim]-v[dim]*2*tN*DInv) + u[dim]*(2*b*tN*DInv -d) )*DInv;
							}
							break;
						case 3:
							for( size_t dim=0 ; dim<dimension ; ++dim ) {
								dsdxa_i[dim] = 0;
								dsdxb_i[dim] = 0;
								dsdxa_j[dim] = 0;
								dsdxb_j[dim] = 0;
								dtdxa_i[dim] =0;
								dtdxb_i[dim] = v[dim]*cInv;
								dtdxa_j[dim] = ( v[dim]*( 2*cInv*tN - 1) - w[dim] - u[dim] )*cInv;
								dtdxb_j[dim] = ( w[dim] + u[dim] -  2*cInv*tN*v[dim] )*cInv;
							}
							break;
						case 4:
							for( size_t dim=0 ; dim<dimension ; ++dim ) {
								dtdxa_i[dim] = 0;
								dtdxb_i[dim] = 0;
								dtdxa_j[dim] = 0;
								dtdxb_j[dim] = 0;
								dsdxa_i[dim] = 0;
								dsdxb_i[dim] = 0;
								dsdxa_j[dim] = 0;
								dsdxb_j[dim] = 0;
							}
							break;
						case 5:
							for( size_t dim=0 ; dim<dimension ; ++dim ) {
								dsdxa_i[dim] = ( w[dim] - u[dim]*(1+2*d*aInv) )*aInv;
								dsdxb_i[dim] = (2*d*u[dim]*aInv - w[dim] )*aInv;
								dsdxa_j[dim] = u[dim]*aInv; 
								dsdxb_j[dim] = 0;
								dtdxa_i[dim] = 0;
								dtdxb_i[dim] = 0;
								dtdxa_j[dim] = 0;
								dtdxb_j[dim] = 0;
							}
							break;
						case 6:
							for( size_t dim=0 ; dim<dimension ; ++dim ) {
								dtdxa_i[dim] = 0;
								dtdxb_i[dim] = 0;
								dtdxa_j[dim] = 0;
								dtdxb_j[dim] = 0;
								dsdxa_i[dim] = ( w[dim] - v[dim] - u[dim]*( 2*sN*aInv + 1) )*aInv;
								dsdxb_i[dim] = (v[dim] + 2*tN*u[dim]*aInv - w[dim] )*aInv;
								dsdxa_j[dim] = 0;
								dsdxb_j[dim] = u[dim]*aInv;
							}
							break;
						default:
							std::cerr << "LagranginCigarNew::derivs(): " <<flag << "is not a valid option!\n";
							exit(-1);
				}
				
				double fac = (2*B - dist)*std::sqrt(2*B - dist) ;
				double potential= fac*(2*B - dist);
				fac *= parameter(1)/(dist);
				dydt[i][variableIndex(0,0)] += potential;
				dydt[j][variableIndex(0,0)] += potential;
				double sFac = sc*a + d - tc*b;
				double tFac =tc*c - e - sc*b;
				for( size_t dim=0 ; dim<dimension ; dim++ ) {
					//double zetaInv = 1/(0.0094*(B+A));//based on \zeta=3\pi\eta(B+A)
					double celliPoint1Update = fac*( (1 - sc)*dP[dim] +
																					 dsdxa_i[dim]*sFac + 
																					 dtdxa_i[dim]*tFac );
					
					double celliPoint2Update = fac*( sc*dP[dim] +
																					 dsdxb_i[dim]*sFac + 
																					 dtdxb_i[dim]*tFac );
					dydt[i][varIndex + dim] += celliPoint1Update;
					dydt[i][varIndex + dim + dimension] += celliPoint2Update;
					
					
					dydt[i][variableIndex(0,1)] += (2*B-dist>0) ? (2*B-dist) : 0;
					dydt[i][variableIndex(0,2)] += celliPoint1Update*celliPoint1Update+
						celliPoint2Update*celliPoint2Update+
						2*celliPoint1Update*celliPoint2Update;
					dydt[i][variableIndex(0,3)] += std::sqrt(
						celliPoint1Update*celliPoint1Update+
						celliPoint2Update*celliPoint2Update+
						2*celliPoint1Update*celliPoint2Update);
					dydt[i][variableIndex(0,4)] += celliPoint1Update*celliPoint1Update+
						celliPoint2Update*celliPoint2Update;
          
					
					double celljPoint1Update=fac*( (tc -1)*dP[dim] +
																				 dsdxa_j[dim]*sFac + 
																				 dtdxa_j[dim]*tFac );
					double celljPoint2Update=fac*( -tc*dP[dim] +
																				 dsdxb_j[dim]*sFac + 
																				 dtdxb_j[dim]*tFac );
					//zetaInv = 1/(0.0094*(B+ANeigh));
					dydt[j][varIndex + dim] +=celljPoint1Update; 
					dydt[j][varIndex + dim + dimension] +=celljPoint2Update; 
					
					
					dydt[j][variableIndex(0,1)] += (2*B-dist>0) ? (2*B-dist) : 0;
					dydt[j][variableIndex(0,2)] += celljPoint1Update*celljPoint1Update+
						celljPoint2Update*celljPoint2Update+
						2*celljPoint1Update*celljPoint2Update;

					dydt[j][variableIndex(0,3)] += 
						std::sqrt(
							celljPoint1Update*celljPoint1Update+
							celljPoint2Update*celljPoint2Update+
							2*celljPoint1Update*celljPoint2Update);
					dydt[j][variableIndex(0,4)] += celljPoint1Update*celljPoint1Update+
							celljPoint2Update*celljPoint2Update;
				}
			}
    }
  }
	/*static int count = 0;
	if (count % 1000000 ==0) {
		typedef std::map<int,int>::const_iterator CI;
		int total = 0;
		for (CI p=flagStat.begin(); p!=flagStat.end(); ++p) 
			total += p->second;
		std::cerr << count << "\n";
		for (CI p=flagStat.begin(); p!=flagStat.end(); ++p) {
			std::cerr << p->first << " " << p->second << " " << total <<"\n";
		}
		std::cerr << "\n";
			}
			count++;*/
}


//!Constructor for the lagrangianCigar class
LagrangianCigarFriction::
LagrangianCigarFriction(std::vector<double> &paraValue, 
		     std::vector< std::vector<size_t> > &indValue ) {
  
  //Do some checks on the parameters and variable indeces
  //////////////////////////////////////////////////////////////////////
  if( paraValue.size() != 4 ) {
    std::cerr << "LagrangianCigarFriction::LagrangianCigarFriction() "
	      << "Uses three parameters K_Internal, K_External b and gamma\n";
    exit(0);
  }
  if( indValue.size()>1 ||
      (indValue.size() == 1 && indValue[0].size() != 1) ) {
    std::cerr << "LagrangianCigarFriction::LagrangianCigarFriction() "
	      << "One variable index can be used. Stores Force.\n";
    exit(0);
  }
  if( paraValue.size()==2 )
    paraValue.push_back(1.0);
  //Set the variable values
  //////////////////////////////////////////////////////////////////////
  setId("lagrangianCigarFriction");
  setParameter(paraValue);  
  setVariableIndex(indValue);
  
  //Set the parameter identities
  //////////////////////////////////////////////////////////////////////
  std::vector<std::string> tmp( numParameter() );
  tmp[0] = "K_int";
  tmp[1] = "K_ext";
  tmp[2] = "b";
  tmp[3] = "gamma";
  setParameterId( tmp );
}

void LagrangianCigarFriction::
derivs(Compartment &compartment,size_t varIndex,
       std::vector< std::vector<double> > &y,
       std::vector< std::vector<double> > &dydt ) {
  
  size_t i=compartment.index(); 
//Set saving column to zero (UGLY)
  if( numVariableIndexLevel() && i==0 )
    for( size_t ii=0 ; ii<y.size() ; ii++ )
      y[ii][variableIndex(0,0)] = 0.0;
  
  if( compartment.numTopologyVariable() != 9 ) {
    std::cerr << "LagrangianCigar::derivs() "
	      << "Wrong number of topology variables.\n";
    exit(-1);
  }

  size_t dimension=2;    
  double B=parameter(2);
  //Get data columns to be used
  std::vector<double> u(dimension),v(dimension),w(dimension);
  std::vector<double> dsdxa_i(dimension), dsdxb_i(dimension), 
    dsdxa_j(dimension), dsdxb_j(dimension), 
    dtdxa_i(dimension), dtdxb_i(dimension), 
    dtdxa_j(dimension), dtdxb_j(dimension); 
 
  for( size_t dim=0 ; dim<dimension ; ++dim )
    u[dim] = y[i][varIndex+dim+dimension]-y[i][varIndex+dim];
  double a = 0.0;
  for( size_t dim=0 ; dim<dimension ; ++dim )   
    a += u[dim]*u[dim];
  
        
//contribution from the spring between the two points
  double A= std::sqrt(a);
  for( size_t dim =0 ; dim<dimension ; dim++ ) {
    double derivative =
      parameter(0)*(y[i][varIndex + dim]-y[i][varIndex + dim + dimension])*
      (A - y[i][compartment.numTopologyVariable() - 1 + varIndex])/(A);
    //Update position derivative (ONLY ONCE!!)
    dydt[i][varIndex + dim] += y[i][varIndex + 2*dimension + dim];
    dydt[i][varIndex + dim + dimension] += y[i][varIndex + 3*dimension + dim];    
//Update velocity derivs from internal spring
    dydt[i][varIndex + dim + 2*dimension] += -derivative - parameter(3)*y[i][varIndex + 2*dimension +dim];
    dydt[i][varIndex + dim + 3*dimension] += derivative - parameter(3)*y[i][varIndex + dim + 3*dimension];  
  }
  
  if( compartment.numNeighbor()<1 ) return;
  //Loop over neighbors
  for( size_t k=0 ; k<compartment.numNeighbor() ; k++ ) {
    //Set neighbor parameters
    size_t j = compartment.neighbor(k);
    if( j > i) {
      for( size_t dim=0 ; dim<dimension ; ++dim ){
				v[dim] = y[j][varIndex+dim+dimension]-y[j][varIndex+dim];
				w[dim] = y[i][varIndex+dim]-y[j][varIndex+dim];
      }
      double b=0.0,c=0.0,d=0.0,e=0.0;
      for( size_t dim=0 ; dim<dimension ; ++dim ) {
				b += u[dim]*v[dim];
				c += v[dim]*v[dim];
				d += u[dim]*w[dim];
				e += v[dim]*w[dim];
      } 
      double D=a*c-b*b;
      double sN,sD=D,tN,tD=D;
      double aInv = 1/a, cInv = 1/c, DInv = 1/D;
      int flag = -1;
      if( D<1e-10 ) {
	//almost parallel, use yi[d] to prevent division by zero
				sN = 0.0;
				sD = 1.0;
				tN = e;
				tD = c;
				flag = 1;
      }
      else {
				sN = b*e-c*d;
				tN = a*e-b*d;
				flag = 2;
				if( sN<0.0 ) {
					sN = 0.0;
					tN = e;
					tD = c;
					flag = 1;
				}
				else if( sN>sD ) {
					sN = sD;
					tN = e+b;
					tD = c;
					flag = 3;
				}
      }
      if( tN<0.0 ) {
				tN = 0.0;
				flag = 4;
				if( -d<0 )
					sN = 0.0;
				else if( -d>a )
					sN = sD;
				else {
					flag = 5;
					sN = -d;
					sD = a;
				}
      }
      else if( tN>tD ) {
				tN = tD;
				flag = 4;
				if( (-d+b)<0.0 )
					sN = 0.0;
				else if( (-d+b)>a )
					sN = sD;
				else {
					sN = -d+b;
					sD = a;
					flag = 6;
				}
      }
      double sc = sN/sD; 
      double tc = tN/tD;
      
      std::vector<double> dP(dimension);
      
      for( size_t dim=0 ; dim<dimension ; ++dim ) {
				dP[dim] = w[dim]+sc*u[dim]-tc*v[dim];
      }
      double dist = std::sqrt(dP[0]*dP[0] + dP[1]*dP[1]);
			
			//dist = std::sqrt(dist);
      
      //only if it's a true neighbor
      if(2*B - dist > 0){
				//checks which derivative that should be used
				switch (flag) {
					case 1:
						for( size_t dim=0 ; dim<dimension ; ++dim ) {
							dsdxa_i[dim] = 0;
							dsdxb_i[dim] = 0;
							dsdxa_j[dim] = 0;
							dsdxb_j[dim] = 0;
							dtdxa_i[dim] = v[dim]*cInv;
							dtdxb_i[dim] = 0;
							dtdxa_j[dim] = (v[dim]*(2*e*cInv - 1) - w[dim] )*cInv;
							dtdxb_j[dim] = (w[dim] - 2*e*v[dim]*cInv)*cInv;
						}
						break;
					case 2:
						for( size_t dim=0 ; dim<dimension ; ++dim ) {
							dsdxa_i[dim] = ( v[dim]*( b*(1  - 2*sN*DInv) -e ) + u[dim]*c*( 2*sN*DInv - 1 ) + c*w[dim] )*DInv;
							dsdxb_i[dim] = ( v[dim]*( e + 2*b*sN*DInv ) - c*( w[dim] + 2*sN*DInv*u[dim] )  )*DInv;
							dsdxa_j[dim] = ( v[dim]*( 2*d - b + 2*a*sN*DInv ) + u[dim]*( c - e - 2*b*sN*DInv ) - b*w[dim] )*DInv;
							dsdxb_j[dim] = ( -v[dim]*( 2*d + 2*a*sN*DInv ) + u[dim]*( e + 2*b*sN*DInv ) + b*w[dim] )*DInv;
							
							dtdxa_i[dim] = ( v[dim]*( a + d - 2*b*tN*DInv ) + u[dim]*( 2*c*tN*DInv - 2*e - b) + b*w[dim] )*DInv;
							dtdxb_i[dim] = ( v[dim]*( 2*b*tN*DInv - d ) + u[dim]*( 2*e - 2*c*tN*DInv) - b*w[dim] )*DInv;
							dtdxa_j[dim] = ( v[dim]*(2*a*tN*DInv - a) + u[dim]*( d + b - 2*tN*b*DInv ) - a*w[dim] )*DInv;
							dtdxb_j[dim] = ( a*( w[dim]-v[dim]*2*tN*DInv) + u[dim]*(2*b*tN*DInv -d) )*DInv;
						}
						break;
					case 3:
						for( size_t dim=0 ; dim<dimension ; ++dim ) {
							dsdxa_i[dim] = 0;
							dsdxb_i[dim] = 0;
							dsdxa_j[dim] = 0;
							dsdxb_j[dim] = 0;
							dtdxa_i[dim] =0;
							dtdxb_i[dim] = v[dim]*cInv;
							dtdxa_j[dim] = ( v[dim]*( 2*cInv*tN - 1) - w[dim] - u[dim] )*cInv;
							dtdxb_j[dim] = ( w[dim] + u[dim] -  2*cInv*tN*v[dim] )*cInv;
						}
						break;
					case 4:
						for( size_t dim=0 ; dim<dimension ; ++dim ) {
							dtdxa_i[dim] = 0;
							dtdxb_i[dim] = 0;
							dtdxa_j[dim] = 0;
							dtdxb_j[dim] = 0;
							dsdxa_i[dim] = 0;
							dsdxb_i[dim] = 0;
							dsdxa_j[dim] = 0;
							dsdxb_j[dim] = 0;
						}
						break;
					case 5:
						for( size_t dim=0 ; dim<dimension ; ++dim ) {
							dsdxa_i[dim] = ( w[dim] - u[dim]*(1+2*d*aInv) )*aInv;
							dsdxb_i[dim] = (2*d*u[dim]*aInv - w[dim] )*aInv;
							dsdxa_j[dim] = u[dim]*aInv; 
							dsdxb_j[dim] = 0;
							dtdxa_i[dim] = 0;
							dtdxb_i[dim] = 0;
							dtdxa_j[dim] = 0;
							dtdxb_j[dim] = 0;
	  }
						break;
					case 6:
						for( size_t dim=0 ; dim<dimension ; ++dim ) {
							dtdxa_i[dim] = 0;
							dtdxb_i[dim] = 0;
							dtdxa_j[dim] = 0;
							dtdxb_j[dim] = 0;
							dsdxa_i[dim] = ( w[dim] - v[dim] - u[dim]*( 2*sN*aInv + 1) )*aInv;
							dsdxb_i[dim] = (v[dim] + 2*tN*u[dim]*aInv - w[dim] )*aInv;
							dsdxa_j[dim] = 0;
							dsdxb_j[dim] = u[dim]*aInv;
						}
						break;
					default:
						std::cerr << "LagrangianCigarFriction::derivs(): " <<flag << "is not a valid option!\n";
						exit(-1);
				}
				
				double fac = parameter(1)*pow(2*B - dist, 1.5)/dist;
				double sFac = sc*a + d - tc*b;
				double tFac =tc*c - e - sc*b;
				for( size_t dim=0 ; dim<dimension ; dim++ ) {
					//first point for cell i
					dydt[i][varIndex + 2*dimension + dim] +=
						fac*( (1 - sc)*dP[dim] +
									dsdxa_i[dim]*sFac + 
									dtdxa_i[dim]*tFac );
					//second point
					dydt[i][varIndex + dim + 3*dimension] +=
						fac*( sc*dP[dim] +
									dsdxb_i[dim]*sFac + 
									dtdxb_i[dim]*tFac );
					//add force to total force on i
					y[i][variableIndex(0,0)] += 
						dydt[i][varIndex + 2*dimension + dim]* dydt[i][varIndex + 2*dimension + dim] +
						dydt[i][varIndex + dim + 3*dimension]* dydt[i][varIndex + dim + 3*dimension];
					//first point for cell j
					dydt[j][varIndex + 2*dimension + dim] +=
						fac*( (tc -1)*dP[dim] +
									dsdxa_j[dim]*sFac + 
									dtdxa_j[dim]*tFac );
					//Second point
					dydt[j][varIndex + dim + 3*dimension] +=
						fac*( -tc*dP[dim] +
									dsdxb_j[dim]*sFac + 
									dtdxb_j[dim]*tFac );
					//add force to total force on j
					y[j][variableIndex(0,0)] += 
						dydt[j][varIndex + 2*dimension + dim]*dydt[j][varIndex + 2*dimension + dim]+
						dydt[j][varIndex + dim + 3*dimension]* dydt[j][varIndex + dim + 3*dimension];
				}
      }
    }
  }
}

//!Constructor for the LagrangianWallCigar class
LagrangianWallCigarNew::LagrangianWallCigarNew(std::vector<double> &paraValue, 
				   std::vector< std::vector<size_t> > 
				   &indValue ) {
  
  //Do some checks on the parameters and variable indeces
  //////////////////////////////////////////////////////////////////////
  if( paraValue.size()!=6 ) {
    std::cerr << "LagrangianWallCigarNew::LagrangianWallCigarNew()"
	      << "Uses seven parameters K_force x_1, y_1, x_2, "
	      << "y_2 and b.\n";
    exit(0);
  }
  if( indValue.size()!=1 && indValue[0].size() != 5 ) {
    std::cerr << "LagrangianWallCigarNew::LagrangianWallCigarNew()"
							<< "Five variable indices must be used (index for force)\n";
    exit(0);
  }
  //Set the variable values
  //////////////////////////////////////////////////////////////////////
  setId("lagrangianWallCigarNew");
  setParameter(paraValue);  
  setVariableIndex(indValue);
  
  //Set the parameter identities
  //////////////////////////////////////////////////////////////////////
  std::vector<std::string> tmp( numParameter() );
  tmp[0] = "K_force";
  tmp[1] = "x_1";
  tmp[2] = "y_1";
  tmp[3] = "x_2";
  tmp[4] = "y_2";
  tmp[5] = "b";
  setParameterId( tmp );
}


void LagrangianWallCigarNew::
derivs(Compartment &compartment,size_t varIndex,
       std::vector< std::vector<double> > &y,
       std::vector< std::vector<double> > &dydt ) {
  
  size_t i=compartment.index(); 
  //Set saving column to zero (UGLY)
  //if( numVariableIndexLevel() && i==0 )
	//for( size_t ii=0 ; ii<y.size() ; ii++ )
	//  y[ii][variableIndex(0,0)] = 0.0;
  
  if( compartment.numTopologyVariable() != 5 ) {
    std::cerr << "LagrangianWallCigar::derivs() "
	      << "Wrong number of topology variables.\n";
    exit(-1);
  }
  size_t dimension=2;    
  //Wallproperties
  std::vector<double> wallPos1(dimension), wallPos2(dimension);
  wallPos1[0]=parameter(1);
  wallPos1[1]=parameter(2); 
  wallPos2[0]=parameter(3); 
  wallPos2[1]=parameter(4);
  
  double B=parameter(5);
  //Get data columns to be used
  std::vector<double> u(dimension),v(dimension),w(dimension);
  std::vector<double> dsdxa_i(dimension), dsdxb_i(dimension), 
    dtdxa_i(dimension), dtdxb_i(dimension); 
  for( size_t dim=0 ; dim<dimension ; ++dim ){
    u[dim] = y[i][varIndex+dim+dimension]-y[i][varIndex+dim];
    v[dim] = wallPos2[dim] - wallPos1[dim];
    w[dim] = y[i][varIndex+dim] - wallPos1[dim];
  }
  double a = 0.0;     double b=0.0,c=0.0,d=0.0,e=0.0;
  for( size_t dim=0 ; dim<dimension ; ++dim ) {
    a += u[dim]*u[dim];
    b += u[dim]*v[dim];
    c += v[dim]*v[dim];
    d += u[dim]*w[dim];
    e += v[dim]*w[dim];
  } 
  //double A = std::sqrt(a);
	double D=a*c-b*b;
  double sN,sD=D,tN,tD=D;
  double aInv = 1/a, cInv = 1/c, DInv = 1/D;
  int flag = -1;
  if( D<1e-10 ) {
    //almost parallel, use yi[d] to prevent division by zero
    sN = 0.0;
    sD = 1.0;
    tN = e;
    tD = c;
    flag = 0;
  }
  else {
    sN = b*e-c*d;
    tN = a*e-b*d;
    flag = 1;
    if( sN<0.0 ) {
      sN = 0.0;
      tN = e;
      tD = c;
      flag = 2;
    }
    else if( sN>sD ) {
      sN = sD;
      tN = e+b;
      tD = c;
      flag = 3;
    }
  }
  if( tN<0.0 ) {
    tN = 0.0;
    flag = 4;
    if( -d<0 )
      sN = 0.0;
    else if( -d>a )
      sN = sD;
    else {
      flag = 5;
      sN = -d;
      sD = a;
    }
  }
  else if( tN>tD ) {
    tN = tD;
    flag = 4;
    if( (-d+b)<0.0 )
      sN = 0.0;
    else if( (-d+b)>a )
      sN = sD;
    else {
      sN = -d+b;
      sD = a;
      flag = 6;
    }
  }
  double sc = sN/sD; 
  double tc = tN/tD;
  
  std::vector<double> dP(dimension);
  
  for( size_t dim=0 ; dim<dimension ; ++dim ) {
    dP[dim] = w[dim]+sc*u[dim]-tc*v[dim];
  }
  double dist = std::sqrt(dP[0]*dP[0] + dP[1]*dP[1]);
  
  //break if it's not a true neighbor
  if(2*B - dist <= 0)
    return;
  
  //checks which derivative that should be used
  switch (flag) {
  case 0:
    for( size_t dim=0 ; dim<dimension ; ++dim ) {
      dsdxa_i[dim] = 0;
      dsdxb_i[dim] = 0;
      dtdxa_i[dim] = v[dim]*cInv;
      dtdxb_i[dim] = 0;
    }
    break;
  case 1:
    for( size_t dim=0 ; dim<dimension ; ++dim ) {
      dsdxa_i[dim] = ( v[dim]*( b*(1  - 2*sN*DInv) -e ) + u[dim]*c*( 2*sN*DInv - 1 ) + c*w[dim] )*DInv;
      dsdxb_i[dim] = ( v[dim]*( e + 2*b*sN*DInv ) - c*( w[dim] + 2*sN*DInv*u[dim] )  )*DInv;
      
      dtdxa_i[dim] = ( v[dim]*( a + d - 2*b*tN*DInv ) + u[dim]*( 2*c*tN*DInv - 2*e - b) + b*w[dim] )*DInv;
      dtdxb_i[dim] = ( v[dim]*( 2*b*tN*DInv - d ) + u[dim]*( 2*e - 2*c*tN*DInv) - b*w[dim] )*DInv;
    }
    break;
  case 2:
    for( size_t dim=0 ; dim<dimension ; ++dim ) {
      dsdxa_i[dim] = 0;
      dsdxb_i[dim] = 0;
      
      dtdxa_i[dim] = v[dim]*cInv;
      dtdxb_i[dim] = 0;
    }
    break;
  case 3:
    for( size_t dim=0 ; dim<dimension ; ++dim ) {
      dsdxa_i[dim] = 0;
      dsdxb_i[dim] = 0;
      
      dtdxa_i[dim] =0;
      dtdxb_i[dim] = v[dim]*cInv;
    }
    break;
  case 4:
    for( size_t dim=0 ; dim<dimension ; ++dim ) {
      dtdxa_i[dim] = 0;
      dtdxb_i[dim] = 0;
      
      dsdxa_i[dim] = 0;
      dsdxb_i[dim] = 0;
    }
    break;
  case 5:
    for( size_t dim=0 ; dim<dimension ; ++dim ) {
      dsdxa_i[dim] = ( w[dim] - u[dim]*(1+2*d*aInv) )*aInv;
      dsdxb_i[dim] = (2*d*u[dim]*aInv - w[dim] )*aInv;
      
      dtdxa_i[dim] = 0;
      dtdxb_i[dim] = 0;
    }
    break;
  case 6:
    for( size_t dim=0 ; dim<dimension ; ++dim ) {
      dtdxa_i[dim] = 0;
      dtdxb_i[dim] = 0;
      
      dsdxa_i[dim] = ( w[dim] - v[dim] - u[dim]*( 2*sN*aInv + 1) )*aInv;
      dsdxb_i[dim] = (v[dim] + 2*tN*u[dim]*aInv - w[dim] )*aInv;
    }
    break;
  default:
    std::cerr << "LagranginCigarNew::derivs(): " <<flag << "is not a valid option!\n";
    exit(-1);
  }
	double potential=parameter(0)*pow(2*B - dist, 1.5);
	double fac = potential/(dist);
  double sFac = sc*a + d - tc*b;
  double tFac =tc*c - e - sc*b;
  for( size_t dim=0 ; dim<dimension ; dim++ ) {
		//double zetaInv = 1/(0.0094*(B+A));
    double point1Update = fac*( (1 - sc)*dP[dim] +
															 dsdxa_i[dim]*sFac + 
															 dtdxa_i[dim]*tFac );
		double point2Update = fac*( sc*dP[dim] +
															 dsdxb_i[dim]*sFac + 
															 dtdxb_i[dim]*tFac );     
		dydt[i][varIndex + dim] += point1Update;
    dydt[i][varIndex + dim + dimension] += point2Update;
		
	}
}

//!Constructor for the MultipleWallCigar class
MultipleWallCigar::MultipleWallCigar(std::vector<double> &paraValue, 
				     std::vector< std::vector<size_t> > 
				     &indValue ) {
  
  //Do some checks on the parameters and variable indeces
  //////////////////////////////////////////////////////////////////////
  if( paraValue.size()<6 || (paraValue.size()-2)%4 ) {
    std::cerr << "MultipleWallCigar::MultipleWallCigar()"
	      << "Uses at least six parameters K_force b and x_1, y_1, x_2, "
	      << "y_2 [x_1 y_1 x_2 y_2,...].\n";
    exit(0);
  }
  if( indValue.size()!=1 || indValue[0].size() != 5 ) {
    std::cerr << "MultipleWallCigar::MultipleWallCigar()"
							<< "Five variable index can be used (index for force)\n";
    exit(0);
  }
  //Set the variable values
  //////////////////////////////////////////////////////////////////////
  setId("MultipleWallCigar");
  setParameter(paraValue);  
  setVariableIndex(indValue);
  
  //Set the parameter identities
  //////////////////////////////////////////////////////////////////////
  std::vector<std::string> tmp( numParameter() );
  tmp[0] = "K_force";
  tmp[1] = "b";
  size_t numWall = (paraValue.size()-2)/4;
  for( size_t i=0 ; i<numWall ; ++i ) {
    size_t add = i*4;
    tmp[2+add] = "x_1";
    tmp[3+add] = "y_1";
    tmp[4+add] = "x_2";
    tmp[5+add] = "y_2";
  }
  setParameterId( tmp );
}

void MultipleWallCigar::
derivs(Compartment &compartment,size_t varIndex,
       std::vector< std::vector<double> > &y,
       std::vector< std::vector<double> > &dydt ) {
  
  size_t i=compartment.index(); 
  //Set saving column to zero if compartment index=0 (UGLY)
  //if( numVariableIndexLevel() && i==0 )
	//for( size_t ii=0 ; ii<y.size() ; ++ii )
	//  y[ii][variableIndex(0,0)] = 0.0;
  
  assert( compartment.numTopologyVariable() == 5 );
  size_t dimension=2;    
  
  size_t numNeigh = compartment.numNeighborAtLevel(1);

  if( !numNeigh ) return;//Only update if compartment has wall neighs
  
  //Get data columns to be used
  std::vector<double> u(dimension),v(dimension),w(dimension);
  std::vector<double> dsdxa_i(dimension), dsdxb_i(dimension), 
    dtdxa_i(dimension), dtdxb_i(dimension); 
  for( size_t dim=0 ; dim<dimension ; ++dim )
    u[dim] = y[i][varIndex+dim+dimension]-y[i][varIndex+dim];
  
  
  for( size_t k=0 ; k<numNeigh ; ++k ) {
    size_t wallI = compartment.neighborAtLevel(1,k);
    //Wallproperties
    std::vector<double> wallPos1(dimension), wallPos2(dimension);
    size_t add = 4*wallI;
    wallPos1[0]=parameter(2+add);
    wallPos1[1]=parameter(3+add); 
    wallPos2[0]=parameter(4+add); 
    wallPos2[1]=parameter(5+add);
    
    double B=parameter(1);
    for( size_t dim=0 ; dim<dimension ; ++dim ){
      v[dim] = wallPos2[dim] - wallPos1[dim];
      w[dim] = y[i][varIndex+dim] - wallPos1[dim];
    }
    double a = 0.0,b=0.0,c=0.0,d=0.0,e=0.0;
    for( size_t dim=0 ; dim<dimension ; ++dim ) {
      a += u[dim]*u[dim];
      b += u[dim]*v[dim];
      c += v[dim]*v[dim];
      d += u[dim]*w[dim];
      e += v[dim]*w[dim];
    }
		//double A = std::sqrt(a);
    double D=a*c-b*b;
    double sN,sD=D,tN,tD=D;
    double aInv = 1/a, cInv = 1/c, DInv = 1/D;
    int flag = -1;
    if( D<1e-10 ) {
      //almost parallel, use yi[d] to prevent division by zero
      sN = 0.0;
      sD = 1.0;
      tN = e;
      tD = c;
      flag = 0;
    }
    else {
      sN = b*e-c*d;
      tN = a*e-b*d;
      flag = 1;
      if( sN<0.0 ) {
				sN = 0.0;
				tN = e;
				tD = c;
				flag = 2;
      }
      else if( sN>sD ) {
				sN = sD;
				tN = e+b;
				tD = c;
				flag = 3;
      }
    }
    if( tN<0.0 ) {
      tN = 0.0;
      flag = 4;
      if( -d<0 )
				sN = 0.0;
      else if( -d>a )
				sN = sD;
      else {
				flag = 5;
				sN = -d;
				sD = a;
      }
    }
    else if( tN>tD ) {
      tN = tD;
      flag = 4;
      if( (-d+b)<0.0 )
				sN = 0.0;
      else if( (-d+b)>a )
				sN = sD;
      else {
				sN = -d+b;
				sD = a;
				flag = 6;
      }
    }
    double sc = sN/sD; 
    double tc = tN/tD;
    
    std::vector<double> dP(dimension);
  
    for( size_t dim=0 ; dim<dimension ; ++dim ) {
      dP[dim] = w[dim]+sc*u[dim]-tc*v[dim];
    }
    double dist = std::sqrt(dP[0]*dP[0] + dP[1]*dP[1]);
    if (dist <= 0) {
			std::cerr << i << " " << " " << dist << "\n";
			std::cerr<< y[i][0] << " " << y[i][1] << "\n";
			std::cerr<< y[i][2] << " " << y[i][3] << "\n\n";
			//std::cerr<< y[j][0] << " " << y[j][1] << "\n";
			//std::cerr<< y[j][2] << " " << y[j][3] << "\n";
			
			mySignal::myExit();
		}
    //break if it's not a true neighbor
    if(2*B - dist <= 0) {
      continue;
    }
    //checks which derivative that should be used
    switch (flag) {
			case 0:
				for( size_t dim=0 ; dim<dimension ; ++dim ) {
					dsdxa_i[dim] = 0;
					dsdxb_i[dim] = 0;
					
					dtdxa_i[dim] = v[dim]*cInv;
					dtdxb_i[dim] = 0;
				}
				break;
			case 1:
				for( size_t dim=0 ; dim<dimension ; ++dim ) {
					dsdxa_i[dim] = ( v[dim]*( b*(1  - 2*sN*DInv) -e ) + u[dim]*c*( 2*sN*DInv - 1 ) + c*w[dim] )*DInv;
					dsdxb_i[dim] = ( v[dim]*( e + 2*b*sN*DInv ) - c*( w[dim] + 2*sN*DInv*u[dim] )  )*DInv;
					
					dtdxa_i[dim] = ( v[dim]*( a + d - 2*b*tN*DInv ) + u[dim]*( 2*c*tN*DInv - 2*e - b) + b*w[dim] )*DInv;
					dtdxb_i[dim] = ( v[dim]*( 2*b*tN*DInv - d ) + u[dim]*( 2*e - 2*c*tN*DInv) - b*w[dim] )*DInv;
				}
				break;
			case 2:
				for( size_t dim=0 ; dim<dimension ; ++dim ) {
					dsdxa_i[dim] = 0;
					dsdxb_i[dim] = 0;
					
					dtdxa_i[dim] = v[dim]*cInv;
					dtdxb_i[dim] = 0;
				}
				break;
			case 3:
				for( size_t dim=0 ; dim<dimension ; ++dim ) {
					dsdxa_i[dim] = 0;
					dsdxb_i[dim] = 0;

					dtdxa_i[dim] =0;
					dtdxb_i[dim] = v[dim]*cInv;
				}
				break;
			case 4:
				for( size_t dim=0 ; dim<dimension ; ++dim ) {
					dsdxa_i[dim] = 0;
					dsdxb_i[dim] = 0;
					
					dtdxa_i[dim] = 0;
					dtdxb_i[dim] = 0;	
				}
				break;
    case 5:
      for( size_t dim=0 ; dim<dimension ; ++dim ) {
				dsdxa_i[dim] = ( w[dim] - u[dim]*(1+2*d*aInv) )*aInv;
				dsdxb_i[dim] = (2*d*u[dim]*aInv - w[dim] )*aInv;
				
				dtdxa_i[dim] = 0;
				dtdxb_i[dim] = 0;
      }
      break;
			case 6:
				for( size_t dim=0 ; dim<dimension ; ++dim ) {
					dsdxa_i[dim] = ( w[dim]-v[dim]-u[dim]*( 2*sN*aInv + 1) )*aInv;
					dsdxb_i[dim] = (v[dim] + 2*tN*u[dim]*aInv - w[dim] )*aInv;
					
					dtdxa_i[dim] = 0;
					dtdxb_i[dim] = 0;	
				}
				break;
			default:
				std::cerr << "MultipleWallCigar::derivs(): " <<flag << "is not a valid option!\n";
				exit(-1);
    }
    
		double fac = (2*B-dist)*std::sqrt(2*B-dist);
		double potential= fac*(2*B - dist);
		fac *= parameter(0)/(dist);
		dydt[i][variableIndex(0,0)] += potential;
		double sFac = sc*a + d - tc*b;
    double tFac =tc*c - e - sc*b;
    for( size_t dim=0 ; dim<dimension ; dim++ ) {
			//double zetaInv = 1/(0.0094*(B+A));
      double point1Update = fac*( (1 - sc)*dP[dim] +
																 dsdxa_i[dim]*sFac + 
																 dtdxa_i[dim]*tFac );
			double point2Update =fac*( sc*dP[dim] +
																 dsdxb_i[dim]*sFac + 
																 dtdxb_i[dim]*tFac );
			dydt[i][varIndex + dim] += point1Update;
			
      dydt[i][varIndex + dim + dimension] += point2Update;
			
			//dydt[i][variableIndex(0,0)] += point1Update*point1Update+point2Update*point2Update;
			
			dydt[i][variableIndex(0,1)] += (2*B-dist>0) ? (2*B-dist) : 0;
			dydt[i][variableIndex(0,2)] += point1Update*point1Update+
				point2Update*point2Update+2*point1Update*point2Update;
			dydt[i][variableIndex(0,3)] += std::sqrt(point1Update*point1Update+ 
																							 point2Update*point2Update+
																							 2*point1Update*point2Update);
			dydt[i][variableIndex(0,4)] += point1Update*point1Update+
				point2Update*point2Update;
    }
		
  }
}


//!Constructor for the lagrangianCigar class
CigarAdhesion::
CigarAdhesion(std::vector<double> &paraValue, 
		     std::vector< std::vector<size_t> > &indValue ) {
  
  //Do some checks on the parameters and variable indeces
  //////////////////////////////////////////////////////////////////////
  if( paraValue.size() != 4 ) {
    std::cerr << "CigarAdhesion::CigarAdhesion() "
	      << "Uses four parameters V_0, steepness(s), b and dx\n";
    exit(0);
  }
  if( indValue.size() != 1 && indValue[0].size() != 1 ) {
    std::cerr << "CigarAdhesion::CigarAdhesion()"
	      << "One variable index must be used. Stores Force.\n";
    exit(0);
  }
  if( paraValue.size()==2 )
    paraValue.push_back(1.0);
  //Set the variable values
  //////////////////////////////////////////////////////////////////////
  setId("cigarAdhesion");
  setParameter(paraValue);  
  setVariableIndex(indValue);
  
  //Set the parameter identities
  //////////////////////////////////////////////////////////////////////
  std::vector<std::string> tmp( numParameter() );
  tmp[0] = "V_0";
  tmp[1] = "s";
	tmp[2] = "b";
	tmp[3] = "dx";
  setParameterId( tmp );
}

void CigarAdhesion::
derivs(Compartment &compartment,size_t varIndex,
       std::vector< std::vector<double> > &y,
       std::vector< std::vector<double> > &dydt ) {
  
  size_t i=compartment.index(); 
  //Set saving column to zero (UGLY)
  if( numVariableIndexLevel() && i==0 )
	for( size_t ii=0 ; ii<y.size() ; ii++ )
	  y[ii][variableIndex(0,0)] = 0.0;
  
  if( compartment.numTopologyVariable() != 5 ) {
    std::cerr << "CigarAdhesion::derivs() "
							<< "Wrong number of topology variables.\n";
    exit(-1);
  }
	
  size_t dimension=2;
	double V_0= parameter(0);
	double s=parameter(1);
  double B=parameter(2);
	double dx = parameter(3);
  //Get data columns to be used
  std::vector<double> u(dimension),v(dimension),w(dimension);
  std::vector<double> dsdxa_i(dimension), dsdxb_i(dimension), 
    dsdxa_j(dimension), dsdxb_j(dimension), 
    dtdxa_i(dimension), dtdxb_i(dimension), 
    dtdxa_j(dimension), dtdxb_j(dimension); 
	
  for( size_t dim=0 ; dim<dimension ; ++dim )
    u[dim] = y[i][varIndex+dim+dimension]-y[i][varIndex+dim];
  double a = 0.0;
  for( size_t dim=0 ; dim<dimension ; ++dim )   
    a += u[dim]*u[dim];

  assert( a>0.0 );
	//double A = std::sqrt(a);

  if( compartment.numNeighbor()<1 ) return;
  //Loop over neighbors
  for( size_t k=0 ; k<compartment.numNeighbor() ; k++ ) {
    //Set neighbor parameters
    size_t j = compartment.neighbor(k);
    if( j > i) {
      for( size_t dim=0 ; dim<dimension ; ++dim ){
				v[dim] = y[j][varIndex+dim+dimension]-y[j][varIndex+dim];
				w[dim] = y[i][varIndex+dim]-y[j][varIndex+dim];
      }
      double b=0.0,c=0.0,d=0.0,e=0.0;
      for( size_t dim=0 ; dim<dimension ; ++dim ) {
				b += u[dim]*v[dim];
				c += v[dim]*v[dim];
				d += u[dim]*w[dim];
				e += v[dim]*w[dim];
      } 
			
			assert( c>0.0 );
			//double ANeigh = std::sqrt(c);
			double D=a*c-b*b;
      double sN,sD=D,tN,tD=D;
      double aInv = 1.0/a, cInv = 1.0/c, DInv = 1.0/D;
      int flag = -1;
      if( D<1e-10 ) {
				//almost parallel, use yi[d] to prevent division by zero
				sN = 0.0;
				sD = 1.0;
				tN = e;
				tD = c;
				flag = 1;
      }
      else {
				sN = b*e-c*d;
				tN = a*e-b*d;
				flag = 2;
				if( sN<0.0 ) {
					sN = 0.0;
					tN = e;
					tD = c;
					flag = 1;
				}
				else if( sN>sD ) {
					sN = sD;
					tN = e+b;
					tD = c;
					flag = 3;
				}
      }
      if( tN<0.0 ) {
				tN = 0.0;
				flag = 4;
				if( -d<0 )
					sN = 0.0;
				else if( -d>a )
					sN = sD;
				else {
					flag = 5;
					sN = -d;
					sD = a;
				}
      }
      else if( tN>tD ) {
				tN = tD;
				flag = 4;
				if( (-d+b)<0.0 )
					sN = 0.0;
				else if( (-d+b)>a )
					sN = sD;
				else {
					sN = -d+b;
					sD = a;
					flag = 6;
				}
      }
			assert( sD != 0.0 );
			assert( tD != 0.0 );
      double sc = sN/sD; 
      double tc = tN/tD;
      //assert( sc != NaN && sc != Inf && sc != -Inf );
      //assert( tc != NaN && tc != Inf && tc != -Inf );
			
      std::vector<double> dP(dimension);
      
			double dist=0.0;
      for( size_t dim=0 ; dim<dimension ; ++dim ) {
				dP[dim] = w[dim]+sc*u[dim]-tc*v[dim];
				dist += dP[dim]*dP[dim];

      }

			//assert( dist>0.0 );
      if (dist <= 0) {
				std::cerr << i << " " << j << " " << dist << "\n";
				std::cerr<< y[i][0] << " " << y[i][1] << "\n";
				std::cerr<< y[i][2] << " " << y[i][3] << "\n\n";
				std::cerr<< y[j][0] << " " << y[j][1] << "\n";
				std::cerr<< y[j][2] << " " << y[j][3] << "\n";
							
				exit(-1);
			}
			dist = std::sqrt(dist);
			assert( dist>0.0 );

      //only if it's a true neighbor
      //if(2*B - dist > 0){

				//checks which derivative that should be used
				switch (flag) {
						case 1:
							for( size_t dim=0 ; dim<dimension ; ++dim ) {
								dsdxa_i[dim] = 0;
								dsdxb_i[dim] = 0;
								dsdxa_j[dim] = 0;
								dsdxb_j[dim] = 0;
								dtdxa_i[dim] = v[dim]*cInv;
								dtdxb_i[dim] = 0;
								dtdxa_j[dim] = (v[dim]*(2*e*cInv - 1) - w[dim] )*cInv;
								dtdxb_j[dim] = (w[dim] - 2*e*v[dim]*cInv)*cInv;
							}
							break;
						case 2:
							//assert( DInv !=NaN && DInv != Inf && DInv != -Inf );
							assert( D != 0.0 );
							for( size_t dim=0 ; dim<dimension ; ++dim ) {
								dsdxa_i[dim] = ( v[dim]*( b*(1  - 2*sN*DInv) -e ) + u[dim]*c*( 2*sN*DInv - 1 ) + c*w[dim] )*DInv;
								dsdxb_i[dim] = ( v[dim]*( e + 2*b*sN*DInv ) - c*( w[dim] + 2*sN*DInv*u[dim] )  )*DInv;
								dsdxa_j[dim] = ( v[dim]*( 2*d - b + 2*a*sN*DInv ) + u[dim]*( c - e - 2*b*sN*DInv ) - b*w[dim] )*DInv;
								dsdxb_j[dim] = ( -v[dim]*( 2*d + 2*a*sN*DInv ) + u[dim]*( e + 2*b*sN*DInv ) + b*w[dim] )*DInv;
								
								dtdxa_i[dim] = ( v[dim]*( a + d - 2*b*tN*DInv ) + u[dim]*( 2*c*tN*DInv - 2*e - b) + b*w[dim] )*DInv;
								dtdxb_i[dim] = ( v[dim]*( 2*b*tN*DInv - d ) + u[dim]*( 2*e - 2*c*tN*DInv) - b*w[dim] )*DInv;
								dtdxa_j[dim] = ( v[dim]*(2*a*tN*DInv - a) + u[dim]*( d + b - 2*tN*b*DInv ) - a*w[dim] )*DInv;
								dtdxb_j[dim] = ( a*( w[dim]-v[dim]*2*tN*DInv) + u[dim]*(2*b*tN*DInv -d) )*DInv;
							}
							break;
					case 3:
							for( size_t dim=0 ; dim<dimension ; ++dim ) {
								dsdxa_i[dim] = 0;
								dsdxb_i[dim] = 0;
								dsdxa_j[dim] = 0;
								dsdxb_j[dim] = 0;
								dtdxa_i[dim] =0;
								dtdxb_i[dim] = v[dim]*cInv;
								dtdxa_j[dim] = ( v[dim]*( 2*cInv*tN - 1) - w[dim] - u[dim] )*cInv;
								dtdxb_j[dim] = ( w[dim] + u[dim] -  2*cInv*tN*v[dim] )*cInv;
							}
							break;
						case 4:
							for( size_t dim=0 ; dim<dimension ; ++dim ) {
								dtdxa_i[dim] = 0;
								dtdxb_i[dim] = 0;
								dtdxa_j[dim] = 0;
								dtdxb_j[dim] = 0;
								dsdxa_i[dim] = 0;
								dsdxb_i[dim] = 0;
								dsdxa_j[dim] = 0;
								dsdxb_j[dim] = 0;
							}
							break;
						case 5:
							for( size_t dim=0 ; dim<dimension ; ++dim ) {
								dsdxa_i[dim] = ( w[dim] - u[dim]*(1+2*d*aInv) )*aInv;
								dsdxb_i[dim] = (2*d*u[dim]*aInv - w[dim] )*aInv;
								dsdxa_j[dim] = u[dim]*aInv; 
								dsdxb_j[dim] = 0;
								dtdxa_i[dim] = 0;
								dtdxb_i[dim] = 0;
								dtdxa_j[dim] = 0;
								dtdxb_j[dim] = 0;
							}
							break;
						case 6:
							for( size_t dim=0 ; dim<dimension ; ++dim ) {
								dtdxa_i[dim] = 0;
								dtdxb_i[dim] = 0;
								dtdxa_j[dim] = 0;
								dtdxb_j[dim] = 0;
								dsdxa_i[dim] = ( w[dim] - v[dim] - u[dim]*( 2*sN*aInv + 1) )*aInv;
								dsdxb_i[dim] = (v[dim] + 2*tN*u[dim]*aInv - w[dim] )*aInv;
								dsdxa_j[dim] = 0;
								dsdxb_j[dim] = u[dim]*aInv;
							}
							break;
						default:
							std::cerr << "cigarAdhesion::derivs(): " <<flag << "is not a valid option!\n";
							exit(-1);
				}
				double potential=V_0/(1+std::exp((2*B-dist+dx)/s));
				double fac = -V_0*std::exp((2*B-dist+dx)/s)/(s*dist*(1+std::exp((2*B-dist+dx)/s))*(1+std::exp((2*B-dist+dx)/s)));
				double sFac = sc*a + d - tc*b;
				double tFac =tc*c - e - sc*b;
				for( size_t dim=0 ; dim<dimension ; dim++ ) {
					//double zetaInv = 1/(0.0094*(B+A));//based on \zeta = 3*pi*\eta*(B+A)
					double celliPoint1Update = fac*( (1 - sc)*dP[dim] +
																					 dsdxa_i[dim]*sFac + 
																					 dtdxa_i[dim]*tFac );
					
					double celliPoint2Update = fac*( sc*dP[dim] +
																					 dsdxb_i[dim]*sFac + 
																					 dtdxb_i[dim]*tFac );
					dydt[i][varIndex + dim] += celliPoint1Update;
					dydt[i][varIndex + dim + dimension] += celliPoint2Update;
					y[i][variableIndex(0,0)] += potential;
          //dydt[i][variableIndex(0,0)] += celliPoint1Update*celliPoint1Update+
					//	celliPoint2Update*celliPoint2Update;
					//zetaInv = 1/(0.0094*(B+ANeigh));
					double celljPoint1Update=fac*( (tc -1)*dP[dim] +
																				 dsdxa_j[dim]*sFac + 
																				 dtdxa_j[dim]*tFac );
					double celljPoint2Update=fac*( -tc*dP[dim] +
																				 dsdxb_j[dim]*sFac + 
																				 dtdxb_j[dim]*tFac );
					dydt[j][varIndex + dim] +=celljPoint1Update; 
					dydt[j][varIndex + dim + dimension] +=celljPoint2Update; 
					y[j][variableIndex(0,0)] += potential;
				}
		}
	}
}


//!Constructor for the lagrangianCigar class
WallCigar3D::
WallCigar3D(std::vector<double> &paraValue, 
						std::vector< std::vector<size_t> > &indValue ) {
  
  //Do some checks on the parameters and variable indeces
  //////////////////////////////////////////////////////////////////////
  if( paraValue.size() < 12 || (paraValue.size()-3)%3 ) {
    std::cerr << "WallCigar3D::WallCigar3D() "
	      << "Uses at least 12 k,b,D,x_1,x_2 and x_3 \n";
    exit(0);
  }
  if( indValue.size() != 1 || indValue[0].size() != 3 ) {
    std::cerr << "WallCigar3D::WallCigar3D() "
							<< "Three variable indices must be used. Stores potential, overlap, and F**2.\n";
    exit(0);
  }
  if( paraValue.size()==2 )
    paraValue.push_back(1.0);
  //Set the variable values
  //////////////////////////////////////////////////////////////////////
  setId("wallCigarNew");
  setParameter(paraValue);  
  setVariableIndex(indValue);
  
  //Set the parameter identities
  //////////////////////////////////////////////////////////////////////
  std::vector<std::string> tmp( numParameter() );
	tmp[0] = "K_force";
  tmp[1] = "b";
	tmp[2] = "D";
  size_t numPoints = (paraValue.size()-3)/3;
  for( size_t i=0 ; i<numPoints ; ++i ) {
    size_t add = i*3;
    tmp[3+add] = "x_1";
    tmp[4+add] = "y_1";
    tmp[5+add] = "z_1";
  }
	setParameterId( tmp );
  
   //Check that all points is in a plane and that the polygon is convex
  ///////////////////////////////////////////////////////////////////
	using myVector3::Vector3;
	std::vector< Vector3 > point(numPoints);
	for( size_t p=0 ; p<numPoints ; ++p ) {
		size_t add = p*3;
		point[p].setVector3(parameter(3+add), parameter(4+add), parameter(5+add));
	}
	//use two vectors two span the plane via the normal n
	Vector3 n=cross(point[1]-point[0], point[2]-point[0]);
	n*=(1/sqrt(dot(n,n)));
	for (size_t p = 3; p<numPoints; ++p) {
		if (dot(n,point[p]-point[0])!=0) {
			std::cerr << "WallCigar3D() "  
								<< point[p](0) << " " << point[p](1) << " " << point[p](2)
								<< " is not in plane (" << dot(n,point[3]-point[0]) << ")\n";
			exit(-1);	
		}
	}
	int signOfCross=0; 
	for( size_t p=0 ; p<numPoints ; ++p ) {
		Vector3 u = point[(p+1)%numPoints]-point[p];
		Vector3 v = point[(p+2)%numPoints]-point[(p+1)%numPoints];
		
		if (!p) {
			signOfCross = cross(u,v)(2)>=0 ? 1:-1;
			std::cerr << signOfCross << "\n";
		}
			else {
			if (signOfCross != ( cross(u,v)(2)>=0 ? 1:-1) ) {
				std::cerr << "WallCigar3D(): Polygon is not convex!\n";
				std::cerr << u(0) << " " << u(1) << " " << u(2) << "\n"
									<< v(0) << " " << v(1) << " " << v(2) << "\n"
									<< signOfCross << " " << (cross(u,v)(2)>=0 ? 1:-1) << "\n";
				exit(-1);
			}
		}
	}
}



void WallCigar3D::
derivs(Compartment &compartment,size_t varIndex,
       std::vector< std::vector<double> > &y,
       std::vector< std::vector<double> > &dydt ) {
	using myVector3::Vector3;
  size_t i=compartment.index(); 
	
	assert( compartment.numTopologyVariable() == 7 );
	size_t dimension=(compartment.numTopologyVariable()-1)/2;    
  double k=parameter(0);
	double B=parameter(1);
	double D=parameter(2);
 
	size_t numPoints = static_cast<size_t>( (numParameter()-3)/3 );
	
	std::vector< Vector3 > point(numPoints);
 
  for( size_t p=0 ; p<numPoints ; ++p ) {
		size_t add = p*3;
		point[p].setVector3(parameter(3+add), parameter(4+add), parameter(5+add));
	}
  //use two vectors two span the plane via the normal n
	Vector3 n=cross(point[1]-point[0], point[2]-point[0]);
	n*=(1/sqrt(dot(n,n)));
	//cellproperties
	Vector3 x_a(y[i][varIndex],y[i][varIndex+1],y[i][varIndex+2]);
	Vector3 x_b(y[i][varIndex+3],y[i][varIndex+4],y[i][varIndex+5]);
  //find distance to plane
	double t_a = dot((point[0]-x_a),n);
	double t_b = dot((point[0]-x_b),n);
	double t= fabs(t_a)<fabs(t_b)? t_a : t_b;
	size_t index = fabs(t_a)<fabs(t_b)? varIndex : varIndex+dimension;
	Vector3 x_close = fabs(t_a)<fabs(t_b)? x_a : x_b;
	double distToPlane=fabs(t);
	if(distToPlane <= 0) {
		mySignal::myExit();
	}
	//if close enough and if the interaction point lies within object: update
	if (distToPlane<B+D) {
		Vector3 intPoint = x_close + t*n;
		Vector3 object = point[1]-point[0];
		Vector3 objToIntPoint = intPoint-point[0];
		int signOfCross=cross(objToIntPoint,object)(2)>=0 ? 1:-1;
		bool flag = true;
		for( size_t p=1 ; p<numPoints ; ++p ) {
			object = point[(p+1)%numPoints]-point[p];
			objToIntPoint = intPoint-point[p];
			if (!p)
				signOfCross= cross(objToIntPoint,object)(2)>=0 ? 1:-1;
			else if (signOfCross != (cross(objToIntPoint,object)(2)>=0 ? 1:-1) )
				flag = false;
		}
		//Update
		if (flag) {
			//double A =sqrt( dot(x_b-x_a,x_b-x_a) );
			//double zetaInv = 1/(0.0094*(B+A));
			double fac =k*(B+D-distToPlane)*sqrt(B+D-distToPlane);
			double potential = fac*(B+D-distToPlane);
			Vector3 update = n*(fac/(2*distToPlane));
			if(t>0)
				update*=(-1.0);
			for( size_t dim=0 ; dim<dimension ; dim++ ) {
				dydt[i][index+dim]+= update(dim);
			}
			dydt[i][variableIndex(0,0)] += potential;
			dydt[i][variableIndex(0,1)] += (B+D-distToPlane>0) ? (B+D-distToPlane) : 0;
			dydt[i][variableIndex(0,2)] += dot(update,update);
		}
	}
}

