///
/// Filename     : ellipticSpring.h
/// Description  : Classes describing springs between spherical cells
/// Author(s)    : Henrik Jonsson (henrik@thep.lu.se)
/// Created      : March 2010
/// Revision     : $Id:$
///
#ifndef ELLIPTICSPRING_H
#define ELLIPTICSPRING_H

#include"../baseReaction.h"
#include<cmath>

class EllipticRepulsion : public BaseReaction {
  
 public:
  
	///
	/// @brief Main constructor
	///
	/// This is the main constructor which sets the parameters and variable
	/// indices that defines the reaction.
	///
	/// @param paraValue vector with parameters
	///
	/// @param indValue vector of vectors with variable indices
	///
	/// @see BaseReaction::createReaction(std::vector<double> &paraValue,...)
	///
  EllipticRepulsion(std::vector<double> &paraValue, 
	     std::vector< std::vector<size_t> > &indValue );
  
	///
	/// @brief Derivative function for this reaction class
	///
	/// @see BaseReaction::derivs(Compartment &compartment,size_t species,...)
	///
  void derivs(Compartment &compartment,size_t varIndex,
	      std::vector< std::vector<double> > &y,
	      std::vector< std::vector<double> > &dydt );
};

class NormalEllipticRepulsion : public BaseReaction {
  
 public:
  
	///
	/// @brief Main constructor
	///
	/// This is the main constructor which sets the parameters and variable
	/// indices that defines the reaction.
	///
	/// @param paraValue vector with parameters
	///
	/// @param indValue vector of vectors with variable indices
	///
	/// @see BaseReaction::createReaction(std::vector<double> &paraValue,...)
	///
  NormalEllipticRepulsion(std::vector<double> &paraValue, 
	     std::vector< std::vector<size_t> > &indValue );
  
	///
	/// @brief Derivative function for this reaction class
	///
	/// @see BaseReaction::derivs(Compartment &compartment,size_t species,...)
	///
  void derivs(Compartment &compartment,size_t varIndex,
	      std::vector< std::vector<double> > &y,
	      std::vector< std::vector<double> > &dydt );
};

//!A mechanical cell-cell interaction for ellipses
class SpringAsymmetricEllipse : public BaseReaction {
  
 public:
  
	///
	/// @brief Main constructor
	///
	/// This is the main constructor which sets the parameters and variable
	/// indices that defines the reaction.
	///
	/// @param paraValue vector with parameters
	///
	/// @param indValue vector of vectors with variable indices
	///
	/// @see BaseReaction::createReaction(std::vector<double> &paraValue,...)
	///
  SpringAsymmetricEllipse(std::vector<double> &paraValue, 
			  std::vector< std::vector<size_t> > &indValue );
  
	///
	/// @brief Derivative function for this reaction class
	///
	/// @see BaseReaction::derivs(Compartment &compartment,size_t species,...)
	///
  void derivs(Compartment &compartment,size_t varIndex,
	      std::vector< std::vector<double> > &y,
	      std::vector< std::vector<double> > &dydt );
};

//!WallSpringEllipstic simulates an unmovable wall (for a cell colony)
class WallSpringElliptic : public BaseReaction {
  
 public:
  
	///
	/// @brief Main constructor
	///
	/// This is the main constructor which sets the parameters and variable
	/// indices that defines the reaction.
	///
	/// @param paraValue vector with parameters
	///
	/// @param indValue vector of vectors with variable indices
	///
	/// @see BaseReaction::createReaction(std::vector<double> &paraValue,...)
	///
  WallSpringElliptic(std::vector<double> &paraValue, 
	     std::vector< std::vector<size_t> > &indValue );
  
	///
	/// @brief Derivative function for this reaction class
	///
	/// @see BaseReaction::derivs(Compartment &compartment,size_t species,...)
	///
  void derivs(Compartment &compartment,size_t varIndex,
	      std::vector< std::vector<double> > &y,
	      std::vector< std::vector<double> > &dydt );
};

//!SpringElasticWallEllipse simulates an elastic wall (for a cell colony)
class SpringElasticWallEllipse : public BaseReaction {
  
 private:

  std::vector< std::vector<double> > xW_;
  std::vector< std::vector<double> > dxW_;
  double dRelax_;
  
 public:
  
	///
	/// @brief Main constructor
	///
	/// This is the main constructor which sets the parameters and variable
	/// indices that defines the reaction.
	///
	/// @param paraValue vector with parameters
	///
	/// @param indValue vector of vectors with variable indices
	///
	/// @see BaseReaction::createReaction(std::vector<double> &paraValue,...)
	///
  SpringElasticWallEllipse(std::vector<double> &paraValue, 
			   std::vector< std::vector<size_t> > &indValue );
  
	///
	/// @brief Derivative function for this reaction class
	///
	/// @see BaseReaction::derivs(Compartment &compartment,size_t species,...)
	///
  void derivs(Compartment &compartment,size_t varIndex,
	      std::vector< std::vector<double> > &y,
	      std::vector< std::vector<double> > &dydt );

  void update(double h, double t,
	      std::vector< std::vector<double> > &y);
  void print(std::ofstream &os);
};

//!GravitaionElliptic simulates a constant force in (g_x,g_y) direction 
class GravitationElliptic : public BaseReaction {
  
 public:
  
	///
	/// @brief Main constructor
	///
	/// This is the main constructor which sets the parameters and variable
	/// indices that defines the reaction.
	///
	/// @param paraValue vector with parameters
	///
	/// @param indValue vector of vectors with variable indices
	///
	/// @see BaseReaction::createReaction(std::vector<double> &paraValue,...)
	///
  GravitationElliptic(std::vector<double> &paraValue, 
											std::vector< std::vector<size_t> > &indValue );
  
	///
	/// @brief Derivative function for this reaction class
	///
	/// @see BaseReaction::derivs(Compartment &compartment,size_t species,...)
	///
  void derivs(Compartment &compartment,size_t varIndex,
							std::vector< std::vector<double> > &y,
							std::vector< std::vector<double> > &dydt );
};

#endif //ELLIPTICSPRING_H
