//
// Filename     : sphereForce.h
// Description  : Classes describing forces applied to spherical cells
// Author(s)    : Henrik Jonsson (henrik@thep.lu.se)
// Created      : April 2010
// Revision     : $Id:$
//
#ifndef SPHEREFORCE_H
#define SPHEREFORCE_H

#include"../baseReaction.h"
#include<cmath>

namespace Sphere {

  ///
  /// @brief Adds a force in given direction for spherical compartments
  ///
  /// The position update is given by
  /// 
  /// @f[ \frac{dx_i}{dt} = c \frac{x_i-<x_i>}{|x_i-<x_i>|}@f]
  ///
  /// if linearFlag=0, and
  ///
  /// @f[ \frac{dx_i}{dt} = c (x_i-<x_i>)@f]
  ///
  /// if linearFlag=1, where @f$x_i@f$ is the position in given direction i and @f$<x_i>@f$
  /// is the average position in this direction.
  ///
  /// In a model file the reaction is defined as
  ///
  /// @verbatim
  /// Sphere::forceDirection 2 1 1
  /// c linearFlag
  /// i
  /// @endverbatim
  ///
  class ForceDirection : public BaseReaction {
    
  public:
    
    ///
    /// @brief Main constructor
    ///
    /// This is the main constructor which sets the parameters and variable
    /// indices that defines the reaction.
    ///
    /// @param paraValue vector with parameters
    ///
    /// @param indValue vector of vectors with variable indices
    ///
    /// @see BaseReaction::createReaction(std::vector<double> &paraValue,...)
    ///
    ForceDirection(std::vector<double> &paraValue, 
		   std::vector< std::vector<size_t> > &indValue );
    
    ///
    /// @brief Derivative function for this reaction class
    ///
    /// @see BaseReaction::derivs(Compartment &compartment,size_t species,...)
    ///
    void derivs(Compartment &compartment,size_t varIndex,
		std::vector< std::vector<double> > &y,
		std::vector< std::vector<double> > &dydt );
  };

  ///
  /// @brief Adds a force in direction given by cell vector for spherical compartments
  ///
  /// The position update is given by
  /// 
  /// @f[ \frac{dx}{dt} = \Theta_i c d_x @f]
  ///
  /// where @f$x@f$ is the position vector and @f$d@f$ the cellular direction
  /// vector. @f$\Theta@f$ is a flag set for individual cells.
  ///
  /// In a model file the reaction is defined as
  ///
  /// @verbatim
  /// Sphere::forceCellDirection 1 1 2
  /// c
  /// theta x0
  /// @endverbatim
  ///
  class ForceCellDirection : public BaseReaction {
    
  public:
    
    ///
    /// @brief Main constructor
    ///
    /// This is the main constructor which sets the parameters and variable
    /// indices that defines the reaction.
    ///
    /// @param paraValue vector with parameters
    ///
    /// @param indValue vector of vectors with variable indices
    ///
    /// @see BaseReaction::createReaction(std::vector<double> &paraValue,...)
    ///
    ForceCellDirection(std::vector<double> &paraValue, 
		       std::vector< std::vector<size_t> > &indValue );
    
    ///
    /// @brief Derivative function for this reaction class
    ///
    /// @see BaseReaction::derivs(Compartment &compartment,size_t species,...)
    ///
    void derivs(Compartment &compartment,size_t varIndex,
		std::vector< std::vector<double> > &y,
		std::vector< std::vector<double> > &dydt );
  };

  ///
  /// @brief Adds a force in direction given by cell velocity vector for spherical 
	/// compartments
  ///
  /// The position update is given by
  /// 
  /// @f[ \frac{dx}{dt} = \Theta_i c v_x @f]
  ///
  /// where @f$x@f$ is the position vector and @f$v@f$ the cellular
  /// velocity direction
  /// vector. @f$\Theta@f$ is a flag set for individual cells.
  ///
  /// In a model file the reaction is defined as
  ///
  /// @verbatim
  /// sphere::forceVelocityDirection 1 1 2
  /// c
  /// theta x0
  /// @endverbatim
  ///
  class ForceVelocityDirection : public BaseReaction {
    
  public:
    
    ///
    /// @brief Main constructor
    ///
    /// This is the main constructor which sets the parameters and variable
    /// indices that defines the reaction.
    ///
    /// @param paraValue vector with parameters
    ///
    /// @param indValue vector of vectors with variable indices
    ///
    /// @see BaseReaction::createReaction(std::vector<double> &paraValue,...)
    ///
    ForceVelocityDirection(std::vector<double> &paraValue, 
			   std::vector< std::vector<size_t> > &indValue );
    
    ///
    /// @brief Derivative function for this reaction class
    ///
    /// @see BaseReaction::derivs(Compartment &compartment,size_t species,...)
    ///
    void derivs(Compartment &compartment,size_t varIndex,
		std::vector< std::vector<double> > &y,
		std::vector< std::vector<double> > &dydt );
  };

  ///
  /// @brief Adds a radial force to move cells out(in)wards from origo
  ///
	/// The tissue grows from vertex movement radially outwards. The update can be
	///
	/// @f[ \frac{dr}{dt} = p_{0} @f] (if @f$ p_1=0 @f$ linFlag) or 
	/// @f[ \frac{dr}{dt} = p_{0} r @f] (if @f$ p_{1}=1 @f$) 
	///
	/// @f$ p_{0} @f$ is the rate (@f$ k_{r} @f$), 
	/// @f$ p_{1} @f$ {0,1} is a flag determining which function to be used 
	/// (@f$ r_{pow} @f$). 
  ///
  /// In a model file the reaction is defined as
  ///
  /// @verbatim
  /// sphere::forceRadial 2 0
  /// k_r linFlag
  /// @endverbatim
  ///
  class ForceRadial : public BaseReaction {
    
  public:
    
    ///
    /// @brief Main constructor
    ///
    /// This is the main constructor which sets the parameters and variable
    /// indices that defines the reaction.
    ///
    /// @param paraValue vector with parameters
    ///
    /// @param indValue vector of vectors with variable indices
    ///
    /// @see BaseReaction::createReaction(std::vector<double> &paraValue,...)
    ///
    ForceRadial(std::vector<double> &paraValue, 
								std::vector< std::vector<size_t> > &indValue );
    
    ///
    /// @brief Derivative function for this reaction class
    ///
    /// @see BaseReaction::derivs(Compartment &compartment,size_t species,...)
    ///
    void derivs(Compartment &compartment,size_t varIndex,
								std::vector< std::vector<double> > &y,
								std::vector< std::vector<double> > &dydt );
  };
  
} //namespace Sphere

#endif // SPHEREFORCE_H
