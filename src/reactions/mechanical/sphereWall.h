//
// Filename     : sphereWall.h
// Description  : Classes describing wall forces applied to spherical cells
// Author(s)    : Henrik Jonsson (henrik@thep.lu.se)
// Created      : April 2010
// Revision     : $Id:$
//
#ifndef SPHEREWALL_H
#define SPHEREWALL_H

#include"../baseReaction.h"
#include<cmath>

namespace Sphere {

  ///
  /// @brief Adds a force from a spherical 'wall' for spherical compartments
  ///
  /// The position update is given by a spring force
  /// 
  /// @f[ \frac{dx}{dt} = k (|x-X|-(r+R))@f]
  ///
  /// where x and r are the position vector and radius of the cellular compartment, and
  /// X and R is the position vector and the radius of the spherical wall. k is the spring
  /// constant.
  /// If the spring constant is positive, the force will act to keep the cell compartments 
  /// outside the sphere and if negative the cells will be kept inside. 
  /// It is assumed that the first variables in a compartment 
  /// describes the position and the next the radius of the spherical cellular compartments.
  ///
  /// In a model file the reaction is defined as
  ///
  /// @verbatim
  /// sphere::sphericalWall 5 0
  /// k X Y Z R
  /// @endverbatim
  ///
  /// where the Z is disregarded in 2D simulations.
  ///
  class SphericalWall : public BaseReaction {
    
  public:
    
    ///
    /// @brief Main constructor
    ///
    /// This is the main constructor which sets the parameters and variable
    /// indices that defines the reaction.
    ///
    /// @param paraValue vector with parameters
    ///
    /// @param indValue vector of vectors with variable indices
    ///
    /// @see BaseReaction::createReaction(std::vector<double> &paraValue,...)
    ///
    SphericalWall(std::vector<double> &paraValue, 
	       std::vector< std::vector<size_t> > &indValue );
    
    ///
    /// @brief Derivative function for this reaction class
    ///
    /// @see BaseReaction::derivs(Compartment &compartment,size_t species,...)
    ///
    void derivs(Compartment &compartment,size_t varIndex,
		std::vector< std::vector<double> > &y,
		std::vector< std::vector<double> > &dydt );
  };
  
} //namespace Sphere

#endif // SPHEREWALL_H
