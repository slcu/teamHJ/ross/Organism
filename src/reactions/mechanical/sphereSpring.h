///
/// Filename     : sphereSpring.h
/// Description  : Classes describing springs between spherical cells
/// Author(s)    : Henrik Jonsson (henrik@thep.lu.se)
/// Created      : March 2010
/// Revision     : $Id:$
///
#ifndef SPHERESPRING_H
#define SPHERESPRING_H

#include"../baseReaction.h"
#include<cmath>

///
/// @brief Spring mechanics between spherical compartments
///
/// The parameters are r_relax which is the fraction of the radii sum
/// determining the relaxation radius, and K_force which decides the strength
/// of the spring (spring constant). The update only applys if the neighbor
/// index is larger. The update needs the size index as a variable index. the
/// update (in all dimensions) are
///
/// @f[ \frac{dx}{dt} = (x-x_n) K_{force}(1-\frac{d}{r}) @f]
///
/// where @f$ d = r_{relax}*(r+r_n)@f$.
///
class Spring : public BaseReaction {
  
 public:
  
	///
	/// @brief Main constructor
	///
	/// This is the main constructor which sets the parameters and variable
	/// indices that defines the reaction.
	///
	/// @param paraValue vector with parameters
	///
	/// @param indValue vector of vectors with variable indices
	///
	/// @see BaseReaction::createReaction(std::vector<double> &paraValue,...)
	///
  Spring(std::vector<double> &paraValue, 
				 std::vector< std::vector<size_t> > &indValue );
  
	///
	/// @brief Derivative function for this reaction class
	///
	/// @see BaseReaction::derivs(Compartment &compartment,size_t species,...)
	///
  void derivs(Compartment &compartment,size_t varIndex,
							std::vector< std::vector<double> > &y,
							std::vector< std::vector<double> > &dydt );
};

///
/// @brief SpringAsymmetric uses an asymmetric(adhesion,repelling) spring
/// between spherical compartments
///
/// The only difference from the Spring class is that an extra parameter
/// defining the strength of the adhesion compared to the repelling spring
/// constant is needed for adhesion @f$ K=K_{force}K_{adhFrac} @f$.
///
class SpringAsymmetric : public BaseReaction {
  
 public:
  
	///
	/// @brief Main constructor
	///
	/// This is the main constructor which sets the parameters and variable
	/// indices that defines the reaction.
	///
	/// @param paraValue vector with parameters
	///
	/// @param indValue vector of vectors with variable indices
	///
	/// @see BaseReaction::createReaction(std::vector<double> &paraValue,...)
	///
  SpringAsymmetric(std::vector<double> &paraValue, 
									 std::vector< std::vector<size_t> > &indValue );
  
	///
	/// @brief Derivative function for this reaction class
	///
	/// @see BaseReaction::derivs(Compartment &compartment,size_t species,...)
	///
  void derivs(Compartment &compartment,size_t varIndex,
	      std::vector< std::vector<double> > &y,
	      std::vector< std::vector<double> > &dydt );
  void derivsWithAbs(Compartment &compartment,size_t varIndex,DataMatrix &y,DataMatrix &dydt,DataMatrix &sdydt);
};

///
/// @brief Defines a truncated(adhesion) spring between spherical compartments
///
/// The \e i and \e j are the cells. Derivs contribution is from
///
/// @f[ \frac{dx_i}{dt} = - div(V) = -div(\sum_j V_{ij}) @f]
///
/// @f[ V_{ij} \propto -T log( 1 + exp((r_{ij}-d_{ij})^2 - \mu) ) @f]
///
/// Caveat: @f$ \mu=(d_ij/3)^2 @f$ and @f$ T=\mu/2 @f$ is chosen ad hoc in
/// this first version. These choices seem to scale quite ok with @f$ d_{ij}
/// @f$ and truncates the spring to give zero velocity at about @f$ 1.5*d_{ij}
/// @f$...*/
///
class SpringTruncated : public BaseReaction {
  
 public:
  
	///
	/// @brief Main constructor
	///
	/// This is the main constructor which sets the parameters and variable
	/// indices that defines the reaction.
	///
	/// @param paraValue vector with parameters
	///
	/// @param indValue vector of vectors with variable indices
	///
	/// @see BaseReaction::createReaction(std::vector<double> &paraValue,...)
	///
  SpringTruncated(std::vector<double> &paraValue, 
		  std::vector< std::vector<size_t> > &indValue );
  
	///
	/// @brief Derivative function for this reaction class
	///
	/// @see BaseReaction::derivs(Compartment &compartment,size_t species,...)
	///
  void derivs(Compartment &compartment,size_t varIndex,
	      std::vector< std::vector<double> > &y,
	      std::vector< std::vector<double> > &dydt );
};

///
/// @brief Adhesion between cells restricted to cells with a molecular content 
///
/// This reaction adds an 'attractive' spring force proportional to the molecular concentration of a species 
/// in the neighboring cells.
///
/// In a model file the reaction is specified as:
///
/// @verbatim
/// springAdhesionRestrictedSphere 2 1 1
/// r_relax K_adh
/// index
/// @endverbatim
///
/// where the r_relax is the factor of the two radii for equilibrium (resting length), K_adh is the strength 
/// of the force, and index is the index of the molecular species to be used.
///
/// @note Since this function only provides an adhesion contribution, it should be used together with another spring force.
///
class SpringAdhesionRestrictedSphere : public BaseReaction {
  
 public:
  
  ///
  /// @brief Main constructor
  ///
  /// This is the main constructor which sets the parameters and variable
  /// indices that defines the reaction.
  ///
  /// @param paraValue vector with parameters
  ///
  /// @param indValue vector of vectors with variable indices
  ///
  /// @see BaseReaction::createReaction(std::vector<double> &paraValue,...)
  ///
  SpringAdhesionRestrictedSphere(std::vector<double> &paraValue, 
				 std::vector< std::vector<size_t> > &indValue );
  
  ///
  /// @brief Derivative function for this reaction class
  ///
  /// @see BaseReaction::derivs(Compartment &compartment,size_t species,...)
  ///
  void derivs(Compartment &compartment,size_t varIndex,
	      std::vector< std::vector<double> > &y,
	      std::vector< std::vector<double> > &dydt );
};

///
/// @brief SpringAsymmetricSphere uses an asymmetric spring between comp. on a sphere
///
class SpringAsymmetricSphere : public BaseReaction {
  
 public:
  
	///
	/// @brief Main constructor
	///
	/// This is the main constructor which sets the parameters and variable
	/// indices that defines the reaction.
	///
	/// @param paraValue vector with parameters
	///
	/// @param indValue vector of vectors with variable indices
	///
	/// @see BaseReaction::createReaction(std::vector<double> &paraValue,...)
	///
  SpringAsymmetricSphere(std::vector<double> &paraValue, 
		   std::vector< std::vector<size_t> > &indValue );
  
	///
	/// @brief Derivative function for this reaction class
	///
	/// @see BaseReaction::derivs(Compartment &compartment,size_t species,...)
	///
  void derivs(Compartment &compartment,size_t varIndex,
	      std::vector< std::vector<double> > &y,
	      std::vector< std::vector<double> > &dydt );
};

//!SpringAsymmetricCylinder uses an asymmetric spring for comp. on a cylinder
class SpringAsymmetricCylinder : public BaseReaction {
  
 public:
  
	///
	/// @brief Main constructor
	///
	/// This is the main constructor which sets the parameters and variable
	/// indices that defines the reaction.
	///
	/// @param paraValue vector with parameters
	///
	/// @param indValue vector of vectors with variable indices
	///
	/// @see BaseReaction::createReaction(std::vector<double> &paraValue,...)
	///
  SpringAsymmetricCylinder(std::vector<double> &paraValue, 
			   std::vector< std::vector<size_t> > &indValue );
  
	///
	/// @brief Derivative function for this reaction class
	///
	/// @see BaseReaction::derivs(Compartment &compartment,size_t species,...)
	///
  void derivs(Compartment &compartment,size_t varIndex,
	      std::vector< std::vector<double> > &y,
	      std::vector< std::vector<double> > &dydt );
};

///
/// @brief A mechanical interaction from springs on a cylinder surface.
///
/// The calculations 
/// are made assuming a 2D cylinder surface world (3D but r is constant).
/// The z-axis is assumed to be along the cylinder 'length'.
///
class SpringAsymmetricSphereCylinder : public BaseReaction {
  
 public:
  
	///
	/// @brief Main constructor
	///
	/// This is the main constructor which sets the parameters and variable
	/// indices that defines the reaction.
	///
	/// @param paraValue vector with parameters
	///
	/// @param indValue vector of vectors with variable indices
	///
	/// @see BaseReaction::createReaction(std::vector<double> &paraValue,...)
	///
  SpringAsymmetricSphereCylinder(std::vector<double> &paraValue, 
				 std::vector< std::vector<size_t> > &indValue );
  
	///
	/// @brief Derivative function for this reaction class
	///
	/// @see BaseReaction::derivs(Compartment &compartment,size_t species,...)
	///
  void derivs(Compartment &compartment,size_t varIndex,
	      std::vector< std::vector<double> > &y,
	      std::vector< std::vector<double> > &dydt );

  void derivsSphere(Compartment &compartment,size_t varIndex,
		    std::vector< std::vector<double> > &y,
		    std::vector< std::vector<double> > &dydt );
  void derivsCylinder(Compartment &compartment,size_t varIndex,
		      std::vector< std::vector<double> > &y,
		      std::vector< std::vector<double> > &dydt );
};

class SphereCylinderWall : public BaseReaction {
  
 public:
  
	///
	/// @brief Main constructor
	///
	/// This is the main constructor which sets the parameters and variable
	/// indices that defines the reaction.
	///
	/// @param paraValue vector with parameters
	///
	/// @param indValue vector of vectors with variable indices
	///
	/// @see BaseReaction::createReaction(std::vector<double> &paraValue,...)
	///
  SphereCylinderWall(std::vector<double> &paraValue, 
		     std::vector< std::vector<size_t> > &indValue );
  
	///
	/// @brief Derivative function for this reaction class
	///
	/// @see BaseReaction::derivs(Compartment &compartment,size_t species,...)
	///
  void derivs(Compartment &compartment,size_t varIndex,
	      std::vector< std::vector<double> > &y,
	      std::vector< std::vector<double> > &dydt );
};


///
/// @brief Model for restricting cells within a parobala shape and spherical primordia
///
/// In a model file the reaction is defined as
///
/// @verbatim
/// parabolaSphereWall 6 0
/// JG
/// @endverbatim
///
class ParabolaSphereWall : public BaseReaction {
  
 public:
  
	///
	/// @brief Main constructor
	///
	/// This is the main constructor which sets the parameters and variable
	/// indices that defines the reaction.
	///
	/// @param paraValue vector with parameters
	///
	/// @param indValue vector of vectors with variable indices
	///
	/// @see BaseReaction::createReaction(std::vector<double> &paraValue,...)
	///
  ParabolaSphereWall(std::vector<double> &paraValue, 
		     std::vector< std::vector<size_t> > &indValue );
  
	///
	/// @brief Derivative function for this reaction class
	///
	/// @see BaseReaction::derivs(Compartment &compartment,size_t species,...)
	///
  void derivs(Compartment &compartment,size_t varIndex,
	      std::vector< std::vector<double> > &y,
	      std::vector< std::vector<double> > &dydt );
};

///
/// @brief Model for restricting cells within a parobala shape and spherical primordia that is 
/// allowed to grow.
///
/// In a model file the reaction is defined as
///
/// @verbatim
/// parabolaSphereWallGrowth 7 0
/// JG
/// @endverbatim
///
class ParabolaSphereWallGrowth : public BaseReaction {
  
 public:
  
  ///
  /// @brief Main constructor
  ///
  /// This is the main constructor which sets the parameters and variable
  /// indices that defines the reaction.
  ///
  /// @param paraValue vector with parameters
  ///
  /// @param indValue vector of vectors with variable indices
  ///
  /// @see BaseReaction::createReaction(std::vector<double> &paraValue,...)
  ///
  ParabolaSphereWallGrowth(std::vector<double> &paraValue, 
			   std::vector< std::vector<size_t> > &indValue );
  
  ///
  /// @brief Derivative function for this reaction class
  ///
  /// @see BaseReaction::derivs(Compartment &compartment,size_t species,...)
  ///
  void derivs(Compartment &compartment,size_t varIndex,
	      std::vector< std::vector<double> > &y,
	      std::vector< std::vector<double> > &dydt );

  ///
  /// @see BaseReaction::update()
  ///
  void update(double h, double t,
	      std::vector< std::vector<double> > &y);

};

class PrimordiumGrowth : public BaseReaction {
  
 public:
  
  ///
  /// @brief Main constructor
  ///
  /// This is the main constructor which sets the parameters and variable
  /// indices that defines the reaction.
  ///
  /// @param paraValue vector with parameters
  ///
  /// @param indValue vector of vectors with variable indices
  ///
  /// @see BaseReaction::createReaction(std::vector<double> &paraValue,...)
  ///
  PrimordiumGrowth(std::vector<double> &paraValue, 
			   std::vector< std::vector<size_t> > &indValue );
  
  ///
  /// @brief Derivative function for this reaction class
  ///
  /// @see BaseReaction::derivs(Compartment &compartment,size_t species,...)
  ///
  void derivs(Compartment &compartment,size_t varIndex,
	      std::vector< std::vector<double> > &y,
	      std::vector< std::vector<double> > &dydt );

  ///
  /// @see BaseReaction::update()
  ///
  void update(double h, double t,
	      std::vector< std::vector<double> > &y);

};


#endif //SPHERESPRING_H
