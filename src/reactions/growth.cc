//
// Filename     : growth.cc
// Description  : Classes describing growth updates
// Author(s)    : Henrik Jonsson (henrik@thep.lu.se)
// Created      : October 2003
// Revision     : $Id: growth.cc 642 2015-07-31 18:48:00Z pau $
//
#include"growth.h"
#include"baseReaction.h"
#include"../organism.h"

GrowthConstant::GrowthConstant(std::vector<double> &paraValue, 
			       std::vector< std::vector<size_t> > 
			      &indValue ) {
  
  // Do some checks on the parameters and variable indeces
  //
  if( paraValue.size()!=1 ) {
    std::cerr << "GrowtConstant::GrowthConstant() "
	      << "Uses only one parameter k_rate\n";
    exit(0);
  }
  if( indValue.size() ) {
    std::cerr << "GrowthConstant::GrowthConstant() "
	      << "No variable index are used.\n";
    exit(0);
  }
  // Set the variable values
  //
  setId("growthConstant");
  setParameter(paraValue);  
  setVariableIndex(indValue);
  
  // Set the parameter identities
  //
  std::vector<std::string> tmp( numParameter() );
  tmp.resize( numParameter() );
  tmp[0] = "k_const";
  setParameterId( tmp );
}

void GrowthConstant::derivs(Compartment &compartment,size_t varIndex,
			    std::vector< std::vector<double> > &y,
			    std::vector< std::vector<double> > &dydt ) {
  
  dydt[compartment.index()][compartment.numTopologyVariable()-1] += 
    parameter(0);
}

void GrowthConstant::derivsWithAbs(Compartment &compartment,size_t varIndex,
			    DataMatrix &y,DataMatrix &dydt,DataMatrix &sdydt) {
   dydt[compartment.index()][compartment.numTopologyVariable()-1] += 
    parameter(0);
}

GrowthExponential::GrowthExponential(std::vector<double> &paraValue, 
				     std::vector< std::vector<size_t> > 
				     &indValue ) {
  
  //Do some checks on the parameters and variable indeces
  //
  if( paraValue.size()!=1 ) {
    std::cerr << "GrowthExponential::GrowthExponential() "
	      << "Uses only one parameter k_exp\n";
    exit(0);
  }
  if( indValue.size() ) {
    std::cerr << "GrowthExponential::GrowthExponential() "
	      << "No variable index are used.\n";
    exit(0);
  }
  //Set the variable values
  //
  setId("growthExponential");
  setParameter(paraValue);  
  setVariableIndex(indValue);
  
  //Set the parameter identities
  //
  std::vector<std::string> tmp( numParameter() );
  tmp.resize( numParameter() );
  tmp[0] = "k_exp";
  setParameterId( tmp );
}

void GrowthExponential::
derivs(Compartment &compartment,size_t varIndex,
       std::vector< std::vector<double> > &y,
       std::vector< std::vector<double> > &dydt )
{  
  dydt[compartment.index()][compartment.numTopologyVariable()-1] += 
    parameter(0)*
    y[compartment.index()][compartment.numTopologyVariable()-1];
}

void GrowthExponential::
derivsWithAbs(Compartment &compartment,size_t species,DataMatrix &y,DataMatrix &dydt,DataMatrix &sdydt) 
{    
  dydt[compartment.index()][compartment.numTopologyVariable()-1] += 
    parameter(0)*
    y[compartment.index()][compartment.numTopologyVariable()-1];
}

GrowthExponentialOne::GrowthExponentialOne(std::vector<double> &paraValue, 
					std::vector< std::vector<size_t> > 
					&indValue ) {
  
  //Do some checks on the parameters and variable indeces
  //
  if( paraValue.size()!=1 ) {
    std::cerr << "GrowthExponential::GrowthExponentialOne() "
							<< "Uses only one parameter k_exp1\n";
    exit(0);
  }
  if( indValue.size() != 1 || indValue[0].size() !=1 ) {
    std::cerr << "GrowthExponential::GrowthExponentialOne() "
							<< "One variable index is used.\n";
    exit(0);
  }
  //Set the variable values
  //
  setId("growthExponentialOne");
  setParameter(paraValue);  
  setVariableIndex(indValue);
  
  //Set the parameter identities
  //
  std::vector<std::string> tmp( numParameter() );
  tmp.resize( numParameter() );
  tmp[0] = "k_exp1";
  setParameterId( tmp );
}

void GrowthExponentialOne::derivs(Compartment &compartment,size_t varIndex,
				  std::vector< std::vector<double> > &y,
				  std::vector< std::vector<double> > &dydt )
{  
  dydt[compartment.index()][compartment.numTopologyVariable()-1] += 
    parameter(0)*
    y[compartment.index()][compartment.numTopologyVariable()-1]*
    y[compartment.index()][variableIndex(0,0)];
}

void GrowthExponentialOne::
derivsWithAbs(Compartment &compartment,size_t species,DataMatrix &y,DataMatrix &dydt,DataMatrix &sdydt) 
{    
  dydt[compartment.index()][compartment.numTopologyVariable()-1] += 
    parameter(0)*
    y[compartment.index()][compartment.numTopologyVariable()-1]*
    y[compartment.index()][variableIndex(0,0)];
}

GrowthExponentialRestricted::
GrowthExponentialRestricted(std::vector<double> &paraValue, 
			    std::vector< std::vector<size_t> > 
			    &indValue ) {
  
  //Do some checks on the parameters and variable indeces
  //
  if( paraValue.size()!=2 ) {
    std::cerr << "GrowthExponentialRestricted::GrowthExponentialRestricted() "
	      << "Uses two parameter k_exp and variable threshold\n";
    exit(0);
  }
  if( indValue.size() != 1 || indValue[0].size() != 1 ) {
    std::cerr << "GrowthExponentialRestricted::GrowthExponentialRestricted() "
	      << "Variable index for the threshold variable is used.\n";
    exit(0);
  }
  //Set the variable values
  //
  setId("growthExponentialRestricted");
  setParameter(paraValue);  
  setVariableIndex(indValue);
  
  //Set the parameter identities
  //
  std::vector<std::string> tmp( numParameter() );
  tmp.resize( numParameter() );
  tmp[0] = "k_exp";
  tmp[1] = "var_threshold";
  setParameterId( tmp );
}

void GrowthExponentialRestricted::
derivs(Compartment &compartment,size_t varIndex,
       std::vector< std::vector<double> > &y,
       std::vector< std::vector<double> > &dydt ) {
  
  varIndex = compartment.numTopologyVariable()-1;
  if( y[compartment.index()][variableIndex(0,0)]>parameter(1) )
    dydt[compartment.index()][varIndex] += parameter(0)*
      y[compartment.index()][varIndex];
}

GrowthExponentialTrunc::
GrowthExponentialTrunc(std::vector<double> &paraValue, 
			    std::vector< std::vector<size_t> > 
			    &indValue ) {
  
  // Do some checks on the parameters and variable indeces
  //
  if( paraValue.size()!=2 ) {
    std::cerr << "GrowthExponentialTrunc::"
	      << "GrowthExponentialTrunc() "
	      << "Uses two parameters k_exp and r_max\n";
    exit(0);
  }
  if( indValue.size() != 0 ) {
    std::cerr << "GrowthExponentialTrunc::"
	      << "GrowthExponentialTrunc() "
	      << "No variable index is used.\n";
    exit(0);
  }
  // Set the variable values
  //
  setId("growthExponentialTrunc");
  setParameter(paraValue);  
  setVariableIndex(indValue);
  
  // Set the parameter identities
  //
  std::vector<std::string> tmp( numParameter() );
  tmp.resize( numParameter() );
  tmp[0] = "k_exp";
  tmp[1] = "r_max";
  setParameterId( tmp );
}

void GrowthExponentialTrunc::
derivs(Compartment &compartment,size_t varIndex,
       std::vector< std::vector<double> > &y,
       std::vector< std::vector<double> > &dydt ) {
  varIndex = compartment.numTopologyVariable()-1;
  dydt[compartment.index()][varIndex] += parameter(0)*
    y[compartment.index()][varIndex]*
    (1-y[compartment.index()][varIndex]/parameter(1));
}


void GrowthExponentialTrunc::
derivsWithAbs(Compartment &compartment,size_t varIndex,DataMatrix &y,DataMatrix &dydt,DataMatrix &sdydt) 
{  
  varIndex = compartment.numTopologyVariable()-1;  
  dydt[compartment.index()][varIndex] += parameter(0)*
    y[compartment.index()][varIndex]*
    (1-y[compartment.index()][varIndex]/parameter(1));
}


GrowthExponentialRestrictedTrunc::
GrowthExponentialRestrictedTrunc(std::vector<double> &paraValue, 
			    std::vector< std::vector<size_t> > 
			    &indValue ) {
  
  // Do some checks on the parameters and variable indeces
  //
  if( paraValue.size()!=3 ) {
    std::cerr << "GrowthExponentialRestrictedTrunc::"
	      << "GrowthExponentialRestrictedTrunc() "
	      << "Uses three parameters k_exp,variable threshold and r_max\n";
    exit(0);
  }
  if( indValue.size() != 1 || indValue[0].size() != 1 ) {
    std::cerr << "GrowthExponentialRestrictedTrunc::"
	      << "GrowthExponentialRestrictedTrunc() "
	      << "Variable index for the threshold variable is used.\n";
    exit(0);
  }
  // Set the variable values
  //
  setId("growthExponentialRestrictedTrunc");
  setParameter(paraValue);  
  setVariableIndex(indValue);
  
  // Set the parameter identities
  //
  std::vector<std::string> tmp( numParameter() );
  tmp.resize( numParameter() );
  tmp[0] = "k_exp";
  tmp[1] = "var_threshold";
  tmp[2] = "r_max";
  setParameterId( tmp );
}

void GrowthExponentialRestrictedTrunc::
derivs(Compartment &compartment,size_t varIndex,
       std::vector< std::vector<double> > &y,
       std::vector< std::vector<double> > &dydt ) {
  
  if( y[compartment.index()][variableIndex(0,0)]>parameter(1) )
    dydt[compartment.index()][compartment.numTopologyVariable()-1] 
			+= parameter(0)*
			y[compartment.index()][compartment.numTopologyVariable()-1]*
      (1-y[compartment.index()][compartment.numTopologyVariable()-1]/
			 parameter(2));
}

Dilution::Dilution(std::vector<double> &paraValue, 
		    std::vector< std::vector<size_t> > 
		    &indValue ) {
  
  // Do some checks on the parameters and variable indeces
  //
  if( paraValue.size() ) {
    std::cerr << "Dilution::Dilution() "
	      << "Does not use any parameters.\n";
    exit(0);
  }
  if( indValue.size() != 1 || indValue[0].size() != 1 ) {
    std::cerr << "Dilution::Dilution() "
	      << "Only variable index of size change is used.\n";
    exit(0);
  }
  // Set the variable values
  //
  setId("dilution");
  setParameter(paraValue);  
  setVariableIndex(indValue);
  
  // Set the parameter identities
  //
  std::vector<std::string> tmp( numParameter() );
  setParameterId( tmp );
}

void Dilution::derivs(Compartment &compartment,size_t species,
		      std::vector< std::vector<double> > &y,
		      std::vector< std::vector<double> > &dydt ) {
  
  double volume,volumeChange,PI=3.1415;
  double r = y[compartment.index()][variableIndex(0,0)];
  double dr = dydt[compartment.index()][variableIndex(0,0)];
  size_t dimension = variableIndex(0,0);
  
  if( dimension==2 ) {
    volume = PI*r*r;
    volumeChange = 2*PI*r*dr;
  }
  else if( dimension==3 ) {
    volume = 4.0*PI*r*r*r/3.0;
    volumeChange = 4*PI*r*r*dr;
  }
  else {
    std::cerr << "Dilution::derivs() wrong dimension for update.\n";
    exit(-1);
  }
  
  if( volume>0. ) 
    dydt[compartment.index()][species] -= y[compartment.index()][species]*
      volumeChange/volume;
  else {
    std::cerr << "Dilution::derivs Division by zero volume.\n";
    exit(-1);
  }  
}

void Dilution::derivsWithAbs(Compartment &compartment,size_t species,
		      std::vector< std::vector<double> > &y,
		      std::vector< std::vector<double> > &dydt,
		      std::vector< std::vector<double> > &sdydt) {
  
  double volume,volumeChange,PI=3.1415;
  double r = y[compartment.index()][variableIndex(0,0)];
  double dr = dydt[compartment.index()][variableIndex(0,0)];
  size_t dimension = variableIndex(0,0);
  
  if( dimension==2 ) {
    volume = PI*r*r;
    volumeChange = 2*PI*r*dr;
  }
  else if( dimension==3 ) {
    volume = 4.0*PI*r*r*r/3.0;
    volumeChange = 4*PI*r*r*dr;
  }
  else {
    std::cerr << "Dilution::derivs() wrong dimension for update.\n";
    exit(-1);
  }
  
  if( volume>0. ) 
    dydt[compartment.index()][species] -= y[compartment.index()][species]*
      volumeChange/volume;
  else {
    std::cerr << "Dilution::derivs Division by zero volume.\n";
    exit(-1);
  }  
}

UpdateVolume::UpdateVolume(std::vector<double> &paraValue, 
		    std::vector< std::vector<size_t> > 
		    &indValue ) {
  
  // Do some checks on the parameters and variable indeces
  //
  if( paraValue.size() ) {
    std::cerr << "UpdateVolume::UpdateVolume() "
	      << "Does not use any parameters." << std::endl;
    exit(EXIT_FAILURE);
  }
  if( indValue.size() != 1 || indValue[0].size() != 1 ) {
    std::cerr << "UpdateVolume::UpdateVolume() "
	      << "Only variable index where the updated volume is stored."
	      << std::endl;
    exit(EXIT_FAILURE);
  }
  // Set the variable values
  //
  setId("UpdateVolume");
  setParameter(paraValue);  
  setVariableIndex(indValue);
  
  // Set the parameter identities
  //
  std::vector<std::string> tmp( numParameter() );
  setParameterId( tmp );
}

void UpdateVolume::
derivs(Compartment &compartment,size_t species,
       std::vector< std::vector<double> > &y,
       std::vector< std::vector<double> > &dydt ) {
  // all updates in update
}
void UpdateVolume::
derivsWithAbs(Compartment &compartment,size_t varIndex,
	      DataMatrix &y,DataMatrix &dydt,DataMatrix &sdydt) {
  // all updates in update
}
void UpdateVolume::
initiate(double t,DataMatrix &y) 
{
  for( size_t i=0; i< y.size(); ++i ){
    y[i][variableIndex(0,0)] = organism()->compartment(i).getVolume(y[i]); 
  }
}
void UpdateVolume::
update(double h, double t,DataMatrix &y) 
{
  for( size_t i=0; i< y.size(); ++i ){
    y[i][variableIndex(0,0)] = organism()->compartment(i).getVolume(y[i]); 
  }
}

EllipticGrowthExponentialTrunc::
EllipticGrowthExponentialTrunc(std::vector<double> &paraValue, 
			       std::vector< std::vector<size_t> > &indValue ) {
  
  // Do some checks on the parameters and variable indeces
  //
  if( paraValue.size()!=3 ) {
    std::cerr << "EllipticGrowtExponentialTrunc::"
	      << "EllipticGrowthExponentialTrunc() "
	      << "Uses three parameters k_growth, DMax,  k_spring\n";
    exit(0);
  }
  if( indValue.size() ) {
    std::cerr << "EllipticGrowthExponentialTrunc::"
	      << "EllipticGrowthExponentialTrunc() "
	      << "No variable index are used.\n";
    exit(0);
  }
  // Set the variable values
  //
  setId("ellipticGrowthExponentialTrunc");
  setParameter(paraValue);  
  setVariableIndex(indValue);
  
  // Set the parameter identities
  //
  std::vector<std::string> tmp( numParameter() );
  tmp.resize( numParameter() );
  tmp[0] = "k_growth";
  tmp[1] = "DMax";
  tmp[2] = "k_spring";
  setParameterId( tmp );
}

void EllipticGrowthExponentialTrunc::
derivs(Compartment &compartment,size_t varIndex,
       std::vector< std::vector<double> > &y,
       std::vector< std::vector<double> > &dydt ) {
  
  if( compartment.numTopologyVariable() != 5 &&
      compartment.numTopologyVariable() != 7 ) {
    std::cerr << "EllipticGrowthExponentialTrunc::derivs() "
	      << "Topological error, " 
	      << compartment.numTopologyVariable() 
	      << " is not equal to 5,7\n";
    exit(-1);
  }
  // List the variable columns used
  size_t i=compartment.index();
  size_t dimension = (compartment.numTopologyVariable()-1)/2;
  std::vector<size_t> x1Col(dimension),x2Col(dimension);
  for( size_t d=0 ; d<dimension ; d++ ) {
    x1Col[d] = d+varIndex;
    x2Col[d] = d+dimension+varIndex;
  }
  size_t dCol = compartment.numTopologyVariable()-1+varIndex;
  
  // derivative contrubution to focal distance  
  double D = y[i][dCol];
  dydt[i][dCol] += parameter(0)*D*( 1. - D / parameter(1) );
  
  // derivative contributions to coordinates  
  double length = 0.0;
  for( size_t d=0 ; d<dimension ; d++ )
    length += (y[i][x2Col[d]]-y[i][x1Col[d]])*
      (y[i][x2Col[d]]-y[i][x1Col[d]]);
  length = std::sqrt( length );
  for( size_t d=0 ; d<dimension ; d++ ) {
    double deriv = parameter(2)*(1.-(D/length))*
      (y[i][x2Col[d]]-y[i][x1Col[d]]);
    dydt[i][x1Col[d]] += deriv;
    dydt[i][x2Col[d]] -= deriv;
  }
}

CigarGrowthExponentialTrunc::
CigarGrowthExponentialTrunc(std::vector<double> &paraValue, 
			       std::vector< std::vector<size_t> > &indValue ) {
  
  // Do some checks on the parameters and variable indeces
  //
  if( paraValue.size()!=2 ) {
    std::cerr << "CigarGrowtExponentialTrunc::"
	      << "CigarGrowthExponentialTrunc() "
	      << "Uses two parameters k_growth, DMax\n";
    exit(0);
  }
  if( indValue.size() ) {
    std::cerr << "CigarGrowthExponentialTrunc::"
	      << "CigarGrowthExponentialTrunc() "
	      << "No variable index are used.\n";
    exit(0);
  }
  // Set the variable values
  //
  setId("cigarGrowthExponentialTrunc");
  setParameter(paraValue);  
  setVariableIndex(indValue);
  
  // Set the parameter identities
  //
  std::vector<std::string> tmp( numParameter() );
  tmp.resize( numParameter() );
  tmp[0] = "k_growth";
  tmp[1] = "DMax";
  setParameterId( tmp );
}

void CigarGrowthExponentialTrunc::
derivs(Compartment &compartment,size_t varIndex,
       std::vector< std::vector<double> > &y,
       std::vector< std::vector<double> > &dydt ) {
  
  // Make sure the topology dimension is correct
  assert( compartment.numTopologyVariable() == 5 ||
	  compartment.numTopologyVariable() != 5 );
  
  // List the variable columns used
  size_t i=compartment.index();
  //int dimension = (compartment.numTopologyVariable()-1)/2;
  size_t dCol = compartment.numTopologyVariable()-1+varIndex;
  
  // derivative contrubution to end-to-end distance  
  double D = y[i][dCol];
  dydt[i][dCol] += parameter(0)*D*( 1. - D / parameter(1) );
}

SphereBudGrowthTruncNeigh::
SphereBudGrowthTruncNeigh(std::vector<double> &paraValue, 
			  std::vector< std::vector<size_t> > 
			  &indValue ) {
  
  //Do some checks on the parameters and variable indeces
  //////////////////////////////////////////////////////////////////////
  if( paraValue.size()!=7 ) {
    std::cerr << "SphereBudGrowthTruncNeigh::SphereBudGrowthTruncNeigh() "
	      << "Uses seven parameters k_exp, k_const, r_max, "
	      << "k_neigh, p_neigh, k_G1 and k_spring.\n";
    exit(0);
  }
  if( indValue.size() ) {
    std::cerr << "SphereBudGrowthTruncNeigh::SphereBudGrowthTruncNeigh() "
	      << "No variable index are used.\n";
    exit(0);
  }
  //Set the variable values
  //////////////////////////////////////////////////////////////////////
  setId("sphereBudGrowthTruncNeigh");
  setParameter(paraValue);  
  setVariableIndex(indValue);
  
  //Set the parameter identities
  //////////////////////////////////////////////////////////////////////
  std::vector<std::string> tmp( numParameter() );
  tmp.resize( numParameter() );
  tmp[0] = "k_exp";
  tmp[0] = "k_const";
  tmp[0] = "r_max";
  tmp[0] = "k_neigh";
  tmp[0] = "p_neigh";
  tmp[0] = "k_G1";
  tmp[0] = "k_spring";
  setParameterId( tmp );
}

//! Derivative contribution for a sphere-bud growth
/*! Deriving the time derivative contribution from a constant growth
  using the values in y and add the results to dydt. Compartment is
  the cell (sphere+bud) and varIndex is the index of the size variable
  to be updated. The parameters are
*/
void SphereBudGrowthTruncNeigh::
derivs(Compartment &compartment,size_t varIndex,
       std::vector< std::vector<double> > &y,
       std::vector< std::vector<double> > &dydt ) {
  
  if( compartment.numTopologyVariable() != 4 &&
      compartment.numTopologyVariable() != 6 &&
      compartment.numTopologyVariable() != 8 ) {
    std::cerr << "SphereBudGrowthTruncNeigh::derivs() "
	      << "Wrong number of dimensions (topology variables).\n";
    exit(-1);
  }
  size_t i=compartment.index();
  size_t dimension = static_cast<size_t>( (compartment.numTopologyVariable()-2)/2 );
  size_t Nn = compartment.numNeighbor();
  size_t x1Col=varIndex;
  size_t x2Col=varIndex+dimension;
  size_t r1Col=varIndex+2*dimension;
  size_t r2Col=r1Col+1;
  
  //Update r1 and r2
  double fMax = parameter(0)*parameter(2)+parameter(1);
  double f = parameter(0)*y[i][r1Col]+parameter(1);
  dydt[i][r1Col] += f*(1.0-f/fMax)*
    std::pow( parameter(3)*Nn ,-parameter(4) );
  if( y[i][r1Col]>=parameter(5)*parameter(2) ) {
    f = parameter(0)*y[i][r2Col]+parameter(1);
    dydt[i][r2Col] += f*(1.0-f/fMax)*
      std::pow( parameter(3)*Nn ,-parameter(4) );
  }

  //Update sphere/bud position
  double r = y[i][r1Col];
  double d=0.;
  for(size_t dim=0 ; dim<dimension ; dim++ )
    d += (y[i][x1Col+dim]-y[i][x2Col+dim])*
      (y[i][x1Col+dim]-y[i][x2Col+dim]);
  if( d<=0. ) {
    std::cerr << "SphereBudGrowthTruncNeigh::derivs() "
	      << "Two cells on top of each other, exiting\n";
    std::cerr << i << "  " << "d=" << d << " r=" << r << "\n";
    exit(-1);
  }
  d = std::sqrt(d);
  double rCoff = parameter(6)*(1.-(r/d));
  //Update for each dimension
  for(size_t dim=0 ; dim<dimension ; dim++ ) {
    double div = (y[i][x1Col+dim]-y[i][x2Col+dim])*rCoff;
    dydt[i][x1Col+dim] -= div;
    dydt[i][x2Col+dim] += div;
  }
}

LensRadiusGrowth::LensRadiusGrowth(std::vector<double> &paraValue, 
																	 std::vector< std::vector<size_t> > 
																	 &indValue ) {
  
  //Do some checks on the parameters and variable indeces
  //////////////////////////////////////////////////////////////////////
  if( paraValue.size()!=0 ) {
    std::cerr << "LensRadiusGrowth::LensRadiusGrowth() "
							<< "Uses no parameters.\n";
    exit(0);
  }
  if( indValue.size()!=1 || indValue[0].size()!=1 ) {
    std::cerr << "LensRadiusGrowth::LensRadiusGrowth() "
							<< "Variable index for dr is used.\n";
    exit(0);
  }
  //Set the variable values
  //////////////////////////////////////////////////////////////////////
  setId("lensRadiusGrowth");
  setParameter(paraValue);  
  setVariableIndex(indValue);
  
  //Set the parameter identities
  //////////////////////////////////////////////////////////////////////
  std::vector<std::string> tmp( numParameter() );
  tmp.resize( numParameter() );
  setParameterId( tmp );
}

//! Derivative contribution for a constant growth
/*! Deriving the time derivative contribution from a constant growth
  using the values in y and add the results to dydt. Compartment is
  the cell and varIndex is the index of the size variable to be
  updated. The parameter is k_const in dr/dt=k_const
*/
void LensRadiusGrowth::derivs(Compartment &compartment,size_t varIndex,
															std::vector< std::vector<double> > &y,
															std::vector< std::vector<double> > &dydt ) {
  
	double r0=0.0;
	size_t i=compartment.index();
	if (i)
		r0 += y[i-1][varIndex]+0.5*y[i-1][variableIndex(0,0)];
	
  y[i][varIndex] = r0+0.5*y[i][variableIndex(0,0)]; 
}

LensLayerGrowth::LensLayerGrowth(std::vector<double> &paraValue, 
																 std::vector< std::vector<size_t> > 
																 &indValue ) {
  
  //Do some checks on the parameters and variable indeces
  //////////////////////////////////////////////////////////////////////
  if( paraValue.size()!=2 ) {
    std::cerr << "LensLayerGrowth::LensLayerGrowth() "
							<< "Uses two parameters, dR/dt and a.\n";
    exit(0);
  }
  if( indValue.size()!=1 || indValue[0].size()!=1 ) {
    std::cerr << "LensLayerGrowth::LensLayerGrowth() "
							<< "Variable index for r is used.\n";
    exit(0);
  }
  //Set the variable values
  //////////////////////////////////////////////////////////////////////
  setId("lensLayerGrowth");
  setParameter(paraValue);  
  setVariableIndex(indValue);
  
  //Set the parameter identities
  //////////////////////////////////////////////////////////////////////
  std::vector<std::string> tmp( numParameter() );
  tmp.resize( numParameter() );
	tmp[0] = "dR/dt";
	tmp[1] = "a";
  setParameterId( tmp );
}

//! Derivative contribution for a constant growth
/*! Deriving the time derivative contribution from a constant growth
  using the values in y and add the results to dydt. Compartment is
  the cell and varIndex is the index of the size variable to be
  updated. The parameter is k_const in dr/dt=k_const
*/
void LensLayerGrowth::derivs(Compartment &compartment,size_t varIndex,
														 std::vector< std::vector<double> > &y,
														 std::vector< std::vector<double> > &dydt ) {
  
	size_t i=compartment.index();
	size_t N=y.size()-1;
// 	if( i!=N ) {
// 		double r2 = y[i][variableIndex(0,0)]*y[i][variableIndex(0,0)];
// 		double R2 = y[N][variableIndex(0,0)]*y[N][variableIndex(0,0)];
// 		double a2R2 = parameter(1)*parameter(1)*R2;
// 		double a5R6 = a2R2*a2R2*parameter(1);
		
// 		dydt[i][varIndex] = -3.0*parameter(0)*r2*std::pow(a2R2+r2,1.5)/a5R6; 
// 	}
// 	if( i!=N ) {
// 		double r2 = y[i][variableIndex(0,0)]*y[i][variableIndex(0,0)];
// 		double R = y[N][variableIndex(0,0)];
// 		double R2 = R*R;
// 		double a2R2 = parameter(1)*parameter(1)*R2;
// 		double a5R6 = a2R2*a2R2*parameter(1);
// 		double powD = std::pow(y[i][varIndex],5.0/3.0);
// 		dydt[i][varIndex] = -3.0*parameter(0)*powD*r2/(R*(a2R2+r2)); 
// 	}
	if( i!=N ) {
		double r2 = y[i][variableIndex(0,0)]*y[i][variableIndex(0,0)];
		double R = y[N][variableIndex(0,0)];
		double R2 = R*R;
		double a2R2 = parameter(1)*parameter(1)*R2;
		dydt[i][varIndex] = -3.0*parameter(0)*y[i][varIndex]*r2/(R*a2R2);
	}
}

LensDilution::LensDilution(std::vector<double> &paraValue, 
													 std::vector< std::vector<size_t> > 
													 &indValue ) {
  
  //Do some checks on the parameters and variable indeces
  //////////////////////////////////////////////////////////////////////
  if( paraValue.size()!=0 ) {
    std::cerr << "LensDilution::LensDilution() "
							<< "Uses no parameters.\n";
    exit(0);
  }
  if( indValue.size()!=2 || indValue[0].size()!=1 || indValue[1].size()<1) {
    std::cerr << "LensDilution::LensDilution() "
							<< "Variable index for dr is used in first level and dilution conc in second.\n";
    exit(0);
  }
  //Set the variable values
  //////////////////////////////////////////////////////////////////////
  setId("lensDilution");
  setParameter(paraValue);  
  setVariableIndex(indValue);
  
  //Set the parameter identities
  //////////////////////////////////////////////////////////////////////
  std::vector<std::string> tmp( numParameter() );
  tmp.resize( numParameter() );
  setParameterId( tmp );
}

//! Derivative contribution for a constant growth
/*! Deriving the time derivative contribution from a constant growth
  using the values in y and add the results to dydt. Compartment is
  the cell and varIndex is the index of the size variable to be
  updated. The parameter is k_const in dr/dt=k_const
*/
void LensDilution::derivs(Compartment &compartment,size_t varIndex,
														 std::vector< std::vector<double> > &y,
														 std::vector< std::vector<double> > &dydt ) {
  
	size_t i=compartment.index();
	size_t N=y.size()-1;
// 	if( i!=N ) {
// 		double r2 = y[i][variableIndex(0,0)]*y[i][variableIndex(0,0)];
// 		double R2 = y[N][variableIndex(0,0)]*y[N][variableIndex(0,0)];
// 		double a2R2 = parameter(1)*parameter(1)*R2;
// 		double a5R6 = a2R2*a2R2*parameter(1);
		
// 		dydt[i][varIndex] = -3.0*parameter(0)*r2*std::pow(a2R2+r2,1.5)/a5R6; 
// 	}
	if( i!=N ) {
		for (size_t v=0; v<numVariableIndex(1); ++v)
			dydt[i][variableIndex(1,v)] -= y[i][variableIndex(1,v)]*
				dydt[i][variableIndex(0,0)]/y[i][variableIndex(0,0)];
	}
}
