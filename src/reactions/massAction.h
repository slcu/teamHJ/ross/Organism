//
// Filename     : massAction.h
// Description  : Classes describing mass action reactions
// Author(s)    : Henrik Jonsson (henrik@thep.lu.se)
// Created      : November 2003
// Revision     : $Id: massAction.h 664 2016-02-26 17:15:54Z andre $
//
#ifndef MASSACTION_H
#define MASSACTION_H

#include "baseReaction.h"

///
/// @brief A collection of one way mass action kinetics reactions, including special enzyme 
/// and Michaelis Menten type of reactions.
///
/// These reactions follows mass action kinetics, i.e. the reaction rate is proportional to the reactants
/// concentrations. The general reaction handles any number of reactants and products, while specialized
/// versions a restricted to specific numbers of reactants/products. These specialized versions have been 
/// implemented to handle combinatoric factors in stochastic simulations where more than one reactant are of
/// the same species. The keyword Enzymatic indicates the possibility to have molecular species increase the 
/// reaction rate proportionaly to their concentrations, but that are not updated (exists before and after the
/// reaction). MM implements a Michaelis-Menten type of dynamics for both the loss of the reactant and increase of
/// the product.
///
namespace MassAction 
{
  ///
  /// @brief A one way mass action reaction
  ///
  /// The reactant indeces are in level zero and products in level one in
  /// variableIndex. One parameter (rate constant) is needed.
  ///
  /// Reactants [R] and products [P] stored in variableIndexLevel 0/1 are
  /// updated. The parameter is k_f in 
  ///
  /// @f[ \frac{d[P]}{dt} = k_f * \prod [R] @f]
  /// @f[ \frac{d[R]}{dt} = - k_f \prod [R] @f]
  ///
  /// In a model file the reaction is defined as:
  ///
  /// @verbatim
  /// massAction 1 2 N_R N_P
  /// k_f
  /// R_1 ... R_{N_R}
  /// P_1 ... P_{N_P}
  /// @endverbatim
  ///
  /// @note In the model file also massAction::general 1 2 ... can be used.
  /// @note In the propensity (for stochastic simulations) combanitoric factors due to same reactant species are not handled.
  /// @note (for developers) )varIndex is not used.
  ///
  class General : public BaseReaction {
    
  public:
    
    ///
    /// @brief Main constructor
    ///
    /// This is the main constructor which sets the parameters and variable
    /// indices that defines the reaction.
    ///
    /// @param paraValue vector with parameters
    ///
    /// @param indValue vector of vectors with variable indices
    ///
    /// @see BaseReaction::createReaction(std::vector<double> &paraValue,...)
    ///
    General(std::vector<double> &paraValue, 
	   std::vector< std::vector<size_t> > &indValue );
    
    ///
    /// @brief Derivative function for this reaction class
    ///
    /// @see BaseReaction::derivs(Compartment &compartment,size_t species,...)
    ///
    void derivs(Compartment &compartment,size_t varIndex,DataMatrix &y,DataMatrix &dydt );

    ///
    /// @brief Derivative function for this reaction class calculating the absolute value for noise solvers
    ///
    /// @see BaseReaction::derivsWithAbs(Compartment &compartment,size_t species,...)
    ///
    void derivsWithAbs(Compartment &compartment,size_t species,DataMatrix &y,DataMatrix &dydt,DataMatrix &sdydt);

    ///
    /// @brief Jacobian function for this reaction class
    ///
    /// @see BaseReaction::Jacobian(Compartment &compartment,size_t species,...)
    ///
    size_t Jacobian(Compartment &compartment,size_t species,DataMatrix &y,JacobianMatrix &A);
    
    ///
    /// @brief Propensity calculation (probability to happen in [t,t+dt]) for this reaction class
    ///
    /// @see BaseReaction::propensity(Compartment &compartment,size_t species,DataMatrix &y)
    ///
    /// @note This function is under construction and currently only works for the case
    /// when the reactants are of different kinds.
    ///
    double propensity(Compartment &compartment,size_t species,DataMatrix &y);
    
    ///
    /// @brief Discrete update for this reaction used in stochastic simulations
    ///
    /// @see BaseReaction::discreteUpdate(Compartment &compartment,size_t species,DataMatrix &y)
    ///
    void discreteUpdate(Compartment &compartment,size_t species,DataMatrix &y);
  };
  
  ///
  /// @brief A one way mass action reaction assuming a single reactant and a single product
  ///
  /// The reactant index are in level zero and the product in level one in
  /// variableIndex. One parameter (rate constant) is needed.
  ///
  /// Reactant [R] and product [P] stored in variableIndexLevel 0/1 are
  /// updated. The parameter is k_f in 
  ///
  /// @f[ \frac{d[P]}{dt} = k_f R @f]
  /// @f[ \frac{d[R]}{dt} = - k_f R @f]
  ///
  /// In a model file the reaction is defined as:
  ///
  /// @verbatim
  /// massAction::OneToOne 1 2 1 1
  /// k_f
  /// R
  /// P
  /// @endverbatim
  ///
  /// @note In the model file also massAction1to1 1 2... can be used.
  /// @note Can be used for stochastic simulations.
  /// @note (for developers) varIndex is not used.
  ///
  class OneToOne : public BaseReaction {
    
  public:
    
    ///
    /// @brief Main constructor
    ///
    /// This is the main constructor which sets the parameters and variable
    /// indices that defines the reaction.
    ///
    /// @param paraValue vector with parameters
    ///
    /// @param indValue vector of vectors with variable indices
    ///
    /// @see BaseReaction::createReaction(std::vector<double> &paraValue,...)
    ///
    OneToOne(std::vector<double> &paraValue, 
	     std::vector< std::vector<size_t> > &indValue );
    
    ///
    /// @brief Derivative function for this reaction class
    ///
    /// @see BaseReaction::derivs(Compartment &compartment,size_t species,...)
    ///
    void derivs(Compartment &compartment,size_t varIndex,DataMatrix &y,DataMatrix &dydt );

    ///
    /// @brief Derivative function for this reaction class calculating the absolute value for noise solvers
    ///
    /// @see BaseReaction::derivsWithAbs(Compartment &compartment,size_t species,...)
    ///
    void derivsWithAbs(Compartment &compartment,size_t species,DataMatrix &y,DataMatrix &dydt,DataMatrix &sdydt);

    ///
    /// @brief Jacobian function for this reaction class
    ///
    /// @see BaseReaction::Jacobian(Compartment &compartment,size_t species,...)
    ///
    size_t Jacobian(Compartment &compartment,size_t species,DataMatrix &y,JacobianMatrix &A);
    
    ///
    /// @brief Propensity calculation (probability to happen in [t,t+dt]) for this reaction class
    ///
    /// @see BaseReaction::propensity(Compartment &compartment,size_t species,DataMatrix &y)
    ///
    double propensity(Compartment &compartment,size_t species,DataMatrix &y);
    
    ///
    /// @brief Discrete update for this reaction used in stochastic simulations
    ///
    /// @see BaseReaction::discreteUpdate(Compartment &compartment,size_t species,DataMatrix &y)
    ///
    void discreteUpdate(Compartment &compartment,size_t species,DataMatrix &y);
  };
  
  ///
  /// @brief A one way mass action reaction assuming a single reactant and a two products
  ///
  /// The reactant index are in level zero and the two products in level one in
  /// variableIndex. One parameter (rate constant) is needed.
  ///
  /// Reactant R and products @f$P_1@f$, @f$P_2@f$ stored in variableIndexLevel 0/1 are
  /// updated. The parameter is k_f in 
  ///
  /// @f[ \frac{d[P_i]}{dt} = k_f R @f]
  /// @f[ \frac{d[R]}{dt} = - k_f R @f]
  ///
  /// In a model file the reaction is defined as:
  ///
  /// @verbatim
  /// massAction::OneToTwo 1 2 1 2
  /// k_f
  /// R
  /// P_1 P_2
  /// @endverbatim
  ///
  /// @note In the model file also massAction1to2 1 2 can be used. 
  /// @note Can be used in stochastic simulations.
  /// @note (for developers) varIndex is not used.
  ///
  class OneToTwo : public BaseReaction {
    
  public:
    
    ///
    /// @brief Main constructor
    ///
    /// This is the main constructor which sets the parameters and variable
    /// indices that defines the reaction.
    ///
    /// @param paraValue vector with parameters
    ///
    /// @param indValue vector of vectors with variable indices
    ///
    /// @see BaseReaction::createReaction(std::vector<double> &paraValue,...)
    ///
    OneToTwo(std::vector<double> &paraValue, 
	     std::vector< std::vector<size_t> > &indValue );
    
    ///
    /// @brief Derivative function for this reaction class
    ///
    /// @see BaseReaction::derivs(Compartment &compartment,size_t species,...)
    ///
    void derivs(Compartment &compartment,size_t varIndex,DataMatrix &y,DataMatrix &dydt );

    ///
    /// @brief Derivative function for this reaction class calculating the absolute value for noise solvers
    ///
    /// @see BaseReaction::derivsWithAbs(Compartment &compartment,size_t species,...)
    ///
    void derivsWithAbs(Compartment &compartment,size_t species,DataMatrix &y,DataMatrix &dydt,DataMatrix &sdydt);

    ///
    /// @brief Jacobian function for this reaction class
    ///
    /// @see BaseReaction::Jacobian(Compartment &compartment,size_t species,...)
    ///
    size_t Jacobian(Compartment &compartment,size_t species,DataMatrix &y,JacobianMatrix &A);
    
    ///
    /// @brief Propensity calculation (probability to happen in [t,t+dt]) for this reaction class
    ///
    /// @see BaseReaction::propensity(Compartment &compartment,size_t species,DataMatrix &y)
    ///
    double propensity(Compartment &compartment,size_t species,DataMatrix &y);
    
    ///
    /// @brief Discrete update for this reaction used in stochastic simulations
    ///
    /// @see BaseReaction::discreteUpdate(Compartment &compartment,size_t species,DataMatrix &y)
    ///
    void discreteUpdate(Compartment &compartment,size_t species,DataMatrix &y);
  };
  
  ///
  /// @brief A one way mass action reaction assuming a single reactant and three products
  ///
  /// The reactant index are in level zero and the three products in level one in
  /// variableIndex. One parameter (rate constant) is needed.
  ///
  /// Reactant R and products @f$P_i@f$ stored in variableIndexLevel 0/1 are
  /// updated. The parameter is k_f in 
  ///
  /// @f[ \frac{d[P_i]}{dt} = k_f R @f]
  /// @f[ \frac{d[R]}{dt} = - k_f R @f]
  ///
  /// In a model file the reaction is defined as:
  ///
  /// @verbatim
  /// massAction::OneToThree 1 2 1 3
  /// k_f
  /// R
  /// P_1 P_2 P_3
  /// @endverbatim
  ///
  /// @note In the model file also massAction1to3 1 2... can be used.
  /// @note (for developers) varIndex is not used.
  /// @note Can be used in stochastic simulations.
  ///
  class OneToThree : public BaseReaction {
    
  public:
    
    ///
    /// @brief Main constructor
    ///
    /// This is the main constructor which sets the parameters and variable
    /// indices that defines the reaction.
    ///
    /// @param paraValue vector with parameters
    ///
    /// @param indValue vector of vectors with variable indices
    ///
    /// @see BaseReaction::createReaction(std::vector<double> &paraValue,...)
    ///
    OneToThree(std::vector<double> &paraValue, 
	       std::vector< std::vector<size_t> > &indValue );
    
    ///
    /// @brief Derivative function for this reaction class
    ///
    /// @see BaseReaction::derivs(Compartment &compartment,size_t species,...)
    ///
    void derivs(Compartment &compartment,size_t varIndex,DataMatrix &y,DataMatrix &dydt );

    ///
    /// @brief Derivative function for this reaction class calculating the absolute value for noise solvers
    ///
    /// @see BaseReaction::derivsWithAbs(Compartment &compartment,size_t species,...)
    ///
    void derivsWithAbs(Compartment &compartment,size_t species,DataMatrix &y,DataMatrix &dydt,DataMatrix &sdydt);

    ///
    /// @brief Jacobian function for this reaction class
    ///
    /// @see BaseReaction::Jacobian(Compartment &compartment,size_t species,...)
    ///
    size_t Jacobian(Compartment &compartment,size_t species,DataMatrix &y,JacobianMatrix &A);
    
    ///
    /// @brief Propensity calculation (probability to happen in [t,t+dt]) for this reaction class
    ///
    /// @see BaseReaction::propensity(Compartment &compartment,size_t species,DataMatrix &y)
    ///
    double propensity(Compartment &compartment,size_t species,DataMatrix &y);
    
    ///
    /// @brief Discrete update for this reaction used in stochastic simulations
    ///
    /// @see BaseReaction::discreteUpdate(Compartment &compartment,size_t species,DataMatrix &y)
    ///
    void discreteUpdate(Compartment &compartment,size_t species,DataMatrix &y);
  };
  
  ///
  /// @brief A one way mass action reaction assuming a single reactant and four products
  ///
  /// The reactant index are in level zero and the four products in level one in
  /// variableIndex. One parameter (rate constant) is needed.
  ///
  /// Reactant R and products @f$P_i@f$ stored in variableIndexLevel 0/1 are
  /// updated. The parameter is k_f in 
  ///
  /// @f[ \frac{d[P_i]}{dt} = k_f R @f]
  /// @f[ \frac{d[R]}{dt} = - k_f R @f]
  ///
  /// In a model file the reaction is defined as:
  ///
  /// @verbatim
  /// massAction::OneToFour 1 2 1 4
  /// k_f
  /// R
  /// P_1 P_2 P_3 P_4
  /// @endverbatim
  ///
  /// @note In the model file also massAction1to4 1 2... can be used.
  /// @note Can be used in stochastic simulations.
  /// @note (for developers) varIndex is not used.
  ///
  class OneToFour : public BaseReaction {
    
  public:
    
    ///
    /// @brief Main constructor
    ///
    /// This is the main constructor which sets the parameters and variable
    /// indices that defines the reaction.
    ///
    /// @param paraValue vector with parameters
    ///
    /// @param indValue vector of vectors with variable indices
    ///
    /// @see BaseReaction::createReaction(std::vector<double> &paraValue,...)
    ///
    OneToFour(std::vector<double> &paraValue, 
	      std::vector< std::vector<size_t> > &indValue );
    
    ///
    /// @brief Derivative function for this reaction class
    ///
    /// @see BaseReaction::derivs(Compartment &compartment,size_t species,...)
    ///
    void derivs(Compartment &compartment,size_t varIndex,DataMatrix &y,DataMatrix &dydt );

    ///
    /// @brief Derivative function for this reaction class calculating the absolute value for noise solvers
    ///
    /// @see BaseReaction::derivsWithAbs(Compartment &compartment,size_t species,...)
    ///
    void derivsWithAbs(Compartment &compartment,size_t species,DataMatrix &y,DataMatrix &dydt,DataMatrix &sdydt);

    ///
    /// @brief Jacobian function for this reaction class
    ///
    /// @see BaseReaction::Jacobian(Compartment &compartment,size_t species,...)
    ///
    size_t Jacobian(Compartment &compartment,size_t species,DataMatrix &y,JacobianMatrix &A);
    
    ///
    /// @brief Propensity calculation (probability to happen in [t,t+dt]) for this reaction class
    ///
    /// @see BaseReaction::propensity(Compartment &compartment,size_t species,DataMatrix &y)
    ///
    double propensity(Compartment &compartment,size_t species,DataMatrix &y);
    
    ///
    /// @brief Discrete update for this reaction used in stochastic simulations
    ///
    /// @see BaseReaction::discreteUpdate(Compartment &compartment,size_t species,DataMatrix &y)
    ///
    void discreteUpdate(Compartment &compartment,size_t species,DataMatrix &y);
  };
  
  ///
  /// @brief A one way mass action reaction assuming two reactants and a single product
  ///
  /// The reactant index are in level zero and the product in level one in
  /// variableIndex. One parameter (rate constant) is needed.
  ///
  /// Reactants @f$R_1,R_2@f$ and product P stored in variableIndexLevel 0/1 are
  /// updated. The parameter is k_f in 
  ///
  /// @f[ \frac{d[P]}{dt} = k_f R_1 R_2 @f]
  /// @f[ \frac{d[R_i]}{dt} = - k_f R_1 R_2 @f]
  ///
  /// In a model file the reaction is defined as:
  ///
  /// @verbatim
  /// massAction::TwoToOne 1 2 2 1
  /// k_f
  /// R_1 R_2
  /// P
  /// @endverbatim
  ///
  /// @note In the model file also massAction2to1 1 2... can be used.
  /// @note Can be used in stochastic simulations.
  /// @note (for developers) varIndex is not used.
  ///
  class TwoToOne : public BaseReaction {
    
  public:
    
    ///
    /// @brief Main constructor
    ///
    /// This is the main constructor which sets the parameters and variable
    /// indices that defines the reaction.
    ///
    /// @param paraValue vector with parameters
    ///
    /// @param indValue vector of vectors with variable indices
    ///
    /// @see BaseReaction::createReaction(std::vector<double> &paraValue,...)
    ///
    TwoToOne(std::vector<double> &paraValue, 
	     std::vector< std::vector<size_t> > &indValue );
    
    ///
    /// @brief Derivative function for this reaction class
    ///
    /// @see BaseReaction::derivs(Compartment &compartment,size_t species,...)
    ///
    void derivs(Compartment &compartment,size_t varIndex,DataMatrix &y,DataMatrix &dydt );

    ///
    /// @brief Derivative function for this reaction class calculating the absolute value for noise solvers
    ///
    /// @see BaseReaction::derivsWithAbs(Compartment &compartment,size_t species,...)
    ///
    void derivsWithAbs(Compartment &compartment,size_t species,DataMatrix &y,DataMatrix &dydt,DataMatrix &sdydt);

    ///
    /// @brief Jacobian function for this reaction class
    ///
    /// @see BaseReaction::Jacobian(Compartment &compartment,size_t species,...)
    ///
    size_t Jacobian(Compartment &compartment,size_t species,DataMatrix &y,JacobianMatrix &A);
    
    ///
    /// @brief Propensity calculation (probability to happen in [t,t+dt]) for this reaction class
    ///
    /// @see BaseReaction::propensity(Compartment &compartment,size_t species,DataMatrix &y)
    ///
    double propensity(Compartment &compartment,size_t species,DataMatrix &y);
    
    ///
    /// @brief Discrete update for this reaction used in stochastic simulations
    ///
    /// @see BaseReaction::discreteUpdate(Compartment &compartment,size_t species,DataMatrix &y)
    ///
    void discreteUpdate(Compartment &compartment,size_t species,DataMatrix &y);
  };
  
  ///
  /// @brief A one way mass action reaction assuming three reactants and a single product
  ///
  /// The reactant indices are in level zero and the product in level one in
  /// variableIndex. One parameter (rate constant) is needed.
  ///
  /// Reactants @f$R_i@f$ and product P stored in variableIndexLevel 0/1 are
  /// updated. The parameter is k_f in 
  ///
  /// @f[ \frac{d[P]}{dt} = k_f R_1 R_2 R_3 @f]
  /// @f[ \frac{d[R_i]}{dt} = - k_f R_1 R_2 R_3 @f]
  ///
  /// In a model file the reaction is defined as:
  ///
  /// @verbatim
  /// massAction::ThreeToOne 1 2 3 1
  /// k_f
  /// R_1 R_2 R_3
  /// P
  /// @endverbatim
  ///
  /// @note In the model file also massAction3to1 1 2... can be used.
  /// @note Can be used in stochastic simulations.
  /// @note (for developers) varIndex is not used.
  ///
  class ThreeToOne : public BaseReaction {
    
  public:
    
    ///
    /// @brief Main constructor
    ///
    /// This is the main constructor which sets the parameters and variable
    /// indices that defines the reaction.
    ///
    /// @param paraValue vector with parameters
    ///
    /// @param indValue vector of vectors with variable indices
    ///
    /// @see BaseReaction::createReaction(std::vector<double> &paraValue,...)
    ///
    ThreeToOne(std::vector<double> &paraValue, 
	       std::vector< std::vector<size_t> > &indValue );
    
    ///
    /// @brief Derivative function for this reaction class
    ///
    /// @see BaseReaction::derivs(Compartment &compartment,size_t species,...)
    ///
    void derivs(Compartment &compartment,size_t varIndex,DataMatrix &y,DataMatrix &dydt );

    ///
    /// @brief Derivative function for this reaction class calculating the absolute value for noise solvers
    ///
    /// @see BaseReaction::derivsWithAbs(Compartment &compartment,size_t species,...)
    ///
    void derivsWithAbs(Compartment &compartment,size_t species,DataMatrix &y,DataMatrix &dydt,DataMatrix &sdydt);

    ///
    /// @brief Jacobian function for this reaction class
    ///
    /// @see BaseReaction::Jacobian(Compartment &compartment,size_t species,...)
    ///
    size_t Jacobian(Compartment &compartment,size_t species,DataMatrix &y,JacobianMatrix &A);
    
    ///
    /// @brief Propensity calculation (probability to happen in [t,t+dt]) for this reaction class
    ///
    /// @see BaseReaction::propensity(Compartment &compartment,size_t species,DataMatrix &y)
    ///
    double propensity(Compartment &compartment,size_t species,DataMatrix &y);
    
    ///
    /// @brief Discrete update for this reaction used in stochastic simulations
    ///
    /// @see BaseReaction::discreteUpdate(Compartment &compartment,size_t species,DataMatrix &y)
    ///
    void discreteUpdate(Compartment &compartment,size_t species,DataMatrix &y);
  };
  
  ///
  /// @brief A one way mass action reaction with enzyme contribution
  ///
  /// The reactant indeces are in level zero, products in level one, and enzymes
  /// in level two in variableIndex. One parameter (rate constant) is needed.
  ///
  /// Reactants [R], products [P] and enzymes [E] are stored in
  /// variableIndexLevel 0/1/2. [R] and [P] are updated. The parameter is k_f in
  /// 
  /// @f[ \frac{d[P]}{dt} = k_f \prod [E] \prod [R] @f]
  /// @f[ \frac{d[R]}{dt} = - k_f \prod [E] \prod [R] @f]
  /// @f[ \frac{d[E]}{dt} = 0 @f]
  ///
  /// In a model file the reaction is defined as:
  ///
  /// @verbatim
  /// massAction::Enzymatic 1 3 N_R N_P N_E
  /// k_f
  /// R_1 ... R_{N_R}
  /// P_1 ... P_{N_P}
  /// E_1 ... E_{N_E}
  /// @endverbatim
  ///
  /// @note In the model file also massActionEnzymatic 1 3... can be used.
  /// @note Can be used in stochastic simulations.
  /// @note (for developers) varIndex is not used.
  /// 
  class Enzymatic : public BaseReaction {
    
  public:
    
    ///
    /// @brief Main constructor
    ///
    /// This is the main constructor which sets the parameters and variable
    /// indices that defines the reaction.
    ///
    /// @param paraValue vector with parameters
    ///
    /// @param indValue vector of vectors with variable indices
    ///
    /// @see BaseReaction::createReaction(std::vector<double> &paraValue,...)
    ///
    Enzymatic(std::vector<double> &paraValue, 
	      std::vector< std::vector<size_t> > &indValue );
    
    ///
    /// @brief Derivative function for this reaction class
    ///
    /// @see BaseReaction::derivs(Compartment &compartment,size_t species,...)
    ///
    void derivs(Compartment &compartment,size_t varIndex,DataMatrix &y,DataMatrix &dydt );

    ///
    /// @brief Derivative function for this reaction class calculating the absolute value for noise solvers
    ///
    /// @see BaseReaction::derivsWithAbs(Compartment &compartment,size_t species,...)
    ///
    void derivsWithAbs(Compartment &compartment,size_t species,DataMatrix &y,DataMatrix &dydt,DataMatrix &sdydt);
    
    ///
    /// @brief Jacobian function for this reaction class
    ///
    /// @see BaseReaction::Jacobian(Compartment &compartment,size_t species,...)
    ///
    size_t Jacobian(Compartment &compartment,size_t species,DataMatrix &y,JacobianMatrix &A);
    ///
    /// @brief Prints the reaction in Cambium format
    ///
    /// @see BaseReaction::printCambium()
    ///
    void printCambium( std::ostream &os, size_t varIndex ) const;
  };
  
  ///
  /// @brief A one way mass action enzymatic reaction using Michaelis Menten
  ///
  /// The reactant index is in level zero and the product in level one in
  /// variableIndex.  V,K parameters are needed.
  ///
  /// Reactant [R] and product [P] stored in variableIndexLevel 0/1 are
  /// updated acccording to
  ///
  /// @f[ \frac{d[P]}{dt} = \frac{V_{max} [R]}{(K+[R])} @f]
  /// @f[ \frac{d[R]}{dt} = - \frac{d[P]}{dt} @f]
  ///
  /// In a model file the reaction is defined as:
  ///
  /// @verbatim
  /// massAction::MM 2 2 1 1
  /// V_max K
  /// R
  /// P
  /// @endverbatim
  ///
  /// @note In the model file also massActionMM 2 2... can be used.
  /// @note Can be used in stochastic simulations.
  /// @note (for developers) varIndex is not used.
  ///
  class MM : public BaseReaction {
    
  public:
    
    ///
    /// @brief Main constructor
    ///
    /// This is the main constructor which sets the parameters and variable
    /// indices that defines the reaction.
    ///
    /// @param paraValue vector with parameters
    ///
    /// @param indValue vector of vectors with variable indices
    ///
    /// @see BaseReaction::createReaction(std::vector<double> &paraValue,...)
    ///
    MM(std::vector<double> &paraValue, 
       std::vector< std::vector<size_t> > &indValue );
    
    ///
    /// @brief Derivative function for this reaction class
    ///
    /// @see BaseReaction::derivs(Compartment &compartment,size_t species,...)
    ///
    void derivs(Compartment &compartment,size_t varIndex,DataMatrix &y,DataMatrix &dydt );
  };
  
  ///
  /// @brief A one way mass action enzymatic reaction using Michaelis Menten
  ///
  /// The reactant index is in level zero and the product in level one in
  /// variableIndex.  V,K parameters are needed.
  ///
  /// Reactants [R] and products [P] stored in variableIndexLevel 0/1 are
  /// updated, and [E] is in variableIndexLevel 2 . The update is 
  ///
  /// @f[ \frac{d[P]}{dt} = \frac{V_{max} [E][R]}{(K+[R])} @f]
  /// @f[ \frac{d[R]}{dt} = - \frac{d[P]}{dt} @f]
  /// @f[ \frac{d[E]}{dt} = 0 @f]
  ///
  /// In a model file the reaction is defined as:
  ///
  /// @verbatim
  /// massAction::EnzymaticMM 2 3 1 1 1
  /// V_max K
  /// R
  /// P
  /// E
  /// @endverbatim
  ///
  /// @note In the model file also massActionEnzymaticMM 2 3... can be used.
  /// @note Can be used in stochastic simulations.
  /// @note (for developers) varIndex is not used.
  ///
  class EnzymaticMM : public BaseReaction {
    
  public:
    
    ///
    /// @brief Main constructor
    ///
    /// This is the main constructor which sets the parameters and variable
    /// indices that defines the reaction.
    ///
    /// @param paraValue vector with parameters
    ///
    /// @param indValue vector of vectors with variable indices
    ///
    /// @see BaseReaction::createReaction(std::vector<double> &paraValue,...)
    ///
    EnzymaticMM(std::vector<double> &paraValue, 
		std::vector< std::vector<size_t> > &indValue );
    
    ///
    /// @brief Derivative function for this reaction class
    ///
    /// @see BaseReaction::derivs(Compartment &compartment,size_t species,...)
    ///
    void derivs(Compartment &compartment,size_t varIndex,DataMatrix &y,DataMatrix &dydt );
  };
  
  ///
  /// @brief A one way mass action enzymatic reaction using Michaelis Menten and
  /// a fraction of the molecular concentration
  ///
  /// The reactant index is in level zero and the product in level one in
  /// variableIndex.  V,K,f parameters are needed.
  ///
  /// Reactant [R] and product [P] stored in variableIndexLevel 0/1 are updated
  ///
  /// @f[ \frac{d[P]}{dt} = \frac{V_{max} f [R]}{(K + f[R])} @f]
  /// @f[ \frac{d[R]}{dt} = - \frac{d[P]}{dt} @f]
  ///
  /// In a model file the reaction is defined as:
  ///
  /// @verbatim
  /// massAction::MMFraction 3 2 1 1
  /// V_max K f
  /// R
  /// P
  /// @endverbatim
  ///
  /// @note In the model file also massActionMMFraction 3 2... can be used.
  /// @note Can be used in stochastic simulations.
  /// @note (for developers) varIndex is not used.
  ///
  class MMFraction : public BaseReaction {
    
  public:
    
    ///
    /// @brief Main constructor
    ///
    /// This is the main constructor which sets the parameters and variable
    /// indices that defines the reaction.
    ///
    /// @param paraValue vector with parameters
    ///
    /// @param indValue vector of vectors with variable indices
    ///
    /// @see BaseReaction::createReaction(std::vector<double> &paraValue,...)
    ///
    MMFraction(std::vector<double> &paraValue, 
	       std::vector< std::vector<size_t> > &indValue );
    
    ///
    /// @brief Derivative function for this reaction class
    ///
    /// @see BaseReaction::derivs(Compartment &compartment,size_t species,...)
    ///
    void derivs(Compartment &compartment,size_t varIndex,DataMatrix &y,DataMatrix &dydt );
  };

  ///
  /// @brief A one way mass action enzymatic reaction using a Hill function for the enzymes
  ///
  /// This reaction is a Michaelis-Menten formalism for production with a
  /// restricting variable given by
  ///
  /// @f[ \frac{d[P]}{dt} = p_0 [R] \frac{E_{k}^{p_2}}{p_1^{p_2}+E_{k}^{p_2}}...\frac{p_{1'}^{p_{2'}}}{p_{1'}^{p_{2'}}+E_{k'}^{p_{2'}}} @f]
  /// @f[ \frac{d[R]}{dt} = - \frac{d[P]}{dt} @f]
  /// @f[ \frac{d[E_k]}{dt} = 0 @f]

  ///
  /// where p_0 is the maximal rate (@f$V_{max}@f$), and @f$p_1@f$ is the Hill
  /// constant (@f$K_{half}@f$), and @f$p_2@f$ is the Hill coefficient (n). The
  /// k index is given in the first level of varIndex and corresponds to the
  /// activators, and the k' in the second level which corresponds to the
  /// repressors. In both layers each index corresponds to a pair of parameters
  /// (K,n) that are preceded by the @f$V_{max}@f$ parameter.
  ///
  /// In the model file the reaction will be defined (somewhat different for different number of 
  /// activators and repressors) e.g. as
  ///
  /// Product with index P_index, reactant with index R_index and
  /// two activators (A1 and A2) and one inhibitor (I1):
  /// @verbatim
  /// massAction::EnzymaticHill 5 4 1 1 2 1
  /// V K_A1 n_A1 K_A2 n_A2 K_I1 n_I1 
  /// P_index R_index
  /// A1_index A2_index I1_index
  /// @endverbatim

  class EnzymaticHill : public BaseReaction {
    
  public:
    
    ///
    /// @brief Main constructor
    ///
    /// This is the main constructor which sets the parameters and variable
    /// indices that defines the reaction.
    ///
    /// @param paraValue vector with parameters
    ///
    /// @param indValue vector of vectors with variable indices
    ///
    /// @see BaseReaction::createReaction(std::vector<double> &paraValue,...)
    ///
    EnzymaticHill(std::vector<double> &paraValue, 
		std::vector< std::vector<size_t> > &indValue );
    
    ///
    /// @brief Derivative function for this reaction class
    ///
    /// @see BaseReaction::derivs(Compartment &compartment,size_t species,...)
    ///
    void derivs(Compartment &compartment,size_t varIndex,DataMatrix &y,DataMatrix &dydt );
  };


  
} //end namespace MassAction
#endif

