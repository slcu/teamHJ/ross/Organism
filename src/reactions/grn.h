//
// Filename     : grn.h
// Description  : Classes describing gene regulatory network updates
// Author(s)    : Henrik Jonsson (henrik@thep.lu.se)
// Created      : October 2003
// Revision     : $Id: grn.h 669 2016-05-12 08:53:12Z korsbo $
//
#ifndef GRN_H
#define GRN_H

#include <cmath>

#include "baseReaction.h"

///
/// @brief The class Grn use a neural network inspired mathematics for gene
/// regulation
///
/// This class uses a neural network inspired update with a sigmoidal function
/// for gene regulation as defined in Mjolsness et al (1991). The update is
/// done according to:
///
/// @f[ \frac{dy_{ij}}{dt} = \frac{1}{p_1}g\left( p_0 + \sum_k p_k y_{ik} \right) @f]
///
/// where g() is a sigmoidal function, the @f$p_k@f$ are the parameters given
/// from position 2 and forward, and the k indices for @f$y_{ik}@f$ are given
/// at the first level of variableIndex. Hence the number of parameters must
/// be two more than the number of indices given at the first level.
///
/// @see Grn::sigmoid(double x) for implementation of sigmoid function.
///
class Grn : public BaseReaction {
  
 public:
  
  ///
  /// @brief Main constructor
  ///
  /// This is the main constructor which checks and sets the parameters and
  /// variable indices that defines the reaction.
  ///
  /// @param paraValue vector with parameters
  ///
  /// @param indValue vector of vectors with variable indices
  ///
  /// @see BaseReaction::createReaction(std::vector<double> &paraValue,...)
  ///
  Grn(std::vector<double> &paraValue, 
      std::vector< std::vector<size_t> > &indValue );
  
  ///
  /// @brief Derivative function for this reaction class
  ///
  /// @see BaseReaction::derivs(Compartment &compartment,size_t species,...)
  ///
  void derivs(Compartment &compartment,size_t specie,DataMatrix &y,DataMatrix &dydt);

  ///
  /// @brief Derivative function for this reaction class calculating the absolute value for noise solvers
  ///
  /// @see BaseReaction::derivsWithAbs(Compartment &compartment,size_t species,...)
  ///
  void derivsWithAbs(Compartment &compartment,size_t species,DataMatrix &y,DataMatrix &dydt,DataMatrix &sdydt);
  ///
  /// @brief Prints the reaction in Cambium format
  ///
  /// @see BaseReaction::printCambium()
  ///
  void printCambium( std::ostream &os, size_t varIndex ) const;
  ///
  /// @brief Sigmoidal function used in the derivs function
  ///
  /// Sigmoidal function defined by
  ///
  /// @f[ g(x) = \frac{1}{2}\left( 1 + \frac{x}{\sqrt{1+x^2}}\right)@f]
  ///
  inline double sigmoid( double value );  
};

inline double Grn::sigmoid(double x) 
{ 
  return 0.5*(1 + x/sqrt(1+x*x) );
}

///
/// @brief The class Gsrn use a neural network inspired mathematics with
/// neighbor signals for gene regulation
///
/// This class uses a neural network inspired update with a sigmoidal function
/// for gene regulation as defined in Mjolsness et al (1991). This class adds
/// a direct contribution from neighboring cells, compared to the Grn
/// class. The update is done according to:
///
/// @f[ \frac{dy_{ij}}{dt} = \frac{1}{p_1}g\left( p_0 + \sum_k p_k y_{ik} +
/// \sum_{k'} p_{k'} y_{jk'} \right) @f]
///
/// where g() is a sigmoidal function. The @f$p_k@f$ are the parameters given
/// from position 2 and K steps forward and defines the intracellular
/// contributions. The @f$p_{k'}@f$ are given from 2+K and forward and defines
/// the intercellular (from neighbors) contribution. The k indices for
/// @f$y_{ik}@f$ are given at the first level of variableIndex
/// (intracellular), and the k' indices are given in the second level
/// (intercellular). Hence the number of parameters must be two more than the
/// number of indices given at the first and second levels.
///
/// @see Gsrn::sigmoid(double x) for implementation of sigmoid function.
///
class Gsrn : public BaseReaction {
  
 public:
  
  ///
  /// @brief Main constructor
  ///
  /// This is the main constructor which sets the parameters and variable
  /// indices that defines the reaction.
  ///
  /// @param paraValue vector with parameters
  ///
  /// @param indValue vector of vectors with variable indices
  ///
  /// @see BaseReaction::createReaction(std::vector<double> &paraValue,...)
  ///
  Gsrn(std::vector<double> &paraValue, 
       std::vector< std::vector<size_t> > &indValue );
  
  ///
  /// @brief Derivative function for this reaction class
  ///
  /// @see BaseReaction::derivs(Compartment &compartment,size_t species,...)
  ///
  void derivs(Compartment &compartment,size_t specie,DataMatrix &y,DataMatrix &dydt);
  
  ///
  /// @brief Derivative function for this reaction class calculating the absolute value for noise solvers
  ///
  /// @see BaseReaction::derivsWithAbs(Compartment &compartment,size_t species,...)
  ///
  void derivsWithAbs(Compartment &compartment,size_t species,DataMatrix &y,DataMatrix &dydt,DataMatrix &sdydt);

  ///
  /// @brief Sigmoidal function used in the derivs function
  ///
  /// Sigmoidal function defined by
  ///
  /// @f[ g(x) = \frac{1}{2}\left( 1 + \frac{x}{\sqrt{1+x^2}}\right)@f]
  ///
  inline double sigmoid( double value );  
};

inline double Gsrn::sigmoid(double x) 
{ 
  return 0.5*(1 + x/sqrt(1+x*x) );
}

///
/// @brief The class Gsrn2 use a neural network inspired mathematics with a
/// ligand/receptor neighbor signals for gene regulation
///
/// This class uses a neural network inspired update with a sigmoidal function
/// for gene regulation as defined in Mjolsness et al (1991). This class adds
/// a ligand/receptor contribution from neighboring/internal cells, compared
/// to the Gsrn class. The update is done according to:
///
/// @f[ \frac{dy_{ij}}{dt} = \frac{1}{p_1}g\left( p_0 + \sum_k p_k y_{ik} +
/// \sum_{k'} p_{k'} y_{jk'} + \sum_{k''} p_{k''} y_{jk''}y_{ik'''} \right) @f]
///
/// where g() is a sigmoidal function. The @f$p_k@f$ are the parameters given
/// from position 2 and K steps forward and defines the intracellular
/// contributions. The @f$p_{k'}@f$ are given from 2+K and forward and defines
/// the intercellular (from neighbors) contribution. The k indices for
/// @f$y_{ik}@f$ are given at the first level of variableIndex
/// (intracellular), the k' indices are given in the second level
/// (intercellular), and the k'/k''' are given in the third level ( and comes
/// in ligand/receptor pairs). Hence the number of parameters must be two more
/// than the number of indices given at the first and second levels plus half
/// the number given in the third layer.
///
/// @see Gsrn2::sigmoid(double x) for implementation of sigmoid function.
///
class Gsrn2 : public BaseReaction {
  
 public:
  
  ///
  /// @brief Main constructor
  ///
  /// This is the main constructor which sets the parameters and variable
  /// indices that defines the reaction.
  ///
  /// @param paraValue vector with parameters
  ///
  /// @param indValue vector of vectors with variable indices
  ///
  /// @see BaseReaction::createReaction(std::vector<double> &paraValue,...)
  ///
  Gsrn2(std::vector<double> &paraValue, 
	std::vector< std::vector<size_t> > &indValue );
  
  ///
  /// @brief Derivative function for this reaction class
  ///
  /// @see BaseReaction::derivs(Compartment &compartment,size_t species,...)
  ///
  void derivs(Compartment &compartment,size_t specie,DataMatrix &y,DataMatrix &dydt);

  ///
  /// @brief Derivative function for this reaction class calculating the absolute value for noise solvers
  ///
  /// @see BaseReaction::derivsWithAbs(Compartment &compartment,size_t species,...)
  ///
  void derivsWithAbs(Compartment &compartment,size_t species,DataMatrix &y,DataMatrix &dydt,DataMatrix &sdydt);
  
  ///
  /// @brief Sigmoidal function used in the derivs function
  ///
  /// Sigmoidal function defined by
  ///
  /// @f[ g(x) = \frac{1}{2}\left( 1 + \frac{x}{\sqrt{1+x^2}}\right)@f]
  ///
  inline double sigmoid( double value );  
};

inline double Gsrn2::sigmoid(double x) 
{ 
  return 0.5*(1 + x/sqrt(1+x*x) );
}

///
/// @brief This class describes a simple Michaelis-Menten production
///
/// This reaction is a Michaelis-Menten formalism for production given by
///
/// @f[ \frac{dy_{ij}}{dt} = p_0 \frac{y_{ik}}{p_1+y_{ik}}@f]
///
/// where p_0 is the maximal rate (@f$V_{max}@f$), and @f$p_1@f$ is the half
/// maximal rate constant (@f$K_{half}@f$). The k index is given in the first
/// level of varIndex.
///
class MichaelisMenten : public BaseReaction {
  
 public:
  
  ///
  /// @brief Main constructor
  ///
  /// This is the main constructor which sets the parameters and variable
  /// indices that defines the reaction.
  ///
  /// @param paraValue vector with parameters
  ///
  /// @param indValue vector of vectors with variable indices
  ///
  /// @see BaseReaction::createReaction(std::vector<double> &paraValue,...)
  ///
  MichaelisMenten(std::vector<double> &paraValue, 
		  std::vector< std::vector<size_t> > &indValue );
  
  ///
  /// @brief Derivative function for this reaction class
  ///
  /// @see BaseReaction::derivs(Compartment &compartment,size_t species,...)
  ///
  void derivs(Compartment &compartment,size_t specie,DataMatrix &y,DataMatrix &dydt);

  ///
  /// @brief Derivative function for this reaction class calculating the absolute value for noise solvers
  ///
  /// @see BaseReaction::derivsWithAbs(Compartment &compartment,size_t species,...)
  ///
  void derivsWithAbs(Compartment &compartment,size_t species,DataMatrix &y,DataMatrix &dydt,DataMatrix &sdydt);

  ///
  /// @brief Propensity calculation (probability to happen in [t,t+dt]) for this reaction class
  ///
  /// @see BaseReaction::propensity(Compartment &compartment,size_t species,DataMatrix &y)
  ///
  double propensity(Compartment &compartment,size_t species,DataMatrix &y);

  ///
  /// @brief Discrete update for this reaction used in stochastic simulations
  ///
  /// @see BaseReaction::discreteUpdate(Compartment &compartment,size_t species,DataMatrix &y)
  ///
  void discreteUpdate(Compartment &compartment,size_t species,DataMatrix &y);
};

///
/// @brief This class describes a simple Michaelis-Menten production with a
/// restricting variable
///
/// This reaction is a Michaelis-Menten formalism for production with a
/// restricting variable given by
///
/// @f[ \frac{dy_{ij}}{dt} = p_0 \frac{y_{ik} y_{ik'}}{p_1+y_{ik}}@f]
///
/// where p_0 is the maximal rate (@f$V_{max}@f$), and @f$p_1@f$ is the half
/// maximal rate constant (@f$K_{half}@f$). The k index is given in the first
/// level of varIndex, and the k' in the second level.
///
class MichaelisMentenRestricted : public BaseReaction {
  
 public:
  
  ///
  /// @brief Main constructor
  ///
  /// This is the main constructor which sets the parameters and variable
  /// indices that defines the reaction.
  ///
  /// @param paraValue vector with parameters
  ///
  /// @param indValue vector of vectors with variable indices
  ///
  /// @see BaseReaction::createReaction(std::vector<double> &paraValue,...)
  ///
  MichaelisMentenRestricted(std::vector<double> &paraValue, 
			    std::vector< std::vector<size_t> > &indValue );
  
  ///
  /// @brief Derivative function for this reaction class
  ///
  /// @see BaseReaction::derivs(Compartment &compartment,size_t species,...)
  ///
  void derivs(Compartment &compartment,size_t specie,DataMatrix &y,DataMatrix &dydt);

  ///
  /// @brief Derivative function for this reaction class calculating the absolute value for noise solvers
  ///
  /// @see BaseReaction::derivsWithAbs(Compartment &compartment,size_t species,...)
  ///
  void derivsWithAbs(Compartment &compartment,size_t species,DataMatrix &y,DataMatrix &dydt,DataMatrix &sdydt);
};

///
/// @brief This class describes a Hill-type production with activators and
/// repressors
///
/// This reaction is a Michaelis-Menten formalism for production with a
/// restricting variable given by
///
/// @f[ \frac{dy_{ij}}{dt} = p_0 \frac{y_{ik}^{p_2}}{p_1^{p_2}+y_{ik}^{p_2}}...\frac{p_{1'}^{p_{2'}}}{p_{1'}^{p_{2'}}+y_{ik'}^{p_{2'}}}@f]
///
/// where p_0 is the maximal rate (@f$V_{max}@f$), and @f$p_1@f$ is the Hill
/// constant (@f$K_{half}@f$), and @f$p_2@f$ is the Hill coefficient (n). The
/// k index is given in the first level of varIndex and corresponds to the
/// activators, and the k' in the second level which corresponds to the
/// repressors. In both layers each index corresponds to a pair of parameters
/// (K,n) that are preceded by the @f$V_{max}@f$ parameter.
///
/// In the model file the reaction will be defined (somewhat different for different number of 
/// activators and repressors) e.g. as
///
/// One activator:
/// @verbatim
/// hill 3 2 1 0
/// V K n
/// activator_index
/// @endverbatim
///
/// One repressor:
/// @verbatim
/// hill 3 2 0 1
/// V K n
/// repressor_index
/// @endverbatim
///
/// Two activators and one repressor:
/// @verbatim
/// hill 5 2 2 1
/// V K_A1 n_A1 K_A2 n_A2 K_R n_R
/// A1_index A2_index R_index
/// @endverbatim
///
class Hill : public BaseReaction {
  
 public:
  
  ///
  /// @brief Main constructor
  ///
  /// This is the main constructor which sets the parameters and variable
  /// indices that defines the reaction.
  ///
  /// @param paraValue vector with parameters
  ///
  /// @param indValue vector of vectors with variable indices
  ///
  /// @see BaseReaction::createReaction(std::vector<double> &paraValue,...)
  ///
  Hill(std::vector<double> &paraValue, 
       std::vector< std::vector<size_t> > &indValue );
  
  ///
  /// @brief Derivative function for this reaction class
  ///
  /// @see BaseReaction::derivs(Compartment &compartment,size_t species,...)
  ///
  void derivs(Compartment &compartment,size_t specie,DataMatrix &y,DataMatrix &dydt);
  
  ///
  /// @brief Derivative function for this reaction class calculating the absolute value for noise solvers
  ///
  /// @see BaseReaction::derivsWithAbs(Compartment &compartment,size_t species,...)
  ///
  void derivsWithAbs(Compartment &compartment,size_t species,DataMatrix &y,DataMatrix &dydt,DataMatrix &sdydt);

  ///
  /// @brief Jacobian function for this reaction class
  ///
  /// @see BaseReaction::Jacobian(Compartment &compartment,size_t species,...)
  ///
  size_t Jacobian(Compartment &compartment,size_t species,DataMatrix &y,JacobianMatrix &A);  

  ///
  /// @brief Propensity calculation (probability to happen in [t,t+dt]) for this reaction class
  ///
  /// @see BaseReaction::propensity(Compartment &compartment,size_t species,DataMatrix &y)
  ///
  double propensity(Compartment &compartment,size_t species,DataMatrix &y);

  ///
  /// @brief Discrete update for this reaction used in stochastic simulations
  ///
  /// @see BaseReaction::discreteUpdate(Compartment &compartment,size_t species,DataMatrix &y)
  ///
  void discreteUpdate(Compartment &compartment,size_t species,DataMatrix &y);
};

///
/// @brief This class describes a Hill-type production with activators,
///  repressors and one variable restricting @f$V_{max}@f$
///
/// This reaction is a Michaelis-Menten formalism for production with a
/// restricting variable given by
///
/// @f[ \frac{dy_{ij}}{dt} = p_0\cdot y_{Restricting} \frac{y_{ik}^{p_2}}{p_1^{p_2}+y_{ik}^{p_2}}...\frac{p_{1'}^{p_{2'}}}{p_{1'}^{p_{2'}}+y_{ik'}^{p_{2'}}}@f]
///
/// where @f$p_0 \cdot y_{Restricting}@f$ is the maximal rate (@f$V_{max}@f$), and @f$p_1@f$ is the Hill
/// constant (@f$K_{half}@f$), and @f$p_2@f$ is the Hill coefficient (n). The
/// k index is given in the first level of varIndex and corresponds to the
/// activators, and the k' in the second level which corresponds to the
/// repressors.
///
///	There are three variable layers (hillRepressed x 3 x x). @f$y_{Restricting}@f$ is given by the first layer
///
///	The second layer is for activators and the third for repressors. In both of these layers each index corresponds to a pair of parameters
/// (K,n) that are preceded by the @f$p_0@f$ parameter.
///
/// In the model file the reaction will be defined (somewhat different for different number of
/// activators and repressors) e.g. as
///
/// One activator:
/// @verbatim
/// hill 3 3 1 1 0
/// V K n
/// restrictingVariable_index
/// activator_index
/// @endverbatim
///
/// One repressor:
/// @verbatim
/// hillRestricted 3 3 1 0 1
/// V K n
/// restrictingVariable_index
/// repressor_index
/// @endverbatim
///
/// Two activators and one repressor:
/// @verbatim
/// hillrestricted 5 3 1 2 1
/// V K_A1 n_A1 K_A2 n_A2 K_R n_R
/// restrictingVariable_index
/// A1_index A2_index R_index
/// @endverbatim
///
///	The original use for this reaction was to be able to use a dummy variable in order to smoothly regulate the
///	production of a certain compound during optimizations.
///
class HillRestricted : public BaseReaction {

 public:

  ///
  /// @brief Main constructor
  ///
  /// This is the main constructor which sets the parameters and variable
  /// indices that defines the reaction.
  ///
  /// @param paraValue vector with parameters
  ///
  /// @param indValue vector of vectors with variable indices
  ///
  /// @see BaseReaction::createReaction(std::vector<double> &paraValue,...)
  ///
  HillRestricted(std::vector<double> &paraValue,
       std::vector< std::vector<size_t> > &indValue );

  ///
  /// @brief Derivative function for this reaction class
  ///
  /// @see BaseReaction::derivs(Compartment &compartment,size_t species,...)
  ///
  void derivs(Compartment &compartment,size_t specie,DataMatrix &y,DataMatrix &dydt);

  ///
  /// @brief Derivative function for this reaction class calculating the absolute value for noise solvers
  ///
  /// @see BaseReaction::derivsWithAbs(Compartment &compartment,size_t species,...)
  ///
  void derivsWithAbs(Compartment &compartment,size_t species,DataMatrix &y,DataMatrix &dydt,DataMatrix &sdydt);

  ///
  /// @brief Jacobian function for this reaction class
  ///
  /// @see BaseReaction::Jacobian(Compartment &compartment,size_t species,...)
  ///
  size_t Jacobian(Compartment &compartment,size_t species,DataMatrix &y,JacobianMatrix &A);  
};

///
/// @brief This class describes a simple Hill-type production (repression)
///
/// This reaction uses a single input and describes a Hill repression
/// production by
///
/// @f[ \frac{dy_{ij}}{dt} = p_0
/// \frac{y_{ik}^{p_2}}{p_1^{p_2}+y_{ik}^{p_2}}@f]
///
/// where p_0 is the maximal rate (@f$V_{max}@f$), and @f$p_1@f$ is the Hill
/// constant (@f$K_{half}@f$), and @f$p_2@f$ is the Hill coefficient. The
/// single k index is given in the first level of varIndex.
///
class HillRepressor : public BaseReaction {
  
 public:
  
  ///
  /// @brief Main constructor
  ///
  /// This is the main constructor which sets the parameters and variable
  /// indices that defines the reaction.
  ///
  /// @param paraValue vector with parameters
  ///
  /// @param indValue vector of vectors with variable indices
  ///
  /// @see BaseReaction::createReaction(std::vector<double> &paraValue,...)
  ///
  HillRepressor(std::vector<double> &paraValue, 
		std::vector< std::vector<size_t> > &indValue );
  
  ///
  /// @brief Derivative function for this reaction class
  ///
  /// @see BaseReaction::derivs(Compartment &compartment,size_t species,...)
  ///
  void derivs(Compartment &compartment,size_t specie,DataMatrix &y,DataMatrix &dydt);  

  ///
  /// @brief Jacobian function for this reaction class
  ///
  /// @see BaseReaction::Jacobian(Compartment &compartment,size_t species,...)
  ///
  size_t Jacobian(Compartment &compartment,size_t species,DataMatrix &y,JacobianMatrix &A);  
};

///
/// @brief This class describes a simple Hill-type production where each bound/unbound state contributes
/// and for a single transcription factor
///
/// This reaction uses a single input and describes a Hill production (activation or repression) by
///
/// @f[ \frac{dy_{ij}}{dt} =
/// \frac{p_0 p_2^{p_3} + p_1 y_{ik}^{p_3}}{p_2^{p_3}+y_{ik}^{p_3}}@f]
///
/// where @f$ p_0 @f$ is the maximal unbound rate @f$ p_1 @f$ is the maximal bound rate
/// (@f$V_{max}@f$ if the other V is set to zero), and @f$ p_2 @f$ is the Hill
/// constant (@f$K_{half}@f$), and @f$ p_3 @f$ is the Hill coefficient. The
/// single k index is given in the first level of varIndex.
///
/// In a model file the reaction looks like
/// @verbatim
/// hillGeneralOne 4 1 1
/// Vunbound Vbound K_H n_H
/// k
/// @endverbatim
///
class HillGeneralOne : public BaseReaction {
  
 public:
  
  ///
  /// @brief Main constructor
  ///
  /// This is the main constructor which sets the parameters and variable
  /// indices that defines the reaction.
  ///
  /// @param paraValue vector with parameters
  ///
  /// @param indValue vector of vectors with variable indices
  ///
  /// @see BaseReaction::createReaction(std::vector<double> &paraValue,...)
  ///
  HillGeneralOne(std::vector<double> &paraValue, 
		 std::vector< std::vector<size_t> > &indValue );
  
  ///
  /// @brief Derivative function for this reaction class
  ///
  /// @see BaseReaction::derivs(Compartment &compartment,size_t species,...)
  ///
  void derivs(Compartment &compartment,size_t specie,DataMatrix &y,DataMatrix &dydt);  
};

///
/// @brief This class describes a Hill-type production where each bound/unbound state contributes
/// and for two transcription factors
///
/// This reaction uses two inputs and describes a Hill production (activation and repression) by
///
/// @f[ \frac{dy_{ij}}{dt} =
/// \frac{p_0 p_4^{p_5} p_6^{p_7} + p_1 y_{ik}^{p_5} p_6^{p_7} + p_2 p_4^{p_5} y_{il}^{p_7} + 
/// p_3 y_{ik}^{p_5} y_{il}^{p_7} }{ (p_4^{p_5}+y_{ik}^{p_5})(p_6^{p_7}+y_{ik}^{p_7})}@f]
///
/// where @f$ p_0 @f$ is the maximal unbound rate, @f$ p_1 @f$ is the maximal rate when the first TF is bound
/// , @f$ p_2 @f$ is the maximal rate when the second TF is bound, and @f$ p_3 @f$ is the maximal rate when
/// both TFs are bound (@f$V_{max}@f$ if the other Vs are set to zero). @f$ p_4,p_6 @f$ are the Hill
/// constants for the individual TFs (@f$K_{half}@f$), and @f$ p_5,p_7 @f$ are the Hill coefficients. The
/// two indices (@f$ k,l @f$) are given in the first level of varIndex.
///
/// In a model file the reaction looks like
/// @verbatim
/// hillGeneralTwo 8 1 2
/// V_unbound V_firstbound V_secondbound V_bothbound K_H1 n_H1 K_H2 n_H2 
/// k l
/// @endverbatim
///
class HillGeneralTwo : public BaseReaction {
  
 public:
  
  ///
  /// @brief Main constructor
  ///
  /// This is the main constructor which sets the parameters and variable
  /// indices that defines the reaction.
  ///
  /// @param paraValue vector with parameters
  ///
  /// @param indValue vector of vectors with variable indices
  ///
  /// @see BaseReaction::createReaction(std::vector<double> &paraValue,...)
  ///
  HillGeneralTwo(std::vector<double> &paraValue, 
		 std::vector< std::vector<size_t> > &indValue );
  
  ///
  /// @brief Derivative function for this reaction class
  ///
  /// @see BaseReaction::derivs(Compartment &compartment,size_t species,...)
  ///
  void derivs(Compartment &compartment,size_t specie,DataMatrix &y,DataMatrix &dydt);  

  ///
  /// @brief Derivative function for this reaction class calculating the absolute value for noise solvers
  ///
  /// @see BaseReaction::derivsWithAbs(Compartment &compartment,size_t species,...)
  ///
  void derivsWithAbs(Compartment &compartment,size_t species,DataMatrix &y,DataMatrix &dydt,DataMatrix &sdydt);
};

///
/// @brief This class describes a Hill-type production where each bound/unbound state contributes
/// and for three transcription factors
///
/// This reaction uses three inputs and describes a Hill production (activation and repression) by
///
/// @f[ \frac{dy_{ij}}{dt} =
/// \frac{p_0 p_8^{p_9} p_{10}^{p_{11}} p_{12}^{p_{13}} + p_1 y_{ik}^{p_9} p_{10}^{p_{11}} p_{12}^{p_{13}}
/// + p_2 p_8^{p_9} y_{jl}^{p_{11}} p_{12}^{p_{13}} + p_3 p_8^{p_9} p_{10}^{p_{11}} y_{jm}^{p_{13}}
/// + p_4 y_{jk}^{p_9} y_{il}^{p_{11}} p_{12}^{p_{13}} + p_5 y_{ik}^{p_9} p_{10}^{p_{11}} y_{im}^{p_{13}}
/// + p_6 p_8^{p_9} y_{il}^{p_{11}} y_{im}^{p_{13}} + p_7 y_{ik}^{p_9} y_{il}^{p_{11}} y_{im}^{p_{13}}  }
/// { (p_8^{p_9}+y_{ik}^{p_9})(p_{10}^{p_{11}}+y_{il}^{p_{11}})(p_{12}^{p_13}+y_{im}^{p_{13}})}@f]
///
/// where @f$ p_0 @f$ is the maximal unbound rate, @f$ p_1 @f$ is the maximal rate when the first TF is bound
/// , @f$ p_2 @f$ is the maximal rate when the second TF is bound, and @f$ p_3 @f$ is the maximal rate when
/// the third TF is bound. For two bound TFs the maximal rates are given by @f$ p_4 @f$ (first,second), 
/// @f$ p_5 @f$ (first,third), and @f$ p_6 @f$ (second,third), and maximal rate or production for all 
/// three TFs bound is given by @f$ p_7 @f$. 
/// @f$ p_8,p_{10},p_{12} @f$ are the Hill
/// constants for the individual TFs (@f$K_{half}@f$), and @f$ p_9,p_{11},p_{}13 @f$ are the Hill coefficients. The
/// three indices (@f$ k,l,m @f$) are given in the first level of varIndex.
///
/// In a model file the reaction looks like
/// @verbatim
/// hillGeneralThree 14 1 3
/// V_000 V_100 V_010 V_001 V_110 V_101 V_011 V_111 K_H1 n_H1 K_H2 n_H2 K_H3 n_H3 
/// k l m
/// @endverbatim
///
class HillGeneralThree : public BaseReaction {
  
 public:
  
  ///
  /// @brief Main constructor
  ///
  /// This is the main constructor which sets the parameters and variable
  /// indices that defines the reaction.
  ///
  /// @param paraValue vector with parameters
  ///
  /// @param indValue vector of vectors with variable indices
  ///
  /// @see BaseReaction::createReaction(std::vector<double> &paraValue,...)
  ///
  HillGeneralThree(std::vector<double> &paraValue, 
		   std::vector< std::vector<size_t> > &indValue );
  
  ///
  /// @brief Derivative function for this reaction class
  ///
  /// @see BaseReaction::derivs(Compartment &compartment,size_t species,...)
  ///
  void derivs(Compartment &compartment,size_t specie,DataMatrix &y,DataMatrix &dydt);  
};

///
/// @brief This class describes a SheaAckers-type production with activators and
/// repressors
///
/// This reaction is a Shea-Ackers formalism for transcription given by
///
/// @f[ \frac{dy_{ij}}{dt} = \frac{a_0 + \sum_k a_k y_{ik}}{1 + \sum_l b_l y_{il}} @f]
///
/// The k index is given in the first level of varIndex and corresponds to the
/// activators, and the l in the second level which corresponds to the
/// repressors. For each variable there is one parameter.
///
/// In a model file the reaction is given as
///
/// @verbatim
/// sheaAckers (N_A+N_R+1) 2 N_A N_R
/// p_0 p_A1 ... p_AN p_R1 ... p_RN
/// A_1 ... A_N
/// R_1 ... R_N
/// @endverbatim
///
/// where the number of parameters (@f$N_A+N_R+1@f$) is one more than the number of activators and 
/// repressors, Activator indices are given in the first and repressors in the second level.
///
/// For example if we have one activator X and otwo repressors Y,Z it will be
///
/// @verbatim
/// sheaAckers 4 2 1 2
/// p_0 p_X p_Y p_Z
/// X_index
/// Y_index Z_index
/// @endverbatim
 
class SheaAckers : public BaseReaction {
 public:
  
  ///
  /// @brief Main constructor
  ///
  /// This is the main constructor which sets the parameters and variable
  /// indices that defines the reaction.
  ///
  /// @param paraValue vector with parameters
  ///
  /// @param indValue vector of vectors with variable indices
  ///
  /// @see BaseReaction::createReaction(std::vector<double> &paraValue,...)
  ///
  SheaAckers(std::vector<double> &paraValue, 
	     std::vector< std::vector<size_t> > &indValue );
  
  ///
  /// @brief Derivative function for this reaction class
  ///
  /// @see BaseReaction::derivs(Compartment &compartment,size_t species,...)
  ///
  void derivs(Compartment &compartment,size_t specie,DataMatrix &y,DataMatrix &dydt);
  
  ///
  /// @brief Jacobian function for this reaction class
  ///
  /// @see BaseReaction::Jacobian(Compartment &compartment,size_t species,...)
  ///
  size_t Jacobian(Compartment &compartment,size_t species,DataMatrix &y,JacobianMatrix &A);  
};


///
/// @brief The non-competitive binding (NonComBinding) class describes a Hill-type production where each bound/unbound state contributes
/// and for two transcription factors
///
/// This reaction uses two inputs and describes a Hill production (activation and repression) by
///
/// @f[ \frac{dy_{ij}}{dt} =
/// \frac{p_0 + p_1 y_{ik}^{p_5}/p_4^{p_5} + p_2 y_{il}^{p_8}/p_6^{p_8} + 
/// p_3 y_{ik}^{p_5} y_{il}^{p_8} /(p_4^{p_5}*p_7^{p_8}) }{ 1 + y_{ik}^{p_5}/p_4^{p_5} + y_{il}^{p_8}/p_6^{p_8} + y_{ik}^{p_5} y_{il}^{p_8} /(p_4^{p_5}*p_7^{p_8}) }@f]
///
/// where @f$ p_0 @f$ is the maximal unbound rate, @f$ p_1 @f$ is the maximal rate when the first TF is bound
/// , @f$ p_2 @f$ is the maximal rate when the second TF is bound, and @f$ p_3 @f$ is the maximal rate when
/// both TFs are bound (@f$V_{max}@f$ if the other Vs are set to zero). @f$ p_4,p_6, p_7 @f$ are the Hill
/// constants for the individual TFs (@f$K_1, K_2, K'_2@f$ respectively), and @f$ p_5,p_8 @f$ are the Hill coefficients. Note that the second TF binds with a strength that is dependent on whether the first TF is bound or not. The
/// two indices (@f$ k,l @f$) are given in the first level of varIndex.
///
/// In a model file the reaction looks like
/// @verbatim
/// nonComBinding 9 1 2
/// V_unbound V_firstbound V_secondbound V_bothbound K_1 n_H1 K_2 K'_2 n_H2 
/// k l
/// @endverbatim
///
class NonComBinding : public BaseReaction {
  
 public:
  
  ///
  /// @brief Main constructor
  ///
  /// This is the main constructor which sets the parameters and variable
  /// indices that defines the reaction.
  ///
  /// @param paraValue vector with parameters
  ///
  /// @param indValue vector of vectors with variable indices
  ///
  /// @see BaseReaction::createReaction(std::vector<double> &paraValue,...)
  ///
  NonComBinding(std::vector<double> &paraValue, 
		 std::vector< std::vector<size_t> > &indValue );
  
  ///
  /// @brief Derivative function for this reaction class
  ///
  /// @see BaseReaction::derivs(Compartment &compartment,size_t species,...)
  ///
  void derivs(Compartment &compartment,size_t specie,DataMatrix &y,DataMatrix &dydt);  
};


///
/// @brief This class describes a SheaAckers-type production with activators and
/// repressors
///
/// This reaction is a Shea-Ackers formalism for transcription given by
///
/// @f[ \frac{dy_{ij}}{dt} = \frac{a_0 + \sum_k a_k y_{ik} + \sum_l a_l y_{il1}y_{il2}}
/// {1 + \sum_m b_m y_{im}} + \sum_n a_n y_{in1}y_{in2} @f]
///
/// The k index is given in the first level of varIndex and corresponds to the
/// activators, and the l in the second level which corresponds to the
/// diemer activators, m is the repressors and n are the dimer repressors. 
/// For each single activator/repressor variable there is one parameter, and for each dimer pair
/// there is one parameter, and in addition there is a contant (parameter(0)) in the nominator.
///
class SheaAckersDimer : public BaseReaction {
	
 public:
  
  ///
  /// @brief Main constructor
  ///
  /// This is the main constructor which sets the parameters and variable
  /// indices that defines the reaction.
  ///
  /// @param paraValue vector with parameters
  ///
  /// @param indValue vector of vectors with variable indices
  ///
  /// @see BaseReaction::createReaction(std::vector<double> &paraValue,...)
  ///
  SheaAckersDimer(std::vector<double> &paraValue, 
	     std::vector< std::vector<size_t> > &indValue );
  
  ///
  /// @brief Derivative function for this reaction class
  ///
  /// @see BaseReaction::derivs(Compartment &compartment,size_t species,...)
  ///
  void derivs(Compartment &compartment,size_t specie,DataMatrix &y,DataMatrix &dydt);
  
  ///
  /// @brief Jacobian function for this reaction class
  ///
  /// @see BaseReaction::Jacobian(Compartment &compartment,size_t species,...)
  ///
  size_t Jacobian(Compartment &compartment,size_t species,DataMatrix &y,JacobianMatrix &A);  
};


#endif
