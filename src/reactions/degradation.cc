//
// Filename     : degradation.cc
// Description  : Classes describing degradation updates
// Author(s)    : Henrik Jonsson (henrik@thep.lu.se)
// Created      : October 2003
// Revision     : $Id: degradation.cc 646 2015-08-23 18:35:06Z henrik $
//

#include<cmath>
#include"baseReaction.h"
#include"degradation.h"
#include"../organism.h"

DegradationLinear::DegradationLinear(std::vector<double> &paraValue, 
				     std::vector< std::vector<size_t> > 
				     &indValue ) 
{  
	// Do some checks on the parameters and variable indeces
	if( paraValue.size()!=1 ) {
		std::cerr << "DegradationLinear::DegradationLinear() "
			  << "Uses only one parameter k_deg (lambda)\n";
		exit(0);
	}
	if( indValue.size() != 0 && indValue.size() !=1 ) {
		std::cerr << "DegradationLinear::DegradationLinear() "
			  << "At most one level of variable indices allowed"
			  << std::endl;
		exit(0);
	}

	// Set the variable values
	setId("degradationLinear");
	setParameter(paraValue);  
	setVariableIndex(indValue);
  
	// Set the parameter identities
	std::vector<std::string> tmp( numParameter() );
	tmp.resize( numParameter() );
	tmp[0] = "k_deg";
	setParameterId( tmp );
}

void DegradationLinear::
derivs(Compartment &compartment,size_t varIndex,DataMatrix &y,DataMatrix &dydt) 
{  
	double rate = parameter(0)*y[compartment.index()][varIndex];
	if( numVariableIndexLevel() )
		for( size_t k=0; k<numVariableIndex(0); ++k )
			rate *= y[compartment.index()][ variableIndex(0,k) ];
	dydt[compartment.index()][varIndex] -= rate;    
}

void DegradationLinear::
derivsWithAbs(Compartment &compartment,size_t varIndex,DataMatrix &y,DataMatrix &dydt,DataMatrix &sdydt) 
{  
	double rate = parameter(0)*y[compartment.index()][varIndex];
	if( numVariableIndexLevel() )
		for( size_t k=0; k<numVariableIndex(0); ++k )
			rate *= y[compartment.index()][ variableIndex(0,k) ];
	dydt[compartment.index()][varIndex] -= rate;    
	sdydt[compartment.index()][varIndex] += rate;    
}

DegradationOne::DegradationOne(std::vector<double> &paraValue, 
			       std::vector< std::vector<size_t> > 
			       &indValue ) 
{  
	// Do some checks on the parameters and variable indeces
	if( paraValue.size()!=1 ) {
		std::cerr << "DegradationOne::DegradationOne() "
			  << "Uses only one parameter k_deg (lambda)\n";
		exit(0);
	}
	if( indValue.size() ) {
		std::cerr << "DegradationOne::DegradationOne() "
			  << "No variable index are used.\n";
		exit(0);
	}

	// Set the variable values
	setId("degradationOne");
	setParameter(paraValue);  
	setVariableIndex(indValue);
  
	// Set the parameter identities
	std::vector<std::string> tmp( numParameter() );
	tmp.resize( numParameter() );
	tmp[0] = "k_deg";
	setParameterId( tmp );
}

void DegradationOne::
derivs(Compartment &compartment,size_t varIndex,DataMatrix &y,DataMatrix &dydt) 
{  
	dydt[compartment.index()][varIndex] -= parameter(0)*
		y[compartment.index()][varIndex];
}

void DegradationOne::
derivsWithAbs(Compartment &compartment,size_t varIndex,DataMatrix &y,DataMatrix &dydt,DataMatrix &sdydt) 
{  
  double rate = parameter(0)*y[compartment.index()][varIndex];
  dydt[compartment.index()][varIndex] -= rate;
  sdydt[compartment.index()][varIndex] += rate;
}

size_t DegradationOne::
Jacobian(Compartment &compartment,size_t varIndex,DataMatrix &y,JacobianMatrix &A) 
{  
	size_t i = compartment.index()*y[0].size() + varIndex;
	A(i,i) -= parameter(0);
	return 0;
}

double DegradationOne::
propensity(Compartment &compartment,size_t varIndex,DataMatrix &y)
{
  double prop = parameter(0)*y[compartment.index()][varIndex]; 
  if (prop<=0.0) 
    return 0.0;
  return prop;
}

void DegradationOne::
discreteUpdate(Compartment &compartment,size_t varIndex,DataMatrix &y)
{
  y[compartment.index()][varIndex] -= 1.0;
}

void DegradationOne::printCambium( std::ostream &os, size_t varIndex ) const
{
  std::string varName=organism()->variableId(varIndex);
  os << "Arrow[Organism[" << id() << "],{" << varName << "[i]},{},{},Parameters[";
  os << parameter(0);
  os << "], ParameterNames[" << parameterId(0) << "], VarIndices[],"; 
  os << "Solve{" << varName << "[i]\' = - p_0 " << varName << "[i], "
     << "Using[" << organism()->topology().id() << "::Cell, i]}";
  os << "]" << std::endl;
}

DegradationTwo::DegradationTwo(std::vector<double> &paraValue, 
			       std::vector< std::vector<size_t> > 
			       &indValue ) 
{  
	// Do some checks on the parameters and variable indeces
	if( paraValue.size()!=1 ) {
		std::cerr << "DegradationTwo::DegradationTwo() "
			  << "Uses only one parameter k_deg\n";
		exit(0);
	}
	if( indValue.size() != 1 || indValue[0].size() != 1 ) {
		std::cerr << "DegradationTwo::DegradationTwo() "
			  << "One variable index is used.\n";
		exit(0);
	}

	// Set the variable values
	setId("degradationTwo");
	setParameter(paraValue);  
	setVariableIndex(indValue);
  
	// Set the parameter identities
	std::vector<std::string> tmp( numParameter() );
	tmp.resize( numParameter() );
	tmp[0] = "k_deg";
	setParameterId( tmp );
}

void DegradationTwo::
derivs(Compartment &compartment,size_t varIndex,DataMatrix &y,DataMatrix &dydt) 
{
	dydt[compartment.index()][varIndex] -= parameter(0)*
		y[compartment.index()][varIndex]*
		y[compartment.index()][variableIndex(0,0)];
}

void DegradationTwo::
derivsWithAbs(Compartment &compartment,size_t varIndex,DataMatrix &y,DataMatrix &dydt,DataMatrix &sdydt) 
{  
  double rate = parameter(0)*y[compartment.index()][varIndex]*y[compartment.index()][variableIndex(0,0)];
  dydt[compartment.index()][varIndex] -= rate;
  sdydt[compartment.index()][varIndex] += rate;
}

size_t DegradationTwo::
Jacobian(Compartment &compartment,size_t varIndex,DataMatrix &y,JacobianMatrix &A) 
{  
	size_t i = compartment.index()*y[0].size() + varIndex;
	A(i,i) -= parameter(0)*y[compartment.index()][variableIndex(0,0)];
	size_t j = compartment.index()*y[0].size() + variableIndex(0,0);
	A(i,j) -= parameter(0)*y[compartment.index()][varIndex];
	return 1;
}

double DegradationTwo::
propensity(Compartment &compartment,size_t varIndex,DataMatrix &y)
{
  double prop = parameter(0)*y[compartment.index()][varIndex]; 
	// x(x-1)
	if (varIndex==variableIndex(0,0)) {
		prop *= (y[compartment.index()][varIndex]-1);
	}
	// xy
	else {
		prop *= y[compartment.index()][variableIndex(0,0)];
	}
  if (prop<=0.0) 
    return 0.0;
  return prop;
}

void DegradationTwo::
discreteUpdate(Compartment &compartment,size_t varIndex,DataMatrix &y)
{
  y[compartment.index()][varIndex] -= 1.0;
}

void DegradationTwo::printCambium( std::ostream &os, size_t varIndex ) const
{
  std::string varName=organism()->variableId(varIndex);
  std::string varNameDeg=organism()->variableId(variableIndex(0,0));
  os << "Arrow[Organism[" << id() << "],{" << varName << "[i]},{"
     << varNameDeg
     << "[i]},{},Parameters[";
  os << parameter(0);
  os << "], ParameterNames[" << parameterId(0) << "], VarIndices[{"
     << variableIndex(0,0) << "}],"; 
  os << "Solve{" << varName << "[i]\' = - p_0 " << varName << "[i] "
     << varNameDeg << "[i], "
     << "Using[" << organism()->topology().id() << "::Cell, i]}";
  os << "]" << std::endl;
}

DegradationThree::DegradationThree(std::vector<double> &paraValue, 
																	 std::vector< std::vector<size_t> > 
																	 &indValue ) 
{  
	// Do some checks on the parameters and variable indeces
	if( paraValue.size()!=1 ) {
		std::cerr << "DegradationThree::DegradationThree() "
							<< "Uses only one parameter k_deg\n";
		exit(0);
	}
	if( indValue.size() != 1 || indValue[0].size() != 2 ) {
		std::cerr << "DegradationThree::DegradationThree() "
							<< "Two variable indices are used.\n";
		exit(0);
	}
	
	// Set the variable values
	setId("degradationThree");
	setParameter(paraValue);  
	setVariableIndex(indValue);
  
	// Set the parameter identities
	std::vector<std::string> tmp( numParameter() );
	tmp.resize( numParameter() );
	tmp[0] = "k_deg";
	setParameterId( tmp );
}

void DegradationThree::
derivs(Compartment &compartment,size_t varIndex,DataMatrix &y,DataMatrix &dydt) 
{  
	dydt[compartment.index()][varIndex] -= parameter(0)*
		y[compartment.index()][varIndex]*
		y[compartment.index()][variableIndex(0,0)]*y[compartment.index()][variableIndex(0,1)];
}

DegradationHill::DegradationHill(std::vector<double> &paraValue, 
				 std::vector< std::vector<size_t> > 
				 &indValue ) 
{  
  // Do some checks on the parameters and variable indeces
  if( paraValue.size()!=3 ) {
    std::cerr << "DegradationHill::DegradationHill() "
	      << "Uses three parameters k_deg K_hill n_hill\n";
    exit(0);
  }
  if( ! ((indValue.size()==1 && indValue[0].size() == 1 ) ||
	 (indValue.size()==2 && indValue[1].size() == 1 ))) {
    std::cerr << "DegradationHill::DegradationHill() "
	      << "Can be used in the following ways:\n"
	      << "No parameter index\n"
	      << "One parameter index, for variable in Hill function\n"
	      << "Two parameter indices, first will be restricting variables, second is variable in Hill function\n";
    exit(0);
  }
  
  // Set the variable values
  setId("degradationHill");
  setParameter(paraValue);  
  setVariableIndex(indValue);
  
  // Set the parameter identities
  std::vector<std::string> tmp( numParameter() );
  tmp.resize( numParameter() );
  tmp[0] = "k_deg";
  tmp[1] = "K_hill";
  tmp[2] = "n_hill";
  setParameterId( tmp );
}

void DegradationHill::
derivs(Compartment &compartment,size_t varIndex,DataMatrix &y,DataMatrix &dydt) 
{  
  size_t degraderIndex;
  if(numVariableIndexLevel() ==1)
    degraderIndex = variableIndex(0,0);
  else if(numVariableIndexLevel() == 2)
    degraderIndex = variableIndex(1,0);

  double fac = parameter(0);

  // multiply with possible restricting variables
  if (numVariableIndexLevel() ==2 ) {
    for( size_t k=0; k<numVariableIndex(0); ++k )
      fac *= y[compartment.index()][ variableIndex(0,k) ];
  }
  
  // multiply with own concentration
  fac *= y[compartment.index()][ varIndex ];

  // multiply derivative with fac and hill factor
  double Kpow = std::pow(parameter(1),parameter(2));
  double yPow = std::pow(y[compartment.index()][degraderIndex],parameter(2));
  dydt[compartment.index()][varIndex] -= fac*yPow/(Kpow+yPow);

}
  
DegradationHillR::DegradationHillR(std::vector<double> &paraValue, 
				   std::vector< std::vector<size_t> > 
				   &indValue ) 
{  
  // Do some checks on the parameters and variable indeces
  if( paraValue.size()!=3 ) {
    std::cerr << "DegradationHillR::DegradationHillR() "
	      << "Uses three parameters k_deg K_hill n_hill\n";
    exit(0);
  }
  if( indValue.size() != 1 || indValue[0].size() != 1 ) {
    std::cerr << "DegradationHillR::DegradationHillR() "
	      << "One variable index is used.\n";
    exit(0);
  }
  
  // Set the variable values
  setId("degradationThree");
  setParameter(paraValue);  
  setVariableIndex(indValue);
  
  // Set the parameter identities
  std::vector<std::string> tmp( numParameter() );
  tmp.resize( numParameter() );
  tmp[0] = "k_deg";
  tmp[1] = "K_hill";
  tmp[2] = "n_hill";
  setParameterId( tmp );
}

void DegradationHillR::
derivs(Compartment &compartment,size_t varIndex,DataMatrix &y,DataMatrix &dydt) 
{  
  double Kpow = std::pow(parameter(1),parameter(2));
	double yPow = std::pow(y[compartment.index()][variableIndex(0,0)],parameter(2));
	dydt[compartment.index()][varIndex] -= parameter(0)*
		y[compartment.index()][varIndex]*Kpow/(Kpow+yPow);

}

DegradationSpatialThreshold::DegradationSpatialThreshold(std::vector<double> &paraValue, 
							 std::vector< std::vector<size_t> > 
							 &indValue ) 
{  
  // Do some checks on the parameters and variable indeces
  if( paraValue.size()!=3 ) {
    std::cerr << "DegradationSpatialThreshold::DegradationSpatialThreshold() "
	      << "Uses three parameters k_d X_th X_sign\n";
    exit(0);
  }
  if( indValue.size() != 1 || indValue[0].size() != 1 ) {
    std::cerr << "DegradationSpatialThreshold::DegradationSpatialThreshold()"
	      << "Variable index indicating threshold variable needed.\n";
    exit(0);
  }
  //Sign should be -/+1
  if( paraValue[2] != -1 && paraValue[2] != 1 ) {
    std::cerr << "DegradationSpatialThreshold::DegradationSpatialThreshold() "
	      << "X_sign should be +/-1\n";
    exit(0);
  }
	
  // Set the variable values
  setId("degradationSpatialThreshold");
  setParameter(paraValue);  
  setVariableIndex(indValue);
  
  // Set the parameter identities
  std::vector<std::string> tmp( numParameter() );
  tmp.resize( numParameter() );
  tmp[0] = "k_d";
  tmp[1] = "X_th";
  tmp[2] = "X_sign";  
  setParameterId( tmp );
}

void DegradationSpatialThreshold::
derivs(Compartment &compartment,size_t varIndex,DataMatrix &y,DataMatrix &dydt) 
{  
  if( y[compartment.index()][variableIndex(0,0)]*parameter(2)>parameter(1)*parameter(2) )
    dydt[compartment.index()][varIndex] -= parameter(0) * y[compartment.index()][varIndex];
}

DegradationSpatialThresholdHill::DegradationSpatialThresholdHill(std::vector<double> &paraValue, 
							 std::vector< std::vector<size_t> > 
							 &indValue ) 
{  
  // Do some checks on the parameters and variable indeces
  if( paraValue.size()!=4 ) {
    std::cerr << "DegradationSpatialThresholdHill::DegradationSpatialThresholdHill() "
	      << "Uses four parameters k_d, X_th, n, X_sign\n";
    exit(0);
  }
  if( indValue.size() != 1 || indValue[0].size() != 1 ) {
    std::cerr << "DegradationSpatialThresholdHill::DegradationSpatialThresholdHill()"
	      << "Variable index indicating threshold variable needed.\n";
    exit(0);
  }
  //Sign should be -/+1
  if( paraValue[3] != -1 && paraValue[2] != 1 ) {
    std::cerr << "DegradationSpatialThresholdHill::DegradationSpatialThresholdHill() "
	      << "X_sign should be +/-1\n";
    exit(0);
  }
	
  // Set the variable values
  setId("degradationSpatialThresholdHill");
  setParameter(paraValue);  
  setVariableIndex(indValue);
  
  // Set the parameter identities
  std::vector<std::string> tmp( numParameter() );
  tmp.resize( numParameter() );
  tmp[0] = "k_d";
  tmp[1] = "X_th";
  tmp[2] = "n";
  tmp[3] = "X_sign";  
  setParameterId( tmp );
}

void DegradationSpatialThresholdHill::
derivs(Compartment &compartment,size_t varIndex,DataMatrix &y,DataMatrix &dydt) 
{  
  if (parameter(3) == 1) 
    {dydt[compartment.index()][varIndex] -= y[compartment.index()][varIndex] * parameter(0) * std::pow(y[compartment.index()][variableIndex(0,0)],parameter(2)) / (std::pow(y[compartment.index()][variableIndex(0,0)],parameter(2)) + std::pow(parameter(1),parameter(2)));}
  else {dydt[compartment.index()][varIndex] -= y[compartment.index()][varIndex] * parameter(0) * std::pow(parameter(1),parameter(2)) / (std::pow(y[compartment.index()][variableIndex(0,0)],parameter(2)) + std::pow(parameter(1),parameter(2)));}
}
