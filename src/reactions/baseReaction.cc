//
// Filename     : baseReaction.cc
// Description  : A base class describing variable updates
// Author(s)    : Henrik Jonsson (henrik@thep.lu.se)
// Created      : October 2003
// Revision     : $Id: baseReaction.cc 674 2017-05-02 10:09:29Z korsbo $
//

#include <vector>

#include "../organism.h"
#include "baseReaction.h"
#include <boost/algorithm/string.hpp>

#include "adhocReaction.h"
#include "completeModel.h"
#include "creation.h"
#include "degradation.h"
#include "grn.h"
#include "growth.h"
#include "massAction.h"
#include "mechanical.h"
#include "transport.h"
#include "rootTransport.h"
#include "rootTransport2D.h"
#include "meshedRoot.h"
#include "elongatedBacteria.h"
#include "sphericalBacteria.h"
#include "simplifiedAuxinTransportJTB/auxinTransport.h"
#include "clawus/clawusDynamics.h"
#include "clawus/clvcrn.h"
#include "BenchMarkMendez.h"
#include "parameterUpdate.h"
#include "stochastic.h"

BaseReaction::~BaseReaction()
{
}

BaseReaction *
BaseReaction::createReaction(std::vector<double> &paraValue,
			     std::vector< std::vector<size_t> > &indValue, 
			     const std::string &idValue ) 
{
	std::cerr << idValue << "\n";
	//Grn type of (creation) reactions
	//grn.h,grn.cc
	if( boost::iequals(idValue,"grn") ) 
		return new Grn(paraValue,indValue);
	else if( boost::iequals(idValue,"gsrn") )
		return new Gsrn(paraValue,indValue);
	else if(boost::iequals(idValue,"gsrn2"))
		return new Gsrn2(paraValue,indValue);
	else if(boost::iequals(idValue,"michaelisMenten"))
		return new MichaelisMenten(paraValue,indValue);  
	else if(boost::iequals(idValue,"michaelisMentenRestricted"))
		return new MichaelisMentenRestricted(paraValue,indValue);  
	else if(boost::iequals(idValue,"hill"))
		return new Hill(paraValue,indValue);  
	//else if(boost::iequals(idValue,"hillRestricted"))
	else if(boost::iequals(idValue,"hillrestricted"))
		return new HillRestricted(paraValue,indValue);  
	else if(boost::iequals(idValue,"hillRepressor"))
		return new HillRepressor(paraValue,indValue);  
	else if(boost::iequals(idValue,"hillGeneralOne"))
		return new HillGeneralOne(paraValue,indValue);  
	else if(boost::iequals(idValue,"hillGeneralTwo"))
		return new HillGeneralTwo(paraValue,indValue);  
	else if(boost::iequals(idValue,"hillGeneralThree"))
		return new HillGeneralThree(paraValue,indValue);  
	else if(boost::iequals(idValue,"nonComBinding"))
		return new NonComBinding(paraValue,indValue);  
	else if(boost::iequals(idValue,"sheaAckers"))
		return new SheaAckers(paraValue,indValue);  
	else if(boost::iequals(idValue,"sheaAckersDimer"))
		return new SheaAckersDimer(paraValue,indValue);  
	//Creation
	//creation.h,creation.cc
	else if(boost::iequals(idValue,"creationLinear"))
		return new CreationLinear(paraValue,indValue);
	else if(boost::iequals(idValue,"creationZero"))
		return new CreationZero(paraValue,indValue);
	else if(boost::iequals(idValue,"creationOne"))
		return new CreationOne(paraValue,indValue);
	else if(boost::iequals(idValue,"creationTwo"))
		return new CreationTwo(paraValue,indValue);
	else if(boost::iequals(idValue,"creationSpatialThreshold"))
		return new CreationSpatialThreshold(paraValue,indValue);
	else if(boost::iequals(idValue,"creationSpatialSphere"))
		return new CreationSpatialSphere(paraValue,indValue);
	else if(boost::iequals(idValue,"creationSpatialParabolaSphere"))
		return new CreationSpatialParabolaSphere(paraValue,indValue);		
	else if(boost::iequals(idValue,"creationSpatialParabolaSphereGrowth"))
		return new CreationSpatialParabolaSphereGrowth(paraValue,indValue);		
	else if(boost::iequals(idValue,"creationPrimordium"))
		return new CreationPrimordium(paraValue,indValue);		
	else if(boost::iequals(idValue,"creationSpatialSphereCylinder"))
		return new CreationSpatialSphereCylinder(paraValue,indValue);
	else if(boost::iequals(idValue,"creationSpatialMax"))
		return new CreationSpatialMax(paraValue,indValue);
	else if(boost::iequals(idValue,"creationExponent"))
		return new CreationExponent(paraValue,indValue);
	else if(boost::iequals(idValue,"creationZeroDegradationOne"))
		return new CreationZeroDegradationOne(paraValue,indValue);
	else if(boost::iequals(idValue,"creationZeroDegradationTwo"))
		return new CreationZeroDegradationTwo(paraValue,indValue);
	else if(boost::iequals(idValue,"creationOneDegradationOne"))
		return new CreationOneDegradationOne(paraValue,indValue);
	else if(boost::iequals(idValue,"creationOneDegradationTwo"))
		return new CreationOneDegradationTwo(paraValue,indValue);
	else if(boost::iequals(idValue,"constant"))
		return new Constant(paraValue,indValue);
	else if(boost::iequals(idValue,"creationNeighbor"))
		return new CreationNeighbor(paraValue,indValue);
	else if(boost::iequals(idValue,"creationSinus"))
		return new CreationSinus(paraValue,indValue);
	else if(boost::iequals(idValue,"constantInterval"))
		return new ConstantInterval(paraValue,indValue);
	//Degradation
	//degradation.h,degradation.cc
	else if(boost::iequals(idValue,"degradationLinear"))
		return new DegradationLinear(paraValue,indValue);
	else if(boost::iequals(idValue,"degradationOne"))
		return new DegradationOne(paraValue,indValue);
	else if(boost::iequals(idValue,"degradationTwo"))
		return new DegradationTwo(paraValue,indValue);
	else if(boost::iequals(idValue,"degradationThree"))
		return new DegradationThree(paraValue,indValue);
	else if(boost::iequals(idValue,"degradationHill"))
		return new DegradationHill(paraValue,indValue);
	else if(boost::iequals(idValue,"degradationHillR"))
		return new DegradationHillR(paraValue,indValue);
	else if(boost::iequals(idValue,"degradationSpatialThreshold"))
	  return new DegradationSpatialThreshold(paraValue,indValue);
	else if(boost::iequals(idValue,"degradationSpatialThresholdHill"))
	  return new DegradationSpatialThresholdHill(paraValue,indValue);
	//Stochastic reactions
	//stochastic.h,stochastic.cc
	else if( boost::iequals(idValue,"stochastic::gammaOU") || boost::iequals(idValue,"stochastic::GammaOU"))
	  return new stochastic::gammaOU(paraValue,indValue);
	//Mass action types of reactions
	//massAction.h,massAction.cc
	else if( boost::iequals(idValue,"massAction") || boost::iequals(idValue,"massAction::general"))
	  return new MassAction::General(paraValue,indValue);
	else if( boost::iequals(idValue,"massAction::OneToOne") || boost::iequals(idValue,"massAction1to1"))
	  return new MassAction::OneToOne(paraValue,indValue);
	else if( boost::iequals(idValue,"massAction:OneToTwo") || boost::iequals(idValue,"massAction1to2"))
	  return new MassAction::OneToTwo(paraValue,indValue);
	else if( boost::iequals(idValue,"massAction::OneToThree") || boost::iequals(idValue,"massAction1to3"))
	  return new MassAction::OneToThree(paraValue,indValue);
	else if( boost::iequals(idValue,"massAction::OneToFour") || boost::iequals(idValue,"massAction1to4"))
	  return new MassAction::OneToFour(paraValue,indValue);
	else if( boost::iequals(idValue,"massAction::TwoToOne") || boost::iequals(idValue,"massAction2to1"))
	  return new MassAction::TwoToOne(paraValue,indValue);
	else if( boost::iequals(idValue,"massAction::ThreeToOne") || boost::iequals(idValue,"massAction3to1"))
	  return new MassAction::ThreeToOne(paraValue,indValue);
	else if( boost::iequals(idValue,"massAction::Enzymatic") || boost::iequals(idValue,"massActionEnzymatic"))
	  return new MassAction::Enzymatic(paraValue,indValue);
	else if( boost::iequals(idValue,"massAction::MM") || boost::iequals(idValue,"massActionMM"))
	  return new MassAction::MM(paraValue,indValue);
	else if( boost::iequals(idValue,"massAction::EnzymaticMM") || boost::iequals(idValue,"massActionEnzymaticMM"))
	  return new MassAction::EnzymaticMM(paraValue,indValue);
	else if( boost::iequals(idValue,"massAction::MMFraction") || boost::iequals(idValue,"massActionMMFraction"))
	  return new MassAction::MMFraction(paraValue,indValue);  
	else if( boost::iequals(idValue,"massAction::EnzymaticHill") || boost::iequals(idValue,"massActionEnzymaticHill"))
	  return new MassAction::EnzymaticHill(paraValue,indValue);

	//
	// Transport reactions
	//
	//transport/diffusion.h
	else if(boost::iequals(idValue,"diffusion"))
		return new Diffusion(paraValue,indValue);
	else if(boost::iequals(idValue,"diffusionSimple"))
		return new DiffusionSimple(paraValue,indValue);
	else if(boost::iequals(idValue,"diffusionSimpleSpatial"))
		return new DiffusionSimpleSpatial(paraValue,indValue);
	else if(boost::iequals(idValue,"diffusionRestricted"))
		return new DiffusionRestricted(paraValue,indValue);
	else if(boost::iequals(idValue,"diffusionSimpleRestricted"))
		return new DiffusionSimpleRestricted(paraValue,indValue);
	// transport/polarizationTransport.h
	// namespace polarizationTransportFast
	else if(boost::iequals(idValue,"polarizationTransportFast::lin"))
	  return new PolarizationTransportFast::Lin(paraValue,indValue);
	else if(boost::iequals(idValue,"polarizationTransportFast::constlin"))
	  return new PolarizationTransportFast::Constlin(paraValue,indValue);
	else if(boost::iequals(idValue,"polarizationTransportFast::hill"))
	  return new PolarizationTransportFast::Hill(paraValue,indValue);
	    // TODO? (for completeness)else if(boost::iequals(idValue,"polarizationTransportFast::exp"))
	    //return new PolarizationTransportFast::Exp(paraValue,indValue);
	else if(boost::iequals(idValue,"polarizationTransportFast::general"))
	  return new PolarizationTransportFast::General(paraValue,indValue);
	else if(boost::iequals(idValue,"polarizationTransportFast::constMM"))
	  return new PolarizationTransportFast::ConstMM(paraValue,indValue);
	else if(boost::iequals(idValue,"polarizationTransportFast::linMM"))
	  return new PolarizationTransportFast::LinMM(paraValue,indValue);
	else if(boost::iequals(idValue,"polarizationTransportFast::constlinMM"))
	  return new PolarizationTransportFast::ConstlinMM(paraValue,indValue);
	else if(boost::iequals(idValue,"polarizationTransportFast::hillMM"))
	  return new PolarizationTransportFast::HillMM(paraValue,indValue);
	else if(boost::iequals(idValue,"polarizationTransportFast::expMM"))
	  return new PolarizationTransportFast::ExpMM(paraValue,indValue);
	else if(boost::iequals(idValue,"polarizationTransportFast::constMMInflux"))
	  return new PolarizationTransportFast::ConstMMInflux(paraValue,indValue);
	else if(boost::iequals(idValue,"polarizationTransportFast::linMMInflux"))
	  return new PolarizationTransportFast::LinMMInflux(paraValue,indValue);
	else if(boost::iequals(idValue,"polarizationTransportFast::hillMMInflux"))
	  return new PolarizationTransportFast::HillMMInflux(paraValue,indValue);
	else if(boost::iequals(idValue,"polarizationTransportFast::linMMSpatial"))
	  return new PolarizationTransportFast::LinMMSpatial(paraValue,indValue);
	else if(boost::iequals(idValue,"polarizationTransportFast::linMMSwitched"))
	  return new PolarizationTransportFast::LinMMSwitched(paraValue,indValue);
	// these reactions are still in polarizationTransport.h but outside namespace PolarizationTransportFast
	// (to come...) namespace PolarizationTransportFastRestricted (?)
	else if(boost::iequals(idValue,"polarizationLinFastTransportRestrictedMM"))
		return new PolarizationLinFastTransportRestrictedMM(paraValue,indValue);
	else if(boost::iequals(idValue,"polarizationConstFastTransportRestrictedMM"))
		return new PolarizationConstFastTransportRestrictedMM(paraValue,indValue);
	else if(boost::iequals(idValue,"polarizationConstFastTransportFractionRestrictedMM"))
		return new PolarizationConstFastTransportFractionRestrictedMM(paraValue,indValue);
	else if(boost::iequals(idValue,"polarizationConstHillFastTransportFractionRestrictedMM"))
		return new PolarizationConstHillFastTransportFractionRestrictedMM(paraValue,indValue);
	else if(boost::iequals(idValue,"polarizationConstHillFastTransportRestrictedMM"))
		return new PolarizationConstHillFastTransportRestrictedMM(paraValue,indValue);
	else if(boost::iequals(idValue,"polarizationConstHillFastTransportRestrictedMM"))
		return new PolarizationConstHillFastTransportRestrictedMM(paraValue,indValue);
	else if(boost::iequals(idValue,"polarizationTransportFastGeneralFGRestricted"))
		return new PolarizationTransportFastGeneralFGRestricted(paraValue,indValue);
	//transport/directionalTransport.h
	else if(boost::iequals(idValue,"directionalTransport"))
		return new DirectionalTransport(paraValue,indValue);
	else if(boost::iequals(idValue,"directionalTransportMM"))
		return new DirectionalTransportMM(paraValue,indValue);
	else if(boost::iequals(idValue,"directionalTransportSwitchedMM"))
		return new DirectionalTransportSwitchedMM(paraValue,indValue);
	//transport/membraneTransport.h
	else if(boost::iequals(idValue,"activeTransportRestricted"))
		return new ActiveTransportRestricted(paraValue,indValue);
	else if(boost::iequals(idValue,"activeTransportMMRestricted"))
		return new ActiveTransportMMRestricted(paraValue,indValue);
	else if(boost::iequals(idValue,"activeTransportBRestricted"))
		return new ActiveTransportBRestricted(paraValue,indValue);
	else if(boost::iequals(idValue,"membraneTransport"))
		return new MembraneTransport(paraValue,indValue);
	else if(boost::iequals(idValue,"membraneTransportRestricted"))
		return new MembraneTransportRestricted(paraValue,indValue);
	else if(boost::iequals(idValue,"activeMembraneTransportRestricted"))
		return new ActiveMembraneTransportRestricted(paraValue,indValue);
	else if(boost::iequals(idValue,"activeMembraneTransportRestrictedMM"))
		return new ActiveMembraneTransportRestrictedMM(paraValue,indValue);
	else if(boost::iequals(idValue,"activeMembraneTransportBRestricted"))
		return new ActiveMembraneTransportBRestricted(paraValue,indValue);
	else if(boost::iequals(idValue,"activeMembraneTransportBHillRestricted"))
		return new ActiveMembraneTransportBHillRestricted(paraValue,indValue);
	else if(boost::iequals(idValue,"activeTransportVariableSimple"))
		return new ActiveTransportVariableSimple(paraValue,indValue);
	//rootTransport.h
	else if(boost::iequals(idValue,"rootTransport"))
		return new RootTransport(paraValue,indValue);
	else if(boost::iequals(idValue,"rootTransport2D"))
		return new RootTransport2D(paraValue,indValue);
	//meshedRoot/*.h
	else if(boost::iequals(idValue,"meshedRoot::activeTransport"))
		return new meshedRoot::ActiveTransport(paraValue,indValue);
	else if(boost::iequals(idValue,"meshedRoot::activeTransportMM"))
		return new meshedRoot::ActiveTransportMM(paraValue,indValue);
	else if(boost::iequals(idValue,"meshedRoot::membraneTransportRestricted"))
		return new meshedRoot::MembraneTransportRestricted(paraValue,indValue);
	else if(boost::iequals(idValue,"meshedRoot::diffusionRestricted"))
	  return new meshedRoot::DiffusionRestricted(paraValue,indValue);
	else if(boost::iequals(idValue,"meshedRoot::outflux"))
	  return new meshedRoot::Outflux(paraValue,indValue);
	else if(boost::iequals(idValue,"meshedRoot::influx"))
	  return new meshedRoot::Influx(paraValue,indValue);
	else if(boost::iequals(idValue,"meshedRoot::outfluxMM"))
	  return new meshedRoot::OutfluxMM(paraValue,indValue);
	else if(boost::iequals(idValue,"meshedRoot::influxMM"))
		return new meshedRoot::InfluxMM(paraValue,indValue);
	else if(boost::iequals(idValue,"meshedRoot::constantMassProduction"))
		return new meshedRoot::ConstantMassProduction(paraValue,indValue);
	//Growth related updates
	//growth.h,growth.cc
	else if(boost::iequals(idValue,"growthConstant"))
		return new GrowthConstant(paraValue,indValue);
	//else if(boost::iequals(idValue,"growthExponential"))
	else if(boost::iequals(idValue,"growthexponential"))
		return new GrowthExponential(paraValue,indValue);
	else if(boost::iequals(idValue,"growthExponentialOne"))
		return new GrowthExponentialOne(paraValue,indValue);
	else if(boost::iequals(idValue,"growthExponentialRestricted"))
		return new GrowthExponentialRestricted(paraValue,indValue);
	else if(boost::iequals(idValue,"growthExponentialTrunc"))
		return new GrowthExponentialTrunc(paraValue,indValue);
	else if(boost::iequals(idValue,"growthExponentialRestrictedTrunc"))
		return new GrowthExponentialRestrictedTrunc(paraValue,indValue);
	else if(boost::iequals(idValue,"dilution"))
		return new Dilution(paraValue,indValue);
	else if(boost::iequals(idValue,"updateVolume"))
		return new UpdateVolume(paraValue,indValue);
	else if(boost::iequals(idValue,"ellipticGrowthExponentialTrunc"))
		return new EllipticGrowthExponentialTrunc(paraValue,indValue);
	else if(boost::iequals(idValue,"cigarGrowthExponentialTrunc"))
		return new CigarGrowthExponentialTrunc(paraValue,indValue);
	else if(boost::iequals(idValue,"sphereBudGrowthTruncNeigh"))
		return new SphereBudGrowthTruncNeigh(paraValue,indValue);
	else if(boost::iequals(idValue,"lensRadiusGrowth"))
		return new LensRadiusGrowth(paraValue,indValue);
	else if(boost::iequals(idValue,"lensLayerGrowth"))
		return new LensLayerGrowth(paraValue,indValue);
	else if(boost::iequals(idValue,"lensDilution"))
		return new LensDilution(paraValue,indValue);
	//Mechanical interactions between compartments
	//mechanical.h,mechanical.cc
	else if(boost::iequals(idValue,"spring"))
		return new Spring(paraValue,indValue);
	else if(boost::iequals(idValue,"springAsymmetric"))
		return new SpringAsymmetric(paraValue,indValue);
	else if(boost::iequals(idValue,"springTruncated"))
		return new SpringTruncated(paraValue,indValue); 
	else if(boost::iequals(idValue,"springAdhesionRestrictedSphere"))
		return new SpringAdhesionRestrictedSphere(paraValue,indValue); 
	else if(boost::iequals(idValue,"springAsymmetricSphere"))
		return new SpringAsymmetricSphere(paraValue,indValue);
	else if(boost::iequals(idValue,"springAsymmetricCylinder"))
		return new SpringAsymmetricCylinder(paraValue,indValue);
	else if(boost::iequals(idValue,"springAsymmetricSphereCylinder"))
		return new SpringAsymmetricSphereCylinder(paraValue,indValue);
	else if(boost::iequals(idValue,"sphereCylinderWall"))
		return new SphereCylinderWall(paraValue,indValue);
	else if(boost::iequals(idValue,"parabolaSphereWall"))
		return new ParabolaSphereWall(paraValue,indValue);
	else if(boost::iequals(idValue,"parabolaSphereWallGrowth"))
		return new ParabolaSphereWallGrowth(paraValue,indValue);
	else if(boost::iequals(idValue,"primordiumGrowth"))
		return new PrimordiumGrowth(paraValue,indValue);
	else if(boost::iequals(idValue,"ellipticRepulsion"))
		return new EllipticRepulsion(paraValue,indValue);
	else if(boost::iequals(idValue,"normalEllipticRepulsion"))
		return new NormalEllipticRepulsion(paraValue,indValue);
	else if(boost::iequals(idValue,"springAsymmetricEllipse"))
		return new SpringAsymmetricEllipse(paraValue,indValue);
	else if(boost::iequals(idValue,"springAsymmetricSphereBud"))
		return new SpringAsymmetricSphereBud(paraValue,indValue);
	else if(boost::iequals(idValue,"wallSpringElliptic"))
		return new WallSpringElliptic(paraValue,indValue);
	else if(boost::iequals(idValue,"wallSpringCigar"))
		return new WallSpringCigar(paraValue,indValue);
	else if(boost::iequals(idValue,"springElasticWallEllipse"))
		return new SpringElasticWallEllipse(paraValue,indValue);
	else if(boost::iequals(idValue,"gravitationElliptic"))
		return new GravitationElliptic(paraValue,indValue);
	else if(boost::iequals(idValue,"springAsymmetricCigar"))
		return new SpringAsymmetricCigar(paraValue,indValue);
	else if(boost::iequals(idValue,"springRepulsiveCigar"))
		return new SpringRepulsiveCigar(paraValue,indValue);
	else if(boost::iequals(idValue,"springWallCigar2"))
		return new SpringWallCigar2(paraValue,indValue);
	else if(boost::iequals(idValue,"springElasticWallCigar2"))
		return new SpringElasticWallCigar2(paraValue,indValue);
	else if(boost::iequals(idValue,"lagrangianCigar"))
		return new LagrangianCigar(paraValue,indValue);
	else if(boost::iequals(idValue,"lagrangianWallCigar"))
		return new LagrangianWallCigar(paraValue,indValue);
	else if(boost::iequals(idValue,"perforatedLagrangianWallCigar"))
		return new PerforatedLagrangianWallCigar(paraValue,indValue);
	else if(boost::iequals(idValue,"lagrangianCigarNew"))
		return new LagrangianCigarNew(paraValue,indValue);
	else if(boost::iequals(idValue,"lagrangianCigarFriction"))
		return new LagrangianCigarFriction(paraValue,indValue);
	else if(boost::iequals(idValue,"lagrangianWallCigarNew"))
		return new LagrangianWallCigarNew(paraValue,indValue);
	else if(boost::iequals(idValue,"multipleWallCigar"))
		return new MultipleWallCigar(paraValue,indValue);
	else if(boost::iequals(idValue,"cigarAdhesion"))
		return new CigarAdhesion(paraValue,indValue);
	else if(boost::iequals(idValue,"wallCigar3D"))
		return new WallCigar3D(paraValue,indValue);	
	// mechanics/sphereForce
	else if(boost::iequals(idValue,"sphere::ForceDirection"))
	  return new Sphere::ForceDirection(paraValue,indValue);	
	else if(boost::iequals(idValue,"sphere::ForceCellDirection"))
	  return new Sphere::ForceCellDirection(paraValue,indValue);	
	else if(boost::iequals(idValue,"sphere::forceRadial"))
	  return new Sphere::ForceRadial(paraValue,indValue);	
	//TODO else if(boost::iequals(idValue,"sphere::ForceRestricted"))
	//  return new Sphere::ForceRestricted(paraValue,indValue);	
	// mechanics/sphereWall
	else if(boost::iequals(idValue,"sphere::sphericalWall"))
	  return new Sphere::SphericalWall(paraValue,indValue);	
	// elongatedBacteria/
	else if(boost::iequals(idValue,"elongatedBacteria::flagellarMotor")) 
		return new elongatedBacteria::FlagellarMotor(paraValue,indValue);
	else if(boost::iequals(idValue,"elongatedBacteria::cellMechanics2D")) 
		return new elongatedBacteria::CellMechanics<2>(paraValue,indValue);
	else if(boost::iequals(idValue,"elongatedBacteria::cellMechanics3D")) 
		return new elongatedBacteria::CellMechanics<3>(paraValue,indValue);
	else if(boost::iequals(idValue,"elongatedBacteria::cellMechanicsAdhesion2D")) 
		return new elongatedBacteria::CellMechanicsAdhesion<2>(paraValue,indValue);
	else if(boost::iequals(idValue,"elongatedBacteria::cellMechanicsAdhesion3D")) 
		return new elongatedBacteria::CellMechanicsAdhesion<3>(paraValue,indValue);
	else if(boost::iequals(idValue,"elongatedBacteria::wallMechanics2D")) 
		return new elongatedBacteria::WallMechanics2D(paraValue,indValue);
	else if(boost::iequals(idValue,"elongatedBacteria::wallMechanics3D")) 
		return new elongatedBacteria::WallMechanics3D(paraValue,indValue);
	else if(boost::iequals(idValue,"elongatedBacteria::exponentialGrowth")) 
	 	return new elongatedBacteria::ExponentialGrowth(paraValue,indValue);
	else if(boost::iequals(idValue,"elongatedBacteria::substrateDependentGrowth2D")) 
		return new elongatedBacteria::SubstrateDependentGrowth2D(paraValue,indValue);
	else if(boost::iequals(idValue,"elongatedBacteria::substrateDependentGrowth3D")) 
		return new elongatedBacteria::SubstrateDependentGrowth3D(paraValue,indValue);
	else if(boost::iequals(idValue,"elongatedBacteria::chemotaxisSimple"))
		return new elongatedBacteria::ChemotaxisSimple(paraValue,indValue);
	//gradients
	else if(boost::iequals(idValue,"elongatedBacteria::gradientSmooth2D"))
		return new elongatedBacteria::GradientSmooth2D(paraValue,indValue);
	else if(boost::iequals(idValue,"elongatedBacteria::gradientGaussian2D"))
		return new elongatedBacteria::GradientGaussian2D(paraValue,indValue);
	else if(boost::iequals(idValue,"sphericalBacteria::substrateDependentGrowth2D")) 
		return new sphericalBacteria::SubstrateDependentGrowth2D(paraValue,indValue);
	else if(boost::iequals(idValue,"sphericalBacteria::substrateDependentGrowth3D")) 
		return new sphericalBacteria::SubstrateDependentGrowth3D(paraValue,indValue);
	else if(boost::iequals(idValue,"sphericalBacteria::cellMechanics2D")) 
		return new sphericalBacteria::CellMechanics<2>(paraValue,indValue);
	else if(boost::iequals(idValue,"sphericalBacteria::cellMechanics3D")) 
		return new sphericalBacteria::CellMechanics<3>(paraValue,indValue);
	else if(boost::iequals(idValue,"sphericalBacteria::wallMechanics2D") ||
		boost::iequals(idValue,"Sphere::WallMechanics2D")) 
		return new Sphere::WallMechanics2D(paraValue,indValue);
	//gradients
	else if(boost::iequals(idValue,"sphericalBacteria::gradientSmooth2D"))
		return new sphericalBacteria::GradientSmooth2D(paraValue,indValue);
	else if(boost::iequals(idValue,"sphericalBacteria::gradientGaussian2D"))
		return new sphericalBacteria::GradientGaussian2D(paraValue,indValue);
	//Complete models
	else if(boost::iequals(idValue,"auxinFlux"))
		return new AuxinFlux(paraValue,indValue);
	else if (boost::iequals(idValue,"simplifiedAuxinTransportJTB::auxinTransport"))
		return new SimplifiedAuxinTransportJTB::AuxinTransport(paraValue, indValue);
	else if (boost::iequals(idValue,"simplifiedAuxinTransportJTB::auxinTransportExperimentalOne"))
		return new SimplifiedAuxinTransportJTB::AuxinTransportExperimentalOne(paraValue, indValue);
	else if (boost::iequals(idValue,"simplifiedAuxinTransportJTB::auxinTransportExperimentalTwo"))
		return new SimplifiedAuxinTransportJTB::AuxinTransportExperimentalTwo(paraValue, indValue);
	else if (boost::iequals(idValue,"simplifiedAuxinTransportJTB::auxinTransportPosterA"))
		return new SimplifiedAuxinTransportJTB::AuxinTransportPosterA(paraValue, indValue);
	else if (boost::iequals(idValue,"simplifiedAuxinTransportJTB::auxinTransportPosterB"))
		return new SimplifiedAuxinTransportJTB::AuxinTransportPosterB(paraValue, indValue);
	else if (boost::iequals(idValue,"simplifiedAuxinTransportJTB::auxinTransportPosterC"))
		return new SimplifiedAuxinTransportJTB::AuxinTransportPosterC(paraValue, indValue);
	else if (boost::iequals(idValue,"simplifiedAuxinTransportJTB::auxinTransportNoCompetition"))
		return new SimplifiedAuxinTransportJTB::AuxinTransportNoCompetition(paraValue, indValue);
	else if (boost::iequals(idValue,"simplifiedAuxinTransportJTB::CWII"))
		return new SimplifiedAuxinTransportJTB::CWII(paraValue, indValue);
	else if (boost::iequals(idValue,"cellCellAuxinTransport"))
		return new CellCellAuxinTransport(paraValue, indValue);
	else if (boost::iequals(idValue,"cellWallAuxinTransport"))
		return new CellWallAuxinTransport(paraValue, indValue);
	else if (boost::iequals(idValue,"cytokininNetwork"))
		return new CytokininNetwork(paraValue, indValue);
	// clawus/
	else if (boost::iequals(idValue,"clawus::clawusDynamics"))
	  return new clawus::ClawusDynamics(paraValue, indValue);
	else if (boost::iequals(idValue,"clawus::clvcrn"))
	  return new clawus::Clvcrn(paraValue, indValue);
	else if (boost::iequals(idValue,"clawus::clvcrnNonLin"))
	  return new clawus::ClvcrnNonLin(paraValue, indValue);
	// Ad-hoc reations adhocReaction.h
	else if (boost::iequals(idValue,"sumCompartmentVar"))
	  return new SumCompartmentVar(paraValue, indValue);
	else if (boost::iequals(idValue,"sumCompartmentMass"))
	  return new SumCompartmentMass(paraValue, indValue);
	else if (boost::iequals(idValue,"diffusionDivided"))
	  return new DiffusionDivided(paraValue, indValue);
	else if (boost::iequals(idValue,"add"))
	  return new Add(paraValue, indValue);
	else if (boost::iequals(idValue,"setRestricted"))
	  return new SetRestricted(paraValue, indValue);
	else if (boost::iequals(idValue,"switchCompartmentThreshold"))
	  return new SwitchCompartmentThreshold(paraValue, indValue);
	else if (boost::iequals(idValue,"thresholdSwitch"))
	  return new ThresholdSwitch(paraValue, indValue);
	else if (boost::iequals(idValue,"factorOnDerivsMultiple"))
	  return new FactorOnDerivsMultiple(paraValue, indValue);
	//  BenchMarkMendez
	else if (boost::iequals(idValue,"benchMarkMendez"))
	  return new benchMarkMendez(paraValue, indValue);
	//  ParameterUpdate
	// ParameterExponential
	else if (boost::iequals(idValue,"parameterExponentialPos"))
	  return new ParameterExponentialPos(paraValue, indValue);
	else if (boost::iequals(idValue,"parameterExponentialNeg"))
	  return new ParameterExponentialNeg(paraValue, indValue);	
	// ParameterCopy
	else if (boost::iequals(idValue,"parameterCopy"))
	  return new ParameterCopy(paraValue, indValue);
	// ParameterStochastic
	else if (boost::iequals(idValue,"parameterStochastic"))
	  return new ParameterStochastic(paraValue, indValue);
	// OLD reaction names that might appear in model files with information on replacement
	//
	// old reactions moved to namespace polarizationTransportFast
	else if(boost::iequals(idValue,"polarizationTransport")) {
	  std::cerr << "BaseReaction::createReaction: polarizationTransport reaction has been replaced by "
		    << "polarizationTransportFast::lin using p_1=0" << std::endl;
	  exit(EXIT_FAILURE);
	  //return new PolarizationTransport(paraValue,indValue);
	}
	else if(boost::iequals(idValue,"polarizationTransportFast")) {
	  std::cerr << "BaseReaction::createReaction: polarizationTransportFast reaction has been replaced by "
		    << "polarizationTransportFast::lin" << std::endl;
	  exit(EXIT_FAILURE);
	  //return new PolarizationTransportFast(paraValue,indValue);
	}
	else if(boost::iequals(idValue,"polarizationTransportFastMM")) {
	  std::cerr << "BaseReaction::createReaction: polarizationTransportFastMM reaction has been replaced by "
		    << "polarizationTransportFast::linMM" << std::endl;
	  exit(EXIT_FAILURE);
	  //return new PolarizationTransportFastMM(paraValue,indValue);
	}
	else if(boost::iequals(idValue,"polarizationTransportInfluxFastMM")) {
	  std::cerr << "BaseReaction::createReaction: polarizationTransportInfluxFastMM reaction has been replaced by "
		    << "polarizationTransportFast::linMMInflux" << std::endl;
	  exit(EXIT_FAILURE);
	  //return new PolarizationTransportInfluxFastMM(paraValue,indValue);
	}
	else if(boost::iequals(idValue,"polarizationHillTransportInfluxFastMM")) {
	  std::cerr << "BaseReaction::createReaction: polarizationHillTransportInfluxFastMM reaction has been replaced by "
		    << "polarizationTransportFast::hillMMInflux" << std::endl;
	  exit(EXIT_FAILURE);
	  //return new PolarizationHillTransportInfluxFastMM(paraValue,indValue);
	}
	else if(boost::iequals(idValue,"polarizationTransportFastMMSpatial")) {
	  std::cerr << "BaseReaction::createReaction: polarizationTransportFastMMSpatial reaction has been replaced by "
		    << "polarizationTransportFast::linMMSpatial" << std::endl;
	  exit(EXIT_FAILURE);
	  //return new PolarizationTransportFastMMSpatial(paraValue,indValue);
	}
	else if(boost::iequals(idValue,"polarizationHillTransportFast")) {
	  std::cerr << "BaseReaction::createReaction: polarizationHillTransportFast reaction has been replaced by "
		    << "polarizationTransportFast::hill" << std::endl;
	  exit(EXIT_FAILURE);
	  //return new PolarizationHillTransportFast(paraValue,indValue);
	}
	else if(boost::iequals(idValue,"polarizationHillTransportFastMM")) {
	  std::cerr << "BaseReaction::createReaction: polarizationHillTransportFastMM reaction has been replaced by "
		    << "polarizationTransportFast::hillMM" << std::endl;
	  exit(EXIT_FAILURE);
	  //return new PolarizationHillTransportFastMM(paraValue,indValue);
	}
	else if(boost::iequals(idValue,"polarizationExpTransportFastMM")) {
	  std::cerr << "BaseReaction::createReaction: polarizationExpTransportFastMM reaction has been replaced by "
		    << "polarizationTransportFast::expMM" << std::endl;
	  exit(EXIT_FAILURE);
	  //return new PolarizationExpTransportFastMM(paraValue,indValue);
	}
	else if(boost::iequals(idValue,"polarizationConstLinTransportFast")) {
	  std::cerr << "BaseReaction::createReaction: polarizationConstLinTransportFast reaction has been replaced by "
		    << "polarizationTransportFast::constlin" << std::endl;
	  exit(EXIT_FAILURE);
	  //return new PolarizationConstLinTransportFast(paraValue,indValue);
	}
	else if(boost::iequals(idValue,"polarizationTransportFastB")) {
	  std::cerr << "BaseReaction::createReaction: polarizationTransportFastB reaction has been replaced by "
		    << "polarizationTransportFast::constlin" << std::endl;
	  exit(EXIT_FAILURE);
	  //return new PolarizationTransportFastB(paraValue,indValue);
	}
	else if(boost::iequals(idValue,"polarizationTransportFastBMM")) {
	  std::cerr << "BaseReaction::createReaction: polarizationTransportFastBMM reaction has been replaced by "
		    << "polarizationTransportFast::constlinMM" << std::endl;
	  exit(EXIT_FAILURE);
	  //return new PolarizationTransportFastBMM(paraValue,indValue);
	}
	else if(boost::iequals(idValue,"polarizationTransportFastSwitchedMM")) {
	  std::cerr << "BaseReaction::createReaction: polarizationTransportFastSwitchedMM reaction has been replaced by "
		    << "polarizationTransportFast::linMMSwitched" << std::endl;
	  exit(EXIT_FAILURE);
	  //return new PolarizationTransportFastSwitchedMM(paraValue,indValue);
	}
	else if(boost::iequals(idValue,"generalPolarizationTransportFast")) {
	  std::cerr << "BaseReaction::createReaction: generalPolarizationTransportFast reaction has been replaced by "
		    << "polarizationTransportFast::general" << std::endl;
	  exit(EXIT_FAILURE);
	  //return new GeneralPolarizationTransportFast(paraValue,indValue);
	}
	else if(boost::iequals(idValue,"symmetricTransportFastMM")) {
	  std::cerr << "BaseReaction::createReaction: symmetricTransportFastMM reaction has been replaced by "
		    << "polarizationTransportFast::constMM" << std::endl;
	  exit(EXIT_FAILURE);
	  //return new SymmetricTransportFastMM(paraValue,indValue);
	}
	else if(boost::iequals(idValue,"symmetricTransportInfluxFastMM")) {
	  std::cerr << "BaseReaction::createReaction: symmetricTransportInfluxFastMM reaction has been replaced by "
		    << "polarizationTransportFast::constMMInflux" << std::endl;
	  exit(EXIT_FAILURE);
	  //return new SymmetricTransportInfluxFastMM(paraValue,indValue);
	}
	// Default, if nothing found
	//
	else {
	  std::cerr << "\nBaseReaction::createReaction() WARNING: Reactiontype " 
		    << idValue << " not known, no reaction created.\n\7";
	  exit(-1);
	}
}

BaseReaction* 
BaseReaction::createReaction(std::istream &IN ) 
{
	std::string idVal;
  size_t pNum,levelNum;
  IN >> idVal;
  IN >> pNum;
  IN >> levelNum;
  std::vector<size_t> varIndexNum( levelNum );
  for( size_t i=0 ; i<levelNum ; i++ )
    IN >> varIndexNum[i];
  
  std::vector<double> pVal( pNum );
  for( size_t i=0 ; i<pNum ; i++ )
    IN >> pVal[i];
  
  std::vector< std::vector<size_t> > varIndexVal( levelNum );
  for( size_t i=0 ; i<levelNum ; i++ )
    varIndexVal[i].resize( varIndexNum[i] );
  
  for( size_t i=0 ; i<levelNum ; i++ )
    for( size_t j=0 ; j<varIndexNum[i] ; j++ )
      IN >> varIndexVal[i][j];
  
  return createReaction(pVal,varIndexVal,idVal);
}

void BaseReaction::initiate(double t,DataMatrix &y) 
{
}

void BaseReaction::
derivs(Compartment &compartment,size_t species,DataMatrix &y,DataMatrix &dydt) 
{
  std::cerr << "BaseReaction::derivs() should not be used. "
	    << "Should always be mapped onto one of the real types.\n";
  exit(EXIT_FAILURE);
}  

void BaseReaction::update(double h, double t,DataMatrix &y) 
{
}

void BaseReaction::
derivsWithAbs(Compartment &compartment,size_t species,DataMatrix &y,DataMatrix &dydt,DataMatrix &sdydt) 
{
  std::cerr << "BaseReaction::derivsWithAbs() should not be used. "
	    << "Should always be mapped onto one of the real reaction types." << std::endl
	    << "This indicates that the derivsWithAbs is not defined for one of the reactions" << std::endl
	    << " in the model file and that a solver with noise (e.g. heunito) might not be used." << std::endl;
  exit(EXIT_FAILURE);
}  

size_t BaseReaction::
Jacobian(Compartment &compartment,size_t species,DataMatrix &y,JacobianMatrix &A)
{
  std::cerr << "BaseReaction::Jacobian() should not be used. "
	    << "Should always be mapped onto one of the real types." << std::endl
	    << "This indicates that the Jacobian is not defined for one of the reactions" << std::endl
	    << " and that an implicit solver might not be used." << std::endl;
  exit(EXIT_FAILURE);
}

double BaseReaction::
propensity(Compartment &compartment,size_t species,DataMatrix &y)
{
  std::cerr << "BaseReaction::propensity() should not be used. "
	    << "Should always be mapped onto one of the real types." 
	    << std::endl
	    << "This indicates that the propensity is not defined for one of the reactions" 
	    << std::endl
	    << " and that a stochastic solver can not be used." << std::endl;
  exit(EXIT_FAILURE);
}

void BaseReaction::
discreteUpdate(Compartment &compartment,size_t species,DataMatrix &y)
{
  std::cerr << "BaseReaction::discreteUpdate() should not be used. "
	    << "Should always be mapped onto one of the real types." 
	    << std::endl
	    << "This indicates that the propensity is not defined for one of the reactions" 
	    << std::endl
	    << " and that a stochastic solver can not be used." << std::endl;
  exit(EXIT_FAILURE);
}

void BaseReaction::print( std::ofstream &os ) 
{
  std::cerr << "BaseReaction::print(ofstream) should not be used. "
	    << "Should always be mapped onto one of the real types.\n";
  exit(EXIT_FAILURE);
}

void BaseReaction::printCambium( std::ostream &os, size_t varIndex ) const
{
  std::cerr << "BaseReaction::printCambium(ofstream) should not be used. " << std::endl
	    << "Should always be mapped onto one of the real types." << std::endl
	    << "Creating unresolve reaction in output." << std::endl;
  os << "Arrow[Organism[" << id() << "],{},{},{},Parameters[";
  for (size_t i=0; i<numParameter(); ++i) {
    if (i!=0)
      os << ", ";
    os << parameter(i);
  }
  os << "]" << std::endl;
}

Organism* BaseReaction::organism() const 
{
  return organism_;
}

/*size_t BaseReaction::numSpecies(){
	return organism()->numSpecies();
}*/
