//
// Filename     : completeModel.h
// Description  : Classes describing complete models
// Author(s)    : Pontus Melke (pontus@thep.lu.se)
// Created      : August 2006
// Revision     : $Id: completeModel.h 612 2015-03-05 17:28:13Z henrik $
//

#ifndef COMPLETEMODEL_H
#define COMPLETEMODEL_H

#include"baseReaction.h"
#include<cmath>

///
/// @brief Complete model describing the auxin flux
///
/// Auxin model is defined as:
///
/// @f[
/// \frac{dA_i}{dt} = W\left(p_{AH}(A_{oH}-f_{AH}A_i)+w_A\cdot
/// p_A\left(\frac{N_{Inf}A_o}{K_A+A_o}-\frac{N_{Eff}f_AA_i}{K_A+f_AA_i}\right)+w_{AUX}\cdot
/// p_{AUX}\left(\frac{N_{Eff}A_o}{K_{AUX}+A_o}-\frac{N_{Inf}f_AA_i}{K_{AUX}+f_AA_i}\right)\right) 
/// -k_1A_i + k_2A_c
/// @f]
/// @f[
/// \frac{dA_c}{dt} = k_1A_i - k_2A_c
/// @f]
/// @f[ A_{label} = dummy*y @f]
///
///	 Fourteen parameters are needed @f$p_{AH}@f$,@f$p_A@f$, @f$K_A@f$,
///	 @f$p_{AUX}@f$, @f$K_{AUX}@f$, @f$V@f$, @f$T@f$, @f$pH@f$,
///	 @f$pK_d@f$, @f$W@f$, @f$w_{A}@f$, @f$w_{AUX}@f$, @f$k_1@f$ and @f$k_2@f$. Where
///	 @f$V@f$, @f$T@f$, @f$pH@f$ and @f$pK_d@f$ control @f$f_A@f$,
///	 @f$f_{AH}@f$,@f$N_{Eff}@f$ and @f$N_{inf}@f$. Six variables must
///	 be specified @f$A_{label}@f$, @f$A_i@f$, @f$A_{oH}@f$, @f$A_o@f$, a dummy variable that sets the fraction of labeled auxin and @f$A_c@f$. Out
///	 of these five variables only the two first are updated.  All
///	 indices are defined at level zero.
///
class AuxinFlux : public BaseReaction {
	
public:
	///
	/// @brief Main constructor
	///
	/// This is the main constructor which sets the parameters and variable
	/// indices that defines the reaction.
	///
	/// @param paraValue vector with parameters
	///
	/// @param indValue vector of vectors with variable indices
	///
	/// @see BaseReaction::createReaction(std::vector<double> &paraValue,...)
	///
	AuxinFlux(std::vector<double> &paraValue, 
			std::vector< std::vector<size_t> > &indValue );
	///
	/// @brief Derivative function for this reaction class
	///
	/// @see BaseReaction::derivs(Compartment &compartment,size_t species,...)
	///
	void derivs(Compartment &compartment,size_t species,
			  std::vector< std::vector<double> > &y,
			  std::vector< std::vector<double> > &dydt );
};

///
/// @brief Complete transport model describing the auxin flux in a cell cell model 
///
/// The auxin transport is using PIN and AUX values given as neighborVariables.
/// The auxin transport model is defined as:
///
/// @f[
/// \frac{da_i}{dt} = 
/// @f]
///
/// where a_i and a_j are the auxin concentrationsa in the neighboring cells,
/// V_i, V_j are the volumes and area_{ij}=area_{ji}, P_{ij}, and A_{ij} are
/// the areas and PIN/AUX concentrations.
///
///	Four parameters are needed: @f$d_{1}=f_{AH}^{cell}p_{AH}@f$,
///	@f$p_{p}=f_{A-}^{cell}N(\phi)p_{PIN}@f$, @f$d_2=f_{AH}^{wall}p_{AH}@f$,
///	and @f$p_{a}=f_{A-}^{wall}N(\phi)p_{AUX}@f$.
///
/// First level of indices indicates the volume index, the second indicates
/// the indices of neighborVariables for area, PIN and AUX for the connecting
/// wall/membrane.
///
class CellCellAuxinTransport : public BaseReaction {
	
public:
	///
	/// @brief Main constructor
	///
	/// This is the main constructor which sets the parameters and variable
	/// indices that defines the reaction.
	///
	/// @param paraValue vector with parameters
	///
	/// @param indValue vector of vectors with variable indices
	///
	/// @see BaseReaction::createReaction(std::vector<double> &paraValue,...)
	///
	CellCellAuxinTransport(std::vector<double> &paraValue, 
												 std::vector< std::vector<size_t> > &indValue );
	///
	/// @brief Derivative function for this reaction class
	///
	/// @see BaseReaction::derivs(Compartment &compartment,size_t species,...)
	///
	void derivs(Compartment &compartment,size_t species,
							std::vector< std::vector<double> > &y,
							std::vector< std::vector<double> > &dydt );
};

///
/// @brief Complete transport model describing the auxin flux in a cell wall model 
///
/// The auxin transport is using PIN and AUX values given as neighborVariables.
/// The auxin transport model is defined as:
///
/// @f[
/// \frac{da_i}{dt} = 
/// @f]
///
/// where a_i and a_ij are the auxin concentrationsa in the neighboring cell/wall,
/// V_i, V_j are the volumes and area_{ij}=area_{ji}, P_{ij}, and A_{ij} are
/// the areas and PIN/AUX concentrations.
///
/// Five parameters are needed: 
///
/// @f$d_{1}=f_{AH}^{cell}p_{AH}@f$,
/// @f$p_{p}=f_{A-}^{cell}N(\phi)p_{PIN}@f$,
/// @f$d_2=f_{AH}^{wall}p_{AH}@f$, and
/// @f$p_{a}=f_{A-}^{wall}N(\phi)p_{AUX}@f$.
/// @f$D=apoplastic diffusion rate@f$.
///
/// First level of indices indicates the volume index, cell marker
/// index and wall marker index, the second indicates the indices of
/// neighborVariables for area, PIN and AUX for the connecting
/// wall/membrane.
///
class CellWallAuxinTransport : public BaseReaction {
	
public:
  ///
  /// @brief Main constructor
  ///
  /// This is the main constructor which sets the parameters and variable
  /// indices that defines the reaction.
  ///
  /// @param paraValue vector with parameters
  ///
  /// @param indValue vector of vectors with variable indices
  ///
  /// @see BaseReaction::createReaction(std::vector<double> &paraValue,...)
  ///
  CellWallAuxinTransport(std::vector<double> &paraValue, 
			 std::vector< std::vector<size_t> > &indValue );
  ///
  /// @brief Derivative function for this reaction class
  ///
  /// @see BaseReaction::derivs(Compartment &compartment,size_t species,...)
  ///
  void derivs(Compartment &compartment,size_t species,
	      std::vector< std::vector<double> > &y,
	      std::vector< std::vector<double> > &dydt );
  ///
  /// @brief Derivative function for this reaction class calculating the absolute value for noise solvers
  ///
  /// @see BaseReaction::derivsWithAbs(Compartment &compartment,size_t species,...)
  ///
  void derivsWithAbs(Compartment &compartment,size_t species,DataMatrix &y,DataMatrix &dydt,DataMatrix &sdydt);
};

///
/// @brief Complete Cytokinin network a la Gordon 2009
///
/// Cytokinin signalling is mediated via AHPT and ARR proteins and induces WUS
/// and inactivates CLV1 transcription
///
/// @f[
/// \frac{da_i}{dt} = 
/// @f]
///
/// The indices are listed as AHPT AHPTp B Bp A WUS CLV1 CLV3 CLV13 Cytokinin
///
class CytokininNetwork : public BaseReaction {
	
public:
  ///
  /// @brief Main constructor
  ///
  /// This is the main constructor which sets the parameters and variable
  /// indices that defines the reaction.
  ///
  /// @param paraValue vector with parameters
  ///
  /// @param indValue vector of vectors with variable indices
  ///
  /// @see BaseReaction::createReaction(std::vector<double> &paraValue,...)
  ///
  CytokininNetwork(std::vector<double> &paraValue, 
		   std::vector< std::vector<size_t> > &indValue );
  ///
  /// @brief Derivative function for this reaction class
  ///
  /// @see BaseReaction::derivs(Compartment &compartment,size_t species,...)
  ///
  void derivs(Compartment &compartment,size_t species,
	      std::vector< std::vector<double> > &y,
	      std::vector< std::vector<double> > &dydt );
};


#endif
