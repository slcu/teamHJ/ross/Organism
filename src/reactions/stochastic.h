//
// Filename     : stochastic.h
// Description  : Classes describing stochastic creation of molecules
// Author(s)    : André Larsson (andre@thep.lu.se)
//              : Henrik Jonsson (henrik@thep.lu.se)
// Created      : August 2013
// Revision     : $Id: creation.h 565 2013-02-08 12:44:44Z jeremy $
//

#ifndef STOCHASTIC_H
#define STOCHASTIC_H

#include "baseReaction.h"
#include "../common/myRandom.h"
#include "../common/typedefs.h"
#include <cmath>

///
/// @brief A collection of reactions with stochastic elements. 
///
/// Typically these reactions set a noisy production rate, used when
/// updating the variable values. Can be used to test how a system responds
/// to external or internal noise, or to model a "hybrid" system where some
/// parts are deterministic and some parts are stochastic.
///

namespace stochastic {

  ///
  /// @brief Implement the stochastic Gamma-distributed Ornstein-Uhlenbeck (OU) process.
  ///
  /// The implementation is similar to the one used in Locke et. al., Science, 2011,
  /// based on Barndorff-Nielsen and Shephard, J. R. Stat. Soc., 2002. Variables are
  /// updated according to
  ///
  /// @f[ dc_i(t) = -\lambda*c_i(t)*dt + dG( \lambda*t ) @f]
  ///
  /// where @f$ \lambda @f$ is the sampling frequency or intensity, and
  /// G is a gamma-distributed stochastic process.
  /// The update is applied to the supplied variable indices @f$ C_i @f$. The
  /// stochastic gamma-distributed process is defined by its shape
  /// (@f$ \nu @f$) and mean (@f$ \mu = \nu/\alpha @f$) parameters.
  ///
  /// Lambda sets the update frequency (how "fast" the stochastic process is).
  /// Protein distributions in E. Coli cells typically have values of @f$ \nu @f$ ~ 0.1 - 10 and
  /// @f$ \alpha @f$ ~ 1 - 100 [Taniguchi et. al., Science, 2010].
  ///
  /// This process can be seen as gamma-distributed pulses combined with 
  /// exponential decay, capturing some of the features of real biological
  /// molecular fluctuations.
  ///
  ///
  /// In a model file the reaction is defined as (with sample parameter values in comments):
  ///
  /// @verbatim
  /// stochastic::gammaOU 3 1 N_i
  /// lambda nu mu # 0.01 10 1
  /// index1 ... indexN_i @endverbatim
  ///
  /// Caveat: The derivative function in organism is NOT used, instead the
  /// update function is used and care should be taken when modulating the
  /// dynamics of affected molecules. To be on the safe side, do not mix this
  /// process with other reactions (suggested to create a new molecule instead)!
  ///

  class gammaOU : public BaseReaction {

  private:
    BoostRandoms boost_rand_ln; // random number generator
    BoostRandoms boost_rand_a; // random number generator
  
  public:
  
    ///
    /// @brief Main constructor
    ///
    /// This is the main constructor which sets the parameters and variable
    /// indices that defines the reaction.
    ///
    /// @param paraValue vector with parameters
    ///
    /// @param indValue vector of vectors with variable indices
    ///
    /// @see BaseReaction::createReaction(std::vector<double>&,...)
    ///
    gammaOU(std::vector<double> &paraValue, 
		   std::vector< std::vector<size_t> > &indValue );
  
    ///
    /// @brief Derivative function for this reaction class
    ///
    /// @see BaseReaction::derivs(Compartment &compartment,size_t species,...)
    ///
    void derivs(Compartment &compartment,size_t species,DataMatrix &y,DataMatrix &dydt );

    ///
    /// @brief Initialization function for this reaction class
    ///
    /// @see BaseReaction::initiate(double t,DataMatrix &y)
    ///
    void initiate(double t,DataMatrix &y);
    ///
    /// @brief Update function for this reaction class
    ///
    /// @see BaseReaction::update(double h, double t, ...)
    ///
    void update(double h, double t,DataMatrix &y);

  };

}
#endif

