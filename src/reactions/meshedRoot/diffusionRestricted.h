#ifndef MESHEDROOT_DIFFUSIONRESTRICTED_H
#define MESHEDROOT_DIFFUSIONRESTRICTED_H

#include<cmath>

#include"../baseReaction.h"

namespace meshedRoot {
	class DiffusionRestricted : public BaseReaction {
  
	public:
		
		///
		/// @brief Main constructor
		///
		/// This is the main constructor which sets the parameters and variable
		/// indices that defines the reaction.
		///
		/// @param paraValue vector with parameters
		///
		/// @param indValue vector of vectors with variable indices
		///
		/// @see BaseReaction::createReaction(std::vector<double> &paraValue,...)
		///
		DiffusionRestricted(std::vector<double> &paraValue, 
											std::vector< std::vector<size_t> > &indValue );
		
		///
		/// @brief Derivative function for this reaction class
		///
		/// @see BaseReaction::derivs(Compartment &compartment,size_t species,...)
		///
		void derivs(Compartment &compartment,size_t species,
								std::vector< std::vector<double> > &y,
								std::vector< std::vector<double> > &dydt );
	private:
		double invDist_;
	};
}
#endif
