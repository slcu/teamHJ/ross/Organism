/*
 * Filename     : rootTransportMesh.cc
 * Description  : Classes describing updates due to molecule transports
 * Author(s)    : Pontus (pontus@thep.lu.se)
 * Created      : October 2007
 * Revision     : $Id: 
 */
 
#include "../baseReaction.h"
#include "constantMassProduction.h"

meshedRoot::ConstantMassProduction::ConstantMassProduction(std::vector<double> &paraValue, std::vector< std::vector<size_t> > &indValue ) 
{

	//Do some checks on the parameters and variable indeces
	//////////////////////////////////////////////////////////////////////
	if ( paraValue.size()!= 2 ) {
		std::cerr << "ConstantMassProduction::ConstantMassProduction() "
		<< "uses two parameters production and M\n";
		exit(0);
	}
	if ( indValue.size() != 2 || indValue[0].size() !=1 || indValue[1].size() !=1) {
		std::cerr << "ConstantMassProduction::ConstantMassProduction() "
		<< "Index at level one, "
		<< "Volume index at level 2\n"; 
		exit(0);
	}
	//Check parameters
	//////////////////////////////////////////////////////////////////////
	if( paraValue[0]<0.0 || paraValue[1]<0.0 || paraValue[2]<0.0 ||
		paraValue[3]<0.0 || paraValue[4]<0.0 || paraValue[4]>1 ) {
		std::cerr << "ConstantMassProduction::ConstantMassProduction() "
		<< " Parameters need to be positive (and fractions).\n";
		exit(0);
	}
        //Set the variable values
	//////////////////////////////////////////////////////////////////////
	setId("constantMassProduction");
	setParameter(paraValue);  
	setVariableIndex(indValue);
	
	//Set the parameter identities
	//////////////////////////////////////////////////////////////////////
	std::vector<std::string> tmp( numParameter() );
	tmp.resize( numParameter() );
	tmp[0] = "cp";
	tmp[1] = "M";
	setParameterId( tmp );
}

void meshedRoot::ConstantMassProduction::derivs(Compartment &compartment,size_t species,
				std::vector< std::vector<double> > &y,
				std::vector< std::vector<double> > 
				&dydt ) {
	size_t i = compartment.index();
	dydt[i][species] += parameter(0)/(parameter(1)*y[i][variableIndex(1,0)])*
		y[i][ variableIndex(0,0) ]*1e9;
	// if (y[i][ variableIndex(0,0) ]>0)
// 		std::cerr << parameter(0)/(parameter(1)*y[i][variableIndex(1,0)])*
// 			y[i][ variableIndex(0,0) ]*1e9 << " " << i << "\n";
}
