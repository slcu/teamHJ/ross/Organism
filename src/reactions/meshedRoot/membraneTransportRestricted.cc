#include "membraneTransportRestricted.h"

//!Constructor for the MembraneTransportRestricted class
meshedRoot::MembraneTransportRestricted::MembraneTransportRestricted(std::vector<double> 
								     &paraValue, 
								     std::vector< 
								     std::vector<size_t> > 
								     &indValue ) {
  
  //Do some checks on the parameters and variable indeces
  //////////////////////////////////////////////////////////////////////
  if ( paraValue.size()!=2 ) {
    std::cerr << "MembraneTransportRestricted::MembraneTransportRestricted() "
	      << "Uses three parameters P_out, P_in (m/s) .\n";
    exit(0);
  }
  if ( (indValue.size() != 2 && indValue.size() != 3) ||
	  indValue[0].size() !=2 || indValue[1].size() !=1 ) {
    std::cerr << "MembraneTransportRestricted::MembraneTransportRestricted() "
	      << "Variable indices for two restrictMarkers and size"
	      << " (second level) needed.\n"
	      << "down, up, left, right\n";
    exit(0);
  }
  if ( indValue.size() == 3 && indValue[2].size() !=4) {
		std::cerr << "need four directions; down, up, left, right\n";
		exit(-1);
  }
  if ( indValue[0][0] == indValue[0][1] && paraValue[0] != paraValue[1] ) {
    std::cerr << "MembraneTransportRestricted::MembraneTransportRestricted() "
	      << "Same type of compartments requires same P_out and P_in.\n";
    exit(0);
  }
  //Set the variable values
  //////////////////////////////////////////////////////////////////////
  setId("membraneTransportRestricted");
  setParameter(paraValue);  
  setVariableIndex(indValue);
  
  //Set the parameter identities
  //////////////////////////////////////////////////////////////////////
  std::vector<std::string> tmp( numParameter() );
  tmp.resize( numParameter() );
  tmp[0] = "P_out";
  tmp[1] = "P_in";
  setParameterId( tmp );
}

//! Derivative contribution for a restricted membrane transport
void meshedRoot::MembraneTransportRestricted::derivs(Compartment &compartment,size_t species,
						     std::vector< std::vector<double> > &y,
						     std::vector< std::vector<double> > 
						     &dydt ) {
  
  size_t i=compartment.index();
  //Check that this compartment has neighbors and is marked for diffusion
  if( y[i][variableIndex(0,0)] < 0.5 || compartment.numNeighbor()<1 )
    return;
  double invVi = 1/y[i][variableIndex(1,0)];
  
  size_t N = compartment.numNeighbor();
  for( size_t n=0 ; n<N ; ++n ) {
    size_t j=compartment.neighbor(n);
    double invVj = 1/y[j][variableIndex(1,0)];
    //double invVj = y[j][variableIndex(1,0)];
    double Aij=compartment.neighborArea(n);
    //Only update if neighbor marked for this diffusion
    //If same type of compartments only update once (i>j)
    if( y[j][variableIndex(0,1)] > 0.5 )
      if( variableIndex(0,0) != variableIndex(0,1) || i>j ) {
	
        //D_out
	double tmp = parameter(0)*Aij*y[i][species];
	dydt[i][species] -= tmp*invVi;
	dydt[j][species] += tmp*invVj;
	//D_in
	tmp = parameter(1)*Aij*y[j][species];
	dydt[i][species] += tmp*invVi;
	dydt[j][species] -= tmp*invVj;
	//save fluxes if chosen
	if (numVariableIndexLevel()==3) {
		if (y[i][0] == y[j][0] && y[i][1] > y[j][1])  
			y[i][variableIndex(2,0)] +=  tmp*invVi;
		else if ( y[i][0] == y[j][0] && y[i][1] < y[j][1])  
			y[i][variableIndex(2,1)] +=  tmp*invVi;
		else if ( y[i][0] > y[j][0] && y[i][1] == y[j][1])  
			y[i][variableIndex(2,2)] +=  tmp*invVi;
		else if ( y[i][0] < y[j][0] && y[i][1] == y[j][1])  
			y[i][variableIndex(2,3)] += tmp*invVi;
		else {
			std::cerr << "Something is fishy!!\n";
			std::cerr << i << " " << y[i][0] << " " << y[i][1] << "\n"
			<< j << " " << y[j][0] << " " << y[j][1] << "\n";
			exit(-1);
		}
	}	
      }  
  }
}
