/*
 * Filename     : rootTransportMesh.cc
 * Description  : Classes describing updates due to molecule transports
 * Author(s)    : Pontus (pontus@thep.lu.se)
 * Created      : October 2007
 * Revision     : $Id: 
 */

#include "../baseReaction.h"
#include "outflux.h"

meshedRoot::Outflux::Outflux(std::vector<double> &paraValue, std::vector< std::vector<size_t> > &indValue ) 
{

	//Do some checks on the parameters and variable indeces
	//////////////////////////////////////////////////////////////////////
	if ( paraValue.size()!= 7 ) {
		std::cerr << "Outflux::"
			  << "Outflux() "
			  << "uses seven parameters (T_eff, T_inf, K_M, f_i, f_ij, P_out, P_in).\n";
		exit(0);
	}
	if ( indValue.size() != 3 || indValue[0].size() !=3 
	     || indValue[1].size() !=1 || indValue[2].size() !=1) {
		std::cerr << "Outflux::"
			  << "Outflux() "
			  << "cell marker, wall marker and flux marker"
			  << "indices needed in"
			  << "level 1.\n"
			  << "Polarized molecule index needed att level 2\n"
			  << "Volume index at level 3\n"; 
		exit(0);
	}
	//Check parameters
	//////////////////////////////////////////////////////////////////////
	if( paraValue[0]<0.0 || paraValue[1]<0.0 || paraValue[2]<0.0 ||
	    paraValue[3]<0.0 || paraValue[4]<0.0 || paraValue[4]>1 ) {
		std::cerr << "Outflux::"
			  << "Outflux()"
			  << " Parameters need to be positive (and fractions).\n";
		exit(0);
	}
	//Set the variable values
	//////////////////////////////////////////////////////////////////////
	setId("polarizationConstFastTransportFractionRestrictedMM");
	setParameter(paraValue);  
	setVariableIndex(indValue);
  
	//Set the parameter identities
	//////////////////////////////////////////////////////////////////////
	std::vector<std::string> tmp( numParameter() );
	tmp.resize( numParameter() );
	tmp[0] = "T_eff";
	tmp[1] = "T_inf";
	tmp[2] = "K_M";
	tmp[3] = "f_i";
	tmp[4] = "f_ij";
	tmp[5] = "P_out";
	tmp[6] = "P_in";
	setParameterId( tmp );
}

void meshedRoot::Outflux::derivs(Compartment &compartment,size_t species,
				 std::vector< std::vector<double> > &y,
				 std::vector< std::vector<double> > 
				 &dydt ) {
	size_t i = compartment.index();
	if ( y[i][variableIndex(0,2)]<=0.0 || y[i][variableIndex(0,1)]<0.5) return;
	double invVi = 1/y[i][variableIndex(2,0)];
	size_t N = compartment.numNeighbor();        
	double outfluxActive = parameter(1)*parameter(4)*invVi;
	double outfluxPassive= parameter(6)*invVi;
	for( size_t n=0 ; n<N; ++n ) {
		size_t j=compartment.neighbor(n);
		double Aij=compartment.neighborArea(n);	
		double transporter = y[j][variableIndex(1,0)];
		if ( y[j][variableIndex(0,0)] > 0.5 && transporter > 0.0) {
			double transportOut = (outfluxActive*transporter + outfluxPassive)*Aij*y[i][species];
			dydt[i][species] -= transportOut;
		}
	}
}
