#include "diffusionRestricted.h"

meshedRoot::DiffusionRestricted::DiffusionRestricted(std::vector<double> &paraValue, 
						     std::vector< std::vector<size_t> > 
						     &indValue )
{
  //Do some checks on the parameters and variable indeces
  //////////////////////////////////////////////////////////////////////
  if (paraValue.size() != 3) {
    std::cerr << "DiffusionRestricted::DiffusionRestricted() "
	      << "Uses three parameters D_out, D_in and box size.\n";
    exit(EXIT_FAILURE);
  }
  if (( indValue.size() != 2 &&  indValue.size() != 3) || indValue[0].size() != 2 || 
      indValue[1].size() != 1 ) {
    std::cerr << "DiffusionRestricted::DiffusionRestricted() "
	      << "Variable indices for two restrictMarkers, and size"
	      << " (second level) needed.\n"
	      << "down, up, left, right\n";
    exit(EXIT_FAILURE);
  }
  if ( indValue.size() == 3 && indValue[2].size() !=4) {
		std::cerr << "need four directions; down, up, left, right\n";
		exit(-1);
  }
  if (indValue[0][0] == indValue[0][1] && paraValue[0] != paraValue[1]) {
    std::cerr << "DiffusionRestricted::DiffusionRestricted() "
	      << "Same type of compartments requires same D_out and D_in.\n";
    exit(EXIT_FAILURE);
  }
  //Set the variable values
  //////////////////////////////////////////////////////////////////////
  setId("diffusionRestricted");
  setParameter(paraValue);  
  setVariableIndex(indValue);
	
  //Set the parameter identities
  //////////////////////////////////////////////////////////////////////
  std::vector<std::string> tmp(numParameter());
  tmp.resize(numParameter());
  tmp[0] = "D_out";
  tmp[1] = "D_in";
  tmp[2] = "boxSize";
  setParameterId(tmp);
  //member variables
  //factoriToj_ = parameter(0)/(parameter(2)*parameter(2));
  //factorjToi_ = parameter(1)/(parameter(2)*parameter(2));
  invDist_=1/parameter(2);
}

void meshedRoot::DiffusionRestricted::derivs(Compartment &compartment,size_t species,
					     std::vector< std::vector<double> > &y,
					     std::vector< std::vector<double> > &dydt )
{
  size_t i = compartment.index();	
  //Check that this compartment is marked for this diffusion
  if (y[i][variableIndex(0,0)] < 0.5)
    return;
  double invVi = 1/y[i][variableIndex(1,0)];
  //double invVi = y[i][variableIndex(1,0)];
  size_t N = compartment.numNeighbor();
  for (size_t n = 0; n < N; ++n) {
    size_t j = compartment.neighbor(n);	
    double invVj = 1/y[j][variableIndex(1,0)];
    //double invVj = y[j][variableIndex(1,0)];
    double Aij = compartment.neighborArea(n);
		
    //Only update if neighbor marked for this diffusion
    //If same type of compartments only update once (i>j)
    if (y[j][variableIndex(0,1)] > 0.5) {
      if (variableIndex(0,0) != variableIndex(0,1) || i > j) {
	//D_out
	double tmp = parameter(0)*Aij*y[i][species]*invDist_;
	dydt[i][species] -= tmp*invVi;
	dydt[j][species] += tmp*invVj;
	//D_in
	tmp = parameter(1)*Aij*y[j][species]*invDist_;
	dydt[i][species] += tmp*invVi;
	dydt[j][species] -= tmp*invVj;
	if (numVariableIndexLevel() == 3) {
		if (y[i][0] == y[j][0] && y[i][1] > y[j][1])  
			y[i][variableIndex(2,0)] +=  tmp*invVi;
		else if ( y[i][0] == y[j][0] && y[i][1] < y[j][1])  
		y[i][variableIndex(2,1)] +=  tmp*invVi;
		else if ( y[i][0] > y[j][0] && y[i][1] == y[j][1])  
			y[i][variableIndex(2,2)] +=  tmp*invVi;
		else if ( y[i][0] < y[j][0] && y[i][1] == y[j][1])  
			y[i][variableIndex(2,3)] += tmp*invVi;
		else {
			std::cerr << "Something is fishy!!\n";
			std::cerr << i << " " << y[i][0] << " " << y[i][1] << "\n"
			<< j << " " << y[j][0] << " " << y[j][1] << "\n";
			exit(-1);
		}
	}
      }  
    }
  }
}
