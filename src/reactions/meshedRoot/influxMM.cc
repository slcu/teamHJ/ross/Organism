/*
 * Filename     : rootTransportMesh.cc
 * Description  : Classes describing updates due to molecule transports
 * Author(s)    : Pontus (pontus@thep.lu.se)
 * Created      : October 2007
 * Revision     : $Id: 
 */
 
#include "../baseReaction.h"
#include "influxMM.h"

meshedRoot::InfluxMM::InfluxMM(std::vector<double> &paraValue, std::vector< std::vector<size_t> > &indValue ) 
{

	//Do some checks on the parameters and variable indeces
	//////////////////////////////////////////////////////////////////////
	if ( paraValue.size()!= 9 ) {
		std::cerr << "RootTransport::"
			  << "RootTransport() "
			  << "uses nine parameters "
			  << "(T_eff, T_inf, K_M, f_i, f_ij, P_out, P_in, " 
			  << "polarized molecule conc. and hypthetical auxin conc.).\n";
		exit(0);
	}
	if ( indValue.size() != 2 || indValue[0].size() !=3 
	     || indValue[1].size() !=1 ) {
		std::cerr << "RootTransport::"
			  << "RootTransport() "
			  << "cell marker, wall marker and influxMM marker at level 1"
			  << "Volume index at level 2\n"; 
		exit(0);
	}
	//Check parameters
	//////////////////////////////////////////////////////////////////////
	if( paraValue[0]<0.0 || paraValue[1]<0.0 || paraValue[2]<0.0 ||
	    paraValue[3]<0.0 || paraValue[4]<0.0 || paraValue[4]>1 ) {
		std::cerr << "RootTransport::"
			  << "RootTransport()"
			  << " Parameters need to be positive (and fractions).\n";
		exit(0);
	}
	//Set the variable values
	//////////////////////////////////////////////////////////////////////
	setId("polarizationConstFastTransportFractionRestrictedMM");
	setParameter(paraValue);  
	setVariableIndex(indValue);
  
	//Set the parameter identities
	//////////////////////////////////////////////////////////////////////
	std::vector<std::string> tmp( numParameter() );
	tmp.resize( numParameter() );
	tmp[0] = "T_eff";
	tmp[1] = "T_inf";
	tmp[2] = "K_M";
	tmp[3] = "f_i";
	tmp[4] = "f_ij";
	tmp[5] = "D_out";
	tmp[6] = "D_in";
	tmp[7] = "P";
	tmp[8] = "A";
	setParameterId( tmp );
}

void meshedRoot::InfluxMM::derivs(Compartment &compartment,size_t species,
				std::vector< std::vector<double> > &y,
				std::vector< std::vector<double> > 
				&dydt ) {
	size_t i = compartment.index();
	
	if ( y[i][variableIndex(0,2)] <= 0.0 || y[i][variableIndex(0,1)]<0.5) return;
	double invVi = 1/y[i][variableIndex(1,0)];
	//double invVi = y[i][variableIndex(1,0)];
	
	size_t N = compartment.numNeighbor();        
    
	double influxMMActive = parameter(0)*parameter(3)*invVi;
	double influxPassive= parameter(5)*invVi;
	for( size_t n=0 ; n<N; ++n ) {
		size_t j=compartment.neighbor(n);
		//Make sure neighbor marked as cell
		if ( y[j][variableIndex(0,0)] > 0.5) {
			double Aij=compartment.neighborArea(n);	
			dydt[i][species] += (influxMMActive*parameter(7)/(parameter(2)+ parameter(3)*parameter(8)) + influxPassive)*Aij*parameter(8);
			//std::cerr << (influxMMActive*parameter(7)/(parameter(2)+ parameter(3)*parameter(8)) + influxPassive)*Aij*parameter(8) << "\n";
		}
		
	}
}
