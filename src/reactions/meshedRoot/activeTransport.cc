/*
 * Filename     : rootTransportMesh.cc
 * Description  : Classes describing updates due to molecule transports
 * Author(s)    : Pontus (pontus@thep.lu.se)
 * Created      : October 2007
 * Revision     : $Id: 
 */

#include "../baseReaction.h"
#include "activeTransport.h"

meshedRoot::ActiveTransport::ActiveTransport(std::vector<double> &paraValue, std::vector< std::vector<size_t> > &indValue ) 
{

	//Do some checks on the parameters and variable indeces
	//////////////////////////////////////////////////////////////////////
	if( paraValue.size()!= 5 ){
		std::cerr << "ActiveTransport::"
		<< "ActiveTransport() "
		<< "uses five parameters (T_eff, T_inf, K_M, f_i, f_ij).\n";
		exit(0);
	}
	if ( (indValue.size() != 3 && indValue.size() != 4) || indValue[0].size() !=2 
		|| indValue[1].size() !=1 || indValue[2].size() != 1
		) {
		std::cerr << "ActiveTransport::"
		<< "ActiveTransport() "
		<< "cell marker + wall marker "
		<< "indices needed in"
		<< "level 1.\n"
		<< "Polarized molecule index needed att level 2\n"
		<< "Volume index at level 3\n" 
		<< "down, up, left, right\n";
		
		exit(0);
	}
	
	if ( indValue.size() == 4 && indValue[3].size() !=4) {
		std::cerr << "need four directions; down, up, left, right\n";
		exit(-1);
	}
	//Check parameters
	//////////////////////////////////////////////////////////////////////
	if( paraValue[0]<0.0 || paraValue[1]<0.0 || paraValue[2]<0.0 ||
	    paraValue[3]<0.0 || paraValue[4]<0.0 || paraValue[4]>1 ) {
		std::cerr << "ActiveTransport::"
			  << "ActiveTransport()"
			  << " Parameters need to be positive (and fractions).\n";
		exit(0);
	}
	//Set the variable values
	//////////////////////////////////////////////////////////////////////
	setId("polarizationConstFastTransportFractionRestrictedMM");
	setParameter(paraValue);  
	setVariableIndex(indValue);
  
	//Set the parameter identities
	//////////////////////////////////////////////////////////////////////
	std::vector<std::string> tmp( numParameter() );
	tmp.resize( numParameter() );
	tmp[0] = "T_eff";
	tmp[1] = "T_inf";
	tmp[2] = "K_M";
	tmp[3] = "f_i";
	tmp[4] = "f_ij";
	setParameterId( tmp );
}

void meshedRoot::ActiveTransport::derivs(Compartment &compartment,size_t species,
					 std::vector< std::vector<double> > &y,
					 std::vector< std::vector<double> > 
					 &dydt ) {
	size_t i = compartment.index();
	
	
	//double invVi = y[i][variableIndex(2,0)];
	//Only if the comp has neighbors and is a cell
	if( y[i][variableIndex(1,0)] <= 0 ||
	    compartment.numNeighbor() == 0 || 
	    y[i][variableIndex(0,0)]<0.5 ) return;
	double invVi = 1/y[i][variableIndex(2,0)];
        //Update all compartments    
	//////////////////////////////////////////////////////////////////////
	size_t N = compartment.numNeighbor();        
	double transporter = y[i][variableIndex(1,0)];
	for( size_t n=0 ; n<N; ++n ) {
		size_t j=compartment.neighbor(n);
		double invVj = 1/y[j][variableIndex(2,0)];
		//double invVj = y[j][variableIndex(2,0)];
		double Aij=compartment.neighborArea(n);
	
		//Make sure neighbor marked as wall and that it contains transporter 
		if ( y[j][variableIndex(0,1)] > 0.5 ) {
			double transportiToj = 
				transporter*parameter(0)*parameter(3)*Aij*y[i][species];
			double transportjToi = 
				transporter*parameter(1)*parameter(4)*Aij*y[j][species];
			dydt[i][species] -= transportiToj*invVi;
			dydt[j][species] += transportiToj*invVj;
			dydt[i][species] += transportjToi*invVi;
			dydt[j][species] -= transportjToi*invVj;
			if (numVariableIndexLevel()==4) {
				if (y[i][0] == y[j][0] && y[i][1] > y[j][1])  
					y[i][variableIndex(3,0)] +=  (transportjToi-transportiToj)*invVi;
				else if ( y[i][0] == y[j][0] && y[i][1] < y[j][1])  
					y[i][variableIndex(3,1)] +=  (transportjToi-transportiToj)*invVi;
				else if ( y[i][0] > y[j][0] && y[i][1] == y[j][1])  
					y[i][variableIndex(3,2)] +=  (transportjToi-transportiToj)*invVi;
				else if ( y[i][0] < y[j][0] && y[i][1] == y[j][1])  
					y[i][variableIndex(3,3)] +=  (transportjToi-transportiToj)*invVi;
				else {
					std::cerr << "Something is fishy!!\n";
					std::cerr << i << " " << y[i][0] << " " << y[i][1] << "\n"
					<< j << " " << y[j][0] << " " << y[j][1] << "\n";
					exit(-1);
				}
			}
		}
	}
}
