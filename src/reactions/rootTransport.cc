/*
 * Filename     : rootTransport.cc
 * Description  : Classes describing updates due to molecule transports
 * Author(s)    : Pontus (pontus@thep.lu.se)
 * Created      : October 2007
 * Revision     : $Id: 
 */

#include"baseReaction.h"
#include"rootTransport.h"

RootTransport::RootTransport(std::vector<double> 
					   &paraValue, 
					   std::vector< 
					   std::vector<size_t> > 
					   &indValue ) {
  
  //Do some checks on the parameters and variable indeces
  //////////////////////////////////////////////////////////////////////
  if( paraValue.size()!=6 ) {
    std::cerr << "RootTransport::"
							<< "RootTransport() "
							<< "uses six parameters (T_eff, T_inf, b/a, K_M, f_i, f_ij).\n";
    exit(0);
  }
  if( indValue.size() != 2 || indValue[0].size() !=3 
      || indValue[1].size() !=1  ) {
    std::cerr << "RootTransport::"
							<< "RootTransport() "
							<< "Polarized molecule + "
							<< "cell marker + wall marker "
							<< "indeces needed in"
							<< "level 1.\n" 
							<< "Variable index for size parameter needed at "
							<< "second level.\n";
    exit(0);
  }
  //Check parameters
  //////////////////////////////////////////////////////////////////////
  if( paraValue[0]<0.0 || paraValue[1]<0.0 || paraValue[2]<0.0 ||
      paraValue[3]<0.0 || paraValue[4]<0.0 || paraValue[4]>1.0 ||
      paraValue[5]<0.0 || paraValue[5]>1.0 ) {
    std::cerr << "RootTransport::"
	      << "RootTransport()"
	      << " Parameters need to be positive (and fractions).\n";
    exit(0);
  }
  //Set the variable values
  //////////////////////////////////////////////////////////////////////
  setId("polarizationConstFastTransportFractionRestrictedMM");
  setParameter(paraValue);  
  setVariableIndex(indValue);
  
  //Set the parameter identities
  //////////////////////////////////////////////////////////////////////
  std::vector<std::string> tmp( numParameter() );
  tmp.resize( numParameter() );
  tmp[0] = "T_eff";
  tmp[1] = "T_inf";
  tmp[2] = "b/a";
  tmp[3] = "K_M";
  tmp[4] = "f_i";
  tmp[5] = "f_ij";
  setParameterId( tmp );
}

void RootTransport::derivs(Compartment &compartment,size_t species,
       std::vector< std::vector<double> > &y,
       std::vector< std::vector<double> > 
       &dydt ) {
  
  size_t i=compartment.index();
  //Only if the comp has neighbors and is a cell
  if( compartment.numNeighbor() == 0 || 
      y[i][variableIndex(0,1)]<0.5 ) return;
  
  //Extract volume of the compartment and make sure it is physical
  double Vi = y[i][variableIndex(1,0)];
		
  assert( Vi>0.0 );
  
//Update all compartments
  //////////////////////////////////////////////////////////////////////
  for( size_t n=0 ; n<compartment.numNeighbor() ; n++ ) {
    size_t j=compartment.neighbor(n);
    
    //Make sure neighbor marked as wall 
		if ( y[j][variableIndex(0,2)]>0.5 ) { 
			//Extract volume of neighbor and make sure it is physical
			double Vj = y[j][variableIndex(1,0)];
			assert( Vj>0.0 );
			if( compartment.numNeighbor()==compartment.numNeighborArea() ) {
				//std::cerr << compartment.neighborArea(n) << "\n";
				double coeff = compartment.neighborArea(n)*
					parameter(4)*y[i][species]/( parameter(3)+parameter(4)*y[i][species] );
				dydt[i][species] -= parameter(0)*coeff/Vi;
				dydt[j][species] += parameter(0)*coeff/Vj;
				coeff = compartment.neighborArea(n)*
					parameter(5)*y[j][species]/( parameter(3)+parameter(5)*y[j][species] );
				dydt[i][species] += parameter(1)*coeff/Vi;
				dydt[j][species] -= parameter(1)*coeff/Vj;
			}
			else {
				std::cerr << "Warning\n";
				exit(-1);
			}
			
		}
	}
}
