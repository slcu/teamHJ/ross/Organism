#include "helperFunctions.h"

#ifndef ELONGATEDBACTERIA_WALLMECHANICS2D_H
#define ELONGATEDBACTERIA_WALLMECHANICS2D_H

namespace elongatedBacteria {
	class WallMechanics2D: public BaseReaction {
	
	public:
		
		///
		/// @brief Main constructor
		///
		/// This is the main constructor which sets the parameters and variable
		/// indices that defines the reaction.
		///
		/// @param paraValue vector with parameters
		///
		/// @param indValue vector of vectors with variable indices
		///
		/// @see BaseReaction::createReaction(std::vector<double> &paraValue,...)
	///
		WallMechanics2D(std::vector<double> &paraValue, 
										std::vector< std::vector<size_t> > &indValue );
		
		///
		/// @brief Derivative function for this reaction class
		///
		/// @see BaseReaction::derivs(Compartment &compartment,size_t species,...)
		///
		void derivs(Compartment &compartment,size_t varIndex,
								std::vector< std::vector<double> > &y,
								std::vector< std::vector<double> > &dydt );
		
		bool calculateForces(double k, double B, const Vector<2> &u, const Vector<2> &v,
												 const Vector<2> &w, Vector<2> &Fi_1, 
												 Vector<2> &Fi_2, double& ovelap);
		
	};
}
#endif
