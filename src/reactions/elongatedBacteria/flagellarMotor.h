#ifndef ELONGATEDBACTERIA_FLAGELLARMOTOR_H
#define ELONGATEDBACTERIA_FLAGELLARMOTOR_H

#include "../baseReaction.h"
#include <cmath>
#include <deque>

namespace elongatedBacteria {
	class FlagellarMotor : public BaseReaction {
		
	public:
		///
		/// @brief Main constructor
		///
		/// This is the main constructor which sets the parameters and variable
		/// indices that defines the reaction.
		///
		/// @param paraValue vector with parameters
		///
		/// @param indValue vector of vectors with variable indices
		///
		/// @see BaseReaction::createReaction(std::vector<double> &paraValue,...)
		///
		FlagellarMotor(std::vector<double> &paraValue, 
									 std::vector< std::vector<size_t> > &indValue );
		///
		/// @brief Derivative function for this reaction class
		///
		/// @see BaseReaction::derivs(Compartment &compartment,size_t species,...)
	///
		void derivs(Compartment &compartment,size_t varIndex,
								std::vector< std::vector<double> > &y,
								std::vector< std::vector<double> > &dydt );
	void update(double h, double t,
							std::vector< std::vector<double> > &y);
 private:
	std::deque<bool> tumbleFlag_;//Why deque? Beause S.Meyers says so!
	std::vector<double> runningTime_, tumbleForce_,noise_;
	std::vector<int> rotDir_;
	};
}
#endif
	
