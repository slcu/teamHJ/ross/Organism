#include "WallMechanics3D.h"

elongatedBacteria::WallMechanics3D::
WallMechanics3D(std::vector<double> &paraValue, 
								std::vector< std::vector<size_t> > &indValue ) {
  
  //Do some checks on the parameters and variable indeces
  //////////////////////////////////////////////////////////////////////
  if( paraValue.size() < 12 || (paraValue.size()-3)%3 ) {
    std::cerr << "WallCigar3D::WallCigar3D() "
							<< "Uses at least 12 k,b,D,x_1,x_2 and x_3 \n";
    exit(0);
  }
  if( indValue.size() != 1 || indValue[0].size() != 3 ) {
    std::cerr << "WallCigar3D::WallCigar3D() "
							<< "Three variable indices must be used. Stores potential, overlap, and F**2.\n";
    exit(0);
  }
  if( paraValue.size()==2 )
    paraValue.push_back(1.0);
  //Set the variable values
  //////////////////////////////////////////////////////////////////////
  setId("wallCigarNew");
  setParameter(paraValue);  
  setVariableIndex(indValue);
  
  //Set the parameter identities
  //////////////////////////////////////////////////////////////////////
  std::vector<std::string> tmp( numParameter() );
	tmp[0] = "K_force";
  tmp[1] = "b";
	tmp[2] = "D";
  size_t numPoints = (paraValue.size()-3)/3;
  for( size_t i=0 ; i<numPoints ; ++i ) {
    size_t add = i*3;
    tmp[3+add] = "x_1";
    tmp[4+add] = "y_1";
    tmp[5+add] = "z_1";
  }
	setParameterId( tmp );
  
   //Check that all points is in a plane and that the polygon is convex
  ///////////////////////////////////////////////////////////////////
	const int dimension=3;
	std::vector< Vector<dimension> > point(numPoints);
	for( size_t p=0 ; p<numPoints ; ++p ) {
		size_t add = p*3;
		point[p][0]=parameter(3+add); 
		point[p][1]=parameter(4+add); 
		point[p][2]=parameter(5+add);
	}
//use two vectors two span the plane via the normal n
	// Vector<3> tmp1 = point[1]-point[0];
// 	Vector<3> tmp2 = point[2]-point[0];
// 	Vector<3> n;
// 	n[0] = tmp1[1]*tmp2[2] - tmp1[2]*tmp2[1];
// 	n[1] = tmp1[2]*tmp2[0] - tmp1[0]*tmp2[2];
// 	n[2] = tmp1[0]*tmp2[1] - tmp1[1]*tmp2[0];
	Vector<3> n=cross(point[1]-point[0], point[2]-point[0]);
	n*=(1/sqrt(dot(n,n)));
	for (size_t p = 3; p<numPoints; ++p) {
		if (dot(n,point[p]-point[0])!=0) {
			std::cerr << "WallCigar3D() "  
								<< point[p][0] << " " << point[p][1] << " " << point[p][2]
								<< " is not in plane (" << dot(n,point[3]-point[0]) << ")\n";
			exit(-1);	
		}
	}
	int signOfCross=0; 
	for( size_t p=0 ; p<numPoints ; ++p ) {
		Vector<3> u = point[(p+1)%numPoints]-point[p];
		Vector<3> v = point[(p+2)%numPoints]-point[(p+1)%numPoints];
		
		if (!p) {
			signOfCross = cross(u,v)[2]>=0 ? 1:-1;
			//signOfCross = u[0]*v[1]-u[1]*v[0]>=0 ? 1:-1;
		}
		else {
			if (signOfCross != ( cross(u,v)[2]>=0 ? 1:-1) ) {
				//if (signOfCross != (u[0]*v[1]-u[1]*v[0]>=0 ? 1:-1 ) ) {	
				std::cerr << "WallCigar3D(): Polygon is not convex!\n";
				std::cerr << u[0] << " " << u[1] << " " << u[2] << "\n"
									<< v[0] << " " << v[1] << " " << v[2] << "\n"
									<< signOfCross << " " << (cross(u,v)[2]>=0 ? 1:-1) << "\n";
				exit(-1);
			}
		}
	}
}



void elongatedBacteria::WallMechanics3D::
derivs(Compartment &compartment,size_t varIndex,
       std::vector< std::vector<double> > &y,
       std::vector< std::vector<double> > &dydt ) {
	
	size_t i=compartment.index(); 

	assert( compartment.numTopologyVariable() == 7 );
	const int dimension=3;
  double k=parameter(0);
	double B=parameter(1);
	double D=parameter(2);
 
	size_t numPoints = static_cast<size_t>( (numParameter()-3)/3 );
	
	std::vector< Vector<3> > point(numPoints);
 
  for( size_t p=0 ; p<numPoints ; ++p ) {
		size_t add = p*3;
		point[p][0]=parameter(3+add); 
		point[p][1]=parameter(4+add); 
		point[p][2]=parameter(5+add);
	}
  //use two vectors two span the plane via the normal n
	// Vector<3> tmp1 = point[1]-point[0];
// 	Vector<3> tmp2 = point[2]-point[0];
// 	Vector<3> n;
// 	n[0] = tmp1[1]*tmp2[2] - tmp1[2]*tmp2[1];
// 	n[1] = tmp1[2]*tmp2[0] - tmp1[0]*tmp2[2];
// 	n[2] = tmp1[0]*tmp2[1] - tmp1[1]*tmp2[0];
	Vector<3> n=cross(point[1]-point[0], point[2]-point[0]);
	n*=(1/sqrt(dot(n,n)));
	//cellproperties
	Vector<3> x_1, x_2, update;
	for (int dim=0; dim<dimension; ++dim) {
		x_1[dim] = y[i][varIndex+dim];
		x_2[dim] = y[i][varIndex+dimension+dim];
	}
  //find distance to plane
	double t_a = dot((point[0]-x_1),n);
	double t_b = dot((point[0]-x_2),n);
	double t= std::fabs(t_a)< std::fabs(t_b)? t_a : t_b;
	size_t index = std::fabs(t_a)<std::fabs(t_b)? varIndex : varIndex+dimension;
	Vector<3> x_close = std::fabs(t_a)<std::fabs(t_b)? x_1 : x_2;
	double distToPlane=std::fabs(t);
	if(distToPlane <= 0) {
		mySignal::myExit();
	}
	//if close enough and if the interaction point lies within object: update
	if (distToPlane<B+D) {
		Vector<3> intPoint = x_close + t*n;
		Vector<3> object = point[1]-point[0];
		Vector<3> objToIntPoint = intPoint-point[0];
		int signOfCross=cross(objToIntPoint,object)[2]>=0 ? 1:-1;
		//int signOfCross=objToIntPoint[0]*object[1]-objToIntPoint[1]*object[0]>=0 ? 1:-1;
		bool flag = true;
		for( size_t p=1 ; p<numPoints ; ++p ) {
			object = point[(p+1)%numPoints]-point[p];
			objToIntPoint = intPoint-point[p];
			if (!p)
				signOfCross= cross(objToIntPoint,object)[2]>=0 ? 1:-1;
			//signOfCross=objToIntPoint[0]*object[1]-objToIntPoint[1]*object[0]>=0 ? 1:-1;
			else if (signOfCross != (cross(objToIntPoint,object)[2]>=0 ? 1:-1) )
				//else if (signOfCross != objToIntPoint[0]*object[1]-objToIntPoint[1]*object[0]>=0 ? 1:-1)
				flag = false;
		}
		//Update
		if (flag) {
			double fac =k*(B+D-distToPlane)*sqrt(B+D-distToPlane);
			double potential = fac*(B+D-distToPlane);
			update = n*(fac/(2*distToPlane));
			if(t>0)
				update*=(-1.0);
			for (int dim=0 ; dim<dimension ; dim++ ) {
				dydt[i][index+dim]+= update[dim];
			}
			dydt[i][variableIndex(0,0)] += potential;
			dydt[i][variableIndex(0,1)] += (B+D-distToPlane>0) ? (B+D-distToPlane) : 0;
			dydt[i][variableIndex(0,2)] += dot(update,update);
		}
	}
}

