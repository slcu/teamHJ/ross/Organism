#ifndef ELONGATEDBACTERIA_NEIHBORHOODLATTICEWALLS2D_H
#define ELONGATEDBACTERIA_NEIHBORHOODLATTICEWALLS2D_H
#include "../../organism.h"
#include "../../compartment/baseCompartmentNeighborhood.h"

///
/// @brief Defines neighborhood by placing each cigar-shaped cell in a lattice
/// box
///
namespace elongatedBacteria {
	class NeighborhoodLatticeWalls2D: public BaseCompartmentNeighborhood {
		
	public:
  	
		///
		/// @brief Main constructor
		///
		/// This is the main constructor which checks and sets the parameters and
		/// variable indices that defines the neighborhood rule.
		///
		/// @param paraValue Vector with parameters used.
		///
		/// @param indValue Vector of vectors with variable indices used in the
		/// neighborhood calculations.
		///
		/// @see createCompartmentNeighborhood(std::vector<double> &, std::vector<
		/// std::vector<size_t> > &, const std::string &)
		///
		NeighborhoodLatticeWalls2D(std::vector<double> &paraValue, 
															 std::vector< std::vector<size_t> > 
															 &indValue );
		
		///
		/// @brief Creates the (potential) neighborhood before simulation
		///
		/// @see BaseCompartmentNeighborhood::create()
		///
		unsigned int create(Organism &O, std::vector< std::vector<double> > &y,
												double t=0.0 );
		
		///
		/// @brief Updates the (potential) neighborhood during a simulation
		///
		/// @see BaseCompartmentNeighborhood::update()
		///
		unsigned int update(Organism &O, std::vector< std::vector<double> > &y,
												double t=0.0 );
		
		unsigned int setNeighbors(const std::vector< std::vector<double> > &y,
														 std::vector< std::vector<size_t> > &neighbors,
														 std::vector< std::vector<size_t> > &neighborsWall);
		void checkBactPosition(size_t N, const std::vector< std::vector<double> > &y,
													std::vector<size_t> &isInBox);
	private:
		std::vector< std::vector< size_t > > wallNeighborLattice_;
		unsigned int numWall_;
		unsigned int numBox_;
		unsigned int totalNumberOfBoxes_;
		unsigned int  boxCount_[2];
		std::vector< std::vector< size_t > > hasBactCell_;
	};
}
#endif //ELONGATEDBACTERIA_NEIHBORHOODLATTICEWALLS2D_H
