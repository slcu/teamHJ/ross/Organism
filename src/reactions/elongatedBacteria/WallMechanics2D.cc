#include "WallMechanics2D.h"

elongatedBacteria::WallMechanics2D::WallMechanics2D(std::vector<double> &paraValue, 
						    std::vector< std::vector<size_t> > 
						    &indValue ) {
  
	//Do some checks on the parameters and variable indeces
	//////////////////////////////////////////////////////////////////////
	if( paraValue.size()<6 || (paraValue.size()-2)%4 ) {
		std::cerr << "Wall2D::Wall2D()"
			  << "Uses at least six parameters K_force b and x_1, y_1, x_2, "
			  << "y_2 [x_1 y_1 x_2 y_2,...].\n";
		exit(0);
	}	
	if( indValue.size() != 1 || indValue[0].size() != 2 ) {
		std::cerr << "ElongatedBacteria::ElongatedBacteria() "
			  << "use two index, stores force and overlap\n" << indValue.size() << "\n";;
		exit(0);
	}
	//Set the variable values
	//////////////////////////////////////////////////////////////////////
	setId("Wall2D");
	setParameter(paraValue);  
	setVariableIndex(indValue);
  
	//Set the parameter identities
	//////////////////////////////////////////////////////////////////////
	std::vector<std::string> tmp( numParameter() );
	tmp[0] = "K_force";
	tmp[1] = "b";
	size_t numWall = (paraValue.size()-2)/4;
	for( size_t i=0 ; i<numWall ; ++i ) {
		size_t add = i*4;
		tmp[2+add] = "x_1";
		tmp[3+add] = "y_1";
		tmp[4+add] = "x_2";
		tmp[5+add] = "y_2";
	}
	setParameterId( tmp );
}

void elongatedBacteria::WallMechanics2D::
derivs(Compartment &compartment,size_t varIndex,
       std::vector< std::vector<double> > &y,
       std::vector< std::vector<double> > &dydt ) {
	size_t numNeigh = compartment.numNeighborAtLevel(1);
	if( !numNeigh ) return;//Only update if compartment has wall neighs
	size_t i=compartment.index(); 
	size_t dimension=2;    
  
	//initialize the vectors we need
	Vector<2> u,v,w, Fi_1, Fi_2;
	for ( size_t dim=0 ; dim<dimension ; ++dim )
		u[dim] = y[i][varIndex+dim+dimension]-y[i][varIndex+dim];
  
	for ( size_t k=0 ; k<numNeigh ; ++k ){
		size_t wallI = compartment.neighborAtLevel(1,k);
		//Wallproperties
		Vector<2> wallPos1, wallPos2;
		size_t add = 4*wallI;
		wallPos1[0]=parameter(2+add);
		wallPos1[1]=parameter(3+add); 
		wallPos2[0]=parameter(4+add); 
		wallPos2[1]=parameter(5+add);
    
		for( size_t dim=0 ; dim<dimension ; ++dim ){
			v[dim] = wallPos2[dim] - wallPos1[dim];
			w[dim] = y[i][varIndex+dim] - wallPos1[dim];
		}
		double overlap;
		if (calculateForces(parameter(0),parameter(1),u,v,w, Fi_1, Fi_2, overlap) ) {
			double FiSquare = dot(Fi_1+Fi_2, Fi_1+Fi_2);
			double zetaInv_i = 
				1/(2*parameter(1)+std::sqrt(dot(u,u))-std::fabs(dot(u, Fi_1+Fi_2))/std::sqrt(FiSquare));
			for( size_t dim=0 ; dim<dimension ; ++dim ) {
				dydt[i][varIndex + dim] += zetaInv_i*Fi_1[dim];
				dydt[i][varIndex + dim + dimension] += zetaInv_i*Fi_2[dim];
			}
			dydt[i][variableIndex(0,0)] += FiSquare;
			dydt[i][variableIndex(0,1)] += overlap;
		}
	}
}


bool elongatedBacteria::WallMechanics2D::calculateForces(double k,double B, const Vector<2> &u, 
							 const Vector<2> &v, const Vector<2> &w,
							 Vector<2> &Fi_1, Vector<2> &Fi_2, double &overlap)
{
	bool hasUpdated=false;
	Fi_1.fill(0.0);
	Fi_2.fill(0.0);
	double a = dot(u,u);
	double b = dot(u,v);
	double c = dot(v,v);
	double d = dot(u,w);
	double e = dot(v,w);
	
	double D=a*c-b*b;
	double sN,sD=D,tN,tD=D;
	double aInv = 1.0/a, cInv = 1.0/c, DInv = 1.0/D;
	
	Cases flag = getCase(sN,sD,tN,tD,D, a, b, c, d ,e);
	
	double sc = sN/sD; 
	double tc = tN/tD;
	
	Vector<2> dP; 
	dP = w + u*sc -tc*v;
	
	double dist=dot(dP,dP);     
	if (dist <= 0) {
		mySignal::myExit();
	}
	
	dist = std::sqrt(dist);
	//only if it's a true neighbor
	overlap=2*B - dist;
	if(overlap > 0){
		hasUpdated=true;
		//checks which derivative that should be used
		Vector<2> dsdxi_1, dsdxi_2,	dtdxi_1, dtdxi_2;
		dsdxi_1.fill(0.0);
		dsdxi_2.fill(0.0);	
		dtdxi_1.fill(0.0); 
		dtdxi_2.fill(0.0);	
		
		switch (flag) {
		case Boundary1_u_Inner_v:
			dtdxi_1 = cInv*v;
			break;
		case Inner_u_Inner_v:
			assert( D != 0.0 );
			dsdxi_1 = ( v*( b*(1  - 2*sN*DInv) -e ) + u*c*( 2*sN*DInv - 1 ) + c*w)*DInv;
			dsdxi_2 = ( v*( e + 2*b*sN*DInv ) - c*( w + 2*sN*DInv*u ))*DInv;
				
			dtdxi_1 = ( v*( a + d - 2*b*tN*DInv ) + u*( 2*c*tN*DInv - 2*e - b) + b*w )*DInv;
			dtdxi_2 = ( v*( 2*b*tN*DInv - d ) + u*( 2*e - 2*c*tN*DInv) - b*w )*DInv;
			break;
		case Boundary2_u_Inner_v:
			dtdxi_2 = v*cInv;
			break;
		case Boundary_u_Boundary_v:
			break;
		case Inner_u_Boundary1_v:
			dsdxi_1 = ( w - u*(1+2*d*aInv) )*aInv;
			dsdxi_2 = (2*d*u*aInv - w )*aInv;
			break;
		case Inner_u_Boundary2_v:
			dsdxi_1 = ( w - v + u*( 2*sN*aInv - 1) )*aInv;
			dsdxi_2 = (v - 2*sN*aInv*u - w )*aInv;
			break;
		default:
			std::cerr << "elongatedBacteria::derivs(): " << flag << "is not a valid option!\n";
			exit(-1);
		}		
		double fac = (overlap)*std::sqrt(overlap) ;
		fac *= k/(dist);
		double sFac = sc*a + d - tc*b;
		double tFac =tc*c - e - sc*b;
		Fi_1 = fac*( (1 - sc)*dP + dsdxi_1*sFac +	 dtdxi_1*tFac );
		Fi_2 = fac*( sc*dP + dsdxi_2*sFac + dtdxi_2*tFac );
		
	}
	return hasUpdated;
}

