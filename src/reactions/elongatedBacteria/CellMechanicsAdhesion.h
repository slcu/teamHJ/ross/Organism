#ifndef ELONGATEDBACTERIA_CELLMECHANICSADHESION_H
#define ELONGATEDBACTERIA_CELLMECHANICSADHESION_H

#include "helperFunctions.h"

namespace elongatedBacteria {
  ///
  /// @brief Calculates mechanical update for elongated bacteria including adhesion
  /// 
  /// A distance between two potentially neighboring bacteria is
  /// calculated and a perpendicular (to the maximal ocerlap/minimal
  /// distance) Force is used for updating velocities of the two
  /// positions describing the cells. In addition an internal spring
  /// Force is calculated. The distance is calculated via a distance
  /// rule between two line segments. If overlapping, the cells are
  /// repelled and if close enough, while not overlapping, an adhesive
  /// force is used. In a model file, it can be called either for 2D
  /// or 3D as
  ///
  /// @verbatim
  /// elongatedBacteria::cellMechanicsAdhesion2D 6 1 2
  /// K_int K_el b K_adh T_adh
  /// F^2_index overlap_index
  /// @verbatim
  /// 
  /// where 2D can be changed to 3D (in the name).
  /// The parameters are @f$K_{int}@f$: the internal spring constant,
  /// @f$K_{el}@f$: the elastic overlap constant, @f$b@f$: the width
  /// (radius) of the bacteria, @f$K_{adh}@f$:the adhesive strength,
  /// and @f$T_{adh}@f$: the distance between bacteria for which there
  /// are adhesion.
  /// The two index given are for variables used to store @f$F^2@f$:
  /// total force (squared) acting on the bacteria, and overlap
  /// (negative if adhesion is acting).
  ///
  /// The mathematics for this function is described in the
  /// Supplementary Material for Cho, Jonsson et al, PLoS Biology
  /// (2008)
  ///
  template<int T>
    class CellMechanicsAdhesion : public BaseReaction {
    
  public:
    
    //
    /// @brief Main constructor
    ///
    /// This is the main constructor which sets the parameters and variable
    /// indices that defines the reaction.
    ///
    /// @param paraValue vector with parameters
    ///
    /// @param indValue vector of vectors with variable indices
    ///
    /// @see BaseReaction::createReaction(std::vector<double> &paraValue,...)
    ///
    CellMechanicsAdhesion(std::vector<double> &paraValue, 
			  std::vector< std::vector<size_t> > &indValue );
    
    ///
    /// @brief Derivative function for this reaction class
    ///
    /// @see BaseReaction::derivs(Compartment &compartment,size_t species,...)
    ///
    void derivs(Compartment &compartment,size_t varIndex,
		std::vector< std::vector<double> > &y,
		std::vector< std::vector<double> > &dydt );
    
    bool calculateForces(double k_re, double k_ad, double B, const Vector<T> &u, const Vector<T> &v,
			 const Vector<T> &w, Vector<T> &Fi_1, 
			 Vector<T> &Fi_2, Vector<T> &Fj_1, Vector<T> &Fj_2, double &overlap);
  };
  
  
  template <int T> CellMechanicsAdhesion<T>::
    CellMechanicsAdhesion(std::vector<double> &paraValue, 
			  std::vector< std::vector<size_t> > &indValue ) {
    
    // Do some checks on the parameters and variable indeces
    //
    if( paraValue.size() != 5 ) {
      std::cerr << "CellMechanicsAdhesion::CellMechanicsAdhesion() "
		<< "Uses five parameters K_Internal, K_elastic, b, "
		<< "K_adhesion Threshold_adhesion." << std::endl;
      exit(0);
    }
    if( indValue.size() != 1 || indValue[0].size() !=2 ) {
      std::cerr << "CellMechanicsAdhesion::CellMechanicsAdhesion() "
		<< "use two index, stores force squared and overlap" 
		<<  std::endl << indValue.size() << std::endl;
      exit(0);
    }
    std::cerr <<indValue.size() << " " << indValue[0].size()<< "\n";
    
    // Set the variable values
    //
    setId("cellMechanicsAdhesion");
    setParameter(paraValue);  
    setVariableIndex(indValue);
    
    // Set the parameter identities
    //
    std::vector<std::string> tmp( numParameter() );
    tmp[0] = "K_int";
    tmp[1] = "K_el";
    tmp[2] = "b";
    tmp[3] = "K_adh";
    tmp[4] = "Threshold_adh";
    setParameterId( tmp );
  }
  
  template <int T> void CellMechanicsAdhesion<T>::
    derivs(Compartment &compartment,size_t varIndex,
	   std::vector< std::vector<double> > &y,
	   std::vector< std::vector<double> > &dydt ) {
    
    size_t i=compartment.index(); 
    size_t dimension=(compartment.numTopologyVariable()-1)/2;    
    if ( dimension != T ) {
      std::cerr << "CellMechanics::derivs() "
		<< "Wrong number of topology variables.\n";
      exit(-1);
    }
    //initialize the vectors we need
    Vector<T> u,v,w, Fi_1, Fi_2, Fj_1, Fj_2;
    
    
    for( size_t dim=0 ; dim<T; ++dim )
      u[dim]=y[i][varIndex+dim+T]-y[i][varIndex+dim];
    
    double a = dot(u,u);
    if (a<=0.0 ) {
      std::cerr << "negative square?";
      exit(-1);
    }
    
    //contribution from the spring between the two points
    double A= std::sqrt(a);
    double B=parameter(2);
    double restLength =y[i][compartment.numTopologyVariable() - 1 + varIndex];
    double internalDerivFactor = parameter(0)*(A-restLength)/A;
    for ( size_t dim =0 ; dim<T ; dim++ ) {
      dydt[i][varIndex + dim] += internalDerivFactor*u[dim];
      dydt[i][varIndex + dim + T] -= internalDerivFactor*u[dim];
    }
    
    if( compartment.numNeighbor()<1 ) return;
    //Loop over neighbors
    for ( size_t k=0 ; k<compartment.numNeighbor() ; k++ ) {
      //Set neighbor parameters
      size_t j = compartment.neighbor(k);
      if ( j > i ) {
	for( size_t dim=0 ; dim<T ; ++dim ){
	  v[dim]=y[j][varIndex+dim+T]-y[j][varIndex+dim];
	  w[dim]=y[i][varIndex+dim]-y[j][varIndex+dim];
	}
	double overlap;
	if (calculateForces(parameter(1),parameter(3),B,u,v,w, Fi_1, Fi_2, Fj_1, Fj_2, overlap) ) {
	  // Closer than threshold
	  double FiSquare = dot(Fi_1+Fi_2, Fi_1+Fi_2);
	  double FjSquare = dot(Fj_1+Fj_2, Fj_1+Fj_2);
	  double zetaInv_i = 1/(2*B+A-std::fabs(dot(u, Fi_1+Fi_2))/std::sqrt(FiSquare));
	  double zetaInv_j = 1/(2*B+std::sqrt(dot(v,v))-std::fabs(dot(v, Fj_1+Fj_2)) /
				std::sqrt(FjSquare));
	  for ( size_t dim=0 ; dim<T ; ++dim ) {
	    dydt[i][varIndex + dim] += zetaInv_i*Fi_1[dim]; 
	    dydt[i][varIndex + dim + T] += zetaInv_i*Fi_2[dim]; 
	    dydt[j][varIndex + dim] += zetaInv_j*Fj_1[dim];  
	    dydt[j][varIndex + dim + T] += zetaInv_j*Fj_2[dim]; 
	  }
	  dydt[i][variableIndex(0,0)] += FiSquare;
	  dydt[j][variableIndex(0,0)] += FjSquare;
	  dydt[i][variableIndex(0,1)] += overlap;
	  dydt[j][variableIndex(0,1)] += overlap;
	}
      }
    }
  }
  
  template <int T> bool CellMechanicsAdhesion<T>::
    calculateForces(double k_re,double k_ad,double B, 
		    const Vector<T> &u, 
		    const Vector<T> &v, const Vector<T> &w,
		    Vector<T> &Fi_1, Vector<T> &Fi_2, 
		    Vector<T> &Fj_1, Vector<T> &Fj_2, 
		    double &overlap)
    {
      bool hasUpdated = false;
      Fi_1.fill(0.0);
      Fi_2.fill(0.0);
      Fj_1.fill(0.0);
      Fj_2.fill(0.0);
      
      double a = dot(u,u);
      double b = dot(u,v);
      double c = dot(v,v);
      double d = dot(u,w);
      double e = dot(v,w);
      if (c<0.0 ) {
	std::cerr << "negative square?";
	exit(-1);
      }
      double D=a*c-b*b;
      double sN,sD=D,tN,tD=D;
      double aInv = 1.0/a, cInv = 1.0/c, DInv = 1.0/D;
      
      Cases flag = getCase(sN,sD,tN,tD,D, a, b, c, d ,e);
      
      double sc = sN/sD; 
      double tc = tN/tD;
      
      Vector<T> dP; 
      dP = w + u*sc -tc*v;
      
      double dist=dot(dP,dP);     
      if (dist <= 0) {
	mySignal::myExit();
      }
      
      dist = std::sqrt(dist);
      //only if it's a true neighbor (the cells are closer than the provided threshold)
      overlap = 2*B-dist;
      if(overlap > -parameter(4)){
	hasUpdated=true;
	//checks which derivative that should be used
	Vector<T> dsdxi_1, dsdxi_2,	dsdxj_1, dsdxj_2,
	  dtdxi_1, dtdxi_2,	dtdxj_1, dtdxj_2;
	dsdxi_1.fill(0.0);
	dsdxi_2.fill(0.0);	
	dsdxj_1.fill(0.0); 
	dsdxj_2.fill(0.0);
	dtdxi_1.fill(0.0); 
	dtdxi_2.fill(0.0);	
	dtdxj_1.fill(0.0); 
	dtdxj_2.fill(0.0);
	
	switch (flag) {
	case Boundary1_u_Inner_v:
	  dtdxi_1 = cInv*v;
	  dtdxj_1 = (v*(2*e*cInv - 1)-w)*cInv;
	  dtdxj_2 = (w - 2*e*cInv*v)*cInv;
	  break;
	case Inner_u_Inner_v:
	  assert( D != 0.0 );
	  dsdxi_1 = ( v*( b*(1  - 2*sN*DInv) -e ) + u*c*( 2*sN*DInv - 1 ) + c*w)*DInv;
	  dsdxi_2 = ( v*( e + 2*b*sN*DInv ) - c*( w + 2*sN*DInv*u ))*DInv;
	  dsdxj_1 = ( v*( 2*d - b + 2*a*sN*DInv ) + u*( c - e - 2*b*sN*DInv ) - b*w )*DInv;
	  dsdxj_2 = ( u*( e + 2*b*sN*DInv ) - v*( 2*d + 2*a*sN*DInv ) + b*w )*DInv;
	  
	  dtdxi_1 = ( v*( a + d - 2*b*tN*DInv ) + u*( 2*c*tN*DInv - 2*e - b) + b*w )*DInv;
	  dtdxi_2 = ( v*( 2*b*tN*DInv - d ) + u*( 2*e - 2*c*tN*DInv) - b*w )*DInv;
	  dtdxj_1 = ( v*( 2*a*tN*DInv - a) + u*( d + b - 2*tN*b*DInv ) - a*w)*DInv;
	  dtdxj_2 = ( a*( w - v*2*tN*DInv) + u*(2*b*tN*DInv -d) )*DInv;
	  break;
	case Boundary2_u_Inner_v:
	  dtdxi_2 = v*cInv;
	  dtdxj_1 = ( v*( 2*cInv*tN - 1) - w - u )*cInv;
	  dtdxj_2 = ( w + u -  2*cInv*tN*v )*cInv;
	  break;
	case Boundary_u_Boundary_v:
	  break;
	case Inner_u_Boundary1_v:
	  dsdxi_1 = ( w - u*(1+2*d*aInv) )*aInv;
	  dsdxi_2 = (2*d*u*aInv - w )*aInv;
	  dsdxj_1 = u*aInv; 
	  break;
	case Inner_u_Boundary2_v:
	  dsdxi_1 = ( w - v + u*( 2*sN*aInv - 1) )*aInv;
	  dsdxi_2 = (v - 2*sN*aInv*u - w )*aInv;
	  dsdxj_2 = u*aInv;
	  break;
	default:
	  std::cerr << "elongatedBacteriaAdhesion::calculateForce(): " << flag 
		    << "is not a valid option!\n";
	  exit(-1);
	}		
	double fac = (overlap)*std::sqrt(std::fabs(overlap)) ;
	//double potential= fac*(2*B - dist);
	if (overlap>=0.0)
	  fac *= k_re/(dist);
	else
	  fac *= k_ad/(dist);
	
	double sFac = sc*a + d - tc*b;
	double tFac = tc*c - e - sc*b;
	Fi_1 = fac*( (1 - sc)*dP + dsdxi_1*sFac + dtdxi_1*tFac );
	Fi_2 = fac*( sc*dP + dsdxi_2*sFac + dtdxi_2*tFac );
	Fj_1 = fac*( (tc -1)*dP + dsdxj_1*sFac + dtdxj_1*tFac );
	Fj_2 = fac*( -tc*dP + dsdxj_2*sFac + dtdxj_2*tFac );			
      }
      return hasUpdated;
    }
}

#endif 
