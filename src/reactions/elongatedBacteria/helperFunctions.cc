#include "helperFunctions.h"

using elongatedBacteria::Cases;

//!Constructor for the lagrangianCigar class

Cases elongatedBacteria::getCase(double& sN,double& sD,double& tN,double& tD,const double D, 
						const double a,const double b, const double c, const double d,
						const double e)
{
	Cases flag;
  
	if( D<1e-10 ) {
		//almost parallel, use yi[d] to prevent division by zero
		sN = 0.0;
		sD = 1.0;
		tN = e;
		tD = c;
		flag = Boundary1_u_Inner_v;
	}
	else {
		sN = b*e-c*d;
		tN = a*e-b*d;
		flag = Inner_u_Inner_v;
		if( sN<=0.0 ) {
			sN = 0.0;
			tN = e;
			tD = c;
			flag = Boundary1_u_Inner_v;
		}
		else if( sN>=sD ) {
			sN = sD;
			tN = e+b;
			tD = c;
			flag = Boundary2_u_Inner_v;
		}
	}
	if( tN<=0.0 ) {
		tN = 0.0;
		flag = Boundary_u_Boundary_v;
		if( -d<0 )
			sN = 0.0;
		else if( -d>a )
			sN = sD;
		else {
			flag = Inner_u_Boundary1_v;
			sN = -d;
			sD = a;
		}
	}
	else if( tN>=tD ) {
		tN = tD;
		flag = Boundary_u_Boundary_v;;
		if( (-d+b)<0.0 )
			sN = 0.0;
		else if( (-d+b)>a )
			sN = sD;
		else {
			sN = -d+b;
			sD = a;
			flag = Inner_u_Boundary2_v;
		}
	}
	
	assert( sD != 0.0 );
	assert( tD != 0.0 );
	return flag;
}
