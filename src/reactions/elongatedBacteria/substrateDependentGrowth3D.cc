#include "substrateDependentGrowth3D.h"


elongatedBacteria::SubstrateDependentGrowth3D::
SubstrateDependentGrowth3D(std::vector<double> &paraValue, 
			    std::vector< std::vector<size_t> > &indValue )
{
	
	//Do some checks on the parameters and variable indeces
	//////////////////////////////////////////////////////////////////////
	if ( paraValue.size()!=3 ) {
		std::cerr << "elongatedBacteria::SubstrateDependentGrowth::SubstrateDependentGrowth() "
			  << "Uses three parameters k_max, K and b\n";
		exit(0);
	}
	if ( indValue.size() !=1 || indValue[0].size() !=1 ) {
		std::cerr << "elongatedBacteria::SubstrateDependentGrowthRestriced::SubstrateDependentGrowth() "
			  << "Needs one index, ligand concentration.\n";
		exit(0);
	}
	//Set the variable values
	//////////////////////////////////////////////////////////////////////
	setId("exponentialGrowth");
	setParameter(paraValue);  
	setVariableIndex(indValue);
  
	//Set the parameter identities
	//////////////////////////////////////////////////////////////////////
	std::vector<std::string> tmp( numParameter() );
	tmp.resize( numParameter() );
	tmp[0] = "k_growth";
	setParameterId( tmp );
}


void elongatedBacteria::SubstrateDependentGrowth3D::
derivs(Compartment &compartment,size_t varIndex,
       std::vector< std::vector<double> > &y,
       std::vector< std::vector<double> > &dydt )
{
	size_t i=compartment.index();
	size_t dCol = compartment.numTopologyVariable()-1+varIndex;
	double S = y[i][variableIndex(0,0)];
	dydt[i][dCol] += parameter(0)*(4*parameter(2)/3 + y[i][dCol])*S / ( parameter(1) + S );
}
