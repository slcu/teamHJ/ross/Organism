#include "gradientGaussian2D.h"
#include <cmath>
elongatedBacteria::GradientGaussian2D::GradientGaussian2D(std::vector<double> &paraValue, 
					     std::vector< std::vector<size_t> > 
					     &indValue ) 
{
	//Do some checks on the parameters and variable indeces
	//////////////////////////////////////////////////////////////////////
	if( paraValue.size()!=4 ) {
		std::cerr << "GradientGaussian2D::GradientGaussian2D() "
			  << "Uses four parameters heigth, width, x_c, y_c\n";
		exit(0);
	}
	if ( indValue.size() !=1 && 
	     indValue[0].size() != 1) { 
		std::cerr << "GradientGaussian2D::GradientGaussian2D() "
			  << "uses one index\n"; 
		exit(0);
	}
	//Set the variable values
	//////////////////////////////////////////////////////////////////////
	setId("gradientGaussian2D");
	setParameter(paraValue);  
	setVariableIndex(indValue);
	//Set the parameter identities
	//////////////////////////////////////////////////////////////////////
	std::vector<std::string> tmp( numParameter() );
	tmp[0] = "height";
	tmp[1] = "width";
	tmp[2] = "x_c";
	tmp[3] = "y_c";
	setParameterId( tmp );
}
void elongatedBacteria::GradientGaussian2D::derivs(Compartment &compartment,size_t varIndex,
				   std::vector< std::vector<double> > &y,
				   std::vector< std::vector<double> > &dydt ) 
{
	size_t i=compartment.index();
	double X = 0.5*(y[i][0] + y[i][2]);
	double Y = 0.5*(y[i][1] + y[i][3]);
	double grad =parameter(0)*std::exp(-( 
						   (X-parameter(2))*(X-parameter(2))+
						   (Y-parameter(3))*(Y-parameter(3)))/parameter(1));
	y[i][variableIndex(0,0)] = grad > 0.0 ? grad:0.0;
}
