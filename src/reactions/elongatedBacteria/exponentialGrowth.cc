#include "exponentialGrowth.h"

//!Constructor for the EllipticGrowth class
elongatedBacteria::ExponentialGrowth::
ExponentialGrowth(std::vector<double> &paraValue, 
		  std::vector< std::vector<size_t> > &indValue ) {
  
	//Do some checks on the parameters and variable indeces
	//////////////////////////////////////////////////////////////////////
	if ( paraValue.size()!=1 ) {
		std::cerr << "elongatedBacteria::ExponentialGrowth::ExponentialGrowth() "
			  << "Uses one parameter k_growth\n";
		exit(0);
	}
	if ( indValue.size() ) {
		std::cerr << "elongatedBacteria::ExponentialGrowth::ExponentialGrowth() "
			  << "No variable index are used.\n";
		exit(0);
	}
	//Set the variable values
	//////////////////////////////////////////////////////////////////////
	setId("exponentialGrowth");
	setParameter(paraValue);  
	setVariableIndex(indValue);
  
	//Set the parameter identities
	//////////////////////////////////////////////////////////////////////
	std::vector<std::string> tmp( numParameter() );
	tmp.resize( numParameter() );
	tmp[0] = "k_growth";
	setParameterId( tmp );
}


void elongatedBacteria::ExponentialGrowth::
derivs(Compartment &compartment,size_t varIndex,
       std::vector< std::vector<double> > &y,
       std::vector< std::vector<double> > &dydt ) {
	size_t i=compartment.index();
	size_t dCol = compartment.numTopologyVariable()-1+varIndex;
	dydt[i][dCol] += parameter(0)*y[i][dCol];
}
