#ifndef ELONGATEDBACTERIA_H
#define ELONGATEDBACTERIA_H

#include "../../reactions/baseReaction.h"

#include "../../common/vectorTemplate.h"
#include "../../common/mySignal.h"

namespace elongatedBacteria {
	enum Cases{Boundary1_u_Inner_v=0, Inner_u_Inner_v, Boundary2_u_Inner_v, 
						 Boundary_u_Boundary_v, Inner_u_Boundary1_v, Inner_u_Boundary2_v};
	Cases getCase(double& sN,double& sD,double& tN,double& tD,const double D,const double a,
								const double b, const double c, const double d, const double e);
}
#endif //ELONGATEDBACTERIA_H
