#ifndef ELONGATEDBACTERIA_CELLDIVISION_H
#define ELONGATEDBACTERIA_CELLDIVISION_H

#include <cmath>

#include "../../compartment/baseCompartmentChange.h"
#include "../../common/vectorTemplate.h"
#include "../../common/myRandom.h"

namespace elongatedBacteria {
	const double PI=3.141592653589793238512808959406186204433;
	template <int T>
	class CellDivision : public BaseCompartmentChange {
		
	public:
		CellDivision(std::vector<double> &paraValue, 
			     std::vector< std::vector<size_t> > 
			     &indValue );
		
		int flag(Compartment &compartment,
			 std::vector< std::vector<double> > &y,
			 std::vector< std::vector<double> > &dydt );
		void update(Organism &O,Compartment &compartment,
			    std::vector< std::vector<double> > &y,
			    std::vector< std::vector<double> > &dydt, double t=0.0 );
		double getVolume(double length, double radius);
		double getLength(double volume, double radius);
	};
	
	
	template <int T> CellDivision<T>::
	CellDivision(std::vector<double> &paraValue, 
		     std::vector< std::vector<size_t> > 
		     &indValue ) {
		
		//Do some checks on the parameters and variable indeces
		//////////////////////////////////////////////////////////////////////
		if( paraValue.size()!=4 ) {
			std::cerr << "CellDivison::"
				  << "CellDivision() "
				  << "Uses four parameters b d_th d_diff d_random.\n";
			exit(0);
		}
		if( indValue.size() ) {
			std::cerr << "CellDivision::CellDivision() "
				  << "No variable index is used.\n";
			exit(0);
		}
		//Set the variable values
		//////////////////////////////////////////////////////////////////////
		setId("CellDivision");
		setParameter(paraValue);  
		setVariableIndex(indValue);
		
		//Set the parameter identities
		//////////////////////////////////////////////////////////////////////
		std::vector<std::string> tmp( numParameter() );
		tmp.resize( numParameter() );
		tmp[0] = "b";
		tmp[1] = "d_th";
		tmp[2] = "d_diff";
		tmp[3] = "d_random";
		
		setParameterId( tmp );
		
		//Set the number of compartment change (+1 for division)
		//////////////////////////////////////////////////////////////////////
		setNumChange(1);
	}
	
//! Flags for division if the radius is larger than a threshold value
	template<int T> int CellDivision<T>::
	flag(Compartment &compartment,
	     std::vector< std::vector<double> > &y,
	     std::vector< std::vector<double> > &dydt ) {
		
		if( y[compartment.index()][compartment.numTopologyVariable()-1] > 
		    parameter(1) ) 
			return 1;
		else 
			return 0;
	}
	
	//! Divides a cigar-shaped cell
	template<int T>
	void CellDivision<T>::update(Organism &O, Compartment &compartment, std::vector< std::vector<double> > &y,
				     std::vector< std::vector<double> > &dydt, double t )
	{	
		size_t i=compartment.index();
		size_t x1Col[T], x2Col[T];
		for( size_t d=0; d < T; ++d ) {
			x1Col[d] = d;
			x2Col[d] = d+T;
		}
		size_t dCol = compartment.numTopologyVariable()-1;
  
		O.addDivision(i,t);
		Vector<T> center, direction;
  
		//Extract direction from spatial variables
		//////////////////////////////////////////////////////////////////////
		for( size_t d=0 ; d<T ; d++ ) {
			center[d] = 0.5*(y[i][x2Col[d]]+y[i][x1Col[d]]);
			direction[d] = (y[i][x2Col[d]]-y[i][x1Col[d]]);
		}
		double length = std::sqrt( dot(direction,direction) );
		direction *= 1/length;
		double volume = getVolume(length, parameter(0));
		//Divide
		//////////////////////////////////////////////////////////////////////
		size_t NN=O.numCompartment();
		size_t N=NN+1;
		size_t M=compartment.numVariable();
		O.addCompartment( compartment );
		O.compartment(NN).setIndex(NN);
  
		y.resize( N );
		dydt.resize( N );
		y[NN].resize( M );
		dydt[NN].resize( M );
		for( size_t j=0 ; j<M ; j++ ) {
			y[NN][j] = y[i][j];
			dydt[NN][j] = dydt[i][j];
		}
		//volume and length of daughter-cells
		double va = 0.5*volume*( 1.0 + parameter(2)*
					 (2.0*myRandom::Rnd()-1.0) );
		double vb = volume - va;
		double da = getLength(va, parameter(0));
		double db = getLength(vb, parameter(0));
		
		//Minimize forces
		//y[i][dCol] = da-parameter(0);
		//y[NN][dCol] = db-parameter(0);;
		//Mass "conserved"
		y[i][dCol] = da;
		y[NN][dCol] = db;
		assert( y[i][dCol]>0.0 && y[NN][dCol]>0.0 );
  
		//Add center point to first/second point for the new cells
		for( size_t d=0 ; d<T ; d++ ) {
			y[i][x2Col[d]] = y[i][x1Col[d]] + da*direction[d]+parameter(3)*(myRandom::Rnd()-0.5);
			y[NN][x1Col[d]] = y[NN][x2Col[d]]-db*direction[d]+parameter(3)*(myRandom::Rnd()-0.5);;
		}
		//Add link relationship between mother/daughter
		//assume this is done directly after divisions!!!
	}

	//takes length and radius, returns the volume
	template<int T> double CellDivision<T>::getVolume(double length, double radius) {
		double V=0;
		if ( T==2 ) 
			V = ( PI*radius + 2*length )*radius;
		else if ( T==3 )
			V = ( 4*radius/3 + length )*PI*radius*radius;
		else {
			std::cerr << "Only two our three dimensions are allowed";
			exit(EXIT_FAILURE);
		}
		return V;
	}
	
	//takes volume and radius, returns the length
	template<int T> double CellDivision<T>::getLength(double volume, double radius) {
		double l=0;
		if ( T==2 ) 
			l = volume - 0.5*PI*radius;
		else if ( T==3 )
			l = volume - 4*radius/3;
		else {
			std::cerr << "Only two our three dimensions are allowed";
			exit(EXIT_FAILURE);
		}
		return l;
	}
}

#endif //ELONGATEDBACTERIA_CELLDIVISION_H
