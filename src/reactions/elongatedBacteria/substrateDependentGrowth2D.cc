#include "substrateDependentGrowth2D.h"

const double PI=3.141592653589793238512808959406186204433;
elongatedBacteria::SubstrateDependentGrowth2D::
SubstrateDependentGrowth2D(std::vector<double> &paraValue, 
			    std::vector< std::vector<size_t> > &indValue )
{
	
	//Do some checks on the parameters and variable indeces
	//////////////////////////////////////////////////////////////////////
	if ( paraValue.size()!=3 ) {
		std::cerr << "elongatedBacteria::SubstrateDependentGrowth::SubstrateDependentGrowth() "
			  << "Uses three parameters k_max, K and b\n";
		exit(0);
	}
	if ( indValue.size() !=1 || indValue[0].size() !=1 ) {
		std::cerr << "elongatedBacteria::SubstrateDependentGrowthRestriced::SubstrateDependentGrowth() "
			  << "Needs one index, ligand concentration.\n";
		exit(0);
	}
	//Set the variable values
	//////////////////////////////////////////////////////////////////////
	setId("exponentialGrowth");
	setParameter(paraValue);  
	setVariableIndex(indValue);
  
	//Set the parameter identities
	//////////////////////////////////////////////////////////////////////
	std::vector<std::string> tmp( numParameter() );
	tmp.resize( numParameter() );
	tmp[0] = "k_growth";
	tmp[1] = "K";
	tmp[2] = "b";
	setParameterId( tmp );
}


void elongatedBacteria::SubstrateDependentGrowth2D::
derivs(Compartment &compartment,size_t varIndex,
       std::vector< std::vector<double> > &y,
       std::vector< std::vector<double> > &dydt )
{
	size_t i=compartment.index();
	size_t dCol = compartment.numTopologyVariable()-1+varIndex;
	double S = y[i][variableIndex(0,0)];
	dydt[i][dCol] += parameter(0)*S*( y[i][dCol]+ PI*parameter(2)*0.5)/( parameter(1) + S );
}
