#include "chemoTaxisSimple.h"
#include <cmath>

elongatedBacteria::ChemotaxisSimple::ChemotaxisSimple(std::vector<double> &paraValue, 
						      std::vector< std::vector<size_t> > 
						      &indValue ) 
{
	//Do some checks on the parameters and variable indeces
	//////////////////////////////////////////////////////////////////////
	if( paraValue.size()!=3 ) {
		std::cerr << "ChemotaxisSimple::ChemotaxisSimple() "
			  << "Uses three parameters; \n"
			  << "V_R, V_B and K\n";
		exit(0);
	}
	if( indValue.size() != 1 || indValue[0].size() != 3 ) {
		std::cerr << "ChemotaxisSimple::ChemotaxisSimple()"
			  << "three variable indices needed X, X_active, gradient\n";
		exit(0);
	}
	//Set the variable values
	//////////////////////////////////////////////////////////////////////
	setId("chemotaxisSimple");
	setParameter(paraValue);  
	setVariableIndex(indValue);
	
	//Set the parameter identities
	//////////////////////////////////////////////////////////////////////
	std::vector<std::string> tmp( numParameter() );
	tmp.resize( numParameter() );
	tmp[0] = "V_R";
	tmp[1] = "V_B";
	tmp[2] = "K";
	setParameterId( tmp );
}


void elongatedBacteria::ChemotaxisSimple::derivs(Compartment &compartment, size_t species,
						 std::vector< std::vector<double> > &y,
						 std::vector< std::vector<double> > &dydt)
{
	double V_R=parameter(0);
	double V_B=parameter(1);
	double K=parameter(2);
	double alpha = std::exp(-y[compartment.index()][variableIndex(0,2)]);
	y[compartment.index()][variableIndex(0,1)]= alpha*y[compartment.index()][variableIndex(0,0)];
	dydt[compartment.index()][variableIndex(0,0)] = 
		V_R - V_B*y[compartment.index()][variableIndex(0,1)]/(K+y[compartment.index()][variableIndex(0,1)]);
}

