#include "flagellarMotor.h"
#include "../../common/mySignal.h"
#include "../../common/myRandom.h"

static const double PI = 3.141592653589793238512808959406186204433;

elongatedBacteria::FlagellarMotor::FlagellarMotor(std::vector<double> &paraValue, 
						  std::vector< std::vector<size_t> > 
						  &indValue ) {
	std::cerr << 	 "FlagellarMotor::FlagellarMotor()\n";
	//Do some checks on the parameters and variable indeces
	//////////////////////////////////////////////////////////////////////
	if( paraValue.size()!=5 ) {
		std::cerr << "FlagellarMotor::FlagellarMotor() "
			  << "Uses five parameters v, N_hill, K_hill, k, random contribution \n";
		exit(0);
	}
	if ( indValue.size() !=1 && 
	     (indValue[0].size() != 1 ||indValue[0].size() != 4) ) { 
		std::cerr << "FlagellarMotor::FlagellarMotor() "
			  << "uses one index, cheY-P concentration or " 
			  << "four index cheY-p concentration, time in runMode, " 
			  << "time in tumbleMode and frequency counter\n";
		exit(0);
	}
	//Set the variable values
	//////////////////////////////////////////////////////////////////////
	setId("flagellarMotor");
	setParameter(paraValue);  
	setVariableIndex(indValue);
  
	//Set the parameter identities
	//////////////////////////////////////////////////////////////////////
	std::vector<std::string> tmp( numParameter() );
	tmp[0] = "v";
	tmp[1] = "N_hill";
	tmp[2] = "K_hill";
	tmp[3] = "k";
	setParameterId( tmp );
	
}

void elongatedBacteria::FlagellarMotor::derivs(Compartment &compartment,size_t varIndex,
					       std::vector< std::vector<double> > &y,
					       std::vector< std::vector<double> > &dydt )
{	
	//Initiates tumbleFlag and runningTime. Kind of ugly but the only way I could
	//think of at the time!!!
	static bool first = true;
	if (first) {
		size_t size = y.size();
		tumbleFlag_.resize(size);
		runningTime_.resize(size);
		tumbleForce_.resize(size);
		rotDir_.resize(size);
		noise_.resize(size);
		for (size_t i = 0; i<size; ++i) {
			tumbleFlag_[i]=false;
			runningTime_[i]=0.0;	
			//the factor 1.40331 is here to ensure that on average the bacteria will
			//rotate pi/2 radians in 0.36 s
			tumbleForce_[i] = 1.40331*PI*myRandom::Rnd();
			noise_[i] = parameter(4)*myRandom::Grand();
			rotDir_[i] = myRandom::Rnd() < 0.5? 1:-1;
		}
		first=false;
	}
	size_t i=compartment.index();
	size_t dimension=(compartment.numTopologyVariable()-1)/2;    
	if (dimension != 2) { 
		std::cerr << "FlagellarMotor::derivs(): dimension must be 2\n";
		exit(-1);
	}
	std::vector<double> n(dimension),com(dimension),N(dimension);
	for( size_t dim=0 ; dim<dimension ; ++dim ) {
		n[dim] = y[i][varIndex+dim+dimension]-y[i][varIndex+dim];
		com[dim] = 0.5*(y[i][varIndex+dim+dimension]+y[i][varIndex+dim]);
	}	
	N[0] = -n[1];
	N[1] = n[0];
	double a = 0.0;
	for( size_t dim=0 ; dim<dimension ; ++dim )   
		a += n[dim]*n[dim];
	a = std::sqrt(a);
	for( size_t dim=0 ; dim<dimension ; ++dim )   
		n[dim] /=a;
	
	if (!tumbleFlag_[i]) {
		for( size_t dim=0 ; dim<dimension ; ++dim ) {
			double derivative = parameter(0)*n[dim];
			dydt[i][varIndex + dim] += derivative-noise_[i]*N[dim];
			dydt[i][varIndex + dim + dimension] += derivative+noise_[i]*N[dim];
		}
	}
	else {
		for( size_t dim=0 ; dim<dimension ; ++dim ) {
			double F = tumbleForce_[i]*N[dim]*rotDir_[i];
			dydt[i][varIndex+dim] -= F;
			dydt[i][varIndex+dimension+dim] += F;
		}
	}
}


void elongatedBacteria::FlagellarMotor::update(double h, double t,
					       std::vector< std::vector<double> > &y)
{
	for (size_t i=0; i< y.size(); ++i) {
		runningTime_[i] += h;
		if (numVariableIndex(0)==4) {
			if (tumbleFlag_[i])
				y[i][variableIndex(0,2)]+=h;
			else
				y[i][variableIndex(0,1)]+=h;
		}
		
		double powY = std::pow(y[i][variableIndex(0,0)],parameter(1)); 
		double powK = std::pow(parameter(2),parameter(1));
		double runTime = (powK+powY)/(parameter(3)*powY);
		double tumbleTime = (powK+powY)/(3.30243*powK);
		//check if we are to enter tumble mode
		if (!tumbleFlag_[i] && runningTime_[i]>runTime) {
			tumbleFlag_[i]=true;
			runningTime_[i]=0.0;
			//the factor 1.40331 is here to ensure that on average the bacteria will
			//rotate pi/2 radians in 0.36 s
			tumbleForce_[i] = 1.40331*PI*myRandom::Rnd();
			if (numVariableIndex(0)==4) 
				y[i][variableIndex(0,3)]++; 
					
		}
		//check wheather to leave tumble mode
		if (tumbleFlag_[i] && runningTime_[i]>tumbleTime) {
			tumbleFlag_[i]=false;
			runningTime_[i]=0.0;
			noise_[i] = parameter(4)*myRandom::Grand();
			rotDir_[i] = myRandom::Rnd() < 0.5? 1:-1;
			if (numVariableIndex(0)==4) 
				y[i][variableIndex(0,3)]++; 
		}
	}
}


