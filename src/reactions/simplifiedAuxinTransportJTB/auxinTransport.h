#ifndef SIMPLIFIEDAUXINTRANSPORTJTB_AUXINTRANSPORT_H
#define SIMPLIFIEDAUXINTRANSPORTJTB_AUXINTRANSPORT_H

#include "../baseReaction.h"
#include <cmath>

///
/// @brief Auxin transport reactions developed for the analysis in Sahlin et al J Theor Biol (2009)
///
/// These functions have some overlap with the reactions described in the namespace
/// PolarizationTransportFast
///
/// @see PolarizationTransportFast
///
namespace SimplifiedAuxinTransportJTB
{
  ///
  /// @brief PIN polarization (Hill, qusiequilibrium) and auxin tranport (passive and MM active) reaction 
  ///
  /// This reaction was developed for the simulations in Sahlin et al J Theor Biol (2009) (Equations 8, 25 and 26) 
  /// and polarizes PIN with the assumption of fast dynamics (quasiequilibrium) according to a Hill function 
  /// from the auxin concentrations in the neighboring cells. Then auxin transport is applied between cells
  /// with a passive (diffusive) term and a PIN dependent term of active transport using a Michaelis Menten
  /// formalism. The equations are:
  ///
  /// @f[ P_{ij} = \frac{f(A_j)}{1+\sum_{k}f(A_k)}@f]
  /// @f[ f(A_j) = k_1 + k_2 \frac{A_j^n}{K_H^n+A_j^n}
  ///
  /// and
  ///
  /// @f[ \frac{dA_i}{dt} = 2 c_a - 2 d_a A_i - D \sum_k (A_i-A_k) - \sum_k ( P_k \frac{A_k}{K_M+A_k} - P_i \frac{A_i}{K_M+A_i})@f]
  ///
  /// where @f$P_{ij}@f$ is the PIN in the membrane in cell i pointing towards cell j 
  /// (from equilibrium calculation), @f$A_{i}@f$ is the auxin concentration in cell i.
  /// @f$f(A_j)@f$ is the constant plus Hill feedback with the parameters @f$k_1,k_2,K_H,n@f$ describing the
  /// constant feedback, maximal from Hill feedback, Hill constant, and Hill coefficient, respectively.
  /// @f$c_A,d_A@f$ are the auxin production and degradation rates, @f$D@f$ is the passive transport rate,
  /// and @f$P_i@f$ is the total amount of PIN in cell i determining the active rate of transport.
  ///
  /// In a model file the reaction is described as:
  /// @verbatim
  /// SimplifiedAuxinTRansportJTB::auxinTransport 8 1 2
  /// D k_1 k_2 K_H n K_M c_a d_a
  /// auxinIndex PINIndex
  /// @endverbatim
  ///
  /// Note that the first variableindex sets the feedback signal that can but do not need to be auxin itself.
  ///
  class AuxinTransport : public BaseReaction
  {
  public:
    AuxinTransport(std::vector<double> &paraValue, 
		   std::vector< std::vector<size_t> > &indValue );
    
    ///
    /// @brief Derivative function for this reaction class
    ///
    /// @see BaseReaction::derivs(Compartment &compartment,size_t species,...)
    ///
    void derivs(Compartment &compartment,size_t species,
		std::vector< std::vector<double> > &y,
		std::vector< std::vector<double> > &dydt );
    
    /// @brief helper function to calculate the feedback (via the Hill) 
    double PIN1Feedback(double auxin);
    /// @brief helper function to calculate the active transport rate (via MM)
    double AuxinActiveTransport(double auxin);
  };

  class AuxinTransportExperimentalOne : public BaseReaction
  {
  public:
    AuxinTransportExperimentalOne(std::vector<double> &paraValue, 
				  std::vector< std::vector<size_t> > &indValue );
    
    void derivs(Compartment &compartment,size_t species,
		std::vector< std::vector<double> > &y,
		std::vector< std::vector<double> > &dydt );
    
    double PIN1Feedback(double auxin);
    double AuxinActiveTransport(double auxin);
  };
  
  class AuxinTransportExperimentalTwo : public BaseReaction
  {
  public:
    AuxinTransportExperimentalTwo(std::vector<double> &paraValue, 
				  std::vector< std::vector<size_t> > &indValue );
    
    void derivs(Compartment &compartment,size_t species,
		std::vector< std::vector<double> > &y,
		std::vector< std::vector<double> > &dydt );
    
    double PIN1Feedback(double auxin);
    double AuxinActiveTransport(double auxin);
  };	
  
  class AuxinTransportPosterA : public BaseReaction
  {
  public:
    AuxinTransportPosterA(std::vector<double> &paraValue, 
			  std::vector< std::vector<size_t> > &indValue );
    
    void derivs(Compartment &compartment,size_t species,
		std::vector< std::vector<double> > &y,
		std::vector< std::vector<double> > &dydt );
    
    double PIN1Feedback(double auxin);
    double AuxinActiveTransport(double auxin);
  };	
  
  class AuxinTransportPosterB : public BaseReaction
  {
  public:
    AuxinTransportPosterB(std::vector<double> &paraValue, 
			  std::vector< std::vector<size_t> > &indValue );
    
    void derivs(Compartment &compartment,size_t species,
		std::vector< std::vector<double> > &y,
		std::vector< std::vector<double> > &dydt );
    
    double PIN1Feedback(double auxin);
    double AuxinActiveTransport(double auxin);
  };
  
  class AuxinTransportPosterC : public BaseReaction
  {
  public:
    AuxinTransportPosterC(std::vector<double> &paraValue, 
			  std::vector< std::vector<size_t> > &indValue );
    
    void derivs(Compartment &compartment,size_t species,
		std::vector< std::vector<double> > &y,
		std::vector< std::vector<double> > &dydt );
    
    double PIN1Feedback(double auxin);
    double AuxinActiveTransport(double auxin);
  };
  
  class AuxinTransportNoCompetition : public BaseReaction
  {
  private:
    double KPow_;
  public:
    AuxinTransportNoCompetition(std::vector<double> &paraValue, 
				std::vector< std::vector<size_t> > &indValue );
    
    void derivs(Compartment &compartment,size_t species,
		std::vector< std::vector<double> > &y,
		std::vector< std::vector<double> > &dydt );
    
    double PIN1Feedback(double auxin);
    double AuxinActiveTransport(double auxin);
  };
  
  class CWII : public BaseReaction
  {
  public:
    CWII(std::vector<double> &paraValue,
	 std::vector< std::vector<size_t> > &indValue);
    
    void derivs(Compartment &compartment, size_t species,
		std::vector< std::vector<double> > &y,
		std::vector< std::vector<double> > &dydt);
  private:
    double PIN1Feedback(double auxin);
    double AuxinActiveTransport(double auxin);
  };
}

#endif /* SIMPLIFIEDAUXINTRANSPORTJTB_AUXINTRANSPORT_H */
