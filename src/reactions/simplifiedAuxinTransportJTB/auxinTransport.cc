#include "auxinTransport.h"

namespace SimplifiedAuxinTransportJTB
{
  AuxinTransport::AuxinTransport(std::vector<double> &paraValue, 
				 std::vector< std::vector<size_t> > &indValue)
  {
    if (paraValue.size() != 8)
      {
	std::cerr << "SimplifiedAuxinTransport::AuxinTransport::AuxinTransport "
		  << "Uses eight parameters: \n"
		  << "D, "             // 0
		  << "k1, "            // 1
		  << "k2, "            // 2
		  << "K_H, "           // 3
		  << "n, "             // 4
		  << "K_M, "           // 5
		  << "c_a, "           // 6
		  << "and d_a.\n";     // 7
	exit(EXIT_FAILURE);
      }
    
    if (indValue.size() != 1 || indValue[0].size() != 2)
      {
	std::cerr << "SimplifiedAuxinTransport::AuxinTransport::AuxinTransport "
		  << "Two variable indices are needed: \n"
		  << "Auxin and "      // 0
		  << "PIN1.\n";        // 1
	exit(EXIT_FAILURE);
      }
    
    setId("auxinTransport");
    setParameter(paraValue);  
    setVariableIndex(indValue);
    
    std::vector<std::string> tmp(numParameter());
    tmp.resize(numParameter());
    
    tmp[0] = "D";
    tmp[1] = "k1";
    tmp[2] = "k2";
    tmp[3] = "K_H";
    tmp[4] = "n";
    tmp[5] = "K_M";
    tmp[6] = "c_a";
    tmp[7] = "d_a";
    
    setParameterId(tmp);
  }
  
  void AuxinTransport::derivs(Compartment &compartment, size_t species,
			      std::vector< std::vector<double> > &y,
			      std::vector< std::vector<double> > &dydt)
  {
    size_t i = compartment.index();
    
    double sum = 1.0;
    
    for (size_t n = 0; n < compartment.numNeighbor(); ++n)
      {
	size_t k = compartment.neighbor(n);
	sum += PIN1Feedback(y[k][variableIndex(0, 0)]);
      }
    
    const double D = parameter(0);
    
    for (size_t n = 0; n < compartment.numNeighbor(); ++n)
      {
	size_t j = compartment.neighbor(n);
	
	double passive = D * y[i][variableIndex(0, 0)];
	
	double Pij = (PIN1Feedback(y[j][variableIndex(0, 0)]) * y[i][variableIndex(0, 1)]) / sum;		
	
	double active = AuxinActiveTransport(y[i][variableIndex(0, 0)]) * Pij; 
	
	dydt[i][species] -= (passive + active);
	dydt[j][species] += (passive + active);
      }
    
    const double C_a = parameter(6);
    const double D_a = parameter(7);
    
    dydt[i][species] += 2.0 * C_a;
    dydt[i][species] -= 2.0 * D_a * y[i][variableIndex(0, 0)];
  }
  
  double AuxinTransport::PIN1Feedback(double auxin)
  {
    const double k1 = parameter(1);
    const double k2 = parameter(2);
    const double K_H = parameter(3);
    const double n = parameter(4);
    
    return k1 + (k2 * std::pow(auxin, n) / (std::pow(K_H, n) + std::pow(auxin, n)));
  }
  
  double AuxinTransport::AuxinActiveTransport(double auxin)
  {
    const double K_M = parameter(5);
    return auxin / (K_M + auxin);
  }	

	AuxinTransportExperimentalOne::AuxinTransportExperimentalOne(std::vector<double> &paraValue, 
								     std::vector< std::vector<size_t> > &indValue )
	{
		if (paraValue.size() != 2) {
			std::cerr << "SimplifiedAuxinTransport::AuxinTransport::AuxinTransport "
				  << "Uses three parameters: " << std::endl
				  << "D/T, "                // 0
				  << "n"                  // 1
				  << std::endl;
			exit(EXIT_FAILURE);
		}
		
		if (indValue.size() != 1 || indValue[0].size() != 2) {
			std::cerr << "SimplifiedAuxinTransport::AuxinTransport::AuxinTransport "
				  << "Two variable indices are needed: " << std::endl
				  << "Auxin and "   // 0
				  << "PIN1."        // 1
				  << std::endl;
			exit(EXIT_FAILURE);
		}
		
		setId("auxinFlux");
		setParameter(paraValue);  
		setVariableIndex(indValue);
		
		std::vector<std::string> tmp(numParameter());
		tmp.resize(numParameter());
		
		tmp[0] = "D/T";
		tmp[1] = "n";
		
		setParameterId( tmp );
	}
	
	void AuxinTransportExperimentalOne::derivs(Compartment &compartment, size_t species,
						   std::vector< std::vector<double> > &y,
						   std::vector< std::vector<double> > &dydt)
	{
		size_t i = compartment.index();
		
		double sum = 1.0;
		for (size_t n = 0; n < compartment.numNeighbor(); ++n) {
			size_t k = compartment.neighbor(n);
			sum += PIN1Feedback(y[k][variableIndex(0,0)]);
		}
		
		for (size_t n = 0; n < compartment.numNeighbor(); ++n) {
			size_t j = compartment.neighbor(n);
			
			double passive = parameter(0) * y[i][variableIndex(0,0)];
			
			double Pij = PIN1Feedback(y[j][variableIndex(0,0)]) * 
				y[i][variableIndex(0,1)] / sum;		
			
			double active = AuxinActiveTransport(y[i][variableIndex(0,0)]) * Pij; 
			
			dydt[i][species] -= (passive + active);
			dydt[j][species] += (passive + active);
		}
	}
	
	double AuxinTransportExperimentalOne::PIN1Feedback(double auxin)
	{
		double tmp = pow(auxin, parameter(1));
		return tmp;
	}
	
	double AuxinTransportExperimentalOne::AuxinActiveTransport(double auxin)
	{
		return auxin;
	}




	AuxinTransportExperimentalTwo::AuxinTransportExperimentalTwo(std::vector<double> &paraValue, 
								     std::vector< std::vector<size_t> > &indValue )
	{
		if (paraValue.size() != 2) {
			std::cerr << "SimplifiedAuxinTransport::AuxinTransportExperimentalTwo::AuxinTransportExperimentalTwo "
				  << "Uses four parameters: " << std::endl
				  << "D/T, "                // 0
				  << "1/m"                  // 1
				  << std::endl;
			exit(EXIT_FAILURE);
		}
		
		if (indValue.size() != 1 || indValue[0].size() != 2) {
			std::cerr << "SimplifiedAuxinTransport::AuxinTransport::AuxinTransport "
				  << "Two variable indices are needed: " << std::endl
				  << "Auxin and "   // 0
				  << "PIN1."        // 1
				  << std::endl;
			exit(EXIT_FAILURE);
		}
		
		setId("auxinFlux");
		setParameter(paraValue);  
		setVariableIndex(indValue);
		
		std::vector<std::string> tmp(numParameter());
		tmp.resize(numParameter());
		
		tmp[0] = "D/T";
		tmp[1] = "1 / m";
		
		setParameterId( tmp );
	}

	void AuxinTransportExperimentalTwo::derivs(Compartment &compartment, size_t species,
						   std::vector< std::vector<double> > &y,
						   std::vector< std::vector<double> > &dydt)
	{
		size_t i = compartment.index();
		
		double sum = 1.0;
		for (size_t n = 0; n < compartment.numNeighbor(); ++n) {
			size_t k = compartment.neighbor(n);
			sum += PIN1Feedback(y[k][variableIndex(0,0)]);
		}
		
		for (size_t n = 0; n < compartment.numNeighbor(); ++n) {
			size_t j = compartment.neighbor(n);
			
			double passive = parameter(0) * y[i][variableIndex(0,0)];
			
			double Pij = PIN1Feedback(y[j][variableIndex(0,0)]) * 
				y[i][variableIndex(0,1)] / sum;		
			
			double active = AuxinActiveTransport(y[i][variableIndex(0,0)]) * Pij; 
			
			dydt[i][species] -= (passive + active);
			dydt[j][species] += (passive + active);
		}
	}
	
	double AuxinTransportExperimentalTwo::PIN1Feedback(double auxin)
	{
		double tmp = auxin;
		return tmp;
	}
	
	double AuxinTransportExperimentalTwo::AuxinActiveTransport(double auxin)
	{
		return pow(auxin, 1.0 / parameter(1));
	}






	AuxinTransportPosterA::AuxinTransportPosterA(std::vector<double> &paraValue, 
						     std::vector< std::vector<size_t> > &indValue )
	{
		if (paraValue.size() != 3) {
			std::cerr << "AuxinTransportPosterA::AuxinTransportPosterA "
				  << "Uses three parameters: " << std::endl
				  << "d, "                // 0
				  << "T, "                // 1
				  << "k_{p1}/k_{p2} and " // 2
				  << std::endl;
			exit(EXIT_FAILURE);
		}
		
		if (indValue.size() != 1 || indValue[0].size() != 2) {
			std::cerr << "AuxinTransportPosterA::AuxinTransportPosterA "
				  << "Two variable indices are needed: " << std::endl
				  << "Auxin and "   // 0
				  << "PIN1."        // 1
				  << std::endl;
			exit(EXIT_FAILURE);
		}
		
		setId("auxinFlux");
		setParameter(paraValue);  
		setVariableIndex(indValue);
		
		std::vector<std::string> tmp(numParameter());
		tmp.resize(numParameter());
		
		tmp[0] = "d";
		tmp[1] = "T";
		tmp[2] = "k_{p1}/k_{p2}";
		
		setParameterId( tmp );
	}

	void AuxinTransportPosterA::derivs(Compartment &compartment, size_t species,
						   std::vector< std::vector<double> > &y,
						   std::vector< std::vector<double> > &dydt)
	{
		size_t i = compartment.index();
		
		double sum = 1.0;
		for (size_t n = 0; n < compartment.numNeighbor(); ++n) {
			size_t k = compartment.neighbor(n);
			sum += PIN1Feedback(y[k][variableIndex(0,0)]);
		}
		
		for (size_t n = 0; n < compartment.numNeighbor(); ++n) {
			size_t j = compartment.neighbor(n);
			
			double passive = parameter(0) * y[i][variableIndex(0,0)];
			
			double Pij = PIN1Feedback(y[j][variableIndex(0,0)]) * 
				y[i][variableIndex(0,1)] / sum;		
			
			double active = AuxinActiveTransport(y[i][variableIndex(0,0)]) * Pij; 
			
			dydt[i][species] -= (passive + active);
			dydt[j][species] += (passive + active);
		}
	}
	
	double AuxinTransportPosterA::PIN1Feedback(double auxin)
	{
		double tmp = parameter(2) * auxin;
		return tmp;
	}
	
	double AuxinTransportPosterA::AuxinActiveTransport(double auxin)
	{
		return parameter(1) * auxin;
	}







	AuxinTransportPosterB::AuxinTransportPosterB(std::vector<double> &paraValue, 
						     std::vector< std::vector<size_t> > &indValue )
	{
		if (paraValue.size() != 4) {
			std::cerr << "AuxinTransportPosterB::AuxinTransportPosterB "
				  << "Uses four parameters: " << std::endl
				  << "d, "                // 0
				  << "T, "                // 1
				  << "k_{p1}/k_{p2} and " // 2
				  << "m" // 3
				  << std::endl;
			exit(EXIT_FAILURE);
		}
		
		if (indValue.size() != 1 || indValue[0].size() != 2) {
			std::cerr << "AuxinTransportPosterB::AuxinTransportPosterB "
				  << "Two variable indices are needed: " << std::endl
				  << "Auxin and "   // 0
				  << "PIN1."        // 1
				  << std::endl;
			exit(EXIT_FAILURE);
		}
		
		setId("auxinFlux");
		setParameter(paraValue);  
		setVariableIndex(indValue);
		
		std::vector<std::string> tmp(numParameter());
		tmp.resize(numParameter());
		
		tmp[0] = "d";
		tmp[1] = "T";
		tmp[2] = "k_{p1}/k_{p2}";
		tmp[3] = "m";
		
		setParameterId( tmp );
	}

	void AuxinTransportPosterB::derivs(Compartment &compartment, size_t species,
						   std::vector< std::vector<double> > &y,
						   std::vector< std::vector<double> > &dydt)
	{
		size_t i = compartment.index();
		
		double sum = 1.0;
		for (size_t n = 0; n < compartment.numNeighbor(); ++n) {
			size_t k = compartment.neighbor(n);
			sum += PIN1Feedback(y[k][variableIndex(0,0)]);
		}
		
		for (size_t n = 0; n < compartment.numNeighbor(); ++n) {
			size_t j = compartment.neighbor(n);
			
			double passive = parameter(0) * y[i][variableIndex(0,0)];
			
			double Pij = PIN1Feedback(y[j][variableIndex(0,0)]) * 
				y[i][variableIndex(0,1)] / sum;		
			
			double active = AuxinActiveTransport(y[i][variableIndex(0,0)]) * Pij; 
			
			dydt[i][species] -= (passive + active);
			dydt[j][species] += (passive + active);
		}
	}
	
	double AuxinTransportPosterB::PIN1Feedback(double auxin)
	{
		return parameter(2) * auxin;
	}
	
	double AuxinTransportPosterB::AuxinActiveTransport(double auxin)
	{
		return parameter(1) * pow(auxin, parameter(3)) / (1.0 + pow(auxin, parameter(3)));
	}






	AuxinTransportPosterC::AuxinTransportPosterC(std::vector<double> &paraValue, 
						     std::vector< std::vector<size_t> > &indValue )
	{
		if (paraValue.size() != 5) {
			std::cerr << "AuxinTransportPosterC::AuxinTransportPosterC "
				  << "Uses four parameters: " << std::endl
				  << "d, "                // 0
				  << "T, "                // 1
				  << "k_{p1}/k_{p2} and " // 2
				  << "n" // 3
				  << "m" // 4
				  << std::endl;
			exit(EXIT_FAILURE);
		}
		
		if (indValue.size() != 1 || indValue[0].size() != 2) {
			std::cerr << "AuxinTransportPosterC::AuxinTransportPosterC "
				  << "Two variable indices are needed: " << std::endl
				  << "Auxin and "   // 0
				  << "PIN1."        // 1
				  << std::endl;
			exit(EXIT_FAILURE);
		}
		
		setId("auxinFlux");
		setParameter(paraValue);  
		setVariableIndex(indValue);
		
		std::vector<std::string> tmp(numParameter());
		tmp.resize(numParameter());
		
		tmp[0] = "d";
		tmp[1] = "T";
		tmp[2] = "k_{p1}/k_{p2}";
		tmp[3] = "m";
		
		setParameterId( tmp );
	}

	void AuxinTransportPosterC::derivs(Compartment &compartment, size_t species,
					   std::vector< std::vector<double> > &y,
					   std::vector< std::vector<double> > &dydt)
	{
		size_t i = compartment.index();
		
		double sum = 1.0;
		for (size_t n = 0; n < compartment.numNeighbor(); ++n) {
			size_t k = compartment.neighbor(n);
			sum += PIN1Feedback(y[k][variableIndex(0,0)]);
		}
		
		for (size_t n = 0; n < compartment.numNeighbor(); ++n) {
			size_t j = compartment.neighbor(n);
			
			double passive = parameter(0) * y[i][variableIndex(0,0)];
			
			double Pij = PIN1Feedback(y[j][variableIndex(0,0)]) * 
				y[i][variableIndex(0,1)] / sum;		
			
			double active = AuxinActiveTransport(y[i][variableIndex(0,0)]) * Pij; 
			
			dydt[i][species] -= (passive + active);
			dydt[j][species] += (passive + active);
		}
	}
	
	double AuxinTransportPosterC::PIN1Feedback(double auxin)
	{
		return parameter(2) * pow(auxin, parameter(3)) / (1.0 + pow(auxin, parameter(3)));

	}
	
  double AuxinTransportPosterC::AuxinActiveTransport(double auxin)
  {
    return parameter(1) * pow(auxin, parameter(4)) / (1.0 + pow(auxin, parameter(4)));
  }
  
  AuxinTransportNoCompetition::
  AuxinTransportNoCompetition(std::vector<double> &paraValue, 
			      std::vector< std::vector<size_t> > &indValue )
  {
    if (paraValue.size() != 8) {
      std::cerr << "SimplifiedAuxinTransportNoCompetition::"
		<< "AuxinTransportNoCompetition::AuxinTransportNoCompetition "
		<< "Uses eight parameters: " << std::endl
		<< "D/T, "           // 0
		<< "k1, "            // 1
		<< "k2, "            // 2
		<< "K_H, "           // 3
		<< "n, "             // 4
		<< "K_M, "           // 5
		<< "c_a, "           // 6
		<< "and d_a."        // 7
		<< std::endl;
      exit(EXIT_FAILURE);
    }
    
    if (indValue.size() != 1 || indValue[0].size() != 2) {
      std::cerr << "SimplifiedAuxinTransportNoCompetition::"
		<< "AuxinTransportNoCompetition::AuxinTransportNoCompetition "
		<< "Two variable indices are needed: " << std::endl
		<< "Auxin and "   // 0
		<< "PIN1."        // 1
		<< std::endl;
      exit(EXIT_FAILURE);
    }
    
    setId("auxinTransportNoCompetition");
    setParameter(paraValue);  
    setVariableIndex(indValue);
    
    std::vector<std::string> tmp(numParameter());
    tmp.resize(numParameter());
    
    tmp[0] = "D/T";
    tmp[1] = "k1";
    tmp[2] = "k2";
    tmp[3] = "K_H";
    tmp[4] = "n";
    tmp[5] = "K_M";
    tmp[6] = "c_a";
    tmp[7] = "d_a";
    
    setParameterId( tmp );
    KPow_ = std::pow(parameter(3),parameter(4));
  }
  
  void AuxinTransportNoCompetition::
  derivs(Compartment &compartment, size_t species,
	 std::vector< std::vector<double> > &y,
	 std::vector< std::vector<double> > &dydt)
  {
    size_t i = compartment.index();
    
    for (size_t n = 0; n < compartment.numNeighbor(); ++n) {
      size_t j = compartment.neighbor(n);
      
      double passive = parameter(0) * y[i][variableIndex(0, 0)];
      
      double Pij = PIN1Feedback(y[j][variableIndex(0, 0)]) * 
	y[i][variableIndex(0, 1)];		
      
      double active = AuxinActiveTransport(y[i][variableIndex(0, 0)]) * Pij; 
      
      dydt[i][species] -= (passive + active);
      dydt[j][species] += (passive + active);
    }
    
    dydt[i][species] += 2.0*(parameter(6) - parameter(7) * y[i][variableIndex(0, 0)]);
  }
  
  double AuxinTransportNoCompetition::PIN1Feedback(double auxin)
  {
    double tmp;
    tmp = parameter(1);
    double aPow = pow(auxin, parameter(4));
    tmp += parameter(2) * aPow /
      (KPow_ + aPow);
    return tmp;
  }
  
  double AuxinTransportNoCompetition::AuxinActiveTransport(double auxin)
  {
    return auxin / (parameter(5) + auxin);
  }
  
  CWII::CWII(std::vector<double> &paraValue, std::vector< std::vector<size_t> > &indValue)
  {
    if (paraValue.size() != 6) {
      std::cerr << "SimplifiedAuxinTransport::AuxinTransport::AuxinTransport "
		<< "Uses eight parameters: " << std::endl
		<< "d1, "            // 0
		<< "d2, "            // 1
		<< "k1, "            // 2
		<< "k2, "            // 3
		<< "K_H and "        // 4
		<< "n."              // 5
		<< "\n";
      exit(EXIT_FAILURE);
    }
    
    if (indValue.size() != 1 || indValue[0].size() != 3) {
      std::cerr << "SimplifiedAuxinTransport::AuxinTransport::AuxinTransport "
		<< "Two variable indices are needed: " << std::endl
		<< "Auxin, "    // 0
		<< "PIN1 and "  // 1
		<< "wall flag." // 2
		<< "\n";
      exit(EXIT_FAILURE);
    }
    
    setId("CWII");
    setParameter(paraValue);  
    setVariableIndex(indValue);
    
    std::vector<std::string> tmp(numParameter());
    tmp.resize(numParameter());
    
    tmp[0] = "d1";
    tmp[1] = "d2";
    tmp[2] = "k1";
    tmp[3] = "k2";
    tmp[4] = "K_H";
    tmp[5] = "n";
    
    setParameterId( tmp );
  }
  
  void CWII::derivs(Compartment &compartment, size_t species, std::vector< std::vector<double> > &y, std::vector< std::vector<double> > &dydt)
  {
    size_t i = compartment.index();
    
    if (y[i][variableIndex(0, 2)] == 0) {
      double sum = 1.0;
      for (size_t n = 0; n < compartment.numNeighbor(); ++n) {
	size_t k = compartment.neighbor(n);
	sum += PIN1Feedback(y[k][variableIndex(0, 0)]);
      }
      
      for (size_t n = 0; n < compartment.numNeighbor(); ++n) {
	size_t j = compartment.neighbor(n);
	
	double passive = parameter(0) * y[i][variableIndex(0, 0)];
	
	double Pij = PIN1Feedback(y[j][variableIndex(0, 0)]) * 
	  y[i][variableIndex(0, 1)] / sum;		
	
	double active = AuxinActiveTransport(y[i][variableIndex(0, 0)]) * Pij; 
	
	dydt[i][species] -= (passive + active);
	dydt[j][species] += (passive + active);
      }
    } else {
      for (size_t n = 0; n < compartment.numNeighbor(); ++n) {
	size_t j = compartment.neighbor(n);
	
	double passive = parameter(1) * y[i][variableIndex(0, 0)];
	
	dydt[i][species] -= passive;
	dydt[j][species] += passive;
      }
    }
  }
  
  double CWII::PIN1Feedback(double auxin)
  {
    double tmp;
    tmp = parameter(2);
    tmp += parameter(3) * pow(auxin, parameter(5)) /
      (parameter(4) + pow(auxin, parameter(5)));
    return tmp;
  }
  
  double CWII::AuxinActiveTransport(double auxin)
  {
    return auxin;
  }
}















