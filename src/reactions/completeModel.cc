//
// Filename     : completeModel.cc
// Description  : Classes describing complete models
// Author(s)    : Pontus Melke (pontus@thep.lu.se)
// Created      : August 2006
// Revision     : $Id: completeModel.cc 613 2015-03-06 17:40:04Z henrik $
//

#include"completeModel.h"
#include"baseReaction.h"


AuxinFlux::AuxinFlux(std::vector<double> &paraValue, 
				 std::vector< std::vector<size_t> > 
				 &indValue ) 
{
	//Do some checks on the parameters and variable indeces
	//////////////////////////////////////////////////////////////////////
	if( paraValue.size()!=14 ) {
		std::cerr << "AuxinFlux::AuxinFlux() "
				<< "Uses fourteen parameters; \n"
				<< "p_AH, p_A-, K_A, p_AUX, K_AUX, V, T, pH and pK_d W w_A w_AUX k_1 k_2\n\n";
		exit(0);
	}
	if( indValue.size() != 1 || indValue[0].size() != 6 ) {
		std::cerr << "AuxinFlux::AuxinFlux()"
			  << "Six variable indices needed A_Label, A_i, A_oH, A_o-, dummy and A_c.\n";
		exit(0);
	}
	//Set the variable values
	//////////////////////////////////////////////////////////////////////
	setId("auxinFlux");
	setParameter(paraValue);  
	setVariableIndex(indValue);
	
	//Set the parameter identities
	//////////////////////////////////////////////////////////////////////
	std::vector<std::string> tmp( numParameter() );
	tmp.resize( numParameter() );
	tmp[0] = "p_AH";
	tmp[1] = "p_A";
	tmp[2] = "K_A";
	tmp[3] = "p_AUX";
	tmp[4] = "K_AUX";
	tmp[5] = "V";
	tmp[6] = "T";
	tmp[7] = "pH";
	tmp[8] = "pK";
	tmp[9] = "W";
	tmp[10] = "w_A";
	tmp[11] = "w_AUX";
	tmp[12] = "k_1";
	tmp[13] = "k_2";
	setParameterId( tmp );
}

//! Derivative contribution for a complete auxin flux model
/*! Deriving the time derivative contribution from an auxin flux model using
  the values in y and add the results to dydt. Compartment is the cell and
  varIndex is not used. Instead reactants and products are all stored in
  variableIndexLevel 0.Only the two first indices are updated according to:
  dydt = p_AH*(A_oH-fAH*A_i)+
  p_A*(NInf*A_o/(K_A+A_o)-NEff*fA*A_i/(K_A+fA*A_i)+
  p_AUX*(NEff*A_o/(K_AUX+A_o)-NInf*fA*A_i/(K_AUX+fA*A_i) 
  yl = dummy*y
*/
void AuxinFlux::derivs(Compartment &compartment,size_t species,
				   std::vector< std::vector<double> > &y,
				   std::vector< std::vector<double> > &dydt ) 
{
	double A_i = y[compartment.index()][variableIndex(0,1)];
	double A_oH = y[compartment.index()][variableIndex(0,2)];
	double A_o = y[compartment.index()][variableIndex(0,3)];
	//Note F/R = 11605 C*K/J
	double phi = parameter(5)*11605/parameter(6);
	double expPhi = std::exp(phi);
	double NEff = phi/(expPhi-1);
	double NInf = expPhi*NEff;
	double fAH = 1/(1+ std::pow(10,parameter(7)-parameter(8)));
	double fA = 1-fAH;
	
	
	//Derivative contributions
	dydt[compartment.index()][variableIndex(0,1)] +=
		parameter(9) * (
					 parameter(0)*(A_oH - fAH*A_i) +
					 parameter(10) * parameter(1)*( NInf*A_o/(parameter(2)+ A_o)-
											  NEff*fA*A_i/(parameter(2)+ fA*A_i) ) +
					 parameter(11) * parameter(3)*( NEff*A_o/(parameter(4) +A_o) -
											  NInf*fA*A_i/(parameter(4)+ fA*A_i) )
					 );
	
	dydt[compartment.index()][variableIndex(0,1)] +=
		-parameter(12) * y[compartment.index()][variableIndex(0,1)]
		+parameter(13) * y[compartment.index()][variableIndex(0,5)];
	dydt[compartment.index()][variableIndex(0,5)] +=
		+parameter(12) * y[compartment.index()][variableIndex(0,1)]
		-parameter(13) * y[compartment.index()][variableIndex(0,5)];
	
	//Label species
	y[compartment.index()][variableIndex(0,0)] =  
		y[compartment.index()][variableIndex(0,1)]*
		y[compartment.index()][variableIndex(0,4)];
}

CellCellAuxinTransport::CellCellAuxinTransport(std::vector<double> &paraValue, 
					       std::vector< std::vector<size_t> > 
					       &indValue ) 
{
  // Do some checks on the parameters and variable indeces
  //
  if( paraValue.size()!=4 ) {
    std::cerr << "CellCellAuxinTransport::CellCellAuxinTransport() "
	      << "Uses four parameters; \n"
	      << "d_1 (cell-wall passive trasport), p_p (pin-mediated cell-wall transport, "
	      << "d_2 (wall-cell passive trasport), p_a (aux-mediated wall-cell transport."
	      << std::endl;
    exit(0);
  }
  if( (indValue.size() != 2 && indValue.size() != 3) || 
      indValue[0].size() != 1 || 
      indValue[1].size() != 2 ||
      (indValue.size()==3 && indValue[2].size() != 2) ) {
    std::cerr << "CellCellAuxinTransport::CellCellAuxinTransport() "
	      << "One variable index V_i needed at first level and two at second "
	      << "PIN_ij, AUX_ij." << std::endl << " A third level where "
	      << "indices for P_i and A_i are given is optional." << std::endl;		
    exit(0);
  }
  // Set the variable values
  //
  setId("cellCellAuxinTransport");
  setParameter(paraValue);  
  setVariableIndex(indValue);
  
  // Set the parameter identities
  //
  std::vector<std::string> tmp( numParameter() );
  tmp.resize( numParameter() );
  tmp[0] = "d_1";
  tmp[1] = "p_p";
  tmp[2] = "d_2";
  tmp[3] = "p_a";
  setParameterId( tmp );
}

void CellCellAuxinTransport::derivs(Compartment &compartment,size_t species,
				    std::vector< std::vector<double> > &y,
				    std::vector< std::vector<double> > &dydt ) 
{
  size_t i = compartment.index();
  double V_i = y[i][variableIndex(0,0)];
  for (size_t k=0; k<compartment.numNeighbor(); ++k) {
    size_t j = compartment.neighborRef(k).index();
    if (i<j) {
      double area_ij = compartment.neighborArea(k);
      double PIN_ij = compartment.neighborVariable(variableIndex(1,0),k);
      double AUX_ij = compartment.neighborVariable(variableIndex(1,1),k);
      if (numVariableIndexLevel()==3) {
	PIN_ij *= y[i][variableIndex(2,0)];
	AUX_ij *= y[i][variableIndex(2,1)];
      }
      size_t kk = compartment.neighborRef(k).neighborIndex(i);
      if (kk==static_cast<size_t>(-1)) {
	std::cerr << "CellCellAuxinTransport::derivs() Not neighbor with my neighbor."
		  << std::endl;
	exit(-1);
      }
      double PIN_ji = compartment.neighborRef(k).neighborVariable(variableIndex(1,0),kk);
      double AUX_ji = compartment.neighborRef(k).neighborVariable(variableIndex(1,1),kk);
      if (numVariableIndexLevel()==3) {
	PIN_ji *= y[j][variableIndex(2,0)];
	AUX_ji *= y[j][variableIndex(2,1)];
      }
      // Derivative contributions
      double denominator = 1.0/(2*parameter(2)+parameter(3)*(AUX_ij+AUX_ji));
      double outFlux = (parameter(2)+parameter(3)*AUX_ji)*
	(parameter(0)+parameter(1)*PIN_ij)*
	y[i][species]*denominator;
      double inFlux = (parameter(2)+parameter(3)*AUX_ij)*
	(parameter(0)+parameter(1)*PIN_ji)*
	y[j][species]*denominator;
      double flux = inFlux-outFlux;
      // compartment.setNeighborVariable(size_t(2),k,outFlux);
      dydt[i][species] += flux*area_ij/V_i;
      
      double V_j = y[j][variableIndex(0,0)];
      dydt[j][species] -= flux*area_ij/V_j;
      // compartment.neighborRef(k).setNeighborVariable(size_t(2),kk,inFlux);			
    }
  }
}

CellWallAuxinTransport::
CellWallAuxinTransport(std::vector<double> &paraValue, 
		       std::vector< std::vector<size_t> > 
		       &indValue ) 
{
  // Do some checks on the parameters and variable indeces
  //
  if( paraValue.size()!=5 ) {
    std::cerr << "CellWallAuxinTransport::CellWallAuxinTransport() "
	      << "Uses five parameters; \n"
	      << "d_1 (cell-wall passive trasport), "
	      << "p_p (pin-mediated cell-wall transport, "
	      << "d_2 (wall-cell passive trasport), "
	      << "p_a (aux-mediated wall-cell transport,"
	      << " and apoplastic diffusion D."
	      << std::endl;
    exit(EXIT_FAILURE);
  }
  if( (indValue.size() != 2 && indValue.size() != 3) || 
      indValue[0].size() != 3 || 
      indValue[1].size() != 2 ||
      (indValue.size()==3 && indValue[2].size() != 2) ) {
    std::cerr << "CellWallAuxinTransport::CellWallAuxinTransport() "
	      << "Three variable indices V_i, cell_i and wall_i needed at first level"
	      << " and two at second "
	      << "PIN_ij, AUX_ij." << std::endl << " A third level where "
	      << "indices for P_i and A_i are given is optional." << std::endl;
    exit(EXIT_FAILURE);
  }
  // Set the variable values
  //
  setId("cellWallAuxinTransport");
  setParameter(paraValue);  
  setVariableIndex(indValue);
  
  // Set the parameter identities
  //
  std::vector<std::string> tmp( numParameter() );
  tmp.resize( numParameter() );
  tmp[0] = "d_1";
  tmp[1] = "p_p";
  tmp[2] = "d_2";
  tmp[3] = "p_a";
  tmp[4] = "D";
  setParameterId( tmp );
}

void CellWallAuxinTransport::
derivs(Compartment &compartment,size_t species,
       std::vector< std::vector<double> > &y,
       std::vector< std::vector<double> > &dydt ) 
{
  size_t i = compartment.index();
  double V_i = y[i][variableIndex(0,0)];
  for (size_t k=0; k<compartment.numNeighbor(); ++k) {
    size_t j = compartment.neighborRef(k).index();
    if (i<j) { //Compute both directions at once
      if (y[i][variableIndex(0,1)]==1 && y[j][variableIndex(0,2)]==1 ) {
	//i cell and j wall
	double area_ij = compartment.neighborArea(k);
	double PIN_ij = compartment.neighborVariable(variableIndex(1,0),k);
	double AUX_ij = compartment.neighborVariable(variableIndex(1,1),k);
	if (numVariableIndexLevel()==3) {
	  PIN_ij *= y[i][variableIndex(2,0)];
	  AUX_ij *= y[i][variableIndex(2,1)];
	}
	//Derivative contributions
	double outFlux = (parameter(0)+parameter(1)*PIN_ij)*y[i][species];
	double inFlux = (parameter(2)+parameter(3)*AUX_ij)*y[j][species];
	double flux = inFlux-outFlux;
	//compartment.setNeighborVariable(size_t(2),k,outFlux);
	dydt[i][species] += flux*area_ij/V_i;
	
	double V_j = y[j][variableIndex(0,0)];
	dydt[j][species] -= flux*area_ij/V_j;
      }
      else if (y[j][variableIndex(0,1)]==1 && y[i][variableIndex(0,2)]==1 ) {
	//j cell and i wall
	double area_ij = compartment.neighborArea(k);
	double PIN_ij = compartment.neighborVariable(variableIndex(1,0),k);
	double AUX_ij = compartment.neighborVariable(variableIndex(1,1),k);
	if (numVariableIndexLevel()==3) {
	  PIN_ij *= y[j][variableIndex(2,0)];
	  AUX_ij *= y[j][variableIndex(2,1)];
	}
	//Derivative contributions
	double outFlux = (parameter(0)+parameter(1)*PIN_ij)*y[j][species];
	double inFlux = (parameter(2)+parameter(3)*AUX_ij)*y[i][species];
	double flux = inFlux-outFlux;
	//compartment.setNeighborVariable(size_t(2),k,outFlux);
	dydt[i][species] -= flux*area_ij/V_i;
	
	double V_j = y[j][variableIndex(0,0)];
	dydt[j][species] += flux*area_ij/V_j;
      }
      else if (y[i][variableIndex(0,2)]==1 && y[j][variableIndex(0,2)]==1 ) {
	//i wall and j wall
	double area_ij = compartment.neighborArea(k);
	double flux = y[j][species]-y[i][species];
	double d_ij = std::sqrt( (y[j][0]-y[i][0])*(y[j][0]-y[i][0])+
				 (y[j][1]-y[i][1])*(y[j][1]-y[i][1]) );
	dydt[i][species] += parameter(4)*flux*area_ij/(V_i*d_ij);
	double V_j = y[j][variableIndex(0,0)];
	dydt[j][species] -= parameter(4)*flux*area_ij/(V_j*d_ij);
      }
    }
  }
}

void CellWallAuxinTransport::
derivsWithAbs(Compartment &compartment,size_t species,DataMatrix &y,DataMatrix &dydt, DataMatrix &sdydt) 
{  
  size_t i = compartment.index();
  double V_i = y[i][variableIndex(0,0)];
  for (size_t k=0; k<compartment.numNeighbor(); ++k) {
    size_t j = compartment.neighborRef(k).index();
    if (i<j) { //Compute both directions at once
      if (y[i][variableIndex(0,1)]==1 && y[j][variableIndex(0,2)]==1 ) {
	//i cell and j wall
	double area_ij = compartment.neighborArea(k);
	double PIN_ij = compartment.neighborVariable(variableIndex(1,0),k);
	double AUX_ij = compartment.neighborVariable(variableIndex(1,1),k);
	if (numVariableIndexLevel()==3) {
	  PIN_ij *= y[i][variableIndex(2,0)];
	  AUX_ij *= y[i][variableIndex(2,1)];
	}
	//Derivative contributions
	double outFlux = (parameter(0)+parameter(1)*PIN_ij)*y[i][species];
	double inFlux = (parameter(2)+parameter(3)*AUX_ij)*y[j][species];
	double flux = inFlux-outFlux;
	//compartment.setNeighborVariable(size_t(2),k,outFlux);
	double value = flux*area_ij/V_i;
	dydt[i][species] += value;
	sdydt[i][species] += std::fabs(value);
	
	double V_j = y[j][variableIndex(0,0)];
	value = flux*area_ij/V_j;
	dydt[j][species] -= value;
	sdydt[j][species] += std::fabs(value);
      }
      else if (y[j][variableIndex(0,1)]==1 && y[i][variableIndex(0,2)]==1 ) {
	//j cell and i wall
	double area_ij = compartment.neighborArea(k);
	double PIN_ij = compartment.neighborVariable(variableIndex(1,0),k);
	double AUX_ij = compartment.neighborVariable(variableIndex(1,1),k);
	if (numVariableIndexLevel()==3) {
	  PIN_ij *= y[j][variableIndex(2,0)];
	  AUX_ij *= y[j][variableIndex(2,1)];
	}
	//Derivative contributions
	double outFlux = (parameter(0)+parameter(1)*PIN_ij)*y[j][species];
	double inFlux = (parameter(2)+parameter(3)*AUX_ij)*y[i][species];
	double flux = inFlux-outFlux;
	//compartment.setNeighborVariable(size_t(2),k,outFlux);
	double value = flux*area_ij/V_i;
	dydt[i][species] -= value;
	sdydt[i][species] += std::fabs(value);
	
	double V_j = y[j][variableIndex(0,0)];
	value = flux*area_ij/V_j;
	dydt[j][species] += value;
	sdydt[j][species] += std::fabs(value);
      }
      else if (y[i][variableIndex(0,2)]==1 && y[j][variableIndex(0,2)]==1 ) {
	//i wall and j wall
	double area_ij = compartment.neighborArea(k);
	double flux = y[j][species]-y[i][species];
	double d_ij = std::sqrt( (y[j][0]-y[i][0])*(y[j][0]-y[i][0])+
				 (y[j][1]-y[i][1])*(y[j][1]-y[i][1]) );
	double value = parameter(4)*flux*area_ij/(V_i*d_ij);
	dydt[i][species] += value;
	sdydt[i][species] += std::fabs(value);
	double V_j = y[j][variableIndex(0,0)];
	value = parameter(4)*flux*area_ij/(V_j*d_ij);
	dydt[j][species] -= value;
	sdydt[j][species] += std::fabs(value);
      }
    }
  }
}


    
CytokininNetwork::CytokininNetwork(std::vector<double> &paraValue, 
				   std::vector< std::vector<size_t> > 
				   &indValue ) 
{
  //Do some checks on the parameters and variable indeces
  //////////////////////////////////////////////////////////////////////
  if( paraValue.size()!=24 ) {
    std::cerr << "CytokininNetwork::CytokininNetwork() "
	      << "Uses 28 parameters; \n"
	      << std::endl;
    exit(0);
  }
  if( indValue.size() != 1 || indValue[0].size() != 10 ) {
    std::cerr << "CytokininNetwork::CytokininNetwork() "
	      << "Ten variable indices needed at first level." << std::endl;
    exit(0);
  }
  //Set the variable values
  //////////////////////////////////////////////////////////////////////
  setId("cellCellAuxinTransport");
  setParameter(paraValue);  
  setVariableIndex(indValue);
  
  //Set the parameter identities
  //////////////////////////////////////////////////////////////////////
  std::vector<std::string> tmp( numParameter() );
  tmp.resize( numParameter() );
  tmp[0] = "K_h1";
  tmp[1] = "v_h1";
  tmp[2] = "K_h2";
  tmp[3] = "K_1";
  tmp[4] = "alpha";
  tmp[5] = "v_1";
  tmp[6] = "K_2";
  tmp[7] = "a_0";
  tmp[8] = "a_1";
  tmp[9] = "a_2";
  tmp[10] = "b_0";
  tmp[11] = "b_1";
  tmp[12] = "b_3";
  tmp[13] = "c_0";
  tmp[14] = "c_1";
  tmp[15] = "k_1";
  tmp[16] = "k_2";
  tmp[17] = "d_1";
  tmp[18] = "g_1";
  tmp[19] = "g_2";
  tmp[20] = "g_3";
  tmp[21] = "g_4";
  tmp[22] = "c_c";
  tmp[23] = "d_c";
  setParameterId( tmp );
}

void CytokininNetwork::derivs(Compartment &compartment,size_t species,
			      std::vector< std::vector<double> > &y,
			      std::vector< std::vector<double> > &dydt ) 
{
  size_t i = compartment.index();
  // Variables
  double AHPT = y[i][variableIndex(0,0)];
  double AHPTp = y[i][variableIndex(0,1)];
  double B = y[i][variableIndex(0,2)];
  double Bp = y[i][variableIndex(0,3)];
  double A = y[i][variableIndex(0,4)];
  double WUS = y[i][variableIndex(0,5)];
  double CLV1 = y[i][variableIndex(0,6)];
  double CLV3 = y[i][variableIndex(0,7)];
  double CLV13 = y[i][variableIndex(0,8)];
  double Cyt = y[i][variableIndex(0,9)];
  // Constants
  double K_h1 = parameter(0);
  double v_h1 = parameter(1);
  double K_h2 = parameter(2);
  double K_1 = parameter(3);
  double alpha = parameter(4);
  double v_1 = parameter(5);
  double K_2 = parameter(6);
  double a_0 = parameter(7);
  double a_1 = parameter(8);
  double a_2 = parameter(9);
  double b_0 = parameter(10);
  double b_1 = parameter(11);
  double b_3 = parameter(12);
  double c_0 = parameter(13);
  double c_1 = parameter(14);
  double k_1 = parameter(15);
  double k_2 = parameter(16);
  double d_1 = parameter(17);
  double g_1 = parameter(18);
  double g_2 = parameter(19);
  double g_3 = parameter(20);
  double g_4 = parameter(21);
  double c_c = parameter(22);
  double d_c = parameter(23);

  // AHPT - AHPTp
  double rate = Cyt*AHPT/(K_h1+AHPT) - v_h1*AHPTp/(K_h2+AHPTp);
  dydt[i][variableIndex(0,0)] -= rate; 
  dydt[i][variableIndex(0,1)] += rate; 

  // B - Bp
  rate = (AHPTp*B/(K_1+B))*(1/(1+alpha*A)) - v_1*Bp/(K_2+Bp);
  dydt[i][variableIndex(0,2)] -= rate; 
  dydt[i][variableIndex(0,3)] += rate; 

  // A
  rate = (a_0+a_1*Bp)/(1+a_0+a_1*Bp+a_2*WUS) - g_1*A;
  dydt[i][variableIndex(0,4)] += rate;

  // WUS
  rate = (b_0+b_1*Bp)/(1+b_0+b_1*Bp+b_3*CLV13) - g_4*WUS;
  dydt[i][variableIndex(0,5)] += rate;

  // CLV1
  rate = (c_0)/(1+c_0+c_1*Bp) - g_2*CLV1;
  dydt[i][variableIndex(0,6)] += rate;

  // CLV3
  rate = (d_1*WUS)/(1+d_1*WUS) - g_3*CLV3;
  dydt[i][variableIndex(0,7)] += rate;


  // CLV1+CLV3 - CLV13
  rate = k_1*CLV1*CLV3 - k_2*CLV13;
  dydt[i][variableIndex(0,6)] -= rate;
  dydt[i][variableIndex(0,7)] -= rate;
  dydt[i][variableIndex(0,8)] += rate;

  // Cytokinin
  rate = c_c - d_c*Cyt;
  dydt[i][variableIndex(0,9)] += rate;
}
