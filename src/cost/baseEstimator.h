 

#ifndef BASEESTIMATOR_H
#define BASEESTIMATOR_H

#include <iostream>
#include <fstream>

#include "../solvers/baseSolver.h"
#include "baseProblem.h"

typedef int ID_t;

///
///@brief A base class using a factory method to create optimizers. 
///
/// The BaseEstimator coordinates optimization. It parses the estimator, 
/// costTemplate and init files. Each time an optimizer suggests a new parameter
/// set BaseEstimator::getCost() is used to evaluate how good it was.
class BaseEstimator : public BaseOptimizationProblem {
	
 private:
  BaseSolver *S_;
  size_t numPara_;						// Total number of parameters (Caught from the solver)
  size_t  numCostFile_, numInitFile_;				// should be the same, by the way
  
  std::vector< std::vector< std::vector<double> > > init_;	// Triple table of num files * numCompartments * numVariables
  std::vector< std::vector<double> > finalStateOfFirstRun_; // Double table of numCompartments * numVariables, containing the final state of variables after simuating the first init file given as an input. 
  // std::vector<std::tuple<int, int, int> > initsToReplace_;		// Stores the positions of the elements which are to be copied from finalSateOfFirstRun_ to init. the order is: file, compartment, variable._
  //std::vector< std::vector<size_t> > initsToReplace_;		// vector of 3-element vectors, containing the  indexes file, compartment and variable of init_ which are flagged for replacement of with the final values of the first simulation.


  struct initElementToReplace {
				size_t fileNumber;
				size_t compartment;
				size_t variable;
				double multiplier;
  };
  std::vector<initElementToReplace> initsToReplace_;
	std::vector<std::vector<std::vector<double> > > finalSimulationValues_;

	bool all_init_from_previous_;
	bool first_init_from_previous_;
  std::vector<std::string> costTemplateFile_, initFile_;	
  
  std::vector< std::vector<size_t> > costLists_;
  std::vector< std::vector<double> > costTemplateTime_;
  std::vector< std::vector<std::vector<std::vector<double> > > > costTemplate_;
  
  //static BaseEstimator *createEstimator(BaseSolver *S, const std::string &file, bool &init_from_previous);
  //static BaseEstimator* createEstimator(BaseSolver *S, const std::ifstream &IN, bool &init_from_previous);
	
  void readEstimator(const std::string &file);
  void readEstimator(std::ifstream &IN); // The reading point should be just  before NumCostFiles (ie very beginning of the cost file)
  
  void readCostTemplate();
  void readInit();
  void processInit(); 
	void copyFinalValueToInit(size_t fileNumber);
  //std::vector<bool> table_occupation_ID;
  //std::vector< std::vector< std::vector< std::vector<double> > > > saved_init_;
  
 public:
	///
	/// @brief the constructor initiates the parsing of the estimator, cost and init files.
	///
	/// \section estimator Defining the estimator file:
	/// 
	/// <pre>
	/// numCostTemplates
	/// numOptVariable_1 numOptVariable_2 ... numOptVariable_n 
	/// file_1.cost
	/// file_2.cost
	/// ...
	/// file_N.cost
	/// optIndex_1 optIndex_2 ... optIndex_n
	/// numInitFiles
	/// file_1.init
	/// file_2.init
	/// ...
	/// file_3.init
	/// </pre>
	///
	/// Explanation of the parameters:
	///
	/// <ul>
	///
	/// <li> numCostTemplates is an interger specifying how many cost template
	/// files are to be used.
	///
	/// <li> numOptVariable are integers specifying how many variables in the 
	/// corresponding cost file are to be used.
	///
	/// <li> file_x.cost are strings specifying the name of the cost file. They
	/// must be as many as numCostTemplates.
	///
	/// <li> optIndex_x is the (integer) index/indices of the variables to be used
	/// from file_x.cost. The total number of indices specified must be equal to
	/// the sum of all numOptVariables.
	///
	/// <li> numInitFiles is an integer specifying how many init files will be
	/// used. All cost files must have an accompanying init file, thus 
	/// numIniFiles must be equal to numCostTemplates.
	///
	/// <li> file_x.init is a string specifying the init file which is to be used
	/// together with file_x.cost.
	///
	/// </ul>
	///
	/// Example of file:
	///
	/// <pre> 
	/// 3			# Number of cost template files 
	/// 2 1 1			# Number of variables to be optimized in each cost template file
	/// wt.cost			# Cost template file names
	/// mutant_1.cost
	/// mutant_2.cost
	/// 0 2 2 2		# Index of variables to be optimized against. The first two belong to wt.cost
	/// 3			# Number of initial files (needs to be same as cost template files)
	/// wt.init			# Init file name(s)
	/// mutant_1.init		
	/// mutant_2.init
	/// </pre>
	///
	/// These cost files will override the one wich is specified in the 
	/// RK5AdaptiveTemplate solver file. There must however be one specified in
	/// the solver file, or the parser will get sad.
	///
	/// \section cost Defining the cost file:
	///
	/// <pre>
	/// time_1 numCompartments numVariables
	/// val_comp1_var1 val_comp1_var2 ... val_comp1_varN 
	/// val_comp2_var1 val_comp2_var2 ... val_comp2_varN 
	/// ...
	/// val_compM_var1 val_compM_var2 ... val_compM_varN 
	/// 
	/// readNewCompartments
	///
	/// time_2 numCompartments numVariables
	/// val_comp1_var1 val_comp1_var2 ... val_comp1_varN 
	/// val_comp2_var1 val_comp2_var2 ... val_comp2_varN 
	/// ...
	/// val_compM_var1 val_compM_var2 ... val_compM_varN 
	/// 
	/// readNewCompartments
	///
	/// ...
	/// 
	/// time_T numCompartments numVariables
	/// val_comp1_var1 val_comp1_var2 ... val_comp1_varN 
	/// val_comp2_var1 val_comp2_var2 ... val_comp2_varN 
	/// ...
	/// val_compM_var1 val_compM_var2 ... val_compM_varN 
	/// 
	/// readNewCompartments
	/// </pre>
	///
	/// Explanation of the parameters:
	/// <ul>
	/// <li> time_x  -- the cost will be evaluated at each of the time points 
	/// given.
	///
	/// <li> numCompartments is the number of compartments.
	///
	/// <li> numVariables is the number of variables (duh!).
	///
	/// <li> val_compX_varY is the target simulation value of variable Y in
	/// compartment X. These does not have to be the same for each time point.
	/// They will only matter if they are in the column of variables specified 
	/// for cost calculation by the estimator file (optIndex_X in the description
	/// above).
	///
	/// <li> readNewCompartments is supposed to flag compartment changes, but is
	/// currently not in use and must be set to 0.
	/// </ul>
	///
	/// Example of cost template file:
	/// <pre>
	/// 400 2 4		# evaluate at time 400, 2 compartments, 4 variables.
	/// 0 0 2.0 0	# target values for the first compartment.
	/// 0 0 3.0 0	# second compartment. Values only matter if specified in estimator file.
	/// 
	/// 0		# required (no compartment update)
	/// 
	/// 500 2 4		# evaluate at time 500, same compartment and variable numbers as above.
	/// 0 0 5 0		# target values
	/// 0 0 6 0
	///
	/// 0 
	/// </pre>
	/// The file supports comments preceded by a #.
	///
	///
	/// \section initialFile Defining the init file for the optimizer:
	/// 
	/// <pre>
	/// numCompartments numVariables
	/// val_comp0_var0 val_comp0_var1 ... val_comp0_varN
	/// val_comp1_var0 val_comp1_var1 ... val_comp1_varN
	/// ...
	/// val_compM_var0 val_compM_var1 ... val_compM_varN
	/// </pre>
	///
	/// Explanation of the parameters:
	/// <ul>
	///
	/// <li> numCompartments is an integer defining the number of compartments
	/// of the simulation (defines how many rows of values the file should have).
	///
	/// <li> numVariables is an integer defining the number of variables in the 
	/// model.
	///
	/// <li> val_compX_varY defines the initial value of the simulations for 
	/// variable Y, in compartment X. This is commonly a number, but if you are 
	/// using more than one init file for the optimization procedure it does not
	/// have to be. In any file after the first one specified in the estimator
	/// file you may specify a special flag. 
	/// <ul>
	/// <li> $ use the final simulation value of the same compartment and variable
	/// which were attained by the simulation which used the first init file 
	/// specified. That was a difficult sentence... it basically means that in
	/// order to calculate the cost of a parameter set the optimizer will run a 
	/// bunch of simulations. This flag ensures that you can start any simulation
	/// after the first where the first left off.
	/// <li> $x followed by a number. Like the above, but multiply the copied
	/// value with the number specified. Ex. $x10.5
	/// </ul>
	/// </ul>
	///
	/// First example of files:
	/// 
	/// file_1.init
	/// <pre>
	/// 2 4		# numCompartments numVariables
	/// 1.0 1.0 1.0 1.0 # the first init file must specify numbers
	/// 1.0 1.0 1.0 1.0
	/// </pre>
	/// file_2.init
	/// <pre>
	/// 2 4		# numCompartents must be the same as in file_1.init if any $ is to 
	///			# be used. numVariable must be equal to that of file_1.init regardless
	/// $x10 $ $ 0	# specify initial values, or flags.
	/// $x10 $ $ 0
	/// </pre>
	/// Mutants are often implemented by a binary switch in the init files. Using
	/// these flags you may let a mutant start in the equilibrium configuration of
	/// the WT. Using the $x<number> flag you may optimize towards transient 
	/// perturbations. 
	///
	/// Comments can be used in the file, and should be preceded by a #.
	///
	/// Note:
	/// The optimizer can take command line arguments (enter the command 
	/// optimizer -help to see more) which causes the initial values to be taken
	/// from the corresponding final values of the last parameter update attempt.
	/// If you specify that all init files should be treated such this overrides
	/// specifications in the init file. It may however be beneficial to use these
	/// input arguments to copy the init values of the first file (file_1.init) 
	/// from its previous simulation, as this may decrease the time it takes for
	/// the solver to reach equilibrium. You must however be mindfull of how this
	/// affects the results.

	BaseEstimator(BaseSolver *S, const std::string &file);
	BaseEstimator(BaseSolver *S, const std::ifstream &IN);
	BaseEstimator(BaseSolver *S, const std::string &file, bool &all_init_from_previous, bool &first_init_from_previous);
	BaseEstimator(BaseSolver *S, const std::ifstream &IN, bool &all_init_from_previous, bool &first_init_from_previous);
	virtual ~BaseEstimator();
	double getCost();
	void printState(double E,std::ostream &os = std::cout) const;

	//void set_init_from_ID(ID_t);
	//void save_values_to_ID(ID_t);
	//void kill_ID(ID_t);
	//ID_t create_ID();
	//void set_initial_init();
 private:
	void save_initial_init_to_ID0();


	inline size_t numPara() const;
	inline double parameter(size_t i) const;
  inline void setParameter(size_t i, double value);
  //inline void setParameter(std::vector<double> &parameter);    
  
};

inline size_t BaseEstimator::numPara() const
{
  return numPara_;
}

inline double BaseEstimator::parameter(size_t i) const
{
  return S_->getOrganism()->parameter(i);
}    

inline void BaseEstimator::setParameter(size_t i, double value) 
{
  S_->getOrganism()->setParameter(i, value);
}

#endif		
