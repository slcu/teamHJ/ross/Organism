#include "baseEstimator.h" 
#include "../common/typedefs.h"

BaseEstimator::BaseEstimator(BaseSolver *S, const std::ifstream &IN) : BaseOptimizationProblem()
{
	S_ = S;
	all_init_from_previous_ = false;
	first_init_from_previous_ = false;
	//saved_init_.resize(1);
	//table_occupation_ID.resize(1, false);
	readEstimator((std::ifstream &) IN);
	//table_occupation_ID[0] = true; // The first saved init is and will remain the initial init.
	//save_initial_init_to_ID0();
}

BaseEstimator::BaseEstimator(BaseSolver *S, const std::string &file)  : BaseOptimizationProblem()
{
	S_ = S;
	all_init_from_previous_ = false;
	first_init_from_previous_ = false;
	//saved_init_.resize(1);
	//table_occupation_ID.resize(1, false);
	readEstimator(file);
	//table_occupation_ID[0] = true; // The first saved init is and will remain the initial init.
	//save_initial_init_to_ID0();
}
BaseEstimator::BaseEstimator(BaseSolver *S, const std::ifstream &IN, bool &all_init_from_previous, bool &first_init_from_previous) : BaseOptimizationProblem()
{
	S_ = S;
	all_init_from_previous_=all_init_from_previous;
	first_init_from_previous_ = first_init_from_previous;
	//saved_init_.resize(1);
	//table_occupation_ID.resize(1, false);
	readEstimator((std::ifstream &) IN);
	//table_occupation_ID[0] = true; // The first saved init is and will remain the initial init.
	//save_initial_init_to_ID0();
}

BaseEstimator::BaseEstimator(BaseSolver *S, const std::string &file, bool &all_init_from_previous, bool &first_init_from_previous)  : BaseOptimizationProblem()
{
	S_ = S;
	all_init_from_previous_=all_init_from_previous;
	first_init_from_previous_ = first_init_from_previous;
	//saved_init_.resize(1);
	//table_occupation_ID.resize(1, false);
	readEstimator(file);
	//table_occupation_ID[0] = true; // The first saved init is and will remain the initial init.
	//save_initial_init_to_ID0();
}

BaseEstimator::~BaseEstimator()
{
	// free the init table
}
/*
BaseEstimator *BaseEstimator::createEstimator(BaseSolver *S, const std::string &file, bool all_init_from_previous) 
{
	return (new BaseEstimator(S, file, all_init_from_previous));
}

BaseEstimator *BaseEstimator::createEstimator(BaseSolver *S, const std::ifstream &IN, bool all_init_from_previous) 
{
	return (new BaseEstimator(S, (std::ifstream &) IN, all_init_from_previous));
}
*/
void BaseEstimator::readEstimator(const std::string &file)
{
	std::istream *IN = myFiles::openFile(file);
	if (!IN) {
		std::cerr << "BaseEstimator::readEstimator() - " << "Cannot open file " << file << std::endl;
		exit(-1);
	}
	readEstimator((std::ifstream &) *IN);
	delete IN;
}

void BaseEstimator::readEstimator(std::ifstream &IN) 
{

	//read in the number of costTemplateFiles used and how many variables each
	//file use
	IN >> numCostFile_;
	costTemplateFile_.resize(numCostFile_);
	costLists_.resize(numCostFile_);
	costTemplateTime_.resize(numCostFile_);
	costTemplate_.resize(numCostFile_);
	init_.resize(numCostFile_);
	finalSimulationValues_.resize(numCostFile_);
	//initsToReplace_.resize(numCostFile_);

	for (size_t i = 0; i < numCostFile_; i++) {
		int numCostVar;
		IN >> numCostVar; 
		costLists_[i].resize(numCostVar);
	}

	// read in the costTemplateFiles
	//
	std::cerr << numCostFile_ << " (" << costTemplateFile_.size() << ") costtemplate files used:" << std::endl;
	for (size_t i=0; i < numCostFile_; ++i) 
		IN >> costTemplateFile_[i];

	//read in the different costlists
	//
	for (size_t i = 0; i < costLists_.size(); ++i) {
		std::cerr << "Cost template file " << costTemplateFile_[i] << " fitted with variables ";
		for (size_t j= 0; j < costLists_[i].size(); j++) {
			IN >> costLists_[i][j];
			std::cerr << costLists_[i][j] << " ";
		}
		std::cerr << "\n";
	} 
	readCostTemplate();

	// read in the init files
	//
	IN >> numInitFile_;
	if (numCostFile_ != 0 && numInitFile_ !=numCostFile_) {
		std::cerr << "The number of costTemplateFiles must match the number of initFiles.\n";
		exit(-1);
	}
	initFile_.resize(numInitFile_);

	std::cerr << "Using "<< numInitFile_ << " init files:\n";
	for (size_t i = 0; i < numInitFile_; ++i) {
		IN >> initFile_[i];
		std::cerr << initFile_[i] << "\n";
	}

	std::cerr << "readInit\n";
	readInit();
	std::cerr << "Done\n";

	//Sets inital values
	getCost();
	numPara_ = S_->getOrganism()->numParameter();
}

void BaseEstimator::readInit()
{

	std::cerr << "in readInit\n";


	for (size_t n=0; n<numInitFile_; ++n) {
		const char* tmp = initFile_[n].c_str();
		std::istream *IN = myFiles::openFile(tmp);
		if( !IN ) {
			std::cerr << "BaseEstimator::readInit() "
				<< "Cannot open init file " << initFile_[n]
				<< "\n\n\7";exit(-1);
		}
		size_t numCompartmentVal,numVariableVal;
		*IN >> numCompartmentVal;
		*IN >> numVariableVal;
		init_[n].resize(numCompartmentVal);
		finalSimulationValues_[n].resize(numCompartmentVal);
		for( size_t i=0 ; i<numCompartmentVal ; i++ ) { 
			init_[n][i].resize(numVariableVal);
			finalSimulationValues_[n][i].resize(numVariableVal);
		}

		for( size_t i=0 ; i<numCompartmentVal ; i++ ) {
			for( size_t j=0 ; j<numVariableVal ; j++ ) {
				*IN >> std::ws;

				// peek at the next value of the stream, without removing it.
				int comingStreamValue = (*IN).peek();
				// If the next value of *IN is starts with a digit then enter the value
				// into init_
				if ( std::isdigit( comingStreamValue ) ) {
					*IN >> init_[n][i][j];
				}
				// if the next value of *IN is a $-flag then store the memory of which
				// init_ element must be post processed by BaseEstimator::getCost()
				else if ( comingStreamValue=='$' && n!=0  ) { 
					(*IN).ignore(); // throw away the next value in the sream *IN

					initElementToReplace tmpInitElement;
					tmpInitElement.fileNumber = n;
					tmpInitElement.compartment = i;
					tmpInitElement.variable = j;
					// check if any multiplying factors were specified in the init file.	
					if ( (*IN).peek()=='x' || (*IN).peek()=='*' ){
						(*IN).ignore();
						if ( std::isdigit( (*IN).peek() ) ){
							*IN >> tmpInitElement.multiplier; 
						}
						else {
							std::cerr << "The flag $x was used in an init file without being \n"
								<< "	directly followed by a digit.";
							exit(-1);
						}

					}
					else tmpInitElement.multiplier=1; //multiplying factor set to one.

					initsToReplace_.push_back( tmpInitElement );

				}
				else if ( !( (*IN).eof() ) ) {
					*IN >> init_[n][i][j];
					// char a;
					// *IN >> a;
					/*
					  std::cerr << "BaseEstimator::readInit() "
						<< "unsupported character in init file number "
						<< n << std::endl
						<< "character: " << a << std::endl
						<< "input arguments must be numbers or the flag $, which tells the \n"
						<< "optimizer to use the final values attained by the simulation \n"
						<< "of the first file specified in the analyser file.\n"
						<< "The flag $ must therefore not be present in the first init file \n"
						<< "given as an argument in the analyzer file.\n";
					exit(-1);
					*/
				}
			}
		}
		delete IN;
	}
}

void BaseEstimator::readCostTemplate()
{    
	for (size_t c=0; c<costTemplateFile_.size(); ++c) {
		if( costLists_[c].size()==0 ) {
			std::cerr << "BaseEstimator::readCostTemplate() "
				<< "No variables defined to calculate cost from.\n";
			exit(-1);
		}

		costTemplateTime_[c].resize(0);
		costTemplate_[c].resize(0); 
		const char* tmp = costTemplateFile_[c].c_str();
		std::istream *IN = myFiles::openFile(tmp);
		if( !IN ) {
			std::cerr << "BaseEstimator::readCostTemplate() "
				<< "Cannot open cost templatefile " << costTemplateFile_[c]
				<< "\n\n\7";exit(-1);}
		std::cerr << "Opening file " << costTemplateFile_[c] << "\n";
		int count=0;
		double tmpVal;
		size_t N1,M1,Ndiv1;
		double time1;

		do {
			//not first time
			if( count++ ) {
				*IN >> Ndiv1;
				for (size_t i=0 ; i<Ndiv1 ; i++ ) {
					*IN >> tmpVal;
					*IN >> tmpVal;
				}
			}
			*IN >> time1;
			*IN >> N1;
			*IN >> M1;
			if( time1>=S_->startTime() && *IN ) {
				//Start reading file
				costTemplateTime_[c].push_back( time1 );

				std::vector< std::vector<double> > tmp( N1 );
				for (size_t i=0 ; i<N1 ; i++) {
					size_t k=0;
					tmp[i].resize( costLists_[c].size() );
					for (size_t j=0 ; j<M1 ; j++ ) {
						*IN >> tmpVal;
						if( k<costLists_[c].size() && j == size_t(costLists_[c][k]) ) {
							tmp[i][k] = tmpVal;
							//		std::cerr << "found match: " << i << " " << j << " " << k 
							//				<< " " << tmpVal << " " << count << "\n"; 
							k++;
						}
					}
				}
				costTemplate_[c].push_back( tmp );
				//std::cerr << S_->startTime() << " " << S_->endTime() << " " 
				//					<< time1 << " " << c << " " << check <<"\n";
			}
		} while( time1<=S_->endTime() && *IN );
		delete IN;
	}
}

double BaseEstimator::getCost() 
{
	double cost = 0;
	assert(init_.size() == numCostFile_);
	assert(costTemplateFile_.size() == numCostFile_);
	assert(costTemplate_.size() == numCostFile_);
	assert(costTemplateTime_.size() == numCostFile_);
	assert(costLists_.size() == numCostFile_);

	for (size_t k=0; k<numCostFile_; k++) {
		if (k==1) // if the first simulation is done and others are to come.
			processInit();

		S_->getOrganism()->setInit(init_[k]);		
		S_->setCostTemplateFile(costTemplateFile_[k]);
		S_->setCostTemplate(costTemplate_[k]);
		S_->setCostTemplateTime(costTemplateTime_[k]);
		S_->setCostList(costLists_[k]);
		S_->getInit();
		S_->simulate();
		cost += S_->cost();

		if (all_init_from_previous_ || (first_init_from_previous_ && k==0) ){
			//overwrite the init which will be used the next time the file k is used.
			//this overrides any $ flags in the init file (processed by processInit().
			copyFinalValueToInit(k);
		}
	}
	return cost;
}

void BaseEstimator::copyFinalValueToInit(size_t fileNumber) {
	for (size_t i=0; i<init_[fileNumber].size(); i++) {
		for (size_t j=0; j<init_[fileNumber][i].size(); j++) {
			init_[fileNumber][i][j]= S_->y(i,j);
		}
	}
}

void BaseEstimator::processInit () {
	// Processes the init_ vector such that variables of the coming simulations 
	// may start where the first one finished (if this is specified in the init file).
	double newValue;
	for (size_t i=0; i<initsToReplace_.size(); i++) {
		newValue = S_->y(initsToReplace_[i].compartment, initsToReplace_[i].variable);
		init_[ initsToReplace_[i].fileNumber ]
			[ initsToReplace_[i].compartment ]
			[ initsToReplace_[i].variable ]
			= initsToReplace_[i].multiplier * newValue;
	}
}

void BaseEstimator::printState(double E,std::ostream &os) const
{
	os << E << "  ";
	for (size_t i=0; i<numPara(); ++i)
		os << parameter(i) << " ";
	os << "\n";
	os.flush();
}



/// THIS PART HAS NOT BEEN CHECKED
/*

	 void BaseEstimator::set_init_from_ID(ID_t targetID){
	 if(table_occupation_ID[targetID] != true) {
	 std::cerr << "attempts to assign a init condition with wrong ID\n";
	 return;
	 }

	 init_.resize(numInitFile_);
	 for(size_t i = 0; i < numInitFile_; ++i){
	 int numCompartmentVal = saved_init_[targetID][i].size();
	 init_[i].resize(numCompartmentVal);
	 for(int j = 0; j < numCompartmentVal; ++j){
	 int numVariableVal = saved_init_[targetID][i][j].size();
	 init_[i][j].resize(numVariableVal);
	 for(int k = 0; k < numVariableVal; ++k){
	 init_[i][j][k] = saved_init_[targetID][i][j][k];
	 }
	 }
	 }
	 }

	 void BaseEstimator::save_initial_init_to_ID0(){
	 if(table_occupation_ID.size() < 1) table_occupation_ID.resize(1);
	 table_occupation_ID[0] = true;

	 saved_init_[0].resize(numInitFile_);
	 for(size_t i = 0; i < numInitFile_; ++i){
	 int numCompartmentVal = init_[i].size();
	 saved_init_[0][i].resize(numCompartmentVal);
	 for(int j = 0; j < numCompartmentVal; ++j){
	 int numVariableVal = init_[i][j].size();
	 saved_init_[0][i][j].resize(numVariableVal);
	 for(int k = 0; k < numVariableVal; ++k){
	 saved_init_[0][i][j][k] = init_[i][j][k];
	 }
	 }
	 }
	 }


	 void BaseEstimator::save_values_to_ID(ID_t targetID){
	 if(targetID == 0){
	 std::cerr << "Can not save in ID=0 because it is the place for the initial inits\n";
	 return;
	 }
	 if(table_occupation_ID[targetID] == true){
	 std::cerr << "Impossible to save values in ID " << targetID << " because the place is already taken. Use kill ID to free it if you want.\n";
	 return;
	 }
// to check, I'm not sure the values are here
S_->getOrganism()->neighborhoodCreate((DataMatrix&) saved_init_[targetID], S_->t_);	
}



void BaseEstimator::kill_ID(ID_t targetID){
if(saved_init_.size() != table_occupation_ID.size()){
std::cerr << "Intern problem : tablo occupation ID and saved_init_ have not the same size.\n";
return;
}
if(targetID > 0 && targetID < (ID_t) table_occupation_ID.size()){ // 0 is reserved for the initial inits
if(table_occupation_ID[targetID] == true){
for(size_t i = 0; i < numInitFile_; ++i){
int numCompartmentVal = saved_init_[targetID][i].size();
for(int j = 0; j < numCompartmentVal; ++j){
saved_init_[targetID][i][j].clear();
}
saved_init_[targetID][i].clear();
}
saved_init_[targetID].clear();

if(table_occupation_ID[targetID] == false);
if(targetID == (ID_t) table_occupation_ID.size() - 1){
	table_occupation_ID.resize(targetID-1);
	saved_init_.resize(targetID - 1);
}
}
}
}

ID_t BaseEstimator::create_ID(){
	if(saved_init_.size() != table_occupation_ID.size()){
		std::cerr << "Intern problem : tablo occupation ID and saved_init_ have not the same size.\n";
		return -1;
	}
	int first_available = -1;
	for(size_t i = 0; i < table_occupation_ID.size(); ++i){
		if(table_occupation_ID[i] == false){
			first_available = i;
			break;
		}
	}
	if(first_available == -1){
		table_occupation_ID.resize(table_occupation_ID.size() + 1);
		saved_init_.resize(saved_init_.size() + 1);
		return saved_init_.size()-1;
	}
	return first_available;
}

void BaseEstimator::set_initial_init(){
	set_init_from_ID(0);
}
*/
