#ifndef BASEOPTPROBLEM_H
#define BASEOPTPROBLEM_H

class BaseOptimizationProblem{
	public:
		BaseOptimizationProblem(){};
		virtual double getCost(){return 0;};
		virtual inline double parameter(size_t i) const {return 0;};
		virtual inline void setParameter(size_t i, double value){};
		virtual inline size_t numPara() const{return 0;};
		virtual void printState(double E,std::ostream &os = std::cout) const {};
};

#endif